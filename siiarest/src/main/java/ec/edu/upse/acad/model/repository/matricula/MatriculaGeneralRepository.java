package ec.edu.upse.acad.model.repository.matricula;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.matricula.MatriculaGeneral;

@Repository
public interface MatriculaGeneralRepository extends JpaRepository<MatriculaGeneral, Integer>{

	/**
	 * Método para llamar al procedimiento almacenado recuperando las asignatura de un estudiante para registrar una  matricula
	 * 
	 * @param idEstudianteOferta
	 * @param opcion
	 * @return
	 */

	@Query(value = "{call  aca.pa_generar_asignaturas_a_tomar(:idEstudianteOferta,:opcion)}", nativeQuery = true)
	public List<CustomObjectCargarAsignaturasMatriculas> getSPAsignaturasGeneradas(@Param("idEstudianteOferta") Integer idEstudianteOferta,
			@Param("opcion") Integer opcion);
	/**
	 * interfaz para recuperar las variables de procedimiento almacenado CustomObjectCargarAsignaturasMatriculas
	 * @author msoriano
	 *
	 */
	public interface CustomObjectCargarAsignaturasMatriculas{
		Integer getIdDocenteAsignaturaAprend();
		Long getIdEstudianteAsignatura();
		Integer getIdMallaAsignatura();
		String getNivel();
		String getNombreAsignatura();
		String getTotalHorasA();
		String getTotalHorasD();
		String getNumVez();
		Integer getNumeroCreditos();
		Integer getIdCostoAsignatura();
		Float getValorAsignatura();
		Integer getIdDocentePractica();
		Long getIdEstudianteAsignaturaPrac();
	}

	@Query(value=" select pa.id as idPeriodoAcademico ,pa.codigo as codigo, pa.descripcion as descripcion," +
			" mg.id as idMatriculaGeneral,mg.fechaDesde as fechaDesde,mg.fechaHasta as fechaHasta  from MatriculaGeneral mg " + 
			" inner join PeriodoAcademico pa on pa.id = mg.idPeriodoAcademico where pa.estado ='A' and mg.estado= 'A' "+
			" order by pa.codigo desc")
	List<CustomObjectPeriodoMatricula> listarPeriodosMatricula();

	interface CustomObjectPeriodoMatricula{ 
		Integer getIdPeriodoAcademico(); 
		String getCodigo(); 
		String getDescripcion(); 
		Integer getIdMatriculaGeneral();
		Date getFechaDesde();
		Date getFechaHasta();
	} 
	
	@Query(value=" select mg.id as idMatriculaGeneral,pa.id as idPeriodoAcademico,mft.id as idTipoMatriculaFecha,"+
			 " tm.id as idTipoMatricula, tm.descripcion as tipoMatricula, "+
			" mft.fechaDesde as fechaDesdeTm, mft.fechaHasta as fechaHastaTm  " + 
			" from MatriculaGeneral as mg " + 
			" inner join PeriodoAcademico as pa on mg.idPeriodoAcademico=pa.id " + 
			" inner join TipoMatriculaFecha as mft on mg.id=mft.idMatriculaGeneral " + 
			" inner join TipoMatricula as tm on mft.idTipoMatricula=tm.id "+
		  " where mg.estado='A' and pa.estado='A' and mft.estado='A' and tm.estado='A'")
	List<CustomObjectPeriodoTipoMatricula> listarPeriodoTipoMatricula();

	interface CustomObjectPeriodoTipoMatricula{ 
		Integer getIdMatriculaGeneral(); 
		Integer getIdPeriodoAcademico(); 
		Integer getIdTipoMatriculaFecha(); 
		Integer getIdTipoMatricula();
		String getTipoMatricula();
		Date getFechaDesdeTm();
		Date getFechaHastaTm();
	} 
			
	@Query(value=" select mg.id as idMatriculaGeneral, mg.idPeriodoAcademico  as idPeriodoAcademico from MatriculaGeneral mg where mg.idPeriodoAcademico = "+
			" (select pa.id from PeriodoAcademico pa where cast(CURRENT_TIMESTAMP as date) >= pa.fechaDesde and cast(CURRENT_TIMESTAMP as date)<=pa.fechaHasta)")
	List<CustomObjectValidarPeriodo> validarUltimoPeriodoMatricula();
	public interface CustomObjectValidarPeriodo{ 
		Integer getIdMatriculaGeneral();
		Integer getIdPeriodoAcademico(); 
	} 

	
//
//	@Query(value=" SELECT pma.idPeriodoAcademico as idPeriodoAcademico, ma.id as idMalla, mas.id as idMallaAsignatura,ofe.id as idOfertaAsignatura,"+
//			" ofe.descripcionCorta as ofertaAsignatura,asi.id as idAsignatura,"+
//			"asi.descripcion as asignatura, asi.descripcionCorta as asignaturaCorta, n.orden as nivel " + 
//			"FROM PeriodoMalla as pma " + 
//			"INNER JOIN Malla as ma on pma.idMalla=ma.id " + 
//			"INNER JOIN MallaAsignatura as mas on ma.id=mas.idMalla " + 
//			"INNER JOIN Asignatura as asi on mas.idAsignatura=asi.id " + 
//			"INNER JOIN OfertaAsignatura as oas on asi.id=oas.idAsignatura " + 
//			"INNER JOIN Oferta as ofe on oas.idOferta=ofe.id " + 
//			"INNER JOIN Nivel as n on mas.idNivel=n.id " + 
//			"WHERE pma.idPeriodoAcademico=(?1) and ofe.id=(?2) and mas.numParalelos>0  " + 
//			"and n.id>=ma.idNivelMinAperturado and n.id<=ma.idNivelMaxAperturado " + 
//			"and pma.estado='A' and ma.estado='A' and mas.estado='A'  " + 
//			"and asi.estado='A' and oas.estado='A' and ofe.estado='A' and n.estado='A' " 		)
//	List<CustomObjectOfertaAsignatura> listarOfertaAsignatura(Integer idPeridoAcademico,Integer IdOferta);	
//	interface CustomObjectOfertaAsignatura  { 
//		Integer getIdPeriodoAcademico(); 
//		Integer getIdMalla();
//		Integer getIdMallaAsignatura();
//		Integer getIdOfertaAsignatura();
//		String getOfertaAsignatura();
//		Integer getIdAsignatura();
//		String getAsignatura();
//		String getAsignaturaCorta();
//		Integer getNivel();
//	}
//	

//	@Query(value=" SELECT mas.id as idMallaAsignatura, par.id as idParalelo, "+
//			"CONCAT(cast(n.orden as text),'/',par.descripcionCorta) as paralelo "+
//			"FROM PeriodoMalla as pma, Malla as ma, MallaAsignatura as mas, Nivel as n , Paralelo as par, " + 
//			" Asignatura as asi, OfertaAsignatura as oas, Oferta as ofe "+
//			"WHERE pma.idMalla=ma.id and ma.id=mas.idMalla and mas.idAsignatura=asi.id and asi.id=oas.idAsignatura and "+
//			"oas.idOferta=ofe.id  and  mas.idNivel=n.id and par.orden<= mas.numParalelos and pma.idPeriodoAcademico=(?1) and ofe.id=(?2) " + 
//			"and mas.estado='A' and n.estado='A' and par.estado='A' " 		)
//	List<CustomObjectAsignaturaParalelo> listarAsignaturaParalelo(Integer idPeriodo, Integer idOferta);	
//	interface CustomObjectAsignaturaParalelo  { 
//		Integer getIdMallaAsignatura();
//		Integer getIdParalelo();
//		String getParalelo(); 
//	}

	@Query(value=" select ma.idMalla as idMalla,ma.idNivel as idNivel,count(ma.idNivel) as numMaterias from MallaAsignatura ma " + 
			" where ma.idMalla = (select eo.idMalla from EstudianteOferta eo where eo.id = (?1)) " + 
			" group by ma.idMalla,ma.idNivel ")
	List<CustObjtNumAsignaturas> numeroAsignaturasPorNivel(Integer idEstudianteOferta);	
	interface CustObjtNumAsignaturas  { 
		Integer getIdMalla();
		Integer getIdNivel();
		Integer getNumMaterias(); 
	}

}
