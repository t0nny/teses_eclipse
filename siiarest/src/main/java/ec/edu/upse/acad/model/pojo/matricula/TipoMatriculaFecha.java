package ec.edu.upse.acad.model.pojo.matricula;

import java.util.Date;
import javax.persistence.*;
import org.hibernate.annotations.Where;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="aca", name="tipo_matricula_fecha")
@Where(clause = "estado='A'")
@NoArgsConstructor
public class TipoMatriculaFecha{

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_tipo_matricula_fecha")
	@Getter @Setter private Integer id;

	@Column(name="id_matricula_general")
	@Getter @Setter private Integer idMatriculaGeneral;
	
	@Column(name="id_tipo_matricula")
	@Getter @Setter private Integer idTipoMatricula;
	
	@Getter @Setter private String estado;

	@Column(name="fecha_desde")
	@Getter @Setter private Date fechaDesde;

	@Column(name="fecha_hasta")
	@Getter @Setter private Date fechaHasta;

//	@Column(name="fecha_ingreso")
//	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private int version;

	//RELACIONES
	//bi-directional many-to-one association to MatriculaGeneral
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_matricula_general" , insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private MatriculaGeneral matriculaGeneral;

	//bi-directional many-to-one association to TipoMatricula
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_tipo_matricula" , insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private TipoMatricula tipoMatricula;

	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }
	 
}