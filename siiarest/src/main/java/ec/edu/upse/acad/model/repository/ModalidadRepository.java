package ec.edu.upse.acad.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.Modalidad;


@Repository
public interface ModalidadRepository extends JpaRepository<Modalidad, Integer> {
	
	@Query(value="SELECT m.id as id,m.codigo as codigo,m.descripcion as descripcion,m.estado as estado, m.version as version " + 
			"FROM Modalidad m " + 
			"WHERE m.estado = 'A' ")
	List<CustomObjectModalidad> buscarmodalidad();
	
	interface CustomObjectModalidad {
		Integer getId();
		String getCodigo();
		String getDescripcion();
		String getEstado();
		Integer getVersion();
	}

	
	@Query(value="SELECT m.id,m.codigo,m.descripcion,m.estado,m.version " + 
			"FROM Modalidad m " + 
			"WHERE m.estado = 'A' and m.id = (?1) ")
	List<CustomObjectModalidadId> buscarmodalidadId(Integer id);
	
	interface CustomObjectModalidadId {
		Integer getId();
		String getCodigo();
		String getDescripcion();
		String getEstado();
		Integer getVersion();
	}
	
	@Query(value="SELECT m.id,m.codigo,m.descripcion,m.estado,m.version " + 
			"FROM Modalidad m " + 
			"WHERE m.estado = 'A' and m.codigo LIKE  (?1) ")
	List<CustomObjectModalidadCampo> buscarmodalidadCampo(String codigo);
	
	interface CustomObjectModalidadCampo {
		Integer getId();
		String getCodigo();
		String getDescripcion();
		String getEstado();
		Integer getVersion();
	}
}
