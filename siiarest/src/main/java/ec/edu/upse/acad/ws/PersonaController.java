package ec.edu.upse.acad.ws;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ec.edu.upse.acad.model.pojo.seguridad.Persona;
import ec.edu.upse.acad.model.pojo.seguridad.Usuario;
import ec.edu.upse.acad.model.repository.DepartamentosRepository;
import ec.edu.upse.acad.model.repository.ModulosRepository;
import ec.edu.upse.acad.model.repository.ModulosRolesRepository;
import ec.edu.upse.acad.model.repository.PersonasRepository;
import ec.edu.upse.acad.model.repository.UsuariosRepository;
import ec.edu.upse.acad.model.service.SecurityService;
import ec.edu.upse.acad.model.specification.PersonaSpecification;
import ec.edu.upse.util.db.FilterInformation;
import ec.edu.upse.util.db.PageInformation;
import ec.edu.upse.util.db.SearchCriteria;


@RestController
@RequestMapping("/api/persona")
@CrossOrigin
public class PersonaController {

	@Autowired private SecurityService securityService;
	@Autowired private PersonasRepository personaRepository;
	@Autowired private UsuariosRepository usuarioRepository;
	@Autowired private ModulosRepository moduloRepository;
	@Autowired private DepartamentosRepository departamentoRepository;
	@Autowired private ModulosRolesRepository moduloRolRepository;

	@RequestMapping(value="/buscarTodos", method=RequestMethod.GET)
	public ResponseEntity<?> buscarTodos() {
		return ResponseEntity.ok(personaRepository.findAll());
	}
	//Intenta paginar
	@RequestMapping(value="/buscarPaginado",params = { "page", "size" },  method=RequestMethod.GET)
	public ResponseEntity<?> buscarPaginado(@RequestParam("page") int page, 
			@RequestParam("size") int size) {
		Page<Persona> personas = personaRepository.findAll(PageRequest.of(page, size));
		return ResponseEntity.ok(personas);
	}

	//Intenta paginar ordenar
	@RequestMapping(value="/buscarPaginadoOrdenado",  params = { "page", "size", "direction", "sortfield" },  method=RequestMethod.GET)
	public ResponseEntity<?> buscarPaginadoOrdenado(@RequestParam("page") int page, @RequestParam("size") int size,
			@RequestParam("direction") Sort.Direction direction,@RequestParam("sortfield") String sortfield) {
		Page<Persona> personas = personaRepository.findAll(PageRequest.of(page, size, direction, sortfield));
		return ResponseEntity.ok(personas);
	}

	@RequestMapping(value="/buscarFiltradoPaginadoOrdenado", method = RequestMethod.POST, consumes = { "application/json" })
	public ResponseEntity<?> buscarFiltradoPaginadoOrdenado(@RequestHeader(value="Authorization") String Authorization,
			@RequestBody PageInformation pageInf) { 
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		//Defino variable Page de personas
		Page<Persona> personas;
		//criterios de consuta provinientes del cliente
		List<FilterInformation> filterInf = pageInf.getFilterInformation();

		if (filterInf.size() != 0) {
			List<SearchCriteria> params = new ArrayList<SearchCriteria>();
			for(int i = 0;i<filterInf.size();i++)
			{
				//construccion de criterios con los datos provinientes de la lista de filtrado
				params.add(new SearchCriteria(filterInf.get(i).getField(), ":", filterInf.get(i).getValue()));
			}
			//Concatena criterios de filtrado
			List<Specification<Persona>> specs = params.stream().map(PersonaSpecification::new).collect(Collectors.toList());
			Specification<Persona> result = specs.get(0);
			for (int i = 1; i < params.size(); i++) {
				result = params.get(i).getOperation().contains("0")
						? Specification.where(result)
								.or(specs.get(i))
								: Specification.where(result)
								.and(specs.get(i));
			}       

			//Construccion de filtrado con opciones de paginacion y ordenado
			personas = personaRepository.findAll(result,PageRequest.of(pageInf.getPage(), pageInf.getSize(),pageInf.getDirection(),pageInf.getSortfield()));
			System.err.println(personas);
		}else {
			//Contruccion de paginacion y ordenado
			personas = personaRepository.findAll(PageRequest.of(pageInf.getPage(), pageInf.getSize(),pageInf.getDirection(),pageInf.getSortfield()));
		}
		//Retorno de la consulta
		return ResponseEntity.ok(personas);
	}

	//Intenta filtrar paginar ordenar 
	@RequestMapping(value="/buscarFiltradoPaginadoOrdenadoGet",  
			params = {"page", "size", "direction", "sortfield" },  
			method=RequestMethod.GET)
	public ResponseEntity<?> buscarFiltradoPaginadoOrdenadoGet(@RequestParam("filterfield") String filterfield, 
			@RequestParam("filtervalue") String filtervalue, 
			@RequestParam("page") int page, 
			@RequestParam("size") int size,
			@RequestParam("direction") Sort.Direction direction,
			@RequestParam("sortfield") String sortfield
			) {
		/* Parametros provinientes
					   filtervalue: orrala
                       filtercondition:contains
                       filteroperador:0 para or o 1 and
                       filterdatafield: apellidos
		 */
		PersonaSpecification spec = new PersonaSpecification(new SearchCriteria(filterfield, ":", filtervalue));
		Page<Persona> personas = personaRepository.findAll(spec,PageRequest.of(page, size, direction, sortfield));
		return ResponseEntity.ok(personas);
	}

	@RequestMapping(value="/buscar/{id}", method=RequestMethod.GET)
	public ResponseEntity<?> buscar(@PathVariable("id") Integer id) {
		Persona persona;
		if (personaRepository.findById(id).isPresent()) {
			persona = personaRepository.findById(id).get();
			return ResponseEntity.ok(persona);
		}else {
			return ResponseEntity.notFound().build();
		}
	}

	@RequestMapping(value="/buscarPorCedula/{cedula}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarPorCedula(@PathVariable("cedula") String cedula) {
		return ResponseEntity.ok(personaRepository.findByIdentificacion(cedula));
	}

	@RequestMapping(value="/buscarPorNombre/{nombre}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarPorNombre(@PathVariable("nombre") String nombre) {
		return ResponseEntity.ok(personaRepository.buscarPorNombre("%" + nombre + "%"));
	}

	@RequestMapping(value="/grabar", method=RequestMethod.POST)
	public ResponseEntity<?> grabar(@RequestBody Persona persona) {
		Persona _persona = new Persona();
		if (persona.getId() != null) {
			_persona = personaRepository.findById(persona.getId()).get();
		}
		BeanUtils.copyProperties(persona, _persona, "usuario");
		personaRepository.save(_persona);
		return ResponseEntity.ok(_persona);
	}

	/*Metodo para consutar las personas que tienen usuario 
	 * con funcionalidad de filtrado y paginado
	 * Autor: Ing. Omar Orrala Palacios 
	 * Cargo: Director TIC
	 * */

	@RequestMapping(value="/buscarUserPaginadoOrdenado", method = RequestMethod.POST, 
			consumes = { "application/json" })
	public ResponseEntity<?> buscarUserPaginadoOrdenado(@RequestHeader(value="Authorization") String authorization,
			@RequestBody PageInformation pageInf) { 
		//Defino variable Page de personas
		Page<Persona> personas;
		//criterios de consuta provinientes del cliente
		List<FilterInformation> filterInf = pageInf.getFilterInformation();
		if (filterInf.size() != 0) {
			List<SearchCriteria> params = new ArrayList<SearchCriteria>();
			for(int i = 0;i<filterInf.size();i++)
			{
				//construccion de criterios con los datos provinientes de la lista de filtrado
				params.add(new SearchCriteria(filterInf.get(i).getField(), ":", filterInf.get(i).getValue()));

			}
			//Concatena criterios de filtrado
			List<Specification<Persona>> specs = params.stream().map(PersonaSpecification::new).collect(Collectors.toList());
			Specification<Persona> result = specs.get(0);
			for (int i = 1; i < params.size(); i++) {
				result = params.get(i).getOperation().contains("0")
						? Specification.where(result)
								.or(specs.get(i))
								: Specification.where(result)
								.and(specs.get(i));
			}       
			//agrega un criterio adicional si existe usuario
			//result.and(PersonasRepository.existeUsuario());
			//Construccion de filtrado de personas con usuario con opciones de paginacion y ordenado
			personas = personaRepository.findAll(Specification.where(PersonasRepository.existeUsuario()).and(result),PageRequest.of(pageInf.getPage(), pageInf.getSize(),pageInf.getDirection(),pageInf.getSortfield()));
		}else {
			//Contruccion de paginacion y ordenado de personas con usuario
			personas = personaRepository.findAll(Specification.where(PersonasRepository.existeUsuario()),PageRequest.of(pageInf.getPage(), pageInf.getSize(),pageInf.getDirection(),pageInf.getSortfield()));
		}
		//Retorno de la consulta
		return ResponseEntity.ok(personas);

	}
	//***************** CONTROLLER PARA USUARIOS ***************
	//buscar por Id
	@RequestMapping(value="/buscarUsuario/{id}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarUsuario(@PathVariable("id") Integer id) {
		Usuario usuario;
		if (usuarioRepository.findById(id).isPresent()) {
			usuario = usuarioRepository.findById(id).get();
			return ResponseEntity.ok(usuario);
		}else {
			return ResponseEntity.notFound().build();
		}
	}


	//Grabar Usuario
	@RequestMapping(value="/grabarUsuario", method=RequestMethod.POST)
	public ResponseEntity<?> grabarUsuario(@RequestBody Usuario usuario) {
		//se instancia un objeto encoder tipo BCryptPasswordEncoder de la clase que provee las funciones de encriptado
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		//Encripta la clave
		String clave =  encoder.encode(usuario.getClaveAuxiliar());
		//para actualizar la clave en el caso de que este en null o sea diferente de la almacenada
		if ((usuario.getClave() == null)||(!encoder.matches(usuario.getClaveAuxiliar(), usuario.getClave()))) {
			usuario.setClave(clave);
		}
		Usuario _usuario = new Usuario();
		if (usuario.getId() != null) {
			_usuario = usuarioRepository.findById(usuario.getId()).get();
		}
		BeanUtils.copyProperties(usuario, _usuario);
		usuarioRepository.save(_usuario);
		return ResponseEntity.ok(_usuario);
	}

	//lista departamentos
	@RequestMapping(value="/listaDepartamentos", method=RequestMethod.GET)
	public ResponseEntity<?> listaDepartamentos() {
		return ResponseEntity.ok(departamentoRepository.findAll());
	}

	//lista de modulos
	@RequestMapping(value="/listaModulos", method=RequestMethod.GET)
	public ResponseEntity<?> listaModulos() {
		return ResponseEntity.ok(moduloRepository.findAll());
	}

	//lista roles_modulos
	@RequestMapping(value="/listaRolesModulos", method=RequestMethod.GET)
	public ResponseEntity<?> listaRolesModulos() {
		return ResponseEntity.ok(moduloRolRepository.listaRolesModulos());
	}



}
