package ec.edu.upse.acad.model.repository.matricula;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.matricula.TipoMatricula;

@Repository
public interface TipoMatriculaRepository extends JpaRepository<TipoMatricula, Integer>{
	@Query(value="SELECT pa.id as idPeriodoActual,mg.id as idMatriculaGeneral, pa.codigo as codigo, "+
	" tmf.fechaHasta as fechaHasta,tm.descripcion as descripcionTipoOferta, tm.codigo as codigoTipoMat "+
	" FROM PeriodoAcademico pa " +
	" INNER JOIN MatriculaGeneral mg on mg.idPeriodoAcademico = pa.id " +
	" INNER JOIN TipoMatriculaFecha tmf on tmf.idMatriculaGeneral = mg.id " +
	" INNER JOIN TipoMatricula tm on tmf.idTipoMatricula = tm.id " +
	" WHERE pa.id = (select pa.id from PeriodoAcademico pa where cast(CURRENT_TIMESTAMP as date) >= pa.fechaDesde  " + 
	" and cast(CURRENT_TIMESTAMP as date)<=pa.fechaHasta) and " +
	" tmf.fechaHasta >= cast( CURRENT_TIMESTAMP as date) and tmf.fechaDesde <= cast( CURRENT_TIMESTAMP as date)")
	List<CustObjTipoMatricula>recuperarUltimoPeriodoMatricula();
	
	interface CustObjTipoMatricula{
		Integer getIdPeriodoActual();
		Integer getIdMatriculaGeneral();
		String getCodigo();
		//Boolean getMatriculaNivel();
		Date getFechaHasta();
		String getDescripcionTipoOferta();
		String getCodigoTipoMat();
	}

	
	@Query(value=" select pa.id as idPeriodoAcademico, pa.codigo as codigo from PeriodoAcademico pa where cast(CURRENT_TIMESTAMP as date) >= pa.fechaDesde  " + 
			"and cast(CURRENT_TIMESTAMP as date)<=pa.fechaHasta")
	List<CustObjPeriodoMatricula>recuperarLabelPeriodoMatricula();

	interface CustObjPeriodoMatricula{
		Integer getIdPeriodoAcademico();
		String getCodigo();
	}
	
	@Query(value=" select eo.id as idEstudianteOferta, eo.numeroMatricula as numeroMatricula from EstudianteOferta eo "+
			" where eo.id = (select max(eo1.id) from EstudianteOferta eo1) ")
	List<CustObjUltimoMatriculado>recuperarUltimoMatriculado();

	interface CustObjUltimoMatriculado{
		Integer getIdEstudianteOferta();
		String getNumeroMatricula();
	}
	
	@Query(value=" select ca.id as idCostoAsignatura, ca.codigo as codigo,ca.valor as valor from CostoAsignatura ca " + 
			"where ca.codigo in ('DMAEX','DMAES') ")
	List<CustObjValoresMatricula>recuperarValoresMatricula();

	interface CustObjValoresMatricula{
		Integer getIdCostoAsignatura();
		String getCodigo();
		Double getValor();
	}
}
