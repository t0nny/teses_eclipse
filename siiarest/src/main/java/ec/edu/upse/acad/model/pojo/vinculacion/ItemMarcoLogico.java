package ec.edu.upse.acad.model.pojo.vinculacion;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema = "vin", name = "item_marco_logico")
@NoArgsConstructor //un constructorsin argumentos
public class ItemMarcoLogico {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_item_marco_logico")
	@Getter	@Setter	private Integer id;
	
	@Column(name = "id_item_marco_logico_padre")
	@Getter	@Setter	private Integer idItemMarcoLogicoPadre;
	
	@Column(name = "id_tipo_objetivo_proyecto")
	@Getter	@Setter	private Integer idTipoObjetivoProyecto;
	
	/*
	 * @Column(name = "id_proyecto")
	 * 
	 * @Getter @Setter private Integer idProyecto;
	 */
	@Getter	@Setter	private String indicador;
	
	@Column(name = "descripcion_corta")
	@Getter	@Setter	private String descripcionCorta;
	
	@Getter	@Setter	private String descripcion;

	@Column(name = "tiempo_planeado")
	@Getter	@Setter	private Integer tiempoPlaneado;
	
	@Column(name = "meta_programada")
	@Getter	@Setter	private String metaProgramada;
	
	@Column(name = "linea_base")
	@Getter	@Setter	private String lineaBase;
	
	@Column(name = "usuario_ingreso_id")
	@Getter	@Setter	private String usuarioIngresoId;

	@Version
	@Getter	@Setter	private Integer version;

	@Getter	@Setter	private String estado;
	
	@ManyToOne
	@JoinColumn(name = "id_proyecto", nullable = false, updatable = false)
	@JsonBackReference
	@Getter	@Setter	private Proyecto proyecto;
	
	@OneToMany(mappedBy="itemMarcoLogico", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JsonManagedReference
	@Getter	@Setter	private List<ObjetivoActividad> objetivoActividad;
	
	@PrePersist
	void preInsert() {
		if (this.estado == null)
			this.estado = "A";
	}

}
