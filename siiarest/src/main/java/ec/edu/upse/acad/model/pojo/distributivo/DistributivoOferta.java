package ec.edu.upse.acad.model.pojo.distributivo;

import javax.persistence.*;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ec.edu.upse.acad.model.pojo.Oferta;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Entity
@Table(schema="aca", name="distributivo_oferta")
@Where(clause = "estado='A'")
@NoArgsConstructor

public class DistributivoOferta {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_distributivo_oferta")
	@Getter @Setter private Integer id;
	
	@Column(name="id_oferta")
	@Getter @Setter private Integer idOferta;
	
	@Column(name="id_distributivo_general_version")
	@Getter @Setter private Integer idDistributivoGeneralVersion;
	
	@Getter @Setter private String estado;
	
//	@Column(name="fecha_ingreso")
//	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private Integer usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;
	
	
	
	//RELACIONES
	
	//bi-directional many-to-one association to DistributivoGeneral
		@ManyToOne(fetch=FetchType.LAZY)
		@JoinColumn(name="id_distributivo_general_version", insertable=false, updatable = false)
		@JsonIgnore
		@Getter @Setter private DistributivoGeneralVersion distributivoGeneralVersion;


		
		@ManyToOne(fetch=FetchType.LAZY)
		@JoinColumn(name="id_oferta", insertable=false, updatable = false)
		@JsonIgnore
		@Getter @Setter private Oferta oferta;
		
	//bi-directional many-to-one association to Distributivo Oferta Version
	    @OneToMany(mappedBy="distributivoOferta", cascade=CascadeType.ALL)
	    @JsonIgnore
	    @Getter @Setter private List<DistributivoOfertaVersion> distributivoOfertaVersion;
	    

	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }

}