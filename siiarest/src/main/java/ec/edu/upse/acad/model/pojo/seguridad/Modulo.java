package ec.edu.upse.acad.model.pojo.seguridad;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="man", name="modulos")
@NoArgsConstructor
//@AdditionalCriteria("this.estado='AC'")

public class Modulo {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Getter @Setter private Integer id;
	
	@Getter @Setter private String codigo;
		
	@Getter @Setter private String nombre;
	
	@Getter @Setter private String descripcion;
	
	@Getter @Setter private String url;
	
	@Getter @Setter private String logo;
	
	@Getter @Setter private String img_out;
	
	@Getter @Setter private String img_in;	
	
	@Getter @Setter private String estado;
	
	@Getter @Setter private String ambiente;
	
	@Version
	@Getter @Setter private Integer version;
	
	@OneToMany(mappedBy="modulo", cascade=CascadeType.ALL)
	//@JsonIgnore
	@Getter @Setter private List<Opcion> opcion;
	
	@OneToMany(mappedBy="modulo", cascade=CascadeType.ALL)
	//@JsonIgnore
	@Getter @Setter private List<ModuloRol> moduloRol;
	
	@PrePersist
	void preInsert() {
	   if (this.estado == null)
	       this.estado = "AC";
	}
}
