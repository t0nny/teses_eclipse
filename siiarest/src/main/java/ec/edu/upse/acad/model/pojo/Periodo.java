package ec.edu.upse.acad.model.pojo;


import javax.persistence.*;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Date;

import java.util.List;

@Entity
@Table(schema="aca", name="periodo")
@NoArgsConstructor
public class Periodo {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_periodo")
	@Getter @Setter private Integer id;

	@Column(name="codigo")
	@Getter @Setter private String codigo;
	
	@Getter @Setter private String descripcion;

	@Getter @Setter private String estado;
	

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;
	
	@Column(name="fecha_desde")
	@Getter @Setter private Date fechaDesde;

	@Column(name="fecha_hasta")
	@Getter @Setter private Date fechaHasta;

	//RELACIONES
	//bi-directional many-to-one association to PeriodoAcademico
	@OneToMany(mappedBy="periodo", cascade=CascadeType.ALL)
	@Getter @Setter private List<PeriodoAcademico> periodoAcademico;

	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }

}