package ec.edu.upse.acad.model.pojo.vinculacion;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema = "vin", name = "proyecto_docente")

@NoArgsConstructor //un constructorsin argumentos
public class ProyectoDocente {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_proyecto_docente")
	@Getter	@Setter	private Integer id;

	@Column(name = "id_docente")
	@Getter	@Setter	private String idDocente;
	
	@Column(name = "id_tipo_usuario_proyecto")
	@Getter	@Setter	private String idTipoUsuarioProyecto;

	@Column(name = "usuario_ingreso_id")
	@Getter	@Setter	private String usuarioIngresoId;

	@Version
	@Getter	@Setter	private Integer version;

	@Getter	@Setter	private String estado;

	@ManyToOne
	@JoinColumn(name = "id_proyecto", nullable = false, updatable = false)
	@JsonBackReference
	@Getter	@Setter	private Proyecto proyecto;
	
	
	
	@PrePersist
	void preInsert() {
		if (this.estado == null)
			this.estado = "A";
	}


}
