package ec.edu.upse.acad.model.repository.calificaciones;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ec.edu.upse.acad.model.pojo.calificaciones.ActividadesPonderada;

@Repository
public interface ActividadesPonderadaRepository extends JpaRepository<ActividadesPonderada, Integer> {

}
