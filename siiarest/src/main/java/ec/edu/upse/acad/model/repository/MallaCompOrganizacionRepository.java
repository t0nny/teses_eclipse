package ec.edu.upse.acad.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.ReglamentoCompOrganizacion;

@Repository
public interface MallaCompOrganizacionRepository extends JpaRepository<ReglamentoCompOrganizacion, Integer>{
//	@Query(value="SELECT mg.id,tco.descripcion as TIPO,co.descripcion AS NOMBRE from MallaGeneral as mg " 
//			+ "INNER JOIN MallaCompOrganizacion mco on mco.idMallaGeneral=mg.id " 
//			+ "INNER JOIN ComponenteOrganizacion co on mco.idCompOrganizacion=co.id " 
//			+ "INNER JOIN TipoCompOrganizacion tco on co.idTipoCompOrganizacion=tco.id ")
//	 List<CustomObject> listaComponentesOrganizacion();
//	
//	
//	@Query(value="select mg.id as codigo,tco.descripcion as tipo,co.descripcion AS NOMBRE from MallaGeneral as mg " 
//			+ "inner join MallaCompOrganizacion mco on mco.idMallaGeneral=mg.id " 
//			+ "inner join ComponenteOrganizacion co on mco.idCompOrganizacion=co.id " 
//			+ "inner join TipoCompOrganizacion tco on co.idTipoCompOrganizacion=tco.id "
//			+ "WHERE mg.id=?1 ")
//	 List<CustomObject> listaComponentesOrganizacion(Integer id);
//	
//	interface CustomObject { 
//	    Integer getCodigo(); 
//	    String getTipo(); 
//	    String getNombre(); 
//	     
//	  
//	} 
	

}
