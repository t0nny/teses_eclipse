package ec.edu.upse.acad.model.pojo.planificacion_docente;


import javax.persistence.*;

import org.hibernate.annotations.Where;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.util.List;


/**
 * The persistent class for the estrategia_evaluacion database table.
 * 
 */
@Entity
@Table(schema="aca", name="estrategia_evaluacion")
@Where(clause = "estado='A'")
@NoArgsConstructor
public class EstrategiaEvaluacion  {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_estrategia_evaluacion")
	@Getter @Setter private Integer id;

	@Getter @Setter private String codigo;

	@Getter @Setter private String descripcion;

	@Getter @Setter private String estado;

//	@Column(name="fecha_ingreso")
//	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;

	//bi-directional many-to-one association to PlanEstrategia
	@OneToMany(mappedBy="estrategiaEvaluacion", cascade=CascadeType.ALL)
	@Getter @Setter private List<PlanEstrategia> planEstrategias;

	@PrePersist
	void preInsert() {
	   if (this.estado == null)
	       this.estado = "A";
	}
}