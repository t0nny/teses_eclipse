package ec.edu.upse.acad.model.pojo;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ec.edu.upse.acad.model.pojo.seguridad.Persona;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;


@Entity
@Table(schema="proy", name="miembros")
@NoArgsConstructor
public class Miembro {

	@Id
	@Getter @Setter private Integer id;

	@Getter @Setter private BigDecimal dedicacion;

	@Column(name="departamento_id")
	@Getter @Setter private Integer departamentoId;

	@Getter @Setter private String desde;

	@Getter @Setter private String estado;

	@Column(name="fecha_ing")
	@Getter @Setter private String fechaIng;

	@Column(name="fecha_mod")
	@Getter @Setter private String fechaMod;

	@Column(name="grupo_id")
	@Getter @Setter private Integer grupoId;

	@Getter @Setter private String hasta;
	//fk
	@Column(name="persona_id")
	@Getter @Setter private Integer personaId;
	
	@Column(name="rol_id")
	@Getter @Setter private int rolId;

	@Column(name="usuario_ing")
	@Getter @Setter private String usuarioIng;

	@Column(name="usuario_mod")
	@Getter @Setter private String usuarioMod;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="persona_id", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Persona persona;
	
	//bi-directional many-to-one association to Portafolio
	@ManyToOne
	@JoinColumn(name="portafolio_id")
	@Getter @Setter private Portafolio portafolio;
	
	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "AC";
	 }

}