package ec.edu.upse.acad.model.pojo;

import javax.persistence.*;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ec.edu.upse.acad.model.pojo.seguridad.Departamento;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.sql.Timestamp;
import java.util.List;


@Entity
@Table(schema="aca", name="departamento_oferta")
@Where(clause = "estado='A'")
@NoArgsConstructor
public class DepartamentoOferta {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_departamento_oferta")
	@Getter @Setter private Integer id;
	
	@Column(name="id_oferta")
	@Getter @Setter private Integer idOferta;
	
	@Column(name="id_departamento")
	@Getter @Setter private Integer idDepartamento;
	
	@Getter @Setter  private String estado;


	@Column(name="usuario_ingreso_id")
	@Getter @Setter  private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;
	
	
		
    //RELACIONES
	//bi-directional many-to-one association to Departamento
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_departamento", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter  private Departamento departamento;

	//bi-directional many-to-one association to Oferta
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_oferta", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter  private Oferta oferta;
	
	//bi-directional many-to-one association to Malla
	@OneToMany(mappedBy="departamentoOferta", cascade=CascadeType.ALL)
	@Getter @Setter private List<Malla> malla;

	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }
}
