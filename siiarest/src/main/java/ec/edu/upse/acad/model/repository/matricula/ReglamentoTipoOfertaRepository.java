package ec.edu.upse.acad.model.repository.matricula;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.matricula.ReglamentoTipoOferta;

@Repository
public interface ReglamentoTipoOfertaRepository extends JpaRepository<ReglamentoTipoOferta, Integer>{

}
