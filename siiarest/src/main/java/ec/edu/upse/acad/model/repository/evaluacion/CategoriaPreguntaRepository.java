package ec.edu.upse.acad.model.repository.evaluacion;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.evaluacion.CategoriaPregunta;
import ec.edu.upse.acad.model.repository.evaluacion.EvaluacionDocenteGeneralRepository.CO_Reglamentos;

@Repository
public interface CategoriaPreguntaRepository extends JpaRepository<CategoriaPregunta, Integer>{
	//consulta para buscar los tipos preguntas
	@Query(value="SELECT cp "
			+ "FROM CategoriaPregunta cp "
			+ "WHERE cp.estado = 'A' ")
	List<CategoriaPregunta> listarCategoriaPregunta();
	
/*	@Query(value = "select r.id as idReglamento,r.nombre as nombreReglamento, r.estado as estado "
			+ "from Reglamento r "
			+ "where r.estado = 'A'")
	List<CO_Reglamentos> getReglamentos();
	interface CO_Reglamentos{
		Integer getIdReglamento();
		String getNombreReglamento();
		String getEstado();
	}*/
}
