package ec.edu.upse.acad.model.pojo.vinculacion;

import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema = "vin", name = "cabecera_matriz")

@NoArgsConstructor //un constructorsin argumentos
public class CabeceraMatriz {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_cabecera_matriz")
	@Getter	@Setter	private Integer id;

	@Column(name = "id_proyecto_version")
	@Getter	@Setter	private Integer idProyectoVersion;
		
	@Column(name = "fecha_desde")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", locale = "es_EC", timezone = "America/Guayaquil")
	@Getter	@Setter	private Date fechaDesde;

	@Column(name = "fecha_hasta")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", locale = "es_EC", timezone = "America/Guayaquil")
	@Getter	@Setter	private Date fechaHasta;

	@Column(name = "estado_matriz")
	@Getter	@Setter	private String estadoMatriz;

	@Column(name = "usuario_ingreso_id")
	@Getter	@Setter	private String usuarioIngresoId;

	@Version
	@Getter	@Setter	private Integer version;

	@Getter	@Setter	private String estado;

	
	// bi-directional many-to-one association to versionProyecto
		@OneToMany(mappedBy = "cabeceraMatriz", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
		@JsonManagedReference
		@Getter	@Setter	private List<DetalleMatriz> detalleMatriz;
		
		@PrePersist
		void preInsert() {
			if (this.estado == null)
				this.estado = "A";
		}
}
