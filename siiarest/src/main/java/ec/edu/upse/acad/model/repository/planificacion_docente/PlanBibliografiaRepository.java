package ec.edu.upse.acad.model.repository.planificacion_docente;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.planificacion_docente.PlanBibliografia;

@Repository
public interface PlanBibliografiaRepository   extends JpaRepository<PlanBibliografia, Long> {

}
