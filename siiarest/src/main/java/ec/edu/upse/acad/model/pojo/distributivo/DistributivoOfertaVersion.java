package ec.edu.upse.acad.model.pojo.distributivo;





import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import javax.persistence.Version;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="aca", name="distributivo_oferta_version")
@Where(clause = "estado='A' or estado='D' or estado='V' or estado='P'  or estado='R'")
@NoArgsConstructor

public class DistributivoOfertaVersion {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_distributivo_oferta_version")
	@Getter @Setter private Integer id;
	
	@Column(name="id_distributivo_oferta")
	@Getter @Setter private Integer idDistributivoOferta;
	
	@Getter @Setter  private String descripcion;
	
	@Column(name="version_distributivo_oferta")
	@Getter @Setter private Integer versionDistributivoOferta;
	
	
	@Column(name="fecha_desde")
	//@Temporal(TemporalType.DATE)
	@Getter @Setter  private Date fechaDesde;
	
	
	@Column(name="fecha_hasta")
	
	@Getter @Setter  private Date fechaHasta;
	
    @Getter @Setter private String estado;
	

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private Integer usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;
	
	
	//RELACIONES
	
	//bi-directional many-to-one association to DistributivoOferta
			@ManyToOne(fetch=FetchType.LAZY)
			@JoinColumn(name="id_distributivo_oferta", insertable=false, updatable = false)
			 @JsonIgnore
			@Getter @Setter private DistributivoOferta distributivoOferta;
	
	//bi-directional many-to-one association to Distributivo Revision
		    @OneToMany(mappedBy="distributivoOfertaVersion", cascade=CascadeType.ALL)
		    @Getter @Setter private List<DistributivoRevision> distributivoRevision;
	
    //bi-directional many-to-one association to Distributivo Docente
		    @OneToMany(mappedBy="distributivoOfertaVersion", cascade=CascadeType.ALL)
		    @Getter @Setter private List<DistributivoDocente> distributivoDocentes;
	@PrePersist
	void preInsert() {
	   if (this.estado == null)
	       this.estado = "A";
 }

}
