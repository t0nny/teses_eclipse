package ec.edu.upse.acad.model.pojo;


import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Table(schema="aca", name="oferta_docente")
@NoArgsConstructor
public class OfertaDocente {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_oferta_docente")
	@Getter @Setter private Integer id;
	
	@Column(name="id_docente")
	@Getter @Setter private Integer idDocente;
	
	@Column(name="id_oferta")
	@Getter @Setter private Integer idOferta;

	@Column(name="id_periodo_academico")
	@Getter @Setter private Integer idPeriodoAcademico;

	@Getter @Setter private String estado;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private Integer usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;
	
	//RELACIONES
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_oferta" , insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Oferta oferta;
	
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_docente" , insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Docente docente;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_periodo_academico" , insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private PeriodoAcademico periodoAcademico;
	
	
	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }
	
}