package ec.edu.upse.acad.model.repository.evaluacion;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.evaluacion.TipoRecursoApelacion;

@Repository
public interface TipoRecursoApelacionRepository extends JpaRepository<TipoRecursoApelacion, Integer> {
	@Query(value="SELECT tra "
			+ "FROM TipoRecursoApelacion tra "
			+ "WHERE tra.estado = 'A' ")
	List<TipoRecursoApelacion> listarTipoRecursoApelacion();
}
