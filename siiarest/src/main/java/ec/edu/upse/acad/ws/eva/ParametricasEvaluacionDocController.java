package ec.edu.upse.acad.ws.eva;

import java.util.ArrayList;
import java.util.List;

import javax.json.Json;

import org.jfree.util.Log;
import org.json.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.upse.acad.model.pojo.evaluacion.CalificacionEscala;
import ec.edu.upse.acad.model.pojo.evaluacion.CategoriaPregunta;
import ec.edu.upse.acad.model.pojo.evaluacion.Instrumento;
import ec.edu.upse.acad.model.pojo.evaluacion.OpcionRespuesta;
import ec.edu.upse.acad.model.pojo.evaluacion.ReglaCalificacion;
import ec.edu.upse.acad.model.pojo.evaluacion.TipoPregunta;
import ec.edu.upse.acad.model.pojo.evaluacion.TipoRecursoApelacion;
import ec.edu.upse.acad.model.pojo.reglamento.Reglamento;
import ec.edu.upse.acad.model.repository.distributivo.ReglamentoRepository;
import ec.edu.upse.acad.model.repository.evaluacion.CalificacionEscalaRepository;
import ec.edu.upse.acad.model.repository.evaluacion.CategoriaPreguntaRepository;
import ec.edu.upse.acad.model.repository.evaluacion.InstrumentoRepository;
import ec.edu.upse.acad.model.repository.evaluacion.OpcionRespuestaRepository;
import ec.edu.upse.acad.model.repository.evaluacion.ReglaCalificacionRepository;
import ec.edu.upse.acad.model.repository.evaluacion.TipoPreguntaRepository;
import ec.edu.upse.acad.model.repository.evaluacion.TipoRecursoApelacionRepository;
import ec.edu.upse.acad.model.service.SecurityService;
import ec.edu.upse.acad.model.service.eva.EvaParametricasService;
import ec.edu.upse.acad.util.RestUtil;
import lombok.Getter;
import lombok.Setter;

@RestController
@RequestMapping("/api/parametricasEvaluacion")
@CrossOrigin
public class ParametricasEvaluacionDocController {
	@Autowired private TipoPreguntaRepository tipoPreguntaRepository; 
	@Autowired private CategoriaPreguntaRepository categoriaPreguntaRepository;
	@Autowired private InstrumentoRepository instrumentoRepository;
	@Autowired private TipoRecursoApelacionRepository tipoRecursoApelacionRepository;
	@Autowired private CalificacionEscalaRepository calificacionEscalaRepository;
	@Autowired private ReglaCalificacionRepository reglaCalificacionRepository;
	@Autowired private SecurityService securityService;
	@Autowired private EvaParametricasService evaParametricasService;
	@Autowired private ReglamentoRepository reglamentoRepository;
	@Autowired private OpcionRespuestaRepository opcionRespuestaRepository;
	
	@RequestMapping(value="/listarCalificacionEscala", method=RequestMethod.GET)
	public ResponseEntity<?> listarCalificacionEscala(@RequestHeader(value="Authorization") String authorization) {		
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(calificacionEscalaRepository.listarCalificacionEscala());
		
	}
	
	@RequestMapping(value="/listarReglaCalificacion", method=RequestMethod.GET)
	public ResponseEntity<?> listarReglaCalificacion(@RequestHeader(value="Authorization") String authorization) {		
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(reglaCalificacionRepository.listarReglaCalificacion());
		
	}
		
	@RequestMapping(value="/listarReglaCalificacionConId/{id}", method=RequestMethod.GET)
	public ResponseEntity<?> listarReglaCalificacionConId(@RequestHeader(value="Authorization") String authorization, @PathVariable Integer id) {		
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		} 
		return ResponseEntity.ok(reglaCalificacionRepository.listarReglaCalificacionEscala(id));
		
	}
	
	@RequestMapping(value="/listarCategoriaPregunta", method=RequestMethod.GET)
	public ResponseEntity<?> listarCategoria(@RequestHeader(value="Authorization") String authorization) {		
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(categoriaPreguntaRepository.listarCategoriaPregunta());
		
	}	
	
	/*@RequestMapping(value="/listarInstrumento", method=RequestMethod.GET)
	public ResponseEntity<?> listarInstrumento(@RequestHeader(value="Authorization") String authorization) {		
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(instrumentoRepository.listarInstrumento());
		
	}*/
	
	@RequestMapping(value="/listarInstrumento", method=RequestMethod.GET)
	public ResponseEntity<?> listarInstrumento(@RequestHeader(value="Authorization") String authorization) {		
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(instrumentoRepository.listarInstrumento());
		
	}
	
	@RequestMapping(value="/listarTipoPregunta", method=RequestMethod.GET)
	public ResponseEntity<?> listarTipoPregunta(@RequestHeader(value="Authorization") String authorization) {		
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(tipoPreguntaRepository.listarTipoPregunta());
		
	}
	
	@RequestMapping(value="/listarTipoRecursoApelacion", method=RequestMethod.GET)
	public ResponseEntity<?> listarTipoRecursoApelacion(@RequestHeader(value="Authorization") String authorization) {		
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(tipoRecursoApelacionRepository.listarTipoRecursoApelacion());
		
	}
	
	// servicio que devuelve el jason completo CalificacionEscala
	@RequestMapping(value = "/buscarCalificacionEscala/{idCalificacionEscala}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarCalificacionEscala(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable("idCalificacionEscala") Integer idCalificacionEscala) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		CalificacionEscala _calificacionEscala;
		if (calificacionEscalaRepository.findById(idCalificacionEscala).isPresent()) {
			_calificacionEscala = calificacionEscalaRepository.findById(idCalificacionEscala).get();
			CalificacionEscala calificacionEscala = new CalificacionEscala();
			BeanUtils.copyProperties(_calificacionEscala, calificacionEscala, "reglaCalificaciones");
			return ResponseEntity.ok(calificacionEscala);
		} else {
			return ResponseEntity.notFound().build();
		}
	}		
	
	// servicio que devuelve el jason completo ReglaCalificacionEscala
	@RequestMapping(value = "/buscarReglaCalificacionEscala/{idReglaCalificacionEscala}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarReglaCalificacionEscala(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable("idReglaCalificacionEscala") Integer idReglaCalificacionEscala) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(reglaCalificacionRepository.findByIdRegCalifEsc(idReglaCalificacionEscala));
		
		/*ReglaCalificacion _reglaCalificacion;
		if (reglaCalificacionRepository.findById(idReglaCalificacionEscala).isPresent()) {
			_reglaCalificacion = reglaCalificacionRepository.findById(idReglaCalificacionEscala).get();
			ReglaCalificacion reglaCalificacion = new ReglaCalificacion();
			BeanUtils.copyProperties(_reglaCalificacion, reglaCalificacion, "reglaCalificacionEva", "reglaCalificaciones");
			return ResponseEntity.ok(reglaCalificacion);
		} else {
			return ResponseEntity.notFound().build();
		}*/
	}
	
	// servicio que devuelve el jason completo CategoriaPregunta
	@RequestMapping(value = "/buscarCategoriaPregunta/{idCategoriaPregunta}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarCategoriaPregunta(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable("idCategoriaPregunta") Integer idCategoriaPregunta) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		CategoriaPregunta _categoriaPregunta;
		if (categoriaPreguntaRepository.findById(idCategoriaPregunta).isPresent()) {
			_categoriaPregunta = categoriaPreguntaRepository.findById(idCategoriaPregunta).get();
			CategoriaPregunta categoriaPregunta = new CategoriaPregunta();
			BeanUtils.copyProperties(_categoriaPregunta, categoriaPregunta, "preguntas");
			return ResponseEntity.ok(categoriaPregunta);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	// servicio que devuelve el jason completo Instrumento
	/*@RequestMapping(value = "/buscarFuncionEvaluacion/{idFuncionEvaluacion}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarFuncionEvaluacion(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable("idFuncionEvaluacion") Integer idFuncionEvaluacion) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		FuncionEvaluacion _funcionEvaluacion;
		if (funcionEvaluacionRepository.findById(idFuncionEvaluacion).isPresent()) {
			_funcionEvaluacion = funcionEvaluacionRepository.findById(idFuncionEvaluacion).get();
			FuncionEvaluacion funcionEvaluacion = new FuncionEvaluacion();
			BeanUtils.copyProperties(_funcionEvaluacion, funcionEvaluacion, "componenteFuncionDedicacions");
			return ResponseEntity.ok(funcionEvaluacion);
		} else {
			return ResponseEntity.notFound().build();
		}
	}*/ 
	 
	@RequestMapping(value = "/buscarInstrumento/{idInstrumento}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarInstrumento(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable("idInstrumento") Integer idInstrumento) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		Instrumento _instrumento;
		if (instrumentoRepository.findById(idInstrumento).isPresent()) {
			_instrumento = instrumentoRepository.findById(idInstrumento).get();
			Instrumento instrumento = new Instrumento();
			BeanUtils.copyProperties(_instrumento, instrumento, "instrumentoPregunta", "perTipoEvalInstrumento");
			return ResponseEntity.ok(instrumento);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	// servicio que devuelve el jason completo TipoPregunta
	@RequestMapping(value = "/buscarTipoPregunta/{idTipoPregunta}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarTipoPregunta(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable("idTipoPregunta") Integer idTipoPregunta) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		TipoPregunta _tipoPregunta;
		if (tipoPreguntaRepository.findById(idTipoPregunta).isPresent()) {
			_tipoPregunta = tipoPreguntaRepository.findById(idTipoPregunta).get();
			TipoPregunta tipoPregunta = new TipoPregunta();
			BeanUtils.copyProperties(_tipoPregunta, tipoPregunta, "preguntas");
			return ResponseEntity.ok(tipoPregunta);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
		
	// servicio que devuelve el jason completo TipoPregunta
	@RequestMapping(value = "/buscarTipoRecursoApelacion/{idTipoRecursoApelacion}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarTipoRecursoApelacion(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable("idTipoRecursoApelacion") Integer idTipoRecursoApelacion) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		TipoRecursoApelacion _tipoRecursoApelacion;
		if (tipoRecursoApelacionRepository.findById(idTipoRecursoApelacion).isPresent()) {
			_tipoRecursoApelacion = tipoRecursoApelacionRepository.findById(idTipoRecursoApelacion).get();
			TipoRecursoApelacion tipoRecursoApelacion = new TipoRecursoApelacion();
			BeanUtils.copyProperties(_tipoRecursoApelacion, tipoRecursoApelacion, "recursoApelaciones");
			return ResponseEntity.ok(tipoRecursoApelacion);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	public static final String ALREADY_REPORTED = "ERROR! base de datos";
	
//	// servicio que graba un CategoriaPregunta
//	@RequestMapping(value="/grabarReglaCalificacionEscala", method=RequestMethod.POST)
//	public ResponseEntity<?> grabarReglaCalificacionEscala(@RequestHeader(value="Authorization") 
//														   String Authorization, 
//														   @RequestBody  Json json) {
//		try {
//			if (!securityService.isTokenValido(Authorization)) {
//				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
//			}
//			
//			Log.debug(json);
//			JSONObject jsonObject = new JSONObject (json); 
//			Log.debug(jsonObject);
//			Log.info("mensaje de prueba "+jsonObject.getString("descripcion"));
//			//JSONParser parser = new JSONParser();
//			//JSONObject json = (JSONObject) parser.parse(stringToParse);
//			 
//			ReglaCalificacion _reglaCalificacion = new ReglaCalificacion();
//			if (jsonObject.getInt("id") != 0) {
//				_reglaCalificacion = reglaCalificacionRepository.findById(jsonObject.getInt("id")).get();
//				_reglaCalificacion.getCalificacionEscala().setDescripcion(jsonObject.getString("descripcion"));
//				_reglaCalificacion.getReglamento().setId(jsonObject.getInt("idReglamento"));
//				_reglaCalificacion.setValorMaximo(jsonObject.getInt("valorMaximo"));
//				_reglaCalificacion.setValorMinimo(jsonObject.getInt("valorMinimo"));
//			}else {
//			 
//			}
//			//BeanUtils.copyProperties(reglaCalificacion, _reglaCalificacion);
//			reglaCalificacionRepository.save(_reglaCalificacion);
//			return ResponseEntity.ok(_reglaCalificacion);
//		} catch (Exception e) {
//			e.printStackTrace();
//			return RestUtil.preconditionFailedError(ALREADY_REPORTED);			
//		}		
//	} 
	
	// servicio que graba un ReglaCalificacion
		@RequestMapping(value="/grabarReglaCalificacionEscala", method=RequestMethod.POST)
		public ResponseEntity<?> grabarReglaCalificacionEscala(@RequestHeader(value="Authorization") 
															   String Authorization, 
															   @RequestBody ReglaCalificacion reglaCalificacion) {
			try {
				if (!securityService.isTokenValido(Authorization)) {
					return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
				}
				Log.debug(reglaCalificacion.getCalificacionEscala().getDescripcion());
				 
				ReglaCalificacion _reglaCalificacion = new ReglaCalificacion();
				if (reglaCalificacion.getId() != null) {
					_reglaCalificacion = reglaCalificacionRepository.findById(reglaCalificacion.getId()).get();
					_reglaCalificacion.getCalificacionEscala().setDescripcion(reglaCalificacion.getCalificacionEscala().getDescripcion());
					_reglaCalificacion.getReglamento().setId(reglaCalificacion.getReglamento().getId());
					_reglaCalificacion.setValorMaximo(reglaCalificacion.getValorMaximo());
					_reglaCalificacion.setValorMinimo(reglaCalificacion.getValorMinimo());
					reglaCalificacionRepository.save(_reglaCalificacion);
					return ResponseEntity.ok(_reglaCalificacion);
				}else {
					_reglaCalificacion.setValorMaximo(reglaCalificacion.getValorMaximo());
					_reglaCalificacion.setValorMinimo(reglaCalificacion.getValorMinimo());
					
					Reglamento reglamento = reglamentoRepository.findById(reglaCalificacion.getReglamento().getId()).get();
					_reglaCalificacion.setReglamento(reglamento);
					
					CalificacionEscala calificacionEscala = new CalificacionEscala();
					calificacionEscala.setDescripcion(reglaCalificacion.getCalificacionEscala().getDescripcion());
					calificacionEscala.setUsuarioIngresoId(reglaCalificacion.getUsuarioIngresoId());
					List<ReglaCalificacion> listaCalificacion = new ArrayList<>();
					
			
					_reglaCalificacion.setCalificacionEscala(calificacionEscala);
					_reglaCalificacion.setUsuarioIngresoId(reglaCalificacion.getUsuarioIngresoId());
					listaCalificacion.add(_reglaCalificacion);
					calificacionEscala.setReglaCalificaciones(listaCalificacion);
					calificacionEscalaRepository.save(calificacionEscala);
					return ResponseEntity.ok(calificacionEscala);
				}
				//BeanUtils.copyProperties(reglaCalificacion, _reglaCalificacion);
				
			} catch (Exception e) {
				e.printStackTrace();
				return RestUtil.preconditionFailedError(ALREADY_REPORTED);			
			}		
		} 
			
/*	// servicio que graba un CategoriaPregunta
	@RequestMapping(value="/grabarCalificacionEscala", method=RequestMethod.POST)
	public ResponseEntity<?> grabarCalificacionEscala(@RequestHeader(value="Authorization") String Authorization, @RequestBody CalificacionEscala calificacionEscala) {
		try {
			if (!securityService.isTokenValido(Authorization)) {
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
			}
			CalificacionEscala _calificacionEscala = new CalificacionEscala();
			if (calificacionEscala.getId() != null) {
				_calificacionEscala = calificacionEscalaRepository.findById(calificacionEscala.getId()).get();
			}
			BeanUtils.copyProperties(calificacionEscala, _calificacionEscala);
			calificacionEscalaRepository.save(_calificacionEscala);
			return ResponseEntity.ok(_calificacionEscala);
		} catch (Exception e) {
			e.printStackTrace();
			return RestUtil.preconditionFailedError(ALREADY_REPORTED);
			
		}
		
	}*/
		
	// servicio que graba un CategoriaPregunta
	@RequestMapping(value="/grabarCategoriaPregunta", method=RequestMethod.POST)
	public ResponseEntity<?> grabarCategoriaPregunta(@RequestHeader(value="Authorization") String Authorization, @RequestBody CategoriaPregunta categoriaPregunta) {
		try {
			if (!securityService.isTokenValido(Authorization)) {
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
			}
			CategoriaPregunta _categoriaPregunta = new CategoriaPregunta();
			if (categoriaPregunta.getId() != null) {
				_categoriaPregunta = categoriaPreguntaRepository.findById(categoriaPregunta.getId()).get();
			}
			BeanUtils.copyProperties(categoriaPregunta, _categoriaPregunta);
			categoriaPreguntaRepository.save(_categoriaPregunta);
			return ResponseEntity.ok(_categoriaPregunta);
		} catch (Exception e) {
			e.printStackTrace();
			return RestUtil.preconditionFailedError(ALREADY_REPORTED);
		}
		
	} 
	
	/*// servicio que graba una FuncionEvaluacion
	@RequestMapping(value="/grabarFuncionEvaluacion", method=RequestMethod.POST)
	public ResponseEntity<?> grabarFuncionEvaluacion(@RequestHeader(value="Authorization") String Authorization, @RequestBody FuncionEvaluacion funcionEvaluacion) {
		try {
			if (!securityService.isTokenValido(Authorization)) {
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
			}
			FuncionEvaluacion _funcionEvaluacion = new FuncionEvaluacion();
			if (funcionEvaluacion.getId() != null) {
				_funcionEvaluacion = funcionEvaluacionRepository.findById(funcionEvaluacion.getId()).get();
			}
			BeanUtils.copyProperties(funcionEvaluacion, _funcionEvaluacion);
			funcionEvaluacionRepository.save(_funcionEvaluacion);
			return ResponseEntity.ok(_funcionEvaluacion);
		} catch (Exception e) {
			e.printStackTrace();
			return RestUtil.preconditionFailedError(ALREADY_REPORTED);
		}
		
	}*/
	// servicio que graba una FuncionEvaluacion
	@RequestMapping(value="/grabarInstrumento", method=RequestMethod.POST)
	public ResponseEntity<?> grabarInstrumento(@RequestHeader(value="Authorization") String Authorization, @RequestBody Instrumento instrumento) {
		try {
			if (!securityService.isTokenValido(Authorization)) {
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
			}
			Instrumento _instrumento = new Instrumento();
			if (instrumento.getId() != null) {
				_instrumento = instrumentoRepository.findById(instrumento.getId()).get();
			}
			BeanUtils.copyProperties(instrumento, _instrumento);
			instrumentoRepository.save(_instrumento);
			return ResponseEntity.ok(_instrumento);
		} catch (Exception e) {
			e.printStackTrace();
			return RestUtil.preconditionFailedError(ALREADY_REPORTED);
		}
		
	}
	
	// servicio que graba un TipoPregunta
	@RequestMapping(value="/grabarTipoPregunta", method=RequestMethod.POST)
	public ResponseEntity<?> grabarTipoPregunta(@RequestHeader(value="Authorization") String Authorization, @RequestBody TipoPregunta tipoPregunta) {
		try {
			if (!securityService.isTokenValido(Authorization)) {
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
			}
			TipoPregunta _tipoPregunta = new TipoPregunta();
			if (tipoPregunta.getId() != null) {
				_tipoPregunta = tipoPreguntaRepository.findById(tipoPregunta.getId()).get();
			}
			BeanUtils.copyProperties(tipoPregunta, _tipoPregunta);
			tipoPreguntaRepository.save(_tipoPregunta);
			return ResponseEntity.ok(_tipoPregunta);
		} catch (Exception e) {
			e.printStackTrace();
			return RestUtil.preconditionFailedError(ALREADY_REPORTED);
		}
		
	}
	
	// servicio que graba un TipoRecursoApelacion
	@RequestMapping(value="/grabarTipoRecursoApelacion", method=RequestMethod.POST)
	public ResponseEntity<?> grabarTipoRecursoApelacion(@RequestHeader(value="Authorization") String Authorization, @RequestBody TipoRecursoApelacion tipoRecursoApelacion) {
		try {
			if (!securityService.isTokenValido(Authorization)) {
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
			}
			TipoRecursoApelacion _tipoRecursoApelacion = new TipoRecursoApelacion();
			if (tipoRecursoApelacion.getId() != null) {
				_tipoRecursoApelacion = tipoRecursoApelacionRepository.findById(tipoRecursoApelacion.getId()).get();
			}
			BeanUtils.copyProperties(tipoRecursoApelacion, _tipoRecursoApelacion);
			tipoRecursoApelacionRepository.save(_tipoRecursoApelacion);
			return ResponseEntity.ok(_tipoRecursoApelacion);
		} catch (Exception e) {
			e.printStackTrace();
			return RestUtil.preconditionFailedError(ALREADY_REPORTED);
		}
		
	}
	
	
	//servicio para eliminar una ReglaCalificacionEscala
	@RequestMapping(value="/eliminarReglaCalificacionEscala/{idReglaCalificacionEscala}", method=RequestMethod.DELETE)
	public ResponseEntity<?> eliminarReglaCalificacionEscala(@RequestHeader(value="Authorization") String Authorization, 
		@PathVariable("idReglaCalificacionEscala") Integer idReglaCalificacionEscala) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		evaParametricasService.eliminaReglaCalificacionEscala(idReglaCalificacionEscala);
		return ResponseEntity.ok().build();
	}
	
	//servicio para eliminar una CalificacionEscala
	@RequestMapping(value="/eliminarCalificacionEscala/{idCalificacionEscala}", method=RequestMethod.DELETE)
	public ResponseEntity<?> eliminarCalificacionEscala(@RequestHeader(value="Authorization") String Authorization, 
			@PathVariable("idCalificacionEscala") Integer idCalificacionEscala) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		evaParametricasService.eliminaCalificacionEscala(idCalificacionEscala);
		return ResponseEntity.ok().build();
	}

	//servicio para borrar una CategoriaPregunta
	@RequestMapping(value="/eliminarCategoriaPregunta/{idCategoriaPregunta}", method=RequestMethod.DELETE)
	public ResponseEntity<?> eliminarCategoriaPregunta(@RequestHeader(value="Authorization") String Authorization, 
			@PathVariable("idCategoriaPregunta") Integer idCategoriaPregunta) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		evaParametricasService.eliminaCategoriaPregunta(idCategoriaPregunta);
		return ResponseEntity.ok().build();
	}

	//servicio para borrar una FuncionEvaluacion
	@RequestMapping(value="/eliminarInstrumento/{idInstrumento}", method=RequestMethod.DELETE)
	public ResponseEntity<?> eliminarInstrumento(@RequestHeader(value="Authorization") String Authorization, 
			@PathVariable("idInstrumento") Integer idInstrumento) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		evaParametricasService.eliminaInstrumento(idInstrumento);
		return ResponseEntity.ok().build();
	}

	//servicio para borrar un TipoPregunta
	@RequestMapping(value="/eliminarTipoPregunta/{idTipoPregunta}", method=RequestMethod.DELETE)
	public ResponseEntity<?> eliminarTipoPregunta(@RequestHeader(value="Authorization") String Authorization, 
			@PathVariable("idTipoPregunta") Integer idTipoPregunta) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		evaParametricasService.eliminaTipoPregunta(idTipoPregunta);
		return ResponseEntity.ok().build();
	}

	//servicio para borrar un TipoRecursoApelacion
	@RequestMapping(value="/eliminarTipoRecursoApelacion/{idTipoRecursoApelacion}", method=RequestMethod.DELETE)
	public ResponseEntity<?> eliminarTipoRecursoApelacion(@RequestHeader(value="Authorization") String Authorization, 
			@PathVariable("idTipoRecursoApelacion") Integer idTipoRecursoApelacion) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		evaParametricasService.eliminaTipoRecursoApelacion(idTipoRecursoApelacion);
		return ResponseEntity.ok().build();
	}
	
	@RequestMapping(value="/listarOpcionRespuesta", method=RequestMethod.GET)
	public ResponseEntity<?> listarOpcionRespuesta(@RequestHeader(value="Authorization") String authorization) {		
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(opcionRespuestaRepository.listarOpcion());		
	}
	
	// servicio que devuelve el jason completo CategoriaPregunta
	@RequestMapping(value = "/buscarOpcionRespuesta/{idOpcionRespuesta}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarOpcionRespuesta(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable("idOpcionRespuesta") Integer idOpcionRespuesta) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		OpcionRespuesta _opcionRespuesta;
		if (opcionRespuestaRepository.findById(idOpcionRespuesta).isPresent()) {
			_opcionRespuesta = opcionRespuestaRepository.findById(idOpcionRespuesta).get();
			OpcionRespuesta opcionRespuesta = new OpcionRespuesta();
			BeanUtils.copyProperties(_opcionRespuesta, opcionRespuesta, "opcionPregunta");
			return ResponseEntity.ok(opcionRespuesta);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
		
	// servicio que graba un TipoRecursoApelacion
	@RequestMapping(value="/grabarOpcionRespuesta", method=RequestMethod.POST)
	public ResponseEntity<?> grabarOpcionRespuesta(@RequestHeader(value="Authorization") String Authorization, @RequestBody OpcionRespuesta opcionRespuesta) {
		try {
			if (!securityService.isTokenValido(Authorization)) {
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
			}
			OpcionRespuesta _opcionRespuesta = new OpcionRespuesta();
			if (opcionRespuesta.getId() != null) {
				_opcionRespuesta = opcionRespuestaRepository.findById(opcionRespuesta.getId()).get();
			}
			BeanUtils.copyProperties(opcionRespuesta, _opcionRespuesta);
			opcionRespuestaRepository.save(_opcionRespuesta);
			return ResponseEntity.ok(_opcionRespuesta);
		} catch (Exception e) {
			e.printStackTrace();
			return RestUtil.preconditionFailedError(ALREADY_REPORTED);
		}
		
	}
	
	//servicio para borrar un TipoRecursoApelacion
	@RequestMapping(value="/eliminarOpcionRespuesta/{idOpcionRespuesta}", method=RequestMethod.DELETE)
	public ResponseEntity<?> eliminarOpcionRespuesta(@RequestHeader(value="Authorization") String Authorization, 
			@PathVariable("idOpcionRespuesta") Integer idOpcionRespuesta) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		evaParametricasService.eliminaOpcionRespuesta(idOpcionRespuesta);
		return ResponseEntity.ok().build();
	}
	
	

}
