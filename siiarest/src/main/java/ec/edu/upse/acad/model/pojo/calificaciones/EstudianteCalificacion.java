package ec.edu.upse.acad.model.pojo.calificaciones;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ec.edu.upse.acad.model.pojo.ComponenteAprendizaje;

import ec.edu.upse.acad.model.pojo.matricula.Estudiante;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="aca", name="estudiante_calificacion")
@NoArgsConstructor
public class EstudianteCalificacion {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_estudiante_calificacion")
	@Getter @Setter private Integer id;
	
	@Column(name="id_estudiante")
	@Getter @Setter private Integer idEstudiante;
	
	@Column(name="id_acta_calificacion")
	@Getter @Setter private Integer idActaCalificacion;
	
	@Column(name="id_componente_aprendizaje")
	@Getter @Setter private Integer idComponenteAprendizaje;
	
	@Getter @Setter private Integer calificacion;

	@Getter @Setter private String estado;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;
	
	//bi-directional many-to-one association to Reglamento
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_estudiante" , insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Estudiante estudiante;
	
	//bi-directional many-to-one association to Reglamento
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_acta_calificacion" , insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private ActaCalificacion actaCalificacion;
	
	//bi-directional many-to-one association to Reglamento
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_componente_aprendizaje" , insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private ComponenteAprendizaje componenteAprendizaje;
	
	//bi-directional one-to-many association to EstudianteRecalificacion
  	@OneToMany(mappedBy="estudianteCalificacion")
  	@Getter @Setter private List<EstudianteRecalificacion> estudianteRecalificaciones;
	
	@PrePersist
	void preInsert() {
	   if (this.estado == null)
	       this.estado = "A";
	}
}
