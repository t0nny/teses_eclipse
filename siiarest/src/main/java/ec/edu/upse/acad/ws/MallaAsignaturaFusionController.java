package ec.edu.upse.acad.ws;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ec.edu.upse.acad.model.pojo.distributivo.MallaAsignaturaFusion;
import ec.edu.upse.acad.model.repository.distributivo.MallaAsignaturaFusionRepository;
import ec.edu.upse.acad.model.service.MallaAsignaturaFusionService;


@RestController
@RequestMapping("/api/mallaAsignaturaFusion")
@CrossOrigin

public class MallaAsignaturaFusionController {
	@Autowired private MallaAsignaturaFusionRepository mallaAsignaturaFusionRepository;
	@Autowired private MallaAsignaturaFusionService  mallaAsignaturaFusionService;

	@RequestMapping(value="/buscarMallaAsignatura/{idPeriodoAcademico}/{idOferta}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarMallaAsignatura(@PathVariable("idPeriodoAcademico") Integer idPeriodoAcademico,
			@PathVariable("idOferta") Integer idOferta) {
		return ResponseEntity.ok(mallaAsignaturaFusionRepository.buscarMallaAsigatura(idPeriodoAcademico, idOferta));
	}
	
	@RequestMapping(value="/buscarMallaAsignaturaOtraCar/{idPeriodoAcademico}/{idOferta}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarMallaAsignaturaOtraCar(@PathVariable("idPeriodoAcademico") Integer idPeriodoAcademico,
			@PathVariable("idOferta") Integer idOferta) {
		return ResponseEntity.ok(mallaAsignaturaFusionRepository.buscarMallaAsigOtraCar(idPeriodoAcademico, idOferta));
	}

	@RequestMapping(value="/buscarMallaAsignaturaComp/{idPeriodoAcademico}/{idOferta}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarMallaAsignaturaComp(@PathVariable("idPeriodoAcademico") Integer idPeriodoAcademico,
			@PathVariable("idOferta") Integer idOferta) {
		return ResponseEntity.ok(mallaAsignaturaFusionRepository.buscarMallaAsigaturaCom(idPeriodoAcademico, idOferta));
	}
	
	@RequestMapping(value="/buscarMallaAsigParalelo/{idPeriodoAcademico}/{idOferta}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarMallaAsigParalelo(@PathVariable("idPeriodoAcademico") Integer idPeriodoAcademico,
			@PathVariable("idOferta") Integer idOferta) {
		return ResponseEntity.ok(mallaAsignaturaFusionRepository.buscarMallaAsigaturaParalelos(idPeriodoAcademico, idOferta));
	}
	
	@RequestMapping(value="/buscarMallaAsigParaleloOtraCar/{idPeriodoAcademico}/{idOferta}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarMallaAsigParaleloOtraCar(@PathVariable("idPeriodoAcademico") Integer idPeriodoAcademico,
			@PathVariable("idOferta") Integer idOferta) {
		return ResponseEntity.ok(mallaAsignaturaFusionRepository.buscarMallaAsigParalelosOtroCar(idPeriodoAcademico, idOferta));
	}
	
	@RequestMapping(value="/buscarMallaAsignaturaFusionCab/{idPeriodoAcademico}/{idOferta}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarMallaAsignaturaFusionCab(@PathVariable("idPeriodoAcademico") Integer idPeriodoAcademico,
			@PathVariable("idOferta") Integer idOferta) {
		return ResponseEntity.ok(mallaAsignaturaFusionRepository.buscarMallaAsigaturaFusionCab(idPeriodoAcademico, idOferta));
	}
	
	@RequestMapping(value="/buscarMallaAsignaturaFusionDet/{idPeriodoAcademico}/{idOferta}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarMallaAsignaturaFusionDet(@PathVariable("idPeriodoAcademico") Integer idPeriodoAcademico,
			@PathVariable("idOferta") Integer idOferta) {
		return ResponseEntity.ok(mallaAsignaturaFusionRepository.buscarMallaAsigaturaFusionDet(idPeriodoAcademico, idOferta));
	}
	
	@RequestMapping(value="/buscarMallaAsignaturaFusionPrueba/{idPeriodoAcademico}/{idOferta}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarMallaAsignaturaFusionPrueba(@PathVariable("idPeriodoAcademico") Integer idPeriodoAcademico,
			@PathVariable("idOferta") Integer idOferta) {
		return ResponseEntity.ok(mallaAsignaturaFusionRepository.buscarMallaAsigaturaParalelo(idPeriodoAcademico, idOferta));
	}
	
	@RequestMapping(value="/grabarMallaAsignaturaFusion", method=RequestMethod.POST)
	public ResponseEntity<?> grabarDistributivo(@RequestBody List<MallaAsignaturaFusion> mallaAsignaturaFusion) {
		mallaAsignaturaFusionService.grabarMallaAsignaturaFusion(mallaAsignaturaFusion);
		return ResponseEntity.ok().build();
	}
}
