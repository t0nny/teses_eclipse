package ec.edu.upse.acad.model.pojo.calificaciones;

import java.util.List;
import javax.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="aca", name="ciclo")
@NoArgsConstructor
public class Ciclo {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_ciclo")
	@Getter @Setter private Integer id;

	@Getter @Setter private String descripcion;
	
	@Column(name="numero_semanas")
	@Getter @Setter private Integer numeroSemanas;

	@Getter @Setter private String estado;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;

	//bi-directional many-to-one association to CiclosPeriodo
	//@OneToMany(mappedBy="ciclo")
	//@Getter @Setter private List<CiclosPeriodo> ciclosPeriodos;

	@OneToMany(mappedBy="ciclo")
	@Getter @Setter private List<ActaCalificacion> ActaCalificaciones;
	@PrePersist
	void preInsert() {
	   if (this.estado == null)
	       this.estado = "A";
	}
}