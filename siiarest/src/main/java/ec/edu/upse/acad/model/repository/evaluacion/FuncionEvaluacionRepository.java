package ec.edu.upse.acad.model.repository.evaluacion;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.evaluacion.FuncionEvaluacion;

@Repository
public interface FuncionEvaluacionRepository extends JpaRepository<FuncionEvaluacion, Integer> {

	@Query(value="SELECT fe "
			+ "FROM FuncionEvaluacion fe "
			+ "WHERE fe.estado = 'A' ")
	List<FuncionEvaluacion> listarFuncionEvaluacion();
}	
