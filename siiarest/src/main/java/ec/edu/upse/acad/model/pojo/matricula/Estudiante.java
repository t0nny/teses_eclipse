package ec.edu.upse.acad.model.pojo.matricula;


import javax.persistence.*;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ec.edu.upse.acad.model.pojo.seguridad.Persona;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


import java.util.List;

@Entity
@Table(schema="aca", name="estudiante")
@Where(clause = "estado='A'")
@NoArgsConstructor
public class Estudiante{

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_estudiante")
	@Getter @Setter private Integer id;
	
	@Column(name="id_persona")
	@Getter @Setter private Integer idPersona;
	
	@Column(name="id_tipo_estudiante")
	@Getter @Setter  private Integer idTipoEstudiante;

	@Getter @Setter private String estado;

//	@Column(name="fecha_ingreso")
//	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private int version;

	//RELACIONES
	//bi-directional many-to-one association to Persona
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="id_persona", nullable = false, insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Persona persona;
	
	//bi-directional many-to-one association to TipoEstudiante
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_tipo_estudiante", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private TipoEstudiante tipoEstudiante;
	
	//bi-directional many-to-one association to EstudianteOferta
	@OneToMany(mappedBy="estudiante", cascade=CascadeType.ALL)
	@Getter @Setter private List<EstudianteOferta> estudianteOfertas;
	
	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }
	

}