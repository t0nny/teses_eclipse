package ec.edu.upse.acad.model.repository.security;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.seguridad.Usuario;


@Repository
public interface UsuarioRepo extends JpaRepository<Usuario, Integer> {

	Optional<Usuario> findByUsuario(String usuario);

}
