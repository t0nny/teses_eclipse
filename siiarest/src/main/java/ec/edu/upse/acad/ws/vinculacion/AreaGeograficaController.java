package ec.edu.upse.acad.ws.vinculacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.upse.acad.model.repository.vinculacion.AreaGeograficaRepository;
import ec.edu.upse.acad.model.service.SecurityService;

@RestController
@RequestMapping("/api/areageografica")
@CrossOrigin
public class AreaGeograficaController {
	@Autowired
	private SecurityService securityService;
	@Autowired
	private AreaGeograficaRepository areaGeograficaRepository;

	// buscar por el id de AreaGeograficaRepository
	@RequestMapping(value = "/buscarAreaGeograficaPorId/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarAreaGeograficaPorId(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable("id") Integer id) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(areaGeograficaRepository.findById(id).get());
	}

	// buscar solo activos
	@RequestMapping(value = "/buscarListaAreaGeografica", method = RequestMethod.GET)
	public ResponseEntity<?> buscarListaAreaGeografica(@RequestHeader(value = "Authorization") String authorization) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(areaGeograficaRepository.buscarListaAreaGeografica());
	}

	// buscar por el idPadre de areaGeografica
	@RequestMapping(value = "/ListarPorIdAreaGeoPadre/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> ListarPorIdAreaGeoPadre(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable("id") Integer id) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(areaGeograficaRepository.ListarPorIdAreaGeoPadre(id).get());
	}

}
