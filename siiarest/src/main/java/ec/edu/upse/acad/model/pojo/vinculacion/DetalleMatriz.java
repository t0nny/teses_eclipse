package ec.edu.upse.acad.model.pojo.vinculacion;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema = "vin", name = "detalle_matriz")

@NoArgsConstructor //un constructorsin argumentos
public class DetalleMatriz {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_detalle_matriz")
	@Getter	@Setter	private Integer id;

	@Column(name = "id_opcion_pregunta")
	@Getter	@Setter	private Integer idOpcionPregunta;
	
	@Getter	@Setter	private String observacion;

	@Column(name = "usuario_ingreso_id")
	@Getter	@Setter	private String usuarioIngresoId;

	@Version
	@Getter	@Setter	private Integer version;

	@Getter	@Setter	private String estado;
	
	@ManyToOne
	@JoinColumn(name = "id_cabecera_matriz", nullable = false, updatable = false)
	@JsonBackReference
	@Getter	@Setter	private CabeceraMatriz cabeceraMatriz;
	
	@PrePersist
	void preInsert() {
		if (this.estado == null)
			this.estado = "A";
	}

}
