package ec.edu.upse.acad.ws;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.upse.acad.model.pojo.DepartamentoOferta;
import ec.edu.upse.acad.model.pojo.seguridad.Departamento;
import ec.edu.upse.acad.model.repository.DepartamentoOfertaRepository;
import ec.edu.upse.acad.model.repository.DepartamentosRepository;
import ec.edu.upse.acad.model.service.DepartamentoService;
import ec.edu.upse.acad.model.service.SecurityService;

@RestController
@RequestMapping("/api/departamento")
@CrossOrigin

public class DepartamentoController {

	@Autowired private DepartamentosRepository departamentosRepository;
	@Autowired private DepartamentoService departamentoService;
	@Autowired private DepartamentoOfertaRepository departamentoOfertaRepository;
	@Autowired private SecurityService securityService;
	// Buscar todos los modulos, para listar en angular
	@RequestMapping(value = "/buscarDepartamento", method = RequestMethod.GET)
	public ResponseEntity<?> buscarDepartamento(@RequestHeader(value="Authorization") String Authorization) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		List<Departamento> departamento= departamentosRepository.findAll();
		for (Departamento departamento2 : departamento) {
			departamento2.setPersonaDepartamento(null);
			departamento2.setDepartamentoOfertas(null);
			departamento2.setArchivos(null);
			departamento2.setDepartamentosCargos(null);
			departamento2.setRolesDepartamentos(null);
	
		}
		
		return ResponseEntity.ok(departamento);
	}
	
	@RequestMapping(value = "/buscarDepartamentoK", method = RequestMethod.GET)
	public ResponseEntity<?> buscarDepartamentoK(@RequestHeader(value="Authorization") String Authorization) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}

		return ResponseEntity.ok(departamentosRepository.listaDepartamentoKanban());
	}

	
	
	
	@RequestMapping(value="/buscarDepartamentoId/{id}", method=RequestMethod.GET)
		public ResponseEntity<?> buscarDepartamentoId(@PathVariable("id") Integer id) {
			Departamento _departamento;
			if (departamentosRepository.findById(id).isPresent()) {
				_departamento = departamentosRepository.findById(id).get();
				_departamento.setPersonaDepartamento(null);
				
				_departamento.setPersonaDepartamento(null);
				_departamento.setDepartamentosCargos(null);
				_departamento.setRolesDepartamentos(null);
				return ResponseEntity.ok(_departamento);
			}else {
				return ResponseEntity.notFound().build();
			}
		}

	@RequestMapping(value = "/grabarDepartamento", method = RequestMethod.POST)
	public ResponseEntity<?> grabarDepartamento(@RequestBody Departamento departamento) {
		Departamento _departamento = new Departamento();
		if (departamento.getId() != null) {
			_departamento = departamentosRepository.findById(departamento.getId()).get();
		}
		BeanUtils.copyProperties(departamento, _departamento);
		departamentosRepository.save(_departamento);
		return ResponseEntity.ok(_departamento);
	}
	


	@RequestMapping(value = "/borrarDepartamento", method = RequestMethod.POST)
	public ResponseEntity<?> borrarDepartamento(@RequestBody Integer id) {
		Departamento departamento = departamentosRepository.findById(id).get();
		if (departamento !=null) {
			departamento.setEstado("IN");
			departamentosRepository.save(departamento);
			
		}
		return ResponseEntity.ok(departamento);
	}

	// lista de todas las facultades
	@RequestMapping(value = "/listaDepartamento", method = RequestMethod.GET)
	public ResponseEntity<?> listaDepartamento() {
		return ResponseEntity.ok(departamentosRepository.listaDepartamento());
	}

	// lista de todas las facultades
	@RequestMapping(value = "/listaDepartamentoPorTipoOferta/{idTipoOferta}", method = RequestMethod.GET)
	public ResponseEntity<?> listaDepartamentoPorTipoOferta(@PathVariable("idTipoOferta") Integer idTipoOferta) {
		return ResponseEntity.ok(departamentosRepository.listaDepartamentoPorTipoOferta(idTipoOferta));
	}

}
