package ec.edu.upse.acad.model.pojo;


import java.sql.Timestamp;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ec.edu.upse.acad.model.pojo.distributivo.DocenteAsignaturaAprend;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Table(schema="aca", name="espacio_fisico")
@NoArgsConstructor
public class EspacioFisico {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_espacio_fisico")
	@Getter @Setter private Integer id;
	
	@Column(name="id_edificacion")
	@Getter @Setter private Integer idEdificacion;
	
	@Column(name="id_tipo_espacio_fisico")
	@Getter @Setter private Integer idTipoEspacioFisico;

	@Getter @Setter private String codigo;

	@Getter @Setter private String descripcion;

	@Getter @Setter private String estado;
	
	@Column(name="fecha_ingreso")
	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;
	
	
	
	//RELACIONES
	//bi-directional many-to-one association to Bloque
	@ManyToOne
	@JoinColumn(name="id_edificacion", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Edificacion edificacion;
	
	@ManyToOne
	@JoinColumn(name="id_tipo_espacio_fisico", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private TipoEspacioFisico tipoEspacioFisico;

	//bi-directional many-to-one association to CompatibilidadAsignatura
	@OneToMany(mappedBy="espacioFisico", cascade=CascadeType.ALL)
	@Getter @Setter private List<DocenteAsignaturaAprend> docenteAsignaturaAprends;

	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }
}