package ec.edu.upse.acad.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.seguridad.Subrol;



@Repository
public interface SubrolesRepository extends JpaRepository<Subrol, Integer>{

	@Query(value="SELECT e "
			+ "FROM Subrol AS e "
			+ "WHERE e.rol_id = ?1 ")
	List<Subrol> buscarPorRolId(Integer rol_id);
}
