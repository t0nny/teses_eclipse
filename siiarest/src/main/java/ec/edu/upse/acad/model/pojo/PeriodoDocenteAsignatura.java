package ec.edu.upse.acad.model.pojo;


import java.sql.Date;


import javax.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="aca", name="vw_rpt_periodo_docente_asignatura")
@NoArgsConstructor
public class PeriodoDocenteAsignatura {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	@Getter @Setter private Integer id;
	
	@Column(name="id_periodo_academico")
	@Getter @Setter private Integer idPeriodoAcademico;
	
	@Column(name="codigo_periodo")
	@Getter @Setter private String codigoPeriodo;

	@Column(name="periodo_academico")
	@Getter @Setter private String periodoAcademico;
	@Column(name="periodo_fecha_hasta")
	@Getter @Setter private Date periodoFechaHasta;

	@Column(name="identificacion")
	@Getter @Setter private String identificacion;
	
	@Column(name="apellidos_nombres")
	@Getter @Setter private String apellidosNombres;

	@Column(name="id_docente")
	@Getter @Setter private Integer idDocente;
	
	@Column(name="id_distributivo_docente")
	@Getter @Setter private Integer idDistributivoDocente;
	@Column(name="carrera_corta")
	@Getter @Setter private String carreraCorta;
	@Column(name="carrera")
	@Getter @Setter private String carrera;
	@Column(name="carrera_asignatura")
	@Getter @Setter private String carreraAsignatura;
	
	@Column(name="asignatura")
	@Getter @Setter private String asignatura;

	@Column(name="total_horas_clases")
	@Getter @Setter private Double totalHorasClases;
	
	@Column(name="horas_maximo")
	@Getter @Setter private Integer horasMaximo;
	
	@Column(name="id_docente_dedicacion")
	@Getter @Setter private Integer idDocenteDedicacion;
	
	@Column(name="docente_dedicacion")
	@Getter @Setter private String docenteDedicacion;
	
	@Column(name="id_docente_categoria")
	@Getter @Setter private Integer idDocenteCategoria;
	
	@Column(name="docente_categoria")
	@Getter @Setter private String docenteCategoria;

}
