package ec.edu.upse.acad.model.repository.planificacion_docente;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.planificacion_docente.Editorial;
@Repository
public interface EditorialRepository   extends JpaRepository<Editorial, Integer> {

	@Query(value="select e.id as idEditorial, e.nombre as nombre, e.nombreCorto as nombreCorto "+
			" from Editorial e ")
	List<CoListadoEditoriales> listaEditoriales();
	interface CoListadoEditoriales{ 
	    Integer getIdEditorial();  
	    String getNombre();
	    String getNombreCorto();
	}
}
