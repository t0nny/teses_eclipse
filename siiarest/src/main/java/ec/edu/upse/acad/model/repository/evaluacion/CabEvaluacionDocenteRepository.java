package ec.edu.upse.acad.model.repository.evaluacion;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.evaluacion.CabEvaluacionDocente;
/**
 * @author dengi
 *
 */
@Repository
public interface CabEvaluacionDocenteRepository extends JpaRepository<CabEvaluacionDocente, Integer>{

}
