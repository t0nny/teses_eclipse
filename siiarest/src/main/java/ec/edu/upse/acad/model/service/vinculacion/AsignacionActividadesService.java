package ec.edu.upse.acad.model.service.vinculacion;

import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jfree.util.Log;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.upse.acad.model.pojo.vinculacion.AsignacionActividad;
import ec.edu.upse.acad.model.repository.vinculacion.AsignacionActividadesRepository;

@Service
@Transactional
public class AsignacionActividadesService {
	@PersistenceContext
	private EntityManager em;
	@Autowired
	private AsignacionActividadesRepository asignacionActividadesRepository;

	public void guardarAsignacionActividades(List<AsignacionActividad> asignacionActividades) {
		for (Iterator iterator = asignacionActividades.iterator(); iterator.hasNext();) {
			AsignacionActividad __asignacionActividades = (AsignacionActividad) iterator.next();

			if (__asignacionActividades.getId() != null) {
				System.out.println("Edita AsignacionActividades");
				AsignacionActividad _asignacionActividades = asignacionActividadesRepository
						.findById(__asignacionActividades.getId()).get();
				BeanUtils.copyProperties(__asignacionActividades, _asignacionActividades);
				asignacionActividadesRepository.save(_asignacionActividades);
			} else {
				System.out.println("Nueva AsignacionActividades");
				AsignacionActividad _asignacionActividades = new AsignacionActividad();
				BeanUtils.copyProperties(__asignacionActividades, _asignacionActividades);
				asignacionActividadesRepository.save(__asignacionActividades);
			}
		}
		em.getEntityManagerFactory().getCache().evict(AsignacionActividad.class);
	}

	public void borrarAsignacionActividades(AsignacionActividad asignacionActividade) {
		Log.info(asignacionActividade);
		asignacionActividade.setEstado("E");
		asignacionActividadesRepository.save(asignacionActividade);

		em.getEntityManagerFactory().getCache().evict(AsignacionActividad.class);
	}

}
