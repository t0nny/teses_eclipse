package ec.edu.upse.acad.model.pojo.evaluacion;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import ec.edu.upse.acad.model.pojo.distributivo.DocenteAsignaturaAprend;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Entity
@Table(schema="eva", name="det_evaluacion_estudiante")
@NoArgsConstructor
public class DetEvaluacionEstudiante {
		    		  
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_det_evaluacion_estudiante")
	@Getter @Setter private Integer id;

	//@Column(name="id_cab_evaluacion_estudiante")
	//@Getter @Setter private Integer idCabEvaluacionEstudiante;
	
	//@Column(name="id_opcion_pregunta")
	//@Getter @Setter private Integer idOpcionPregunta;
		    		  
	@Column(name="id_distributivo_docente")
  	@Getter @Setter private Integer idDistributivoDocente;
		    		  
	@Column(name="id_asignatura_aprendizaje")
	@Getter @Setter private Integer idAsignaturaAprendizaje;
	
	@Column(name="id_paralelo")
	@Getter @Setter private Integer idParalelo;

	@Column(name="valor")
	@Getter @Setter private Integer valor;
	
	@Column(name="respuesta_si_no")
	@Getter @Setter private String respuestaSiNo;
	
	@Getter @Setter private String estado;
	
	@Column(name="fecha_ingreso")
	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;
	
	@Version
	@Getter @Setter private Integer version;

	//RELACIONES
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_cab_evaluacion_estudiante", insertable=true, updatable = true)
	//@JsonIgnore
	@Getter @Setter private CabEvaluacionEstudiante idCabEvaluacionEstudiante;
	
	/*@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_docente_asignatura_aprend", insertable=true, updatable = true)
	//@JsonIgnore
	@Getter @Setter private DocenteAsignaturaAprend docenteAsignaturaAprend;*/

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_opcion_pregunta", nullable = false, insertable=true, updatable = true)
	//@JsonIgnore
	@Getter @Setter private OpcionPregunta idOpcionPregunta;
	
	@PrePersist
	void preInsert() {
	   if (this.estado == null)
	       this.estado = "A";
	}	
	
}
