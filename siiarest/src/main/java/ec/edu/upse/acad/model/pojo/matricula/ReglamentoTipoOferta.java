package ec.edu.upse.acad.model.pojo.matricula;

import javax.persistence.*;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ec.edu.upse.acad.model.pojo.TipoOferta;
import ec.edu.upse.acad.model.pojo.reglamento.Reglamento;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.List;


@Entity
@Table(schema="aca", name="reglamento_tipo_oferta")
@Where(clause = "estado='A'")
@NoArgsConstructor
public class ReglamentoTipoOferta{

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_reglamento_tipo_oferta")
	@Getter @Setter private Integer id;
	
	@Column(name="id_reglamento")
	@Getter @Setter private Integer idReglamento;
	
	@Column(name="id_tipo_oferta")
	@Getter @Setter private Integer idTipoOferta;

	@Getter @Setter private String estado;

//	@Column(name="fecha_ingreso")
//	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private int version;

	//RELACIONES
	//bi-directional many-to-one association to DocumentosEstudiante
	@OneToMany(mappedBy="reglamentoTipoOferta", cascade=CascadeType.ALL)
	@Getter @Setter private List<DocumentosEstudiante> documentosEstudiantes;

	//bi-directional many-to-one association to Reglamento
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_reglamento" , insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Reglamento reglamento;

	//bi-directional many-to-one association to TipoOferta
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_tipo_oferta" , insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private TipoOferta tipoOferta;

	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }
	 
	 
}