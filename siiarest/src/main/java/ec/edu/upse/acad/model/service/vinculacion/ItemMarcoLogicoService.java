package ec.edu.upse.acad.model.service.vinculacion;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.upse.acad.model.pojo.vinculacion.ItemMarcoLogico;
import ec.edu.upse.acad.model.pojo.vinculacion.ObjetivoActividad;
import ec.edu.upse.acad.model.pojo.vinculacion.Proyecto;
import ec.edu.upse.acad.model.repository.vinculacion.ItemMarcoLogicoRepository;
import ec.edu.upse.acad.model.repository.vinculacion.ObjetivoActividadRepository;
import ec.edu.upse.acad.model.repository.vinculacion.ProyectoRepository;

@Service
@Transactional
public class ItemMarcoLogicoService {
	@PersistenceContext
	private EntityManager em;
	@Autowired
	private ProyectoRepository proyectoRepository;
	@Autowired
	private ItemMarcoLogicoRepository itemMarcoLogicoRepository;
	@Autowired
	private ObjetivoActividadRepository objetivoActividadRepository;

	public void proyectoItemMarcoLogico(Proyecto proyecto) {
		Proyecto _proyecto =proyectoRepository.findById(proyecto.getId()).get();
		System.out.println("Nueva Proyecto item Mar Logico");
		for (ItemMarcoLogico itemMarcoLogico : proyecto.getItemMarcoLogico()) {
			if(itemMarcoLogico.getId() != null) {
				System.out.println("Editando Item Marco Logico");
				ItemMarcoLogico _itemMarcoLogico = itemMarcoLogicoRepository.findById(itemMarcoLogico.getId()).get();
				BeanUtils.copyProperties(itemMarcoLogico, _itemMarcoLogico, "objetivoActividad");
				_itemMarcoLogico.setProyecto(_proyecto);
				Integer idItemMarcoLogico=itemMarcoLogicoRepository.saveAndFlush(_itemMarcoLogico).getId();
				ItemMarcoLogico auxItemMarcoLogico=itemMarcoLogicoRepository.findById(idItemMarcoLogico).get();
				for (ObjetivoActividad objetivoActividad : itemMarcoLogico.getObjetivoActividad()) {
					if(objetivoActividad.getId() != null) {
						ObjetivoActividad _objetivoActividad=objetivoActividadRepository.findById(objetivoActividad.getId()).get();
						BeanUtils.copyProperties(objetivoActividad, _objetivoActividad, "itemMarcoLogico");
						_objetivoActividad.setItemMarcoLogico(auxItemMarcoLogico);
						objetivoActividadRepository.save(_objetivoActividad);
					}else {
						ObjetivoActividad __objetivoActividad=new ObjetivoActividad();
						BeanUtils.copyProperties(objetivoActividad, __objetivoActividad, "itemMarcoLogico");
						__objetivoActividad.setItemMarcoLogico(auxItemMarcoLogico);
						objetivoActividadRepository.save(__objetivoActividad);
					}					
				}
			}else {
				System.out.println("Nueva Item Marco Logico");
				ItemMarcoLogico __itemMarcoLogico=new ItemMarcoLogico();
				BeanUtils.copyProperties(itemMarcoLogico, __itemMarcoLogico, "objetivoActividad");
				__itemMarcoLogico.setProyecto(_proyecto);
				Integer idItemMarcoLogico=itemMarcoLogicoRepository.saveAndFlush(__itemMarcoLogico).getId();
				ItemMarcoLogico auxItemMarcoLogico=itemMarcoLogicoRepository.findById(idItemMarcoLogico).get();
				for (ObjetivoActividad objetivoActividad : itemMarcoLogico.getObjetivoActividad()) {
					ObjetivoActividad __objetivoActividad=new ObjetivoActividad();
					BeanUtils.copyProperties(objetivoActividad, __objetivoActividad, "itemMarcoLogico");
					__objetivoActividad.setItemMarcoLogico(auxItemMarcoLogico);
					objetivoActividadRepository.save(__objetivoActividad);
				}
				
				
			}
		}
		em.getEntityManagerFactory().getCache().evict(Proyecto.class);
	}

}
