package ec.edu.upse.acad.model.pojo.planificacion_docente;

import javax.persistence.*;

import org.hibernate.annotations.Where;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.util.List;


/**
 * The persistent class for the metodologia_ensenanza database table.
 * 
 */
@Entity
@Table(schema="aca", name="metodologia_ensenanza")
@Where(clause = "estado='A'")
@NoArgsConstructor
public class MetodologiaEnsenanza  {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_metodologia_ensenanza")
	@Getter @Setter private Integer id;

	@Getter @Setter private String codigo;

	@Getter @Setter private String descripcion;

	@Getter @Setter private String estado;

//	@Column(name="fecha_ingreso")
//	private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;

	//RELACIONES
	//bi-directional many-to-one association to PlanMetodologia
	@OneToMany(mappedBy="metodologiaEnsenanza", cascade=CascadeType.ALL)
	@Getter @Setter private List<PlanMetodologia> planMetodologias;

	@PrePersist
	void preInsert() {
	   if (this.estado == null)
	       this.estado = "A";
	}

	
}