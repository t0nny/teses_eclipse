package ec.edu.upse.acad.model.pojo;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="universidades")
@NoArgsConstructor
public class Universidad {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_universidad")
	@Getter @Setter private Integer id;
	
	@Column(name="id_pais")
	@Getter @Setter private Integer idPais;
	
	@Getter @Setter private String institucion;
	
	@Column(name="fecha_ingreso")
	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Getter @Setter private String estado;

	@Version
	@Getter @Setter private Integer version;

	//RELACIONES
	//bi-directional many-to-one association to Docente
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_pais", insertable=false, updatable = false)
	
	@Getter @Setter private Pais pais;

	//bi-directional many-to-one association to ArchivoDocente
	@OneToMany(mappedBy="universidad", cascade=CascadeType.ALL)
	@JsonIgnore
	@Getter @Setter private List<FormacionProfesional> formacionProfesionales;
	
	@PrePersist
	void preInsert() {
	   if (this.estado == null)
	       this.estado = "A";
	}
}
