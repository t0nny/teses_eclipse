package ec.edu.upse.acad.ws;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.upse.acad.model.pojo.TipoCompOrganizacion;
import ec.edu.upse.acad.model.repository.TipoCompOrganizacionRepository;
import ec.edu.upse.acad.model.service.UpdateForIdService;

@RestController
@RequestMapping("/api/tipocomporganizacion")
@CrossOrigin

public class TipoCompOrganizacionController {
	@Autowired private TipoCompOrganizacionRepository tipocomporganizacionRepository;
	@Autowired private UpdateForIdService eliminaTipoCompOrganizacion;

	//****************** SERVICIOS PARA TIPO COMPONENTE ORGANIZACION************************//

	//Buscar todos los TipoCompOrganizacion, para listar en angular
	@RequestMapping(value="/buscarTipoCompOrganizacion", method=RequestMethod.GET)
	public ResponseEntity<?> buscarTipoCompOrganizacion() {
		return ResponseEntity.ok(tipocomporganizacionRepository.buscarTipoCompOrganizacion());
	}
	//servicio que actualiza o crea un nuevo TipoCompOrganizacion	
	@RequestMapping(value="/buscarTipoCompOrganizacionId/{id}", method=RequestMethod.GET)
	public ResponseEntity<?> buscar(@PathVariable("id") Integer id) {
		TipoCompOrganizacion tipocomporganizacion;
		if (tipocomporganizacionRepository.findById(id).isPresent()) {
			tipocomporganizacion = tipocomporganizacionRepository.findById(id).get();
			return ResponseEntity.ok(tipocomporganizacion);
		}else {
			return ResponseEntity.notFound().build();
		}
	}
	//Servicio que busca por nombre
	@RequestMapping(value="/buscarPorNombre/{nombre}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarPorNombre(@PathVariable("nombre") String nombre) {
		return ResponseEntity.ok(tipocomporganizacionRepository.buscarPorTipoCompOrganizacion("%" + nombre + "%"));
	}
	
	//Servicio de grabar TipoCompOrganizacion
	@RequestMapping(value="/grabarTipoCompOrganizacion", method=RequestMethod.POST)
	public ResponseEntity<?> grabarTipoCompOrganizacion(@RequestBody TipoCompOrganizacion tipocomporganizacion) {
		TipoCompOrganizacion _tipocomporganizacion = new TipoCompOrganizacion();
		if (tipocomporganizacion.getId() != null) {
			_tipocomporganizacion = tipocomporganizacionRepository.findById(tipocomporganizacion.getId()).get();
		}
		BeanUtils.copyProperties(tipocomporganizacion, _tipocomporganizacion);
		tipocomporganizacionRepository.save(_tipocomporganizacion);
		return ResponseEntity.ok(_tipocomporganizacion);
	}

	@RequestMapping(value="/borrar/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<?> borrar(@PathVariable("id") Integer id) {
		eliminaTipoCompOrganizacion.borraTipoCompOrganizacion(id);
		return ResponseEntity.ok().build();
	}
}
