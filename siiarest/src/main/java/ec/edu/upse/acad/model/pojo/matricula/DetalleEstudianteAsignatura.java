package ec.edu.upse.acad.model.pojo.matricula;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="aca", name="detalle_estudiante_asignatura")
@Where(clause = "estado='A'")
@NoArgsConstructor

public class DetalleEstudianteAsignatura {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_detalle_estudiante_asignatura")
	@Getter @Setter private Long id;

	@Column(name="id_estudiante_asignatura")
	@Getter @Setter private Long idEstudianteAsignatura;

	@Column(name="numero_oficio")
	@Getter @Setter private String numeroOficio;

	@Getter @Setter private String observacion;
	
	@Getter @Setter private String estado;
	
	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;
	
//	@Column(name="fecha_ingreso")
//	@Getter @Setter private Timestamp fechaIngreso;
	
	@Version
	@Getter @Setter private int version;

	//RELACIONES
	//bi-directional many-to-one association to EstudianteAsignatura
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_estudiante_asignatura", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private EstudianteAsignatura estudianteAsignatura;

	@PrePersist
	void preInsert() {
		if (this.estado == null) {
			this.estado = "A";
		}
	}

}
