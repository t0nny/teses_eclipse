package ec.edu.upse.acad.model.pojo.seguridad;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.util.List;

@Entity
@Table(schema="seg", name="modulos_roles_usuarios")
@NoArgsConstructor

public class ModuloRolUsuario {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Getter @Setter private Integer id;

	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="modulo_rol_id", insertable=false, updatable = false)
	@Getter @Setter private ModuloRol moduloRol;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="usuario_id", insertable=false, updatable = false)
	@Getter @Setter private Usuario usuario;
	

	@Getter @Setter private String estado;
	
	@Version
	@Getter @Setter private Integer version;
	

	@PrePersist
	void preInsert() {
	   if (this.estado == null)
	       this.estado = "AC";
	}	

}


