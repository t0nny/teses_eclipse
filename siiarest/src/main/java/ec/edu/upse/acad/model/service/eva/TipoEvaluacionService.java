/**
 * 
 */
package ec.edu.upse.acad.model.service.eva;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.upse.acad.model.pojo.evaluacion.Componente;
import ec.edu.upse.acad.model.pojo.evaluacion.FuncionEvaluacion;
import ec.edu.upse.acad.model.pojo.evaluacion.ReglaComponente;
import ec.edu.upse.acad.model.pojo.evaluacion.TipoEvaluacion;
import ec.edu.upse.acad.model.pojo.reglamento.Reglamento;
import ec.edu.upse.acad.model.repository.evaluacion.ReglaComponentRepository;
import ec.edu.upse.acad.model.repository.evaluacion.TipoEvaluacionRepository;
import ec.edu.upse.acad.model.repository.evaluacion.TipoEvaluacionRepository.CO_Componente;
/**
 * @author SIIA_UPSE
 *
 */
@Service
@Transactional
public class TipoEvaluacionService {
	
	@Autowired private TipoEvaluacionRepository tipoEvaluacionRepository;
	@Autowired private ReglaComponentRepository reglaComponentRepository;
	
	@PersistenceContext private EntityManager em;
	
	public List<CO_Componente> filtrarComponentes(){
		List<CO_Componente> componente = tipoEvaluacionRepository.findAllComponente();
		List<CO_Componente> miComponente = new ArrayList<>();
		boolean band = false;
		
		for (CO_Componente _componente : componente) {
			for (CO_Componente __componente : componente) {
				if(_componente.getId() == __componente.getIdComponentePadre()) {
					band = true;
					break;
				}
			}
			if(!band) {
				miComponente.add(_componente);
			}
			band = false;
		}
		return miComponente;
	}
	
	
	public void saveReglaComponente(List<ReglaComponente> reglaComponente) {
		for (ReglaComponente _reglaComponente : reglaComponente) {
			if(_reglaComponente.getId() != null) {
				editaReglaComponente(_reglaComponente);
			}else {
				nuevaReglaComponente(_reglaComponente);
			}
		}
	}
	
	
	public void editaReglaComponente(ReglaComponente reglaComponente) {
		try {
			ReglaComponente _reglaComponente = tipoEvaluacionRepository.findByIdReglaComponente(reglaComponente.getId());
			_reglaComponente.setUsuarioIngresoId(reglaComponente.getUsuarioIngresoId());
			
			for (TipoEvaluacion tipoEvaluacion : reglaComponente.getTipoEvaluacion()) {
				if(tipoEvaluacion.getId() != null) {
					this.editaTipoEvaluacion(tipoEvaluacion);
				}else {
					this.nuevoTipoEvaluacion(_reglaComponente.getId(),tipoEvaluacion);
				}
			}
			reglaComponentRepository.save(_reglaComponente);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public TipoEvaluacion editaTipoEvaluacion(TipoEvaluacion _tipoEvaluacion) {
		TipoEvaluacion tipoEvaluacion = tipoEvaluacionRepository.findById(_tipoEvaluacion.getId()).get();
		tipoEvaluacion.setPonderacion(_tipoEvaluacion.getPonderacion());
		tipoEvaluacion.setUsuarioIngresoId(_tipoEvaluacion.getUsuarioIngresoId());
		tipoEvaluacion.setEstado(_tipoEvaluacion.getEstado());
		return tipoEvaluacion;
	}
	
	public void nuevoTipoEvaluacion(Integer idReglaComponente,TipoEvaluacion _tipoEvaluacion) {
		TipoEvaluacion tipoEvaluacion = new TipoEvaluacion();
		tipoEvaluacion.setIdFuncionEvaluacion(_tipoEvaluacion.getIdFuncionEvaluacion());
		tipoEvaluacion.setIdReglaComponente(idReglaComponente);
		tipoEvaluacion.setPonderacion(_tipoEvaluacion.getPonderacion());
		tipoEvaluacion.setUsuarioIngresoId(_tipoEvaluacion.getUsuarioIngresoId());
		tipoEvaluacionRepository.save(tipoEvaluacion);
	}

	public void nuevaReglaComponente(ReglaComponente reglaComponente) {
		try {			
			ReglaComponente _reglaComponente = new ReglaComponente();
			BeanUtils.copyProperties(reglaComponente, _reglaComponente, "tipoEvaluacion");
			Integer idReglaComponente = reglaComponentRepository.saveAndFlush(_reglaComponente).getId();
			for (TipoEvaluacion _tipoEvaluacion : reglaComponente.getTipoEvaluacion()) {
				this.nuevoTipoEvaluacion(idReglaComponente, _tipoEvaluacion);
			}
			em.refresh(_reglaComponente);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

