package ec.edu.upse.acad.model.pojo.vinculacion;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema = "vin", name = "sub_linea_inv")
@NoArgsConstructor //un constructorsin argumentos
public class SubLineaInv {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_sub_linea_inv")
	@Getter	@Setter	private Integer id;

	@Column(name = "id_lineas_investigacion")
	@Getter	@Setter	private Integer idLineasInv;
	
	@Getter	@Setter	private String descripcion;

	@Column(name = "usuario_ingreso_id")
	@Getter	@Setter	private String usuarioIngresoId;

	@Version
	@Getter	@Setter	private Integer version;

	@Getter	@Setter	private String estado;
	
	@PrePersist
	void preInsert() {
		if (this.estado == null)
			this.estado = "A";
	}


}
