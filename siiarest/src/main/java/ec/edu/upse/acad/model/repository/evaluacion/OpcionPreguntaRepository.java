/**
 * 
 */
package ec.edu.upse.acad.model.repository.evaluacion;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.evaluacion.OpcionRespuesta;
import ec.edu.upse.acad.model.repository.evaluacion.ReglaCalificacionRepository.CustomObjectReglaCalificacion;
import ec.edu.upse.acad.model.pojo.evaluacion.OpcionPregunta;

/**
 * @author dengi
 *
 */
@Repository
public interface OpcionPreguntaRepository extends JpaRepository<OpcionPregunta, Integer>{
	
	//listar opciones de respuestas enviando el id de pregunta 
	@Query(value="SELECT op.idOpcionRespuesta "
			+ "FROM OpcionRespuesta o "
			+ "join OpcionPregunta op on o.id = op.idOpcionRespuesta.id "
			+ "WHERE op.pregunta.id = ?1 and o.estado = 'A' and op.estado = 'A' ")
	List<OpcionRespuesta> listarOpcionRespuestaId(Integer id);
	
	interface CustomObjectOpcionPregunta{
		Integer getId();
		Integer getIdRespuesta();
		Integer getIdPregunta();
		String getDesOpcion(); 
		String getDesPregunta(); 
		Integer getValor(); 		
	}
	
	@Query(value="SELECT op "
			+ " FROM OpcionPregunta op  " 
			+ " inner join OpcionRespuesta o on o.id = op.idOpcionRespuesta.id "
			+ " inner join Pregunta p on p.id = op.pregunta.id "
			+ " WHERE p.id = ?1 "
			+ " AND o.id = ?2 " 
			+ " AND o.estado = 'A' and op.estado = 'A' and p.estado = 'A' " )
	OpcionPregunta findOpcionPreguntaByIdPreguntaAndIdOpcionRespuesta(Integer idPregunta,Integer idOpcionPregunta);
	

}
