package ec.edu.upse.acad.model.pojo.calificaciones;

import java.util.List;

import javax.persistence.*;
import com.fasterxml.jackson.annotation.JsonIgnore;

import ec.edu.upse.acad.model.pojo.PeriodoAcademico;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="aca", name="ciclos_periodo")
@NoArgsConstructor
public class CiclosPeriodo {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_ciclos_periodo")
	@Getter @Setter private Integer id;

	@Getter @Setter private String descripcion;

	@Getter @Setter private String estado;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;

	//bi-directional many-to-one association to ActividadesPonderada
	@OneToMany(mappedBy="ciclosPeriodo")
	@Getter @Setter private List<ActividadesPonderada> actividadesPonderadas;

	//bi-directional many-to-one association to Ciclo
	@ManyToOne
	@JoinColumn(name="id_ciclos")
	@JsonIgnore
	@Getter @Setter private Ciclo ciclo;

	//bi-directional many-to-one association to PeriodoAcademico
	@ManyToOne
	@JoinColumn(name="id_periodo_academico")
	@JsonIgnore
	@Getter @Setter private PeriodoAcademico periodoAcademico;

	//bi-directional many-to-one association to ConfiguracionCalificacione
	@OneToMany(mappedBy="ciclosPeriodo")
	private List<ConfiguracionCalificaciones> configuracionCalificaciones;

	@PrePersist
	void preInsert() {
	   if (this.estado == null)
	       this.estado = "A";
	}
}