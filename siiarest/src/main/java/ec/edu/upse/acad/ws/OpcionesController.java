package ec.edu.upse.acad.ws;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.upse.acad.model.pojo.seguridad.Opcion;
import ec.edu.upse.acad.model.repository.OpcionesRepository;
import ec.edu.upse.acad.model.repository.UsuariosRepository;


@RestController
@RequestMapping("/api/opciones")
@CrossOrigin

public class OpcionesController {

	@Autowired private OpcionesRepository opcionesRepository;
	@Autowired private UsuariosRepository usuarioRepository;


	@RequestMapping(value="/buscarOpcionRol/{usuario}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarOpcionRol(@PathVariable("usuario") String usuario) {
		return ResponseEntity.ok(usuarioRepository.buscarUsuarioModulos(usuario));
	}

	@RequestMapping(value="/buscarUsuarioRol/{idUsuario}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarUsuarioRol(@PathVariable("idUsuario") Integer idUsuario) {
		return ResponseEntity.ok(usuarioRepository.buscarUsuarioRol(idUsuario));

	}

	//cambios de buscar opciones
	@RequestMapping(value="/buscarOpcionPaUsuario/{usuario}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarOpcionPaUsuario(@PathVariable("usuario") Integer usuario) {
		
		return ResponseEntity.ok(usuarioRepository.buscarUsuarioOpcionPa(usuario));
	}

	@RequestMapping(value="/buscarOpcionUsuario/{usuario}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarOpcionUsuario(@PathVariable("usuario") Integer usuario) {
		
		return ResponseEntity.ok(usuarioRepository.buscarUsuarioRolOpcion(usuario));
	}

	//****************** SERVICIOS PARA OPCIONES************************//

	/**Buscar todos los modulos, para listar en angular
	 * Buscar todos los modulos, para listar en angular
	 * @param authorization de acuerdo al rol aasignado
	 * @return visualiza los tipos de roles
	 */
	@RequestMapping(value="/buscarOpcion/{id}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarOpcion(@PathVariable("id") Integer id) {
		Opcion _opcion;
		if (opcionesRepository.findById(id).isPresent()) {
			_opcion = opcionesRepository.findById(id).get();
			return ResponseEntity.ok(_opcion);
		}else {
			return ResponseEntity.notFound().build();
		}
	}

	@RequestMapping(value="/buscarModulosRolesOpciones/{modulo_rol_id}", 
			method=RequestMethod.GET)
	public ResponseEntity<?> buscarModulosRolesOpciones(@PathVariable("modulo_rol_id") Integer modulo_rol_id) {
		return ResponseEntity.ok(opcionesRepository.buscarPorModulosRolesOpcionesUsuario(modulo_rol_id));
	}
}
