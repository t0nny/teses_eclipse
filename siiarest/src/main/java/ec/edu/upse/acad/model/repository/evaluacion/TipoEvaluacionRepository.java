/**
 * 
 */
package ec.edu.upse.acad.model.repository.evaluacion;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ec.edu.upse.acad.model.pojo.evaluacion.Componente;
import ec.edu.upse.acad.model.pojo.evaluacion.FuncionEvaluacion;
import ec.edu.upse.acad.model.pojo.evaluacion.ReglaComponente;
import ec.edu.upse.acad.model.pojo.evaluacion.TipoEvaluacion;
import ec.edu.upse.acad.model.pojo.reglamento.Reglamento;

/**
 * @author SIIA_UPSE
 *
 */
public interface TipoEvaluacionRepository extends JpaRepository<TipoEvaluacion, Integer> {

	@Query(value="select c.id as id, c.idComponentePadre as idComponentePadre, c.descripcion as descripcion, c.codigo as codigo, c.estado as estado "
			+ "from Componente c "
			+ "where c.estado = 'A'")
	List<CO_Componente> findAllComponente();
	interface CO_Componente{
		Integer getId();
		Integer getIdComponentePadre();
		String getDescripcion();
		String getCodigo();
		String getEstado();
	}
	
	@Query(value="select fe.id as idFuncionEvaluacion, fe.descripcion as descripcion "
			+ "from FuncionEvaluacion fe "
			+ "where fe.estado = 'A'")
	List<CO_FunEval> findFuncionEvaluacion();
	interface CO_FunEval{
		Integer getIdFuncionEvaluacion();
		String getDescripcion();
	}
	
	@Query(value = "select r from Reglamento r where r.id = ?1 and r.estado = 'A'")
	Reglamento findByIdReglamento(Integer idReglamento);
	
	@Query(value = "select fe from FuncionEvaluacion fe where fe.id = ?1 and fe.estado = 'A'")
	FuncionEvaluacion findByIdFuncionEvaluacion(Integer idFuncionEvaluacion);
	
	@Query(value = "select c from Componente c where c.id = ?1 and c.estado = 'A'")
	Componente findByIdComponente(Integer idComponente);
	
	@Query(value = "select rc from ReglaComponente rc where rc.id = ?1 and rc.estado = 'A'")
	ReglaComponente findByIdReglaComponente(Integer idReglaComponente);
	
	
	
	
//	@Query(value = "select rc from ReglaComponente rc where rc.estado = 'A'")
//	List<ReglaComponente> findAllReglaComponente();
	
}
