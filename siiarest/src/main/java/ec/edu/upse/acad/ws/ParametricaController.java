package ec.edu.upse.acad.ws;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.upse.acad.model.pojo.seguridad.Parametrica;
import ec.edu.upse.acad.model.repository.ParametricaRepository;

@RestController
@RequestMapping("/api/parametricas")
@CrossOrigin

public class ParametricaController {
	@Autowired private ParametricaRepository parametricaRepository;

	//****************** SERVICIOS PARA DEPARTAMENTOS************************//

	//Buscar todos los parametricas, para listar en angular
	@RequestMapping(value="/buscarParametrica", method=RequestMethod.GET)
	public ResponseEntity<?> buscarParametrica() {
		return ResponseEntity.ok(parametricaRepository.findAll());
	}
	
	//servicio que actualiza o crea un nuevo parametrica	
	@RequestMapping(value="/buscarParametricaId/{id}", method=RequestMethod.GET)
	public ResponseEntity<?> buscar(@PathVariable("id") Integer id) {
		Parametrica parametrica;
		if (parametricaRepository.findById(id).isPresent()) {
			parametrica = parametricaRepository.findById(id).get();
			return ResponseEntity.ok(parametrica);
		}else {
			return ResponseEntity.notFound().build();
		}
	}
	//Servicio que busca por nombre
	@RequestMapping(value="/buscarPorNombre/{nombre}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarPorNombre(@PathVariable("nombre") String nombre) {
		return ResponseEntity.ok(parametricaRepository.buscarPorParametricas("%" + nombre + "%"));
	}
	//Servicio de grabar Parametrica
	@RequestMapping(value="/grabarParametrica", method=RequestMethod.POST)
	public ResponseEntity<?> grabarParametrica(@RequestBody Parametrica parametrica) {
		Parametrica _parametrica = new Parametrica();
		if (parametrica.getId() != null) {
			_parametrica = parametricaRepository.findById(parametrica.getId()).get();
		}
		BeanUtils.copyProperties(parametrica, _parametrica);
		parametricaRepository.save(_parametrica);
		return ResponseEntity.ok(_parametrica);
	}

	@RequestMapping(value="/borrar/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<?> borrar(@PathVariable("id") Integer id) {
		return ResponseEntity.ok().build();
	}
}
