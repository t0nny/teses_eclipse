package ec.edu.upse.acad.model.repository.vinculacion;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.vinculacion.AsignacionActividad;

@Repository
public interface AsignacionActividadesRepository extends JpaRepository<AsignacionActividad, Integer> {
	
	@Transactional
	// lista de asignaciones de tareas
	@Query(value = "SELECT a.id as idAsignacion, a.idProyecto as idProyecto,a.idPersona as idPersona,"
			+ " a.tipoParticipante as descripcion, a.idObjetivoActividad as idObjetivoActividad, a.idObjetivoTarea as idObjetivoTarea, "
			+ " p.titulo as tituloProyecto,(concat(per.nombres ,' ',per.apellidos))as nombres, oa.descripcion as descripcionActividad, "
			+ "ot.descripcion as descripcionTarea, ot.fechaDesde as fechaDesde, ot.fechaHasta as fechaHasta from AsignacionActividad a "
			+ "INNER JOIN Proyecto p on a.idProyecto=p.id "
			+ "INNER JOIN Persona per on a.idPersona=per.id "
			+ "INNER JOIN ObjetivoActividad oa on a.idObjetivoActividad=oa.id "
			+ "INNER JOIN ObjetivoTarea ot on a.idObjetivoTarea=ot.id "
			+ "WHERE a.idProyecto=(?1) and a.estado='A' ")
	List<objetoAsignacionActividades> buscarAsignacionActividades(Integer idProyecto);

	interface objetoAsignacionActividades {
		Integer getIdAsignacion();
		Integer getIdProyecto();
		Integer getIdPersona();
		String getDescripcion();
		Integer getIdObjetivoActividad();
		Integer getIdObjetivoTarea();
		String getTituloProyecto();
		String getNombres();
		String getDescripcionActividad();
		String getDescripcionTarea();
		Date getFechaDesde();
		Date getFechaHasta();
	}

}
