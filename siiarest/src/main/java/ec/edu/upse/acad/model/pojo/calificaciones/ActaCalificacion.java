package ec.edu.upse.acad.model.pojo.calificaciones;


import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;


import ec.edu.upse.acad.model.pojo.distributivo.DocenteAsignaturaAprend;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="aca", name="acta_calificacion")
@Where(clause = "estado='A'")
@NoArgsConstructor
public class ActaCalificacion {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_acta_calificacion")
	@Getter @Setter private Integer id;
	
	@Column(name="id_calificacion_general")
	@Getter @Setter private Integer idCalificacionGeneral;
	
	@Column(name="id_docente_asignatura_aprend")
	@Getter @Setter private Integer idDocenteAsignaturaAprend;
	
	@Column(name="id_ciclo")
	@Getter @Setter private Integer idCiclo;
	
	@Getter @Setter private int codigo;
	
	//@Column(name="fecha_ingreso")
	//@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Getter @Setter private String estado;

	@Version
	@Getter @Setter private Integer version;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JsonIgnore
	@JoinColumn(name="id_calificacion_general", insertable=false, updatable = false)
	@Getter @Setter private CalificacionGeneral calificacionGeneral;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JsonIgnore
	@JoinColumn(name="id_ciclo", insertable=false, updatable = false)
	@Getter @Setter private Ciclo ciclo;

	@ManyToOne(fetch=FetchType.LAZY)
	@JsonIgnore
	@JoinColumn(name="id_docente_asignatura_aprend", insertable=false, updatable = false)
	@Getter @Setter private DocenteAsignaturaAprend docenteAsignaturaAprend;


	//bi-directional one-to-many association to EstudianteRecalificacion
  	@OneToMany(mappedBy="actaCalificacion")
  	@Getter @Setter private List<EstudianteCalificacion> estudianteCalificaciones;

	@PrePersist
	void preInsert() {
	   if (this.estado == null)
	       this.estado = "A";
 }
		
}
