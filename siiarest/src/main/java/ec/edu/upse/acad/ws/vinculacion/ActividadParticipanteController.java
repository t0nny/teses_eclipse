package ec.edu.upse.acad.ws.vinculacion;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.upse.acad.model.pojo.vinculacion.ActividadParticipante;
import ec.edu.upse.acad.model.pojo.vinculacion.AsignacionActividad;
import ec.edu.upse.acad.model.repository.vinculacion.ActividadParticipanteRepository;
import ec.edu.upse.acad.model.service.SecurityService;
import ec.edu.upse.acad.model.service.vinculacion.ActividadParticipanteService;

@RestController
@RequestMapping("/api/actividadParticipante")
@CrossOrigin
public class ActividadParticipanteController {
	@Autowired
	private SecurityService securityService;
	@Autowired
	private ActividadParticipanteRepository actividadParticipanteRepository;
	@Autowired
	private ActividadParticipanteService actividadParticipanteService;
	
	@RequestMapping(value = "/grabarActividadParticipante", method = RequestMethod.POST)
	public ResponseEntity<?> grabarActividadParticipante(@RequestHeader(value = "Authorization") String Authorization, // definimos
			@RequestBody List<ActividadParticipante> actividadParticipante) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		try {
			actividadParticipanteService.guardarActividadParticipante(actividadParticipante);
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(" ya" + e.getMessage() + "este " + e.getCause());
			// return ResponseEntity.status((HttpStatus.NOT_FOUND)).body(null);
		}
		return ResponseEntity.ok().build();
	}
	
	@RequestMapping(value = "/buscarActividadesRealizadas/{idProyecto}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarActividadesRealizadas(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable("idProyecto") Integer idProyecto) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(actividadParticipanteRepository.buscarActividadesRealizadas(idProyecto));
	}
	
	@RequestMapping(value = "/editarActividadesRealizadas/{idActividadParticipante}", method = RequestMethod.GET)
	public ResponseEntity<?> editarActividadesRealizadas(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable("idActividadParticipante") Integer idActividadParticipante) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(actividadParticipanteRepository.editarActividadesRealizadas(idActividadParticipante));
	}
	
	@RequestMapping(value = "/borrarActividadesRealizadas/{idActividadRealizada}", method = RequestMethod.DELETE)
	public ResponseEntity<?> borrarActividadesRealizadas(@RequestHeader(value = "Authorization") String Authorization,
			@PathVariable("idActividadRealizada") Integer idActividadRealizada) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		ActividadParticipante actividadParticipante = actividadParticipanteRepository.findById(idActividadRealizada).get();
		if (actividadParticipante != null) {
			actividadParticipante.setEstado("E");
			actividadParticipanteRepository.save(actividadParticipante);
			return ResponseEntity.ok().build();
		} else {
			return ResponseEntity.notFound().build();
		}
	}
}
