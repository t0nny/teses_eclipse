package ec.edu.upse.acad.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.AsignaturaRelacion;


@Repository
public interface AsignaturasRelacionesRepository extends JpaRepository<AsignaturaRelacion, Integer>{

	@Query(value=" select ma.id as idMallaAsignatura, a.descripcion as nombreAsignatura,n.orden as orden,n.descripcion as nombreNivel" + 
			" from MallaAsignatura ma inner join Asignatura a " + 
			" on ma.idAsignatura = a.id inner join Nivel n " + 
			" on ma.idNivel = n.id  where ma.id = (?1)"
			)
	CustomObjectMA recuperarNombreMateriaNivel(Integer idMallaAsignatura);

	interface CustomObjectMA { 
		Integer getIdMallaAsignatura(); 
		String getNombreAsignatura(); 
		Integer getOrden();  
		String getNombreNivel(); 

	} 

	@Query(value=" select ma.id as id,a.descripcion as nombreAsignatura, ma.estado as state, 0 as tags,'#15B362' as hex,'PRE' as content " + 
			" from MallaAsignatura ma inner join Asignatura a " + 
			" on ma.idAsignatura = a.id inner join Nivel n " + 
			" on ma.idNivel = n.id  inner join Malla m " + 
			" on ma.idMalla = m.id where m.id = (select ma2.idMalla from MallaAsignatura ma2 where ma2.id = (?1))  " +
			" and (n.orden <= (select n1.orden from MallaAsignatura ma1, Nivel n1 " + 
			" where ma1.id = (?1) and ma1.idNivel = n1.id)-1  " + 
			" and n.orden >= (select n1.orden from MallaAsignatura ma1, Nivel n1 " + 
			" where ma1.id = (?1) and ma1.idNivel = n1.id)-3) and ma.id not in (select ar.idMallaAsignaturaRelacion" + 
			" from AsignaturaRelacion ar where ar.idMallaAsignatura = (?1) and ar.tipoRelacion= 'PRE' and ar.estado = 'A')"
			)
	List<CustomObject> buscarPerrequisitos(Integer idMallaAsignatura);
	
	@Query(value=" select ma.id as id,a.descripcion as nombreAsignatura, ma.estado as state, 0 as tags,'#289DE5' as hex, 'COR' as content " + 
			" from MallaAsignatura ma inner join Asignatura a " + 
			" on ma.idAsignatura = a.id inner join Nivel n " + 
			" on ma.idNivel = n.id  inner join Malla m " + 
			" on ma.idMalla = m.id where m.id = (select ma2.idMalla from MallaAsignatura ma2 where ma2.id = (?1)) and ma.id != (?1) " +
			" and n.orden = (select n1.orden from MallaAsignatura ma1,Nivel n1 " + 
			" where ma1.idNivel = n1.id and  ma1.id = (?1)) and ma.id not in (select ar.idMallaAsignaturaRelacion " + 
			" from AsignaturaRelacion ar where ar.idMallaAsignatura = (?1) and ar.tipoRelacion= 'COR' and ar.estado = 'A')"
			)
	List<CustomObject> buscarCorrequisitos(Integer idMallaAsignatura);
	
	interface CustomObject { 
		Integer getId(); 
		String getNombreAsignatura(); 
		String getState(); 
		Integer getTags();
		String getHex(); 
		String getContent();
	} 

	@Query(value=" select ma.id as idMallaAsignatura,a.codigo as codigo, a.descripcion as asignatura,"+
			" n.id as idNivel ,n.orden as orden, n.descripcion as nivel, ma.numHoras as numHoras, ma.numCreditos as numCreditos" 
			+ " from MallaAsignatura  ma  inner join Asignatura  a on ma.idAsignatura = a.id " 
			+ " inner join Nivel  n on ma.idNivel = n.id " 
			+ " inner join Malla  m on m.id = ma.idMalla " 
			+ " where m.id = (?1) "
			+ " and ma.estado='A' and n.estado='A' and m.estado in ('A','P') order by n.orden"
			)
	List<CustomObject1> listarAsignaturasAsociadasMallaCarrera(Integer idMalla);

	interface CustomObject1  { 
		Integer getIdMallaAsignatura(); 
		String getCodigo(); 
		String getAsignatura(); 
		Integer getIdNivel(); 
		Integer getOrden(); 
		String getNivel(); 
		Integer getNumHoras(); 
		Integer getNumCreditos(); 

	} 

	@Query(value = " select ma.id as id,a.descripcion as nombreAsignatura, ma.estado as state, ar.id as tags, ar.version as resourceId,"
			+ " '#4DD88A' as hex,'PRE' as content " 
			+ " from MallaAsignatura ma inner join Asignatura a "
			+ " on ma.idAsignatura = a.id inner join  AsignaturaRelacion ar "
			+ " on ar.idMallaAsignaturaRelacion= ma.id "
			+ " where ar.idMallaAsignatura = (?1) and ar.tipoRelacion= 'PRE' and ar.estado = 'A'  and ma.estado = 'A' ")
	List<CustomObject2> cargarPerrequisitosExistentes(Integer idMallaAsignatura);

	@Query(value = " select ma.id as id, a.descripcion as nombreAsignatura, ma.estado as state, ar.id as tags, ar.version as resourceId,"
			+ " '#3A5FC2' as hex,'COR' as content "
			+ " from MallaAsignatura ma inner join Asignatura a "
			+ " on ma.idAsignatura = a.id inner join  AsignaturaRelacion ar "
			+ " on ar.idMallaAsignaturaRelacion= ma.id "
			+ " where ar.idMallaAsignatura = (?1) and ar.tipoRelacion= 'COR' and ar.estado = 'A'   and ma.estado = 'A' ")
	List<CustomObject2> cargarCorrequisitosExistentes(Integer idMallaAsignatura);

	interface CustomObject2 { 
		Integer getId(); 
		String getNombreAsignatura(); 
		String getState(); 
		Integer getTags();
		Integer getResourceId();
		String getHex(); 
		String getContent();
	} 



}
