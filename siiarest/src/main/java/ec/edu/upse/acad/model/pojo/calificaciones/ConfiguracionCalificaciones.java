package ec.edu.upse.acad.model.pojo.calificaciones;

import java.util.Date;

import javax.persistence.*;
import com.fasterxml.jackson.annotation.JsonIgnore;

import ec.edu.upse.acad.model.pojo.reglamento.Reglamento;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="aca", name="configuracion_calificaciones")
@NoArgsConstructor
public class ConfiguracionCalificaciones {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_configuracion_calificaciones")
	@Getter @Setter private Integer id;

	@Getter @Setter private String descripcion;

	@Getter @Setter private String estado;

	@Column(name="fecha_desde")
	@Getter @Setter private Date fechaDesde;

	@Column(name="fecha_hasta")
	@Getter @Setter private Date fechaHasta;

	@Column(name="num_actividades_ciclo")
	private String numActividadesCiclo;

	@Column(name="num_ciclos")
	@Getter @Setter private int numCiclos;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;

	//RELACIONES
	//bi-directional many-to-one association to CalificacionGeneral
	/*@OneToMany(mappedBy="configuracionCalificaciones")
	@Getter @Setter private List<CalificacionGeneral> calificacionGenerals;*/

	//bi-directional many-to-one association to CiclosPeriodo
	@ManyToOne
	@JoinColumn(name="id_ciclos_periodo")
	@JsonIgnore
	@Getter @Setter private CiclosPeriodo ciclosPeriodo;

	//bi-directional many-to-one association to Reglamento
	@ManyToOne
	@JoinColumn(name="id_reglamento")
	@JsonIgnore
	@Getter @Setter private Reglamento reglamento;

	@PrePersist
	void preInsert() {
	   if (this.estado == null)
	       this.estado = "A";
	}
}