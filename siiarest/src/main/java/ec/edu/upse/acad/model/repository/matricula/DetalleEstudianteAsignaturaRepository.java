package ec.edu.upse.acad.model.repository.matricula;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.matricula.DetalleEstudianteAsignatura;

@Repository
public interface DetalleEstudianteAsignaturaRepository extends JpaRepository<DetalleEstudianteAsignatura, Long>{

}
