package ec.edu.upse.acad.ws;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.upse.acad.model.pojo.DepartamentoOferta;
import ec.edu.upse.acad.model.pojo.Malla;
import ec.edu.upse.acad.model.repository.DepartamentoOfertaRepository;
import ec.edu.upse.acad.model.repository.MallaRepository;

@RestController
@RequestMapping("/api/configuracionOferta")
@CrossOrigin

public class ConfiguracionOfertaController {


	/*
	 * servicios de departamento_oferta
	 * servicios de malla
	 * servicios de malla_version
	 */

	@Autowired private DepartamentoOfertaRepository departamentoOfertasRepository;
	@Autowired private MallaRepository mallaRepository;
	@Autowired private MallaRepository mallaVersionRepository;
	
	//****************** SERVICIOS PARA DEPARTAMENTO OFERTA************************//

	//Buscar todos los departamentos_ofertas, para listar en angular
	@RequestMapping(value="/buscarDepartamentosOfertas", method=RequestMethod.GET)
	public ResponseEntity<?> buscarDepartamentosOfertas() {
		return ResponseEntity.ok(departamentoOfertasRepository.findAll());
	}


	//Buscar todos los departamentos_ofertas, para filtrar por id
	@RequestMapping(value="/buscarDepartamentosOfertasId/{id}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarDepartamentosOfertasId(@PathVariable("id") Integer id) {
		DepartamentoOferta _departamentoOferta;
		if (departamentoOfertasRepository.findById(id).isPresent()) {
			_departamentoOferta = departamentoOfertasRepository.findById(id).get();
			return ResponseEntity.ok(_departamentoOferta);
		}else {
			return ResponseEntity.notFound().build();
		}
	}	

	//Servicio de grabar los departamentos_ofertas
	@RequestMapping(value="/grabarDepartamentoOfertas", method=RequestMethod.POST)
	public ResponseEntity<?> grabarDepartamentoOfertas(@RequestBody DepartamentoOferta departamentoOferta) {
		DepartamentoOferta _departamentoOferta = new DepartamentoOferta();
		if (departamentoOferta.getId() != null) {
			_departamentoOferta = departamentoOfertasRepository.findById(departamentoOferta.getId()).get();
		}
		BeanUtils.copyProperties(departamentoOferta, _departamentoOferta);
		departamentoOfertasRepository.save(_departamentoOferta);
		return ResponseEntity.ok(_departamentoOferta);
	}

	//Servicio de borrar  departamentos_ofertas
	@RequestMapping(value="/borrarDepartamentosOfertas/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<?> borrarDepartamentosOfertas(@PathVariable("id") Integer id) {
		return ResponseEntity.ok().build();
	}
	//***********************FIN SERVICIOS PARA DEPARTAMENTO OFERTA***********************


	//****************** SERVICIOS PARA MALLA************************//

	//Buscar todos las mallas, para listar en angular
	@RequestMapping(value="/buscarMallas", method=RequestMethod.GET)
	public ResponseEntity<?> buscarMallas() {
		return ResponseEntity.ok(mallaRepository.findAll());
	}

	//Buscar todos las mallas, para filtrar por id
	@RequestMapping(value="/buscarMallasId/{id}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarMallasId(@PathVariable("id") Integer id) {
		Malla _malla;
		if (mallaRepository.findById(id).isPresent()) {
			_malla = mallaRepository.findById(id).get();
			return ResponseEntity.ok(_malla);
		}else {
			return ResponseEntity.notFound().build();
		}
	}	

	//Servicio de grabar las mallas
	@RequestMapping(value="/grabarMalla", method=RequestMethod.POST)
	public ResponseEntity<?> grabarMalla(@RequestBody Malla malla) {
		Malla _malla = new Malla();
		if (malla.getId() != null) {
			_malla = mallaRepository.findById(malla.getId()).get();
		}
		BeanUtils.copyProperties(malla, _malla);
		mallaRepository.save(_malla);
		return ResponseEntity.ok(_malla);
	}

	//Servicio de borrar  mallas
	@RequestMapping(value="/borrarMallas/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<?> borrarMallas(@PathVariable("id") Integer id) {
		return ResponseEntity.ok().build();
	}
	//***********************FIN SERVICIOS PARA MALLA ***********************

	//****************** SERVICIOS PARA MALLA VERSION************************//

	//Buscar todos las mallas versiones, para listar en angular
	@RequestMapping(value="/buscarMallasVersiones", method=RequestMethod.GET)
	public ResponseEntity<?> buscarMallasVersiones() {
		return ResponseEntity.ok(mallaVersionRepository.findAll());
	}

	//Servicio de borrar  malla_version
	@RequestMapping(value="/borrarMallaVersion/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<?> borrarMallaVersion(@PathVariable("id") Integer id) {
		return ResponseEntity.ok().build();
	}
	//***********************FIN SERVICIOS PARA MALLA VERSION ***********************
}