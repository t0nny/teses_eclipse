package ec.edu.upse.acad.model.pojo;

import java.sql.Timestamp;

import javax.persistence.*;
import java.lang.String;

import org.hibernate.annotations.Where;

import ec.edu.upse.acad.model.pojo.matricula.DocumentosEstudiante;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Entity
@Table(schema="aca", name="nivel")
@Where(clause="estado = 'A'")
@NoArgsConstructor
public class Nivel  {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_nivel")
	@Getter @Setter private Integer id;

	@Getter @Setter private String codigo;

	@Getter @Setter private String descripcion;

	@Column(name="descripcion_corta")
	@Getter @Setter private String descripcionCorta;
	
	@Getter @Setter private Integer orden;

	@Getter @Setter private String estado;
	
	@Column(name="fecha_ingreso")
	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;

	
	//RELACIONES
	//bi-directional many-to-one association to MallaAsignatura
	@OneToMany(mappedBy="nivel", cascade=CascadeType.ALL)
	@Getter @Setter private List<MallaAsignatura> mallaAsignaturas;

	//bi-directional many-to-one association to Malla
	@OneToMany(mappedBy="nivel", cascade=CascadeType.ALL)
	@Getter @Setter private List<Malla> malla;
		
	//bi-directional many-to-one association to Malla
	@OneToMany(mappedBy="nivel", cascade=CascadeType.ALL)
	@Getter @Setter private List<Malla> malla1;
	
	//bi-directional many-to-one association to DocumentosEstudiante
	@OneToMany(mappedBy="nivel", cascade=CascadeType.ALL)
	@Getter @Setter private List<DocumentosEstudiante> documentosEstudiantes;
				
//	 @PrePersist
//		void preInsert() {
//		   if (this.estado == null)
//		       this.estado = "A";
//	 }
}