package ec.edu.upse.acad.model.pojo.reglamento;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ec.edu.upse.acad.model.pojo.DocenteCategoria;
import ec.edu.upse.acad.model.pojo.distributivo.DocenteDedicacion;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="aca", name="dedicacion_horas_clase")
@NoArgsConstructor
public class DedicacionHorasClase {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_dedicacion_horas_clase")
	@Getter @Setter private Integer id;
	
	@Column(name="id_reglamento")
	@Getter @Setter private Integer idReglamento;
	
	@Column(name="id_docente_dedicacion")
	@Getter @Setter private Integer idDocenteDedicacion;
	
	@Column(name="id_docente_categoria")
	@Getter @Setter private Integer idDocenteCategoria;
	
	@Column(name="horas_minimo")
	@Getter @Setter private Integer horasMinimo;
	
	@Column(name="horas_maximo")
	@Getter @Setter private Integer horasMaximo;

	@Getter @Setter  private String estado;
		
	@Column(name="fecha_ingreso")
	@Getter @Setter  private Date fechaIngreso;
		
	@Column(name="usuario_ingreso_id")
	@Getter @Setter  private String usuarioIngresoId;
		
	@Version
	@Getter @Setter  private Integer version;
	
	 
	//RELACIONES
	
	 //bi-directional many-to-one association to Docente Categoria
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_reglamento" , insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Reglamento reglamento;
	
	//bi-directional many-to-one association to Docente Categoria
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_docente_dedicacion" , insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private DocenteDedicacion docenteDedicacion;
	
	//bi-directional many-to-one association to Docente Categoria
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_docente_categoria" , insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private DocenteCategoria docenteCategoria;
			
			
    
	
   	
	
	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }
}
