package ec.edu.upse.acad.model.repository.matricula;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.matricula.EstudianteOferta;

@Repository
public interface EstudianteOfertaRepository  extends JpaRepository<EstudianteOferta, Integer>{
	
	@Query(value=" select o.id as idCarrera , o.descripcion as carrera,do.idDepartamento as idDepartamento, do.id as idDepartamentoOferta"+
			" from Oferta o inner join DepartamentoOferta do " + 
			" on o.id = do.idOferta ")
	List<CustomObjectCt> cargarTodasCarreras();
	interface CustomObjectCt{ 
	    Integer getIdCarrera(); 
	    String getCarrera(); 
	    Integer getIdDepartamento();
	    Integer getIdDepartamentoOferta();
	}
	
	@Query(value=" SELECT per.identificacion as identificacion  , concat (per.apellidos,' ', per.nombres) as apellidosNombres, " + 
			" est.id as idEstudiante, eso.id as idEstudianteOferta, eso.idDepartamentoOferta as idDepartamentoOferta,"+
			" do.idOferta as idOferta, eso.idMalla as idMalla, mal.descripcion as malla, tie.descripcion as tipoIngreso " + 
			" FROM Persona as per " + 
			" INNER JOIN Estudiante as est on per.id=est.idPersona " + 
			" INNER JOIN EstudianteOferta as eso on est.id=eso.idEstudiante "+
			" INNER JOIN DepartamentoOferta as do on eso.idDepartamentoOferta=do.id"+
			" INNER JOIN Malla as mal on eso.idMalla=mal.id "+
			" INNER JOIN TipoIngresoEstudiante as tie on tie.id=eso.idTipoIngresoEstudiante "+
			" WHERE do.idOferta=(?1) and tie.codigo in ('HOM','REC')")
	List<CustomObjectEstudianteOferta> ListarEstudianteOferta(Integer idOferta);
	interface CustomObjectEstudianteOferta{ 
	    String getIdentificacion(); 
	    String getApellidosNombres(); 
	    Integer getIdEstudiante();
	    Integer getIdEstudianteOferta();
	    Integer getIdDepartamentoOferta();
	    Integer getIdOferta();
	    Integer getIdMalla();
	    String getMalla(); 
	    String getTipoIngreso(); 
	}
	
	@Query(value=" select p.id as idPersona,concat(p.apellidos,' ',p.nombres) as nombresCompletos,p.identificacion as identificacion,e.id as idEstudiante," + 
			" eo.id as idEstudianteOferta,do.id as idDepartamentoOferta,do.idOferta as idOferta, ti.descripcion as tipoDeIngreso, " +
			" (CASE   WHEN eo.estadoEstudianteOferta='EXP' THEN 'Expulsado' WHEN eo.estadoEstudianteOferta='EGR' THEN 'Egresado' " +
			" WHEN eo.estadoEstudianteOferta='GRA' THEN 'Graduado'   WHEN eo.estadoEstudianteOferta='RET' THEN 'Retirado' " + 
			" WHEN eo.estadoEstudianteOferta='ACT' THEN 'Activo'  END) as estadoEstudiante,m.id  as idMalla ,m.descripcion  as malla" + 
			" from Estudiante e " + 
			" inner join Persona p on p.id = e.idPersona " + 
			" inner join EstudianteOferta eo on eo.idEstudiante = e.id " + 
			" inner join DepartamentoOferta do on do.id = eo.idDepartamentoOferta " + 
			" inner join Malla m on m.id =eo.idMalla " + 
			" inner join TipoIngresoEstudiante ti on ti.id = eo.idTipoIngresoEstudiante " + 
			" where do.id = (?1)")
	List<ListEstudiantesRegistrados> listarEstudianteRegistrados(Integer idDepartamentoOferta);
	interface ListEstudiantesRegistrados{ 
	    Integer getIdPersona(); 
	    String getNombresCompletos(); 
	    String getIdentificacion(); 
	    Integer getIdEstudiante();
	    Integer getIdEstudianteOferta();
	    Integer getIdDepartamentoOferta();
	    Integer getIdOferta();
	    String getTipoDeIngreso();
	    String getEstadoEstudiante();
	    String getIdMalla();
	    String getMalla(); 
	}
	

}
