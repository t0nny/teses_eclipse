package ec.edu.upse.acad.ws;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ec.edu.upse.acad.model.pojo.Docente;
import ec.edu.upse.acad.model.pojo.OfertaDocente;
import ec.edu.upse.acad.model.pojo.seguridad.Persona;
import ec.edu.upse.acad.model.repository.AreaConocimientoRepository;
import ec.edu.upse.acad.model.repository.DocenteCategoriaRepository;
import ec.edu.upse.acad.model.repository.DocenteRepository;
import ec.edu.upse.acad.model.repository.FormacionProfesionalRepository;
import ec.edu.upse.acad.model.repository.NivelAcademicoRepository;
import ec.edu.upse.acad.model.repository.PersonasRepository;
import ec.edu.upse.acad.model.repository.UniversidadRepository;
import ec.edu.upse.acad.model.service.PersonaService;

/**
 * @author Mishell
 *
 */
@RestController
@RequestMapping("/api/docentes")
@CrossOrigin
public class DocenteController {

	@Autowired private FormacionProfesionalRepository formacionProfesionalRepository;
	@Autowired private UniversidadRepository universidadRepository;
	@Autowired private NivelAcademicoRepository nivelAcademicoRepository;
	@Autowired private AreaConocimientoRepository areaConocimientoRepository;
	@Autowired private DocenteRepository docenteRepository;
	@Autowired private PersonaService personaService;
	@Autowired private PersonasRepository personasRepository;
	@Autowired private DocenteCategoriaRepository docenteCategoriaRepository; 
	//lista todos los docentes que se encuentren
	
	
	@RequestMapping(value="/buscarDocentePorId/{id}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarDocentePorId (@PathVariable("id") Integer id) {
		Docente docente = docenteRepository.findById(id).get();
		Integer idPersona = docente.getIdPersona();
		Persona persona = personasRepository.findById(idPersona).get();
		return ResponseEntity.ok(persona);
	}
	
	@RequestMapping(value="/listarDocenteRegistrados/{idOferta}", method=RequestMethod.GET)
	public ResponseEntity<?> listarDocenteRegistrados(@PathVariable("idOferta") Integer idOferta) {
		return ResponseEntity.ok(docenteRepository.listarDocenteRegistrados(idOferta));		
	}
	
	//lista todos los docentes que se encuentren
	@RequestMapping(value="/listarTodosDocenteRegistrados", method=RequestMethod.GET)
	public ResponseEntity<?> listarTodosDocenteRegistrados() {
		return ResponseEntity.ok(docenteRepository.listarTodosDocenteRegistrados());		
	}
	
	//recupera Jason completo de un docente por el id
	@RequestMapping(value="/buscarDocente/{idPersona}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarDocente(@RequestHeader(value="Authorization") String authorization, 
			@PathVariable("idPersona") Integer idPersona) {
		return ResponseEntity.ok(personaService.buscarDocente(idPersona));
	}
	
	//recupera Jason completo de un docente por el la cedula
	@RequestMapping(value="/buscarDocentePorCedula/{cedula}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarDocentePorCedula(@PathVariable("cedula") String cedula) {
		Persona _persona = personasRepository.findByIdentificacion(cedula);
		if (_persona==null) {
			return ResponseEntity.ok().build();
		}else {
			Persona persona = new Persona();
			BeanUtils.copyProperties(_persona, persona, "personaDepartamento","miembro","personasCargo","usuario","docente","estudiante");
			return ResponseEntity.ok(persona);
		}
	}
	
	//graba Persona- Estudiante y movilidad Detalle EstudianteOferta
	@RequestMapping(value = "/grabarDocente", method = RequestMethod.POST)
	public ResponseEntity<?> grabarDocente(@RequestBody Persona docente) {	
		personaService.grabaPersonaDocente(docente);
		return ResponseEntity.ok().build();
	}
	
	//lista todos los docentes que se encuentren
	@RequestMapping(value="/listarFormacionProfesionalPorIdPersona/{id}", method=RequestMethod.GET)
	public ResponseEntity<?> listarFormacionProfesionalPorIdPersona(@PathVariable("id") Integer id) {
		return ResponseEntity.ok(docenteRepository.listarFormacionProfesional(id));		
	}
	
	//lista todos los docentes que se encuentren
	@RequestMapping(value="/listarDocenteHistorial/{id}", method=RequestMethod.GET)
	public ResponseEntity<?> listarDocenteHistorial(@PathVariable("id") Integer id) {
		return ResponseEntity.ok(docenteRepository.listarDocenteHistorial(id));		
	}
	
	//Servicio Malla para cargar la pantalla editar malla//
	@RequestMapping(value="/buscarFormacionProfesionalPorId/{id}", method=RequestMethod.GET) 
	public ResponseEntity<?> buscarFormacionProfesionalPorId(@PathVariable("id") Integer id) {
		return ResponseEntity.ok(formacionProfesionalRepository.findById(id).get());
	}
	
	@RequestMapping(value="/listarActualizacionProfesionalPorIdPersona/{id}", method=RequestMethod.GET) 
	public ResponseEntity<?> listarActualizacionProfesionalPorIdPersona(@PathVariable("id") Integer id) {
		return ResponseEntity.ok(docenteRepository.listarActualizacionProfesionalPorIdPersona(id));
	}
	
	//recupera Jason completo de un docente por el id
	@RequestMapping(value="/listarUniversidades", method=RequestMethod.GET)
	public ResponseEntity<?> listarUniversidades() {
		return ResponseEntity.ok(universidadRepository.listarUniversidades());
	}
	
	//recupera Jason completo de un docente por el id
	@RequestMapping(value="/listarNivelesAcademicos", method=RequestMethod.GET)
	public ResponseEntity<?> listarNivelesAcademicos() {
		return ResponseEntity.ok(nivelAcademicoRepository.listarNiveles());
	}
	
	//recupera Jason completo de un docente por el id
	@RequestMapping(value="/listarAreasConocimiento", method=RequestMethod.GET)
	public ResponseEntity<?> listarAreasConocimiento() {
		return ResponseEntity.ok(areaConocimientoRepository.listarAreas());
	}
	
	//recupera Jason completo de un docente por el id
	@RequestMapping(value="/listarDocenteCategoria", method=RequestMethod.GET)
	public ResponseEntity<?> listarDocenteCategoria() {
		return ResponseEntity.ok(docenteCategoriaRepository.listarDocenteCategoria());
	}
	
	@RequestMapping(value="/borrarDocente/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<?> borrarDocente(@PathVariable("id") Integer id) {
		Persona persona = personasRepository.findById(id).get();
		Persona _persona = new Persona();
		if (persona != null) {
			BeanUtils.copyProperties(persona, _persona);
			_persona.setEstado("EL");
			personasRepository.save(_persona);
		}else {
			System.out.println("Vacio");
		}
		return ResponseEntity.ok().build();
	}
	/*CONSULTA PARA LOS MANTENEDORES DE OFERTA DOCENTE */
	@RequestMapping(value="/listarDocenteSinAsignarOferta/{idPeriodo}/{idTipoOferta}", method=RequestMethod.GET) 
	public ResponseEntity<?> listarDocenteSinAsignarOferta(@PathVariable("idPeriodo") Integer idPeriodo,
			@PathVariable("idTipoOferta") Integer idTipoOferta) {
		return ResponseEntity.ok(docenteRepository.listarDocenteSinAsignarOfertaDocente(idPeriodo,idTipoOferta));
	}
	
	@RequestMapping(value="/listarOfertaDocente/{idPeriodo}/{idDepartamento}/{idTipoOferta}", method=RequestMethod.GET) 
	public ResponseEntity<?> listarOfertaDocente(@PathVariable("idPeriodo") Integer idPeriodo,
			@PathVariable("idDepartamento") Integer idDepartamento,@PathVariable("idTipoOferta") Integer idTipoOferta) {
		return ResponseEntity.ok(docenteRepository.listarOfertaDocente(idPeriodo, idDepartamento,idTipoOferta));
	}

	
	@RequestMapping(value = "/grabarOfertaDocenteJson", method = RequestMethod.POST)
	public ResponseEntity<?> grabarOfertaDocenteJson(@RequestBody List<OfertaDocente> listaOfertaDoc) {
		personaService.grabaOfertaDocente(listaOfertaDoc);
		return ResponseEntity.ok().build();
	}
				
}
