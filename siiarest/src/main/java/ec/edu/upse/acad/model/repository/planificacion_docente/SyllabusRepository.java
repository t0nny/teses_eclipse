package ec.edu.upse.acad.model.repository.planificacion_docente;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ec.edu.upse.acad.model.pojo.planificacion_docente.Syllabus;

public interface SyllabusRepository extends JpaRepository<Syllabus, Integer> 
{

	@Query(value="select d.id, d.descripcion as nombre "
		+ "from Departamento d "
		+ "JOIN PersonaDepartamento pd on d.id=pd.id "
		+ "JOIN Persona p on pd.persona_id=p.id "
		+ "JOIN Usuario u on u.persona_id=p.id "
		+ "WHERE u.id = ?1")
	List<CO_As> getFacultades(Integer idUsuario);
	interface CO_As{
		Integer getId();
		String getNombre();
	}
	
	@Query(value="select o.id as idOferta, o.descripcion as descripcion "
			+ "from Oferta o "
			+ "JOIN DepartamentoOferta do on do.idOferta=o.id "
			+ "JOIN Departamento d on do.idDepartamento=d.id "
			+ "WHERE d.id = ?1")
	List<CO_Ofertas> getOfertas(Integer idDepartamento);
	interface CO_Ofertas{
		Integer getIdOferta();
		String getDescripcion();
	}
	
	@Query(value="select a.id as idAsignatura, a.descripcion as asignatura, "
			+ "ma.id as idMallaAsignatura, s.id as idSyllabus, "
			+ "s.estado as estado "
			+ "from Asignatura a "
			+ "JOIN OfertaAsignatura oa on oa.asignatura.id=a.id "
			+ "JOIN Oferta o on oa.oferta.id=o.id "
			+ "JOIN MallaAsignatura ma on ma.idAsignatura = a.id "
			+ "LEFT JOIN Syllabus s on s.idMallaAsignatura = ma.id "
			+ "WHERE o.id = ?1 ")
	List<CO_Asignaturas> getAsignaturas(Integer idOferta);
	interface CO_Asignaturas{
		Integer getIdAsignatura();
		String getAsignatura();
		Integer getIdMallaAsignatura();
		Integer getIdSyllabus();
		String getEstado();
	}
	
	//actualizados
	@Query(value="select distinct a.id as idAsignatura,a.descripcion as nombreAsignatura, "
			+ "ma.numHoras as numHoras, ma.numCreditos as creditos, "
			+ "n.descripcion as descripcionNivel,n.orden as ordenNivel, "
			+ "aa.valor as valor, "
			+ "ar.tipoRelacion as tipoRelacion, "
			+ "ca.codigo as componenteAprendizaje, "
			+ "co.descripcion as componenteOrganizacion,"
			+ "rv.valor as numeroSemanas "
			+ "from Asignatura a "
			+ "JOIN MallaAsignatura ma on ma.idAsignatura=a.id "
			+ "JOIN AsignaturaOrganizacion ao on ao.idMallaAsignatura = ma.id "
			+ "JOIN ComponenteOrganizacion co on ao.idCompOrganizacion = co.id "
			+ "JOIN TipoCompOrganizacion tco on co.idTipoCompOrganizacion = tco.id "
			+ "JOIN Nivel n on ma.idNivel = n.id "
			+ "JOIN AsignaturaAprendizaje aa on aa.idMallaAsignatura = ma.id "
			+ "JOIN ComponenteAprendizaje ca on aa.idComponenteAprendizaje = ca.id "
			+ "JOIN Malla m on ma.idMalla = m.id "
			+ "JOIN Reglamento r on m.idReglamento=r.id "
			+ "JOIN ReglamentoValidacion rv on r.id=rv.idReglamento "
			+ "JOIN AsignaturaRelacion ar on ar.idMallaAsignatura = ma.id "
			+ "JOIN PeriodoMalla pm on pm.idMalla = m.id "
			+ "JOIN PeriodoAcademico pa on pm.idPeriodoAcademico = pa.id "
			+ "JOIN DepartamentoOferta do on m.idDepartamentoOferta = do.id "
			+ "JOIN Oferta o on do.idOferta = o.id "
			+ "WHERE a.id = ?1 and o.id=?2 and ca.id=1 and tco.id=2  and rv.codigo='NUMSEMANASPAO'")
	List<CO_AsiSyllabus> getAsiSyllabus(Integer idAsignatura, Integer idOferta);
	interface CO_AsiSyllabus{
		Integer getIdAsignatura();
		String getNombreAsignatura();
		Integer getNumHoras();
		Integer getCreditos();
		String getDescripcionNivel();
		Integer getOrdenNivel();
		float getValor();
		String getTipoRelacion();
		String getComponenteAprendizaje();
		String getComponenteOrganizacion();
		Integer getNumeroSemanas();
	}

	@Query(value="select distinct a.id as idAsignatura,a.descripcion as nombreAsignatura, "
			+ " ma.numHoras as numHoras, ma.numCreditos as creditos, "
			+ " n.descripcion as descripcionNivel,n.orden as ordenNivel,ma.id as idMallaAsignatura,  "
			+ " co.descripcion as componenteOrganizacion, rv.valor as numeroSemanas "
			+ " from Asignatura a "
			+ " JOIN MallaAsignatura ma on ma.idAsignatura=a.id "
			+ " JOIN AsignaturaOrganizacion ao on ao.idMallaAsignatura = ma.id "
			+ " JOIN ComponenteOrganizacion co on ao.idCompOrganizacion = co.id "
			+ " JOIN TipoCompOrganizacion tco on co.idTipoCompOrganizacion = tco.id "
			+ " JOIN Nivel n on ma.idNivel = n.id "
			+ " JOIN Malla m on ma.idMalla = m.id "
			+ " JOIN Reglamento r on m.idReglamento=r.id "
			+ " JOIN ReglamentoValidacion rv on r.id=rv.idReglamento "
			+ " JOIN PeriodoMalla pm on pm.idMalla = m.id "
			+ " JOIN PeriodoAcademico pa on pm.idPeriodoAcademico = pa.id "
			+ " WHERE ma.id = (?1) and tco.abreviatura='UOC' and rv.codigo='NUMSEMANASPAO'")
	Silabos getSilabus(Integer idMallaAsignatura);
	interface Silabos{
		Integer getIdAsignatura();
		String getNombreAsignatura();
		Integer getNumHoras();
		Integer getCreditos();
		String getDescripcionNivel();
		Integer getOrdenNivel();
		Integer getIdMallaAsignatura();
		String getComponenteOrganizacion();
		Integer getNumeroSemanas();
	}
	
	
	@Query(value = "select distinct a.id as idAsignatura,a.descripcion as nombreAsignatura "
			+ "from Asignatura a "
			+ "JOIN MallaAsignatura ma on ma.idAsignatura=a.id "
			+ "JOIN AsignaturaOrganizacion ao on ao.idMallaAsignatura = ma.id "
			+ "JOIN ComponenteOrganizacion co on ao.idCompOrganizacion = co.id "
			+ "JOIN TipoCompOrganizacion tco on co.idTipoCompOrganizacion = tco.id "
			+ "JOIN Nivel n on ma.idNivel = n.id "
			+ "JOIN AsignaturaAprendizaje aa on aa.idMallaAsignatura = ma.id "
			+ "JOIN ComponenteAprendizaje ca on aa.idComponenteAprendizaje = ca.id "
			+ "JOIN Malla m on ma.idMalla = m.id "
			+ "JOIN AsignaturaRelacion ar on ar.idMallaAsignatura = ma.id "
			+ "JOIN PeriodoMalla pm on pm.idMalla = m.id "
			+ "JOIN PeriodoAcademico pa on pm.idPeriodoAcademico = pa.id "
			+ "JOIN DepartamentoOferta do on m.idDepartamentoOferta = do.id "
			+ "JOIN Oferta o on do.idOferta = o.id "
			+ "WHERE a.id = ?1 and o.id=?2 and ca.id=1 and tco.id=2")
	List<CO_MallaAsig> validaAccesSyllabus(Integer idAsignatura);
	interface CO_MallaAsig{
		Integer getIdAsignatura();
		String getNombreAsignatura();
	}
	
}
