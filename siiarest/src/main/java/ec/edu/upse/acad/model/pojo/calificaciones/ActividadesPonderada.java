package ec.edu.upse.acad.model.pojo.calificaciones;

import javax.persistence.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;



@Entity
@Table(schema="aca", name="actividades_ponderadas")
@NoArgsConstructor
public class ActividadesPonderada {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_actividades_ponderadas")
	@Getter @Setter private Integer id;

	@Getter @Setter private String estado;


	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Column(name="valor_porcentual")
	@Getter @Setter private int valorPorcentual;

	@Version
	@Getter @Setter private Integer version;

	//RELACIONES
	//bi-directional many-to-one association to ActividadesEvaluativa
	@ManyToOne
	@JoinColumn(name="id_actividades_evaluativas")
	@JsonIgnore
	@Getter @Setter private ActividadesEvaluativa actividadesEvaluativa;

	//bi-directional many-to-one association to CiclosPeriodo
	@ManyToOne
	@JoinColumn(name="id_ciclos_periodo")
	@JsonIgnore
	@Getter @Setter private CiclosPeriodo ciclosPeriodo;

	//bi-directional many-to-one association to PonderacionEvaluacion
	@ManyToOne
	@JoinColumn(name="id_ponderacion_evaluacion")
	@JsonIgnore
	@Getter @Setter private PonderacionEvaluacion ponderacionEvaluacion;
	
	@PrePersist
	void preInsert() {
	   if (this.estado == null)
	       this.estado = "A";
	}
}