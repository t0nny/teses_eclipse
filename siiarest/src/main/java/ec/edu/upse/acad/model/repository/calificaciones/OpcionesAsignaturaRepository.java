package ec.edu.upse.acad.model.repository.calificaciones;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.Docente;

import ec.edu.upse.acad.model.repository.distributivo.DistributivoDedicacionRepository.CustomObjectDOC;

@Repository
public interface OpcionesAsignaturaRepository extends JpaRepository<Docente, Integer>{
	@Query(value="select d.id as idDocente, CONCAT(p.apellidos ,' ',p.nombres) as nombresCompletos "+
			" from Persona p "+
			" inner join Docente d on p.id = d.idPersona "+
			" where p.estado='AC' and d.estado='A' "
			)
	List<CustomObjectDocente> listaDocente();
	interface CustomObjectDocente{ 
	    Integer getIdDocente();     
	    String getNombresCompletos();
	}
	
	
	@Query(value="select o.id as idOfertaAsignatura, o.descripcionCorta as ofertaAsignatura, a.descripcion as asignatura, "+
	"ma.id as idMallaAsignatura, CONCAT(cast(ni.orden as text) ,'/',par.descripcionCorta) as paralelo,par.id as idParalelo, daa.id as idDocenteAsignaturaAprend "+
	"from PeriodoAcademico pa "+
	"inner join DistributivoGeneral dg on pa.id=dg.idPeriodoAcademico "+
	"inner join DistributivoGeneralVersion dgv on dg.id=dgv.idDistributivoGeneral "+
	"inner join DistributivoOferta do on dgv.id=do.idDistributivoGeneralVersion "+
	"inner join DistributivoOfertaVersion dov on do.id=dov.idDistributivoOferta "+
	"inner join DistributivoDocente dd on dov.id=dd.idDistributivoOfertaVersion "+
	"inner join DocenteAsignaturaAprend daa on dd.id=daa.idDistributivoDocente "+
	"inner join Paralelo par on daa.idParalelo=par.id "+
	"inner join AsignaturaAprendizaje aa on daa.idAsignaturaAprendizaje=aa.id "+
	"inner join MallaAsignatura ma on aa.idMallaAsignatura=ma.id "+
	"inner join Malla m on ma.idMalla=m.id "+
	"inner join DepartamentoOferta dof on dof.id=m.idDepartamentoOferta "+
	"inner join Oferta o on dof.idOferta=o.id "+
	"inner join Nivel ni on ma.idNivel=ni.id "+
	"inner join Asignatura a on ma.idAsignatura=a.id "+
	"where pa.estado='A' and dg.estado='A' and dgv.estado='A' and do.estado='A' and dov.estado='A' and dd.estado='A' "+
	"and daa.estado='A' and aa.estado='A' and ma.estado='A' and a.estado='A' "+
	"and pa.id=(?1) and dd.idDocente=(?2) "+
	"and dov.versionDistributivoOferta in(select max(dov1.versionDistributivoOferta) "+
	"from DistributivoOferta do1 "+
	"inner join DistributivoOfertaVersion dov1 on do1.id=dov1.idDistributivoOferta "+
	"where do.id=do1.id "+
	"and do1.estado='A' and dov1.estado='A' "+
	"group by  do1.id ) "+
	"group by o.id, o.descripcionCorta , a.descripcion , "+
	"ma.id, par.id,ni.orden,par.descripcionCorta ,daa.id")
	List<CustomObjectDocenteAsignatura> listaDocenteAsignatura(Integer idPeriodo, Integer Idocente);
	interface CustomObjectDocenteAsignatura{ 
	    Integer getIdOfertaAsignatura();     
	    String getOfertaAsignatura();
	    String getAsignatura();
	    Integer getIdMallaAsignatura();
	    String getParalelo();
	    Integer getIdParalelo();
	    Integer getIdDocenteAsignaturaAprend();
	    
	}
	
	@Query(value="select o.id as idOfertaAsignatura, o.descripcion as ofertaAsignatura, ma.id as idMallaAsignatura, a.id as idAsignatura," + 
			"a.descripcion as asignatura, isnull(ma.numHoras,0) as numHoras, isnull( ma.numCreditos ,0) as numCreditos, " + 
			"n.descripcion as descripcionNivel,n.orden as ordenNivel, ar.tipoRelacion as tipoRelacion, " + 
			"co.descripcion as componenteOrganizacion,rv.valor as numeroSemanas " + 
			"from Asignatura a " + 
			"INNER JOIN MallaAsignatura ma on ma.idAsignatura=a.id " + 
			"INNER JOIN AsignaturaOrganizacion ao on ao.idMallaAsignatura = ma.id  " + 
			"INNER JOIN ComponenteOrganizacion co on ao.idCompOrganizacion = co.id " + 
			"INNER JOIN TipoCompOrganizacion tco on co.idTipoCompOrganizacion = tco.id " + 
			"INNER JOIN Nivel n on ma.idNivel = n.id " + 
			"INNER JOIN Malla m on ma.idMalla = m.id " + 
			"INNER JOIN Reglamento r on m.idReglamento=r.id "+
			"INNER JOIN ReglamentoValidacion rv on r.id=rv.idReglamento "+
			"INNER JOIN AsignaturaRelacion ar on ar.idMallaAsignatura = ma.id " + 
			"INNER JOIN PeriodoMalla pm on pm.idMalla = m.id " + 
			"INNER JOIN PeriodoAcademico pa on pm.idPeriodoAcademico = pa.id " + 
			"INNER JOIN DepartamentoOferta do on m.idDepartamentoOferta = do.id " + 
			"INNER JOIN Oferta o on do.idOferta = o.id " + 
			"WHERE a.estado='A' and ma.estado='A' and ao.estado='A' and co.estado='A' and tco.estado='A' and n.estado='A' " + 
			"and m.estado='A' and ar.estado='A' and pm.estado='A' and pa.estado='A' and do.estado='A' and o.estado='A' and " + 
			"pa.id=(?1) and o.id=(?2) and ma.id=(?3) and tco.abreviatura='UOC' and rv.codigo='NSPA' " + 
			"group by o.id, o.descripcion, ma.id ,a.id ,a.descripcion , " + 
			"ma.numHoras , ma.numCreditos , n.descripcion ,n.orden , ar.tipoRelacion , co.descripcion, " + 
			"rv.valor  ")
	List<CustomObjectResumenSilaboAsignatura> listaResumenSilaboAsig(Integer idPeriodo,Integer idOferta, Integer idMallaAsig);
	interface CustomObjectResumenSilaboAsignatura{ 
	    Integer getIdOfertaAsignatura();     
	    String getOfertaAsignatura();
	    Integer getIdMallaAsignatura();
	    Integer getIdAsignatura();
	    String getAsignatura();
	    Integer getNumHoras();
	    Integer getNumCreditos();
	    String getDescripcionNivel();
	    Integer getOrdenNivel();
	    String getTipoRelacion();
	    String getComponenteOrganizacion();
	    Integer getNumeroSemanas();
	}
	
	@Query(value = "SELECT ma.id as idMallaAsignatura,ma.numHoras as numHoras, "
			+ " ma.numCreditos as numCreditos,si.id as idSilabo,con.idContenidoPadre as idContenidoPadre, "
			+ " con.id as idContenido,con.descripcion as contenido,con.resultadoAprendizaje as resultadoAprendizaje, "
			+ " con.horasDocencia as horasDocencia, con.horasAEA as horasAea, con.horasTA as horasTa,(con.horasDocencia + con.horasAEA + con.horasTA ) as totalHora, con.orden as orden "
			+ " FROM MallaAsignatura ma  " 
			+ " INNER JOIN Syllabus si on ma.id=si.idMallaAsignatura "
			+ " INNER JOIN Contenido con on si.id=con.idSilabo " 
			+ " WHERE ma.id=(?1) and ma.estado ='A' and  si.estado ='A' and con.estado ='A' and con.idContenidoPadre is  null " 
			+ " ORDER BY con.orden  ")
	List<silabosList> listaSilaboAsigCab(Integer idMallaAsignatura);
	interface silabosList{ 
	    Integer getIdMallaAsignatura();
	    Integer getNumHoras();
	    Integer getNumCreditos();
	    Integer getIdSilabo();
	    Integer getIdContenido();
	    Integer getIdContenidoPadre();
	    String getContenido();
	    String getResultadoAprendizaje();
	    Integer getHorasDocencia();
	    Integer getHorasAea();
	    Integer getHorasTa();
	    Integer getTotalHora();
	    Integer getOrden();
	}
	
	@Query(value = "SELECT ma.id as idMallaAsignatura,ma.numHoras as numHoras, "
			+ " ma.numCreditos as numCreditos,si.id as idSilabo,con.idContenidoPadre as idContenidoPadre, "
			+ " con.id as idContenido,con.descripcion as contenido,con.resultadoAprendizaje as resultadoAprendizaje, "
			+ " con.horasDocencia as horasDocencia, con.horasAEA as horasAea, con.horasTA as horasTa,(con.horasDocencia + con.horasAEA + con.horasTA ) as totalHora, con.orden as orden "
			+ " FROM MallaAsignatura ma  " 
			+ " INNER JOIN Syllabus si on ma.id=si.idMallaAsignatura "
			+ " INNER JOIN Contenido con on si.id=con.idSilabo " 
			+ " WHERE ma.id=(?1) and ma.estado ='A' and  si.estado ='A' and con.estado ='A' and con.idContenidoPadre is not null " 
			+ " ORDER BY con.orden  ")
	List<silabosList> listaSilaboAsigDet(Integer idMallaAsig);

}
