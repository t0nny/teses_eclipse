package ec.edu.upse.acad.model.pojo;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="aca", name="planificacion_paralelo")
@NoArgsConstructor
public class PlanificacionParalelo {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_planificacion_paralelo")
	@Getter @Setter private Integer id;
	
	@Column(name="id_periodo_academico")
	@Getter @Setter private Integer idPeriodoAcademico;
	
	@Column(name="id_malla_asignatura")
	@Getter @Setter private Integer idMallaAsignatura;
	 
	@Column(name="num_paralelos")
	@Getter @Setter private Integer numParalelos;
	
	@Column(name="num_estudiantes")
	@Getter @Setter private String numEstudiantes;
	
    @Getter @Setter private String estado;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;

	//RELACIONES
	//bi-directional many-to-one association to PeriodoAcademico
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_periodo_academico" , insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private PeriodoAcademico periodoAcademico;
	
	//bi-directional many-to-one association to Malla
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_malla_asignatura" , insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private MallaAsignatura mallaAsignatura;
		
	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }
}
