package ec.edu.upse.acad.model.pojo.vinculacion;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema = "vin", name = "proyecto_presupuesto")

@NoArgsConstructor //un constructorsin argumentos
public class ProyectoPresupuesto {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_proyecto_presupuesto")
	@Getter	@Setter	private Integer id;

	@Column(name = "aporte_universidad")
	@Getter	@Setter	private String aporteUniversidad;
	
	@Column(name = "entidad_auspiciante")
	@Getter	@Setter	private String entidadAuspiciante;
	
	@Column(name = "comunidad_beneficiaria")
	@Getter	@Setter	private String comunidadBeneficiaria;
	
	@Getter	@Setter	private String autogestion;
	
	@Column(name = "usuario_ingreso_id")
	@Getter	@Setter	private String usuarioIngresoId;

	@Version
	@Getter	@Setter	private Integer version;

	@Getter	@Setter	private String estado;
	
	// bi-directional one_to_many association to proyecto
	@ManyToOne
	@JoinColumn(name = "id_proyecto", nullable = false, updatable = false)
	@JsonBackReference
	@Getter	@Setter	private Proyecto proyecto;
	
	@PrePersist
	void preInsert() {
		if (this.estado == null)
			this.estado = "A";
	}


}
