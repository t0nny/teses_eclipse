package ec.edu.upse.acad.model.repository.matricula;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.matricula.ReglamentoTipoMatricula;


@Repository
public interface ReglamentoTipoMatriculaRepository extends JpaRepository<ReglamentoTipoMatricula, Integer>{

	@Query(value=" select r.id as idReglamento,r.nombre as nombreReglamento,r.reglamentoNacional as reglamentoNacional," + 
			" r.fechaAprobacionIes as fechaAprobacionIes, tr.id as idTipoReglamento,tr.descripcion as tipoReglamento from Reglamento r " + 
			" inner join TipoReglamento tr on tr.id = r.idTipoReglamento")
	List<CustomObjectReglamentos> listarTodosReglamentos();

	interface CustomObjectReglamentos{ 
		Integer getIdReglamento(); 
		String getNombreReglamento(); 
		String getReglamentoNacional();
		Date getFechaAprobacionIes();
		Integer getIdTipoReglamento();
		String getTipoReglamento();
	} 
	
	@Query(value=" select tr.id as idTipoReglamento, tr.codigo as codigoTipo,tr.descripcion as descripcionTipo  from TipoReglamento tr ")
	List<CustomObjectTiposReglamento> listarTipoReglamentos();

	interface CustomObjectTiposReglamento{ 
		Integer getIdTipoReglamento();
		String getCodigoTipo();
		String getDescripcionTipo();
	} 
	
	@Query(value=" select r.id as id,r.idTipoReglamento as idTipoReglamento,r.nombre as nombre,r.reglamentoNacional as reglamentoNacional, " + 
			"	r.fechaHasta as fechaHasta,r.fechaAprobacionIes as fechaAprobacionIes,r.usuarioIngresoId as usuarioIngresoId,r.estado as estado,r.version as version " + 
			"	from Reglamento r where r.id = (?1) ")
	List<CustomObjectReglamento> recuperarReglamentoId(Integer idReglamento);

	interface CustomObjectReglamento{ 
		Integer getId();
		Integer getIdTipoReglamento();
		String getNombre();
		String getReglamentoNacional();
		Date getFechaHasta();
		Date getFechaAprobacionIes();
		String getUsuarioIngresoId();
		String getEstado();
		Integer getVersion();
	} 
	
	@Query(value=" select tip.id as idTipoOferta,tip.descripcion as descripcion,tip.descripcionCorta as codigoTipoOferta from TipoOferta tip")
	List<CustomObjectTipoOferta> listarTipoOfertas();

	interface CustomObjectTipoOferta{ 
		Integer getIdTipoOferta();
		String getDescripcion();
		String getCodigoTipoOferta();
	} 



	





}
