package ec.edu.upse.acad.model.pojo.distributivo;

import javax.persistence.*;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ec.edu.upse.acad.model.pojo.PeriodoAcademico;
import ec.edu.upse.acad.model.pojo.reglamento.Reglamento;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Date;

import java.util.List;

@Entity
@Table(schema="aca", name="distributivo_general")
@Where(clause = "estado='A'")
@NoArgsConstructor
public class DistributivoGeneral{

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_distributivo_general")
	@Getter @Setter private Integer id;

	@Column(name="id_reglamento")
	@Getter @Setter private Integer idReglamento;

	@Column(name="id_periodo_academico")
	@Getter @Setter private Integer idPeriodoAcademico;

	@Getter @Setter private String descripcion;

	@Column(name="fecha_desde")
	@Getter @Setter private Date fechaDesde;

	@Column(name="fecha_hasta")
	@Getter @Setter private Date fechaHasta;

	@Getter @Setter private String estado;

//	@Column(name="fecha_ingreso")
//	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngreso;

	@Version
	@Getter @Setter private Integer version;


	//RELACIONES


	//bi-directional many-to-one association to Reglamento
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_reglamento", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Reglamento reglamento;

	//bi-directional many-to-one association to Reglamento
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_periodo_academico", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private PeriodoAcademico periodoAcademico;
	
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="id_distributivo_general", insertable=false, updatable = false)
//	@JsonIgnore
//	@Getter @Setter private DistributivoGeneralVersion distributivoGeneral;

	//bi-directional many-to-one association to Distributivo Oferta Version
	@OneToMany(mappedBy="distributivoGeneral", cascade=CascadeType.ALL)
	@Getter @Setter private List<DistributivoGeneralVersion> distributivoGeneralVersions;



	@PrePersist
	void preInsert() {
		if (this.estado == null)
			this.estado = "A";
	}

}