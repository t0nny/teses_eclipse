package ec.edu.upse.acad.model.repository.vinculacion;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.vinculacion.ProyectoConvocatoria;

@Repository
public interface ProyectoConvocatoriaRepository extends JpaRepository<ProyectoConvocatoria, Integer> {
	@Transactional
	// solo Listado de docentes peertenecientes a una oferta
		@Query(value = "SELECT pc.id as id,pc.idPeriodoAcademico as idPeriodoAcademico, pc.descripcion as descripcion, "
				+ "pc.fechaDesde as fechaDesde, pc.fechaHasta as fechaHasta, pc.estado as estado,pa.codigo as descripcionPeriodo "
				+ "from ProyectoConvocatoria pc "
				+ "INNER JOIN PeriodoAcademico pa on pc.idPeriodoAcademico=pa.id "
				+ "WHERE pc.estado='A' ")
		List<customObjetPeriodoConvocatoria> buscarPeriodoConvocatoria();

		interface customObjetPeriodoConvocatoria {
			Integer getId();
			Integer getIdPeriodoAcademico();
			String getDescripcion();
			Date getFechaDesde();
			Date getFechaHasta();
			String getEstado();
			String getDescripcionPeriodo();
		}
		
		@Query(value = "SELECT TOP 1 pc.id_proyecto_convocatoria as id,pc.id_periodo_academico as idPeriodoAcademico, pc.descripcion as descripcion, "
				+ "pc.fecha_desde as fechaDesde, pc.fecha_hasta as fechaHasta, pc.estado as estado "
				+ "FROM vin.proyecto_convocatoria as pc WHERE pc.estado='A' ORDER BY pc.id_proyecto_convocatoria DESC ",nativeQuery = true)
		Optional<customObjetUltimaConvocatoria> buscarUltimaConvocatoria();

		interface customObjetUltimaConvocatoria {
			Integer getId();
			Integer getIdPeriodoAcademico();
			String getDescripcion();
			Date getFechaDesde();
			Date getFechaHasta();
			String getEstado();
		}

		
		// solo Listado de docentes peertenecientes a una oferta
		@Query(value = "SELECT distinct(dpo.idDepartamento) as idDepartamento,d.nombre as nombre from DistributivoGeneral dg "
				+ "INNER JOIN DistributivoGeneralVersion dgv on dg.id= dgv.idDistributivoGeneral "
				+ "INNER JOIN DistributivoOferta do on dgv.id = do.idDistributivoGeneralVersion "
				+ "INNER JOIN Oferta o on do.idOferta=o.id "
				+ "INNER JOIN DepartamentoOferta dpo on o.id=dpo.idOferta "
				+ "INNER JOIN Departamento d on dpo.idDepartamento=d.id "
				+ "INNER JOIN DistributivoOfertaVersion dov on do.id=dov.idDistributivoOferta "
				+ "INNER JOIN DistributivoDocente dd on dov.id=dd.idDistributivoOfertaVersion "
				+ "INNER JOIN Docente doc on dd.idDocente=doc.id "
				+ "INNER JOIN Persona p on doc.idPersona=p.id "
				+ "WHERE dg.idPeriodoAcademico=(?1) and p.identificacion=(?2) and d.estado='AC'" )
		Optional<customObjetDocentes> facultad(Integer idPeridoAcademico, String cedula);

		interface customObjetDocentes {
			Integer getIdDepartamento();
			String getNombre();
			

		}
		
		@Query(value = "SELECT dpo.id as id,o.descripcion as descripcion from DistributivoGeneral dg "
				+ "INNER JOIN DistributivoGeneralVersion dgv on dg.id= dgv.idDistributivoGeneral "
				+ "INNER JOIN DistributivoOferta do on dgv.id = do.idDistributivoGeneralVersion "
				+ "INNER JOIN Oferta o on do.idOferta=o.id "
				+ "INNER JOIN DepartamentoOferta dpo on o.id=dpo.idOferta "
				+ "WHERE dg.idPeriodoAcademico=(?1) and dpo.idDepartamento=(?2) and o.estado='A' ")
		List<customObjetcargarCarreras> cargarCarreras(Integer idPeridoAcademico, Integer idDepartamento);

		interface customObjetcargarCarreras {
			Integer getId();
			String getDescripcion();
			

		}
		
		@Query(value = "select p.identificacion as identificacion,u.id as idUsuario,tup.id_tipo_usuario_proyecto as idTipoUsuarioProyecto, "
				+ "tup.codigo as codigo, tup.descripcion as descripcion,up.id_usuario_proyecto as idUsuarioProyecto from vin.tipo_usuario_proyecto tup "
				+ "INNER JOIN vin.usuario_proyecto up on tup.id_tipo_usuario_proyecto= up.id_tipo_usuario_proyecto "
				+ "INNER JOIN seg.usuarios u on up.id_usuario=u.id "
				+ "INNER JOIN man.personas p on p.id=u.persona_id "
				+ "WHERE u.id=(:id_Usuario) and u.estado='AC' and up.id_usuario_proyecto=(select min(up.id_usuario_proyecto) from  vin.tipo_usuario_proyecto tup"
				+ "INNER JOIN vin.usuario_proyecto up on tup.id_tipo_usuario_proyecto= up.id_tipo_usuario_proyecto "
				+ "INNER JOIN seg.usuarios u on up.id_usuario=u.id "
				+ "INNER JOIN man.personas p on p.id=u.persona_id "				
				+ "WHERE u.id=(:id_Usuario) and u.estado='AC') ", nativeQuery = true)
		public List<CustomObjectcargarTipoUsuario> cargarTipoUsuario(@Param("id_Usuario") Integer idUsuario);

		public interface CustomObjectcargarTipoUsuario {
			Integer getIdUsuario();
			Integer getIdTipoUsuarioProyecto();
			Integer getIdUsuarioProyecto();
			String getCodigo();
			String getIdentificacion();
			String getDescripcion();
			
		}
		
		//retorna la fecha fin de la convocatoria
		@Query(value = "SELECT TOP 1 pc.fecha_hasta as fechaHasta "
				+ "from vin.proyecto_convocatoria pc "
				+ "WHERE pc.estado='A' "
				+ "ORDER BY pc.id_proyecto_convocatoria desc ", nativeQuery = true)
		
		Optional<fechaFinConvocatoria> buscarFechaFinConvocatoria();

		interface fechaFinConvocatoria {
			Date getFechaHasta();
		}

		
}