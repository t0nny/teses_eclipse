package ec.edu.upse.acad.model.pojo.seguridad;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Date;
import java.sql.Timestamp;


@Entity
@Table(schema="man", name="personas_cargos")
@NoArgsConstructor
public class PersonasCargo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Getter @Setter private Integer id;
	
	@Column(name="cargo_id")
	@Getter @Setter private Integer cargoId;
	
	@Column(name="persona_id")
	@Getter @Setter private Integer personaId;

	@Getter @Setter private String estado;

	@Column(name="fecha_ing")
	@Getter @Setter private Timestamp fechaIng;

	@Column(name="fecha_mod")
	@Getter @Setter private Timestamp fechaMod;

	@Getter @Setter private Date fin;

	@Getter @Setter private Date inicio;

	@Column(name="usuario_ing")
	@Getter @Setter private String usuarioIng;

	@Column(name="usuario_mod")
	@Getter @Setter private String usuarioMod;

	@Version
	@Getter @Setter private Integer version;

	@Getter @Setter private boolean vigente;

	//RELACIONES
	
	//bi-directional many-to-one association to DepartamentosCargo
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cargo_id", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private DepartamentosCargo departamentosCargo;
	
	//bi-directional many-to-one association to Persona
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="persona_id", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Persona persona;
	
	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "AC";
	 }

}