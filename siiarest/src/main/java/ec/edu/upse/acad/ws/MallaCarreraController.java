package ec.edu.upse.acad.ws;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.upse.acad.model.pojo.AsignaturaOrganizacion;
import ec.edu.upse.acad.model.pojo.AsignaturaAprendizaje;
import ec.edu.upse.acad.model.pojo.Malla;
import ec.edu.upse.acad.model.pojo.MallaAsignatura;
import ec.edu.upse.acad.model.pojo.MallaRequisito;

import ec.edu.upse.acad.model.repository.AsignaturaOrganizacionRepository;
import ec.edu.upse.acad.model.repository.AsignaturaRepository;
import ec.edu.upse.acad.model.repository.AsignaturaAprendizajeRepository;
import ec.edu.upse.acad.model.repository.MallaAsignaturaRepository;
import ec.edu.upse.acad.model.repository.MallaRepository;
import ec.edu.upse.acad.model.repository.MallaRequisitoRepository;
import ec.edu.upse.acad.model.repository.OfertaRepository;
import ec.edu.upse.acad.model.repository.RequisitoRepository;
import ec.edu.upse.acad.model.repository.distributivo.ReglamentoRepository;
import ec.edu.upse.acad.model.service.MallasService;
import ec.edu.upse.acad.model.service.PersonaService;
import ec.edu.upse.acad.model.service.UpdateForIdService;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping("/api/mallas")
@CrossOrigin

public class MallaCarreraController {

	@Autowired
	private AsignaturaRepository asignaturaRepository;
	@Autowired
	private MallaAsignaturaRepository mallaasignaturaRepository;
	@Autowired
	private MallaRepository mallaRepository;
	@Autowired
	private PersonaService personaService;
	@Autowired
	private MallasService mallasService;
	@Autowired
	private UpdateForIdService mallaasiganturaService;
	@Autowired
	private AsignaturaOrganizacionRepository asignaturaOrganizacionRepository;
	@Autowired
	private AsignaturaAprendizajeRepository asignaturaAprendizajeRepository;
	@Autowired
	private MallaRequisitoRepository mallaRequisitoRepository;
	@Autowired
	private RequisitoRepository requisitoRepository;
	@Autowired
	private ReglamentoRepository reglamentoRepository;
	@Autowired
	private OfertaRepository ofertaRepository;

	@RequestMapping(value = "/buscarMaterias", method = RequestMethod.GET)
	public ResponseEntity<?> buscarMaterias() {
		return ResponseEntity.ok(asignaturaRepository.findAll());
	}

	// Buscador de malla asignatura que existe en asignatura version pero que no exista en malla asignatura.
	@RequestMapping(value = "/reglamento", method = RequestMethod.GET)
	public ResponseEntity<?> reglamento() {
		return ResponseEntity.ok(mallaRepository.buscarReglamento());
	}

	@RequestMapping(value = "/buscarPeriodoDesde/{fechaDesde}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarPeriodoDesde(@PathVariable("fechaDesde") Date fechaDesde) {
		return ResponseEntity.ok(mallaRepository.listaPeriodoDesde(fechaDesde));
	}

	// Buscador de malla asignatura que existe en asignatura version pero que no exista en malla asignatura.
	@RequestMapping(value = "/buscarPorNombreAsignatura/{id}/{nombre}/{idOferta}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarPorNombreAsignatura(@PathVariable("id") Integer id, @PathVariable("nombre") String nombre,
			@PathVariable("idOferta") Integer idOferta) {
		if (nombre.equals("A")) {
			nombre = "";
		}
		return ResponseEntity.ok(mallaasignaturaRepository.listaAsignaturaOfertanombre(id, "%" + nombre + "%", idOferta));
	}

	@RequestMapping(value = "/buscarReglamentoRegimenAcademico", method = RequestMethod.GET)
	public ResponseEntity<?> buscarReglamentoRegimenAcademico() {
		return ResponseEntity.ok(reglamentoRepository.reglamentoRegimenAcademico());
	}

	@RequestMapping(value = "/buscarReglamento/{idReglamento}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarReglamento(@PathVariable("idReglamento") Integer idReglamento) {
		return ResponseEntity.ok(reglamentoRepository.buscarReglamento(idReglamento));
	}

	@RequestMapping(value = "/buscarOfertaPorTipoOfertaDepartamento/{idDepartamento}/{idTipoOferta}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarOfertaPorTipoOfertaDepartamento(@PathVariable("idDepartamento") Integer idDepartamento,
			@PathVariable("idTipoOferta") Integer idTipoOferta) {
		return ResponseEntity.ok(ofertaRepository.listaOfertaDepartamentoTipoOferta(idDepartamento, idTipoOferta));
	}

	// Lista las ofertas asociadas a una malla
	@RequestMapping(value = "/filtrarOfertaPorFacultades", method = RequestMethod.GET)
	public ResponseEntity<?> filtrarOfertaPorFacultades() {
		return ResponseEntity.ok(mallaasignaturaRepository.listaDepartamento());
	}

	// Lista las ofertas asociadas a una malla
	@RequestMapping(value = "/filtrarOfertaPorFacultadesTodos", method = RequestMethod.GET)
	public ResponseEntity<?> filtrarOfertaPorFacultadesTodos() {
		return ResponseEntity.ok(mallaasignaturaRepository.listaMallaCarreraTodos());
	}

	// lista las materias por oferta y por malla version
	@RequestMapping(value = "/filtrarPorAsignaturaOferta/{idMalla}/{idOferta}", method = RequestMethod.GET)
	public ResponseEntity<?> filtrarPorAsignaturaOferta(@PathVariable("idMalla") Integer idMalla, @PathVariable("idOferta") Integer idOferta) {
		return ResponseEntity.ok(mallaasignaturaRepository.listaAsignaturaOferta(idMalla, idOferta));
	}

	// Lista las materias y las presenta agrupadas por nivel(semestre o years) en el 1er kanban de asginacion de UOC - AB
	@RequestMapping(value = "/filtrarPorMallaNivel/{idMalla}", method = RequestMethod.GET)
	public ResponseEntity<?> filtrarPorMallaNivel(@PathVariable("idMalla") Integer idMalla) {
		return ResponseEntity.ok(mallaasignaturaRepository.listaMallaNivel(idMalla));
	}

	// Lista las materias agrupadas por niveles (years y semestre) y las ubica en su correspondiente UOC - AB

	@RequestMapping(value = "/listarNivelesUnidades/{idMalla}", method = RequestMethod.GET)
	public ResponseEntity<?> listarNivelesUnidades(@PathVariable("idMalla") Integer idMalla) {
		return ResponseEntity.ok(asignaturaOrganizacionRepository.listarNivelesUnidades(idMalla));
	}

	@RequestMapping(value = "/filtrarPorMallaAsignatura/{idMalla}", method = RequestMethod.GET)
	public ResponseEntity<?> filtrarPorMallaAsignatura(@PathVariable("idMalla") Integer idMalla) {
		return ResponseEntity.ok(mallaasignaturaRepository.listaMallaAsignatura(idMalla));
	}

	// Listas materias asigandas a un nivel(semestre o a�o) por idmallaversion y idnivel AB
	@RequestMapping(value = "/filtrarPorMallaAsignaturaNivel/{id}/{idNiv}", method = RequestMethod.GET)
	public ResponseEntity<?> filtrarPorMallaAsignaturaNivel(@PathVariable("id") Integer idOferta,
			@PathVariable("idNiv") Integer idNivel) {
		return ResponseEntity.ok(mallaasignaturaRepository.listaMallaAsignaturaNivel(idOferta, idNivel));
	}

	// Listas materias asigandas a un nivel(semestre o a�o) por idmallaversion y
	// idnivel - Solo si existen en asignatura organizacion AB
	@RequestMapping(value = "/filtrarPorMallaAsignaturaNivelAo/{id}/{idNiv}", method = RequestMethod.GET)
	public ResponseEntity<?> filtrarPorMallaAsignaturaNivelAo(@PathVariable("id") Integer idOferta,
			@PathVariable("idNiv") Integer idNivel) {
		return ResponseEntity.ok(mallaasignaturaRepository.listaMallaAsignaturaNivelAo(idOferta, idNivel));
	}

	@RequestMapping(value = "/grabarMallaEstado", method = RequestMethod.POST)
	public ResponseEntity<?> grabarMallaEstado(@RequestBody Malla malla) {
		mallasService.estadoMalla(malla);
		return ResponseEntity.ok().build();
	}
	
	/**
	 * Servicio que elimina una malla con su periodos relacionados
	 ***/
	@RequestMapping(value = "/eliminarMalla", method = RequestMethod.POST)
	public ResponseEntity<?> eliminarMalla(@RequestBody Malla malla) {
		mallasService.eliminarMalla(malla);
		return ResponseEntity.ok().build();
	}

	@RequestMapping(value = "/borrarMallaVersion/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> borrarMallaVersion(@PathVariable("id") Integer id) {
		personaService.borraAsign(id);
		return ResponseEntity.ok().build();
	}

	// eliminar de malla_asignatura
	@RequestMapping(value = "/borrarMallaAsignatura/{idMallaAsignatura}", method = RequestMethod.DELETE)
	public ResponseEntity<?> borrarMallaAsignatura(@PathVariable("idMallaAsignatura") Integer idMallaAsignatura) {
		mallaasiganturaService.borrarMallaAsignatura(idMallaAsignatura);
		return ResponseEntity.ok().build();
	}

	// eliminar de asignatura aprendizaje
	@RequestMapping(value = "/borrarasiganturaOrganizacion/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> borrarasiganturaOrganizacion(@PathVariable("id") Integer id) {
		mallaasiganturaService.borrarAsignaturaOrganizacion(id);
		return ResponseEntity.ok().build();
	}

	// Servicio para listar por id los componentes de organizacion a los que pertenece una asignatura

	@RequestMapping(value = "/filtrarporcampoformacion/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> filtrarporcampoformacion(@PathVariable("id") Integer idCampFor) {
		return ResponseEntity.ok(asignaturaOrganizacionRepository.filtrarporcampoformacion(idCampFor));
	}

	// Servicios para Grabar Malla Asignatura Organizacion - components de formaci�n
	@RequestMapping(value = "/grabarAsignaturaOrganizacion", method = RequestMethod.POST)
	public ResponseEntity<?> grabarAsignaturaOrganizacion(@RequestBody AsignaturaOrganizacion asigOrg) {
		AsignaturaOrganizacion _asigOrg = new AsignaturaOrganizacion();
		if (asigOrg.getId() != null) {
			_asigOrg = asignaturaOrganizacionRepository.findById(asigOrg.getId()).get();
		}
		BeanUtils.copyProperties(asigOrg, _asigOrg);
		asignaturaOrganizacionRepository.save(_asigOrg);
		return ResponseEntity.ok(_asigOrg);
	}

	// //Lista las materias que no tienen un campo de formacion asigando AB
	@RequestMapping(value = "/filtrarMateriasCampFormacion/{id}/{idOferta}", method = RequestMethod.GET)
	public ResponseEntity<?> filtrarMateriasCampFormacion(@PathVariable("id") Integer idCampForLista, @PathVariable("idOferta") Integer idOferta) {
		return ResponseEntity.ok(asignaturaOrganizacionRepository.filtrarmateriasporcamposdeformacion(idCampForLista, idOferta));
	}

	//Servicio para filtrar la malla componente aprendizaje
	@RequestMapping(value = "/filtrarmallacomponenteaprendizaje/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> filtrarmallacomponenteaprendizaje(@PathVariable("id") Integer idCompo) {
		return ResponseEntity.ok(asignaturaOrganizacionRepository.filtrarmallacomponenteaprendizaje(idCompo));
	}

	@RequestMapping(value = "/grabarComponenteAsignatura", method = RequestMethod.POST)
	public ResponseEntity<?> grabarComponenteAsignatura(@RequestBody AsignaturaAprendizaje compAsig) {
		AsignaturaAprendizaje _compAsig = new AsignaturaAprendizaje();
		if (compAsig.getId() != null) {
			_compAsig = asignaturaAprendizajeRepository.findById(compAsig.getId()).get();
		}
		BeanUtils.copyProperties(compAsig, _compAsig);
		asignaturaAprendizajeRepository.save(_compAsig);
		return ResponseEntity.ok(_compAsig);
	}

	// Servicio Malla para cargar la pantalla editar malla//
	@RequestMapping(value = "/buscarPorMallasId/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarPorMallasId(@PathVariable("id") Integer id) {
		Malla mallas = mallaRepository.findById(id).get();
		mallas.setMallaAsignaturas(null);
		return ResponseEntity.ok(mallas);
	}

	@RequestMapping(value = "/buscarPorMallasAsigId/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarPorMallasAsigId(@PathVariable("id") Integer id) {
		return ResponseEntity.ok(mallaasignaturaRepository.listaCalculoAsignaturaMalla(id));
	}

	@RequestMapping(value = "/listaMallaUtilizadaDistributivo/{idMalla}", method = RequestMethod.GET)
	public ResponseEntity<?> listaMallaUtilizadaDistributivo(@PathVariable("idMalla") Integer idMalla) {
		return ResponseEntity.ok(mallaasignaturaRepository.listaMallaUtilizadaDistributivo(idMalla));
	}

	/***
	 * Servicio para filtrar por la oferta la malla version por oferta y periodo
	 * academico
	 * 
	 * @param authorization
	 * @param idOferta
	 * @param idPeriodo
	 * @return
	 */

	@RequestMapping(value = "/filtrarMallasVersionyPeriodo/{idOferta}/{idPeriodo}", method = RequestMethod.GET)
	public ResponseEntity<?> filtrarMallasVersionyPeriodo(@PathVariable("idOferta") Integer idOferta, @PathVariable("idPeriodo") Integer idPeriodo) {
		return ResponseEntity.ok(mallaRepository.filtrarMallasVersionyPeriodo(idOferta, idPeriodo));
	}

	/***
	 * Servicio para filtrar por la oferta la malla version por tipo oferta y
	 * periodo academico
	 * 
	 * @param authorization
	 * @param idTipoOferta
	 * @param idPeriodo
	 * @return
	 * 
	 */

	@RequestMapping(value = "/filtrarMallasVersionPorPeriodoTipoOferta/{idTipoOferta}/{idPeriodo}/{idDepartamento}", method = RequestMethod.GET)
	public ResponseEntity<?> filtrarMallasVersionPorPeriodoTipoOferta(@PathVariable("idTipoOferta") Integer idTipoOferta, 
			@PathVariable("idPeriodo") Integer idPeriodo,@PathVariable("idDepartamento") Integer idDepartamento) {
		return ResponseEntity.ok(mallaRepository.filtrarMallasVersionPorPeriodoTipoOferta(idTipoOferta, idPeriodo, idDepartamento));
	}

	// Servicio para filtrar por la malla por idmallaversion//
	@RequestMapping(value = "/filtrarMallasVersionId/{idMallaVersion}", method = RequestMethod.GET)
	public ResponseEntity<?> filtrarMallasVersionId(@PathVariable("idMallaVersion") Integer idMallaVersion) {
		return ResponseEntity.ok(mallaRepository.filtrarMallasVersionId(idMallaVersion));
	}

	@RequestMapping(value = "/filtrar/{idMallaVersion}", method = RequestMethod.GET)
	public ResponseEntity<?> filtrar(@PathVariable("idMallaVersion") Integer idMallaVersion) {
		return ResponseEntity.ok(mallaRequisitoRepository.filtrarMallaRequisitoIdMalla(idMallaVersion));
	}

	@RequestMapping(value = "/filtrarMallasRequisitoId/{idMallaVersion}", method = RequestMethod.GET)
	public ResponseEntity<?> filtrarMallasRequisitoId(@PathVariable("idMallaVersion") Integer idMallaVersion) {
		return ResponseEntity.ok(mallaRequisitoRepository.filtrarMallaRequisitoId(idMallaVersion));
	}

	// Buscar todos las buscarRequisitos, para listar en angular
	@RequestMapping(value = "/listarRequisitos", method = RequestMethod.GET)
	public ResponseEntity<?> listarRequisitos() {
		return ResponseEntity.ok(requisitoRepository.listarRequisitos());
	}

	@RequestMapping(value = "/grabarMallaRequisito", method = RequestMethod.POST)
	public ResponseEntity<?> grabarMallaRequisito(@RequestBody MallaRequisito mallaRequisito) {
		MallaRequisito _mallaRequisito = new MallaRequisito();
		if (mallaRequisito.getId() != null) {
			_mallaRequisito = mallaRequisitoRepository.findById(mallaRequisito.getId()).get();
		}
		BeanUtils.copyProperties(mallaRequisito, _mallaRequisito);
		System.out.println(mallaRequisito.toString());
		mallaRequisitoRepository.save(_mallaRequisito);
		return ResponseEntity.ok(_mallaRequisito);
	}

	@RequestMapping(value = "/borrarMallaRequisito/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> borrarMallaRequisito(@PathVariable("id") Integer id) {
		MallaRequisito mallaRequisito = mallaRequisitoRepository.findById(id).get();
		MallaRequisito _mallaRequisito = new MallaRequisito();
		if (mallaRequisito != null) {
			BeanUtils.copyProperties(mallaRequisito, _mallaRequisito);
			_mallaRequisito.setEstado("I");
			mallaRequisitoRepository.save(_mallaRequisito);
		} else {
			System.out.println("Vacio");
		}
		return ResponseEntity.ok().build();
	}

	@RequestMapping(value = "/buscarMallaRequisitoIdMallaIdRequisito/{idMalla}/{idRequisito}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarMallaRequisitoIdMallaIdRequisito(@PathVariable("idMalla") Integer idMalla,
			@PathVariable("idRequisito") Integer idRequisito) {
		return ResponseEntity.ok(mallaRequisitoRepository.buscarMallaRequisitoIdMallaIdRequisito(idMalla, idRequisito));
	}

	/***
	 * Servicio para la pantalla asignar niveles a las asignaturas
	 */
	@RequestMapping(value = "/buscarMallaRequisitoId/{idMallaRequisito}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarMallaRequisitoId(@PathVariable("idMallaRequisito") Integer idRequisito) {
		return ResponseEntity.ok(mallaRequisitoRepository.findById(idRequisito).get());
	}

	/***
	 * Servicio que recupera el nombre de la facultad y la carrera a través del
	 * id_oferta
	 */
	@RequestMapping(value = "/recuperarNombresCarreraFacultad/{idMalla}", method = RequestMethod.GET)
	public ResponseEntity<?> recuperarNombresCarreraFacultad(@PathVariable("idMalla") Integer idMalla) {
		return ResponseEntity.ok(ofertaRepository.recuperarNombresCarreraFacultad(idMalla));
	}

	/***
	 * Servicio que lista las asignaturas de la malla para interfaz de componentes
	 * de aprendizaje
	 */
	@RequestMapping(value = "/listaMallaAsignaturaParaInterfazComponente/{idMalla}", method = RequestMethod.GET)
	public ResponseEntity<?> listaMallaAsignaturaParaInterfazComponente(@PathVariable("idMalla") Integer idMalla) {
		return ResponseEntity.ok(mallaasignaturaRepository.listaMallaAsignaturaParaInterfazComponente(idMalla));
	}

	/***
	 * Servicio que lista las asignaturas aprendizaje de la malla
	 */
	@RequestMapping(value = "/listarAsignaturasAprendizajeMalla/{idMalla}", method = RequestMethod.GET)
	public ResponseEntity<?> listarAsignaturasAprendizajeMalla(@PathVariable("idMalla") Integer idMalla) {
		return ResponseEntity.ok(mallaasignaturaRepository.listarAsignaturasAprendizajeMalla(idMalla));
	}

	/***
	 * Servicio que lista las asignaturas aprendizaje de la malla asociadas a un
	 * distributivo
	 */
	@RequestMapping(value = "/listarAsignaturasAprendizajeDistributivo/{idMalla}", method = RequestMethod.GET)
	public ResponseEntity<?> listarAsignaturasAprendizajeDistributivo(@PathVariable("idMalla") Integer idMalla) {
		return ResponseEntity.ok(mallaasignaturaRepository.listarAsignaturasAprendizajeDistributivo(idMalla));
	}

	/***
	 * Servicio que lista los componentes de organización asociados a la malla
	 */
	@RequestMapping(value = "/listarAsignaturasComponentesOrganizacion/{idMalla}", method = RequestMethod.GET)
	public ResponseEntity<?> listarAsignaturasComponentesOrganizacion(@PathVariable("idMalla") Integer idMalla) {
		return ResponseEntity.ok(mallaasignaturaRepository.listarAsignaturasComponentesOrganizacion(idMalla));
	}

	/***
	 * Lista los componentes de organizacion que tengan el tipo de componente de
	 * Organización Curricular(UOC)
	 **/
	@RequestMapping(value = "/listarUOC", method = RequestMethod.GET)
	public ResponseEntity<?> listarUOC() {
		return ResponseEntity.ok(asignaturaOrganizacionRepository.listarUOC());
	}

	/***
	 * Lista los componentes de organizacion que tengan el tipo Unidad de
	 * Organización Curricular(UOC)
	 **/
	@RequestMapping(value = "/listaMallaAsignaturaJsonUOC/{idMalla}", method = RequestMethod.GET)
	public ResponseEntity<?> listaMallaAsignaturaJsonUOC(@PathVariable("idMalla") Integer idMalla) {
		List<MallaAsignatura> _mallaAsignaturas = mallaasignaturaRepository.listaMallaAsignaturaJson(idMalla);
		if (_mallaAsignaturas.isEmpty() == false) {
			for (int i = 0; i < _mallaAsignaturas.size(); i++) {
				MallaAsignatura mallaAsignatura = new MallaAsignatura();
				BeanUtils.copyProperties(_mallaAsignaturas.get(i), mallaAsignatura, "planificacionParalelo",
						"asignaturaRelaciones", "asignaturaRequisitos", "asignaturaAprendizaje", "itinerarioMaterias");
				_mallaAsignaturas.set(i, mallaAsignatura);
			}
		}
		return ResponseEntity.ok(_mallaAsignaturas);
	}
	
	/***
	 * Lista los requisitos de una malla asignatura
	 **/
	@RequestMapping(value = "/listaMallaAsignaturaRequisitoJson/{idMalla}", method = RequestMethod.GET)
	public ResponseEntity<?> listaMallaAsignaturaRequisitoJson(@PathVariable("idMalla") Integer idMalla) {
		List<MallaAsignatura> _mallaAsignaturas = mallaasignaturaRepository.listaMallaAsignaturaJson(idMalla);
		if (_mallaAsignaturas.isEmpty() == false) {
			for (int i = 0; i < _mallaAsignaturas.size(); i++) {
				MallaAsignatura mallaAsignatura = new MallaAsignatura();
				BeanUtils.copyProperties(_mallaAsignaturas.get(i), mallaAsignatura, "planificacionParalelo",
						"asignaturaRelaciones", "asignaturaOrganizacions", "asignaturaAprendizaje", "itinerarioMaterias");
				_mallaAsignaturas.set(i, mallaAsignatura);
			}
		}
		return ResponseEntity.ok(_mallaAsignaturas);
	}

	/***
	 * Lista los componentes de asignatura que tenga una malla
	 **/
	@RequestMapping(value = "/listaMallaAsignaturaJsonAprendizajes/{idMalla}", method = RequestMethod.GET)
	public ResponseEntity<?> listaMallaAsignaturaJsonAprendizajes(@PathVariable("idMalla") Integer idMalla) {
		List<MallaAsignatura> _mallaAsignaturas = mallaasignaturaRepository.listaMallaAsignaturaJson(idMalla);
		if (_mallaAsignaturas.isEmpty() == false) {
			for (int i = 0; i < _mallaAsignaturas.size(); i++) {
				MallaAsignatura mallaAsignatura = new MallaAsignatura();
				BeanUtils.copyProperties(_mallaAsignaturas.get(i), mallaAsignatura, "planificacionParalelo",
						"asignaturaOrganizacions", "asignaturaRelaciones", "asignaturaRequisitos",
						"itinerarioMaterias");
				for (int j = 0; j < _mallaAsignaturas.get(i).getAsignaturaAprendizaje().size(); j++) {
					AsignaturaAprendizaje asignaturaAprendizaje = new AsignaturaAprendizaje();
					BeanUtils.copyProperties(_mallaAsignaturas.get(i).getAsignaturaAprendizaje().get(j),
							asignaturaAprendizaje, "docenteAsignaturaAprend");
					_mallaAsignaturas.get(i).getAsignaturaAprendizaje().set(j, asignaturaAprendizaje);
				}
				_mallaAsignaturas.set(i, mallaAsignatura);
			}
		}
		return ResponseEntity.ok(_mallaAsignaturas);
	}

	/***
	 * Lista los componentes de asignatura y organizacion que tenga una malla
	 * asignatura
	 **/
	@RequestMapping(value = "/listaMallaAsignaturaJsonComponentes/{idMalla}", method = RequestMethod.GET)
	public ResponseEntity<?> listaMallaAsignaturaJsonComponentes(@PathVariable("idMalla") Integer idMalla) {
		List<MallaAsignatura> _mallaAsignaturas = mallaasignaturaRepository.listaMallaAsignaturaJson(idMalla);
		if (_mallaAsignaturas.isEmpty() == false) {
			for (int i = 0; i < _mallaAsignaturas.size(); i++) {
				MallaAsignatura mallaAsignatura = new MallaAsignatura();
				BeanUtils.copyProperties(_mallaAsignaturas.get(i), mallaAsignatura, "planificacionParalelo",
						"asignaturaRelaciones", "asignaturaRequisitos", "itinerarioMaterias");
				for (int j = 0; j < mallaAsignatura.getAsignaturaAprendizaje().size(); j++) {
					AsignaturaAprendizaje asignaturaAprendizaje = new AsignaturaAprendizaje();
					BeanUtils.copyProperties(mallaAsignatura.getAsignaturaAprendizaje().get(j), asignaturaAprendizaje, "docenteAsignaturaAprend");
					mallaAsignatura.getAsignaturaAprendizaje().set(j, asignaturaAprendizaje);
				}
				_mallaAsignaturas.set(i, mallaAsignatura);
			}
		}
		return ResponseEntity.ok(_mallaAsignaturas);
	}

	/***
	 * Graba/Edita y Elimina registros en la tabla asignatura_organizacion,tabla asignatura_aprendizaje,asignaturaRequisitos,asignaturaRelaciones
	 **/

	@RequestMapping(value = "/grabaComponentesAsignatura", method = RequestMethod.POST)
	public ResponseEntity<?> grabaComponentesAsignatura(@RequestBody List<MallaAsignatura> listaMallaAsignatura) {
		mallasService.grabaComponentesAsignatura(listaMallaAsignatura);
		return ResponseEntity.ok().build();
	}

	/***
	 * Servicio para validar si existen asignaturas en una carrera para poder
	 * ingresar a la interfaz nivel
	 **/

	@RequestMapping(value = "/validaExisteAsignaturaOferta/{idOferta}", method = RequestMethod.GET)
	public ResponseEntity<?> validaExisteAsignaturaOferta(@PathVariable("idOferta") Integer idOferta) {
		return ResponseEntity.ok(mallaasignaturaRepository.validaExisteAsignaturaOferta(idOferta));
	}

	/***
	 * Servicio para que reemplaza el procedimiento almacenado pivot que devolvia
	 * las filas
	 **/

	@RequestMapping(value = "/listAllAsignaturasAprendizaje/{idMalla}", method = RequestMethod.GET)
	public ResponseEntity<?> listAllAsignaturasAprendizaje(@PathVariable("idMalla") Integer idMalla) {
		return ResponseEntity.ok(mallaasignaturaRepository.listAllAsignaturasAprendizaje(idMalla));
	}

	@RequestMapping(value = "/prueba/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> prueba(@PathVariable("id") Integer id) {
		return ResponseEntity.ok(mallaasignaturaRepository.findById(id).get());
	}

	@RequestMapping(value = "/pruebaGrabado", method = RequestMethod.POST)
	public ResponseEntity<?> pruebaGrabado(@RequestBody MallaAsignatura mallaAsignatura) {
		MallaAsignatura _mallaAsignatura = new MallaAsignatura();
		if (mallaAsignatura.getId() != null) {
			_mallaAsignatura = mallaasignaturaRepository.findById(mallaAsignatura.getId()).get();
		}
		BeanUtils.copyProperties(mallaAsignatura, _mallaAsignatura);
		mallaasignaturaRepository.save(_mallaAsignatura);
		return ResponseEntity.ok(_mallaAsignatura);
	}
	
	/***
	 * Graba/Edita una malla
	 **/

	@RequestMapping(value = "/grabarMalla", method = RequestMethod.POST)
	public ResponseEntity<?> grabarMalla(@RequestBody Malla malla) {
		mallasService.grabarMalla(malla);
		return ResponseEntity.ok().build();
	}
	
	/**
	 * Prueba del servicio que solo retorne el numero de elemento de las litas.
	 * **/
	@RequestMapping(value = "/verificarMallaUsada/{idMalla}", method = RequestMethod.GET)
	public ResponseEntity<?> verificarMallaUsada(@PathVariable("idMalla") Integer idMalla) {
		Malla malla = mallaRepository.findById(idMalla).get();
		malla.setListaMallaAsignaturas(malla.getMallaAsignaturas().size());
		malla.setListaEstudianteOfertas(malla.getEstudianteOfertas().size());
		malla.setListaPeriodoMalla(malla.getPeriodoMallaVersion().size());
		malla.setListaMallaRequisitos(malla.getMallaRequisitos().size());
		malla.setMallaAsignaturas(null);
		malla.setMallaRequisitos(null);
		malla.setEstudianteOfertas(null);
		malla.setPeriodoMallaVersion(null);
		return ResponseEntity.ok(malla);
	}

		
}
