package ec.edu.upse.acad.model.service;



import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.upse.acad.model.pojo.calificaciones.ActaCalificacion;
import ec.edu.upse.acad.model.pojo.calificaciones.EstudianteCalificacion;
import ec.edu.upse.acad.model.repository.PersonasRepository;
import ec.edu.upse.acad.model.repository.calificaciones.ActaCalificacionRepository;
import ec.edu.upse.acad.model.repository.calificaciones.EstudianteCalificacionRepository;

@Service
@Transactional
public class CalificacionService {
	
	@Autowired private ActaCalificacionRepository actaCalificacionRepository;
	@Autowired private EstudianteCalificacionRepository estudianteCalificacionRepository;
	
	@PersistenceContext
	private EntityManager em;
	
	public void grabaActaCalificacion(ActaCalificacion actaCalificacion) {	
		if (actaCalificacion.getId() != null) {  
			this.editarActaCalificacion(actaCalificacion);
		}
		else {		
			this.nuevoActaCalificacion(actaCalificacion);
		}
	}
	
	public void nuevoActaCalificacion(ActaCalificacion actaCalificacion) {
		ActaCalificacion _actaCalificacion = new ActaCalificacion();
		BeanUtils.copyProperties(actaCalificacion, _actaCalificacion,"estudianteCalificaciones");
		
		Integer idActaCalificacion=actaCalificacionRepository.saveAndFlush(_actaCalificacion).getId();
		BeanUtils.copyProperties(actaCalificacion, _actaCalificacion);
		if(actaCalificacion.getEstudianteCalificaciones()!=null) {
			for(int i=0 ; i<actaCalificacion.getEstudianteCalificaciones().size() ; i++) {
				
				actaCalificacion.getEstudianteCalificaciones().get(i).setIdActaCalificacion(idActaCalificacion);
				EstudianteCalificacion _estudianteCalificacion = new EstudianteCalificacion();
				BeanUtils.copyProperties(actaCalificacion.getEstudianteCalificaciones().get(i), _estudianteCalificacion);
				estudianteCalificacionRepository.saveAndFlush(_estudianteCalificacion);
			}
		}
		//iteracion para sincronizar relacion una a varios con estudianteOferta
		em.getEntityManagerFactory().getCache().evict(ActaCalificacion.class);
	}
	
	public void editarActaCalificacion(ActaCalificacion actaCalificacion) {
		ActaCalificacion _actaCalificacion = new ActaCalificacion();
		//si id de persona es diferente de null actualiza registro;
		_actaCalificacion = actaCalificacionRepository.findById(actaCalificacion.getId()).get();
				
		if (_actaCalificacion!=null) {
			//BeanUtils.copyProperties(actaCalificacion, _actaCalificacion);
			
			if(actaCalificacion.getEstudianteCalificaciones()!=null) {
				for(int i=0; i<actaCalificacion.getEstudianteCalificaciones().size();i++) {
					EstudianteCalificacion _estudianteCalificacion = new EstudianteCalificacion();
					//si id de persona es diferente de null actualiza registro;
					if(actaCalificacion.getEstudianteCalificaciones().get(i).getId()>0) {
						_estudianteCalificacion = estudianteCalificacionRepository.findById(actaCalificacion.getEstudianteCalificaciones().get(i).getId()).get();
						Integer id = actaCalificacion.getEstudianteCalificaciones().get(i).getId();
						Integer idActaCalificacion = actaCalificacion.getEstudianteCalificaciones().get(i).getIdActaCalificacion();
						Integer idEstudiante = actaCalificacion.getEstudianteCalificaciones().get(i).getIdEstudiante();
						Integer idComponenteAprendizaje = actaCalificacion.getEstudianteCalificaciones().get(i).getIdComponenteAprendizaje();
						Integer calificacion = actaCalificacion.getEstudianteCalificaciones().get(i).getCalificacion();
						String usuario = actaCalificacion.getEstudianteCalificaciones().get(i).getUsuarioIngresoId();
						Integer version = actaCalificacion.getEstudianteCalificaciones().get(i).getVersion();
						String estado = actaCalificacion.getEstudianteCalificaciones().get(i).getEstado();
						_estudianteCalificacion.setId(id);
						_estudianteCalificacion.setIdActaCalificacion(idActaCalificacion);
						_estudianteCalificacion.setIdEstudiante(idEstudiante);
						_estudianteCalificacion.setIdComponenteAprendizaje(idComponenteAprendizaje);
						_estudianteCalificacion.setCalificacion(calificacion);
						_estudianteCalificacion.setUsuarioIngresoId(usuario.trim());
						_estudianteCalificacion.setVersion(version);
						_estudianteCalificacion.setEstado(estado.trim());
						estudianteCalificacionRepository.save(_estudianteCalificacion);	
					}
					
				}
			}
			actaCalificacionRepository.save(_actaCalificacion);
		}
		em.getEntityManagerFactory().getCache().evict(ActaCalificacion.class);
		
		}

}
