package ec.edu.upse.acad.model.pojo.reglamento;

import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ec.edu.upse.acad.model.pojo.ArchivoDocente;

import ec.edu.upse.acad.model.pojo.calificaciones.ConfiguracionCalificaciones;
import ec.edu.upse.acad.model.pojo.distributivo.DistributivoDocente;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="aca", name="reglamento_actividad_detalle")
@NoArgsConstructor
public class ReglamentoActividadDetalle {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_regla_actividad_detalle")
	@Getter @Setter private Integer id;
	
	@Column(name="id_actividad_detalle")
	@Getter @Setter private Integer idActividadDetalle;
	
	@Column(name="id_reglamento_actividad")
	@Getter @Setter private Integer idReglamentoActividad;
	
	@Column(name="obligatorio_distributivo")
	@Getter @Setter  private String obligatorioDistributivo;
	
	@Getter @Setter  private String estado;
//	
//	@Column(name="fecha_ingreso")
//	@Getter @Setter  private Date fechaIngreso;
		
	@Column(name="usuario_ingreso_id")
	@Getter @Setter  private String usuarioIngresoId;
		
	@Version
	@Getter @Setter  private Integer version;
	


	
	//RELACIONES
	//bi-directional many-to-one association to ActividadDocenteDetalle
			@ManyToOne(fetch=FetchType.LAZY)
			@JoinColumn(name="id_actividad_detalle" , insertable=false, updatable = false)
			@JsonIgnore
			@Getter @Setter private ActividadDocenteDetalle actividadDocenteDetalle;
			
  //bi-directional many-to-one association to ReglamentoActividadDocente
			@ManyToOne(fetch=FetchType.LAZY)
			@JoinColumn(name="id_reglamento_actividad" , insertable=false, updatable = false)
			@JsonIgnore
			@Getter @Setter private ReglamentoActividadDocente reglamentoActividadDocente;
			
			
	
}
