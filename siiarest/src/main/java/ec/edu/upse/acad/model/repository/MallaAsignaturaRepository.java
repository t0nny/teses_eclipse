package ec.edu.upse.acad.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ec.edu.upse.acad.model.pojo.MallaAsignatura;



@Repository




public interface MallaAsignaturaRepository extends JpaRepository<MallaAsignatura, Integer>{
	
	//Ejemplo de como usar un procedimiento almacenado
	@Transactional	
	@Procedure(procedureName  ="aca.sp_grb_consulta_materia_nivel")
	Object[] namConsultaMateriaNivel(@Param("idTransaccion") Integer idTransaccion,@Param("idNivel") Integer idNivel,
			@Param("idCompOrganizacion") Integer idCompOrganizacion,@Param("idMallaVersion") Integer idMallaVersion);
	
	
	
	
	@Query("SELECT d.id as idDepartamento,d.nombre as departamento,d.estado as estado,d.version as version "+
			"from Departamento d " +
			"WHERE d.estado='AC' and d.tipo='FAC'")
	 List<CustomObjectDepartamento> listaDepartamento();
	
	interface CustomObjectDepartamento { 
		Integer getIdDepartamento();
		String getDepartamento();
		String getEstado();
		Integer getVersion();
	}
	
	@Query("SELECT m.id as idMalla,SUM(isnull(ma.numHoras,0)) as numHoras ,SUM (isnull(ma.numCreditos,0)) as numCreditos, MIN(n.orden) as minNivel,  MAX(n.orden) as maxNivel, "+
			" (SELECT count (DISTINCT  n1.orden) From MallaAsignatura ma1 INNER JOIN Nivel n1 on ma1.idNivel=n1.id where ma1.estado='A'  and  n1.estado='A' and  ma1.idMalla=(?1) ) as cantNivel "+
			"FROM Malla as m "+
			"INNER JOIN MallaAsignatura as ma on m.id=ma.idMalla "+
			"INNER JOIN Nivel as n on ma.idNivel=n.id " +
			"WHERE m.estado='A' and ma.estado='A' and n.estado='A' and m.id=(?1) "+
			"GROUP BY m.id"
			
			)
	 List<CustomObjectMallaAsig> listaCalculoAsignaturaMalla(Integer id);
	interface CustomObjectMallaAsig {
		//,SUM(isnull(ma.numHoras,0)) as numHoras ,SUM (isnull(ma.numCreditos,0)) as numCreditos 
		Integer getIdMalla();
		Integer getNumHoras();
		Integer getNumCreditos();
		Integer getMinNivel();
		Integer getMaxNivel();
		Integer getCantNivel();
	}
	
	
	@Query("SELECT m.id as idMalla, pa.descripcion as periodoAcademico "+
			"FROM Malla as m "+
			"INNER JOIN MallaAsignatura as ma on m.id=ma.idMalla "+
			"INNER JOIN AsignaturaAprendizaje as aa on ma.id=aa.idMallaAsignatura  " +
			"INNER JOIN DocenteAsignaturaAprend as daa on aa.id=daa.idAsignaturaAprendizaje "+
			"INNER JOIN DistributivoDocente as dd on daa.idDistributivoDocente=dd.id "+
			"INNER JOIN DistributivoOfertaVersion as dov on dd.idDistributivoOfertaVersion=dov.id "+
			"INNER JOIN DistributivoOferta as dof on dov.idDistributivoOferta=dof.id "+
			"INNER JOIN DistributivoGeneralVersion  as dgv on dof.idDistributivoGeneralVersion=dgv.id "+
			"INNER JOIN DistributivoGeneral as dg on dgv.idDistributivoGeneral=dg.id "+
			"INNER JOIN PeriodoAcademico pa on dg.idPeriodoAcademico=pa.id "+
			"WHERE m.estado in ('A','P') and ma.estado='A' and aa.estado='A' and daa.estado='A' and dd.estado='A'"+
			" and dov.estado in ('P','A', 'V', 'R') and dgv.estado='A' and dg.estado='A' and pa.estado='A' and m.id=(?1) "+
			"GROUP BY  m.id , pa.descripcion ")
	
	 List<CustomObjectMallaDistributivo> listaMallaUtilizadaDistributivo(Integer idMalla);
	interface CustomObjectMallaDistributivo {
		Integer getIdMalla();
		String getPeriodoAcademico();

	}
	
	
	
	@Query("SELECT  do.id as id,o.id as idOfer, do.idDepartamento as idDepartamento,o.idTipoOferta as tipo,o.descripcion as carrera,o.estado as estado " + 
				"FROM Oferta o " + 
				"INNER JOIN DepartamentoOferta do on do.idOferta = o.id " + 
				"Where o.estado = 'A' and do.estado='A'  " + 
				"group by do.id,o.id,o.idTipoOferta,o.descripcion,o.estado,do.idDepartamento")
	 List<CustomObject> listaMallaCarreraTodos();
	
	interface CustomObject { 
		Integer getId(); 
		Integer getIdOfer(); 
		Integer getIdDepartamento(); 
		Integer getTipo();
	    String getCarrera();
	   // String getDescripcion();
	    //String getSistemaEstudio();
	    String getEstado();
	  //  Integer getMallaVersion();
	} 
	
	//Lista las materias y las presenta agrupadas por nivel(semestre o a�os) en el 1er kanban de asginacion de UOC
	@Query(	"	SELECT n.id as id, CONCAT('NIVEL ', CAST(n.orden as text)) as detalle, n.estado as estado,0 as resourceId,'#36c7d0' as hex from Nivel n "	
			+ "	WHERE n.id not in (select distinct ma1.idNivel from AsignaturaOrganizacion ao "
			+ " inner join MallaAsignatura ma1 on ma1.id =ao.idMallaAsignatura "
			+ " inner join ComponenteOrganizacion co on co.id = ao.idCompOrganizacion "
			+ " inner join TipoCompOrganizacion tco on tco.id = co.idTipoCompOrganizacion "
			+ " Where tco.abreviatura ='UOC' "
			+ "	and ma1.idMalla = (?1) and ma1.estado='A' and co.estado='A' and tco.estado='A' and ao.estado='A' ) and "
			+ " n.orden<=(select m.numNiveles from Malla AS m where m.id = (?1) and m.estado in ('A','P')) and n.estado='A' " 
			+ "	group by n.id,n.orden,n.estado order by n.orden asc")
	 List<CustomObjectNivel> listaMallaNivel(Integer idMalla);
//	
	interface CustomObjectNivel { 
		Integer getId();
		String getDetalle();
		String getEstado();
		Integer getResourceId(); 	
		String getHex();
	}
	
	@Query("SELECT ma.idAsignatura AS id,a.descripcion AS materia,ma.version AS version, " 
			+ "ma.idNivel AS estado,ma.id as tags,'#36c7d0' as hex,ma.numHoras as content,ma.numCreditos as className  from MallaAsignatura as ma " 
			+ "INNER JOIN Nivel n on ma.idNivel = n.id " 
			+ "INNER JOIN Malla m on ma.idMalla = m.id "
			+ "INNER JOIN Asignatura a on ma.idAsignatura=a.id "
			+ "WHERE ma.idMalla = (?1) and n.orden<=(select m.numNiveles from Malla AS m where m.id = (?1))  "
			+ " and ma.estado='A' and m.estado in ('A','P') and n.estado='A' and a.estado='A'"
			+ "group by ma.idAsignatura,a.descripcion, ma.version,ma.idNivel,ma.id,ma.numHoras,ma.numCreditos,n.orden order by n.orden asc")
	 List<CustomObjectMateria> listaMallaAsignatura (Integer idMalla);
	
	interface CustomObjectMateria {
		Integer getId();
		String getMateria();
		Integer getversion();
		String getEstado();
		Integer getTags();
		String getHex();
		String getContent();
		String getClassName();
	}
	
	@Query("SELECT ma.idAsignatura AS idAsignatura, ma.id as idMallaAsignatura, a.descripcion AS asignatura,ma.idNivel AS idNivel,n.orden as nivel, "
			+ " ma.numHoras as numHoras,ma.numCreditos as numCreditos  from MallaAsignatura as ma "
			+ " INNER JOIN Nivel n on ma.idNivel = n.id " + "INNER JOIN Malla m on ma.idMalla = m.id "
			+ " INNER JOIN Asignatura a on ma.idAsignatura=a.id "
			+ " WHERE ma.idMalla = (?1) and n.orden<=(select m.numNiveles from Malla AS m where m.id = (?1))  "
			+ " and ma.estado='A' and m.estado in ('A','P') and n.estado='A' and a.estado='A'"
			+ " group by ma.idAsignatura,ma.id,a.descripcion,ma.idNivel,n.orden,ma.numHoras,ma.numCreditos order by n.orden asc")
	 List<IntComp> listaMallaAsignaturaParaInterfazComponente (Integer idMalla);
	
	interface IntComp {
		Integer getIdAsignatura();
		Integer getIdMallaAsignatura();
		String getAsignatura();
		Integer getIdNivel();
		Integer getNivel();
		Integer getNumHoras();
		Integer getNumCreditos();
	}

	  //Listas materias asigandas a un nivel(semestre o a�o) por idmallaversion y idnivel AB
	@Query("SELECT ma.id AS id,a.descripcion AS detalle, " 
			+ "ma.estado AS estado from MallaAsignatura as ma " 
			+ "INNER JOIN Nivel n on ma.idNivel = n.id "
			+ "INNER JOIN Asignatura a on ma.idAsignatura = a.id " 
			+ "WHERE ma.idMalla=?1 and ma.idNivel=?2 and ma.estado='A'")
	 List<CustomObjectMateriaNivel> listaMallaAsignaturaNivel (Integer id, Integer idNivel);

	interface CustomObjectMateriaNivel {
		Integer getId();
		String getDetalle();
		String getEstado();
	}
	
	  //Listas materias asigandas a un nivel(semestre o a�o) por idmallaversion y idnivel - Solo si existen en asignatura organizacion AB
	@Query("SELECT ma.id AS id,a.descripcion AS detalle, " 
			+ "ma.estado AS estado from MallaAsignatura as ma " 
			+ "INNER JOIN Nivel n on ma.idNivel = n.id "
			+ "INNER JOIN AsignaturaOrganizacion ao on ao.idMallaAsignatura = ma.id and ao.estado='A'" 
			+ "INNER JOIN Asignatura a on ma.idAsignatura = a.id " 
			+ "WHERE ao.idCompOrganizacion in (SELECT compo.id FROM ComponenteOrganizacion compo WHERE compo.idTipoCompOrganizacion=2) and ma.estado='A' and ma.idMalla=?1 and ma.idNivel=?2 ")
	 List<CustomObjectMateriaNivelAo> listaMallaAsignaturaNivelAo (Integer id, Integer idNivel);

	interface CustomObjectMateriaNivelAo {
		Integer getId();
		String getDetalle();
		String getEstado();
	}
	
	//Valida si existen asignaturas en la carrera para asi permitir el ingreso a la interfaz Niveles
	@Query("SELECT a.id as idAsignatura,a.descripcion as asignatura from Asignatura as a " 
			+ " INNER JOIN OfertaAsignatura oa ON oa.asignatura.id = a.id "
			+ " INNER JOIN Oferta o ON oa.oferta.id = o.id "
			+ " WHERE oa.oferta.id= (?1) and oa.estado='A' and a.estado='A' and o.estado='A' ")
	 List<AsigOferta> validaExisteAsignaturaOferta(Integer idOferta);
	
	interface AsigOferta { 
		Integer getIdAsignatura();
		String getAsignatura();
	}
	
	//consulta de asignaturas version por Malla Id
	@Query("SELECT a.id as id,a.descripcion as materia,o.id as resourceid,a.estado as estado,0 as tags from Asignatura as a " 
			+ " INNER JOIN OfertaAsignatura oa ON oa.asignatura.id = a.id "
			+ " INNER JOIN Oferta o ON oa.oferta.id = o.id "
			+ " WHERE a.id NOT IN (SELECT ma.idAsignatura from MallaAsignatura as ma " 
			+ " INNER JOIN Nivel n on ma.idNivel = n.id "
			+ " INNER JOIN Malla m on ma.idMalla = m.id "
			+ " WHERE ma.idMalla=?1 and ma.estado='A' and m.estado in ('A','P') and n.estado='A') and oa.oferta.id=?2" 
			+ "	and oa.estado='A' and a.estado='A' and o.estado='A' order by a.descripcion" )
	 List<CustomObjectMateriaOferta> listaAsignaturaOferta(Integer idMalla,Integer idOferta);
	
	interface CustomObjectMateriaOferta { 
		Integer getId();
		String getMateria();
		Integer getResourceId();
		String getEstado();
		Integer getTags();
	}

	//consulta de asignaturas version por nombre de asignatura
	@Query("SELECT a.id as id,a.descripcion as materia,o.id as resourceid,a.estado as estado from Asignatura as a " 
			+ "INNER JOIN OfertaAsignatura oa ON oa.asignatura.id = a.id "
			+ "INNER JOIN Oferta o ON oa.oferta.id = o.id "
			+ "WHERE (a.descripcion LIKE ?2) AND a.id NOT IN (SELECT ma.idAsignatura from MallaAsignatura as ma " 
			+ "INNER JOIN Nivel n on ma.idNivel = n.id "
			+ "INNER JOIN Malla m on ma.idMalla = m.id " 
			+ "WHERE ma.idMalla=?1 and ma.estado='A') and oa.oferta.id=?3")
	 List<CustomObjectMateriaOfertanombre> listaAsignaturaOfertanombre(Integer id,String nombre,Integer idOferta);
	
	interface CustomObjectMateriaOfertanombre { 
		Integer getId();
		String getMateria();
		Integer getResourceId();
		String getEstado();
	}
	
	
	@Query(value=" select ma from MallaAsignatura ma where ma.idAsignatura = (?1) and ma.id in " + 
			"(select aa.idMallaAsignatura from AsignaturaAprendizaje aa inner join DocenteAsignaturaAprend daa " + 
			"on aa.id = daa.idAsignaturaAprendizaje inner join DistributivoDocente ddo " + 
			"on daa.idDistributivoDocente = ddo.id and ddo.id = (?2) and ddo.estado = 'A' and daa.estado = 'A' and aa.estado = 'A') ")
	MallaAsignatura asignaturaEliminarDocenteDistributivo(Integer idAsignatura,Integer idDistributivoDocente);
	
	@Query(value="SELECT e from MallaAsignatura as e where e.id = ?1 ")
	MallaAsignatura registroMallaAsignatura(Integer id);
	
	//Listas todos los componentes de aprendizaje de una malla esta es la consulta que reemplaza a esa funcion de sql con pivot
	@Query(value = "	SELECT n.orden as nivel,a.descripcion as asignatura,ma.numHoras as numHoras, ma.numCreditos as numCreditos, "
			+ "	ca.id as idComponenteAprendizaje,ca.codigo as codigoComp,"
			+ " ca.descripcion as componenteAprendizaje,aa.id as idAsignaturaAprendizaje,"
			+ " aa.valor as valorComp,aa.version as versionAsiApre,ma.id as idMallaAsignatura "
			+ "	from MallaAsignatura ma " + "	inner join Asignatura a on ma.idAsignatura = a.id "
			+ "	inner join Nivel n on ma.idNivel = n.id " + "	inner join Malla m on ma.idMalla = m.id "
			+ "	inner join AsignaturaAprendizaje aa on aa.idMallaAsignatura = ma.id "
			+ "	inner join ComponenteAprendizaje ca on ca.id = aa.idComponenteAprendizaje "
			+ "	where m.id = (?1) and ma.estado='A' and a.estado='A' and n.estado='A' and m.estado in ('A','P') and aa.estado='A' and ca.estado='A' "
			+ "	order by n.orden ")
	List<CoAsiApren> listAllAsignaturasAprendizaje(Integer idMalla);
	
	//Estas consulta devuelve las componentes de aprendizaje que no estan siendo usadas en el distributivo solo en AA
	@Query(value = "	SELECT n.orden as nivel,a.descripcion as asignatura,ma.numHoras as numHoras, ma.numCreditos as numCreditos, "
			+ "	ca.id as idComponenteAprendizaje,ca.codigo as codigoComp,"
			+ " ca.descripcion as componenteAprendizaje,aa.id as idAsignaturaAprendizaje,"
			+ " aa.valor as valorComp,aa.version as versionAsiApre,ma.id as idMallaAsignatura "
			+ "	from MallaAsignatura ma " + "	inner join Asignatura a on ma.idAsignatura = a.id "
			+ "	inner join Nivel n on ma.idNivel = n.id " + "	inner join Malla m on ma.idMalla = m.id "
			+ "	inner join AsignaturaAprendizaje aa on aa.idMallaAsignatura = ma.id "
			+ "	inner join ComponenteAprendizaje ca on ca.id = aa.idComponenteAprendizaje "
			+ "	where m.id = (?1) and aa.id not in(" + "	select aa1.id from AsignaturaAprendizaje aa1 "
			+ "	inner join DocenteAsignaturaAprend daa on daa.idAsignaturaAprendizaje = aa1.id "
			+ "	inner join DistributivoDocente ddo on ddo.id = daa.idDistributivoDocente where ddo.estado='A' and daa.estado='A' and aa1.estado='A') "
			+ "	and ma.estado='A' and a.estado='A' and n.estado='A' and m.estado in ('A','P') and aa.estado='A' and ca.estado='A' "
			+ "	order by n.orden ")
	List<CoAsiApren> listarAsignaturasAprendizajeMalla(Integer idMalla);
	
	interface CoAsiApren{ 
		Integer getNivel();
		String getAsignatura();
		String getNumHoras();
		String getNumCreditos();
	
		Integer getIdComponenteAprendizaje();
		String getCodigoComp();
		String getComponenteAprendizaje();
		Integer getIdAsignaturaAprendizaje();
		Integer getValorComp();
		Integer getVersionAsiApre();
		Integer getIdMallaAsignatura();

	}
	
	
	@Query(value="	SELECT distinct n.orden as nivel,m.id as idMalla,a.descripcion as asignatura, " + 
			"	m.estado as estadoMalla,ma.id as idMallaAsignatura," +
			"   dov.id as idDistributivoOfertaVersion ,dov.descripcion as distributivoDes,dg.descripcion as disGeneralDes, pa.codigo as periodoAcademico " + 
			"	from MallaAsignatura ma " + 
			"	inner join Asignatura a on ma.idAsignatura = a.id " + 
			"	inner join Nivel n on ma.idNivel = n.id " + 
			"	inner join Malla m on ma.idMalla = m.id " + 
			"	inner join AsignaturaAprendizaje aa on aa.idMallaAsignatura = ma.id " + 
			"	inner join DocenteAsignaturaAprend daa on daa.idAsignaturaAprendizaje = aa.id " + 
			"	inner join DistributivoDocente ddo on ddo.id = daa.idDistributivoDocente " + 
			"	inner join DistributivoOfertaVersion dov on dov.id = ddo.idDistributivoOfertaVersion " + 
			"	inner join DistributivoOferta dof on dof.id = dov.idDistributivoOferta " + 
			"	inner join DistributivoGeneralVersion dgv on dgv.id = dof.idDistributivoGeneralVersion " + 
			"	inner join DistributivoGeneral dg on dg.id = dgv.idDistributivoGeneral " + 
			"	inner join PeriodoAcademico pa on pa.id = dg.idPeriodoAcademico " + 
			"	where m.id = (?1) and ma.estado='A'  " +
			"	and a.estado='A' and n.estado='A' and m.estado in ('A','P') and aa.estado='A' and daa.estado='A' and ddo.estado='A' " + 
			"	and dov.estado in ('P','A', 'V', 'R') and dof.estado='A' and dgv.estado='A' and dg.estado='A' and pa.estado='A'   " + 
//			"	and dov.id =(select max(dov1.id) from DistributivoOfertaVersion dov1 where dov1.idDistributivoOferta =dov.idDistributivoOferta and dov1.estado in ('P','A', 'V', 'R'))"+
			"	order by n.orden ")
	List<CoAsiAprenDis> listarAsignaturasAprendizajeDistributivo(Integer idMalla);
	
	interface CoAsiAprenDis{ 
		Integer getNivel();
		Integer getIdMalla();
		String getAsignatura();
		String getEstadoMalla();
		Integer getIdMallaAsignatura();
		Integer getIdDistributivoOfertaVersion();
		String getDistributivoDes();
		String getDisGeneralDes();
		String getPeriodoAcademico();
	}

	@Query(value="	SELECT distinct n.orden as nivel,m.id as idMalla,a.descripcion as asignatura,ma.numHoras as numHoras, ma.numCreditos as numCreditos, " + 
			"	m.estado as estadoMalla,co.descripcion as componenteOrganizacion,ma.id as idMallaAsignatura,tco.descripcion as tipoCompOrganizacion " + 
			"	from MallaAsignatura ma " + 
			"	inner join Asignatura a on ma.idAsignatura = a.id " + 
			"	inner join Nivel n on ma.idNivel = n.id " + 
			"	inner join Malla m on ma.idMalla = m.id " + 
			"	inner join AsignaturaOrganizacion ao on ao.idMallaAsignatura = ma.id " + 
			"	inner join ComponenteOrganizacion co on co.id = ao.idCompOrganizacion " + 
			"	inner join TipoCompOrganizacion tco on tco.id = co.idTipoCompOrganizacion " + 
			"	where m.id = (?1) and ma.estado='A' " + 
			"	and a.estado='A' and n.estado='A' and m.estado in ('A','P') and ao.estado='A' and co.estado='A' and tco.estado='A'" + 
			"	order by n.orden ")
	List<CoAsiCompOrg> listarAsignaturasComponentesOrganizacion(Integer idMalla);
	
	interface CoAsiCompOrg{ 
		Integer getNivel();
		Integer getIdMalla();
		String getAsignatura();
		String getNumHoras();
		String getNumCreditos();
		String getEstadoMalla();
		String getComponenteOrganizacion();
		Integer getIdMallaAsignatura();
		String getTipoCompOrganizacion();
	}
	/**
	 * Consulta que devuelve el Json Completo de MallaAsignatura con sus relaciones en Asignatura Organización 
	 * 
	 * **/
	
	@Query("SELECT ma from MallaAsignatura as ma " 
			+ "INNER JOIN Nivel n on ma.idNivel = n.id " 
			+ "WHERE ma.idMalla = (?1) and n.orden<=(select m.numNiveles from Malla AS m where m.id = (?1) and m.estado in ('A','P')) and n.estado='A' and ma.estado='A' "
			+ "order by n.orden asc")
	 List<MallaAsignatura> listaMallaAsignaturaJson (Integer idMalla);

	MallaAsignatura findByIdMalla(Integer idMalla);
	

	
}
