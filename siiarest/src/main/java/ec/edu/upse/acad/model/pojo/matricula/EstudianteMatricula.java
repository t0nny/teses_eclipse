package ec.edu.upse.acad.model.pojo.matricula;

import javax.persistence.*;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.List;



@Entity
@Table(schema="aca", name="estudiante_matricula")
@Where(clause = "estado='A' or estado='X' or estado='R'")
@NoArgsConstructor

public class EstudianteMatricula {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_estudiante_matricula")
	@Getter @Setter private Long id;
	
	@Column(name="id_estudiante_oferta")
	@Getter @Setter private Integer idEstudianteOferta;
	
	@Column(name="id_matricula_general")
	@Getter @Setter private Integer idMatriculaGeneral;
	
	@Getter @Setter private String estado;

//	@Column(name="fecha_ingreso")
//	@Getter @Setter private Timestamp fechaIngreso;

	@Getter @Setter private String observacion;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private int version;

	//RELACIONES
	//bi-directional many-to-one association to EstudianteAsignatura
	@OneToMany(mappedBy="estudianteMatricula", cascade=CascadeType.ALL)
	@Getter @Setter private List<EstudianteAsignatura> estudianteAsignaturas;

	//bi-directional many-to-one association to EstudianteOferta
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_estudiante_oferta", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private EstudianteOferta estudianteOferta;

	//bi-directional many-to-one association to MatriculaGeneral
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_matricula_general", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private MatriculaGeneral matriculaGeneral;

	@PrePersist
	void preInsert() {
	   if (this.estado == null)
	       this.estado = "A";
	}

}