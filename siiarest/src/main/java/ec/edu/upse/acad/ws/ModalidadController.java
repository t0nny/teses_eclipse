package ec.edu.upse.acad.ws;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.upse.acad.model.pojo.Modalidad;
import ec.edu.upse.acad.model.repository.ModalidadRepository;
import ec.edu.upse.acad.model.service.UpdateForIdService;

@RestController
@RequestMapping("/api/modalidades")
@CrossOrigin
public class ModalidadController {

	@Autowired
	private ModalidadRepository modalidadRepository;
	@Autowired
	private UpdateForIdService eliminarModalidad;

	// ****************** SERVICIOS PARA MODALIDAD************************//

	// Buscar todos las modalidades, para listar en angular
	@RequestMapping(value = "/buscarModalidad", method = RequestMethod.GET)
	public ResponseEntity<?> buscarModalidad() {
		return ResponseEntity.ok(modalidadRepository.buscarmodalidad());
	}

	// buscar por el id de la Modalidad
	@RequestMapping(value = "/buscarModalidadId/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarModalidadId(@PathVariable("id") Integer id) {
		return ResponseEntity.ok(modalidadRepository.buscarmodalidadId(id));
	}

	// busca por el codigo de la modalidad
	@RequestMapping(value = "/buscarCodigoModalidad/{codigo}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarCodigoModalidad(@PathVariable("codigo") String codigo) {
		return ResponseEntity.ok(modalidadRepository.buscarmodalidadCampo(codigo));
	}

	// Servicio de grabar las modalidades
	@RequestMapping(value = "/grabarModalidad", method = RequestMethod.POST)
	public ResponseEntity<?> grabarModalidad(@RequestBody Modalidad modalidad) {
		Modalidad _modalidad = new Modalidad();
		if (modalidad.getId() != null) {
			_modalidad = modalidadRepository.findById(modalidad.getId()).get();
		}
		BeanUtils.copyProperties(modalidad, _modalidad);
		modalidadRepository.save(_modalidad);
		return ResponseEntity.ok(_modalidad);
	}

	// Servicio de borrar las modalidades
	@RequestMapping(value = "/borrarModalidad/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> borrarModalidad(@PathVariable("id") Integer id) {
		eliminarModalidad.borrarModalidad(id);
		return ResponseEntity.ok().build();
	}
	// ***********************FIN SERVICIOS PARA MODALIDAD***********************

}
