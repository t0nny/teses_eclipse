package ec.edu.upse.acad.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.Universidad;
import ec.edu.upse.acad.model.repository.PaisRepository.CustomObject;

@Repository
public interface UniversidadRepository extends JpaRepository<Universidad,Integer>{

	@Query(value=" SELECT u.id as idUniversidad, u.institucion as universidad "+
			"FROM Universidad as u " ) 
	List<CustomObject> listarUniversidades();	
	interface CustomObject  { 
		Integer getIdUniversidad(); 
		String getUniversidad();
	}
}
