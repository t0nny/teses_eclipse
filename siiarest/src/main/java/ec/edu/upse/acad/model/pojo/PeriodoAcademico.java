package ec.edu.upse.acad.model.pojo;


import javax.persistence.*;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ec.edu.upse.acad.model.pojo.calificaciones.CalificacionGeneral;

import ec.edu.upse.acad.model.pojo.distributivo.DistributivoGeneral;
import ec.edu.upse.acad.model.pojo.distributivo.MallaAsignaturaFusion;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Date;

import java.util.List;

@Entity
@Table(schema="aca", name="periodo_academico")
@Where(clause = "estado='A'")
@NoArgsConstructor
public class PeriodoAcademico  {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_periodo_academico")
	@Getter @Setter private Integer id;
	
	@Column(name="id_periodo")
	@Getter @Setter private Integer idPeriodo;
	
	@Column(name="codigo_tipo_periodo")
	@Getter @Setter private String codigoTipoPeriodo;
	
	@Getter @Setter private String codigo;

	@Getter @Setter private String descripcion;
	
	@Column(name="extraordinario")
	@Getter @Setter private Boolean extraordinario;

	@Getter @Setter private String estado;

	@Column(name="fecha_desde")
	@Getter @Setter private Date fechaDesde;

	@Column(name="fecha_hasta")
	@Getter @Setter private Date fechaHasta;
	
	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;
	
	//RELACIONES

	//bi-directional many-to-one association to Periodo
	@ManyToOne
	@JoinColumn(name="id_periodo", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Periodo periodo;
	
	//bi-directional many-to-one association to PeriodoModalidad
	@OneToMany(mappedBy="periodoAcademico", cascade=CascadeType.ALL)
	@Getter @Setter private List<PeriodoModalidad> periodoModalidades;

	//bi-directional many-to-one association to Periododistributivo
	@OneToMany(mappedBy="periodoAcademico", cascade=CascadeType.ALL)
	@Getter @Setter private List<DistributivoGeneral> distributivoGeneral;
		
    //bi-directional many-to-one association to Periododistributivo
	@OneToMany(mappedBy="periodoAcademico", cascade=CascadeType.ALL)
	@Getter @Setter private List<PeriodoTipoOferta> periodoTipoOferta;

    //bi-directional many-to-one association to PeriodoMalla Version

	@OneToMany(mappedBy="periodoAcademico", cascade=CascadeType.ALL)
	@Getter @Setter private List<PeriodoMalla> peridoMallaVersion;
	
	//bi-directional many-to-one association to Calificacion General
	@OneToMany(mappedBy="periodoAcademico", cascade=CascadeType.ALL)
	@JsonIgnore
	@Getter @Setter private List<CalificacionGeneral> calificacionGeneral;
	
	@OneToMany(mappedBy="periodoAcademico", cascade=CascadeType.ALL)
	@JsonIgnore
	@Getter @Setter private List<PlanificacionParalelo> planificacionParalelo;
	
	@OneToMany(mappedBy="periodoAcademico", cascade=CascadeType.ALL)
	@JsonIgnore
	@Getter @Setter private List<MallaAsignaturaFusion> mallaAsignaturaFusions;

		@OneToMany(mappedBy="periodoAcademico", cascade=CascadeType.ALL)
		@JsonIgnore
		@Getter @Setter private List<OfertaDocente> ofertaDocente;
		

	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	}
}