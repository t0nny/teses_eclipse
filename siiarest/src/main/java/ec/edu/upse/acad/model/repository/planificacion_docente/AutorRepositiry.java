package ec.edu.upse.acad.model.repository.planificacion_docente;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.planificacion_docente.Autor;
@Repository
public interface AutorRepositiry extends JpaRepository<Autor, Integer> {

	@Query(value="select a.id as idAutor, a.nombres as nombres "+
			" from Autor a ")
	List<CoListadoAutores> listaAutores();
	interface CoListadoAutores{ 
	    Integer getIdAutor();   
	    String getNombres();
	}
}
