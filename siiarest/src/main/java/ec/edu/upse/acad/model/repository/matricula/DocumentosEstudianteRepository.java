package ec.edu.upse.acad.model.repository.matricula;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.matricula.DocumentosEstudiante;

@Repository
public interface DocumentosEstudianteRepository  extends JpaRepository<DocumentosEstudiante, Integer>{
	@Query(value="select te.id,te.descripcion as tipoEstudianteMovilidad,de.descripcion as requisitos, "+
	 "n.descripcion as semestre, tof.descripcion as tipoOferta "+
	 "from DocumentosEstudiante de " +
	"inner join TipoEstudiante te on de.idTipoEstudiante = te.id " +
	"inner join Nivel n on de.idNivel = n.id "  +
	"inner join ReglamentoTipoOferta rto on de.idReglamentoTipoOferta = rto.id " +
	"inner join TipoOferta tof on rto.idTipoOferta = tof.id " +
	"where de.idNivel = (?1) and de.idTipoEstudiante=(?2) and tof.id = (?3) and tof.estado = 'A' ")
	List<CustomObjectDocumentosEstudiantes>requisitosEstudiantes(Integer idNivel, Integer idTipoEstudiante, Integer idTipoOferta);
	
	interface CustomObjectDocumentosEstudiantes{
		Integer getId();
		String getTipoEstudianteModalidad();
		String getRequisitos();
		String getSemestre();
		String getTipoOferta();
	}
}
