package ec.edu.upse.acad.model.repository.matricula;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ec.edu.upse.acad.model.pojo.matricula.SubtipoMovilidad;


public interface SubtipoMovilidadRepository extends JpaRepository<SubtipoMovilidad, Integer> {
	@Query(value="select sm.id as id, sm.codigo as codigo, sm.descripcion as descripcion, sm.estado as estado" +
			" from SubtipoMovilidad sm " +
			" where sm.idTipoMovilidad=(?1) and sm.estado='A' ")
	List<CustomObjectfiltrarSubtipoMovilidad> filtrarSubtipoMovilidad(Integer idTipoMovilidad);
	interface CustomObjectfiltrarSubtipoMovilidad{
		Integer getId();
		String getCodigo();
		String getDescripcion();
		String getEstado();
		
	}
	
	@Query(value=" select tm.id as idTipoMovilidad,stm.id as idSubTipoMovilidad,tm.descripcion as movilidad,"+
			" stm.descripcion as subtipoMovilidad,tm.codigo as codigoMovilidad,stm.codigo as codigoSubtipoMovilidad " + 
			" from SubtipoMovilidad stm " + 
			" inner join TipoMovilidad tm on tm.id = stm.idTipoMovilidad where stm.id = (?1) ")
	List<CustomObjectRecuperarDescripcion> recuperarTipoSubTipoMovilidad(Integer idSubtipoMovilidad);
	interface CustomObjectRecuperarDescripcion{
		Integer getIdTipoMovilidad();
		Integer getIdSubTipoMovilidad();
		String getMovilidad();
		String getSubtipoMovilidad();
		String getCodigoMovilidad();
		String getCodigoSubtipoMovilidad();
		
	}
	
	
}
