package ec.edu.upse.acad.model.repository.vinculacion;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.vinculacion.AreaGeografica;

@Repository
public interface AreaGeograficaRepository extends JpaRepository<AreaGeografica, Integer> {
	
	@Transactional
	//@Query(value = "SELECT ag.id as id, ag.idPadre as idPadre, ag.descripcion as descripcion, ag.estado "
	@Query(value = "SELECT ag "
			+ "FROM AreaGeografica ag " + " WHERE ag.estado='A' ")
	List<AreaGeografica> buscarListaAreaGeografica();

	
	@Query(value = "SELECT ag "
	+ "FROM AreaGeografica ag " + " WHERE ag.idPadre=(?1)  and ag.estado='A' ")
	Optional<AreaGeografica> ListarPorIdAreaGeoPadre(Integer idAreaGeografica);

}
