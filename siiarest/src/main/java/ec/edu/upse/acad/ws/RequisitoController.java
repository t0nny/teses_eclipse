package ec.edu.upse.acad.ws;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.upse.acad.model.pojo.AsignaturaRequisito;
import ec.edu.upse.acad.model.pojo.Malla;
import ec.edu.upse.acad.model.pojo.MallaRequisito;

import ec.edu.upse.acad.model.pojo.Requisito;
import ec.edu.upse.acad.model.repository.AsignaturaRequisitoRepository;
import ec.edu.upse.acad.model.repository.MallaRepository;
import ec.edu.upse.acad.model.repository.MallaRequisitoRepository;
import ec.edu.upse.acad.model.repository.RequisitoRepository;

@RestController
@RequestMapping("/api/requisito")
@CrossOrigin

public class RequisitoController {

	/*
	 * servicios de requisito servivios de mallaRequisito servicios de
	 * AsignaturaRequisito
	 * 
	 */

	@Autowired
	private RequisitoRepository requisitoRepository;
	@Autowired
	private MallaRequisitoRepository mallaRequisitoRepository;
	@Autowired
	private AsignaturaRequisitoRepository asignaturaRequisitoRepository;
	@Autowired
	private MallaRepository mallaRepository;
	// ****************** SERVICIOS PARA REQUISITO************************//

	// Buscar todos las buscarRequisitos, para listar en angular
	@RequestMapping(value = "/buscarRequisitos", method = RequestMethod.GET)
	public ResponseEntity<?> buscarRequisitos() {
		List<Requisito> _requisitos = requisitoRepository.findAll();
		if (_requisitos.isEmpty() == false) {
			for (int i = 0; i < _requisitos.size(); i++) {
				Requisito requisitos = new Requisito();
				BeanUtils.copyProperties(_requisitos.get(i), requisitos, "mallaRequisitos");
				_requisitos.set(i, requisitos);
			}
		}
		return ResponseEntity.ok(_requisitos);
	}

	// servicio que actualiza o crea un nuevo Requisito
	@RequestMapping(value = "/buscarRequisitoId/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> buscar(@PathVariable("id") Integer id) {
		Requisito requisitossig;
		if (requisitoRepository.findById(id).isPresent()) {
			requisitossig = requisitoRepository.findById(id).get();
			return ResponseEntity.ok(requisitossig);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@RequestMapping(value = "/grabarRequisito", method = RequestMethod.POST)
	public ResponseEntity<?> grabaRequisito(@RequestBody Requisito requisitosiga) {
		Requisito _requisitosiga = new Requisito();
		if (requisitosiga.getId() != null) {
			_requisitosiga = requisitoRepository.findById(requisitosiga.getId()).get();
		}
		BeanUtils.copyProperties(requisitosiga, _requisitosiga);
		requisitoRepository.save(_requisitosiga);
		return ResponseEntity.ok(_requisitosiga);
	}

	@RequestMapping(value = "/grabarMallaRequisito", method = RequestMethod.POST)
	public ResponseEntity<?> grabarMallaRequisito(@RequestBody Malla malla) {
		Requisito _malla = new Requisito();
		if (malla.getId() != null) {
			_malla = requisitoRepository.findById(malla.getId()).get();
		}
		BeanUtils.copyProperties(malla, _malla);
		requisitoRepository.save(_malla);
		return ResponseEntity.ok(_malla);
	}
	// ***********************FIN SERVICIOS PARA REQUISITO ***********************

	// ****************** SERVICIOS PARA MALLA REQUISITO************************//

	/***
	 * Devuelve los horas ya resgitradas en las asignaturas para validar en la malla
	 **/

	@RequestMapping(value = "/listarHorasAsignadasEnMalla/{idMalla}", method = RequestMethod.GET)
	public ResponseEntity<?> listarHorasAsignadasEnMalla(@PathVariable("idMalla") Integer idMalla) {
		return ResponseEntity.ok(mallaRequisitoRepository.listarHorasAsignadasEnMalla(idMalla));
	}

	// Buscar todos las buscar Malla Requisitos, para listar en angular
	@RequestMapping(value = "/buscarMallaRequisitos", method = RequestMethod.GET)
	public ResponseEntity<?> buscarMallaRequisitos() {
		return ResponseEntity.ok(mallaRequisitoRepository.findAll());
	}

	// servicio que actualiza o crea un nuevo Malla Requisito
	@RequestMapping(value = "/buscarMallaRequisitoId/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarMallaRequisito(@PathVariable("id") Integer id) {
		MallaRequisito mallarequisitossig;
		if (mallaRequisitoRepository.findById(id).isPresent()) {
			mallarequisitossig = mallaRequisitoRepository.findById(id).get();
			return ResponseEntity.ok(mallarequisitossig);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	//****************** SERVICIOS PARA ASIGNATURA REQUISITO************************//

	// Buscar todos las buscar ASIGNATURA Requisitos, para listar en angular
	@RequestMapping(value = "/buscarAsignaturaRequisitos", method = RequestMethod.GET)
	public ResponseEntity<?> buscarAsignaturaRequisitos() {
		return ResponseEntity.ok(asignaturaRequisitoRepository.findAll());
	}

	// servicio que actualiza o crea un nuevo Malla Requisito
	@RequestMapping(value = "/buscarAsignaturaRequisitoId/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarAsignaturaRequisito(@PathVariable("id") Integer id) {
		AsignaturaRequisito asignaturarequisitossig;
		if (asignaturaRequisitoRepository.findById(id).isPresent()) {
			asignaturarequisitossig = asignaturaRequisitoRepository.findById(id).get();
			return ResponseEntity.ok(asignaturarequisitossig);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@RequestMapping(value = "/grabarAsignaturaRequisito", method = RequestMethod.POST)
	public ResponseEntity<?> grabaAsignaturaRequisito(@RequestBody AsignaturaRequisito asignaturarequisitosiga) {
		AsignaturaRequisito _asignaturarequisitosiga = new AsignaturaRequisito();
		if (asignaturarequisitosiga.getId() != null) {
			_asignaturarequisitosiga = asignaturaRequisitoRepository.findById(asignaturarequisitosiga.getId()).get();
		}
		BeanUtils.copyProperties(asignaturarequisitosiga, _asignaturarequisitosiga);
		asignaturaRequisitoRepository.save(_asignaturarequisitosiga);
		return ResponseEntity.ok(_asignaturarequisitosiga);
	}

	/**
	 * 
	 * Servicio para grabar Malla-Requisitos
	 *
	 **/
	@RequestMapping(value = "/grabarMallaRequisitos", method = RequestMethod.POST)
	public ResponseEntity<?> grabarMallaRequisitos(@RequestBody Malla malla) {
		mallaRepository.save(malla);
		return ResponseEntity.ok().build();
	}

	/***
	 * Lista los requisitos que pueden asignarse a una asignatura
	 */
	// Buscar todos las buscar Malla Requisitos, para listar en angular
	@RequestMapping(value = "/listarRequisitosPermitidosAsignatura/{idMalla}", method = RequestMethod.GET)
	public ResponseEntity<?> listarRequisitosPermitidosAsignatura(@PathVariable("idMalla") Integer idMalla) {
		return ResponseEntity.ok(mallaRequisitoRepository.listarRequisitosPermitidosAsignatura(idMalla));
	}
	// ***********************FIN SERVICIOS PARA MALLA REQUISITO
	// ***********************

}
