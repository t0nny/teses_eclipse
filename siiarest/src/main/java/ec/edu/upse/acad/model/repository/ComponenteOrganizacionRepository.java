package ec.edu.upse.acad.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.AsignaturaOrganizacion;
import ec.edu.upse.acad.model.pojo.ComponenteOrganizacion;

@Repository
public interface ComponenteOrganizacionRepository 
	extends JpaRepository<ComponenteOrganizacion, Integer>{
	@Query(value="SELECT tc "
			+ "FROM ComponenteOrganizacion AS tc "
			+ "WHERE tc.descripcion LIKE ?1 "
			+ "OR tc.abreviatura LIKE ?1 ")
	List<ComponenteOrganizacion> buscarPorComponenteOrganizacion(String descripcion);

	//componentes de organizacion ligados a un nivel //
		@Query(value="select distinct  CONCAT('NIVEL ',ma.idNivel) as detalle, co.descripcion,tco.abreviatura,co.color from AsignaturaOrganizacion ao " 
				+ "inner join MallaAsignatura ma on ao.idMallaAsignatura = ma.id " 
				+ "inner join ComponenteOrganizacion co on ao.idCompOrganizacion = co.id " 
				+ "inner join TipoCompOrganizacion tco on co.idTipoCompOrganizacion = tco.id " 
				+ "where co.id in (6,8,9)")
		List<CustomObjectComponenteOrganizacion> listarcompasocanivel();
		
		interface CustomObjectComponenteOrganizacion {
			String getDetalle();
			String getNombComp();
			String getTipComp();
			String getColor();
		}
		
		@Query(value="SELECT co.id,co.idTipoCompOrganizacion,co.descripcion,co.abreviatura,co.estado,co.version "
				+ "FROM ComponenteOrganizacion AS co "
				+ "WHERE co.estado = 'A' ")
		List<CustomObjectComponenteOrganizacion1> buscarComponenteOrganizacion();
		
		interface CustomObjectComponenteOrganizacion1{
			Integer getId();
			Integer getIdTipoCompOrganizacion();
			String getDescripcion();
			String getAbreviatura();
			String getEstado();
			Integer getVersion();
		}
		
		@Query(value="SELECT co.id,co.descripcion,co.abreviatura,co.idTipoCompOrganizacion,co.estado,co.version "
				+ "FROM ComponenteOrganizacion co "
				+ "WHERE co.estado = 'A' and co.id = (?1) ")
		List<CustomObjectbuscarComponenteOrganizacionId> buscarPorComponenteOrganizacionid(Integer id);
		
		interface CustomObjectbuscarComponenteOrganizacionId{
			Integer getId();
			String getDescripcion();
			String getAbreviatura();
			Integer getIdTipoCompOrganizacion();
			String getEstado();
			Integer getVersion();
		}
		
}

