package ec.edu.upse.acad.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.Nivel;

@Repository
public interface NivelRepository extends JpaRepository<Nivel, Integer> {
//consulta para buscar todos los niveles
	@Query(value="SELECT n.id as id,n.codigo as codigo,n.descripcion as descripcion,n.descripcionCorta " +
			"as descripcionCorta,n.orden as orden,n.estado as estado,n.version as version " +
			 "FROM Nivel AS n ") 
			//+ "WHERE n.estado = 'I'")
	List<CustomObjectNivel> buscarNivel();
	
	interface CustomObjectNivel{
		Integer getId();
		String getCodigo();
		String getDescripcion();
		String getDescripcionCorta();
		Integer getOrden();
		String getEstado();
		Integer getVersion();
	}
	
	//Buscar todos los niveles, para generar la estructura del kanban para la asignacion de niveles AB 
			@Query(value="select n.descripcion as text,n.orden as dataField,10 as maxItems from Nivel AS n where n.estado='A' and n.orden<=(select m.numNiveles from Malla AS m where m.id = (?1))")
			List<CustomObjectNivelColumnas> buscarNivelColumnas(Integer idMalla);
			interface CustomObjectNivelColumnas{
				String getText();
				Integer getDataField();
				Integer getMaxItems();
			}
}
