package ec.edu.upse.acad.model.repository.vinculacion;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.vinculacion.InstitucionBeneficiaria;

@Repository
public interface InstitucionBeneficiariaRepository extends JpaRepository<InstitucionBeneficiaria, Integer>{

}
