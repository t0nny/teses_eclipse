package ec.edu.upse.acad.model.pojo.evaluacion;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="eva", name="cab_evaluacion_docente")
@NoArgsConstructor
public class CabEvaluacionDocente {

    @Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_cab_evaluacion_docente")
	@Getter @Setter private Integer id;
	
	//@Column(name="id_persona_cargo_funcion_relacion")
	//@Getter @Setter private Integer idPersonaCargoFuncionrelacion;
	
	//@Column(name="id_persona_cargo_funcion")
	//@Getter @Setter private Integer idPersonaCargoFuncion;
	
	@Column(name="fecha_realizada")
	@Getter @Setter private Timestamp fechaRealizada;

	@Getter @Setter private String estado;
	
//	@Column(name="fecha_ingreso")
//	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;
	
	//RELACIONES
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_persona_cargo_funcion", insertable=true, updatable = true)
	//@JsonIgnore
	@Getter @Setter private PersonaCargoFuncion personaCargoFuncion;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_persona_cargo_funcion_relacion", insertable=true, updatable = true)
	//@JsonIgnore
	@Getter @Setter private PersonaCargoFuncion personaCargoFuncionRelacion;
	

	
	//bi-directional many-to-one association to DetEvaluacionDocente
	@OneToMany(mappedBy="cabEvaluacionDocente", cascade=CascadeType.ALL)
	@Getter @Setter private List<DetEvaluacionDocente> detEvaluacionDocentes;
	
	@PrePersist
	void preInsert() {
	   if (this.estado == null)
	       this.estado = "A";
	}	
 
}
