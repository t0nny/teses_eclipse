package ec.edu.upse.acad.model.pojo.evaluacion;

import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import ec.edu.upse.acad.model.pojo.Cargo;

import ec.edu.upse.acad.model.pojo.Oferta;
import ec.edu.upse.acad.model.pojo.OfertaDocente;
import ec.edu.upse.acad.model.pojo.seguridad.Departamento;
import ec.edu.upse.acad.model.pojo.seguridad.Persona;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="persona_cargo")
@NoArgsConstructor
public class PersonaCargo {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_persona_cargo")
	@Getter @Setter private Integer id;
	
//	@Column(name="id_persona")
//	@Getter @Setter private Integer idPersona;
//	
//	@Column(name="id_cargo")
//	@Getter @Setter private Integer idCargo;

//	@Column(name="id_oferta")
//	@Getter @Setter private Integer idOferta;

//	@Column(name="id_departamento")
//	@Getter @Setter private Integer idDepartamento;

	@Column(name="descripcion")
	@Getter @Setter private String descripcion;
	
	@Column(name="fecha_desde")
	@Getter @Setter private Date fechaDesde;

	@Column(name="fecha_hasta")
	@Getter @Setter private Date fechaHasta;
	
	@Getter @Setter private String estado;
	
//	@Column(name="fecha_ingreso")
//	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;
	
	@Version
	@Getter @Setter private Integer version;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_persona", nullable = false, insertable=false, updatable = false)
	//@JsonIgnore
	@Getter @Setter private Persona persona;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_cargo", nullable = false, insertable=false, updatable = false)
	//@JsonIgnore
	@Getter @Setter private Cargo cargo;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_oferta", nullable = false, insertable=false, updatable = false)
	//@JsonIgnore
	@Getter @Setter private Oferta oferta;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_departamento", nullable = false, insertable=false, updatable = false)
	//@JsonIgnore
	@Getter @Setter private Departamento departamento;
	
	@OneToMany(mappedBy="personaCargo", cascade=CascadeType.ALL)
	@Getter @Setter private List<PersonaCargoFuncion> personaCargoFuncion;
	
	@PrePersist
	void preInsert() {
		if (this.estado == null)
			this.estado = "A";
	}
}
