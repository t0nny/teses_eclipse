package ec.edu.upse.acad.model.pojo.vinculacion;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(schema = "vin", name = "dep_oferta_proyecto")
public class DepOfertaProyecto {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_dep_oferta_proyecto")
	@Getter	@Setter	private Integer id;

	/*@Column(name = "id_proyecto")
	@Getter	@Setter	private Integer idProyecto;*/
	
	@Column(name = "id_departamento_oferta")
	@Getter	@Setter	private Integer idDepartamentoOferta;
	
		@Column(name = "usuario_ingreso_id")
	@Getter	@Setter	private String usuarioIngresoId;

	@Version
	@Getter	@Setter	private Integer version;

	@Getter	@Setter	private String estado;
	
	@ManyToOne
	@JoinColumn(name = "id_proyecto",nullable=false, insertable=true, updatable=true)
	@JsonBackReference
	//@JsonIgnore //tambien funciona
	@Getter	@Setter	private Proyecto proyecto;

}
