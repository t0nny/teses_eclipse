package ec.edu.upse.acad.model.repository.calificaciones;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;



import ec.edu.upse.acad.model.pojo.calificaciones.CalificacionGeneral;




@Repository
public interface CalificacionGeneralRepository extends JpaRepository<CalificacionGeneral, Integer> {

	/*@Query (value="select e.id,concat(p.nombres,' ',p.apellidos),te.descripcion from Estudiante e " +
	"inner join Persona p on e.idPersona = p.id " +
	"inner join TipoEstudiante te on e.idTipoEstudiante = te.id ")
	List<CustomObjectBuscaEstudiante> listarEstudiante();
	
	interface CustomObjectBuscaEstudiante {
		Integer getId();
		String getNombres();
		Integer getDescripcion();
	}*/

	
	/*@Query (value="SELECT  " + 
			"c.id as id,c.descripcion as ciclo " + 
			"FROM CalificacionCiclo cc " + 
			"inner join ReglamentoCiclo  rc on rc.id=cc.idReglamentoCiclo " + 
			"inner join Ciclo  c on c.id=rc.idCiclo " + 
			"inner join CalificacionGeneral  cg on cc.idCalificacionGeneral=cg.id " + 
			"inner join PeriodoAcademico  pa on pa.id = cg.idPeriodoAcademico " )
	List<ObjectoCiclo> listarCiclosPorPeriodo();
	interface ObjectoCiclo {
		Integer getId();
		String getCiclo();
	}*/
	
	
	@Query (value="SELECT  " + 
			"c.id as id,c.descripcion as ciclo " + 
			"FROM CalificacionCiclo cc " + 
			"inner join ReglamentoCiclo  rc on rc.id=cc.idReglamentoCiclo " + 
			"inner join Ciclo  c on c.id=rc.idCiclo " + 
			"inner join CalificacionGeneral  cg on cc.idCalificacionGeneral=cg.id " + 
			"inner join PeriodoAcademico  pa on pa.id = cg.idPeriodoAcademico " )
	List<ObjectoCiclo> listarCiclosPorPeriodo();
	interface ObjectoCiclo {
		Integer getId();
		String getCiclo();
	}
	
	@Query(value= "select ea.id as idEstudianteAsignatura, "+
			" p.identificacion as identificacion,CONCAT(p.apellidos,' ',p.nombres) as nombreEstudiante, " + 
			" pa.id as idPeriodoAcademico ,pa.descripcion as periodoAcademico, " + 
			" a.id as idAsignatura,a.descripcion as asignatura,para.id as idParalelo,para.descripcion as paralelo " +
			" from Persona p  " + 
			" inner join Estudiante e on e.idPersona = p.id " + 
			" inner join EstudianteOferta eo on eo.idEstudiante = e.id " + 
			" inner join EstudianteMatricula em on em.idEstudianteOferta=eo.id " +
			" inner join EstudianteAsignatura ea on ea.idEstudianteMatricula=em.id "+ 
			" inner join DocenteAsignaturaAprend daa on daa.id=ea.idDocenteAsignaturaAprend " + 
			" inner join AsignaturaAprendizaje aa on aa.id=daa.idAsignaturaAprendizaje " + 
			" inner join MallaAsignatura ma on ma.id=aa.idMallaAsignatura " + 
			" inner join Asignatura a on a.id=ma.idAsignatura " + 
			" inner join Malla m on m.id=ma.idMalla " + 
			" inner join PeriodoMalla pm on pm.idMalla=m.id " + 
			" inner join PeriodoAcademico pa on pa.id=pm.idPeriodoAcademico  " + 
			" inner join Paralelo para on para.id=daa.idParalelo " 	+
			" where   pa.id=?1 and ma.id=?2 and para.id=?3 "
			+ "and e.estado='A' and p.estado='AC'"	)
	
	List<objeto> recuperarEstudiantesPorPeriodoAsignaturaMateria (Integer idPeriodoAcademico,Integer idAsignatura, Integer idParalelo
			);
	interface objeto{ 
	    Integer getIdEstudianteAsignatura(); 
	    String getIdentificacion();
	    String getNombreEstudiante();
	    Integer getIdPeriodoAcademico(); 
	    String getPeriodoAcademico();
	    Integer getIdAsignatura(); 
	    String getAsignatura();
	    Integer getIdParalelo(); 
	    String getParalelo();
	}

	@Query(value= "select cg.idPeriodoAcademico as idPeriodoAcademico,pa.descripcion as periodoAcademico, " + 
			"c.id as idCiclo,c.descripcion as ciclo,coa.id as idComponenteAprendizaje,coa.descripcion as componenteAprendizaje"+
			" from CalificacionCiclo cc  " + 
			" inner join ReglamentoCiclo  rc on rc.id=cc.idReglamentoCiclo "+
			" inner join Ciclo  c on c.id=rc.idCiclo   "+
			" inner join CalificacionGeneral  cg on cc.idCalificacionGeneral=cg.id   " +
			" inner join PeriodoAcademico  pa on pa.id = cg.idPeriodoAcademico "+
			" inner join CicloAprendizaje  ca on ca.idReglamentoCiclo = rc.id "+
			" inner join ReglamentoCompAprendizaje rca on rca.id=ca.idReglamentoCompAprendizaje "+
			" inner join ComponenteAprendizaje coa on coa.id = rca.idCompAprendizaje"+
			" where   pa.id=?1  "	)
	
	List<objetoComponentes> recuperarComponentesAprendizaje (Integer idPeriodoAcademico
			);
	interface objetoComponentes{ 
	    Integer getIdPeriodoAcademico(); 
	    String getPeriodoAcademico();
	    Integer getIdCiclo(); 
	    String getCiclo();
	    Integer getIdComponenteAprendizaje(); 
	    String getComponenteAprendizaje(); 
	}	
	
	@Query(value= " select distinct e.id as idEstudiante,p.identificacion as identificacion,"
			+ "     concat(p.apellidos,' ',p.nombres) as nombreCompleto, " + 
			"		pa.id as idPeriodoAcademico,pa.descripcion as periodoAcademico," + 
			"		daa.id as idDocenteAsignaturaAprend,"+
			"		a.id as idAsignatura,a.descripcion as asignatura,"+ 
			"       para.id as idParalelo,para.descripcion as paralelo, " + 
			"		c.id as idCiclo,c.descripcion as ciclo, ac.id as id,  "+
			"       (select acca.id as idActaCalificacion from ActaCalificacion acca where acca.id=ecc.idActaCalificacion and acca.idCiclo=c.id and acca.idCalificacionGeneral=cg.id and acca.idDocenteAsignaturaAprend=daa.id group by acca.id) as idActaCalificacion, "+
			"       coa.id as idComponenteAprendizaje,coa.descripcion as componenteAprendizaje ,"+
			"       isnull(ecc.id,0) as idEstudianteCalificacion,isnull(ecc.calificacion,0) as calificacion ,ca.ponderacion_calificacion as ponderacion ,ecc.estado as estado, ecc.version as version" +
			
			" 		FROM Persona p" + 
			
			"		inner join Estudiante e on e.idPersona=p.id" + 
			"		inner join EstudianteOferta eo on eo.idEstudiante=e.id" + 
			"		inner join EstudianteMatricula em on em.idEstudianteOferta=eo.id" + 
			"		inner join EstudianteAsignatura ea on ea.idEstudianteMatricula=em.id" + 
			"		inner join DocenteAsignaturaAprend daa on daa.id= (" +
			
			"		SELECT _daa.id FROM DocenteAsignaturaAprend _daa " + 
			"		inner join DistributivoDocente dd on dd.id =_daa.idDistributivoDocente" + 
			"		inner join Docente d on d.id = dd.idDocente " + 
			"		inner join DistributivoOfertaVersion dov on dov.id = dd.idDistributivoOfertaVersion " + 
			"		inner join DistributivoOferta do on do.id = dov.idDistributivoOferta " + 
			"		inner join DistributivoGeneralVersion dgv on dgv.id = do.idDistributivoGeneralVersion" + 
			"		inner join DistributivoGeneral dg on dg.id = dgv.idDistributivoGeneral" + 
			"		inner join AsignaturaAprendizaje aa on aa.id = _daa.idAsignaturaAprendizaje	" + 
			"		where d.id=?4 and dg.idPeriodoAcademico=?1 and aa.idMallaAsignatura=?2 and _daa.estado='A')"+//ea.idDocenteAsignaturaAprend" + 
			
			"		inner join AsignaturaAprendizaje aa on aa.id=daa.idAsignaturaAprendizaje" + 
			
			"		inner join MallaAsignatura ma on ma.id=aa.idMallaAsignatura" + 
			"		inner join Asignatura a on a.id=ma.idAsignatura" + 
			"		inner join Malla m on m.id=ma.idMalla" + 
			"		inner join PeriodoMalla pm on pm.idMalla=m.id" + 
			"		inner join PeriodoAcademico pa on pa.id=pm.idPeriodoAcademico " + 
			"		inner join Paralelo para on para.id=daa.idParalelo" + 
			
			"		inner join CalificacionGeneral cg on cg.idPeriodoAcademico=pa.id" + 
			"		inner join CalificacionCiclo as cc on cc.idCalificacionGeneral=  cg.id" + 
			"		inner join ReglamentoCiclo as rc on rc.id=cc.idReglamentoCiclo" + 
			"		inner join Ciclo as c on c.id=rc.idCiclo" + 
			
			"		inner join CicloAprendizaje ca on ca.idReglamentoCiclo = rc.id" + 
			"		inner join ReglamentoCompAprendizaje rca on rca.id=ca.idReglamentoCompAprendizaje" + 
			"		inner join ComponenteAprendizaje coa on coa.id = rca.idCompAprendizaje" + 
			
			"		left join ActaCalificacion ac on ac.idCalificacionGeneral=cg.id" +
			"		and ac.idCiclo=c.id " + 
			"		and ac.idDocenteAsignaturaAprend=daa.id"+
			"		left join EstudianteCalificacion ecc on ecc.idEstudiante=e.id and ecc.idComponenteAprendizaje=coa.id" + 
			"		and ecc.idActaCalificacion=ac.id "+			
			"		where e.estado='A' and p.estado='AC' and pa.id=?1 and ma.id=?2 and para.id=?3 "
			+ "     order by concat(p.apellidos,' ',p.nombres) ")
	
	List<est> recuperarEstudiantes(Integer idPeriodoAcademico,Integer idAsignatura,Integer idParalelo,Integer idDocente);
	interface est{ 
		Integer getIdEstudiante(); 
		String getIdentificacion(); 
	    String getNombreCompleto();
	    Integer getIdCiclo(); 
	    String getCiclo();
	   
	    Integer getIdActaCalificacion(); 
	    Integer getIdComponenteAprendizaje(); 
	    String getComponenteAprendizaje();
	    Integer getIdEstudianteCalificacion();
	    Integer getCalificacion();
	    Integer getPonderacion();
	}	

	/*
	 * 
	 * SELECT _daa.id_docente_asignatura_aprend FROM aca.docente_asignatura_aprend _daa-- where _daa.
		inner join aca.distributivo_docente dd on dd.id_distributivo_docente=_daa.id_distributivo_docente
		inner join aca.docente d on d.id_docente=dd.id_docente
		inner join aca.distributivo_oferta_version dov on dov.id_distributivo_oferta_version=dd.id_distributivo_oferta_version
		inner join aca.distributivo_oferta do on do.id_distributivo_oferta=dov.id_distributivo_oferta 
		inner join aca.distributivo_general_version dgv on dgv.id_distributivo_general_version=do.id_distributivo_general_version
		inner join aca.distributivo_general dg on dg.id_distributivo_general=dgv.id_distributivo_general
		inner join aca.asignatura_aprendizaje aa on aa.id_asignatura_aprendizaje=_daa.id_asignatura_aprendizaje	
		where d.id_docente=3 and dg.id_periodo_academico=5 and aa.id_malla_asignatura=4
	 * */
	
	
	
	@Query(value=	"       select distinct rca.id as displayfield,CONCAT(CAST(c.id as text),coa.descripcion) as text, "
			+ "  CONCAT(CAST(c.id as text),coa.descripcion) as datafield"+// ea.id as idEstudianteAsignatura,"+
			
			" 		FROM Persona p" + 
			
			"		inner join Estudiante e on e.idPersona=p.id" + 
			"		inner join EstudianteOferta eo on eo.idEstudiante=e.id" + 
			"		inner join EstudianteMatricula em on em.idEstudianteOferta=eo.id" + 
			"		inner join EstudianteAsignatura ea on ea.idEstudianteMatricula=em.id" + 
			"		inner join DocenteAsignaturaAprend daa on daa.id=ea.idDocenteAsignaturaAprend" + 
			"		inner join AsignaturaAprendizaje aa on aa.id=daa.idAsignaturaAprendizaje" + 
			
			"		inner join MallaAsignatura ma on ma.id=aa.idMallaAsignatura" + 
			"		inner join Asignatura a on a.id=ma.idAsignatura" + 
			"		inner join Malla m on m.id=ma.idMalla" + 
			"		inner join PeriodoMalla pm on pm.idMalla=m.id" + 
			"		inner join PeriodoAcademico pa on pa.id=pm.idPeriodoAcademico " + 
			"		inner join Paralelo para on para.id=daa.idParalelo" + 
			
			"		inner join CalificacionGeneral cg on cg.idPeriodoAcademico=pa.id" + 
			"		inner join CalificacionCiclo as cc on cc.idCalificacionGeneral=  cg.id" + 
			"		inner join ReglamentoCiclo as rc on rc.id=cc.idReglamentoCiclo" + 
			"		inner join Ciclo as c on c.id=rc.idCiclo" + 
			
			"		inner join CicloAprendizaje as ca on ca.idReglamentoCiclo = rc.id" + 
			"		inner join ReglamentoCompAprendizaje rca on rca.id=ca.idReglamentoCompAprendizaje" + 
			"		inner join ComponenteAprendizaje coa on coa.id = rca.idCompAprendizaje" + 
			
			"		left join EstudianteCalificacion ec on ec.idEstudiante=e.id" + 
			"		where e.estado='A' and p.estado='AC' and pa.id=?1 and a.id=?2 and para.id=?3 	")
	List<objetoCompon> recuperarComponentes(Integer idPeriodoAcademico ,Integer idAsignatura, Integer idParalelo);
	interface objetoCompon{ 
		String getDisplayfield();
		String getDatafield();
	    String getText();
	}	

	@Query(value=	"       select  coa.codigo as text, "
			+ "  CONCAT(CAST(c.id as text),CAST(coa.id as text),coa.descripcion) as datafield, c.id as columngroup,c.id as displayfield"+// ea.id as idEstudianteAsignatura,"+
			
			" 		FROM CalificacionCiclo cc" + 
			"		inner join ReglamentoCiclo as rc on rc.id=cc.idReglamentoCiclo" + 
			"		inner join Ciclo as c on c.id=rc.idCiclo" + 
			"		inner join CalificacionGeneral as cg on cc.idCalificacionGeneral=cg.id "+
			"		inner join PeriodoAcademico as pa on pa.id = cg.idPeriodoAcademico"+
			"		inner join CicloAprendizaje as ca on ca.idReglamentoCiclo = rc.id" + 
			"		inner join ReglamentoCompAprendizaje rca on rca.id=ca.idReglamentoCompAprendizaje" + 
			"		inner join ComponenteAprendizaje coa on coa.id = rca.idCompAprendizaje" + 
			"		where coa.estado='A' and pa.id=?1 "+
			"		order by c.id	")
	List<Compon> recuperarCiclosComponentes(Integer idPeriodoAcademico);
	interface Compon{ 
		
		String getDatafield();
	    String getText();
	    String getColumngroup();
	   
	}	
	
	
	@Query(value=	"       select distinct  c.descripcion as text, "
			+ "  c.id as name"+// ea.id as idEstudianteAsignatura,"+
			
			" 		FROM CalificacionCiclo cc" + 
			"		inner join ReglamentoCiclo as rc on rc.id=cc.idReglamentoCiclo" + 
			"		inner join Ciclo as c on c.id=rc.idCiclo" + 
			"		inner join CalificacionGeneral as cg on cc.idCalificacionGeneral=cg.id "+
			"		inner join PeriodoAcademico as pa on pa.id = cg.idPeriodoAcademico"+
			"		inner join CicloAprendizaje as ca on ca.idReglamentoCiclo = rc.id" + 
			"		inner join ReglamentoCompAprendizaje rca on rca.id=ca.idReglamentoCompAprendizaje" + 
			"		inner join ComponenteAprendizaje coa on coa.id = rca.idCompAprendizaje" +
			"		where c.estado='A' and pa.id=?1 ")
	List<ciclos> recuperarCiclos(Integer idPeriodoAcademico);
	interface ciclos{ 		
	    String getText();
	    String getName();
	}	
	

	@Query(value=	" select  c.id as idCiclo,c.descripcion as Ciclo" + 
			"	from CalificacionCiclo cc " + 
			"	inner join CalificacionGeneral cg on cc.idCalificacionGeneral=cg.id " + 
			"	inner join ReglamentoCiclo rc on cc.idReglamentoCiclo=rc.id " + 
			"	inner join Ciclo c on c.id = rc.idCiclo " + 
			"	where cg.idPeriodoAcademico=?1 " + 
			"	and cc.fechaDesde<=CONVERT (date, CURRENT_TIMESTAMP) " + 
			"	and CONVERT (date, CURRENT_TIMESTAMP)   <= cc.fechaHasta ")
	List<cicloVi> recuperarCicloVigente(Integer idPeriodoAcademico);
	interface cicloVi{ 		
	    Integer getIdCiclo();
	    String getCiclo();
	}	
}
 