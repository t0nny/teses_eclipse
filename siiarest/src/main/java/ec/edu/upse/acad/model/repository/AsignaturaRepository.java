package ec.edu.upse.acad.model.repository;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.upse.acad.model.pojo.Asignatura;
import ec.edu.upse.acad.model.pojo.Oferta;

@Repository
public interface AsignaturaRepository extends JpaRepository<Asignatura, Integer>{
	@Transactional	
	@Procedure(procedureName  ="rptDocenteAsignaturaPeriodoDesde")
	List<Asignatura> rptDocenteAsignaturaPeriodoDesde(@Param("periodo") Integer idPeriodo,@Param("asignatura") String asignatura);
	/*List<DocenteAsignaturaPeriodo> DocenteAsignaturaPeriodo();
	interface DocenteAsignaturaPeriodo{
		Integer getIdOPeriodoAcademico();
		String getCodigoPeriodo();
		String getPeriodoAcademico();
		String getOfertaDescripCortaDist();
		String getOfertaDescripcionDist();
		String getIdentificacion();
		String getApellidosNombres();
		Integer getIdDocente();
		Integer getIdDistributivoDocente();
		String getOferta();
		String getParalelo();
		String getAsignaturaAprendizaje();
		Double getHoraAsignatura();
		Double getTotalHorasClases();
		Integer getHorasMaximos();
	}*/




	//*****************************************************************************************************************************************************	
	// CONSULTAS UTILIZADAS PARA EL MODULO ASIGNATURAS EN ANGULAR		
	//------------ CONSULTAS USADAS EN EL COMPONENTE nueva-asignatura EN ANGULAR
	//query que busca una ofertaAsignatura por medio del id de una asignatura
	@Query(value="SELECT "
			+ "oa.id as idOferAsignatura,"
			+ "o.id as idOferta,"
			+ "o.descripcion as carrera,"
			+ "a.id as id,"
			+ "a.codigo as codigo,"
			+ "a.descripcion as descripcion,"
			+ "a.descripcionCorta as descripcionCorta,"
			+ "a.tipoAsignatura as tipoAsignatura,"
			+ "a.fechaDesde as fechaDesde,"
			+ "a.fechaHasta as fechaHasta " 
			+ "FROM OfertaAsignatura oa " 
			+ "JOIN Asignatura a ON oa.asignatura.id = a.id " 
			+ "JOIN Oferta o ON oa.oferta.id = o.id " 
			+ "WHERE a.id = ?1 AND a.estado = 'A' AND oa.estado = 'A' AND o.estado = 'A' ")
	CustomObjectOfertasAsignaturas buscarAsignatura(Integer idAsignatura);
	interface CustomObjectOfertasAsignaturas { 
		Integer getIdOferAsignatura(); 
		Integer getIdOferta();
		String getCarrera();
		Integer getId();
		String getCodigo();
		String getDescripcion();
		String getDescripcionCorta();
		String getTipoAsignatura();
		Date getfechaDesde();
		Date getFechaHasta();
	}

	//------------ CONSULTAS USADAS EN EL COMPONENTE nueva-asignatura-comun EN ANGULAR
	//BUSCA UNA ASIGNATURA A MODO DE JSON CON LOS DATOS NECESARIOS
	@Query(value="select distinct "
			+ "a.id as id, "
			+ "a.codigo as codigo, "
			+ "a.tipoAsignatura as tipoAsignatura, "
			+ "a.descripcion as descripcion, "
			+ "a.descripcionCorta as descripcionCorta,"
			+ "a.fechaDesde as fechaDesde,"
			+ "a.fechaHasta as fechaHasta,"
			+ "oa.oferta.id as idOferta, oa.estado as estado "
			+ "from Asignatura a "
			+ "join OfertaAsignatura oa on oa.asignatura.id = a.id "
			+ "WHERE a.id = ?1 and oa.estado='A' and a.estado='A' "
			)
	List<CO_As> buscaAsignaturaById(Integer id);
	interface CO_As{
		Integer getId();
		String getCodigo();
		String getTipoAsignatura();
		String getDescripcion();
		String getDescripcionCorta();
		Date getFechaDesde();
		Date getFechaHasta();
		ArrayList<CO_Oa> getOfertaAsignatura();
	}
	interface CO_Oa{
		Integer getIdOferta();
		String getEstado();
	}	

	//------------ CONSULTAS EN COMUN USADAS EN LOS COMPONENTES nueva-asignatura-comun, asignatura-vicerrectorado, oferta-asignatura, nueva-asignatura EN ANGULAR
	//LISTA TODAS LAS CARRERAS DE ACUERDO AL DEPARTAMENTO-FACULTAD A LA QUE PERTENECE ---------> DEBE FILTRARSE POR EL ID DEL USUARIO
	@Query(value="SELECT o.id as idOferta,o.descripcion as carrera "
			+ "FROM Oferta as o "
			+ "JOIN DepartamentoOferta as do on do.oferta.id = o.id "
			+ "JOIN Departamento as d on do.departamento.id = d.id "
			+ "where o.estado = 'A' and d.id=?1 ")					
	List<CustomObjectOfertas> listarOfertasPorFacultad(Integer idFacultad);
	interface CustomObjectOfertas{
		Integer getIdOferta();
		String getCarrera();

	}		

	//BUSCA EL NOMBRE DE UNA ASIGNATURA CON RESPECTO A UNA CARRERA
	@Query(value="select a.id as idAsignatura ,o.id as idOfertas,a.descripcion as descripcion,a.estado as estado "
			+ "from Asignatura a "
			+ "join OfertaAsignatura oa on a.id = oa.asignatura.id "
			+ "join Oferta o on oa.oferta.id = o.id "
			+ "where a.descripcion = ?1 and o.id = ?2")
	List<CustomObjetctDescipcion> buscarDescripcionAsignatura(String descripcion, Integer idOferta);
	interface CustomObjetctDescipcion{
		Integer getIdAsignatura();
		Integer getIdOfertas();
		String getDescripcion();
		String  getEstado();
	}

	//query para verificar si la asignatura esta publicada en mallaversion
	@Query(value="select m.descripcion as descripcion, m.estado as estado, "
			+ "ma.asignatura.id as id, m.id as idMalla, ma.id as idMallaAsignatura "
			+ "from Malla m "
			+ "join MallaAsignatura ma on ma.malla.id = m.id "
			+ "where ma.asignatura.id = ?1 and m.estado = 'P'")
	List<CustomObjectConsultaAsignaturaPublicada> consultarAsignaturaPublicada(Integer idAsignatura);
	interface CustomObjectConsultaAsignaturaPublicada {
		String getDescripcion();
		String getEstado();
		Integer getIdAsignatura();
		Integer getIdMalla();
		Integer getIdMallaAsignatura();
	}

	//query para verificar si el codigo de la asignatura ya existe en la tabla asignatura

	@Query(value = "select a.id as id,a.codigo as codigo, a.estado as estado "
			+ "from Asignatura a "
			+ "where a.codigo = ?1 and a.estado <> 'I' ")
	List<CustomObjectCodigoAsignatura> buscarCodigoAsignatura(String codigo);
	interface CustomObjectCodigoAsignatura{
		Integer getId();
		String getCodigo();
		String getEstado();
	}
	//*****************************************************************************************************************************************************

	/**
	 * @param id
	 * @return
	 */
	@Query(value = "select o from Oferta o where o.id = ?1 and o.estado = 'A'")
	Oferta findByIdOferta(Integer id);

	@Query(value = "select o.descripcion as descripcion, o.id as idOferta,o.prefijo as prefijo "
			+ "from Oferta o where o.id = ?1 and o.estado = 'A'")
	CO_oferta buscarOfertaById(Integer idOferta);
	interface CO_oferta {
		String getDescripcion();
		Integer getIdOferta();
		String getPrefijo();

	}

	@Query(value = "select o.descripcion as descripcion, o.id as idOferta "
			+ "from Oferta o "
			+ "join DepartamentoOferta do on do.idOferta = o.id "
			+ "join Departamento d on d.id = do.idDepartamento "
			+ "where o.estado = 'A' and do.estado='A' and d.estado = 'AC' and d.tipo = 'FAC'")
	List<CO_Carreras> listarCarreras();
	interface CO_Carreras{
		String getDescripcion();
		Integer getIdOferta();
	}
	
	
	@Query(value = " select a.id as idAsignatura, a.descripcion as asignatura, ma.id as idMallaAsignatura,m.id as idMalla,do.idOferta as idOferta  "
			+ " from Asignatura a inner join MallaAsignatura ma on ma.idAsignatura = a.id "
			+ " inner join Malla m on m.id = ma.idMalla "
			+ " inner join DepartamentoOferta do on do.id = m.idDepartamentoOferta "
			+ " where ma.estado='A' and a.estado ='A' and m.estado in ('A','P') and do.estado ='A' and a.id = (?1)")
	List<AsignaturasUsadas> listaAsignaturasComunUtilizadaMalla(Integer idAsignatura);
	interface AsignaturasUsadas{
		Integer getIdAsignatura();
		String getAsignatura();
		Integer getIdMallaAsignatura();
		Integer getIdMalla();
		Integer getIdOferta();
	}
}
