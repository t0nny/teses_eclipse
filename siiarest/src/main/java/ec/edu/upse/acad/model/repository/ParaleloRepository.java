package ec.edu.upse.acad.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ec.edu.upse.acad.model.pojo.Paralelo;

public interface ParaleloRepository extends JpaRepository<Paralelo, Integer>{
	
	@Query(value="SELECT p.id as id, p.descripcionCorta as label, p.estado as state FROM Paralelo as p ")
	 List<CustomObjectParalelo> listaParalelo();
    
	interface CustomObjectParalelo { 
	  Integer getId(); 
	  String getLabel();
	  String getState();
	} 



}