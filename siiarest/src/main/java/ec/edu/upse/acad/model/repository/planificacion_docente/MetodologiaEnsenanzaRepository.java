package ec.edu.upse.acad.model.repository.planificacion_docente;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.planificacion_docente.MetodologiaEnsenanza;

@Repository
public interface MetodologiaEnsenanzaRepository  extends JpaRepository<MetodologiaEnsenanza, Integer>{
@Query(value="select m.id as idMetodologiaEnsenanza, m.codigo as codigo, m.descripcion as metodologia "+
			" from MetodologiaEnsenanza m ")
	List<CoListadoMetodologias> listaMetodologiasEnsenanza();
	interface CoListadoMetodologias{ 
	    Integer getIdMetodologiaEnsenanza();   
	    String getCodigo();
	    String getMetodologia();
	}
	
}
