package ec.edu.upse.acad.ws;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.upse.acad.model.pojo.Periodo;
import ec.edu.upse.acad.model.pojo.PeriodoAcademico;

import ec.edu.upse.acad.model.repository.OfertaRepository;
import ec.edu.upse.acad.model.repository.PeriodoAcademicoRepository;
import ec.edu.upse.acad.model.service.PeriodoAcademicoService;
import ec.edu.upse.acad.model.service.UpdateForIdService;

@RestController
@RequestMapping("/api/periodoacademico")
@CrossOrigin

public class PeriodoAcademicoController {
	@Autowired private PeriodoAcademicoRepository periodoacademicoRepository;
	@Autowired private UpdateForIdService eliminaPeriodoAcademico;
	@Autowired private PeriodoAcademicoService periodoAcademicoService;
	@Autowired private OfertaRepository ofertaRepository;
	//****************** SERVICIOS PARA PERIODO ACADEMICO ************************//

	/*****
	 * 	retorna una lista de periodo academico utilizada en la pantalla de reportes
	 * @param Authorization
	 * @return  
	 */
	@RequestMapping(value="/buscarPeriodoAcademico", method=RequestMethod.GET)
	public ResponseEntity<?> buscarPeriodoAcademico() {
		return ResponseEntity.ok(periodoacademicoRepository.buscarPeriodoAcademico());
	}

	//servicio que actualiza o crea un nuevo Periodo Academico	
	@RequestMapping(value="/buscarPeriodoAcademicoId/{idPeriodoAcademico}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarPeriodoAcademicoId(@PathVariable("idPeriodoAcademico") Integer idPeriodoAcademico) {
		PeriodoAcademico periodoacademico;
		if (periodoacademicoRepository.findById(idPeriodoAcademico).isPresent()) {
			periodoacademico = periodoacademicoRepository.findById(idPeriodoAcademico).get();
			return ResponseEntity.ok(periodoacademico);
		}else {
			return ResponseEntity.notFound().build();
		}
	}
	//Servicio que busca por nombre
	@RequestMapping(value="/buscarPorNombre/{nombre}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarPorNombre(@PathVariable("nombre") String nombre) {
		return ResponseEntity.ok(periodoacademicoRepository.buscarPorPeriodoAcademico("%" + nombre + "%"));
	}
	//Servicio de grabar Periodo Academico
	@RequestMapping(value="/grabarPeriodoAcademico", method=RequestMethod.POST)
	public ResponseEntity<?> grabarPeriodoAcademico(@RequestBody PeriodoAcademico periodoAcademico) {
		periodoAcademicoService.grabarPeriodoAcademico(periodoAcademico);
		return ResponseEntity.ok(periodoAcademico);
	}
	

	@RequestMapping(value="/borrar/{idPeriodoAcademico}", method=RequestMethod.DELETE)
	public ResponseEntity<?> borrar(@PathVariable("idPeriodoAcademico") Integer id) {
		eliminaPeriodoAcademico.borraPeriodoAcademico(id);
		return ResponseEntity.ok().build();
	}

	//Buscar todos los PeriodoAcademico, para listar en angular
/*	@RequestMapping(value="/buscarOferta", method=RequestMethod.GET)
	public ResponseEntity<?> buscarOferta() {
		return ResponseEntity.ok(ofertaRepository.listaOfertaPre());
	}*/

	//Buscar todos los PeriodoAcademico, para listar en angular
	@RequestMapping(value="/buscarPeriodoAcademicovsperiodo", method=RequestMethod.GET)
	public ResponseEntity<?> buscarPeriodoAcademicovsperiodo() {
		return ResponseEntity.ok(periodoacademicoRepository.buscarPeriodoAcademicovsperiodo());
	}

	//Cargar pantalla de periodo academico nuevo
	@RequestMapping(value="/filtrarPeriodoAcademicoPorId/{id}", method=RequestMethod.GET)
	public ResponseEntity<?> filtrarPeriodoAcademicoPorId(@PathVariable("id") Integer id) {
		return ResponseEntity.ok(periodoacademicoRepository.NuevoPeriodoAcademicoModalidadTipOferta(id));
	}

	@RequestMapping(value="/filtrarPeriodoAcadId/{id}", method=RequestMethod.GET)
	public ResponseEntity<?> filtrarPeriodoAcadId(@PathVariable("id") Integer id) {
		PeriodoAcademico _periodoacademico;
		if (periodoacademicoRepository.findById(id).isPresent()) {
			_periodoacademico = periodoacademicoRepository.findById(id).get();
			return ResponseEntity.ok(_periodoacademico);
		}else {
			return ResponseEntity.notFound().build();
		}
	}

	@RequestMapping(value="/filtrarPeriodoAcadDesde/{id}", method=RequestMethod.GET)
	public ResponseEntity<?> filtrarPeriodoAcadDesde(@PathVariable("id") Integer id) {
		return ResponseEntity.ok(periodoacademicoRepository.buscarPorPeriodoAcademicoDesde(id));
	}

	//para recuperar el ultimo periodo vigente de en caso de un registro nuevo
	@RequestMapping(value="/recuperarUltimoPeriodoVigente", method=RequestMethod.GET)
	public ResponseEntity<?> recuperarUltimoPeriodoVigente() {
		return ResponseEntity.ok(periodoacademicoRepository.recuperarUltimoPeriodoVigente());
	}

	//servicio para cargar periodoacademico filtrado por la descripcion de periodo
	@RequestMapping(value="/filtrarperiodoacademico", method=RequestMethod.GET)
	public ResponseEntity<?> filtrarperiodoacademico() {
		return ResponseEntity.ok(periodoacademicoRepository.filtrarperiodoacademico());
	}

	//Ultimo IdPeriodoAcademico
	@RequestMapping(value="/ultimoPeriodoAcademico", method=RequestMethod.GET)
	public ResponseEntity<?> ultimoPeriodoAcademico() {
		return ResponseEntity.ok(periodoacademicoRepository.ultimoPeriodoAcademico());
	}

}