package ec.edu.upse.acad.model.pojo.evaluacion;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="eva", name="opcion_respuesta")
@NoArgsConstructor
public class OpcionRespuesta {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_opcion_respuesta")
	@Getter @Setter private Integer id;

//	@Column(name="id_periodo_evaluacion")
//	@Getter @Setter private Integer idPeriodoEvaluacion;
	
	@Getter @Setter private Integer valor;

	//@Column(name="cualitativo")
	@Getter @Setter private String descripcion;
	
	//@Column(name="cualitativo")
	@Getter @Setter private String codigo;

	@Getter @Setter private String estado;
	
//	@Column(name="fecha_ingreso")
//	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;
	
	@Version
	@Getter @Setter private Integer version;
	

	//RELACIONES
	//bi-directional many-to-one association to 
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="id_periodo_evaluacion", insertable=false, updatable = false)
//	@JsonIgnore
//	@Getter @Setter private PeriodoEvaluacion periodoEvaluacion;

	//bi-directional many-to-one association to 
	@OneToMany(mappedBy="idOpcionRespuesta", cascade=CascadeType.ALL)
	@JsonIgnore
	@Getter @Setter private List<OpcionPregunta> opcionPregunta;


	@PrePersist
	void preInsert() {
		if (this.estado == null)
			this.estado = "A";
	}
}
