package ec.edu.upse.acad.model.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


import ec.edu.upse.acad.model.pojo.PeriodoDocenteAsignatura;

@Repository
public interface PeriodoDocenteAsignaturaRepository extends JpaRepository<PeriodoDocenteAsignatura, Integer> {

	@Query(value="SELECT pda.idPeriodoAcademico,pda.codigoPeriodo,pda.periodoAcademico,pda.identificacion,pda.apellidosNombres,"
			+ " pda.idDocente,pda.idDistributivoDocente,pda.totalHorasClases,pda.horasMaximo,pda.asignatura "
			+ " FROM PeriodoDocenteAsignatura pda " 
			+ " WHERE pda.idPeriodoAcademico>=?1 and pda.asignatura like %?2% "
			+ " order by pda.idPeriodoAcademico desc, pda.apellidosNombres")
	List<customObjetbuscarPeriodoDocenteAsignatura> buscarPeriodoDocenteAsignatura(Integer idPeriodo, String asignatura);
	interface customObjetbuscarPeriodoDocenteAsignatura{
		Integer getIdPeriodoAcademico();
		String getCodigoPeriodo();
		String getPeriodoAcademico();
		String getIdentificacion();
		String getApellidosNombres();
		Integer getIdDocente();
		Integer getIdDistributivoDocente();
		Double getTotalHorasClases() ;
		Integer getHorasMaximo();
		String getAsignatura();
		
	}
	
	@Query(value="SELECT  pda.idPeriodoAcademico as idPeriodoAcademico, pda.carreraCorta as carreraCorta, pda.codigoPeriodo as codigoPeriodo,"
			+ " pda.idDocente as idDocente,pda.apellidosNombres as apellidosNombres,pda.idDistributivoDocente as idDistributivoDocente,"
			+ " pda.idDocenteDedicacion as idDocenteDedicacion,pda.docenteDedicacion as docenteDedicacion, pda.idDocenteCategoria as idDocenteCategoria,"
			+ " pda.docenteCategoria as docenteCategoria, sum (pda.totalHorasClases) as totalHorasClases,pda.horasMaximo as horasMaximo"
			+ " FROM PeriodoDocenteAsignatura pda " 
			+ " WHERE exists (SELECT distinct a.idDocente  FROM PeriodoDocenteAsignatura a "
			+ "	WHERE pda.idDocente=a.idDocente and  a.asignatura like %?1% )"
			+ " and pda.periodoFechaHasta>=cast (CURRENT_TIMESTAMP as date)"
			+ " and pda.horasMaximo>pda.totalHorasClases"
			+ " GROUP BY  pda.idPeriodoAcademico, pda.carreraCorta,pda.codigoPeriodo,  pda.idDocente, pda.apellidosNombres,pda.idDistributivoDocente,pda.idDocenteDedicacion,pda.docenteDedicacion,"
			+ " pda.idDocenteCategoria,pda.docenteCategoria,pda.horasMaximo"
			+ " order by pda.codigoPeriodo desc,  pda.apellidosNombres,pda.carreraCorta")
	List<customObjetbuscarPeriodoDocente> buscarPeriodoDocente(String asignatura);
	interface customObjetbuscarPeriodoDocente{
		Integer getIdPeriodoAcademico();
		String getCarreraCorta();
		String getCodigoPeriodo();
		Integer getIdDocente();
		String getApellidosNombres();
		Integer getIdDistributivoDocente();
		Integer getIdDocenteDedicacion();
		String getDocenteDedicacion();
		Integer getIdDocenteCategoria();
		String getDocenteCategoria();
		Double getTotalHorasClases() ;
		Integer getHorasMaximo();
		
	
	
	}
	
	
	@Query(value="SELECT pda.carreraAsignatura as carreraAsignatura, pda.codigoPeriodo as codigoPeriodo, pda.idDocente as idDocente,"
			+ "pda.idDistributivoDocente as idDistributivoDocente,pda.asignatura as asignatura"
			+ " FROM PeriodoDocenteAsignatura pda " 
			+ " WHERE pda.idPeriodoAcademico>=?1 and pda.asignatura like %?2% "
			+ " and pda.horasMaximo>pda.totalHorasClases"
			+ " GROUP BY  pda.carreraAsignatura, pda.codigoPeriodo, pda.idDocente,pda.idDistributivoDocente,pda.asignatura"
			+ " order by pda.codigoPeriodo desc, pda.carreraAsignatura, pda.asignatura ")
	List<customObjetbuscarPeriodoAsignaturaDocente> buscarPeriodoAsignaturaDocente(Integer idPeriodo, String asignatura);
	interface customObjetbuscarPeriodoAsignaturaDocente{
		String getCarreraAsignatura();
		String getCodigoPeriodo();
		Integer getIdDocente();
		Integer getIdDistributivoDocente();
		String getAsignatura();
	}
}
