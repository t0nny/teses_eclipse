package ec.edu.upse.acad.model.repository.matricula;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.matricula.EstudianteMatricula;

@Repository
public interface EstudianteMatriculaRepository extends JpaRepository<EstudianteMatricula, Long>{

	/***
	 * método que recupera estudiante oferta por periodo academico
	 * @param idPeriodoAcademico
	 * @param idEstudianteOferta
	 * @return
	 */
	
	@Query(value=" select pac.id as idPeriodoAcademico, ema.id as idEstudianteMatricula, est.id as idEstudiante, "+
			" eof.id as idEstudianteOferta, ofe.id as idOferta, ofe.descripcion as oferta "+
			" from PeriodoAcademico as pac "+
			" inner join MatriculaGeneral as mge on pac.id=mge.idPeriodoAcademico "+
			" inner join EstudianteMatricula as ema on mge.id=ema.idMatriculaGeneral "+
			" inner join EstudianteOferta as eof on ema.idEstudianteOferta=eof.id "+
			" inner join Estudiante as est on eof.idEstudiante=est.id "+
			" inner join DepartamentoOferta as do on eof.idDepartamentoOferta=do.id	"+
			" inner join Oferta as ofe on do.idOferta=ofe.id "+
			" where pac.id=(?1) and eof.id=(?2) " + 
			" and pac.estado='A' and mge.estado='A' and ema.estado='A' " +
			" and eof.estado='A' and est.estado='A' and ofe.estado='A' "
			)
	List<CustomObjectMatriculaEstud> MatriculaEstudiantePorPeriodoAcademico(Integer idPeriodoAcademico, Integer idEstudianteOferta);
	/**
	 * 
	 * @author msoriano
	 *
	 */
	interface CustomObjectMatriculaEstud{ 
	    Integer getIdPeriodoAcademico(); 
	    Integer getIdEstudianteMatricula(); 
	    Integer getIdEstudiante(); 
	    Integer getIdEstudianteOferta();
	    Integer getIdOferta();
	    String  getOferta();
	    
	} 
	
	
//	@Query(value=" select distinct e.id as idEstudiante,eo.id as idEstudianteOferta ,em.id as idEstudianteMatricula, o.descripcion as carrera,"+
//			" p.identificacion as identificacion, CONCAT(p.apellidos ,' ', p.nombres) as nombres," +
//			" pa.codigo as ultimoPeriodo,p.apellidos as apellido " + 
//			" from Estudiante e inner join Persona p on e.idPersona = p.id "+
//			" inner join EstudianteOferta eo on e.id = eo.idEstudiante "+
//			" inner join DepartamentoOferta do on eo.idDepartamentoOferta = do.id "+
//			" inner join Oferta o on do.idOferta = o.id "+
//			" inner join EstudianteMatricula em on em.idEstudianteOferta = eo.id "+
//			" inner join EstudianteAsignatura ea on ea.idEstudianteMatricula = em.id "+
//			" inner join DocenteAsignaturaAprend daa on daa.id = ea.idDocenteAsignaturaAprend "+
//			" inner join AsignaturaAprendizaje aa on aa.id = daa.idAsignaturaAprendizaje "+
//			" inner join MallaAsignatura ma on ma.id = aa.idMallaAsignatura "+
//			" inner join MatriculaGeneral mg on mg.id = em.idMatriculaGeneral "+
//			" inner join PeriodoAcademico pa on pa.id = mg.idPeriodoAcademico " + 
//			" where em.id = (SELECT max(em1.id) FROM EstudianteMatricula em1 " + 
//			" where em1.idEstudianteOferta= em.idEstudianteOferta) " + 
//			" order by p.apellidos asc "
//			)
//	List<CustomObject> listarEstudiantesAMatricular();
	
	
	@Query(value = "select d.id_estudiante as idEstudiante, d.id_estudiante_oferta as idEstudianteOferta, d.id_estudiante_matricula as idEstudianteMatricula, " + 
			" d.id_departamento_oferta as idDepartamentoOferta, d.carrera as carrera, d.identificacion as identificacion, d.nombres as nombres, "+
			" d.codigo as ultimoPeriodo, d.perdioCarrera as perdioCarrera, d.sinComprobantes as sinComprobantes from aca.fn_listar_estudiantes_a_matricular "+
			" (:pi_id_departamento_oferta) as d ", nativeQuery = true)
	public List<CustObjListarEstudiantes> fnListarEstudiantesAMatricular(@Param("pi_id_departamento_oferta") Integer idDepartamentoOferta);
	
	public interface CustObjListarEstudiantes{ 
	    Integer getIdEstudiante(); 
	    Integer getIdEstudianteOferta(); 
	    Long getIdEstudianteMatricula(); 
	    Integer getIdDepartamentoOferta(); 
	    String getCarrera(); 
	    String getIdentificacion(); 
	    String getNombres(); 
	    String getUltimoPeriodo();
	    Integer getPerdioCarrera(); 
	    Integer getSinComprobantes();  
	} 
		
//	@Query(value=" select distinct e.id as idEstudiante,eo.id as idEstudianteOferta ,em.id as idEstudianteMatricula,"+
//			" o.descripcion as carrera,p.identificacion as identificacion, CONCAT(p.apellidos ,' ', p.nombres) as nombres," + 
//			" pa.codigo as ultimoPeriodo , p.apellidos as apellido, eo.numeroMatricula  as numeroMatricula" + 
//			" from Estudiante e inner join Persona p on e.idPersona = p.id "+
//			" inner join EstudianteOferta eo on e.id = eo.idEstudiante "+
//			" inner join DepartamentoOferta do on eo.idDepartamentoOferta = do.id "+
//			" inner join Oferta o on do.idOferta = o.id "+
//			" inner join EstudianteMatricula em on em.idEstudianteOferta = eo.id "+
//			" inner join EstudianteAsignatura ea on ea.idEstudianteMatricula = em.id "+
//			" inner join DocenteAsignaturaAprend daa on daa.id = ea.idDocenteAsignaturaAprend "+
//			" inner join AsignaturaAprendizaje aa on aa.id = daa.idAsignaturaAprendizaje "+
//			" inner join MallaAsignatura ma on ma.id = aa.idMallaAsignatura "+
//			" inner join MatriculaGeneral mg on mg.id = em.idMatriculaGeneral "+
//			" inner join PeriodoAcademico pa on pa.id = mg.idPeriodoAcademico " + 
//			" where em.id = (SELECT max(em1.id) FROM EstudianteMatricula em1 " + 
//			" where em1.idEstudianteOferta= em.idEstudianteOferta) and eo.id = (?1) " + 
//			" order by p.apellidos asc ")
//	List<CustomObjectR> recuperarDatosEstudianteMatricular(Integer idEstudianteOferta);
	
	@Query(value = "select d.id_estudiante as idEstudiante, d.id_estudiante_oferta as idEstudianteOferta,d.id_departamento_oferta as idDepartamentoOferta, "+
			" d.carrera as carrera, d.identificacion as identificacion, d.nombres as nombres,d.codigo as ultimoPeriodo," +
			" d.numero_matricula as numeroMatricula from aca.fn_recuperar_datos_estudiante_matricular "+
			" (:pi_id_estudiante_oferta) as d ", nativeQuery = true)
	public List<CustObjeRecuperarDatosEstudiante> fnRecuperarDatosEstudianteMatricular(@Param("pi_id_estudiante_oferta") Integer idEstudianteOferta);
	
	interface CustObjeRecuperarDatosEstudiante{ 
	    Integer getIdEstudiante(); 
	    Integer getIdEstudianteOferta(); 
	    Integer getIdDepartamentoOferta();
	    String getCarrera(); 
	    String getIdentificacion(); 
	    String getNombres(); 
	    String getUltimoPeriodo();
	    String getNumeroMatricula();
	} 
	
	@Query(value=" select distinct em.id as idEstudianteMatricula, CONCAT(p.apellidos ,' ', p.nombres) as nombres,eo.numeroMatricula as numeroMatricula," +
			" pa.codigo as codigoPeriodoAcademico, pa.descripcion as periodoAcademico " + 
			" from Estudiante e inner join Persona p on e.idPersona = p.id "+
			" inner join EstudianteOferta eo on e.id = eo.idEstudiante "+
			" inner join EstudianteMatricula em on em.idEstudianteOferta = eo.id "+
			" inner join EstudianteAsignatura ea on ea.idEstudianteMatricula = em.id "+
			" inner join MatriculaGeneral mg on mg.id = em.idMatriculaGeneral "+
			" inner join PeriodoAcademico pa on pa.id = mg.idPeriodoAcademico "+ 
			" where em.idEstudianteOferta = (?1) " + 
			" order by pa.codigo"
			)
	List<CustomObjectME> listarMatriculasEstudiante(Integer idEstudianteOferta);
	
	interface CustomObjectME{ 
	    Integer getIdEstudianteMatricula(); 
	    String getNombres(); 
	    String getNumeroMatricula();
	    String getCodigoPeriodoAcademico();
	    String getPeriodoAcademico();
	} 
	
	@Query(value=" select distinct em.id as idEstudianteMatricula, a.codigo as codigoAsignatura, "+
			" a.descripcion as nombreAsignatura, CONCAT(cast(n.orden as text),'/',pl.descripcionCorta) as paralelo, n.orden as orden" + 
			" from EstudianteOferta eo  inner join EstudianteMatricula em on em.idEstudianteOferta = eo.id"+
			" inner join EstudianteAsignatura ea on ea.idEstudianteMatricula = em.id "+
			" inner join DocenteAsignaturaAprend daa on daa.id = ea.idDocenteAsignaturaAprend "+
			" inner join Paralelo pl on daa.idParalelo = pl.id  "+
			" inner join AsignaturaAprendizaje aa on aa.id = daa.idAsignaturaAprendizaje"+
			" inner join MallaAsignatura ma on ma.id = aa.idMallaAsignatura "+
			" inner join Asignatura a on a.id = ma.idAsignatura "+
			" inner join Nivel n on n.id = ma.idNivel  " + 
			" where em.idEstudianteOferta = (?1)  " + 
			" order by n.orden desc "
			)
	List<CustomObjectAM> listaAsignaturasMatriculadasPeriodo(Integer idEstudianteOferta);
	
	interface CustomObjectAM{ 
	    Integer getIdEstudianteMatricula(); 
	    String getCodigoAsignatura(); 
	    String getNombreAsignatura();
	    String getParalelo();
	    String getOrden();
	} 
	
	EstudianteMatricula findByIdEstudianteOfertaAndIdMatriculaGeneral(Integer idEstudianteOferta,Integer idMatriculaGeneral);
	
	@Query(value="  SELECT em "
				+ " FROM EstudianteMatricula em  "
				+ " INNER JOIN EstudianteOferta eo on eo.id = em.idEstudianteOferta "
				+ " INNER JOIN MatriculaGeneral mg on mg.id = em.idMatriculaGeneral " 
				+ " INNER JOIN Estudiante est on est.id = eo.idEstudiante "
				+ " INNER JOIN Persona per on per.id = est.idPersona " 
				+ " WHERE per.usuario.usuario = (?1) ")
	EstudianteMatricula findEstudianteMatriculaByUsuarioEstudiante(String usuario);
}
