package ec.edu.upse.acad.model.pojo.vinculacion;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema = "vin", name = "institucion_beneficiaria")

@NoArgsConstructor //un constructorsin argumentos
public class InstitucionBeneficiaria {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_institucion_beneficiaria")
	@Getter	@Setter	private Integer id;

	@Column(name = "razon_social")
	@Getter	@Setter	private String razonSocial;
	
	@Getter	@Setter	private String correo;
	
	@Getter	@Setter	private String direccion;
	
	@Getter	@Setter	private String telefono;
	
	@Column(name = "benef_directos")
	@Getter	@Setter	private String benefDirectos;
	
	@Column(name = "benef_indirectos")
	@Getter	@Setter	private String benefIndirectos;
	
	@Column(name = "usuario_ingreso_id")
	@Getter	@Setter	private String usuarioIngresoId;

	@Version
	@Getter	@Setter	private Integer version;

	@Getter	@Setter	private String estado;
	
	// bi-directional one_to_many association to proyecto
	@ManyToOne
	@JoinColumn(name = "id_proyecto", nullable = false, updatable = false)
	@JsonBackReference
	//@JsonIgnore //tambien funciona
	@Getter	@Setter	private Proyecto proyecto;
	
	// bi-directional one_to_many association to proyectoAreaGeografica
	//@JsonIdentityInfo(generator=ObjectIdGenerators.UUIDGenerator.class, property="@id")
	@ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	//@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name="id_proyecto_area_geo", nullable=false, insertable=true, updatable=true)// insertable y update no son necesarios por defento son true
	@Getter	@Setter	private ProyectoAreaGeografica proyectoAreaGeografica;
		
	// bi-directional one_to_many association to persona
	//@JsonIdentityInfo(generator=ObjectIdGenerators.UUIDGenerator.class, property="@id")  
	@ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "id_proyecto_persona", nullable = false)
	@Getter	@Setter	private ProyectoPersona proyectoPersona;
	
	@PrePersist
	void preInsert() {
		if (this.estado == null)
			this.estado = "A";
	}

	@Override
	public String toString() {
		return "InstitucionBeneficiaria [id=" + id + ", razonSocial=" + razonSocial + ", correo=" + correo
				+ ", telefono=" + telefono + ", benefDirectos=" + benefDirectos + ", benefIndirectos=" + benefIndirectos
				+ ", usuarioIngresoId=" + usuarioIngresoId + ", version=" + version + ", estado=" + estado + "]";
	}
	

}
