package ec.edu.upse.acad.model.pojo;

import javax.persistence.*;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.List;

import ec.edu.upse.acad.model.pojo.reglamento.DedicacionHorasClase;
@Entity
@Table(schema="aca",name="docente_categoria")
@NoArgsConstructor
public class DocenteCategoria {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_docente_categoria")
	@Getter @Setter private Integer id;

	@Getter @Setter private String descripcion;

	@Getter @Setter private String estado;

	@Column(name="fecha_ingreso")
	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="tipo_relacion_laboral")
	@Getter @Setter private String tipoRelacionLaboral;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Getter @Setter private Integer version;


	//bi-directional many-to-one association to HistorialDocente
///	@OneToMany(mappedBy="docenteCategoria")

//	@Getter @Setter private List<DocenteHistorial> historialDocentes;

//	@Getter @Setter private List<DocenteHistorial> docentesHistorial;

    @OneToMany(mappedBy="docenteCategoria", cascade=CascadeType.ALL)
    @Getter @Setter private List<DedicacionHorasClase> dedicacionHorasClase;

	

	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }

}