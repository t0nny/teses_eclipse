package ec.edu.upse.acad.ws;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ec.edu.upse.acad.model.pojo.Itinerario;
import ec.edu.upse.acad.model.pojo.ItinerarioMateria;
import ec.edu.upse.acad.model.repository.ItinerarioMateriaRepository;
import ec.edu.upse.acad.model.repository.ItinerarioRepository;
import ec.edu.upse.acad.model.service.UpdateForIdService;

@RestController
@RequestMapping("/api/itinerarios")
@CrossOrigin
public class ItinerarioController {

	@Autowired private ItinerarioRepository itinerarioRepository;
	@Autowired private ItinerarioMateriaRepository itinerarioMateriaRepository;
	@Autowired private UpdateForIdService eliminarItinerario;
	@Autowired private UpdateForIdService eliminarItinerarioMateria;

	//****************** SERVICIOS PARA ITINERARIOS************************//

	//Buscar todos los itinerarios, para listar en angular

	@RequestMapping(value="/buscarItinerarios", method=RequestMethod.GET)
	public ResponseEntity<?> buscarItinerarios() {
		return ResponseEntity.ok(itinerarioRepository.findAll());
	}
	//Buscar todos los itinerarios, para filtrar por id
	@RequestMapping(value="/buscarItinerariosId/{id}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarItinerariosId(@PathVariable("id") Integer id) {
		Itinerario _itinerario;
		if (itinerarioRepository.findById(id).isPresent()) {
			_itinerario = itinerarioRepository.findById(id).get();
			return ResponseEntity.ok(_itinerario);
		}else {
			return ResponseEntity.notFound().build();
		}
	}	

	//Servicio de grabar itinerarios
	@RequestMapping(value="/grabarItinerario", method=RequestMethod.POST)
	public ResponseEntity<?> grabarItinerario(@RequestBody Itinerario itinerario) {
		Itinerario _itinerario = new Itinerario();
		if (itinerario.getId() != null) {
			_itinerario = itinerarioRepository.findById(itinerario.getId()).get();
		}
		BeanUtils.copyProperties(itinerario, _itinerario);
		itinerarioRepository.save(_itinerario);
		return ResponseEntity.ok(_itinerario);
	}
	
	//Servicio de borrar itinerarios
	@RequestMapping(value="/borrarItinerario/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<?> borrarItinerario(@PathVariable("id") Integer id) {
		eliminarItinerario.borrarItinerario(id);
		return ResponseEntity.ok().build();
	}
	//****************** SERVICIOS PARA ITINERARIOS ************************//

	//****************** SERVICIOS PARA ITINERARIOS MATERIAS************************//

	//Buscar todos los itinerarios materias, para listar en angular
	@RequestMapping(value="/buscarItinerariosMaterias", method=RequestMethod.GET)
	public ResponseEntity<?> buscarItinerariosMaterias() {
		return ResponseEntity.ok(itinerarioMateriaRepository.findAll());
	}
	//Buscar todos los itinerarios materias, para filtrar por id
	@RequestMapping(value="/buscarItinerariosMateriasId/{id}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarItinerariosMateriasId(@PathVariable("id") Integer id) {
		ItinerarioMateria _itinerarioMateria;
		if (itinerarioMateriaRepository.findById(id).isPresent()) {
			_itinerarioMateria = itinerarioMateriaRepository.findById(id).get();
			return ResponseEntity.ok(_itinerarioMateria);
		}else {
			return ResponseEntity.notFound().build();
		}
	}	
	
	//Servicio de grabar itinerarios materias
	@RequestMapping(value="/grabarItinerarioMaterias", method=RequestMethod.POST)
	public ResponseEntity<?> grabarItinerarioMaterias(@RequestBody ItinerarioMateria itinerarioMateria) {
		ItinerarioMateria _itinerarioMateria = new ItinerarioMateria();
		if (itinerarioMateria.getId() != null) {
			_itinerarioMateria = itinerarioMateriaRepository.findById(itinerarioMateria.getId()).get();
		}
		BeanUtils.copyProperties(itinerarioMateria, _itinerarioMateria);
		itinerarioMateriaRepository.save(_itinerarioMateria);
		return ResponseEntity.ok(_itinerarioMateria);
	}
	
	//Servicio de borrar itinerarios materias
	@RequestMapping(value="/borrarItinerarioMateria/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<?> borrarItinerarioMateria(@PathVariable("id") Integer id) {
		eliminarItinerarioMateria.borrarItinerarioMateria(id);
		return ResponseEntity.ok().build();
	}
	//****************** SERVICIOS PARA ITINERARIOS MATERIAS ************************//
}
