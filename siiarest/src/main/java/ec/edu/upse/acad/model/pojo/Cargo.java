package ec.edu.upse.acad.model.pojo;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import ec.edu.upse.acad.model.pojo.evaluacion.PersonaCargo;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="cargo")
@NoArgsConstructor
public class Cargo {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_cargo")
	@Getter @Setter private Integer id;
	 
	 @Getter @Setter private String codigo;

	@Getter @Setter private String descripcion;
	
	@Getter @Setter private String estado;
	
	@Column(name="fecha_ingreso")
	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;
	
	//Relacion uno a uno tabla persona con tabla cargo persona para el modulo evalkuacion docente
	@OneToMany(mappedBy="cargo", cascade=CascadeType.ALL)
	@Getter @Setter private List<PersonaCargo> personaCargoEval;
		
	@PrePersist
	void preInsert() {
	   if (this.estado == null)
	       this.estado = "A";
	}
		    			
}
