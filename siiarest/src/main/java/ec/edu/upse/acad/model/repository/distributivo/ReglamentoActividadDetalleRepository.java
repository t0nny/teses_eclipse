package ec.edu.upse.acad.model.repository.distributivo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.reglamento.ReglamentoActividadDetalle;
import ec.edu.upse.acad.model.repository.distributivo.ReglamentoActividadDocenteRepository.CustomObjectActividadPersonal;



@Repository
public interface ReglamentoActividadDetalleRepository extends JpaRepository<ReglamentoActividadDetalle, Integer> {
	@Query(value=" select  adde.descripcion as actividadDocenteDetalle,adde.orden as orden,"+
			"adde.estado as estadoActividadDocenteDetalle,adde.version as versionActividadDocenteDetalle,"+
			"rad.id as idActividadDetalle, rad.idReglamentoActividad as idReglamentoActividad,"+
			"rad.obligatorioDistributivo as obligatorioDistributivo,rad.estado as estadoReglamentoActividadDetalle,"+
			"rad.version as versionReglamentoActividadDetalle " + 
			"from ActividadDocenteDetalle adde " + 
			"inner join ReglamentoActividadDetalle rad on adde.id=rad.idActividadDetalle  " + 
			"inner join ReglamentoActividadDocente rado on rado.id=rad.idReglamentoActividad  "+
			"where adde.estado='A' and rad.estado='A'  and rado.estado='A' and rado.idReglamento=?1")
	List<CustomObjectActividadDetalle> listarActividadDetalle(Integer idReglamento);

	interface CustomObjectActividadDetalle{ 
		
		String getActividadDocenteDetalle(); 
		Integer getOrden();
		String getEstadoActividadDocenteDetalle();
		Integer getVersionActividadDocenteDetalle(); 
		Integer getIdActividadDetalle(); 
		Integer getIdReglamentoActividad();
		Boolean getObligatorioDistributivo();
		String getEstadoReglamentoActividadDetalle();
		Integer getVersionReglamentoActividadDetalle(); 
		
	} 
	
	
	@Query(value=" SELECT adde.id as idActividadDetalle, adde.descripcion as actividadDocenteDet ,adde.orden as orden," + 
			"rad.id as idReglaActividadDetalle,rad.idReglamentoActividad as idReglamentoActividad,isnull(rad.obligatorioDistributivo,'false') as obligatorioDistributivo,"+
			"isnull(rad.estado,'A') as estadoActividadDocenteDetalle,rad.usuarioIngresoId as usuarioIngresoId ,isnull(rad.version,0) as versionActividadDocenteDetalle ," + 
			" case when rad.id > 0 then 1 else 0 end as reglamentoActividadDetalle, "+
			" (select count (da.id) from DocenteActividad da where da.estado='A' and adde.id=da.idActividadDetalle ) as cantDistributivo "+
			"FROM ActividadDocenteDetalle adde " + 
			"LEFT JOIN ReglamentoActividadDetalle rad on adde.id=rad.idActividadDetalle and rad.estado='A' " + 
			"WHERE adde.id " + 
			"not in(select addo1.id " + 
			"	  from  ActividadDocenteDetalle addo1 " + 
			"	  inner join ReglamentoActividadDetalle rad1 on addo1.id=rad1.idActividadDetalle " + 
			"	  where  addo1.estado='A' and rad1.idReglamentoActividad not in(?1) and rad1.estado='A' ) " + 
			"and adde.estado='A'")
	List<CustomObjectActividadDetallePorActPersonal> listarActividadDetallePorActPersonal(Integer idReglamentoActividad);

	interface CustomObjectActividadDetallePorActPersonal{ 
		Integer getIdActividadDetalle();
		String  getActividadDocenteDet(); 
		Integer getOrden();
		Integer getIdReglaActividadDetalle();
		Integer getIdReglamentoActividad();
		Boolean getObligatorioDistributivo();
		String  getEstadoActividadDocenteDetalle();
		Integer getUsuarioIngresoId(); 
		Integer getVersionActividadDocenteDetalle(); 
		Integer getReglamentoActividadDetalle(); 
		Integer getCantDistributivo();


	} 
	
	
	
}
