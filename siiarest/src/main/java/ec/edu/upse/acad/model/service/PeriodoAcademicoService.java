package ec.edu.upse.acad.model.service;

import java.beans.PropertyDescriptor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jfree.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.upse.acad.model.pojo.Malla;
import ec.edu.upse.acad.model.pojo.Oferta;
import ec.edu.upse.acad.model.pojo.Periodo;
import ec.edu.upse.acad.model.pojo.PeriodoAcademico;
import ec.edu.upse.acad.model.pojo.PeriodoModalidad;
import ec.edu.upse.acad.model.pojo.PeriodoTipoOferta;
import ec.edu.upse.acad.model.pojo.TipoOferta;
import ec.edu.upse.acad.model.pojo.distributivo.DistributivoDocente;
import ec.edu.upse.acad.model.pojo.distributivo.DistributivoGeneral;
import ec.edu.upse.acad.model.pojo.distributivo.DistributivoGeneralVersion;
import ec.edu.upse.acad.model.pojo.distributivo.DistributivoOferta;
import ec.edu.upse.acad.model.repository.OfertaRepository;
import ec.edu.upse.acad.model.repository.OfertaRepository.CustomObjectOferta;
import ec.edu.upse.acad.model.repository.PeriodoAcademicoRepository;
import ec.edu.upse.acad.model.repository.PeriodoModalidadRepository;
import ec.edu.upse.acad.model.repository.PeriodoRepository;
import ec.edu.upse.acad.model.repository.PeriodoTipoOfertaRepository;
import ec.edu.upse.acad.model.repository.TipoOfertaRepository;
import ec.edu.upse.acad.model.repository.distributivo.DistributivoGeneralRepository;
import ec.edu.upse.acad.model.repository.distributivo.DistributivoGeneralVersionRepository;
import ec.edu.upse.acad.model.repository.distributivo.DistributivoGeneralVersionRepository.CustomObjectJ;
import ec.edu.upse.acad.model.repository.distributivo.DistributivoOfertaRepository;


@Service
@Transactional
public class PeriodoAcademicoService {

	@Autowired private PeriodoRepository periodoRepository ;
	@Autowired private PeriodoAcademicoRepository periodoAcademicoRepository ;
	@Autowired private PeriodoTipoOfertaRepository periodoTipoOfertaRepository;
	@Autowired private PeriodoModalidadRepository periodoModalidaRepository;
	@Autowired private DistributivoGeneralRepository distributivoGeneralRepository;
	@Autowired private DistributivoGeneralVersionRepository distributivoGeneraVersionlRepository;
	@Autowired private DistributivoOfertaRepository  distributivoOfertaRepository;
	@Autowired private OfertaRepository ofertaRepository;
	@Autowired private TipoOfertaRepository tipoOfertaRepository;
	
	@PersistenceContext
	private EntityManager em;
	public void grabarPeriodoAcademico(PeriodoAcademico periodoAcademico) {
		PeriodoAcademico _periodo = new PeriodoAcademico();
		boolean editaPer;
		Integer IdPeriodoAcad=0;
		if (periodoAcademico.getId() == null) {
			editaPer=true;
			BeanUtils.copyProperties(periodoAcademico, _periodo, "periodoModalidades","periodoTipoOferta","distributivoGeneral", "peridoMallaVersion");	
			IdPeriodoAcad= periodoAcademicoRepository.saveAndFlush(_periodo).getId();
		}else {
			editaPer=false;
			_periodo = periodoAcademicoRepository.findById(periodoAcademico.getId()).get();
			IdPeriodoAcad=_periodo.getId();
			periodoAcademico.setVersion(periodoAcademico.getVersion()+1);
			BeanUtils.copyProperties(periodoAcademico, _periodo,"distributivoGeneral", "peridoMallaVersion","periodoTipoOferta","periodoModalidades");	
			IdPeriodoAcad= periodoAcademicoRepository.saveAndFlush(_periodo).getId();
		}	
	
			for (int j = 0; j <periodoAcademico.getPeriodoModalidades().size(); j++) {
				PeriodoModalidad _periodoModalidad = new PeriodoModalidad();
				periodoAcademico.getPeriodoModalidades().get(j).setIdPeriodoAcademico(IdPeriodoAcad);
				BeanUtils.copyProperties(periodoAcademico.getPeriodoModalidades().get(j), _periodoModalidad);	
				periodoModalidaRepository.save(_periodoModalidad);
			}
			for (int j = 0; j <periodoAcademico.getPeriodoTipoOferta().size(); j++) {
				PeriodoTipoOferta _periodoTipoOferta=new PeriodoTipoOferta();
				periodoAcademico.getPeriodoTipoOferta().get(j).setIdPeriodoAcademico(IdPeriodoAcad);
				BeanUtils.copyProperties(periodoAcademico.getPeriodoTipoOferta().get(j), _periodoTipoOferta);	
				periodoTipoOfertaRepository.save(_periodoTipoOferta);
			}
			for (int j = 0; j < periodoAcademico.getDistributivoGeneral().size(); j++) {
				DistributivoGeneral _periodoDistribTmp = new DistributivoGeneral();
				periodoAcademico.getDistributivoGeneral().get(j).setIdPeriodoAcademico(IdPeriodoAcad);
				periodoAcademico.getDistributivoGeneral().get(j).setFechaDesde(periodoAcademico.getFechaDesde());
				periodoAcademico.getDistributivoGeneral().get(j).setFechaHasta(periodoAcademico.getFechaHasta());
				BeanUtils.copyProperties(periodoAcademico.getDistributivoGeneral().get(j), _periodoDistribTmp, "distributivoGeneralVersions" );
				Integer iddistributivoGneeral = distributivoGeneralRepository.saveAndFlush(_periodoDistribTmp).getId();	
				for (int k = 0; k < periodoAcademico.getDistributivoGeneral().get(j).getDistributivoGeneralVersions().size(); k++) {
					periodoAcademico.getDistributivoGeneral().get(j).getDistributivoGeneralVersions().get(k).setIdDistributivoGeneral(iddistributivoGneeral);	
					DistributivoGeneralVersion _periodoTmp1 = new DistributivoGeneralVersion();
					BeanUtils.copyProperties( periodoAcademico.getDistributivoGeneral().get(j).getDistributivoGeneralVersions().get(k), _periodoTmp1, "distributivoOfertas");	
					Integer iddistributivoGneeralVersion = distributivoGeneraVersionlRepository.saveAndFlush(_periodoTmp1).getId();
					System.out.println(iddistributivoGneeralVersion);
					if(editaPer==true) { 
						for (int h = 0; h <periodoAcademico.getPeriodoTipoOferta().size(); h++) {
							//periodoAcademico.getPeriodoTipoOferta().get(h).getIdTipoOferta();	
							List<Oferta> listaOferta =ofertaRepository.listaOfertaPre(periodoAcademico.getPeriodoTipoOferta().get(h).getIdTipoOferta());
							for (int n = 0; n < listaOferta.size(); n++) {
								DistributivoOferta distributivoOferta=new DistributivoOferta();
								distributivoOferta.setIdOferta(listaOferta.get(n).getId());
								distributivoOferta.setIdDistributivoGeneralVersion(iddistributivoGneeralVersion);

								distributivoOferta.setUsuarioIngresoId(1);
								distributivoOfertaRepository.save(distributivoOferta);
							}	
						}

					}else {
						for (int n = 0; n < periodoAcademico.getDistributivoGeneral().get(j).getDistributivoGeneralVersions().get(k).getDistributivoOferta().size(); n++) {
							
							distributivoOfertaRepository.save( periodoAcademico.getDistributivoGeneral().get(j).getDistributivoGeneralVersions().get(k).getDistributivoOferta().get(n));
						}	
						
					}
				}
		}
		em.getEntityManagerFactory().getCache().evict(PeriodoAcademico.class);
		em.getEntityManagerFactory().getCache().evict(PeriodoModalidad.class);
		em.getEntityManagerFactory().getCache().evict(PeriodoTipoOferta.class);
		em.getEntityManagerFactory().getCache().evict(DistributivoDocente.class);
		em.getEntityManagerFactory().getCache().evict(DistributivoGeneralVersion.class);
		em.getEntityManagerFactory().getCache().evict(DistributivoOferta.class);
	
		

	}

	public void editarPeriodoAcademico(PeriodoAcademico periodoAcademico) {

		PeriodoAcademico _periodoAcademico = new PeriodoAcademico();
		
		_periodoAcademico = periodoAcademicoRepository.findById(periodoAcademico.getId()).get();
		BeanUtils.copyProperties(periodoAcademico, _periodoAcademico);
		periodoAcademicoRepository.saveAndFlush(_periodoAcademico);
		em.getEntityManagerFactory().getCache().evict(PeriodoAcademico.class);
	}
	
}
