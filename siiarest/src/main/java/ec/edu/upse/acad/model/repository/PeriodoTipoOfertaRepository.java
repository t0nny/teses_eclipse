package ec.edu.upse.acad.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.PeriodoTipoOferta;

@Repository
public interface PeriodoTipoOfertaRepository extends JpaRepository<PeriodoTipoOferta, Integer>{
	
	@Query(value="SELECT pto.id,pto.idTipoOferta,pto.idPeriodoAcademico,pto.estado,pto.version " + 
			"FROM PeriodoTipoOferta pto " + 
			"WHERE pto.estado = 'A' ")
	List<CustomObjectPeriodoTipoOferta> buscarperiodotipooferta();
	
	interface CustomObjectPeriodoTipoOferta {
		Integer getId();
		Integer getidTipoOferta();
		Integer getidPeriodoAcademico();
		String getEstado();
		Integer getVersion();
	}

	
	@Query(value="SELECT pto.id,pto.idTipoOferta,pto.idPeriodoAcademico,pto.estado,pto.version " + 
			"FROM PeriodoTipoOferta pto " + 
			"WHERE pto.estado = 'A' and pto.id = (?1)")
	List<CustomObjectPeriodoTipoOfertaId> buscarperiodotipoofertaId(Integer id);
	
	interface CustomObjectPeriodoTipoOfertaId {
		Integer getId();
		Integer getidTipoOferta();
		Integer getidPeriodoAcademico();
		String getEstado();
		Integer getVersion();
	}
}
