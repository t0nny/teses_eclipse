package ec.edu.upse.acad.model.pojo.matricula;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ec.edu.upse.acad.model.pojo.reglamento.Reglamento;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;


@Entity
@Table(schema="aca", name="reglamento_tipo_matricula")
@NoArgsConstructor
public class ReglamentoTipoMatricula{

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_reglamento_tipo_matricula")
	@Getter @Setter private Integer id;

	@Column(name="id_reglamento")
	@Getter @Setter  private Integer idReglamento;
	
	@Column(name="id_tipo_matricula")
	@Getter @Setter  private Integer idTipoMatricula;
	
	@Getter @Setter private String estado;

	@Column(name="fecha_ingreso")
	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private int version;

	//RELACIONES
	//bi-directional many-to-one association to Reglamento
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_reglamento" , insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Reglamento reglamento;

	//bi-directional many-to-one association to TipoMatricula
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_tipo_matricula" , insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private TipoMatricula tipoMatricula;
	
	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	}
	

}