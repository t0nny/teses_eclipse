package ec.edu.upse.acad.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ec.edu.upse.acad.model.pojo.TipoOferta;

public interface TipoOfertaRepository extends JpaRepository<TipoOferta, Integer> {
	
	/***
	 * retorna la lista de tipo oferta
	 * @return
	 */
	@Query(value="SELECT tof.id as id,tof.descripcion as descripcion,tof.descripcionCorta as descripcionCorta,"+
			"tof.estado as estado,tof.version as version " + 
			"FROM TipoOferta tof " + 
			"WHERE tof.estado = 'A' ")
	List<CustomObjectTipoOferta> buscarTipoOferta();
	
	interface CustomObjectTipoOferta {
		Integer getId();
		String getDescripcion();
		String getDescripcionCorta();
		String getEstado();
		Integer getVersion();
	}

	/***
	 * retorna la lista de oferta filtrando por tipo de oferta
	 * @param id
	 * @return
	 */
	
	@Query(value="SELECT tof.id as id,tof.descripcion as descripcion ,tof.descripcionCorta as descripcionCorta,"+
			"tof.estado as estado,tof.version as version " + 
			"FROM TipoOferta tof " + 
			"WHERE tof.estado = 'A' and tof.id = (?1)")
	List<CustomObjectTipoOfertaId> buscarTipoOfertaId(Integer id);
	
	interface CustomObjectTipoOfertaId {
		Integer getId();
		String getDescripcion();
		String getDescripcionCorta();
		String getEstado();
		Integer getVersion();
	}

	
	/***
	 * retorna la lista de tipo oferta filtrando por abreviatura de la descripcion
	 * @param descripcion
	 * @return
	 */
	@Query(value="SELECT tof.id as id,tof.descripcion as descripcion,tof.descripcionCorta as descripcionCorta,"+
			"tof.estado as estado,tof.version as version " + 
			"FROM TipoOferta tof " + 
			"WHERE tof.estado = 'A' and tof.descripcion LIKE (?1) ")
	List<CustomObjectTipoOfertaDescrip> buscarTipoOfertaDescripcion(String descripcion);
	
	interface CustomObjectTipoOfertaDescrip {
		Integer getId();
		String getDescripcion();
		String getDescripcionCorta();
		String getEstado();
		Integer getVersion();
	}
}
