package ec.edu.upse.acad.model.pojo.vinculacion;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;


import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema = "vin", name = "objetivo_tarea")
@NoArgsConstructor //un constructorsin argumentos
public class ObjetivoTarea {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_objetivo_tarea")
	@Getter	@Setter	private Integer id;
	
	@Column(name = "id_objetivo_actividad")
	@Getter	@Setter	private Integer idObjetivoActividad;
	
	@Getter	@Setter	private String descripcion;
	
	@Column(name = "credito_int")
	@Getter	@Setter	private String creditoInt;
	
	@Column(name = "credito_ext")
	@Getter	@Setter	private String creditoExt;
	
	@Getter	@Setter	private String cooperacion;
	
	@Getter	@Setter	private String autogestion;
	
	@Getter	@Setter	private String fiscales;
	
	@Column(name = "aport_comunit")
	@Getter	@Setter	private String aporteComunit;
	
	@Column(name = "estado_tarea")
	@Getter	@Setter	private String estadoTarea;
	
	@Column(name = "fecha_desde")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm", locale = "es_EC", timezone = "America/Guayaquil")
	//@Temporal(TemporalType.TIMESTAMP)
	@Getter	@Setter	private Timestamp fechaDesde;

	@Column(name = "fecha_hasta")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm", locale = "es_EC", timezone = "America/Guayaquil")
	//@Temporal(TemporalType.TIMESTAMP)
	@Getter	@Setter	private Timestamp fechaHasta;

	
	@Column(name = "usuario_ingreso_id")
	@Getter	@Setter	private String usuarioIngresoId;

	@Version
	@Getter	@Setter	private Integer version;

	@Getter	@Setter	private String estado;
	
	@PrePersist
	void preInsert() {
		if (this.estado == null)
			this.estado = "A";
	}

}
