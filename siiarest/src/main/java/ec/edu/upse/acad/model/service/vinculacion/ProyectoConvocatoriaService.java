package ec.edu.upse.acad.model.service.vinculacion;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.upse.acad.model.pojo.vinculacion.ProyectoConvocatoria;
import ec.edu.upse.acad.model.repository.vinculacion.ProyectoConvocatoriaRepository;

@Service
@Transactional
public class ProyectoConvocatoriaService {

	@Autowired private ProyectoConvocatoriaRepository periodoConvocatoriaRepository;
	@PersistenceContext	private EntityManager em;

	public void nuevoPeriodoConvocatoria(ProyectoConvocatoria periodoConvocatoria) {
		periodoConvocatoriaRepository.saveAndFlush(periodoConvocatoria);
		em.getEntityManagerFactory().getCache().evict(ProyectoConvocatoria.class);
	}

	public void editarPeriodoConvocatoria(ProyectoConvocatoria periodoConvocatoria) {
		ProyectoConvocatoria _periodoConvocatoria = periodoConvocatoriaRepository.findById(periodoConvocatoria.getId()).get();
		if (_periodoConvocatoria != null) {
			BeanUtils.copyProperties(periodoConvocatoria, _periodoConvocatoria);
			periodoConvocatoriaRepository.save(_periodoConvocatoria);
		}
		em.getEntityManagerFactory().getCache().evict(ProyectoConvocatoria.class);
	}

	public void savePeriodoConvocatoria(ProyectoConvocatoria periodoConvocatoria) {
		System.out.println("Periodo Convocatoria servicio");
		System.out.println(periodoConvocatoria.getId());
		if (periodoConvocatoria.getId() != null) {
			System.out.println("editar registro");
			editarPeriodoConvocatoria(periodoConvocatoria);
		} else {
			System.out.println("ingresar registro");
			nuevoPeriodoConvocatoria(periodoConvocatoria);
		}

	}

}
