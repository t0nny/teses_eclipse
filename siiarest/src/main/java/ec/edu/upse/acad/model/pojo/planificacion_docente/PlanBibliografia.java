package ec.edu.upse.acad.model.pojo.planificacion_docente;


import javax.persistence.*;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * The persistent class for the plan_bibliografia database table.
 * 
 */

@Entity
@Table(schema="aca", name="plan_bibliografia")
@Where(clause = "estado='A'")
@NoArgsConstructor
public class PlanBibliografia  {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_plan_bibliografia")
	@Getter @Setter private Long id;
	
	@Column(name="id_plan_clase")
	@Getter @Setter private Long idPlanClase;
	
	@Column(name="id_recurso_bibliografico")
	@Getter @Setter private Long idRecursoBibliografico;

	@Getter @Setter private String estado;

//	@Column(name="fecha_ingreso")
//	private Timestamp fechaIngreso;

	@Column(name="pag_desde")
	@Getter @Setter private Integer pagDesde;

	@Column(name="pag_hasta")
	@Getter @Setter private Integer pagHasta;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;

	//bi-directional many-to-one association to PlanClase
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_plan_clase", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private PlanClase planClase;

	//bi-directional many-to-one association to RecursoBibliografico
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_recurso_bibliografico", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private RecursoBibliografico recursoBibliografico;

	@PrePersist
	void preInsert() {
	   if (this.estado == null)
	       this.estado = "A";
	}
}