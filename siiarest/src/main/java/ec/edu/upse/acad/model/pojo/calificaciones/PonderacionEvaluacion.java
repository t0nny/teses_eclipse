package ec.edu.upse.acad.model.pojo.calificaciones;

import java.util.List;
import javax.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Table(schema="aca", name="ponderacion_evaluacion")
@NoArgsConstructor
public class PonderacionEvaluacion {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_ponderacion_evaluacion")
	@Getter @Setter private Integer id;

	@Getter @Setter private String descripcion;

	@Getter @Setter private String estado;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;

	//bi-directional many-to-one association to ActividadesPonderada
	@OneToMany(mappedBy="ponderacionEvaluacion")
	@Getter @Setter private List<ActividadesPonderada> actividadesPonderadas;

	

}