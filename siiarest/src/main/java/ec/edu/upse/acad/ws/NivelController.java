package ec.edu.upse.acad.ws;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.upse.acad.model.pojo.Nivel;
import ec.edu.upse.acad.model.repository.NivelRepository;
import ec.edu.upse.acad.model.service.UpdateForIdService;

@RestController
@RequestMapping("/api/nivel")
@CrossOrigin
public class NivelController {
	@Autowired private NivelRepository nivelRepository;
	@Autowired private UpdateForIdService eliminarNivel;

	//****************** SERVICIOS PARA NIVEL************************////

	//Buscar todos los niveles, para listar en angular
	@RequestMapping(value="/buscarNiveles", method=RequestMethod.GET)
	public ResponseEntity<?> buscarNiveles() {
		return ResponseEntity.ok(nivelRepository.buscarNivel());
	}

	//Buscar todos los niveles, para generar la estructura del kanban para la asignacion de niveles AB 
	@RequestMapping(value="/buscarNivelesColumna/{idMalla}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarNivelesColumna(@PathVariable("idMalla") Integer id) {
		return ResponseEntity.ok(nivelRepository.buscarNivelColumnas(id));
	}

	//Buscar todos los niveles, para filtrar por id
	@RequestMapping(value="/buscarNivelesId/{id}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarNivelesId(@PathVariable("id") Integer id) {
		Nivel _nivel;
		if (nivelRepository.findById(id).isPresent()) {
			_nivel = nivelRepository.findById(id).get();
			return ResponseEntity.ok(_nivel);
		}else {
			return ResponseEntity.notFound().build();
		}
	}	

	//Servicio de grabar los niveles
	@RequestMapping(value="/grabarNivel", method=RequestMethod.POST)
	public ResponseEntity<?> grabarNivel(@RequestBody Nivel nivel) {
		Nivel _nivel = new Nivel();
		if (nivel.getId() != null) {
			_nivel = nivelRepository.findById(nivel.getId()).get();
		}
		BeanUtils.copyProperties(nivel, _nivel);
		nivelRepository.save(_nivel);
		return ResponseEntity.ok(_nivel);
	}

	//Servicio de borrar los niveles
	@RequestMapping(value="/borrarNivel/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<?> borrarNivel(@PathVariable("id") Integer id) {
		eliminarNivel.borrarNivel(id);
		return ResponseEntity.ok().build();
	}
	//***********************FIN SERVICIOS PARA NIVEL ***********************

}
