package ec.edu.upse.acad.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ec.edu.upse.acad.model.pojo.DocenteCategoria;


public interface CategoriaRepository extends JpaRepository<DocenteCategoria, Integer>{
	@Query(value="SELECT dc.id as id , dc.descripcion as descripcion, dc.estado as estado"+
			 "  FROM DocenteCategoria dc where dc.estado='A'")
	List<CustomObjectCategoria> listaCategoria();
	interface CustomObjectCategoria { 
		 Integer getId(); 
		 String getDescripcion();
		 String getEstado();
	}
}
