package ec.edu.upse.acad.ws.vinculacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.upse.acad.model.pojo.vinculacion.Programa;
import ec.edu.upse.acad.model.repository.vinculacion.ProgramaRepository;
import ec.edu.upse.acad.model.service.SecurityService;
import ec.edu.upse.acad.model.service.vinculacion.ProgramaService;

@RestController
@RequestMapping("/api/programa")
@CrossOrigin
public class ProgramaController {
	@Autowired
	private SecurityService securityService;
	@Autowired
	private ProgramaRepository programaRepository;
	@Autowired
	private ProgramaService programaService;

	// busca todos activos e inactivos
	@RequestMapping(value = "/buscarTodosProgramas", method = RequestMethod.GET)
	public ResponseEntity<?> buscarTodosProgramas(@RequestHeader(value = "Authorization") String authorization) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(programaRepository.findAll());
	}

	// buscar programa por el id de departamento_oferta
	@RequestMapping(value = "/buscarProgramasIdDepOferta/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarProgramasIdDepOferta(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable("id") Integer id) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(programaRepository.buscarProgramasIdDepOferta(id));
	}

	// buscar proyecto por el id de departamento_oferta
	@RequestMapping(value = "/buscarProyectosIdDepOferta/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarProyectosIdDepOferta(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable("id") Integer id) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(programaRepository.buscarProyectosIdDepOferta(id));
	}

	// buscar por el id de programa
	@RequestMapping(value = "/buscarProgramaPorId/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarProgramaPorId(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable("id") Integer id) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(programaRepository.findById(id).get());
	}

	@RequestMapping(value = "/grabarPrograma", method = RequestMethod.POST)
	public ResponseEntity<?> grabarPrograma(@RequestHeader(value = "Authorization") String Authorization, // definimos
																											// que tenga
																											// autorizacion
			@RequestBody Programa programa) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		programaService.grabarPrograma(programa);
		return ResponseEntity.ok().build();
	}

}
