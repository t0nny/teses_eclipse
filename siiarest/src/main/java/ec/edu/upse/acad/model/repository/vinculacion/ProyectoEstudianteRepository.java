package ec.edu.upse.acad.model.repository.vinculacion;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.vinculacion.ProyectoEstudiante;

@Repository
public interface ProyectoEstudianteRepository extends JpaRepository<ProyectoEstudiante, Integer> {
	@Transactional
	@Query(value = "SELECT p.id as idPersona,(concat(p.nombres ,' ',p.apellidos))as nombres"
			+ " from ProyectoEstudiante pe "
			+ "INNER JOIN Estudiante e on pe.idEstudiante=e.id " 
			+ "INNER JOIN Persona p on e.idPersona=p.id "
			+ "WHERE pe.proyecto.id=(?1) and e.estado='A' AND p.estado='AC' AND pe.estado='A'")
	List<customObjetestudianteProyecto> estudianteProyecto(Integer idProyecto);

	interface customObjetestudianteProyecto {
		Integer getIdPersona();
		String getNombres();		
	}


}
