package ec.edu.upse.acad.model.service.eva;

import java.sql.Date;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.upse.acad.model.pojo.evaluacion.PeriodoEvaluacion;
import ec.edu.upse.acad.model.repository.evaluacion.PeriodoEvaluacionRepository;

@Service
@Transactional
public class PeriodoEvaluacionService {
	@Autowired private PeriodoEvaluacionRepository periodoEvaluacionRepository;
	@PersistenceContext private EntityManager em;
	
	public void savePeriodoEvaluacion(PeriodoEvaluacion periodoEvaluacion){
		if(periodoEvaluacion.getId() != null) {
			this.editaPeriodoEvaluacion(periodoEvaluacion);
		}else {
			this.nuevoPeriodoEvaluacion(periodoEvaluacion);
		}		
	}
	
	public void editaPeriodoEvaluacion(PeriodoEvaluacion periodoEvaluacion) {
		Integer idPeriodoEvaluacion = periodoEvaluacion.getId();
		PeriodoEvaluacion _periodoEvaluacion = periodoEvaluacionRepository.findById(idPeriodoEvaluacion).get();
		String _descripcion = periodoEvaluacion.getDescripcion();
		Date _fechaDesde = periodoEvaluacion.getFechaDesde();
		String _usuarioIngresoId = periodoEvaluacion.getUsuarioIngresoId();		
		_periodoEvaluacion.setDescripcion(_descripcion);
		_periodoEvaluacion.setFechaDesde(_fechaDesde);
		_periodoEvaluacion.setUsuarioIngresoId(_usuarioIngresoId);
		if(periodoEvaluacion.getFechaHasta() != null) {
			Date _fechaHasta = periodoEvaluacion.getFechaHasta();
			_periodoEvaluacion.setFechaHasta(_fechaHasta);
		}
		periodoEvaluacionRepository.save(_periodoEvaluacion);
	}
	
	public void nuevoPeriodoEvaluacion(PeriodoEvaluacion periodoEvaluacion) {
		PeriodoEvaluacion _periodoEvaluacion = new PeriodoEvaluacion();
		BeanUtils.copyProperties(periodoEvaluacion, _periodoEvaluacion, "");
		periodoEvaluacionRepository.save(_periodoEvaluacion);
		em.refresh(_periodoEvaluacion);
	}
	
	public void deletePeriodoEvaluacion(Integer idPeriodoEvaluacion) {
		PeriodoEvaluacion periodoEvaluacion = periodoEvaluacionRepository.findById(idPeriodoEvaluacion).get();
		java.util.Date fecha = new java.util.Date();
		Date fecha2 = new Date(fecha.getTime());
		if(periodoEvaluacion != null) {
			
			if(periodoEvaluacion.getFechaHasta() == null) {
				periodoEvaluacion.setFechaHasta(fecha2);
			}
			periodoEvaluacion.setEstado("I");
			periodoEvaluacionRepository.save(periodoEvaluacion);
		}
	}

}
