package ec.edu.upse.acad.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ec.edu.upse.acad.model.pojo.OfertaDocente;


public interface OfertaDocenteRepository extends JpaRepository<OfertaDocente, Integer>{
	@Query(value=" SELECT  dep.nombre as facultadDist,ofe.id as idOfertaDist,dov.descripcion as distributivoOfertaVersion, "
			+ "ofe.descripcion as ofertaDist,d.id as idDocente, concat (p.nombres, ' ', p.apellidos) as docente, o1.id as idOfertaDoc ,  dov.estado as estado "+ //+ 
			"FROM DistributivoGeneral as dg " + 
			"INNER JOIN DistributivoGeneralVersion as dgv on dg.id=dgv.idDistributivoGeneral " + 
			"INNER JOIN DistributivoOferta as do on dgv.id=do.idDistributivoGeneralVersion " + 
			"INNER JOIN DistributivoOfertaVersion as dov on do.id=dov.idDistributivoOferta " + 
			"INNER JOIN Oferta as ofe on do.idOferta=ofe.id " + 
			"INNER JOIN TipoOferta as tof on ofe.idTipoOferta=tof.id "+
			"INNER JOIN DepartamentoOferta as deo on ofe.id=deo.idOferta " + 
			"INNER JOIN Departamento as dep on deo.idDepartamento=dep.id " + 
			"INNER JOIN DistributivoDocente as dd on dov.id=dd.idDistributivoOfertaVersion " + 
			"INNER JOIN Docente as d on dd.idDocente=d.id " + 
			"INNER JOIN Persona as p on d.idPersona=p.id "+
			"INNER JOIN OfertaDocente as od on d.id=od.idDocente "+
			"INNER JOIN Oferta as o1 on od.idOferta=o1.id "+
			"INNER JOIN TipoOferta as tof1 on o1.idTipoOferta=tof1.id "+
			"and dep.id=?1 " + 
			"and  dg.idPeriodoAcademico=?2 and od.idPeriodoAcademico=?2 " + 
			"and tof.id=?3 and tof1.id=?3 "+
			"and dg.estado='A' and  dgv.estado='A' and do.estado='A' and dov.estado in ('A', 'D', 'V') " + 
			"and ofe.estado='A' and deo.estado='A' and dep.estado='AC'  and dd.estado='A' and d.estado='A' and p.estado='AC' and od.estado='A' and o1.estado='A' "+
			"group by dep.id, dgv.id, ofe.id, do.id,  dep.nombre, ofe.descripcion, dov.estado, d.id,dov.versionDistributivoOferta,dov.descripcion , o1.id , p.nombres, p.apellidos " +  
 			"order by p.apellidos,p.nombres ") 
 	List<ListDocentesDistributivo> listarDocenteDistribu(Integer idDepartamento, Integer idPeriodoAcademico, Integer idTipoOferta);
	interface ListDocentesDistributivo{ 
		String getFacultadDist(); 
	    Integer getIdOfertaDist();  
	    String getDistributivoOfertaVersion(); 
	    String getOfertaDist(); 
	    Integer getIdDocente();
	    String getDocente();
	    Integer getIdOfertaDoc(); 
	    String getEstado();
	    
	}
}
