package ec.edu.upse.acad.model.pojo;

import javax.persistence.*;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;

import ec.edu.upse.acad.model.pojo.distributivo.DocenteAsignaturaAprend;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;


@Entity
@Table(schema="aca", name="asignatura_aprendizaje")
@Where(clause = "estado='A'")
@NoArgsConstructor
public class AsignaturaAprendizaje  {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_asignatura_aprendizaje")
	@Getter @Setter private Integer id;

	@Getter @Setter private String estado;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;

	@Getter @Setter private float valor;
	
	@Getter @Setter private boolean multidocente;
	

	@Column(name="id_componente_aprendizaje")
	@Getter @Setter private Integer idComponenteAprendizaje;
	
	@Column(name="id_malla_asignatura")
	@Getter @Setter private Integer idMallaAsignatura;
	
	//RELACIONES
	//bi-directional many-to-one association to ComponenteAprendizaje
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_componente_aprendizaje", nullable = false, insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private ComponenteAprendizaje componenteAprendizaje;

	//bi-directional many-to-one association to MallaAsignatura
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_malla_asignatura", nullable = false, insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private MallaAsignatura mallaAsignatura;
	
	//bi-directional many-to-one association to Actividad Personal
    @OneToMany(mappedBy="asignaturaAprendizaje", cascade=CascadeType.ALL)
    @Getter @Setter private List<DocenteAsignaturaAprend> docenteAsignaturaAprend;	

	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }

}