package ec.edu.upse.acad.model.repository.matricula;


import java.util.Date;
import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


import ec.edu.upse.acad.model.pojo.matricula.EstudianteAsignatura;

@Repository
public interface EstudianteAsignaturaRepository  extends JpaRepository<EstudianteAsignatura, Long>{

	@Query(value=" select e.id as id,concat(p.nombres,' ',p.apellidos) as nombres,o.descripcion as descripcion,p.identificacion as identificacion, "+
			"eo.numeroMatricula as numeroMatricula "+
			"from Estudiante e " +
			"inner join Persona p on e.idPersona = p.id " +
			"inner join EstudianteOferta eo on eo.idEstudiante = e.id " + 
			"inner join DepartamentoOferta do on eo.idDepartamentoOferta = do.id "+
			"inner join Oferta o on do.idOferta=o.id")
	List<CustomObjetbuscarEstudianteAsignatura> buscarMatriculaDetalle();
	interface CustomObjetbuscarEstudianteAsignatura{
		Integer getId();
		String getNombres();
		String getDescripcion();
		String getIdentificacion();
		String getNumeroMatricula();
	}
	
//	@Query(value=" select distinct daa.id as idDocAsigAprend,ma.id as idMallaAsig," +
//			" concat(cast(n.orden as text),'/',pl.descripcionCorta,'  ' ,p.apellidos,' ',p.nombres) as nombreDocente," + 
//			" pl.descripcionCorta as descripcionCorta,daa.numEstudiantes as numEstudiantes,(coalesce((select count(ma2.id)" + 
//			" from EstudianteMatricula em "
//			+ "inner join EstudianteAsignatura ea2 on em.id = ea2.idEstudianteMatricula " + 
//			" inner join DocenteAsignaturaAprend daa2 on daa2.id = ea2.idDocenteAsignaturaAprend " + 
//			" inner join AsignaturaAprendizaje aa2 on aa2.id = daa2.idAsignaturaAprendizaje " + 
//			" inner join MallaAsignatura ma2 on ma2.id = aa2.idMallaAsignatura where ma2.id = ma.id group by ma2.id),0)) as numMatriculados" +
//			" from DistributivoDocente ddo " + 
//			" inner join Docente d on ddo.idDocente = d.id " + 
//			" inner join Persona p  on p.id = d.idPersona " + 
//			" inner join DocenteAsignaturaAprend daa on daa.idDistributivoDocente = ddo.id " + 
//			" inner join AsignaturaAprendizaje aa on daa.idAsignaturaAprendizaje = aa.id " + 
//			" inner join MallaAsignatura ma on ma.id = aa.idMallaAsignatura " + 
//			" inner join Nivel n on ma.idNivel = n.id  " +
//			" inner join Paralelo pl on daa.idParalelo = pl.id" +
//			" where  ddo.idDistributivoOfertaVersion = " + 
//			" (SELECT max(dov1.id) FROM DepartamentoOferta dof1,Oferta o1, DistributivoOferta do1, DistributivoOfertaVersion dov1  " + 
//			" where dof1.idOferta=o1.id and o1.id=do1.idOferta and do1.id = dov1.idDistributivoOferta and "+
//			" dof1.id=(select eo.idDepartamentoOferta from EstudianteOferta eo where eo.id = (?1))) " + 
//			" and daa.id = (SELECT max(daa1.id) FROM DocenteAsignaturaAprend daa1 " + 
//			" inner join DistributivoDocente ddo1 on ddo1.id = daa1.idDistributivoDocente "+
//			" inner join AsignaturaAprendizaje aa1 on daa1.idAsignaturaAprendizaje = aa1.id " + 
//			" inner join MallaAsignatura ma1 on ma1.id = aa1.idMallaAsignatura " + 
//			" where ma1.id=ma.id and ddo1.id=ddo.id )"+
//			" order by ma.id,pl.descripcionCorta ")
//	List<CustomObjetCargarDocente> listarDocentesAsignaturas(Integer idEstudianteOferta);
	
	//sin el docentes sin asignar
	
//	@Query(value=" select distinct daa.id as idDocAsigAprend,ma.id as idMallaAsig," +
//			" concat(cast(n.orden as text),'/',pl.descripcionCorta,'  ' ,p.apellidos,' ',p.nombres) as nombreDocente," + 
//			" pl.descripcionCorta as descripcionCorta,daa.numEstudiantes as numEstudiantes,(coalesce((select count(ma2.id)" + 
//			" from EstudianteMatricula em "
//			+ "inner join EstudianteAsignatura ea2 on em.id = ea2.idEstudianteMatricula " + 
//			" inner join DocenteAsignaturaAprend daa2 on daa2.id = ea2.idDocenteAsignaturaAprend " + 
//			" inner join AsignaturaAprendizaje aa2 on aa2.id = daa2.idAsignaturaAprendizaje " + 
//			" inner join MallaAsignatura ma2 on ma2.id = aa2.idMallaAsignatura where ma2.id = ma.id group by ma2.id),0)) as numMatriculados," +
//			" ca.codigo as codigo, pl.id as idParalelo" + 
//			" from DistributivoDocente ddo " + 
//			" inner join Docente d on ddo.idDocente = d.id " + 
//			" inner join DistributivoOfertaVersion dov on ddo.idDistributivoOfertaVersion = dov.id " + 
//			" inner join DistributivoOferta do on do.id = dov.idDistributivoOferta " + 
//			" inner join DistributivoGeneralVersion dgv on dgv.id = do.idDistributivoGeneralVersion " + 
//			" inner join DistributivoGeneral dg on dg.id = dgv.idDistributivoGeneral"+
//			" inner join Persona p  on p.id = d.idPersona " + 
//			" inner join DocenteAsignaturaAprend daa on daa.idDistributivoDocente = ddo.id " + 
//			" inner join AsignaturaAprendizaje aa on daa.idAsignaturaAprendizaje = aa.id " + 
//			" inner join MallaAsignatura ma on ma.id = aa.idMallaAsignatura " + 
//			" inner join Nivel n on ma.idNivel = n.id  " +
//			" inner join Paralelo pl on daa.idParalelo = pl.id" +
//			" inner join ComponenteAprendizaje ca on  ca.id = aa.idComponenteAprendizaje" +
//			" where  ddo.idDistributivoOfertaVersion = " + 
//			" (SELECT max(dov1.id) FROM DepartamentoOferta dof1,Oferta o1, DistributivoOferta do1, DistributivoOfertaVersion dov1  " + 
//			" where dof1.idOferta=o1.id and o1.id=do1.idOferta and do1.id = dov1.idDistributivoOferta and "+
//			" dof1.id=(select eo.idDepartamentoOferta from EstudianteOferta eo where eo.id = (?1))) " + 
//			" and ca.codigo in ('PRE', 'APR') " + 
//			" order by ma.id,pl.descripcionCorta ")
//	List<CustomObjetCargarDocente> listarDocentesAsignaturas(Integer idEstudianteOferta);
//	
//	interface CustomObjetCargarDocente{
//		Integer getIdDocAsigAprend();
//		Integer getIdMallaAsig();
//		String getNombreDocente();
//		String getDescripcionCorta();
//		Integer getNumEstudiantes();
//		Integer getNumMatriculados();
//		String getCodigo();
//		Integer getIdParalelo();
//
//	}
	/**
	 * funcion que retorna la lista de docentes de dictan cada asigntura de la malla de una carrera
	 * @param pi_id_estudiante_oferta
	 * @return
	 */

	@Query(value = "select d.id_docente_asignatura_aprend as idDocAsigAprend, d.id_malla_asignatura as idMallaAsig, d.id_distributivo_docente as idDistributivoDocente, " + 
			" d.docente as nombreDocente, d.num_estudiantes as numEstudiantes, d.num_matriculados as numMatriculados, "+
			" d.codigo as codigo, d.id_paralelo as idParalelo from aca.fn_listar_docentes_asignaturas "+
			" (:pi_id_estudiante_oferta) as d ", nativeQuery = true)
	public List<CustomObjetListarDocente> fnListarDocentesAsignaturas(@Param("pi_id_estudiante_oferta") Integer pi_id_estudiante_oferta);
	
	public interface CustomObjetListarDocente{
		Integer getIdDocAsigAprend();
		Integer getIdMallaAsig();
		Integer getIdDistributivoDocente();
		String getNombreDocente();
		Integer getNumEstudiantes();
		Integer getNumMatriculados();
		String getCodigo();
		Integer getIdParalelo();
	}
	
	
	@Query(value=" select em.id as idEstudianteMatricula,ea.id as idEstudianteAsignatura,dea.id as idDetalleEstudianteAsignatura, "+
			" dea.observacion as observacion,dea.numeroOficio as numeroOficio, ea.estado as estado" + 
			" from EstudianteMatricula em " + 
			" inner join EstudianteAsignatura ea on em.id = ea.idEstudianteMatricula " + 
			" inner join DetalleEstudianteAsignatura dea on ea.id = dea.idEstudianteAsignatura " +
			" where em.id = (select  max(em1.id) from EstudianteMatricula em1 " + 
			" where em1.idEstudianteOferta = (?1))  and ea.estado in ('X','R','A')")
	List<CustomObjetRecuperarAsig> recuperarAsignaturasAnuladasRetiradas(Integer idEstudianteOferta);
	
	interface CustomObjetRecuperarAsig{
		Integer getIdEstudianteMatricula();
		Integer getIdEstudianteAsignatura();
		Integer getIdDetalleEstudianteAsignatura();
		String getObservacion();
		String getNumeroOficio();
		String getEstado();
	}
	

}
