package ec.edu.upse.acad.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.seguridad.Rol;


@Repository

public interface RolesRepository 
extends JpaRepository<Rol, Integer>{

@Query(value="SELECT e "
		+ "FROM Rol AS e "
	//	+ "INNER JOIN Modulo m "
	//	+ "INNER JOIN Rol u "
		+ "WHERE e.nombre = ?1 ")
List<Rol> buscarPorNombre(String nombre);

@Query(value="SELECT e "
		+ "FROM Rol AS e "
		+ "WHERE e.nivel > ?1 ")
List<Rol> buscarPorNivel(Integer nivel);

@Query(value="SELECT max(e.nivel)"
		+ "FROM Rol AS e ")
List<Rol> buscarNivelAlto();

@Query(value="SELECT mr.id, r.nombre "
		+ "FROM ModuloRolUsuario AS mru "
		+ "JOIN ModuloRol AS mr ON mru.moduloRol.id=mr.id  "
		+ "JOIN Usuario AS u ON mru.usuario.id=u.id "
		+ "JOIN Rol AS r ON mr.rol.id=r.id "
		+ "WHERE u.usuario = ?1 "
		+ "AND mr.modulo.id = ?2 "
		+ "AND mru.estado = 'AC' ")
List<CustomRolObject> buscarRolesUsuarioModulos(String usuario,Integer modulo_id);

}


interface CustomRolObject { 
    Integer getId(); 
    String getNombre(); 
  
} 

