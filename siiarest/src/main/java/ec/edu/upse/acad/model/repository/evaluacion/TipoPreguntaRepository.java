package ec.edu.upse.acad.model.repository.evaluacion;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.evaluacion.TipoPregunta;

@Repository
public interface TipoPreguntaRepository extends JpaRepository<TipoPregunta, Integer> {
	@Query(value="SELECT tp "
			+ "FROM TipoPregunta tp "
			+ "WHERE tp.estado = 'A' ")
	List<TipoPregunta> listarTipoPregunta();


}
