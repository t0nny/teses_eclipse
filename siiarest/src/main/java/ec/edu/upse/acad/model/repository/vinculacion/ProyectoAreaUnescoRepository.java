package ec.edu.upse.acad.model.repository.vinculacion;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.vinculacion.AreaUnesco;
import ec.edu.upse.acad.model.pojo.vinculacion.ProyectoAreaUnesco;

@Repository
public interface ProyectoAreaUnescoRepository extends JpaRepository<ProyectoAreaUnesco, Integer> {
	
	@Transactional
	@Query(value = "SELECT au "
			+ "FROM AreaUnesco au " + " WHERE au.estado='A' ")
	List<AreaUnesco> buscarListaAreaUnesco();

	
	@Query(value = "SELECT au "
			+ "FROM AreaUnesco au " + " WHERE au.idPadre=(?1)  and au.estado='A' ")
	Optional<AreaUnesco> ListarPorIdAreaUnescoPadre(Integer idAreaUnesco);

}
