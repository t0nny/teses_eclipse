package ec.edu.upse.acad.model.pojo.seguridad;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.annotations.Where;

import ec.edu.upse.acad.model.pojo.Archivo;
import ec.edu.upse.acad.model.pojo.DepartamentoOferta;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="man", name="departamentos")
@NoArgsConstructor
@Where(clause = "estado='AC'")
public class Departamento {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Getter @Setter private Integer id;

	@Getter @Setter private String codigo;

	@Getter @Setter private String nombre;

	@Getter @Setter private String descripcion;

	@Getter @Setter private String mail;

	@Getter @Setter private String tipo;

	@Getter @Setter private Integer padre_id;

	@Getter @Setter private String imagen;

	@Getter @Setter private String estado;
	
	@Column(name="usuario_ing")
	@Getter @Setter private String usuarioIng;

	@Version
	@Getter @Setter private Integer version;


	@OneToMany(mappedBy="departamento", cascade=CascadeType.ALL)
	@Getter @Setter private List<PersonaDepartamento> personaDepartamento;

	@OneToMany(mappedBy="departamento")
	@Getter @Setter private List<Archivo> archivos;	
	

	@OneToMany(mappedBy="departamento")
	@Getter @Setter private List<DepartamentoOferta> departamentoOfertas;


	@OneToMany(mappedBy="departamento")
	@Getter @Setter private List<DepartamentosCargo> departamentosCargos;

	@OneToMany(mappedBy="departamento")
	@Getter @Setter private List<RolDepartamento> rolesDepartamentos;

	@PrePersist
	void preInsert() {
		if (this.estado == null)
			this.estado = "A";
	}

}
