package ec.edu.upse.acad.model.pojo.evaluacion;

import java.sql.Date;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ec.edu.upse.acad.model.pojo.PeriodoAcademico;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="eva", name="periodo_evaluacion")
@NoArgsConstructor
public class PeriodoEvaluacion {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_periodo_evaluacion")
	@Getter @Setter private Integer id;

	@Getter @Setter private String descripcion;

	@Column(name="fecha_desde")
	@Getter @Setter private Date fechaDesde;

	@Column(name="fecha_hasta")
	@Getter @Setter private Date fechaHasta;
	
	@Getter @Setter private String estado;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;
	
	@Version
	@Getter @Setter private Integer version;

	//RELACIONES

	//bi-directional many-to-one association to Periodo
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_periodo_academico",nullable = false)
	@JsonIgnore
	@Getter @Setter private PeriodoAcademico periodoAcademico;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_evaluacion_docente_general", nullable = false)
	@JsonIgnore
	@Getter @Setter private EvaluacionDocenteGeneral evaluacionDocenteGeneral;
	
	@OneToMany(mappedBy="periodoEvaluacion", cascade=CascadeType.ALL)
	@JsonIgnore
	@Getter @Setter private List<PerTipoEvalInstrumento> perTipoEvalInstrumento;

	
	
	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }
		

}
