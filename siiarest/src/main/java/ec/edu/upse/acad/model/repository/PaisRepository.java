package ec.edu.upse.acad.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.Pais;


@Repository
public interface PaisRepository extends JpaRepository<Pais, Integer>{
	@Query(value=" SELECT p.id as idPais, p.descripcion as pais "+
			"FROM Pais as p " ) 
	List<CustomObject> listarPaises();	
	interface CustomObject  { 
		Integer getIdPais(); 
		String getPais();
		
		
	}
	
}
