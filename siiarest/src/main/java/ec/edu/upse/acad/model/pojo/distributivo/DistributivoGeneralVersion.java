package ec.edu.upse.acad.model.pojo.distributivo;


import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="aca", name="distributivo_general_version")
@Where(clause = "estado='A'")
@NoArgsConstructor
public class DistributivoGeneralVersion {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_distributivo_general_version")
	@Getter @Setter private Integer id;
	
	@Column(name="id_distributivo_general")
	@Getter @Setter private Integer idDistributivoGeneral;
	
    @Getter @Setter private String estado;
    @Column(name="version_distributivo_general")
	@Getter @Setter private Integer versionDistributivoGeneral;
	
//	@Column(name="fecha_ingreso")
//	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private Integer usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;
	
	//RELACIONES
	
		//bi-directional many-to-one association to DistributivoGeneral
			@ManyToOne(fetch=FetchType.LAZY)
			@JoinColumn(name="id_distributivo_general", insertable=false, updatable = false)
			@JsonIgnore
			@Getter @Setter private DistributivoGeneral distributivoGeneral;
			
		//bi-directional many-to-one association to Distributivo Oferta Version
		    @OneToMany(mappedBy="distributivoGeneralVersion", cascade=CascadeType.ALL)
		    @Getter @Setter private List<DistributivoOferta> distributivoOferta;
		    

		
		 @PrePersist
			void preInsert() {
			   if (this.estado == null)
			       this.estado = "A";
		 }
	

}
