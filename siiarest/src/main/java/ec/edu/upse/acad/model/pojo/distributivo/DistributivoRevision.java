package ec.edu.upse.acad.model.pojo.distributivo;

import java.sql.Timestamp;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ec.edu.upse.acad.model.pojo.seguridad.Usuario;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Table(schema="aca", name="distributivo_revision")
@NoArgsConstructor
public class DistributivoRevision {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_distributivo_revision")
	@Getter @Setter private Integer id;
	
	@Column(name="id_distributivo_oferta_version")
	@Getter @Setter private Integer idDistributivoOfertaVersion;

	@Getter @Setter private String observacion;
	
	@Getter @Setter private String accion;
	
	@Getter @Setter private String estado;

	@Column(name="fecha_ingreso")
	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="id_usuario")
	@Getter @Setter private String idUsuario;

	@Version
	@Getter @Setter private Integer version;
	
	
	
	
	//RELACIONES
	//bi-directional many-to-one association to Departamento
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_usuario", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Usuario usuario;

	//bi-directional many-to-one association to DistributivoOferta
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_distributivo_oferta_version", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private DistributivoOfertaVersion distributivoOfertaVersion;
	
	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }

}