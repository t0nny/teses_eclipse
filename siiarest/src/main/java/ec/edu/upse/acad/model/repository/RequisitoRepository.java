package ec.edu.upse.acad.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ec.edu.upse.acad.model.pojo.Requisito;

public interface RequisitoRepository 
                 extends JpaRepository<Requisito, Integer> {
	@Query(value="select distinct r.id as id,r.descripcion as descripcion,"
			+ " r.abreviatura as abreviatura" 
			+ " from Requisito r " 
			+"  where r.estado ='A' ")
	List<CustomObject> listarRequisitos();
	interface CustomObject {
		Integer getId();
		String getDescripcion();
		String getAbreviatura();
	}

}
