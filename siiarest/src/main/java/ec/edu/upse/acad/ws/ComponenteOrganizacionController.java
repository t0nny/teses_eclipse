
package ec.edu.upse.acad.ws;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.upse.acad.model.pojo.ComponenteOrganizacion;
import ec.edu.upse.acad.model.repository.ComponenteOrganizacionRepository;
import ec.edu.upse.acad.model.service.SecurityService;

@RestController
@RequestMapping("/api/componenteorganizacion")
@CrossOrigin

public class ComponenteOrganizacionController {
	@Autowired
	private ComponenteOrganizacionRepository componenteorganizacionRepository;
	@Autowired
	private SecurityService securityService;
	// @Autowired private UpdateForIdService eliminaComponenteOrganizacion;

	// ****************** SERVICIOS PARA COMPONENTE
	// ORGANIZACION************************///

	// Buscar todos los ComponenteOrganizacion, para listar en angular
	@RequestMapping(value = "/buscarComponenteOrganizacion", method = RequestMethod.GET)
	public ResponseEntity<?> buscarComponenteOrganizacion() {
		return ResponseEntity.ok(componenteorganizacionRepository.buscarComponenteOrganizacion());
	}

	// servicio que busca por Id un ComponenteOrganizacion
	@RequestMapping(value = "/buscarComponenteOrganizacionId/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarComponenteOrganizacionId(@PathVariable("id") Integer id) {
		return ResponseEntity.ok(componenteorganizacionRepository.buscarPorComponenteOrganizacionid(id));
	}

	// Servicio que busca por nombre
	@RequestMapping(value = "/buscarPorNombre/{nombre}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarPorNombre(@PathVariable("nombre") String nombre) {
		return ResponseEntity.ok(componenteorganizacionRepository.buscarPorComponenteOrganizacion("%" + nombre + "%"));
	}

	// Servicio de grabar ComponenteOrganizacion
	@RequestMapping(value = "/grabarComponenteOrganizacion", method = RequestMethod.POST)
	public ResponseEntity<?> grabarComponenteOrganizacion(@RequestBody ComponenteOrganizacion componenteorganizacion) {
		ComponenteOrganizacion _componenteorganizacion = new ComponenteOrganizacion();
		if (componenteorganizacion.getId() != null) {
			_componenteorganizacion = componenteorganizacionRepository.findById(componenteorganizacion.getId()).get();
		}
		BeanUtils.copyProperties(componenteorganizacion, _componenteorganizacion);
		componenteorganizacionRepository.save(_componenteorganizacion);
		return ResponseEntity.ok(_componenteorganizacion);
	}

	//
	@RequestMapping(value = "/borrar/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> borrar(@PathVariable("id") Integer id) {
		return ResponseEntity.ok().build();
	}

	// listar todos los ComponenteOrganizacion por nivel
	@RequestMapping(value = "/listarcompasocanivel", method = RequestMethod.GET)
	public ResponseEntity<?> listarcompasocanivel() {
		return ResponseEntity.ok(componenteorganizacionRepository.listarcompasocanivel());
	}
}
