package ec.edu.upse.acad.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.AreaConocimiento;


@Repository
public interface AreaConocimientoRepository extends JpaRepository<AreaConocimiento, Integer>{

	@Query(value=" SELECT u.id as idAreaConocimiento,u.codigo as codigo, u.descripcion as areaConocimiento "+
			"FROM AreaConocimiento as u " ) 
	List<CustomObject> listarAreas();	
	interface CustomObject  { 
		Integer getIdAreaConocimiento(); 
		String getCodigo();
		String getAreaConocimiento();

	}
	
	@Query(value=" SELECT a.id as idAsignatura,a.codigo as codigo, a.descripcion as asignatura "+
			"FROM Asignatura as a order by a.descripcion asc" ) 
	List<CoListaAsignaturas> listarAsignaturas();	
	interface CoListaAsignaturas  { 
		Integer getIdAsignatura(); 
		String getCodigo();
		String getAsignatura();

	}
}
