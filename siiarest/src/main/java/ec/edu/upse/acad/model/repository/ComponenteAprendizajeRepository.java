package ec.edu.upse.acad.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.ComponenteAprendizaje;

@Repository
public interface ComponenteAprendizajeRepository extends JpaRepository<ComponenteAprendizaje, Integer>{

	@Query(value="SELECT ca "
			+ "FROM ComponenteAprendizaje AS ca "
			+ "WHERE ca.descripcion LIKE ?1 "
			+ "OR ca.codigo LIKE ?1 ")
	List<ComponenteAprendizaje> buscarPorComponenteAprendizaje(String descripcion);
	
	@Query(value="SELECT ca.id,ca.descripcion,ca.codigo,ca.estado,ca.version "
			+ "FROM ComponenteAprendizaje AS ca "
			+ "WHERE ca.estado = 'A' ")
	List<CustomObjectComponenteAprendizaje> buscarComponenteAprendizaje();
	
	interface CustomObjectComponenteAprendizaje {
		Integer getId();
		String getDescripcion();
		String getAbreviatura();
		String getEstado();
		Integer getVersion();
	}
	
	@Query(value = " SELECT ca.id as idCompAprendizajePadre,ca.abreviatura as abreviaturaPadre,ca.codigo as codigoPadre,ca.descripcion as componentePadre,"
			+ " case when ca1.id is null then ca.id else ca1.id end as idCompAprendizajeHijo,"
			+ " case when ca1.abreviatura IS null then ca.abreviatura else ca1.abreviatura  end as abreviaturaHijo,"
			+ " case when ca1.codigo is null then ca.codigo else ca1.codigo  end as codigoHijo,"
			+ " case when ca1.descripcion is null then ca.descripcion else ca1.descripcion  end  as componenteHijo "
			+ " FROM ComponenteAprendizaje ca "
			+ " LEFT JOIN ComponenteAprendizaje ca1 on ca.id=ca1.idComponenteAprendizajePadre "
			+ " WHERE ca.estado='A' and ca.idComponenteAprendizajePadre in (select ca1.id from ComponenteAprendizaje ca1 where ca1.codigo='FORMATIVA' and ca1.estado='A')"
			+ " and (ca1.estado is null or ca1.estado ='A') order by ca.orden asc ,ca1.orden asc")
	List<COCompAprendizaje> listarComponenteAprendizajeForm();
	
	interface COCompAprendizaje{
		Integer getIdCompAprendizajePadre();
		String getAbreviaturaPadre();
		String getCodigoPadre();
		String getComponentePadre();
		Integer getIdCompAprendizajeHijo();
		String getAbreviaturaHijo();
		String getCodigoHijo();
		String getComponenteHijo();
	}
	
	@Query(value="select distinct mca.idCompAprendizaje,mca.idReglamento,ca.descripcion,m.descripcion,m.id,ca.version from Malla m " + 
			"inner join Reglamento mg on m.idReglamento = mg.id " + 
			"inner join ReglamentoCompAprendizaje mca on mca.idReglamento = mg.id " + 
			"inner join ComponenteAprendizaje ca on mca.idCompAprendizaje = ca.id " + 
			//"inner join Malla m on m.idMalla = m.id " + 
			"where m.id = (?1) ")
	List<CustomObjectComponenteAprendizajeVigente> listarcomponenteaprendizajevigente(Integer id);
	
	interface CustomObjectComponenteAprendizajeVigente {
		Integer getIdCompAprendizaje();
		Integer getIdMallaGeneral();
		String getDescripcionCa();
		String getDescripcionM();
		Integer getIdMallaVersion();
		Integer getVersion();
	}
	

	//AsignarDocente


//	@Query(value="select '',ma.id,mca.idCompAprendizaje,0,mca.asignarDocente,ma.estado,ma.version " + 
//			" from MallaAsignatura ma " + 
//			" left join MallaVersion mv on ma.idMallaVersion = mv.id " + 
//			" left join AsignaturaVersion av on ma.idAsignaturaVersion = av.id " + 
//			" left join AsignaturaAprendizaje aa on ma.id = aa.idMallaAsignatura " + 
//			" left join ComponenteAprendizaje ca on aa.idComponenteAprendizaje = ca.id " + 
//			" left join MallaCompAprendizaje mca on mca.idCompAprendizaje = ca.id " + 
//			" where ma.id not in (select distinct ma.id " + 
//			" from MallaAsignatura ma " + 
//			" where ma.estado = 'A' and (select count(distinct aa1.idComponenteAprendizaje) " + 
//			" from  MallaAsignatura ma1 " + 
//			" left join AsignaturaAprendizaje aa1 on  aa1.idMallaAsignatura = ma1.id " + 
//			" where aa1.idMallaAsignatura = aa.idMallaAsignatura)=3)and ma.estado = 'A' and mv.id = (?1) ")
//	List<CustomObjectComponenteAprendizajeVigente1> listarasignaturaconcompAprendizaje(Integer id);
	
	interface CustomObjectComponenteAprendizajeVigente1 {
		Integer getId();
		Integer getIdMallaAsignatura();
		Integer getIdComponenteAprendizaje();
		Integer getValor();
		Boolean getAsignarDocente();
		String getEstado();
		Integer getVersion();
	}
	
	
}


