package ec.edu.upse.acad.model.pojo.reglamento;


import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;



import ec.edu.upse.acad.model.pojo.distributivo.DocenteActividad;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="aca", name="actividad_docente_detalle")
@NoArgsConstructor

public class ActividadDocenteDetalle {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_actividad_detalle")
	@Getter @Setter private Integer id;
	
	@Getter @Setter  private String descripcion;
	
	@Getter @Setter  private String orden;
	
	@Getter @Setter  private String estado;
		
		
	@Column(name="usuario_ingreso_id")
	@Getter @Setter  private String usuarioIngresoId;
		
	@Version
	@Getter @Setter  private Integer version;
		
		
		//RELACIONES
		
	//bi-directional many-to-one association to ReglamentoActividadDetalle	
			@OneToMany(mappedBy="actividadDocenteDetalle", cascade=CascadeType.ALL)
			@Getter @Setter private List<ReglamentoActividadDetalle> reglamentoActividadDetalles;	
		
	//bi-directional many-to-one association to ReglamentoActividadDetalle	
			@OneToMany(mappedBy="actividadDocenteDetalle", cascade=CascadeType.ALL)
			@Getter @Setter private List<DocenteActividad> docenteActividads;

			@PrePersist
			void preInsert() {
			   if (this.estado == null)
			       this.estado = "A";
		 }
	
}
