package ec.edu.upse.acad.model.repository.vinculacion;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.vinculacion.LineasInvestigacion;
import ec.edu.upse.acad.model.pojo.vinculacion.ProyectoLineasInv;

@Repository
public interface ProyectoLineasInvRepository extends JpaRepository<ProyectoLineasInv, Integer> {

	@Transactional
	@Query(value = "SELECT li " + "FROM LineasInvestigacion li " + " WHERE li.estado='A' ")
	List<LineasInvestigacion> buscarLineasInvestigacion();

	// LLENAR LINEAS y SUB INVESTIGACION 
	@Query(value = "SELECT li.id as idLineaInv, li.descripcion as descripcionLinea, "
			+ "sli.id as idSubLinea,sli.descripcion as descripcionSubLi from LineasInvestigacion li "
			+ "INNER JOIN SubLineaInv sli on li.id=sli.idLineasInv " 
			+ "WHERE li.idDepOferta=(?1) and li.estado='A'")
	List<lineasYSub> lineasYSubLineasInv(Integer idDepOferta);

	interface lineasYSub {
		Integer getIdLineaInv();
		String getDescripcionLinea();
		Integer getIdSubLinea();
		String getDescripcionSubLi();
	}

}
