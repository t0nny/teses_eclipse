package ec.edu.upse.acad.ws.eva;

import java.util.ArrayList;
import java.util.List;

import org.jfree.util.Log;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.upse.acad.model.repository.evaluacion.PreguntaRepository;
import ec.edu.upse.acad.model.pojo.evaluacion.OpcionPregunta;
import ec.edu.upse.acad.model.pojo.evaluacion.Pregunta;
import ec.edu.upse.acad.model.repository.evaluacion.CategoriaPreguntaRepository;
import ec.edu.upse.acad.model.repository.evaluacion.OpcionPreguntaRepository;
import ec.edu.upse.acad.model.service.SecurityService;
import ec.edu.upse.acad.util.RestUtil;

@RestController
@RequestMapping("/api/pregunta")
@CrossOrigin
public class PreguntaController {
	@Autowired private PreguntaRepository preguntaRepository;
	@Autowired private CategoriaPreguntaRepository categoriaPreguntaRepository;
	@Autowired private SecurityService securityService;
	@Autowired private OpcionPreguntaRepository opcionPreguntaRepository;
	
	@RequestMapping(value="/listarPregunta", method=RequestMethod.GET)
	public ResponseEntity<?> listarPregunta(@RequestHeader(value="Authorization") String authorization) {		
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		/*List<CustomObjectPregunta> lista = new ArrayList<>();
		for(CustomObjectPregunta cop: preguntaRepository.listarPregunta()) {
			cop.getCodigo().toUpperCase();
			cop.getDescripcion().toUpperCase();
			cop.getDescripcionCateg().toUpperCase();
			cop.getDescripcionTipo().toUpperCase();
		}*/
		
		
		return ResponseEntity.ok(preguntaRepository.listarPregunta());
		
	}
	

	//Buscar todos las Pregunta, para listar en angular de acuerdo a la categoria
	@RequestMapping(value="/listarPreguntaCategoria/{id}", method=RequestMethod.GET)
	public ResponseEntity<?> listarPreguntaCategoria(@RequestHeader(value="Authorization") String Authorization, @PathVariable Integer id) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(preguntaRepository.listarPreguntaCategoria(id));
	}
	
	// servicio que devuelve el jason completo Pregunta
	@RequestMapping(value = "/buscarPregunta/{idPregunta}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarPregunta(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable("idPregunta") Integer idPregunta) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		Pregunta _pregunta;
		if (preguntaRepository.findById(idPregunta).isPresent()) {
			_pregunta = preguntaRepository.findById(idPregunta).get();
			Pregunta pregunta = new Pregunta();
			BeanUtils.copyProperties(_pregunta, pregunta, "tipoPregunta", "categoriaPregunta");
			return ResponseEntity.ok(pregunta);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	@RequestMapping(value="/buscarDescripcionPregunta/{descripcion}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarDescripcionPregunta(
			@PathVariable("descripcion") String descripcion) 
	{
		return ResponseEntity.ok(preguntaRepository.findByDescripcionPregunta(descripcion));
	}
	
	/**
	 * Graba o edita una pregunta
	 * 
	 * @param asignatura recibe un objeto de tipo Asignatura
	 * @return
	 */
	// servicio que graba un CategoriaPregunta
	public static final String ALREADY_REPORTED = "ERROR! base de datos";
	@RequestMapping(value="/grabarPregunta", method=RequestMethod.POST)
	public ResponseEntity<?> grabarPregunta(@RequestHeader(value="Authorization") 
														   String Authorization, 
														   @RequestBody OpcionPregunta opcionPregunta) {
		try {
			if (!securityService.isTokenValido(Authorization)) {
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
			}
			Log.debug(opcionPregunta.getPregunta().getDescripcion());
			 
			OpcionPregunta _opcionPregunta = new OpcionPregunta();
			if (opcionPregunta.getId() != null) {
				_opcionPregunta = opcionPreguntaRepository.findById(opcionPregunta.getId()).get();
				_opcionPregunta.getIdOpcionRespuesta().setId(opcionPregunta.getIdOpcionRespuesta().getId());
				_opcionPregunta.getPregunta().setCategoriaPregunta(opcionPregunta.getPregunta().getCategoriaPregunta());
				_opcionPregunta.getPregunta().setTipoPregunta(opcionPregunta.getPregunta().getTipoPregunta());
				_opcionPregunta.getPregunta().setCodigo(opcionPregunta.getPregunta().getCodigo());
				_opcionPregunta.getPregunta().setDescripcion(opcionPregunta.getPregunta().getDescripcion());
				opcionPreguntaRepository.save(_opcionPregunta);
				return ResponseEntity.ok(_opcionPregunta);
			}else {				
				Pregunta pregunta = new Pregunta();
				pregunta.setCategoriaPregunta(opcionPregunta.getPregunta().getCategoriaPregunta());
				pregunta.setTipoPregunta(opcionPregunta.getPregunta().getTipoPregunta());
				pregunta.setCodigo(opcionPregunta.getPregunta().getCodigo());
				pregunta.setDescripcion(opcionPregunta.getPregunta().getDescripcion());
				//pregunta.setSiNo(opcionPregunta.getPregunta().getSiNo());
				pregunta.setUsuarioIngresoId(opcionPregunta.getUsuarioIngresoId());
				List<OpcionPregunta> listaOpcionPregunta = new ArrayList<>();
					
				_opcionPregunta.setPregunta(pregunta);
				_opcionPregunta.setUsuarioIngresoId(opcionPregunta.getUsuarioIngresoId());
				listaOpcionPregunta.add(_opcionPregunta);
				pregunta.setOpcionPregunta((listaOpcionPregunta));
				preguntaRepository.save(pregunta);
				return ResponseEntity.ok(pregunta);
			}
			//BeanUtils.copyProperties(reglaCalificacion, _reglaCalificacion);
			
		} catch (Exception e) {
			e.printStackTrace();
			return RestUtil.preconditionFailedError(ALREADY_REPORTED);			
		}		
	} 
	//****************** SERVICIOS PARA PREGUNTA ************************//
	//Buscar todos las escalas, para listar en angular
//	@RequestMapping(value="/listarCategoriaPregunta", method=RequestMethod.GET)
//	public ResponseEntity<?> listarTipoPregunta(@RequestHeader(value="Authorization") String Authorization) {
//		if (!securityService.isTokenValido(Authorization)) {
//			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
//		}
//		return ResponseEntity.ok(categoriaPreguntaRepository.listarCategoriaPregunta());
//	}

	
	/*@RequestMapping(value="/listarTipoPregunta", method=RequestMethod.GET)
	public ResponseEntity<?> listarTipoPregunta(@RequestHeader(value="Authorization") String Authorization) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(categoriaPreguntaRepository.listarTipoPregunta());
	}*/


}
