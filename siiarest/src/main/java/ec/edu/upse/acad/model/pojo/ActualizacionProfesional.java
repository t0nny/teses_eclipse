package ec.edu.upse.acad.model.pojo;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="actualizacion_profesional")
public class ActualizacionProfesional {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_actualizacion_profesional")
	@Getter @Setter private Integer id;
	
	@Getter @Setter private String institucion;
	@Column(name="id_docente")
	@Getter @Setter private Integer idDocente;
	
	@Column(name="id_area_conocimiento")
	@Getter @Setter private Integer idAreaConocimiento;
	
	@Column(name="nombre_curso")
	@Getter @Setter private String nombreCurso;

	@Column(name="tipo_evento")
	@Getter @Setter private String tipoEvento;
	
	@Column(name="tipo_certificado")
	@Getter @Setter private String tipoCertificado;
	
	@Column(name="numero_dias")
	@Getter @Setter private Integer numeroDias;
	
	@Column(name="numero_horas")
	@Getter @Setter private Integer numeroHoras;
	
	@Column(name="tipo_capacitacion")
	@Getter @Setter private String tipoCapacitacion;
	
	@Column(name="fecha_desde")
	@Getter @Setter private Date fechaDesde;
	
	@Column(name="fecha_hasta")
	@Getter @Setter private Date fechaHasta;
		
	@Column(name="fecha_ingreso")
	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Getter @Setter private String estado;

	@Version
	@Getter @Setter private Integer version;
	
	//RELACIONES
	//bi-directional many-to-one association to Docente
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_docente", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Docente docente;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JsonIgnore
	@JoinColumn(name="id_area_conocimiento", insertable=false, updatable = false)
	@Getter @Setter private AreaConocimiento areaConocimiento;

	@PrePersist
	void preInsert() {
	   if (this.estado == null)
	       this.estado = "A";
 }
		
}
