package ec.edu.upse.acad.model.pojo.distributivo;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ec.edu.upse.acad.model.pojo.AsignaturaAprendizaje;
import ec.edu.upse.acad.model.pojo.EspacioFisico;
import ec.edu.upse.acad.model.pojo.Paralelo;
import ec.edu.upse.acad.model.pojo.calificaciones.ActaCalificacion;
import ec.edu.upse.acad.model.pojo.matricula.DetalleEstudianteAsignatura;
import ec.edu.upse.acad.model.pojo.matricula.EstudianteAsignatura;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="aca", name="docente_asignatura_aprend")
@Where(clause = "estado='A'")
@NoArgsConstructor

public class DocenteAsignaturaAprend {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_docente_asignatura_aprend")
	@Getter @Setter private Integer id;
	
	@Column(name="id_distributivo_docente")
	@Getter @Setter private Integer idDistributivoDocente;
	
	@Column(name="id_asignatura_aprendizaje")
	@Getter @Setter private Integer idAsignaturaAprendizaje;
	
	@Column(name="id_paralelo")
	@Getter @Setter private Integer idParalelo;
	
	@Column(name="id_espacio_fisico")
	@Getter @Setter private Integer idEspacioFisico;
	
	@Column(name="num_estudiantes")
	@Getter @Setter private Integer numEstudiantes;
	@Column(name="afin_asignatura")
	@Getter @Setter private boolean afinAsignatura;
	
	
	@Column(name="fecha_desde")
	@Getter @Setter private Date fechaDesde;

	@Column(name="fecha_hasta")
	@Getter @Setter private Date fechaHasta;
	
	@Getter @Setter  private String estado;
	
	//@Column(name="fecha_ingreso")
	//@Getter @Setter  private Timestamp fechaIngreso;
	
	@Column(name="usuario_ingreso_id")
	@Getter @Setter  private String usuarioIngresoId;
	
	@Version
	@Getter @Setter  private Integer version;
	
	//RELACIONES
	//bi-directional many-to-one association to  Asignatura Aprendizaje
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_asignatura_aprendizaje" , insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private AsignaturaAprendizaje asignaturaAprendizaje;
	
	//bi-directional many-to-one association to asignatura Paralelo
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_paralelo" , insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Paralelo paralelo;
			
    //bi-directional many-to-one association to Jornada
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_espacio_fisico" , insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private EspacioFisico espacioFisico;
			
	//bi-directional many-to-one association to Tipo de Reglamento
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_distributivo_docente" , insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private DistributivoDocente distributivoDocente;
			
	//bi-directional one-to-many association to EstudianteRecalificacion
  	@OneToMany(mappedBy="docenteAsignaturaAprend")
  	@Getter @Setter private List<ActaCalificacion> actaCalificaciones;
  	
	@OneToMany(mappedBy="docenteAsignaturaAprend", cascade=CascadeType.ALL)
	@Getter @Setter private List<EstudianteAsignatura> estudianteAsignatura;


  	
	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }
}
