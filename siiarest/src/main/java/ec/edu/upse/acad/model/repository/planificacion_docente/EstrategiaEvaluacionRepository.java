package ec.edu.upse.acad.model.repository.planificacion_docente;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.planificacion_docente.EstrategiaEvaluacion;

@Repository
public interface EstrategiaEvaluacionRepository  extends JpaRepository<EstrategiaEvaluacion, Integer> {

	@Query(value="select e.id as idEstrategiaEvaluacion, e.codigo as codigo, e.descripcion as estrategia "+
			" from EstrategiaEvaluacion e ")
	List<CoListadoEvaluaciones> listaEstrategiasEvaluacion();
	interface CoListadoEvaluaciones{ 
	    Integer getIdEstrategiaEvaluacion();   
	    String getCodigo();
	    String getEstrategia();
	}
}
