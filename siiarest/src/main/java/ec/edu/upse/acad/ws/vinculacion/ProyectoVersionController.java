package ec.edu.upse.acad.ws.vinculacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ec.edu.upse.acad.model.repository.vinculacion.ProyectoVersionRepository;
import ec.edu.upse.acad.model.service.SecurityService;

@RestController
@RequestMapping("/api/proyectoVersion")
@CrossOrigin
public class ProyectoVersionController {
	@Autowired
	private SecurityService securityService;
	@Autowired
	private ProyectoVersionRepository proyectoVersionRepository;
	
	@RequestMapping(value = "/buscarEstadoProyectoVersion/{idProyecto}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarEstadoProyectoVersion(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable("idProyecto") Integer idProyecto) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(proyectoVersionRepository.buscarEstadoProyectoVersion(idProyecto).get());
	}

	@RequestMapping(value = "/buscarEstadoProyectoVersion1/{idProyectoVersion}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarEstadoProyectoVersion1(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable("idProyectoVersion") Integer idProyectoVersion) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(proyectoVersionRepository.buscarEstadoProyectoVersion1(idProyectoVersion).get());
	}
	

}
