package ec.edu.upse.acad.model.pojo;

import javax.persistence.*;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.List;


@Entity
@Table(schema="aca", name="componente_organizacion")
@Where(clause = "estado = 'A' ")
@NoArgsConstructor

public class ComponenteOrganizacion {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_componente_organizacion")
	@Getter @Setter private Integer id;

	@Getter @Setter private String abreviatura;

	@Getter @Setter private String descripcion;

	@Getter @Setter private String estado;
	
	@Getter @Setter private String color;
	
	@Column(name="fecha_ingreso")
	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;
	
	@Column(name="id_tipo_comp_organizacion")
	@Getter @Setter private Integer idTipoCompOrganizacion;

	//bi-directional many-to-one association to AsignaturaOrganizacion
	@OneToMany(mappedBy="componenteOrganizacion", cascade=CascadeType.ALL)
	@Getter @Setter private List<AsignaturaOrganizacion> asignaturaOrganizacions;

	//bi-directional many-to-one association to MallaCompOrganizacion
	@OneToMany(mappedBy="componenteOrganizacion", cascade=CascadeType.ALL)
	@Getter @Setter private List<ReglamentoCompOrganizacion> mallaCompOrganizacions;

	//bi-directional many-to-one association to TipoCompOrganizacion
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_tipo_comp_organizacion", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private TipoCompOrganizacion tipoCompOrganizacion;
	
	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }
}