package ec.edu.upse.acad.model.pojo.evaluacion;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="eva", name="tipo_recurso_apelacion")
@NoArgsConstructor
public class TipoRecursoApelacion {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_tipo_recurso_apelacion")
	@Getter @Setter private Integer id;

	@Column(name="descripcion")
	@Getter @Setter private String descripcion;

	@Getter @Setter private String estado;
	
//	@Column(name="fecha_ingreso")
//	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;
	
	@Version
	@Getter @Setter private Integer version;

	//bi-directional many-to-one association to AsignaturaRequisito
	@OneToMany(mappedBy="tipoRecursoApelacion", cascade=CascadeType.ALL)
	//@JsonIgnore
	@Getter @Setter private List<RecursoApelacion> recursoApelaciones;


	@PrePersist
	void preInsert() {
		if (this.estado == null)
			this.estado = "A";
	}
}
