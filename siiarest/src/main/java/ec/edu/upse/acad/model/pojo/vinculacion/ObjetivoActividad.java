package ec.edu.upse.acad.model.pojo.vinculacion;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema = "vin", name = "objetivo_actividad")
@NoArgsConstructor //un constructorsin argumentos
public class ObjetivoActividad {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_objetivo_actividad")
	@Getter	@Setter	private Integer id;
	
	/*
	 * @Column(name = "id_item_marco_logico")
	 * 
	 * @Getter @Setter private Integer idItemMarcoLogico;
	 */
	@Getter	@Setter	private String descripcion;
	
	@Column(name = "usuario_ingreso_id")
	@Getter	@Setter	private String usuarioIngresoId;

	@Version
	@Getter	@Setter	private Integer version;

	@Getter	@Setter	private String estado;
	
	@ManyToOne
	@JoinColumn(name = "id_item_marco_logico", nullable = false, updatable = false)
	@JsonBackReference
	@Getter	@Setter	private ItemMarcoLogico itemMarcoLogico;
		
	@PrePersist
	void preInsert() {
		if (this.estado == null)
			this.estado = "A";
	}

}
