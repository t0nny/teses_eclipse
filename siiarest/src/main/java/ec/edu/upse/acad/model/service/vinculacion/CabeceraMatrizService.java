package ec.edu.upse.acad.model.service.vinculacion;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jfree.util.Log;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.upse.acad.model.pojo.vinculacion.CabeceraMatriz;
import ec.edu.upse.acad.model.pojo.vinculacion.DetalleMatriz;
import ec.edu.upse.acad.model.pojo.vinculacion.ProyectoVersion;
import ec.edu.upse.acad.model.repository.vinculacion.CabeceraMatrizRepository;
import ec.edu.upse.acad.model.repository.vinculacion.CabeceraMatrizRepository.objetoDetMatriz;
import ec.edu.upse.acad.model.repository.vinculacion.DetalleMatrizRepository;
import ec.edu.upse.acad.model.repository.vinculacion.ProyectoVersionRepository;


@Service
@Transactional
public class CabeceraMatrizService {
	@PersistenceContext
	private EntityManager em;
	@Autowired
	private CabeceraMatrizRepository cabeceraMatrizRepository;
	@Autowired
	private DetalleMatrizRepository detalleMatrizRepository;
	@Autowired
	private ProyectoVersionRepository proyectoVersionRepository;
	
	public void grabarCabeceraMatriz(CabeceraMatriz cabeceraMatriz) {
		System.out.println(cabeceraMatriz.getId());
		if (cabeceraMatriz.getId() != null) {
			System.out.println("editar registro");
			editarCabeceraMatriz(cabeceraMatriz);
		} else {
			System.out.println("ingresar registro");
			nuevoCabeceraMatriz(cabeceraMatriz);
		}		
	}

	private void nuevoCabeceraMatriz(CabeceraMatriz cabeceraMatriz) {
		// TODO Auto-generated method stub
		cabeceraMatrizRepository.save(cabeceraMatriz);
		em.getEntityManagerFactory().getCache().evict(CabeceraMatriz.class);
	}

	private void editarCabeceraMatriz(CabeceraMatriz cabeceraMatriz) {
		// TODO Auto-generated method stub
		Log.info(cabeceraMatriz);
		CabeceraMatriz _cabeceraMatriz =cabeceraMatrizRepository.findById(cabeceraMatriz.getId()).get();
		BeanUtils.copyProperties(cabeceraMatriz, _cabeceraMatriz, "detalleMatriz");
		cabeceraMatrizRepository.save(_cabeceraMatriz);
		for (DetalleMatriz detalleMatriz : cabeceraMatriz.getDetalleMatriz()) {
			if(detalleMatriz.getId() != null) {
				DetalleMatriz _detalleMatriz = detalleMatrizRepository.findById(detalleMatriz.getId()).get();
				BeanUtils.copyProperties(detalleMatriz, _detalleMatriz, "cabeceraMatriz");
				_detalleMatriz.setCabeceraMatriz(_cabeceraMatriz);
				detalleMatrizRepository.save(_detalleMatriz);
			}else {
				System.out.println("Nueva Detalle MATRIZ");
				DetalleMatriz __detalleMatriz=new DetalleMatriz();
				BeanUtils.copyProperties(detalleMatriz, __detalleMatriz, "cabeceraMatriz");
				__detalleMatriz.setCabeceraMatriz(_cabeceraMatriz);
				detalleMatrizRepository.save(__detalleMatriz);
			}
		}
		em.getEntityManagerFactory().getCache().evict(CabeceraMatriz.class);
	}
	
	//actualizar el estado de la matriz ex-ante
			public void updateEstadoMatriz(CabeceraMatriz cabeceraMatriz) {
				List<objetoDetMatriz> _detalleMatriz = cabeceraMatrizRepository.buscarDetMatriz(cabeceraMatriz.getIdProyectoVersion());
				System.out.println("detalle"+cabeceraMatriz.getIdProyectoVersion());
				ProyectoVersion _proyectoVersion = proyectoVersionRepository.findById(cabeceraMatriz.getIdProyectoVersion()).get();
				_proyectoVersion.setEstadoProyectoVersion(cabeceraMatriz.getEstadoMatriz());
				proyectoVersionRepository.save(_proyectoVersion);
				CabeceraMatriz _cabeceraMatriz =cabeceraMatrizRepository.findById(_detalleMatriz.get(0).getIdCabMatriz()).get();
				 _cabeceraMatriz.setEstadoMatriz(cabeceraMatriz.getEstadoMatriz());
				cabeceraMatrizRepository.save(_cabeceraMatriz);
			em.getEntityManagerFactory().getCache().evict(CabeceraMatriz.class);
		}


}
