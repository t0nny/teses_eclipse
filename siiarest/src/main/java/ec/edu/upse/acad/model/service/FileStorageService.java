package ec.edu.upse.acad.model.service;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

@Service
public class FileStorageService {

	private static final String PATH_REPOSITORIO = "/";
	
    private final Path fileStorageLocation;
    
    /*****************hecho por Peters*********************************/
    private final Path fileStorageLocationVinculacion;
    /******************************************************************/

    public FileStorageService() throws Exception {
        this.fileStorageLocation = Paths.get(PATH_REPOSITORIO)
                .toAbsolutePath().normalize();
        
        /********************hecho por Peters****************************/
    	StringBuilder builder= new StringBuilder();// creamos la ruta donde guardaremos el archivo
		builder.append(System.getProperty("user.home"));//OBTINE LA RUTA HOME del cualquier SO win linux mac
		builder.append(File.separator); //para no tener erros con los separadores
		builder.append("proyecto_vinculacion");// nombre de la carpeta que crearemos en la ruta home
		builder.append(File.separator);
		
		this.fileStorageLocationVinculacion = Paths.get(builder.toString()) //creamos la carpeta
                .toAbsolutePath().normalize();
        /******************************************************************/

        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ex) {
            throw new Exception("No se pudo crear el directorio para almacenar los archivos.", ex);
        }
    }

    public String storeFile(MultipartFile file) throws Exception {
        // Normaliza el nombre del archivo.
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        try {
            // Verifica si tiene caracteres especiales
            if(fileName.contains("..")) {
                throw new Exception("El nombre de archivo no es valido. " + fileName);
            }

            // Copia el archivo al destino.
            Path targetLocation = this.fileStorageLocation.resolve(fileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            return fileName;
        } catch (IOException ex) {
            throw new Exception("No se pudo almacenar el archivo " + fileName + ".", ex);
        }
    }

    public Resource loadFileAsResource(String fileName) throws Exception {
        try {
            Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if(resource.exists()) {

                return resource;
            } else {
                throw new Exception("Archivo no encontrado " + fileName);
            }
        } catch (MalformedURLException ex) {
            throw new Exception("Archivo no encontrado " + fileName, ex);
        }
    }
    
    /*****************hecho por Peters*********************************/
    public Resource cargarFileDelServer(String fileName) throws Exception {
        try {
            Path filePath = this.fileStorageLocationVinculacion.resolve(fileName).normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if(resource.exists()) {

                return resource;
            } else {
                throw new Exception("Archivo no encontrado " + fileName);
            }
        } catch (MalformedURLException ex) {
            throw new Exception("Archivo no encontrado " + fileName, ex);
        }
    }
    /******************************************************************/
}