package ec.edu.upse.acad.model.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ec.edu.upse.acad.model.pojo.AsignaturaAprendizaje;
import ec.edu.upse.acad.model.pojo.AsignaturaOrganizacion;
import ec.edu.upse.acad.model.pojo.AsignaturaRelacion;
import ec.edu.upse.acad.model.pojo.ComponenteAprendizaje;
import ec.edu.upse.acad.model.pojo.ComponenteOrganizacion;
import ec.edu.upse.acad.model.pojo.DepartamentoOferta;
import ec.edu.upse.acad.model.pojo.Itinerario;
import ec.edu.upse.acad.model.pojo.ItinerarioMateria;
import ec.edu.upse.acad.model.pojo.Malla;
import ec.edu.upse.acad.model.pojo.MallaAsignatura;
import ec.edu.upse.acad.model.pojo.MallaRequisito;
import ec.edu.upse.acad.model.pojo.Malla;
import ec.edu.upse.acad.model.pojo.Modalidad;
import ec.edu.upse.acad.model.pojo.seguridad.Modulo;
import ec.edu.upse.acad.model.pojo.Nivel;
import ec.edu.upse.acad.model.pojo.seguridad.Opcion;
import ec.edu.upse.acad.model.pojo.seguridad.Parametrica;
import ec.edu.upse.acad.model.pojo.PeriodoAcademico;
import ec.edu.upse.acad.model.pojo.seguridad.Persona;
import ec.edu.upse.acad.model.pojo.seguridad.Rol;
import ec.edu.upse.acad.model.pojo.TipoCompOrganizacion;
import ec.edu.upse.acad.model.pojo.distributivo.DistributivoDedicacion;
import ec.edu.upse.acad.model.pojo.distributivo.DistributivoDocente;
import ec.edu.upse.acad.model.pojo.distributivo.DocenteActividad;
import ec.edu.upse.acad.model.pojo.distributivo.DocenteAsignaturaAprend;
import ec.edu.upse.acad.model.pojo.matricula.DetalleMovilidad;
import ec.edu.upse.acad.model.pojo.matricula.Movilidad;
import ec.edu.upse.acad.model.repository.AsignaturaAprendizajeRepository;
import ec.edu.upse.acad.model.repository.AsignaturaOrganizacionRepository;
import ec.edu.upse.acad.model.repository.AsignaturasRelacionesRepository;
import ec.edu.upse.acad.model.repository.ComponenteAprendizajeRepository;
import ec.edu.upse.acad.model.repository.ComponenteOrganizacionRepository;
import ec.edu.upse.acad.model.repository.DepartamentoOfertaRepository;
import ec.edu.upse.acad.model.repository.ItinerarioMateriaRepository;
import ec.edu.upse.acad.model.repository.ItinerarioRepository;
import ec.edu.upse.acad.model.repository.MallaAsignaturaRepository;
import ec.edu.upse.acad.model.repository.ModulosRepository;
import ec.edu.upse.acad.model.repository.NivelRepository;
import ec.edu.upse.acad.model.repository.OpcionesRepository;
import ec.edu.upse.acad.model.repository.ParametricaRepository;
import ec.edu.upse.acad.model.repository.PeriodoAcademicoRepository;
import ec.edu.upse.acad.model.repository.PersonasRepository;
import ec.edu.upse.acad.model.repository.RolesRepository;
import ec.edu.upse.acad.model.repository.TipoCompOrganizacionRepository;
import ec.edu.upse.acad.model.repository.distributivo.DistributivoDocenteRepository;
import ec.edu.upse.acad.model.repository.MallaRepository;
import ec.edu.upse.acad.model.repository.ModalidadRepository;


@Service
@Transactional
public class UpdateForIdService {

	@Autowired private PersonasRepository estudianteRepository;
	@Autowired private MallaRepository mallaversionRepository;
	@Autowired private MallaRepository mallaRepository;

	@PersistenceContext
	private EntityManager em;
	
	public void borraEstudiante(Integer id) {
		Persona persona = estudianteRepository.findById(id).get();
		if (persona !=null) {
			persona.setEstado("X");
			estudianteRepository.save(persona);
			/* Codigo adicinal */
		}
	}

	@Autowired private ModulosRepository modulosRepository;

	public void borraModulo(Integer id) {
		Modulo modulos = modulosRepository.findById(id).get();
		if (modulos !=null) {
			modulos.setEstado("IN");
			modulosRepository.save(modulos);
		}
	}

	@Autowired private RolesRepository rolRepository;

	public void borraRol(Integer id) {
		Rol rol = rolRepository.findById(id).get();
		if (rol !=null) {
			rol.setEstado("IN");
			rolRepository.save(rol);

		}
	}

	@Autowired private OpcionesRepository opcionRepository;

	public void borraOpcion(Integer id) {
		Opcion opcion = opcionRepository.findById(id).get();
		if (opcion !=null) {
			opcion.setEstado("IN");
			opcionRepository.save(opcion);

		}
	}

	@Autowired private TipoCompOrganizacionRepository tipocomporganizacionRepository;

	public void borraTipoCompOrganizacion(Integer id) {
		TipoCompOrganizacion tipocomporganizacion = tipocomporganizacionRepository.findById(id).get();
		if (tipocomporganizacion !=null) {
			tipocomporganizacion.setEstado("I");
			tipocomporganizacionRepository.save(tipocomporganizacion);

		}
	}

	@Autowired private ComponenteOrganizacionRepository componenteorganizacionRepository;

	public void borraComponenteOrganizacion(Integer id) {
		ComponenteOrganizacion componenteorganizacion = componenteorganizacionRepository.findById(id).get();
		if (componenteorganizacion !=null) {
			componenteorganizacion.setEstado("I");
			componenteorganizacionRepository.save(componenteorganizacion);

		}
	}

	@Autowired private ComponenteAprendizajeRepository componenteaprendizajeRepository;

	public void borraComponenteAprendizaje(Integer idComponenteAprendizaje) {
		ComponenteAprendizaje componenteaprendizaje = componenteaprendizajeRepository.findById(idComponenteAprendizaje).get();
		if (componenteaprendizaje !=null) {
			componenteaprendizaje.setEstado("I");
			componenteaprendizajeRepository.save(componenteaprendizaje);

		}
	}	
	@Autowired private ParametricaRepository parametricaRepository;

	public void borraParametrica(Integer id) {
		Parametrica parametrica = parametricaRepository.findById(id).get();
		if (parametrica !=null) {
			parametrica.setEstado("X");
			parametricaRepository.save(parametrica);

		}
	}

	@Autowired private PeriodoAcademicoRepository periodoacademicoRepository;
	public void borraPeriodoAcademico(Integer idPeriodoAcademico) {

		PeriodoAcademico periodoacademico = periodoacademicoRepository.findById(idPeriodoAcademico).get();
		if (periodoacademico !=null) {
			periodoacademico.setEstado("I");
			periodoacademicoRepository.save(periodoacademico);
		}
	}

	//Funci�n para borrar itinerarios
	@Autowired private ItinerarioRepository itinerarioRepository;
	public void borrarItinerario(Integer idItinerario) {
		Itinerario itinerario = itinerarioRepository.findById(idItinerario).get();
		if (itinerario !=null) {
			itinerario.setEstado("I");
			itinerarioRepository.save(itinerario);
		}

	}

	//Funci�n para borrar itinerarioMateria
	@Autowired private ItinerarioMateriaRepository itinerarioMateriaRepository;
	public void borrarItinerarioMateria(Integer idItinerarioMateria) {
		ItinerarioMateria itinerarioMateria = itinerarioMateriaRepository.findById(idItinerarioMateria).get();
		if (itinerarioMateria !=null) {
			itinerarioMateria.setEstado("I");
			itinerarioMateriaRepository.save(itinerarioMateria);
		}

	}

	//Funci�n para borrar modalidad
	@Autowired private ModalidadRepository modalidadRepository;
	public void borrarModalidad(Integer idModalidad) {
		Modalidad modalidad = modalidadRepository.findById(idModalidad).get();
		if (modalidad !=null) {
			modalidad.setEstado("I");
			modalidadRepository.save(modalidad);
		}

	}

	//Funci�n para borrar nivel
	@Autowired private NivelRepository nivelRepository;
	public void borrarNivel(Integer idNivel) {
		Nivel nivel = nivelRepository.findById(idNivel).get();
		if (nivel !=null) {
			nivel.setEstado("I");
			nivelRepository.save(nivel);
		}

	}

	//Funci�n para borrar departamento_oferta
	@Autowired private DepartamentoOfertaRepository departamentoOfertaRepository;
	public void borrarDepartamentoOferta(Integer idDepartamentoOferta) {
		DepartamentoOferta departamentoOferta = departamentoOfertaRepository.findById(idDepartamentoOferta).get();
		if (departamentoOferta !=null) {
			departamentoOferta.setEstado("I");
			departamentoOfertaRepository.save(departamentoOferta);
		}

	}

	//Funci�n para borrar Malla
	@Autowired private MallaRepository mallaEliminaRepository;
	public void borrarMalla(Integer idMalla) {
		Malla malla = mallaEliminaRepository.findById(idMalla).get();
		if (malla !=null) {
			malla.setEstado("I");
			mallaEliminaRepository.save(malla);
		}

	}

	//Funci�n para borrar Malla Version
	@Autowired private MallaRepository mallaVersionRepository;
	public void borrarMallaVersion(Integer idMallaVersion) {
		Malla mallaVersion = mallaVersionRepository.findById(idMallaVersion).get();
		if (mallaVersion !=null) {
			mallaVersion.setEstado("I");
			mallaVersionRepository.save(mallaVersion);
		}

	}

	/***
	 * Función para borrar los registros de malla_asignatura y sua dependencias en las tablas 
	 * asignatura_aprendizaje y asignatura_organizacion
	 */
	
	@Autowired private MallaAsignaturaRepository mallaAsignaturaRepository;
	public void borrarMallaAsignatura(Integer idMallaAsignatura) {
		MallaAsignatura mallaAsignatura = mallaAsignaturaRepository.findById(idMallaAsignatura).get();
		if (mallaAsignatura !=null) {
			mallaAsignatura.setEstado("I");
			for (int i = 0; i < mallaAsignatura.getAsignaturaAprendizaje().size(); i++) {
				AsignaturaAprendizaje  asignatura_aprendizaje=  mallaAsignatura.getAsignaturaAprendizaje().get(i);
				if (asignatura_aprendizaje.getId() != null)
					asignatura_aprendizaje.setEstado("I");
			}
			for (int i = 0; i < mallaAsignatura.getAsignaturaOrganizacions().size(); i++) {
				AsignaturaOrganizacion  asignatura_organizacion=  mallaAsignatura.getAsignaturaOrganizacions().get(i);
				if (asignatura_organizacion.getId() != null)
					asignatura_organizacion.setEstado("I");
			}

			mallaAsignaturaRepository.save(mallaAsignatura);
		}
	}
	
	//Funci�n para borrar AsignaturaAprendizaje
	@Autowired private AsignaturaOrganizacionRepository asignaturaOrganizacionRepository;
	public void borrarAsignaturaOrganizacion(Integer idAsignaturaAprendizaje) {
		AsignaturaOrganizacion asignaturaOrganizacion = asignaturaOrganizacionRepository.findById(idAsignaturaAprendizaje).get();
		if (asignaturaOrganizacion !=null) {
			asignaturaOrganizacion.setEstado("I");
			asignaturaOrganizacionRepository.save(asignaturaOrganizacion);
		}

	}

	//Funci�n para borrar por completo a un docente de un distributivo 
	@Autowired private DistributivoDocenteRepository distributivoDocenteRepository;
	public void borrarDocenteDeDistributivo(Integer idDistributivoOfertaVersion ,Integer idDocente) {
		DistributivoDocente distributivoDocente = distributivoDocenteRepository.docenteEnDistributivoAEliminar(idDistributivoOfertaVersion,idDocente);
		if (distributivoDocente !=null) {
			distributivoDocente.setEstado("I");
			for (int i = 0; i < distributivoDocente.getDocenteAsignaturaAprends().size(); i++) {
				DocenteAsignaturaAprend  _docenteAsignaturaAprend= distributivoDocente.getDocenteAsignaturaAprends().get(i);
				if (_docenteAsignaturaAprend.getId() != null)
				_docenteAsignaturaAprend.setEstado("I");
			}
			for (int i = 0; i < distributivoDocente.getDistributivoDedicaciones().size(); i++) {
				DistributivoDedicacion  _distributivoDedicacion= distributivoDocente.getDistributivoDedicaciones().get(i);
				if (_distributivoDedicacion.getId() != null)
				_distributivoDedicacion.setEstado("I");
			}
			for (int i = 0; i < distributivoDocente.getDocenteActividades().size(); i++) {
				DocenteActividad  _docenteActividad= distributivoDocente.getDocenteActividades().get(i);
				if (_docenteActividad.getId() != null)
				_docenteActividad.setEstado("I");
			}
			distributivoDocenteRepository.save(distributivoDocente);
		}
	}
	
	public void eliminacionLogicaDistDoc(Integer idDistributivoDocente) {
		//DistributivoDocente distributivoDocente = distributivoDocenteRepository.docenteEnDistributivoAEliminar(idDistributivoOfertaVersion,idDocente);
		DistributivoDocente distributivoDocente = distributivoDocenteRepository.findById(idDistributivoDocente).get();
		if (distributivoDocente !=null) {
			distributivoDocente.setEstado("I");
			for (int i = 0; i < distributivoDocente.getDocenteAsignaturaAprends().size(); i++) {
				DocenteAsignaturaAprend  _docenteAsignaturaAprend= distributivoDocente.getDocenteAsignaturaAprends().get(i);
				if (_docenteAsignaturaAprend.getId() != null)
				_docenteAsignaturaAprend.setEstado("I");
			}
			for (int i = 0; i < distributivoDocente.getDistributivoDedicaciones().size(); i++) {
				DistributivoDedicacion  _distributivoDedicacion= distributivoDocente.getDistributivoDedicaciones().get(i);
				if (_distributivoDedicacion.getId() != null)
				_distributivoDedicacion.setEstado("I");
			}
			for (int i = 0; i < distributivoDocente.getDocenteActividades().size(); i++) {
				DocenteActividad  _docenteActividad= distributivoDocente.getDocenteActividades().get(i);
				if (_docenteActividad.getId() != null)
				_docenteActividad.setEstado("I");
			}
			distributivoDocenteRepository.save(distributivoDocente);
		}
	}
	
	//Funci�n para borrar Una Actividad Dsitributivo
	@Autowired private MallaAsignaturaRepository mallaAsignaturaEliminarRepository;
	public void borrarAsignaturaDocenteDistributivo(Integer idAsignaturaVersion, Integer idDistributivoDocente) {
		MallaAsignatura mallaAsignatura = mallaAsignaturaEliminarRepository.asignaturaEliminarDocenteDistributivo(idAsignaturaVersion, idDistributivoDocente);
		if (mallaAsignatura !=null) {
			for (int i = 0; i < mallaAsignatura.getAsignaturaAprendizaje().size(); i++) {
				for (int j = 0; j <  mallaAsignatura.getAsignaturaAprendizaje().get(i).getDocenteAsignaturaAprend().size(); j++) {
					DocenteAsignaturaAprend  _docenteAsignaturaAprend= mallaAsignatura.getAsignaturaAprendizaje().get(i).getDocenteAsignaturaAprend().get(j);
					if (_docenteAsignaturaAprend.getId() != null)
					_docenteAsignaturaAprend.setEstado("I");
				}
			}
			mallaAsignaturaEliminarRepository.save(mallaAsignatura);
		}

	}

}
