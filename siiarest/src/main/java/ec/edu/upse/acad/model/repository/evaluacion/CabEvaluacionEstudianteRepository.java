package ec.edu.upse.acad.model.repository.evaluacion;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.evaluacion.CabEvaluacionEstudiante;

@Repository
public interface CabEvaluacionEstudianteRepository extends JpaRepository<CabEvaluacionEstudiante, Integer> {

}
