package ec.edu.upse.acad.model.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;

import ec.edu.upse.acad.model.repository.PeriodoDocenteAsignaturaRepository.customObjetbuscarPeriodoDocenteAsignatura;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

import javax.servlet.ServletContext;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
@Service
public class ExcelService {
	private static final String PATH_TEMPORAL = "tmp";
	
	@Autowired private ServletContext context; 
	
    private String getRealPath(String resource) {
    	return context.getRealPath("") + File.separator + resource;
    }
	public Resource generarExcel(List<customObjetbuscarPeriodoDocenteAsignatura> listaDocente) throws Exception {
		String filePath="";
		 filePath = getRealPath(PATH_TEMPORAL) + File.separator + UUID.randomUUID() + ".xls";
	     File fileName = new File("reporte.xls");
	    
	 	if (listaDocente !=null) {
	 		
	 		
			for (int i = 0; i < listaDocente.size(); i++) {
				System.out.println("registro: "+listaDocente.get(i));
				
				
				}	
			}
	     
        // Se crea el libro
		HSSFWorkbook libro = new HSSFWorkbook();

        // Se crea una hoja dentro del libro
        HSSFSheet hoja = libro.createSheet();

        // Se crea una fila dentro de la hoja
        HSSFRow fila = hoja.createRow(0);

        // Se crea una celda dentro de la fila
        HSSFCell celda = fila.createCell((short) 0);

        // Se crea el contenido de la celda y se mete en ella.
        HSSFRichTextString texto = new HSSFRichTextString("hola mundo");
        celda.setCellValue(texto);

        // Se salva el libro.
        try {
            FileOutputStream elFichero = new FileOutputStream(filePath);
            libro.write(elFichero);
            elFichero.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        Resource resource = new UrlResource(Paths.get(filePath).toUri());
        if(resource.exists()) {
            return resource;
        } else {
            throw new Exception("Archivo no encontrado " + fileName);
        }
		
	}
}
