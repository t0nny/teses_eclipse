/**
 * 
 */
package ec.edu.upse.acad.model.repository.evaluacion;


import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.evaluacion.EvaluacionDocenteGeneral;
import ec.edu.upse.acad.model.pojo.reglamento.Reglamento;

/**
 * @author dengi
 *
 */
@Repository
public interface EvaluacionDocenteGeneralRepository extends JpaRepository<EvaluacionDocenteGeneral, Integer>{
	
	@Query(value = "select edg.id as id,edg.descripcion as descripcion ,"
			+ "edg.estado as estado "
			+ "from EvaluacionDocenteGeneral edg "
			+ "where edg.estado = 'A' ")
	List<CO_EvalDocGral> getAllEvalDocGral();
	interface CO_EvalDocGral{
		Integer getId();
		String getDescripcion();
		Date getFechaDesde();
		Date getFechahasta();
		String getEstado();
	}
	
	
	@Query(value = "select edg.id as id,edg.descripcion as descripcion "
			+ "from EvaluacionDocenteGeneral edg "
			+ "JOIN PeriodoEvaluacion pev on pev.evaluacionDocenteGeneral.id = edg.id "
			+ "where edg.estado = 'A' AND pev.evaluacionDocenteGeneral.id = ?1")
	List<CO_PEval> getPEvalDoc(Integer idEvalDocGral);
	interface CO_PEval{
		Integer getId();
		String getDescripcion();
	}	
	
	@Query(value = "select r.id as idReglamento,r.nombre as nombreReglamento, r.estado as estado "
			+ "from Reglamento r "
			+ "where r.estado = 'A'")
	List<CO_Reglamentos> getReglamentos();
	interface CO_Reglamentos{
		Integer getIdReglamento();
		String getNombreReglamento();
		String getEstado();
	}
	/*@Query(value = "select r "+
			+ "from Reglamento r "
			+ "where r.estado = 'A'")
	List<Reglamento> getReglamentos();*/
	
	@Query(value = "select edg.id as id, edg.descripcion as descripcion, edg.fechaDesde as fechaDesde,"
			+ "edg.fechaHasta as fechaHasta, edg.estado as estado, edg.reglamento.id as idReglamento, edg.usuarioIngresoId as usuarioIngresoId "
			+ "FROM EvaluacionDocenteGeneral edg "
			+ "JOIN Reglamento r on edg.reglamento.id = r.id "
			+ "WHERE edg.estado = 'A' AND edg.id=?1")
	List<CO_GetEvalDocGral> getEvalDocGralById(Integer idEvalDocGral);
	interface CO_GetEvalDocGral{
		Integer getId();
		String getDescripcion();
		Date getFechaDesde();
		Date getFechaHasta();
		String getEstado();
		Integer getIdReglamento();
		String getUsuarioIngresoId();
	}
}
