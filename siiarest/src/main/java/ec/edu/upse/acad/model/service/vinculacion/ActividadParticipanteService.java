package ec.edu.upse.acad.model.service.vinculacion;

import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.upse.acad.model.pojo.vinculacion.ActividadParticipante;
import ec.edu.upse.acad.model.pojo.vinculacion.ObjetivoTarea;
import ec.edu.upse.acad.model.repository.vinculacion.ActividadParticipanteRepository;
import ec.edu.upse.acad.model.repository.vinculacion.ObjetivoTareaRepository;

@Service
@Transactional
public class ActividadParticipanteService {
	@PersistenceContext
	private EntityManager em;
	@Autowired
	private ActividadParticipanteRepository actividadParticipanteRepository;
	@Autowired
	private ObjetivoTareaRepository objetivoTareaRepository;
	
	public void guardarActividadParticipante(List<ActividadParticipante> actividadParticipante) {
		for (Iterator iterator = actividadParticipante.iterator(); iterator.hasNext();) {
			ActividadParticipante __actividadParticipante = (ActividadParticipante) iterator.next();

			if (__actividadParticipante.getId() != null) {
				System.out.println("Edita ActividadParticipante");
				ActividadParticipante _actividadParticipante = actividadParticipanteRepository
						.findById(__actividadParticipante.getId()).get();
				BeanUtils.copyProperties(__actividadParticipante, _actividadParticipante);
				actividadParticipanteRepository.save(_actividadParticipante);
			} else {
				System.out.println("Nueva ActividadParticipante");
				ActividadParticipante _actividadParticipante = new ActividadParticipante();
				BeanUtils.copyProperties(__actividadParticipante, _actividadParticipante);
				actividadParticipanteRepository.save(__actividadParticipante);
				ObjetivoTarea _objetivoTarea =objetivoTareaRepository.findById(__actividadParticipante.getIdObjetivoTarea()).get();
				_objetivoTarea.setEstadoTarea("SE");
				objetivoTareaRepository.save(_objetivoTarea);
			}
		}
		em.getEntityManagerFactory().getCache().evict(ActividadParticipante.class);
	}


}
