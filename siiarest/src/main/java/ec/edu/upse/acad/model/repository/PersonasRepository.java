package ec.edu.upse.acad.model.repository;

import java.util.List;

import javax.persistence.criteria.Subquery;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.seguridad.Persona;
import ec.edu.upse.acad.model.pojo.seguridad.Usuario;




@Repository
public interface PersonasRepository 
extends JpaRepository<Persona, Integer>,
JpaSpecificationExecutor<Persona>{

	Persona findByIdentificacion(String identificacion);


	//implementacion de paginado
	Page<Persona> findAll(Pageable pageable);

	//Implementacion de opcion de filtSpecificationrado y paginado
	Page<Persona> findAll(Specification<Persona> spec, Pageable pageable);

	@Query(value="SELECT e "
			+ "FROM Persona AS e "
			+ "WHERE e.nombres LIKE ?1 "
			+ "OR e.apellidos LIKE ?1 ")
	List<Persona> buscarPorNombre(String nombre);

	//Definicion de especificacion para filtrar personas con usuario
	static Specification<Persona> existeUsuario() {
		return (root, query, cb) -> {
			Subquery<Integer> sq = query.subquery(Integer.class);
			return cb.exists(sq.select(cb.literal(1)).where(cb.equal(root,sq.from(Usuario.class).get("persona"))));
		};
	}

}




