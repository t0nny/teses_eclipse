package ec.edu.upse.acad.model.service.eva;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.upse.acad.model.pojo.evaluacion.CalificacionEscala;
import ec.edu.upse.acad.model.pojo.evaluacion.CategoriaPregunta;
import ec.edu.upse.acad.model.pojo.evaluacion.FuncionEvaluacion;
import ec.edu.upse.acad.model.pojo.evaluacion.Instrumento;
import ec.edu.upse.acad.model.pojo.evaluacion.OpcionRespuesta;
import ec.edu.upse.acad.model.pojo.evaluacion.ReglaCalificacion;
import ec.edu.upse.acad.model.pojo.evaluacion.TipoPregunta;
import ec.edu.upse.acad.model.pojo.evaluacion.TipoRecursoApelacion;
import ec.edu.upse.acad.model.repository.evaluacion.CalificacionEscalaRepository;
import ec.edu.upse.acad.model.repository.evaluacion.CategoriaPreguntaRepository;
import ec.edu.upse.acad.model.repository.evaluacion.FuncionEvaluacionRepository;
import ec.edu.upse.acad.model.repository.evaluacion.InstrumentoRepository;
import ec.edu.upse.acad.model.repository.evaluacion.OpcionRespuestaRepository;
import ec.edu.upse.acad.model.repository.evaluacion.ReglaCalificacionRepository;
import ec.edu.upse.acad.model.repository.evaluacion.TipoPreguntaRepository;
import ec.edu.upse.acad.model.repository.evaluacion.TipoRecursoApelacionRepository;

@Service
@Transactional
public class EvaParametricasService {
	//Repositorios para eliminar
	@Autowired private CalificacionEscalaRepository calificacionEscalaRepository;
	@Autowired private CategoriaPreguntaRepository categoriaPreguntaRepository;
	@Autowired private FuncionEvaluacionRepository funcionEvaluacionRepository;
	@Autowired private TipoPreguntaRepository tipoPreguntaRepository;
	@Autowired private TipoRecursoApelacionRepository tipoRecursoApelacionRepository;
	@Autowired private ReglaCalificacionRepository	 reglaCalificacionRepository;
	@Autowired private OpcionRespuestaRepository opcionRespuestaRepository;
	@Autowired private InstrumentoRepository instrumentoRepository;
	
	public void eliminaCalificacionEscala(Integer idCalificacionEscala) {
		CalificacionEscala calificacionEscala = calificacionEscalaRepository.findById(idCalificacionEscala).get();
		if (calificacionEscala !=null) {
			calificacionEscala.setEstado("I");
			calificacionEscalaRepository.save(calificacionEscala);
		}
	}
	
	public void eliminaReglaCalificacionEscala(Integer idReglaCalificacionEscala) {
		ReglaCalificacion reglaCalificacion = reglaCalificacionRepository.findById(idReglaCalificacionEscala).get();
		if (reglaCalificacion  !=null) {
			reglaCalificacion.setEstado("I");
			reglaCalificacionRepository.save(reglaCalificacion);
		}
	}
	
	public void eliminaCategoriaPregunta(Integer idCategoriaPregunta) {
		CategoriaPregunta categoriaPregunta = categoriaPreguntaRepository.findById(idCategoriaPregunta).get();
		if (categoriaPregunta !=null) {
			categoriaPregunta.setEstado("I");
			categoriaPreguntaRepository.save(categoriaPregunta);
		}
	}
	
	public void eliminaFuncionEvaluacion(Integer idFuncionEvaluacion) {
		FuncionEvaluacion funcionEvaluacion = funcionEvaluacionRepository.findById(idFuncionEvaluacion).get();
		if (funcionEvaluacion !=null) {
			funcionEvaluacion.setEstado("I");
			funcionEvaluacionRepository.save(funcionEvaluacion);
		}
	}
	
	public void eliminaTipoPregunta(Integer idTipoPregunta) {
		TipoPregunta tipoPregunta = tipoPreguntaRepository.findById(idTipoPregunta).get();
		if (tipoPregunta !=null) {
			tipoPregunta.setEstado("I");
			tipoPreguntaRepository.save(tipoPregunta);
		}
	}
	
	public void eliminaTipoRecursoApelacion(Integer idTipoRecursoApelacion) {
		TipoRecursoApelacion tipoRecursoApelacion = tipoRecursoApelacionRepository.findById(idTipoRecursoApelacion).get();
		if (tipoRecursoApelacion !=null) {
			tipoRecursoApelacion.setEstado("I");
			tipoRecursoApelacionRepository.save(tipoRecursoApelacion);
		}
	}
	
	public void eliminaOpcionRespuesta(Integer idOpcionRespuesta) {
		OpcionRespuesta opcionRespuesta = opcionRespuestaRepository.findById(idOpcionRespuesta).get();
		if (opcionRespuesta !=null) {
			opcionRespuesta.setEstado("I");
			opcionRespuestaRepository.save(opcionRespuesta);
		}
	}

	public void eliminaInstrumento(Integer idInstrumento) {
		Instrumento instrumento = instrumentoRepository.findById(idInstrumento).get();
		if (instrumento !=null) {
			instrumento.setEstado("I");
			instrumentoRepository.save(instrumento);
		}
	}
}
