package ec.edu.upse.acad.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ec.edu.upse.acad.model.pojo.MallaRequisito;

public interface MallaRequisitoRepository extends JpaRepository<MallaRequisito, Integer> {

	// malla version por idmallaversion
	@Query(value="select distinct mr.id as id,o.descripcion as descripcionOferta, m.descripcion as descripcionMalla, "
			+ " r.descripcion as descripcionRequisito,r.abreviatura as abreviatura, mr.horasTotal as horasTotal " 
			+ " from MallaRequisito mr, Oferta o, DepartamentoOferta do, Requisito r, Malla m " 
			+ " inner join Malla m on mr.idMalla= m.id "  
			+ " inner join DepartamentoOferta do on m.idDepartamentoOferta = do.id " 
			+ " inner join Oferta o on do.idOferta = o.id " 
			+ " inner join Requisito r on mr.idRequisito=r.id "
			+"  where mr.estado ='A' and m.id=?1")
	List<CustomObject> filtrarMallaRequisitoIdMalla(Integer idMallaVersion);
	interface CustomObject {
		Integer getId();
		String getDescripcionOferta();
		String getDescripcionMalla();
		String getDescripcionRequisito();
		String getAbreviatura();
		Integer getHorasTotal();
	}	
	@Query(value="select distinct mr.id as id,m.id as idMalla,m.descripcion as descripcionMalla, "
			+ " r.id as idRequisito, r.descripcion as descripcionRequisito, "
			+ " r.abreviatura as abreviatura, mr.horasTotal as horasTotal " 
			+ " from MallaRequisito mr, Requisito r, Malla m " 
			+ " inner join Malla m on mr.idMalla= m.id "  
			+ " inner join Requisito r on mr.idRequisito=r.id "
			+"  where mr.estado ='A' and mr.id=?1")
	List<MallaRequisitoObject> filtrarMallaRequisitoId(Integer idMallaVersion);
	interface MallaRequisitoObject {
		Integer getId();
		Integer getIdMalla();
		String getDescripcionMalla();
		Integer getIdRequisito();
		String getDescripcionRequisito();
		String getAbreviatura();
		Integer getHorasTotal();
	}

	@Query(value="select distinct mr.id as id,m.id as idMalla,m.descripcion as descripcionMalla, "
			+ " r.id as idRequisito, r.descripcion as descripcionRequisito, "
			+ " r.abreviatura as abreviatura, mr.horasTotal as horasTotal " 
			+ " from MallaRequisito mr, Requisito r, Malla m " 
			+ " inner join Malla m on mr.idMalla= m.id "  
			+ " inner join Requisito r on mr.idRequisito=r.id "
			+"  where mr.estado ='A' and mr.idMalla=?1 and mr.idRequisito=?2")
	List<MallaRequisito> buscarMallaRequisitoIdMallaIdRequisito(Integer idMalla,Integer idRequisito);
	interface MallaRequisito {
		Integer getId();
		Integer getIdMalla();
		String getDescripcionMalla();
		Integer getIdRequisito();
		String getDescripcionRequisito();
		String getAbreviatura();
		Integer getHorasTotal();
	}

	@Query(value=" select mr.idMalla as idMalla, r.id as idRequisito,r.descripcion as descripcion from MallaRequisito mr " + 
			"	inner join Requisito r on r.id = mr.idRequisito " + 
			"	where mr.idMalla = (?1) and mr.estado = 'A' and r.estado = 'A' and mr.aplicaAsignatura = 1")
	List<AsignaturasRequisito> listarRequisitosPermitidosAsignatura(Integer idMalla);
	interface AsignaturasRequisito {
		Integer getIdMalla();
		Integer getIdRequisito();
		String getDescripcion();
	}


	@Query(value = " select ar.idRequisito as idRequisito,r.descripcion as descripcion ,SUM(ar.numCreditos) as creditos,SUM(ar.horas) as horas  "
			+ " from AsignaturaRequisito ar inner join MallaAsignatura ma on ma.id = ar.idMallaAsignatura "
			+ " inner join Requisito r on r.id = ar.idRequisito "
			+ " where ma.idMalla = (?1) and ar.estado='A' and r.estado = 'A' and ma.estado='A' "
			+ " group by ar.idRequisito,r.descripcion")
	List<HorasRequisito> listarHorasAsignadasEnMalla(Integer idMalla);
	interface HorasRequisito {
		Integer getIdRequisito();
		String getDescripcion();
		Integer getCreditos();
		Integer getHoras();
	}




}
