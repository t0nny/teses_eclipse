package ec.edu.upse.acad.ws;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ec.edu.upse.acad.model.repository.CategoriaRepository;


@RestController
@RequestMapping("/api/categoria")
@CrossOrigin	

public class CategoriaController {
	
	@Autowired private CategoriaRepository categoriaRepository;
	//****************** SERVICIOS PARA OFERTA************************//
	
		//Buscar todos las ofertas, para listar en angular
		@RequestMapping(value="/buscarTodosAut", method=RequestMethod.GET)
		public ResponseEntity<?> buscarCategoria() {
			return ResponseEntity.ok(categoriaRepository.findAll());
		}
		
		//para probar servicio sin necesidad de Token en Reset Client
		@RequestMapping(value="/buscarTodosSinAut", method=RequestMethod.GET)
		public ResponseEntity<?> buscarTodos() {
			return ResponseEntity.ok(categoriaRepository.findAll());
		}
		
		/***
		 * retorna la lista de categoria 
		 * utilizadas en la interfaz de reporte
		 * @param Authorization
		 * @return
		 */
		@RequestMapping(value="/buscarCategoria", method=RequestMethod.GET)
		public ResponseEntity<?> listaCategoria() {
			return ResponseEntity.ok(categoriaRepository.listaCategoria());
		}
		
		

}
