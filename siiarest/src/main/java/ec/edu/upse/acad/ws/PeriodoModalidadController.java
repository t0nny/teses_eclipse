package ec.edu.upse.acad.ws;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ec.edu.upse.acad.model.pojo.PeriodoModalidad;
import ec.edu.upse.acad.model.repository.PeriodoModalidadRepository;

@RestController
@RequestMapping("/api/peridomodalidad")

@CrossOrigin
public class PeriodoModalidadController {


	@Autowired private PeriodoModalidadRepository periodomodalidadRepository;

	//****************** SERVICIOS PARA MODALIDAD************************//

	//Buscar todos los periodos modalidades, para listar en angular
	@RequestMapping(value="/buscarperiodoModalidad", method=RequestMethod.GET)
	public ResponseEntity<?> buscarperiodoModalidad() {
		return ResponseEntity.ok(periodomodalidadRepository.buscarperiodomodalidad());
	}

	//buscar por el id de la Modalidad
	@RequestMapping(value="/buscarperiodoModalidadId/{id}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarperiodoModalidadId(@PathVariable("id") Integer id) {
		return ResponseEntity.ok(periodomodalidadRepository.buscarperiodomodalidadId(id));
	}

	//Servicio de grabar las modalidades
	@RequestMapping(value="/grabarPeriodoModalidad", method=RequestMethod.POST)
	public ResponseEntity<?> grabarPeriodoModalidad(@RequestBody PeriodoModalidad permodalidad) {
		PeriodoModalidad _permodalidad = new PeriodoModalidad();
		if (permodalidad.getId() != null) {
			_permodalidad = periodomodalidadRepository.findById(permodalidad.getId()).get();
		}
		BeanUtils.copyProperties(permodalidad, _permodalidad);
		periodomodalidadRepository.save(_permodalidad);
		return ResponseEntity.ok(_permodalidad);
	}

	//Servicio de borrar los periodos de modalidades
	@RequestMapping(value="/borrar/{idPerModalidad}", method=RequestMethod.DELETE)
	public ResponseEntity<?> borrar(@PathVariable("idPerModalidad") Integer id) {
		PeriodoModalidad permodalidad = periodomodalidadRepository.findById(id).get();
		if (permodalidad !=null) {
			permodalidad.setEstado("I");
			periodomodalidadRepository.save(permodalidad);
		}
		return ResponseEntity.ok().build();
	}

	//***********************FIN SERVICIOS PARA MODALIDAD***********************	


}
