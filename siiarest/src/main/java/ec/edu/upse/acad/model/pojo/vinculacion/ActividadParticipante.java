package ec.edu.upse.acad.model.pojo.vinculacion;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema = "vin", name = "actividad_participante")

@NoArgsConstructor //un constructorsin argumentos
public class ActividadParticipante {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_actividad_participante")
	@Getter	@Setter	private Integer id;
	
	@Column(name = "id_objetivo_tarea")
	@Getter	@Setter	private Integer idObjetivoTarea;
	
	@Column(name = "id_persona")
	@Getter	@Setter	private Integer idPersona;
	
	@Column(name = "id_tipo_usuario_proyecto")
	@Getter	@Setter	private Integer idTipoUsuarioProyecto;
	
	@Column(name = "fecha_desde")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm", locale = "es_EC", timezone = "America/Guayaquil")
	//@Temporal(TemporalType.TIMESTAMP)
	@Getter	@Setter	private Timestamp fechaDesde;

	@Column(name = "fecha_hasta")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm", locale = "es_EC", timezone = "America/Guayaquil")
	//@Temporal(TemporalType.TIMESTAMP)
	@Getter	@Setter	private Timestamp fechaHasta;

	@Column(name = "horas_trabajadas")
	@Getter	@Setter	private Integer horasTrabajadas;
	
	@Getter	@Setter	private String lugar;
	
	@Getter	@Setter	private String observaciones;
	
	@Column(name = "documento_soporte_url")
	@Getter	@Setter	private String documentoSoporteUrl;
	
	@Column(name = "usuario_ingreso_id")
	@Getter	@Setter	private String usuarioIngresoId;

	@Version
	@Getter	@Setter	private Integer version;

	@Getter	@Setter	private String estado;
	
	@PrePersist
	void preInsert() {
		if (this.estado == null)
			this.estado = "A";
	}

}
