package ec.edu.upse.acad.model.repository.matricula;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ec.edu.upse.acad.model.pojo.matricula.TipoMovilidad;

public interface TipoMovilidadRepository extends JpaRepository<TipoMovilidad, Integer>{
	@Query(value="select tm.id as id, tm.codigo as codigo, tm.descripcion as descripcion, tm.estado as estado" +
			" from TipoMovilidad tm " +
			" where tm.estado='A' ")
	List<CustomObjectfiltrarTipoMovilidad> filtrarTipoMovilidad();
	interface CustomObjectfiltrarTipoMovilidad{
		Integer getId();
		String getCodigo();
		String getDescripcion();
		String getEstado();
		
	}
}
