package ec.edu.upse.acad.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ec.edu.upse.acad.model.pojo.DocenteCategoria;

public interface DocenteCategoriaRepository extends JpaRepository<DocenteCategoria, Integer>{
	@Query(value=" SELECT dc.id as idDocenteCategoria,concat(dc.tipoRelacionLaboral,', ',dc.descripcion) as  docenteCategoria "+
			"FROM DocenteCategoria as dc " ) 
	List<CustomObject> listarDocenteCategoria();	
	interface CustomObject  { 
		Integer getIdDocenteCategoria(); 
		String getDocenteCategoria();
		//String getTipoRelacionLaboral();

	}
}
