package ec.edu.upse.acad.model.repository;


import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.Periodo;

@Repository
public interface PeriodoRepository extends JpaRepository<Periodo, Integer>{
	
	@Query(value="SELECT p.id as id,p.codigo as codigo,p.descripcion as descripcion,p.fechaDesde as fecdesde,p.fechaHasta as fechasta,p.estado as estado FROM PeriodoAcademico p " + 
			"WHERE p.estado = 'A'")
	List<customObjetbuscarPeriodo> buscarPeriodo();
	interface customObjetbuscarPeriodo{
		Integer getId();
		String getCodigo();
		String getDescripcion();
		Date getfecdesde();
		Date getFecHasta();
		String getestado();
	}
	
	// busca un periodo por su Id
	@Query(value="SELECT p.id as id, p.codigo as codigo, p.descripcion as descripcion, p.fechaDesde as fecdesde,"+
			 " p.fechaHasta as fechasta,p.estado as estado, p.version as version "+
			 " FROM Periodo p "+
			 " WHERE p.id=(?1) and p.estado = 'A'")
	customObjetbuscarPeriodoId buscarPeriodoId(Integer id);
	interface customObjetbuscarPeriodoId{
		Integer getId();
		String getCodigo();
		String getDescripcion();
		Date getfecdesde();
		Date getfechasta();
		String getestado();
		Integer getversion();
	}
	
	
	@Query(value="SELECT p.id as id, p.codigo as codigo, p.descripcion as descripcion, p.fechaDesde as fechaDesde,p.fechaHasta as fechaHasta,p.estado as estado, p.version as version"
			+ " FROM Periodo p " + 
			"WHERE p.estado = 'A' order by p.codigo desc")
	List<customObjetbuscarPeriodo1> buscarPeriodo1();
	interface customObjetbuscarPeriodo1{
		Integer getId();
		String getCodigo();
		String getDescripcion();
		Date getfechaDesde();
		Date getfechaHasta();
		String getestado();
		Integer getversion();
	}
	
	@Query(value="SELECT p.id as id, p.codigo as codigo, p.descripcion as descripcion, p.fechaDesde as fechaDesde,p.fechaHasta as fechaHasta,p.estado as estado, p.version as version FROM Periodo p " + 
			"WHERE p.estado = 'A' and p.id=?1 order by p.codigo desc")
	customObjetbuscarPeriodo1 buscarPeriodoPorId(Integer id);

	

}
