package ec.edu.upse.acad.model.repository.distributivo;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.reglamento.Reglamento;
@Repository
public interface ReglamentoRepository extends JpaRepository<Reglamento, Integer>{
	@Query(value=" SELECT r.id as id,r.idTipoReglamento as idtipoteglamento, r.nombre as nombre "+
			 "FROM Reglamento r  "+
			 "INNER JOIN TipoReglamento tr on r.idTipoReglamento=tr.id "+ 
			 "where tr.codigo='RRA' and r.estado = 'A' and tr.estado='A' ")
	List<CustomObjectReglamentor> reglamentoRegimenAcad();
	interface CustomObjectReglamentor{ 
	    Integer getId();
	    Integer getidtipoteglamento();
	    String getnombre(); 
	} 
	
	
	@Query(value=" SELECT r.id as id,r.idTipoReglamento as idtipoteglamento, tr.codigo as codigoTipo, r.nombre as nombre , "+
			"rv.codigo as codigoValidacion,rv.descripcion as descripcionValidacion,rv.valor as valorValidacion "+
			 "FROM Reglamento r  "+
			 "INNER JOIN TipoReglamento tr on r.idTipoReglamento=tr.id "+ 
			 "INNER JOIN ReglamentoValidacion rv on r.id=rv.idReglamento "+
			 "where tr.codigo='RRA' and r.estado = 'A' and tr.estado='A' and rv.estado='A' and r.fechaHasta>=GETDATE()")
	List<CustomObjectReglamentoRegimen> reglamentoRegimenAcademico();
	interface CustomObjectReglamentoRegimen{ 
	    Integer getId();
	    Integer getIdtipoteglamento();
	    String getCodigoTipo();
	    String getNombre(); 
	    String getCodigoValidacion();
	    String getDescripcionValidacion();
	    Integer getValorValidacion();
	    
	} 
	@Query(value="SELECT r.id as id,r.idTipoReglamento as idtipoteglamento,tr.codigo as codigoTipo, r.nombre as nombre, "+
			"rv.codigo as codigoValidacion,rv.descripcion as descripcionValidacion,rv.valor as valorValidacion "+
			"FROM Reglamento r  "+
			"INNER JOIN TipoReglamento tr on r.idTipoReglamento=tr.id "+ 
			"INNER JOIN ReglamentoValidacion rv on r.id=rv.idReglamento "+
			" where r.id=(?1) and r.estado = 'A'  and tr.estado='A' and rv.estado='A' and tr.estado='A' ")
	List<CustomObjectReglamentoRegimen> buscarReglamento(Integer idReglamento);
	

	@Query(value=" select r.id as id,r.idTipoReglamento as idtipoteglamento, r.nombre as nombre from Reglamento r  where r.idTipoReglamento=(?1) and r.estado = 'A' ")
	List<CustomObjectJo> buscaridTipo(Integer idTipoReglamento);
	interface CustomObjectJo{ 
	    Integer getId();
	    Integer getidtipoteglamento();
	    
	    String getnombre(); 
	} 
	
	@Query(value=" select r.id as idReglamento,r.nombre as nombreReglamento,r.reglamentoNacional as reglamentoNacional," + 
			" r.fechaAprobacionIes as fechaAprobacionIes, tr.id as idTipoReglamento,tr.descripcion as tipoReglamento "+
			" from Reglamento r " + 
			" inner join TipoReglamento tr on tr.id = r.idTipoReglamento "+
			" where r.estado='A' and tr.estado='A' ")
	List<CustomObjectReglamentos> listarTodosReglamentos();

	interface CustomObjectReglamentos{ 
		Integer getIdReglamento(); 
		String getNombreReglamento(); 
		String getReglamentoNacional();
		Date getFechaAprobacionIes();
		Integer getIdTipoReglamento();
		String getTipoReglamento();
	} 
	
	@Query(value=" select tr.id as idTipoReglamento, tr.codigo as codigoTipo,tr.descripcion as descripcionTipo  from TipoReglamento tr ")
	List<CustomObjectTiposReglamento> listarTipoReglamentos();

	interface CustomObjectTiposReglamento{ 
		Integer getIdTipoReglamento();
		String getCodigoTipo();
		String getDescripcionTipo();
	} 
	
	@Query(value=" select r.id as id,r.idTipoReglamento as idTipoReglamento,r.nombre as nombre,r.reglamentoNacional as reglamentoNacional, " + 
			"	r.fechaHasta as fechaHasta,r.fechaAprobacionIes as fechaAprobacionIes,r.usuarioIngresoId as usuarioIngresoId,r.estado as estado,r.version as version " + 
			"	from Reglamento r where r.id = (?1) ")
	List<CustomObjectReglamento> recuperarReglamentoId(Integer idReglamento);

	interface CustomObjectReglamento{ 
		Integer getId();
		Integer getIdTipoReglamento();
		String getNombre();
		String getReglamentoNacional();
		Date getFechaHasta();
		Date getFechaAprobacionIes();
		String getUsuarioIngresoId();
		String getEstado();
		Integer getVersion();
	} 
	
	@Query(value=" select tip.id as idTipoOferta,tip.descripcion as descripcion,tip.descripcionCorta as codigoTipoOferta from TipoOferta tip")
	List<CustomObjectTipoOferta> listarTipoOfertas();

	interface CustomObjectTipoOferta{ 
		Integer getIdTipoOferta();
		String getDescripcion();
		String getCodigoTipoOferta();
	} 



	
	
}
