package ec.edu.upse.acad.model.repository.vinculacion;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.vinculacion.CabeceraMatriz;

@Repository
public interface CabeceraMatrizRepository extends JpaRepository<CabeceraMatriz, Integer> {
	@Transactional
	// lista el detalle de una matriz
	@Query(value = "SELECT dm.id as id,cm.id as idCabMatriz,cm.estadoMatriz as estadoMatriz,dm.idOpcionPregunta as idOpcionPregunta,"
			+ " dm.observacion as observacion,cm.fechaDesde as fechaDesde,cm.fechaHasta as fechaHasta "
			+ "from DetalleMatriz dm "
			+ "INNER JOIN CabeceraMatriz cm on dm.cabeceraMatriz.id=cm.id "
			+ "WHERE cm.idProyectoVersion=(?1) and dm.estado='A' ")
	List<objetoDetMatriz> buscarDetMatriz(Integer idProyectoVersion);

	interface objetoDetMatriz {
		Integer getId();
		Integer getIdCabMatriz();
		Integer getIdOpcionPregunta();
		String getObservacion();
		String getEstadoMatriz();
		Date getFechaDesde();
		Date getFechaHasta();
	}

}
