package ec.edu.upse.acad.model.repository;

import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.TipoCompOrganizacion;
@Repository
public interface TipoCompOrganizacionRepository 
	extends JpaRepository<TipoCompOrganizacion, Integer>{

		@Query(value="SELECT tco "
				+ "FROM TipoCompOrganizacion AS tco "
				+ "WHERE tco.descripcion LIKE ?1 "
				+ "OR tco.abreviatura LIKE ?1 ")
		List<TipoCompOrganizacion> buscarPorTipoCompOrganizacion(String descripcion);
	//listar los tipos de componente de organizacion 
		
		@Query(value="SELECT tco.id,tco.descripcion,tco.abreviatura,tco.estado " + 
				"FROM TipoCompOrganizacion AS tco " + 
				"WHERE tco.estado = 'A' ")
		List<CustomObjetcBuscarTipoCompOrganizacion> buscarTipoCompOrganizacion();
		interface CustomObjetcBuscarTipoCompOrganizacion {
			Integer getId();
			String getDescripcion();
			String getAbreviatura();
			String getEstado();
		}
	
}