package ec.edu.upse.acad.model.repository.vinculacion;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.vinculacion.ProyectoDocente;

@Repository
public interface ProyectoDocenteRepository extends JpaRepository<ProyectoDocente, Integer> {
	
	@Transactional
	@Query(value = "SELECT p.id as idPersona,(concat(p.nombres ,' ',p.apellidos))as nombres"
			+ " from ProyectoDocente pd "
			+ "INNER JOIN Docente d on pd.idDocente= d.id " 
			+ "INNER JOIN Persona p on d.idPersona=p.id "
			+ "WHERE pd.proyecto.id=(?1) and d.estado='A' AND p.estado='AC' AND pd.estado='A'")
	List<customObjetDocenteProyecto> docenteProyecto(Integer idProyecto);

	interface customObjetDocenteProyecto {
		Integer getIdPersona();
		String getNombres();		
	}
	

}
