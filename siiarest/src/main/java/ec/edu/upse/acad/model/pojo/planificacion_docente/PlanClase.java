package ec.edu.upse.acad.model.pojo.planificacion_docente;

import javax.persistence.*;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;


/**
 * The persistent class for the plan_clase database table.
 * 
 */
@Entity
@Table(schema="aca", name="plan_clase")
@Where(clause = "estado='A'")
@NoArgsConstructor
public class PlanClase  {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_plan_clase")
	@Getter @Setter private Long id;
	
	@Column(name="id_contenidos")
	@Getter @Setter private Integer idContenidos;

	@Getter @Setter private Boolean autoevaluacion;

	@Getter @Setter private Boolean coevaluacion;
	
	@Getter @Setter private Boolean heteroevaluacion;

	@Getter @Setter private String contenido;

	@Getter @Setter private String estado;

	@Column(name="fecha_clase")
	@Getter @Setter private Date fechaClase;

//	@Column(name="fecha_ingreso")
//	private Timestamp fechaIngreso;

	@Column(name="horas_aea")
	@Getter @Setter private Integer horasAea;

	@Column(name="horas_doc")
	@Getter @Setter private Integer horasDoc;

	@Column(name="horas_ta")
	@Getter @Setter private Integer horasTa;

	@Column(name="recursos_didacticos")
	@Getter @Setter private String recursosDidacticos;

	@Column(name="resultado_aprendizaje")
	@Getter @Setter private String resultadoAprendizaje;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;

	//RELACIONES
	//bi-directional many-to-one association to PlanBibliografia
	@OneToMany(mappedBy="planClase", cascade=CascadeType.ALL)
	@Getter @Setter private List<PlanBibliografia> planBibliografias;

	//bi-directional many-to-one association to Contenido
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_contenidos", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Contenido contenidol;

	//bi-directional many-to-one association to PlanEstrategia
	@OneToMany(mappedBy="planClase", cascade=CascadeType.ALL)
	@Getter @Setter private List<PlanEstrategia> planEstrategias;

	//bi-directional many-to-one association to PlanMetodologia
	@OneToMany(mappedBy="planClase", cascade=CascadeType.ALL)
	@Getter @Setter private List<PlanMetodologia> planMetodologias;

	@PrePersist
	void preInsert() {
	   if (this.estado == null)
	       this.estado = "A";
	}
}