package ec.edu.upse.acad.model.pojo.matricula;


import javax.persistence.*;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ec.edu.upse.acad.model.pojo.reglamento.Reglamento;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;



@Entity
@Table(schema="aca", name="validacion_general")
@Where(clause = "estado='A'")
@NoArgsConstructor
public class ValidacionGeneral{

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_validacion_general")
	@Getter @Setter private Integer id;
	
	@Column(name="id_reglamento")
	@Getter @Setter private Integer idReglamento;

	@Getter @Setter private String estado;

//	@Column(name="fecha_ingreso")
//	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="num_dias_anular_asig")
	@Getter @Setter private Integer numDiasAnularAsig;

	@Column(name="horas_max_semana_docencia")
	@Getter @Setter private Integer horasMaxSemanaDocencia;

	@Column(name="horas_max_semana_componentes")
	@Getter @Setter private Integer horasMaxSemanaComponentes;

	@Column(name="num_max_nivel_tomados")
	@Getter @Setter private Integer numMaxNivelTomados;
	
	@Column(name="num_max_creditos")
	@Getter @Setter private Integer numMaxCreditos;
	
	@Column(name="calificacion_min_aprobar")
	@Getter @Setter private Integer calificacionMinAprobar;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private int version;

	//RELACIONES
	//bi-directional many-to-one association to Reglamento
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_reglamento" , insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Reglamento reglamento;

	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }
	
}