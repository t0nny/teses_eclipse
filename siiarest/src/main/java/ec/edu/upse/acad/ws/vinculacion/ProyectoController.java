package ec.edu.upse.acad.ws.vinculacion;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.upse.acad.model.pojo.vinculacion.Proyecto;
import ec.edu.upse.acad.model.repository.vinculacion.ProyectoRepository;
import ec.edu.upse.acad.model.repository.vinculacion.ProyectoVersionRepository;
import ec.edu.upse.acad.model.service.SecurityService;
import ec.edu.upse.acad.model.service.vinculacion.ProyectoService;

@RestController
@RequestMapping("/api/proyecto")
@CrossOrigin
public class ProyectoController {
	@Autowired
	private SecurityService securityService;
	@Autowired
	private ProyectoRepository proyectoRepository;
	@Autowired
	private ProyectoVersionRepository proyectoVersionRepository;
	@Autowired
	private ProyectoService proyectoService;
	static Logger logger = Logger.getLogger(ProyectoController.class);

	// busca todos activos e inactivos
	@RequestMapping(value = "/buscarTodosProyecto", method = RequestMethod.GET)
	public ResponseEntity<?> buscarTodosProyecto(@RequestHeader(value = "Authorization") String authorization) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(proyectoRepository.findAll());
	}

	// buscar solo activos
	@RequestMapping(value = "/buscarListaProyecto", method = RequestMethod.GET)
	public ResponseEntity<?> buscarListaProyecto(@RequestHeader(value = "Authorization") String authorization) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(proyectoRepository.buscarListaProyecto());
	}

	// buscar solo Proyectos
	@RequestMapping(value = "/buscarSoloProyectos", method = RequestMethod.GET)
	public ResponseEntity<?> buscarSoloProyectos(@RequestHeader(value = "Authorization") String authorization) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(proyectoRepository.buscarSoloProyectos());
	}

	// buscar por el id de proyecto
	@RequestMapping(value = "/buscarProyectoPorId/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarProyectoPorId(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable("id") Integer id) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(proyectoRepository.findById(id).get());
	}

	// buscar por el id de proyectoVersion
	@RequestMapping(value = "/buscarProyectoVersionPorId/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarProyectoVersionPorId(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable("id") Integer id) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(proyectoVersionRepository.findById(id).get());
	}
	
	// buscar codigo del proyecto
		@RequestMapping(value = "/buscarCodigoProyecto/{idProyectoVersion}", method = RequestMethod.GET)
		public ResponseEntity<?> buscarCodigoProyecto(@RequestHeader(value = "Authorization") String authorization,
				@PathVariable("idProyectoVersion") Integer idProyectoVersion) {
			if (!securityService.isTokenValido(authorization)) {
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
			}
			return ResponseEntity.ok(proyectoRepository.buscarCodigoProyecto(idProyectoVersion).get());
		}

	@RequestMapping(value = "/grabarProyectoVersion", method = RequestMethod.POST)
	public ResponseEntity<?> grabarProyectoVersion(@RequestHeader(value = "Authorization") String Authorization, // definimos
			// que tenga
			// autorizacion
			@RequestBody Proyecto proyecto) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		try {
			// System.out.println(proyecto.toString());
			proyectoService.nuevoProyectoVersion(proyecto);

		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(" ya" + e.getMessage() + "este " + e.getCause());
			extractStackTrace(e, logger);
			// return ResponseEntity.status((HttpStatus.NOT_FOUND)).body(null);
		}
		return ResponseEntity.ok().build();
	}

	// buscar por el id de proyecto
	@RequestMapping(value = "/buscarVersionProyecto/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarVersionProyecto(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable("id") Integer id) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(proyectoRepository.buscarVersionProyecto(id));
	}

	// Retorna la lista de las asignaturas para llenar el combo de asignaturasProyectos
	@RequestMapping(value = "/buscarAsignaturasProyectos/{id}/{idSemestre}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarAsignaturasProyectos(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable("id") Integer id, @PathVariable("idSemestre") Integer idSemestre) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(proyectoRepository.buscarAsignaturasProyectos(id, idSemestre));
	}

	
	// Retorna la lista de las asignaturas para llenar el grid de asignaturasProyectos
	@RequestMapping(value = "/buscarAsignaturasProyectosGrid/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarAsignaturasProyectosGrid(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable("id") Integer id) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(proyectoRepository.buscarAsignaturasProyectosGrid(id));
	}
	
	// buscar por el id de proyecto
	@RequestMapping(value = "/buscarDocentesPorIdDepOferta/{id}/{idDepOferta}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarDocentesPorIdDepOferta(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable("id") Integer id, @PathVariable("idDepOferta") Integer idDepOferta) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(proyectoRepository.buscarDocentesPorIdDepOferta(id, idDepOferta));
	}

	// buscar director por id docente
	@RequestMapping(value = "/buscarDirectorProyecto/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarDirectorProyecto(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable("id") Integer id) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(proyectoRepository.directorProyecto(id));
	}

	// buscar director por id docente
	@RequestMapping(value = "/buscarEstudianteProyecto/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarEstudianteProyecto(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable("id") Integer id) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(proyectoRepository.estudianteProyecto(id));
	}

	// buscar director por id docente
	@RequestMapping(value = "/directorProyectoTitulos/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> directorProyectoTitulos(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable("id") Integer id) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(proyectoRepository.directorProyectoTitulos(id));
	}

	@RequestMapping(value = "/listarEstudianteOferta/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> listarEstudianteOferta(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable("id") Integer id) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(proyectoRepository.ListarEstudianteOferta(id));
	}

	@RequestMapping(value = "/estudianteSemestre/{idPerAcademico}/{idDepOferta}/{idSemestre}/{idParalelo}/{idEstudiante}", method = RequestMethod.GET)
	public ResponseEntity<?> estudianteSemestre(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable("idPerAcademico") Integer idPerAcademico, @PathVariable("idDepOferta") Integer idDepOferta,
			@PathVariable("idSemestre") Integer idSemestre, @PathVariable("idParalelo") Integer idParalelo,
			@PathVariable("idEstudiante") Integer idEstudiante) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(proyectoRepository.estudiantesSemestre(idPerAcademico, idDepOferta, idSemestre, idParalelo,idEstudiante));
	}

	// buscar proyecto version y matriz ex ante
	@RequestMapping(value = "/proyectoVersionMatriz/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> proyectoVersionMatriz(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable("id") Integer id) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(proyectoRepository.proyectoVersionMatriz(id));
	}

	// buscar proyecto version y matriz ex ante
	@RequestMapping(value = "/proyectoCabMatriz/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> proyectoCabMatriz(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable("id") Integer id) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(proyectoRepository.proyectoCabMatriz(id));
	}

	// buscar titulos acaemicos director
	@RequestMapping(value = "/buscarTitulosDirector/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarTitulosDirector(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable("id") Integer id) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(proyectoRepository.tituloDirector(id));
	}

	// buscar titulos acaemicos director
	@RequestMapping(value = "/buscarIdDocente/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarIdDocente(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable("id") Integer id) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(proyectoRepository.buscarIdDocente(id).get());
	}

	// pasar de estado inicial a estado revisar en proyecto
	@RequestMapping(value = "/updateEstadoProyecto/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> updateEstadoProyecto(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable("id") Integer id) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		try {
			proyectoService.updateEstadoProyecto(id);
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(" ya" + e.getMessage() + "este " + e.getCause());
			extractStackTrace(e, logger);
		}
		return ResponseEntity.ok().build();
	}
	
	@RequestMapping(value = "/updateCodigoProyecto", method = RequestMethod.POST)
	public ResponseEntity<?> updateCodigoProyecto(@RequestHeader(value = "Authorization") String authorization,
			@RequestBody Proyecto proyecto) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		try {
			System.out.println("estado2"+ proyecto.getEstadoProyecto());
			proyectoService.updateCodigoProyecto(proyecto);
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(" ya" + e.getMessage() + "este " + e.getCause());
			extractStackTrace(e, logger);
		}
		return ResponseEntity.ok().build();
	}

	@RequestMapping(value = "/grabarProyecto", method = RequestMethod.POST)
	public ResponseEntity<?> grabarProyecto(@RequestHeader(value = "Authorization") String Authorization, // definimos
																											// que tenga
																											// autorizacion
			@RequestBody Proyecto proyecto) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		try {
			// System.out.println(proyecto.toString());
			proyectoService.grabarProyecto(proyecto);

		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(" ya" + e.getMessage() + "este " + e.getCause());
			extractStackTrace(e, logger);
			// return ResponseEntity.status((HttpStatus.NOT_FOUND)).body(null);
		}

		return ResponseEntity.ok().build();
	}

	@RequestMapping(value = "/borrar/{idProyecto}", method = RequestMethod.DELETE)
	public ResponseEntity<?> borrar(@RequestHeader(value = "Authorization") String Authorization,
			@PathVariable("idProyecto") Integer id) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		Proyecto proyecto = proyectoRepository.findById(id).get();
		if (proyecto != null) {
			proyectoService.borrarProyecto(proyecto);
			return ResponseEntity.ok().build();
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@RequestMapping(value = "/fnListarPreguntaInstrumentoVin/{idInstrumento}", method = RequestMethod.GET)
	public ResponseEntity<?> fnListarPreguntaInstrumento(@RequestHeader(value = "Authorization") String Authorization,
			@PathVariable("idInstrumento") Integer idInstrumento) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(proyectoRepository.getFnListarPreguntaInstrumentoVin(idInstrumento));
	}

	public static void extractStackTrace(Exception exception, Logger _logger) {

		StackTraceElement[] elements = exception.getStackTrace();

		String log = null;

		for (StackTraceElement element : elements) {

			log = (log == null) ? element.toString() : log + ";" + element.toString();

		}

		_logger.error(log);

	}
}
