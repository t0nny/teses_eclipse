package ec.edu.upse.acad.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.ActualizacionProfesional;

@Repository
public interface ActualizacionProfesionalRepository extends JpaRepository<ActualizacionProfesional,Integer>{

}
