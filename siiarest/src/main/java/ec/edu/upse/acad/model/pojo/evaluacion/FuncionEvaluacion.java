package ec.edu.upse.acad.model.pojo.evaluacion;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ec.edu.upse.acad.model.pojo.TipoOferta;
import ec.edu.upse.acad.model.pojo.distributivo.DocenteDedicacion;
import ec.edu.upse.acad.model.pojo.reglamento.ActividadPersonalDocente;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="eva", name="funcion_evaluacion")
@NoArgsConstructor
public class FuncionEvaluacion {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_funcion_evaluacion")
	@Getter @Setter private Integer id;
	
//	@Column(name="id_docente_dedicacion")
//	@Getter @Setter private Integer idDocenteDedicacion;

//	@Column(name="id_actividad_personal")
//	@Getter @Setter private Integer idActividadPersonal;

//	@Column(name="id_tipo_oferta")
//	@Getter @Setter private Integer idTipoOferta;
	
	@Column(name="codigo")
	@Getter @Setter private String codigo;
	
	@Column(name="descripcion")
	@Getter @Setter private String descripcion;
	
	@Column(name="menor_a")
	@Getter @Setter private Integer menorA;
	
	@Column(name="mayor_igual_a")
	@Getter @Setter private Integer mayorIgualA;

	@Getter @Setter private String estado;
	
//	@Column(name="fecha_ingreso")
//	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;
	
	@Version
	@Getter @Setter private Integer version;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_actividad_personal", nullable = false, insertable=false, updatable = false)
	//@JsonIgnore
	@Getter @Setter private ActividadPersonalDocente actividadPersonal;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_docente_dedicacion", nullable = false, insertable=false, updatable = false)
	//@JsonIgnore
	@Getter @Setter private DocenteDedicacion docenteDedicacion;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_tipo_oferta", nullable = false, insertable=false, updatable = false)
	//@JsonIgnore
	@Getter @Setter private TipoOferta tipoOferta;

	@OneToMany(mappedBy="funcionEvaluacion", cascade=CascadeType.ALL)
	@Getter @Setter private List<TipoEvaluacion> tipoEvaluacion;
	
	@OneToMany(mappedBy="funcionEvaluacion", cascade=CascadeType.ALL)
	@Getter @Setter private List<PersonaCargoFuncion> personaCargoFuncion;

	@PrePersist
	void preInsert() {
		if (this.estado == null)
			this.estado = "A";
	}

}
