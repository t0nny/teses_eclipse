package ec.edu.upse.acad.ws.eva;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.upse.acad.model.pojo.evaluacion.PeriodoEvaluacion;
import ec.edu.upse.acad.model.repository.evaluacion.PeriodoEvaluacionRepository;
import ec.edu.upse.acad.model.service.eva.PeriodoEvaluacionService;

@RestController
@RequestMapping("/api/periodoEvaluacion")
@CrossOrigin
public class PeriodoEvaluacionController {
	@Autowired private PeriodoEvaluacionRepository periodoEvaluacionRepository;
	@Autowired private PeriodoEvaluacionService periodoEvaluacionService;
	//@Autowired private SpEvaluacionDocenteRepository spEvaluacionDocenteRepository;

	//@Autowired private  spEvaluacionDocenteRepository;
	
	@RequestMapping(value="/listarSemestreUsuarioEstudiante/{usuario}", method=RequestMethod.GET)
	public ResponseEntity<?> listarSemestreUsuarioEstudiante(@RequestHeader(value="Authorization") String authorization, 
			@PathVariable("usuario") String usuario) {		
		System.out.println("usu : "+usuario);
		//return ResponseEntity.ok(spEvaluacionDocenteRepository.getSPEvaluacionDocente(usuario));
		return ResponseEntity.ok(periodoEvaluacionRepository.listarSemestrePorUsuarioEstudiante(usuario));
		
	}	
	
	@RequestMapping(value="/listarMateriaDocentePorEstudiante/{idEstudiante}/{idSemestre}", method=RequestMethod.GET)
	public ResponseEntity<?> listarMateriaDocentePorEstudiante(@RequestHeader(value="Authorization") String authorization, 
		
			@PathVariable("idEstudiante") Integer idEstudiante, @PathVariable("idSemestre") Integer idSemestre) {		 
		return ResponseEntity.ok(periodoEvaluacionRepository.listarMateriaDocentePorEstudiante(idEstudiante, idSemestre));
	}
	
	
	//--------------------------------------------------------------------//
	//			  SERVICIOS PARA MANTENEDOR PERÍODO EVALUACIÓN	 		  //
	//--------------------------------------------------------------------//
	
	/**
	 * Método que me permite obtener todos los Períodos de Evaluación
	 * @return Objeto con resultado esperado.
	 */
	@RequestMapping(value = "/getAllPeriodoEvaluacion", method = RequestMethod.GET)
	public ResponseEntity<?> getAllPeriodoEvaluacion(){
		return ResponseEntity.ok(periodoEvaluacionRepository.getAllPeriodoEvaluacion());
	}
	
//	@RequestMapping(value = "/findOnPeriodoCompFuncion/{idPeriodoEvaluacion}", method = RequestMethod.GET)
//	public ResponseEntity<?> findOnPeriodoCompFuncion(@PathVariable("idPeriodoEvaluacion") Integer idPeriodoEvaluacion){
//		return ResponseEntity.ok(periodoEvaluacionRepository.findOnPeriodoCompFuncion(idPeriodoEvaluacion));
//	}
//	
	/**
	 * Método que me permite obtener un único elemento con los identificadores dados.
	 * @param idPeriodoEvaluacion Identificador Período Evaluación.
	 * @param idEvaluacionDocenteGeneral Identificador Evaluación Docente General.
	 * @return Objeto con resultado esperado.
	 */
	@RequestMapping(value = "/getPeriodoEvaluacionById/{idPeriodoEvaluacion}/{idEvaluacionDocenteGeneral}")
	public ResponseEntity<?> getPeriodoEvaluacionById(
			@PathVariable("idPeriodoEvaluacion") Integer idPeriodoEvaluacion,
			@PathVariable("idEvaluacionDocenteGeneral") Integer idEvaluacionDocenteGeneral){
		return ResponseEntity.ok(periodoEvaluacionRepository.getPeriodoEvaluacionById(idPeriodoEvaluacion,idEvaluacionDocenteGeneral));
	}
	 
	/**
	 * 
	 * @param periodoEvaluacion
	 * @return
	 */
	@RequestMapping(value = "/savePeriodoEvaluacion", method = RequestMethod.POST)
	public ResponseEntity<?> savePeriodoEvaluacion(@RequestBody PeriodoEvaluacion periodoEvaluacion){
		periodoEvaluacionService.savePeriodoEvaluacion(periodoEvaluacion);
		return ResponseEntity.ok(periodoEvaluacion);
	}
	
	@RequestMapping(value = "/deletePeriodoEvaluacion/{idPeriodoEval}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deletePeriodoEvaluacion(@PathVariable("idPeriodoEval") Integer idPeriodoEval){
		periodoEvaluacionService.deletePeriodoEvaluacion(idPeriodoEval);
		return ResponseEntity.ok().build();
	}
	
}
