package ec.edu.upse.acad.ws.vinculacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.upse.acad.model.pojo.vinculacion.CabeceraMatriz;
import ec.edu.upse.acad.model.repository.vinculacion.CabeceraMatrizRepository;
import ec.edu.upse.acad.model.service.SecurityService;
import ec.edu.upse.acad.model.service.vinculacion.CabeceraMatrizService;

@RestController
@RequestMapping("/api/cabeceraMatriz")
@CrossOrigin
public class CabeceraMatrizController {
	@Autowired
	private SecurityService securityService;
	@Autowired
	private CabeceraMatrizRepository cabeceraMatrizRepository;
	@Autowired
	private CabeceraMatrizService cabeceraMatrizService;
	
	@RequestMapping(value = "/buscarCabMatriz/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarCabMatriz(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable("id") Integer id) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(cabeceraMatrizRepository.findById(id).get());
	}
	
	@RequestMapping(value = "/buscarDetMatriz/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarDetMatriz(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable("id") Integer id) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(cabeceraMatrizRepository.buscarDetMatriz(id));
	}
	
	@RequestMapping(value = "/grabarCabMatriz", method = RequestMethod.POST)
	public ResponseEntity<?> grabarCabMatriz(@RequestHeader(value = "Authorization") String Authorization, // definimos
																											// que tenga
																											// autorizacion
			@RequestBody CabeceraMatriz cabeceraMatriz) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
			// System.out.println(proyecto.toString());
			
		cabeceraMatrizService.grabarCabeceraMatriz(cabeceraMatriz);
		

		return ResponseEntity.ok().build();
	}
	
	@RequestMapping(value = "/updateEstadoMatriz", method = RequestMethod.POST)
	public ResponseEntity<?> updateEstadoMatriz(@RequestHeader(value = "Authorization") String authorization,
			@RequestBody CabeceraMatriz cabeceraMatriz) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		try {
			cabeceraMatrizService.updateEstadoMatriz(cabeceraMatriz);
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(" ya" + e.getMessage() + "este " + e.getCause());
			
		}
		return ResponseEntity.ok().build();
	}

}
