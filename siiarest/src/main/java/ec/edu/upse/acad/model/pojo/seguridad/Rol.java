package ec.edu.upse.acad.model.pojo.seguridad;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

//import org.eclipse.persistence.annotations.AdditionalCriteria;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="seg", name="roles")
@NoArgsConstructor
//@AdditionalCriteria("this.estado='AC'")

public class Rol {
	
	@Id

	@GeneratedValue(strategy=GenerationType.IDENTITY)
	
	@Getter @Setter private Integer id;

	@Getter @Setter private Integer nivel;

	@Getter @Setter private String nombre;
	
	@Getter @Setter private String codigo;
	
	@Getter @Setter private String descripcion;

	@Getter @Setter private String estado;
	
	@Getter @Setter private Integer padre_id;
	
	@Getter @Setter private String img_out;
	
	@Getter @Setter private String ver_otros;
	
	@Getter @Setter private String publico;
	
	@Version
	@Getter @Setter private Integer version;
	
	//Relacion uno a uno tabla rol con tabla rolusuario
	@OneToMany(mappedBy="rol", cascade=CascadeType.ALL)
	@JsonIgnore
	@Getter @Setter private List<ModuloRol> moduloRol;
	
	@OneToMany(mappedBy="rol", cascade=CascadeType.ALL)
	//@JsonIgnore
	@Getter @Setter private List<RolDepartamento> rolDepartamento;
	
//	@OneToMany(mappedBy="rolesUsu", cascade=CascadeType.ALL)
//	@Getter @Setter private List<RolUsuario> listaRoles;

	@PrePersist
	void preInsert() {
	   if (this.estado == null)
	       this.estado = "AC";
	}
}
