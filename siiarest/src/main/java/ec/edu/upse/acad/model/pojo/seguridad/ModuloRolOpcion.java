package ec.edu.upse.acad.model.pojo.seguridad;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;


import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="seg", name="modulos_roles_opciones")
@NoArgsConstructor

public class ModuloRolOpcion {
	     
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Getter @Setter private Integer id;
	     
	
	@ManyToOne
	@JoinColumn(name="modulo_rol_id", insertable=false, updatable = false)
	@Getter @Setter private ModuloRol moduloRol;
	     
	@ManyToOne
	@JoinColumn(name="opcion_id", insertable=false, updatable = false)
	@Getter @Setter private Opcion opcion;
	
	
	    
	@Getter @Setter private String estado;

	@Version
	@Getter @Setter private Integer version;
	
	@PrePersist
	void preInsert() {
	   if (this.estado == null)
	       this.estado = "AC";
	}	
}
