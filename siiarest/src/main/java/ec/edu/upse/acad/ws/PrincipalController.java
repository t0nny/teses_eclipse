package ec.edu.upse.acad.ws;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.upse.acad.model.pojo.seguridad.Modulo;
import ec.edu.upse.acad.model.repository.PrincipalRepository;


@RestController
@RequestMapping("/api/roles")
@CrossOrigin

public class PrincipalController {

	@Autowired private PrincipalRepository principalRepository;

	//****************** SERVICIOS PARA MODULOS************************//

	//Buscar todos los modulos, para listar en angular
	@RequestMapping(value="/buscarModulos", method=RequestMethod.GET)
	public ResponseEntity<?> buscarModulos() {
		return ResponseEntity.ok(principalRepository.findAll());
	}


	//servicio que actualiza o crea un nuevo modulo	
	@RequestMapping(value="/buscarModulo/{id}", method=RequestMethod.GET)
	public ResponseEntity<?> buscar(@PathVariable("id") Integer id) {
		Modulo usuario;
		if (principalRepository.findById(id).isPresent()) {
			usuario = principalRepository.findById(id).get();
			return ResponseEntity.ok(usuario);
		}else {
			return ResponseEntity.notFound().build();
		}
	}

	@RequestMapping(value="/buscarPorNombre/{nombre}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarPorNombre(@PathVariable("nombre") String nombre) {
		return ResponseEntity.ok(principalRepository.buscarPorNombre(nombre));
	}


}
