package ec.edu.upse.acad.ws.vinculacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.upse.acad.model.pojo.vinculacion.ProyectoConvocatoria;
import ec.edu.upse.acad.model.repository.vinculacion.ProyectoConvocatoriaRepository;
import ec.edu.upse.acad.model.service.SecurityService;
import ec.edu.upse.acad.model.service.vinculacion.ProyectoConvocatoriaService;

@RestController
@RequestMapping("/api/ConvocatoriaProyecto")

@CrossOrigin
public class ProyectoConvocatoriaController {
	@Autowired
	private SecurityService securityService;
	@Autowired
	private ProyectoConvocatoriaRepository periodoConvocatoriaRepository;
	@Autowired
	private ProyectoConvocatoriaService convocatoriaService;

	// ****************** SERVICIOS PARA PERIODO
	// CONVOCATORIAS************************//

	// Busca todas convocatorias
	@RequestMapping(value = "/buscarPeriodoConvocatoria", method = RequestMethod.GET)
	public ResponseEntity<?> buscarPeriodoConvocatoria(@RequestHeader(value = "Authorization") String authorization) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(periodoConvocatoriaRepository.buscarPeriodoConvocatoria());
	}
	
	// Busca todas convocatorias
		@RequestMapping(value = "/buscarFechaFinConvocatoria", method = RequestMethod.GET)
		public ResponseEntity<?> buscarFechaFinConvocatoria(@RequestHeader(value = "Authorization") String authorization) {
			if (!securityService.isTokenValido(authorization)) {
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
			}
			return ResponseEntity.ok(periodoConvocatoriaRepository.buscarFechaFinConvocatoria().get());
		}

	// buscar por el id de periodoConvocatoria
	@RequestMapping(value = "/buscarPorPeriodoConvocatoriaId/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarPorPeriodoConvocatoriaId(
			@RequestHeader(value = "Authorization") String authorization, @PathVariable("id") Integer id) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(periodoConvocatoriaRepository.findById(id).get());
	}

	/*// buscar convocatoria por descripcion
	@RequestMapping(value = "/buscarPorDescripcion/{descripcion}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarPorDescripcion(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable("descripcion") String descripcion) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(periodoConvocatoriaRepository.buscarPorDescripcion("%" + descripcion + "%"));
	}*/

	// buscar ultima convocatoria 
	@RequestMapping(value = "/buscarUltimaConvocatoria", method = RequestMethod.GET)
	public ResponseEntity<?> buscarUltimaConvocatoria(@RequestHeader(value = "Authorization") String authorization) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(periodoConvocatoriaRepository.buscarUltimaConvocatoria().get());
	}

	// Servicio de grabar las convocatorias
	@RequestMapping(value = "/grabarPeriodoConvocatoria", method = RequestMethod.POST)
	public ResponseEntity<?> grabarPeriodoConvocatoria(@RequestHeader(value = "Authorization") String Authorization,
			@RequestBody ProyectoConvocatoria perConvocatoria) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		convocatoriaService.savePeriodoConvocatoria(perConvocatoria);
		return ResponseEntity.ok().build();
	}

	// Servicio de borrar las Convocatorias
	@RequestMapping(value = "/borrar/{idPerConvocatoria}", method = RequestMethod.DELETE)
	public ResponseEntity<?> borrar(@RequestHeader(value = "Authorization") String Authorization,
			@PathVariable("idPerConvocatoria") Integer id) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		ProyectoConvocatoria perConvocatoria = periodoConvocatoriaRepository.findById(id).get();
		if (perConvocatoria != null) {
			// perConvocatoria.setEstado("I");
			// periodoConvocatoriaRepository.save(perConvocatoria);
			periodoConvocatoriaRepository.deleteById(id);
		}

		return ResponseEntity.ok().build();
	}

	// lista la facultad de donde pertenece el usuario logeado
	@RequestMapping(value = "/cargarFacultad/{idperiodoAcademico}/{cedula}", method = RequestMethod.GET)
	public ResponseEntity<?> cargarFacultad(@RequestHeader(value = "Authorization") String Authorization,
			@PathVariable("idperiodoAcademico") Integer idperiodoAcademico, @PathVariable("cedula") String cedula) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}

		return ResponseEntity.ok(periodoConvocatoriaRepository.facultad(idperiodoAcademico, cedula).get());
	}

	// tipo usuario
	@RequestMapping(value = "/cargarTipoUsuario/{idUsuario}", method = RequestMethod.GET)
	public ResponseEntity<?> cargarTipoUsuario(@RequestHeader(value = "Authorization") String Authorization,
			@PathVariable("idUsuario") Integer idUsuario) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}

		return ResponseEntity.ok(periodoConvocatoriaRepository.cargarTipoUsuario(idUsuario));
	}
	
	// lista las carreras de una facultad
	@RequestMapping(value = "/cargarCarreras/{idperiodoAcademico}/{idFacultad}", method = RequestMethod.GET)
	public ResponseEntity<?> cargarCarreras(@RequestHeader(value = "Authorization") String Authorization,
			@PathVariable("idperiodoAcademico") Integer idperiodoAcademico,
			@PathVariable("idFacultad") Integer idFacultad) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}

		return ResponseEntity.ok(periodoConvocatoriaRepository.cargarCarreras(idperiodoAcademico, idFacultad));
	}

}
