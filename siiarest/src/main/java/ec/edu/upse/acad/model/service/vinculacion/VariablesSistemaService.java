package ec.edu.upse.acad.model.service.vinculacion;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.upse.acad.model.pojo.seguridad.Persona;
import ec.edu.upse.acad.model.pojo.vinculacion.ProyectoConvocatoria;
import ec.edu.upse.acad.model.repository.PersonasRepository;

@Service
@Transactional
public class VariablesSistemaService {
	@PersistenceContext	private EntityManager em;
	@Autowired private PersonasRepository personasRepository;
	
	public void buscarIdDocente(String cedula) {
		Persona _persona;
		Integer id=null;
		_persona = personasRepository.findByIdentificacion(cedula);
		 id = _persona.getId();
		em.getEntityManagerFactory().getCache().evict(ProyectoConvocatoria.class);
	}

}
