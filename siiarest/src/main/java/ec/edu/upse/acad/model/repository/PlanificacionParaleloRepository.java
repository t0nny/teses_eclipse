package ec.edu.upse.acad.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;



import ec.edu.upse.acad.model.pojo.PlanificacionParalelo;

public interface PlanificacionParaleloRepository extends JpaRepository<PlanificacionParalelo, Integer> {

	


	@Query(value="SELECT pm.idPeriodoAcademico as idPeriodoAcademico, m1.id as idMalla,  count(ma1.id) as cantAsigMalla , "+
					" (select count (ma.id) "+
					" FROM PeriodoAcademico pa "+
					" INNER JOIN PlanificacionParalelo pp on pa.id=pp.idPeriodoAcademico "+
					" INNER JOIN MallaAsignatura ma on pp.idMallaAsignatura=ma.id "+
					" INNER JOIN Malla m on ma.idMalla=m.id "+
					" INNER JOIN Nivel n on ma.idNivel=n.id "+
					" WHERE pa.id=(?1) and m.idDepartamentoOferta=(?2) and pp.numParalelos>0 "+
					" and pa.estado='A' and pp.estado='A' and ma.estado='A' and m.estado in ('A','P')  and n.estado='A'" +
					" and ma.idNivel >=m1.idNivelMinAperturado "+ //( SELECT m1.idNivelMinAperturado  " + 
				//	"		  FROM  DepartamentoOferta do1   " + 
					//"		  INNER JOIN Malla m1 on do1.id=m1.idDepartamentoOferta  " + 
					//"		  INNER JOIN PeriodoMalla pm1 on m1.id=pm1.idMalla  " + 
					//"		  WHERE  pm1.idPeriodoAcademico=?1 and do1.id=?2 and do1.estado='A' " + 
					//"		  and m1.estado in ('A', 'P') and pm1.estado='A')    " + 
					"and ma.idNivel <=  m1.idNivelMaxAperturado "+//( SELECT m1.idNivelMaxAperturado " + 
				//	"		  FROM  DepartamentoOferta do1   " + 
				//	"		  INNER JOIN Malla m1 on do1.id=m1.idDepartamentoOferta  " + 
				//	"		  INNER JOIN PeriodoMalla pm1 on m1.id=pm1.idMalla  " + 
				//	"		  WHERE  pm1.idPeriodoAcademico=?1 and do1.id=?2 and do1.estado='A' " + 
				//	"		  and m1.estado in ('A', 'P') and pm1.estado='A')"+
					" GROUP BY pa.id, m.id )  as cantAsigConParalelo, "+	
					" (select count (ma.id) "+
					" FROM PeriodoAcademico pa "+
					" INNER JOIN PlanificacionParalelo pp on pa.id=pp.idPeriodoAcademico "+
					" INNER JOIN MallaAsignatura ma on pp.idMallaAsignatura=ma.id "+
					" INNER JOIN Malla m on ma.idMalla=m.id "+
					" INNER JOIN Nivel n on ma.idNivel=n.id "+
					" WHERE pa.id=(?1) and m.idDepartamentoOferta=(?2) and pp.numParalelos>0  "+
					" and pa.estado='A' and pp.estado='A' and ma.estado='A' and m.estado in ('A','P')  and n.estado='A'" +
					" and ma.id in ( select ma2.id from Malla m2 "+
									 "inner join MallaAsignatura ma2 on m2.id=ma2.idMalla "+
									 "inner join AsignaturaAprendizaje aa on ma2.id=aa.idMallaAsignatura "+
									  " where ma2.estado in ('A', 'P') and ma2.estado='A' and  m2.id=m.id and aa.estado='A' "+
									  "and aa.valor>0 and  ma2.idNivel >=m1.idNivelMinAperturado "+ //( SELECT m1.idNivelMinAperturado  " + 
									//	"		  FROM  DepartamentoOferta do1   " + 
									//	"		  INNER JOIN Malla m1 on do1.id=m1.idDepartamentoOferta  " + 
									//	"		  INNER JOIN PeriodoMalla pm1 on m1.id=pm1.idMalla  " + 
									//	"		  WHERE  pm1.idPeriodoAcademico=?1 and do1.id=?2 and do1.estado='A' " + 
									//	"		  and m1.estado in ('A', 'P') and pm1.estado='A')    " + 
										"and ma2.idNivel <=m1.idNivelMaxAperturado "+//( SELECT m1.idNivelMaxAperturado " + 
									//	"		  FROM  DepartamentoOferta do1   " + 
									//	"		  INNER JOIN Malla m1 on do1.id=m1.idDepartamentoOferta  " + 
									//	"		  INNER JOIN PeriodoMalla pm1 on m1.id=pm1.idMalla  " + 
									//	"		  WHERE  pm1.idPeriodoAcademico=?1 and do1.id=?2 and do1.estado='A' " + 
									//	"		  and m1.estado in ('A', 'P') and pm1.estado='A')"+
									 " group by ma2.id)"+
					
		
					" GROUP BY pa.id, m.id )  as cantAsigConAsigApren "+	
				" FROM PeriodoMalla pm "+
				" INNER JOIN Malla m1 on pm.idMalla=m1.id "+
				" INNER JOIN MallaAsignatura ma1 on m1.id=ma1.idMalla "+
				" INNER JOIN Nivel n1 on ma1.idNivel=n1.id "+
				" WHERE pm.idPeriodoAcademico=(?1) and m1.idDepartamentoOferta=(?2)  "+
				" and pm.estado='A' and m1.estado in ('A','P') and  ma1.estado='A' and n1.estado='A' "+
				" and ma1.idNivel >=m1.idNivelMinAperturado "+ //( SELECT m1.idNivelMinAperturado  " + 
			//	"		  FROM  DepartamentoOferta do1   " + 
			//	"		  INNER JOIN Malla m1 on do1.id=m1.idDepartamentoOferta  " + 
			//	"		  INNER JOIN PeriodoMalla pm1 on m1.id=pm1.idMalla  " + 
			//	"		  WHERE  pm1.idPeriodoAcademico=?1 and do1.id=?2 and do1.estado='A' " + 
			//	"		  and m1.estado in ('A', 'P') and pm1.estado='A')    " + 
				" and ma1.idNivel <=m1.idNivelMaxAperturado " +//( SELECT m1.idNivelMaxAperturado " + 
			//	"		  FROM  DepartamentoOferta do1   " + 
			//	"		  INNER JOIN Malla m1 on do1.id=m1.idDepartamentoOferta  " + 
			//	"		  INNER JOIN PeriodoMalla pm1 on m1.id=pm1.idMalla  " + 
			//	"		  WHERE  pm1.idPeriodoAcademico=?1 and do1.id=?2 and do1.estado='A' " + 
			//	"		  and m1.estado in ('A', 'P') and pm1.estado='A')  "+
				" GROUP BY  pm.idPeriodoAcademico, m1.id,m1.idNivelMinAperturado,m1.idNivelMaxAperturado "
			)
	customObjetbuscarCantParalelo buscarCantParalelo(Integer idPeriodoAca ,Integer idDepartamentoOfer );
	interface customObjetbuscarCantParalelo{
		Integer getIdPeriodoAcademico();
		String getIdMalla();
		Integer getCantAsigMalla();
		Integer getCantAsigConParalelo();
		Integer getCantAsigConAsigApren();
	
		
	}
	
	
}
