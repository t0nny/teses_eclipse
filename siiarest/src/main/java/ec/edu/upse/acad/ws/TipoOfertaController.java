package ec.edu.upse.acad.ws;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;



import ec.edu.upse.acad.model.pojo.TipoOferta;

import ec.edu.upse.acad.model.repository.TipoOfertaRepository;
import ec.edu.upse.acad.model.service.SecurityService;


@RestController
@RequestMapping("/api/tipooferta")
@CrossOrigin

public class TipoOfertaController {
	@Autowired private TipoOfertaRepository tipoofertaRepository;
	@Autowired private SecurityService securityService;
	//****************** SERVICIOS PARA TIPO COMPONENTE ORGANIZACION************************//
	/**
	 * retorna la lista de los tipos de oferta 
	 * utilizado en la interfaz de reporte
	 * @param Authorization
	 * @return
	 */
	@RequestMapping(value="/buscarTipoOferta", method=RequestMethod.GET)
	
	public ResponseEntity<?> buscarTipoOferta(@RequestHeader(value="Authorization") String Authorization) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(tipoofertaRepository.buscarTipoOferta());
	}
	


	//servicio que actualiza o crea un nuevo TipoCompOrganizacion	
	//buscar por el id de la Modalidad

	/***
	 * servicio que retorna la lista de tipo de oferta filtrando por el id del tipo oferta
	 * @param authorization
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/buscartipoOfertaid/{idtipoofer}", method=RequestMethod.GET)
	public ResponseEntity<?> buscartipoOfertaid(@PathVariable("id") Integer id) {
		return ResponseEntity.ok(tipoofertaRepository.buscarTipoOfertaId(id));
	}

	/***
	 * servicio que retorna la lista de tipo de oferta filtrando por abreviatura de la descripcion del tipo oferta
	 * 
	 * @param authorization
	 * @param descripcion
	 * @return
	 */
	@RequestMapping(value="/buscarPorDescripcion/{descripcion}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarPorDescripcion(@PathVariable("descripcion") String descripcion) {
		return ResponseEntity.ok(tipoofertaRepository.buscarTipoOfertaDescripcion("%" + descripcion + "%"));
	}

	/****
	 * servicio que graba el tipo oferta	
	 * @param Authorization
	 * @param tipooferta
	 * @return
	 */

	@RequestMapping(value="/grabarTipooferta", method=RequestMethod.POST)
	public ResponseEntity<?> grabarTipooferta(@RequestBody TipoOferta tipooferta) {
		TipoOferta _tipooferta = new TipoOferta();
		if (tipooferta.getId() != null) {
			_tipooferta = tipoofertaRepository.findById(tipooferta.getId()).get();
		}
		BeanUtils.copyProperties(tipooferta, _tipooferta);
		tipoofertaRepository.save(_tipooferta);
		return ResponseEntity.ok(_tipooferta);
	}

	//Servicio de borrar los tipos de oferta
	@RequestMapping(value="/borrar/{idtipofer}", method=RequestMethod.DELETE)
	public ResponseEntity<?> borrar(@PathVariable("idtipofer") Integer id) {
		TipoOferta TiOfer = tipoofertaRepository.findById(id).get();
		if (TiOfer !=null) {
			TiOfer.setEstado("I");
			tipoofertaRepository.save(TiOfer);
		}

		return ResponseEntity.ok().build();
	}
}
