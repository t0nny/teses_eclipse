package ec.edu.upse.acad.model.repository.vinculacion;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.vinculacion.Proyecto;

@Repository
public interface ProyectoRepository extends JpaRepository<Proyecto, Integer> {
	// Listado de Proyecto activos y sus relaciones
	@Transactional
	@Query(value = "SELECT p FROM Proyecto p " + " WHERE p.estado='A' ")
	List<Proyecto> buscarListaProyecto();

	// solo Listado de Proyecto
	@Query(value = "SELECT p.id as idProyecto, p.titulo as titulo, pg.titulo as tituloPrograma, pa.codigo as codigoPeriodo,"
			+ " p.codigo as codigo, p.fechaDesde as fechaDesde, p.fechaHasta as fechaHasta, "
			+ " p.estadoProyecto as estadoProyecto, p.estado as estado" + " FROM Proyecto p "
			+ "INNER JOIN Programa pg on p.idPrograma=pg.id "
			+ "INNER JOIN ProyectoConvocatoria cp on p.idProyectoConvocatoria=cp.id "
			+ "INNER JOIN PeriodoAcademico pa on cp.idPeriodoAcademico=pa.id " + "WHERE p.estado='A' ")
	List<customObjetbuscarSoloProyectos> buscarSoloProyectos();

	interface customObjetbuscarSoloProyectos {
		Integer getIdProyecto();

		String getTitulo();

		String getTituloPrograma();

		String getCodigoPeriodo();

		String getCodigo();

		Date getFechaDesde();

		Date getFechaHasta();

		String getEstadoProyecto();

		String getEstado();

	}

	// solo Listado de docentes peertenecientes a una oferta
	@Query(value = "SELECT doc.id as idDocente, (CONCAT(p.nombres ,' ' ,p.apellidos)) as nombres, doc.estado as estado from DistributivoGeneral dg "
			+ "INNER JOIN DistributivoGeneralVersion dgv on dg.id= dgv.idDistributivoGeneral "
			+ "INNER JOIN DistributivoOferta do on dgv.id = do.idDistributivoGeneralVersion "
			+ "INNER JOIN DistributivoOfertaVersion dov on do.id=dov.idDistributivoOferta "
			+ "INNER JOIN DistributivoDocente dd on dov.id=dd.idDistributivoOfertaVersion "
			+ "INNER JOIN Docente doc on dd.idDocente=doc.id " + "INNER JOIN Persona p on doc.idPersona=p.id "
			+ "INNER JOIN DepartamentoOferta dpo on do.idOferta=dpo.idOferta "
			+ "WHERE dg.idPeriodoAcademico=(?1) and dpo.id=(?2) and doc.estado='A' ")
	List<customObjetDocentes> buscarDocentesPorIdDepOferta(Integer idPeridoAcademico, Integer idDepOferta);

	interface customObjetDocentes {
		Integer getIdDocente();

		String getNombres();

		String getEstado();

	}

	@Query(value = " SELECT per.identificacion as identificacion  , concat (per.apellidos,' ', per.nombres) as apellidosNombres, "
			+ " est.id as idEstudiante, eso.id as idEstudianteOferta, eso.idDepartamentoOferta as idDepartamentoOferta,"
			+ " do.idOferta as idOferta, eso.idMalla as idMalla, mal.descripcion as malla, tie.descripcion as tipoIngreso "
			+ " FROM Persona as per " + " INNER JOIN Estudiante as est on per.id=est.idPersona "
			+ " INNER JOIN EstudianteOferta as eso on est.id=eso.idEstudiante "
			+ " INNER JOIN DepartamentoOferta as do on eso.idDepartamentoOferta=do.id"
			+ " INNER JOIN Malla as mal on eso.idMalla=mal.id "
			+ " INNER JOIN TipoIngresoEstudiante as tie on tie.id=eso.idTipoIngresoEstudiante "
			+ " WHERE do.id=(?1) and tie.codigo in ('HOM','REC')")
	List<CustomObjectEstudianteOferta> ListarEstudianteOferta(Integer idDepOferta);

	interface CustomObjectEstudianteOferta {
		String getIdentificacion();

		String getApellidosNombres();

		Integer getIdEstudiante();

		Integer getIdEstudianteOferta();

		Integer getIdDepartamentoOferta();

		Integer getIdOferta();

		Integer getIdMalla();

		String getMalla();

		String getTipoIngreso();
	}

	// procedimiento almacenado para recuperar los docentes con sus titulos
	@Query(value = "{call  vin.listar_director_titulos(:idDocente)}", nativeQuery = true)
	public List<customObjetdirectorProyectoTitulos> directorProyectoTitulos(@Param("idDocente") Integer idDocente);

	interface customObjetdirectorProyectoTitulos {
		Integer getIdDocente();

		Integer getIdProyectoDocente();

		String getIdentificacion();

		String getNombres();

		String getApellidos();

		String getEmail();

		String getTituloGrado();

		String getTituloPosgrado();

		String getCedular();

		String getEstado();
	}
	/*
	 * @Procedure(procedureName ="vin.listar_director_titulos",
	 * outputParameterName="25") Object[]
	 * directorProyectoTitulos(@Param("idDocente") Integer idDocente); //interface
	 * customObjetdirectorProyectoTitulos{}
	 */

	// procedimiento alacenado para recuperar los estudiantes de un semestre y
	// paralelo determinado
	@Query(value = "{call  vin.listar_estudiantes_proyecto(:idPerAcademico,:idDepOferta,:idSemestre,:idParalelo,:idEstudiante)}", nativeQuery = true)
	public List<customObjetestudiantes> estudiantesSemestre(@Param("idPerAcademico") Integer idPerAcademico,
			@Param("idDepOferta") Integer idDepOferta, @Param("idSemestre") Integer idSemestre,
			@Param("idParalelo") Integer idParalelo, @Param("idEstudiante") Integer idEstudiante);

	interface customObjetestudiantes {
		Integer getIdEstudiante();

		Integer getIdNivel();

		Integer getIdProyectoEstudiante();

		String getIdentificacion();

		String getNombreCompleto();

		String getCursoSemestre();

		String getEmail();

		String getTelefono();

	}

	// director de Proyecto
	@Query(value = "SELECT d.id AS idDocente, p.identificacion AS identificacion, p.nombres AS nombres, p.apellidos as apellido,"
			+ " p.email_institucional AS email, p.celular AS celular from Docente d "
			+ "INNER JOIN Persona p on d.idPersona=p.id " + "WHERE d.id=(?1) and d.estado='A'")
	List<customObjetdirectorProyecto> directorProyecto(Integer idDocente);

	interface customObjetdirectorProyecto {
		Integer getIdDocente();

		String getIdentificacion();

		String getNombres();

		String getApellido();

		String getEmail();

		String getCelular();

	}

	// estudiante de Proyecto
	@Query(value = "SELECT e.id AS idEstudiante, p.identificacion AS identificacion, p.nombres AS nombres, p.apellidos as apellido,"
			+ " p.email_institucional AS email, p.celular AS celular from Estudiante e "
			+ "INNER JOIN Persona p on e.idPersona=p.id " + "WHERE e.id=(?1) and e.estado='A'")
	List<customObjetestudianteProyecto> estudianteProyecto(Integer idEstudiante);

	interface customObjetestudianteProyecto {
		Integer getIdEstudiante();

		String getIdentificacion();

		String getNombres();

		String getApellido();

		String getEmail();

		String getCelular();

	}

	// retorna proyecto versiones y sus matricez ex-ante
	@Query(value = "SELECT p.id as id, p.titulo as titulo,pv.id as idProyectoVersion,pv.version as version,pv.url as url,"
			+ " cm.id as idCabMatriz,cm.fechaDesde as fechaDesde,cm.fechaHasta as fechaHasta,cm.estadoMatriz as estadoMatriz,"
			+ "pv.estadoProyectoVersion as estadoProyectoVersion from Proyecto p "
			+ "INNER JOIN ProyectoVersion pv on pv.proyecto.id=p.id "
			+ "LEFT JOIN CabeceraMatriz cm on cm.idProyectoVersion=pv.id " + "WHERE p.id=(?1) and p.estado='A'")
	List<proyectoVersionMatriz> proyectoVersionMatriz(Integer idProyecto);

	interface proyectoVersionMatriz {
		Integer getId();
		String getTitulo();
		Integer getIdProyectoVersion();
		String getVersion();
		String getUrl();
		Integer getIdCabMatriz();
		Date getFechaDesde();
		Date getFechaHasta();
		String getEstadoMatriz();
		String getEstadoProyectoVersion();

	}

	// retorna proyecto versiones y sus matricez ex-ante
	@Query(value = "SELECT p.id as idProyecto,pv.id as idProyectoVersion,p.titulo as titulo,"
			+ " p.fechaDesde as fechaDesde,p.fechaHasta as fechaHasta,d.nombre as departamento,o.descripcion as oferta,"
			+ " (concat(pe.nombres ,' ',pe.apellidos))as nombres,tup.codigo as codigoUsuarioProy,tup.descripcion as descripcionUsuProy from Proyecto p "
			+ "INNER JOIN DepOfertaProyecto dop on p.id=dop.proyecto.id "
			+ "INNER JOIN DepartamentoOferta do on dop.idDepartamentoOferta=do.id "
			+ "INNER JOIN Departamento d on do.idDepartamento=d.id " + "INNER JOIN Oferta o on do.idOferta=o.id "
			+ "INNER JOIN ProyectoDocente pd on p.id=pd.proyecto.id "
			+ "INNER JOIN ProyectoVersion pv on pv.proyecto.id=p.id "
			// + "INNER JOIN UsuarioProyecto up on pd.idUsuarioProyecto=up.id "
			+ "INNER JOIN TipoUsuarioProyecto tup on tup.id= pd.idTipoUsuarioProyecto "
			+ "INNER JOIN Docente doc on pd.idDocente=doc.id " + "INNER JOIN Persona pe on doc.idPersona=pe.id "
			+ "WHERE pv.id=(?1) and p.estado='A'")
	List<proyectoCabMatriz> proyectoCabMatriz(Integer idProyectoVersion);

	interface proyectoCabMatriz {
		Integer getIdProyecto();

		String getTitulo();

		Integer getIdProyectoVersion();

		String getDepartamento();

		String getOferta();

		String getCodigoUsuarioProy();

		String getDescripcionUsuProy();

		Date getFechaDesde();

		Date getFechaHasta();

		String getNombres();

	}

	// tituloas academicos del director
	@Query(value = "select fp.titulo AS titulos  from FormacionProfesional fp "
			+ "where fp.idDocente=(?1) and fp.estado='A'")
	List<customObjettituloDirector> tituloDirector(Integer idDocente);

	interface customObjettituloDirector {
		String getTitulos();

	}

	// buscar id docente parametro iduser
	@Query(value = "select d.id AS idDocente from Usuario u " + "inner join Docente d on u.persona_id=d.idPersona "
			+ "where u.id=(?1) and u.estado='AC' and d.estado='A'")
	Optional<obtetoIdUser> buscarIdDocente(Integer idUser);

	interface obtetoIdUser {
		Integer getIdDocente();

		String getUsuario();

	}

	// buscar codigo del proyecto
	@Query(value = "select p.codigo as codigo FROM Proyecto p " 
			+ "inner join ProyectoVersion pv on pv.proyecto.id=p.id "
			+ "where pv.id=(?1) and p.estado='A' and pv.estado='A'")
	Optional<obtetoCodigo> buscarCodigoProyecto(Integer idProyectoVersion);

	interface obtetoCodigo {
		String getCodigo();

	}

	/*
	 * Retorna la lista de las preguntas con sus respuesta de acuerdo al tipo de
	 * instrumento
	 */
	@Query(value = "select id_pregunta as idPregunta , codigo ,descripcion, respuesta,id_categoria_pregunta as idCategoriaPregunta, categoria "
			+ "from vin.fn_listar_preguntas_instrumentoVin(:id_instrumento) ", nativeQuery = true)
	public List<CustomObjectFnPreguntaInstrumentoVin> getFnListarPreguntaInstrumentoVin(
			@Param("id_instrumento") Integer idInstrumento);

	public interface CustomObjectFnPreguntaInstrumentoVin {
		Integer getIdPregunta();

		String getCodigo();

		String getDescripcion();

		String getRespuesta();

		Integer getIdCategoriaPregunta();

		String getCategoria();
	}

	/*
	 * Retorna la version del proyecto
	 */
	@Query(value = "select top 1 pv.url,pv.id_proyecto_version as idProyectoVersion " + "from vin.proyecto p "
			+ "inner join vin.proyecto_version pv on p.id_proyecto=pv.id_proyecto "
			+ "where p.id_proyecto=(:idProyecto) and p.estado='A' and pv.estado='A'"
			+ "order by pv.id_proyecto_version desc ", nativeQuery = true)
	public List<objetoVersionesProyecto> buscarVersionProyecto(@Param("idProyecto") Integer idProyecto);

	public interface objetoVersionesProyecto {
		String getUrl();
		String getIdProyectoVersion();
	}

	/*
	 * Retorna la lista de las asignaturas para llenar el combo de
	 * asignaturasProyectos
	 */
	@Query(value = "select ma.id as idMallaAsignatura, a.id as idAsignatura, "
			+ "a.descripcion as descripcion,n.descripcion as semestre, a.estado as estado " + "from MallaAsignatura ma "
			+ "inner join Malla m on ma.idMalla=m.id " + "inner join Asignatura a on ma.idAsignatura=a.id "
			+ "inner join Nivel n on ma.idNivel=n.id "
			+ "where m.idDepartamentoOferta=(?1) and ma.idNivel=(?2) and a.estado='A' and ma.estado='A'")
	List<obtetoAsignaturas> buscarAsignaturasProyectos(Integer idDepOferta, Integer IdSemestre);

	interface obtetoAsignaturas {
		Integer getIdAsignatura();

		Integer getIdMallaAsignatura();

		String getDescripcion();

		String getSemestre();

	}

	/*
	 * Retorna la lista de las asignaturas para llenar el grid de
	 * asignaturasProyectos
	 */
	@Query(value = "select pa.id as idProyectoAsignatura,ma.id as idMallaAsignatura, a.id as idAsignatura, "
			+ "a.descripcion as descripcion,n.descripcion as semestre, a.estado as estado "
			+ "from ProyectoAsignatura pa " + "inner join MallaAsignatura ma on pa.idMallaAsignatura=ma.id "
			+ "inner join Malla m on ma.idMalla=m.id " + "inner join Asignatura a on ma.idAsignatura=a.id "
			+ "inner join Nivel n on ma.idNivel=n.id "
			+ "where pa.proyecto.id=(?1) and a.estado='A' and ma.estado='A' and pa.estado='A'")
	List<obtetoAsignaturasGrid> buscarAsignaturasProyectosGrid(Integer idProyecto);

	interface obtetoAsignaturasGrid {
		Integer getIdProyectoAsignatura();

		Integer getIdAsignatura();

		Integer getIdMallaAsignatura();

		String getDescripcion();

		String getSemestre();

	}

}
