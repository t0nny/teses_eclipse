package ec.edu.upse.acad.model.service.vinculacion;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jfree.util.Log;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.upse.acad.model.pojo.vinculacion.ConvocatoriaPrograma;
import ec.edu.upse.acad.model.pojo.vinculacion.Programa;
import ec.edu.upse.acad.model.repository.vinculacion.ConvocatoriaProgramaRepository;
import ec.edu.upse.acad.model.repository.vinculacion.ProgramaRepository;

@Service
@Transactional
public class ProgramaService {
	@Autowired	private ProgramaRepository programaRepository;	
	@PersistenceContext	private EntityManager em;
	@Autowired	private ConvocatoriaProgramaRepository convocatoriaProgramaRepository;
	
	public void grabarPrograma(Programa programa) {
		System.out.println(programa.getId());
		if (programa.getId() != null) {
			System.out.println("editar registro");
			editarProyecto(programa);
		} else {
			System.out.println("ingresar registro");
			nuevoProyecto(programa);
		}		
	}

	private void nuevoProyecto(Programa programa) {
		programaRepository.save(programa);
		em.getEntityManagerFactory().getCache().evict(Programa.class);
		
	}

	private void editarProyecto(Programa programa) {
		Log.info(programa);
		Programa _programa =programaRepository.findById(programa.getId()).get();
		BeanUtils.copyProperties(programa, _programa, "convocatoriaProgramas");
		programaRepository.save(_programa);
		System.out.println("entre 1");
		for (ConvocatoriaPrograma convocatoriaPrograma : programa.getConvocatoriaProgramas()) {
			System.out.println("entre 2");
			if(convocatoriaPrograma.getId() != null) {
				System.out.println("entre 3");
				ConvocatoriaPrograma _convocatoriaPrograma = convocatoriaProgramaRepository.findById(convocatoriaPrograma.getId()).get();
				BeanUtils.copyProperties(convocatoriaPrograma, _convocatoriaPrograma, "programa");
				_convocatoriaPrograma.setPrograma(programa);
				convocatoriaProgramaRepository.save(_convocatoriaPrograma);			
			}else {
				System.out.println("Nueva Convocatoria");
				ConvocatoriaPrograma __convocatoriaPrograma=new ConvocatoriaPrograma();
				BeanUtils.copyProperties(convocatoriaPrograma, __convocatoriaPrograma, "programa");
				__convocatoriaPrograma.setPrograma(programa);
				convocatoriaProgramaRepository.save(__convocatoriaPrograma);		
			}
			
		}
		em.getEntityManagerFactory().getCache().evict(Programa.class);
		
	}

}
