package ec.edu.upse.acad.model.repository.vinculacion;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.vinculacion.EjecucionActividad;
@Repository
public interface EjecucionActividadRepository extends JpaRepository<EjecucionActividad, Integer> {
	
}
