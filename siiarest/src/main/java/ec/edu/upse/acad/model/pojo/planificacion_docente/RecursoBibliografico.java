package ec.edu.upse.acad.model.pojo.planificacion_docente;

import javax.persistence.*;

import org.hibernate.annotations.Where;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.util.List;


/**
 * The persistent class for the recurso_bibliografico database table.
 * 
 */
@Entity
@Table(schema="aca", name="recurso_bibliografico")
@Where(clause = "estado='A'")
@NoArgsConstructor
public class RecursoBibliografico  {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_recurso_bibliografico")
	@Getter @Setter  private Long id;
	
	@Column(name="id_autor")
	@Getter @Setter private Integer idAutor;
	
	@Column(name="id_tipo_bibliografia")
	@Getter @Setter private Integer idTipoBibliografia;
	
	@Column(name="id_editorial")
	@Getter @Setter private Integer idEditorial;

	@Column(name="anio_edicion")
	@Getter @Setter private String anioEdicion;

	@Getter @Setter private String descripcion;

	@Getter @Setter private Integer edicion;

	@Getter @Setter private String estado;

//	@Column(name="fecha_ingreso")
//	private Timestamp fechaIngreso;

	@Getter @Setter private String isbn;

	@Getter @Setter private String titulo;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;

	//RELACIONES
	//bi-directional many-to-one association to AreaBibliografia
	@OneToMany(mappedBy="recursoBibliografico", cascade=CascadeType.ALL)
	@Getter @Setter private List<AreaBibliografia> areaBibliografias;

	//bi-directional many-to-one association to AsignaturaBibliografia
	@OneToMany(mappedBy="recursoBibliografico", cascade=CascadeType.ALL)
	@Getter @Setter private List<AsignaturaBibliografia> asignaturaBibliografias;

	//bi-directional many-to-one association to PlanBibliografia
	@OneToMany(mappedBy="recursoBibliografico", cascade=CascadeType.ALL)
	@Getter @Setter private List<PlanBibliografia> planBibliografias;

	//bi-directional many-to-one association to Autor
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_autor", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Autor autor;

	//bi-directional many-to-one association to Editorial
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_editorial", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Editorial editorial;

	//bi-directional many-to-one association to TipoBibliografia
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_tipo_bibliografia", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private TipoBibliografia tipoBibliografia;

	@PrePersist
	void preInsert() {
	   if (this.estado == null)
	       this.estado = "A";
	}

}