package ec.edu.upse.acad.model.repository.matricula;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.matricula.Estudiante;


@Repository
public interface EstudianteRepository extends JpaRepository<Estudiante, Integer>{
	
	@Query(value= "select eo.id as idEstudianteOferta ,m.id as idMalla,m.descripcion as malla,o.descripcion as carrera,"+
			" CONCAT(p.apellidos,' ',p.nombres) as nombreEstudiante,p.identificacion as identificacion " + 
			" from Persona p  " + 
			" inner join Estudiante e on e.idPersona = p.id " + 
			" inner join EstudianteOferta eo on eo.idEstudiante = e.id " + 
			" inner join DepartamentoOferta do on eo.idDepartamentoOferta=do.id"+
			" inner join Oferta o on o.id = do.idOferta " + 
			" inner join Malla m on m.id = eo.idMalla where eo.id = (?1)")
	
	List<CustomObjectCt> recuperarDatosEstudianteMovilidad(Integer idEstudianteOferta);
	interface CustomObjectCt{ 
	    Integer getIdEstudianteOferta(); 
	    Integer getIdMalla(); 
	    String getMalla();
	    String getCarrera(); 
	    String getNombreEstudiante();
	    String getIdentificacion();
	}
	
	@Query(value=" 	select te.id as idTipoEstudiante,te.descripcion as tipoEstudiante  from TipoEstudiante te ")
	List<CustObjTipoEstudiante> listarTiposEstudiantes();
	interface CustObjTipoEstudiante{
		Integer getIdTipoEstudiante();
		String getTipoEstudiante();
	}
	
	@Query(value=" 	select ti.id as idTipoIngresoEstudiante,ti.codigo as codigoTipoIngreso,ti.descripcion  as tipoIngreso from TipoIngresoEstudiante ti ")
	List<CustObjTipoIngreso> listarTipoIngresoEstudiante();
	interface CustObjTipoIngreso{
		Integer getIdTipoIngresoEstudiante();
		String getCodigoTipoIngreso();
		String getTipoIngreso();
	}
	
	@Query(value=" select max(m.id) as idMalla,do.idDepartamento as idDepartamento, d.nombre as facultad,o.id as idOferta,o.descripcion as carrera, " +
			" do.id as idDepartamentoOferta from Malla m " + 
			" inner join DepartamentoOferta do on do.id = m.idDepartamentoOferta " + 
			" inner join Oferta o on o.id = do.idOferta " + 
			" inner join Departamento d on d.id = do.idDepartamento " + 
			" where do.id = (?1) and m.estado in ('A','P') " +
			" group by do.idDepartamento,d.nombre,o.id,o.descripcion,do.id")
	List<CustObjMalla> recuperarMallaCarreraFacultad(Integer idDepartamentoOferta);
	interface CustObjMalla{
		Integer getIdMalla();
		Integer getIdDepartamento();
		String getFacultad();
		Integer getIdOferta();
		String getCarrera();
		Integer getIdDepartamentoOferta();
	}
	
	
}
