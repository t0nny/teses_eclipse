package ec.edu.upse.acad.model.service;

import java.util.List;

import javax.persistence.EntityManager;


import org.json.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.upse.acad.model.pojo.distributivo.DistributivoDedicacion;
import ec.edu.upse.acad.model.pojo.distributivo.DistributivoDocente;
import ec.edu.upse.acad.model.pojo.distributivo.DocenteActividad;
import ec.edu.upse.acad.model.pojo.distributivo.DocenteAsignaturaAprend;
import ec.edu.upse.acad.model.pojo.distributivo.MallaAsignaturaFusion;
import ec.edu.upse.acad.model.pojo.matricula.EstudianteAsignatura;
import ec.edu.upse.acad.model.pojo.matricula.EstudianteMatricula;
import ec.edu.upse.acad.model.pojo.matricula.EstudianteOferta;
import ec.edu.upse.acad.model.repository.distributivo.DistributivoDocenteRepository;
import ec.edu.upse.acad.model.repository.distributivo.MallaAsignaturaFusionRepository;
import javax.persistence.PersistenceContext;
@Service
@Transactional
public class MallaAsignaturaFusionService {
	@Autowired private MallaAsignaturaFusionRepository mallaAsignaturaFusionRepository ;
	@PersistenceContext
	private EntityManager em;
	
	
	
	public void grabarMallaAsignaturaFusion(List<MallaAsignaturaFusion> mallaAsignaturaFusion) {
		em.getEntityManagerFactory().getCache().evict(MallaAsignaturaFusion.class);
	  	JSONObject obj = new JSONObject(mallaAsignaturaFusion);
	    System.out.println(obj);
	    for (int i = 0; i < mallaAsignaturaFusion.size(); i++) {
	    	Integer idMallaAsignaturaFusion=0;
			if(mallaAsignaturaFusion.get(i).getId()==null) {
			
				MallaAsignaturaFusion _mallaAsignaturaFusion = new MallaAsignaturaFusion();	
				_mallaAsignaturaFusion=mallaAsignaturaFusion.get(i);
				BeanUtils.copyProperties(mallaAsignaturaFusion.get(i), _mallaAsignaturaFusion, "detalleMallaAsignaturaFusion");
				idMallaAsignaturaFusion  = mallaAsignaturaFusionRepository.saveAndFlush(_mallaAsignaturaFusion).getId();
				for (int j = 0; j <mallaAsignaturaFusion.get(i).getDetalleMallaAsignaturaFusion().size(); j++) {
				
					mallaAsignaturaFusion.get(i).getDetalleMallaAsignaturaFusion().get(j).setIdMallaAsignaturaFusion(idMallaAsignaturaFusion);
				}
				_mallaAsignaturaFusion.setDetalleMallaAsignaturaFusion(mallaAsignaturaFusion.get(i).getDetalleMallaAsignaturaFusion());
				
			}else {
				idMallaAsignaturaFusion=mallaAsignaturaFusion.get(i).getId();
				for (int j = 0; j <mallaAsignaturaFusion.get(i).getDetalleMallaAsignaturaFusion().size(); j++) {
					//if(mallaAsignaturaFusion.get(i).getDetalleMallaAsignaturaFusion().get(j).getId()==null) {
						mallaAsignaturaFusion.get(i).getDetalleMallaAsignaturaFusion().get(j).setIdMallaAsignaturaFusion(mallaAsignaturaFusion.get(i).getId());
					//}else {
						
						
					//}
				}
				mallaAsignaturaFusion.get(i).setDetalleMallaAsignaturaFusion(mallaAsignaturaFusion.get(i).getDetalleMallaAsignaturaFusion());	
			}
			MallaAsignaturaFusion _mallaAsignaturaFusionC= new MallaAsignaturaFusion();
			_mallaAsignaturaFusionC = mallaAsignaturaFusionRepository.findById(idMallaAsignaturaFusion).get();
			Integer version = _mallaAsignaturaFusionC.getVersion();
			mallaAsignaturaFusion.get(i).setId(idMallaAsignaturaFusion);
			mallaAsignaturaFusion.get(i).setVersion(version);
			mallaAsignaturaFusionRepository.save(mallaAsignaturaFusion.get(i));
			
		}
		em.getEntityManagerFactory().getCache().evict(MallaAsignaturaFusion.class);
	}


}
