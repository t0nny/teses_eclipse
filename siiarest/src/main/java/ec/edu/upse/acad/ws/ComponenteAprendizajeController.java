package ec.edu.upse.acad.ws;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.upse.acad.model.pojo.ComponenteAprendizaje;
import ec.edu.upse.acad.model.repository.ComponenteAprendizajeRepository;


@RestController
@RequestMapping("/api/componenteaprendizaje")
@CrossOrigin

public class ComponenteAprendizajeController {
	@Autowired
	private ComponenteAprendizajeRepository componenteaprendizajeRepository;
	@Autowired

	// ***************** SERVICIOS PARA COMPONENTE APRENDIZAJE ************************//

	// Buscar todos los ComponenteAprendizaje, para listar en angular
	@RequestMapping(value = "/buscarComponenteAprendizaje", method = RequestMethod.GET)
	public ResponseEntity<?> buscarComponenteAprendizaje() {
		return ResponseEntity.ok(componenteaprendizajeRepository.buscarComponenteAprendizaje());
	}

	// servicio que actualiza o crea un nuevo ComponenteAprendizaje
	@RequestMapping(value = "/buscarComponenteAprendizajeId/{idComponenteAprendizaje}", method = RequestMethod.GET)
	public ResponseEntity<?> buscar(@PathVariable("idComponenteAprendizaje") Integer idComponenteAprendizaje) {
		// abre el pojo ComponenteAprendizaje
		ComponenteAprendizaje componenteaprendizaje;
		if (componenteaprendizajeRepository.findById(idComponenteAprendizaje).isPresent()) {
			componenteaprendizaje = componenteaprendizajeRepository.findById(idComponenteAprendizaje).get();
			return ResponseEntity.ok(componenteaprendizaje);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	// Servicio que busca por nombre
	@RequestMapping(value = "/buscarPorNombre/{nombre}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarPorNombre(@PathVariable("nombre") String nombre) {
		return ResponseEntity.ok(componenteaprendizajeRepository.buscarPorComponenteAprendizaje("%" + nombre + "%"));
	}

	// Servicio de grabar ComponenteAprendizaje
	@RequestMapping(value = "/grabarComponenteAprendizaje", method = RequestMethod.POST)
	public ResponseEntity<?> grabarComponenteAprendizaje(@RequestBody ComponenteAprendizaje componenteaprendizaje) {
		ComponenteAprendizaje _componenteaprendizaje = new ComponenteAprendizaje();
		if (componenteaprendizaje.getId() != null) {
			_componenteaprendizaje = componenteaprendizajeRepository.findById(componenteaprendizaje.getId()).get();
		}
		BeanUtils.copyProperties(componenteaprendizaje, _componenteaprendizaje);
		componenteaprendizajeRepository.save(_componenteaprendizaje);
		return ResponseEntity.ok(_componenteaprendizaje);
	}

	@RequestMapping(value = "/borrar/{idComponenteAprendizaje}", method = RequestMethod.DELETE)
	public ResponseEntity<?> borrar(@PathVariable("idComponenteAprendizaje") Integer idComponenteAprendizaje) {
		ComponenteAprendizaje componenteaprendizaje = componenteaprendizajeRepository.findById(idComponenteAprendizaje)
				.get();
		if (componenteaprendizaje != null) {
			componenteaprendizaje.setEstado("I");
			componenteaprendizajeRepository.save(componenteaprendizaje);
		}

		return ResponseEntity.ok().build();
	}

	//listar componente aprendizaje de malla vigente 
	@RequestMapping(value = "/listarComponenteAprendizajeVigente/{idCompVigente}", method = RequestMethod.GET)
	public ResponseEntity<?> listarComponenteAprendizajeVigente(@PathVariable("idCompVigente") Integer id) {
		return ResponseEntity.ok(componenteaprendizajeRepository.listarcomponenteaprendizajevigente(id));
	}

	/***
	 * Servicio que lista los componentes de aprendizaje
	 */
	@RequestMapping(value = "/listarComponenteAprendizajeForm", method = RequestMethod.GET)
	public ResponseEntity<?> listarComponenteAprendizajeForm() {
		return ResponseEntity.ok(componenteaprendizajeRepository.listarComponenteAprendizajeForm());
	}

}
