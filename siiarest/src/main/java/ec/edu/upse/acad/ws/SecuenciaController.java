package ec.edu.upse.acad.ws;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.upse.acad.model.pojo.AsignaturaRelacion;

import ec.edu.upse.acad.model.pojo.MallaAsignatura;
import ec.edu.upse.acad.model.repository.AsignaturasRelacionesRepository;
import ec.edu.upse.acad.model.repository.MallaAsignaturaRepository;

@RestController
@RequestMapping("/api/secuencia")
@CrossOrigin
public class SecuenciaController {


	@Autowired private AsignaturasRelacionesRepository asignaturasRelacionesRepository;
	@Autowired private MallaAsignaturaRepository mallaAsignaturaRepository;

	//****************** SERVICIOS PARA ASIGNATURA RELACIONES************************//

	//Buscar todos las departamentos_periodos, para listar en angular
	@RequestMapping(value="/buscarAsignaturasRelaciones", method=RequestMethod.GET)
	public ResponseEntity<?> buscarAsignaturasRelaciones() {
		return ResponseEntity.ok(asignaturasRelacionesRepository.findAll());
	}

	//Buscar una asigantura_relacion , por medio del ID
	@RequestMapping(value="/buscarAsignaturasRelacionesId/{id}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarAsignaturasRelacionesId(@PathVariable("id") Integer id) {
		AsignaturaRelacion _asignaturaRelaciones;
		if (asignaturasRelacionesRepository.findById(id).isPresent()) {
			_asignaturaRelaciones = asignaturasRelacionesRepository.findById(id).get();
			return ResponseEntity.ok(_asignaturaRelaciones);
		}else {
			return ResponseEntity.notFound().build();
		}
	}	

	//lista las materias del nivel anterior a la materia seleccionada
	@RequestMapping(value="/buscarPerrequisitosAsignatura/{idMallaAsignatura}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarPerrequisitosAsignatura(@PathVariable("idMallaAsignatura") Integer idMallaAsignatura) {
		return ResponseEntity.ok(asignaturasRelacionesRepository.buscarPerrequisitos(idMallaAsignatura));
	}	

	//recupera las materias que ya fueron relacionadas como prerrequisitos
	@RequestMapping(value="/recuperarPerrequisitosAsignatura/{idMallaAsignatura}", method=RequestMethod.GET)
	public ResponseEntity<?> recuperarPerrequisitosAsignatura(@PathVariable("idMallaAsignatura") Integer idMallaAsignatura) {
		return ResponseEntity.ok(asignaturasRelacionesRepository.cargarPerrequisitosExistentes(idMallaAsignatura));
	}	

	//lista las materias del mismo nivel  a la materia seleccionada, a excepci�n de la misma
	@RequestMapping(value="/buscarCorrequisitosAsignatura/{idMallaAsignatura}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarCorrequisitosAsignatura(@PathVariable("idMallaAsignatura") Integer idMallaAsignatura)  {
		return ResponseEntity.ok(asignaturasRelacionesRepository.buscarCorrequisitos(idMallaAsignatura));
	}	


	//recupera las materias que ya fueron relacionadas como prerrequisitos
	@RequestMapping(value="/recuperarCorrequisitosAsignatura/{idMallaAsignatura}", method=RequestMethod.GET)
	public ResponseEntity<?> recuperarCorrequisitosAsignatura(@PathVariable("idMallaAsignatura") Integer idMallaAsignatura) {
		return ResponseEntity.ok(asignaturasRelacionesRepository.cargarCorrequisitosExistentes(idMallaAsignatura));
	}	


	//Buscar todas las materias asociadas a una malla con su carrera y nivel
	@RequestMapping(value="/listarAsignaturasEnMalla/{idMalla}", method=RequestMethod.GET)
	public ResponseEntity<?> listarAsignaturasEnMalla(@PathVariable("idMalla") Integer idMalla) {
		return ResponseEntity.ok(asignaturasRelacionesRepository.listarAsignaturasAsociadasMallaCarrera(idMalla));
	}

	//Recuperar nombre de asignatura y nivel
	@RequestMapping(value="/recuperarNombresAsignaturaNivel/{idMallaAsignatura}", method=RequestMethod.GET)
	public ResponseEntity<?> recuperarNombresAsignaturaNivel(@PathVariable("idMallaAsignatura") Integer idMallaAsignatura) {
		return ResponseEntity.ok(asignaturasRelacionesRepository.recuperarNombreMateriaNivel(idMallaAsignatura));
	}

	/***
	 * Recupera el json completo de malla Asignatura
	 * **/
	@RequestMapping(value="/recuperarJsonMallaAsignatura/{idMallaAsignatura}", method=RequestMethod.GET)
	public ResponseEntity<?> recuperarJsonMallaAsignatura(@PathVariable("idMallaAsignatura") Integer idMallaAsignatura) {
		MallaAsignatura mallaAsignatura = mallaAsignaturaRepository.findById(idMallaAsignatura).get();
		MallaAsignatura mallaAsignatura_ = new MallaAsignatura();
		BeanUtils.copyProperties(mallaAsignatura, mallaAsignatura_, "planificacionParalelo",
				"asignaturaOrganizacions", "asignaturaAprendizaje", "asignaturaRequisitos", "itinerarioMaterias");
		return ResponseEntity.ok(mallaAsignatura_);

	}

	//***********************FIN SERVICIOS PARA ASIGNATURA RELACIONES***********************

	//Buscar todos las asignaturas ligadas a una malla
	@RequestMapping(value="/buscarMallasAsignaturas", method=RequestMethod.GET)
	public ResponseEntity<?> buscarMallasAsignaturas() {
		return ResponseEntity.ok(mallaAsignaturaRepository.findAll());
	}

	//servicio para grabar los mallasAsignaturas y sus relaciones (pre y correquisitos)	
	@RequestMapping(value="/grabarPreCorrequisitos", method=RequestMethod.POST)
	public ResponseEntity<?> grabarPreyCorrequisitos(@RequestBody MallaAsignatura mallaAsignatura) {
		MallaAsignatura _mallaAsignatura = new MallaAsignatura();
		if (mallaAsignatura.getId() != null) {
			_mallaAsignatura = mallaAsignaturaRepository.findById(mallaAsignatura.getId()).get();
		}
		BeanUtils.copyProperties(mallaAsignatura, _mallaAsignatura);
		mallaAsignaturaRepository.save(_mallaAsignatura);
		return ResponseEntity.ok(_mallaAsignatura);
	}


}
