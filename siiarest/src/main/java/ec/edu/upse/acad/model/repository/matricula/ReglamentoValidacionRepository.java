package ec.edu.upse.acad.model.repository.matricula;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ec.edu.upse.acad.model.pojo.reglamento.ReglamentoValidacion;

@Repository
public interface ReglamentoValidacionRepository extends JpaRepository<ReglamentoValidacion, Integer>{

	List<ReglamentoValidacion> findByIdReglamento(Integer idReglamento);

	@Query(value="select rv.codigo as codigo, rv.descripcion as descripcion, rv.valor as valor, 'A' as estado, rv.usuarioIngresoId as usuarioIngresoId, 0 as version " + 
			" from ReglamentoValidacion rv  " + 
			" where rv.idReglamento =(select max(r1.id) from Reglamento r1 where r1.idTipoReglamento = (?1))")
	List<CustomObjectRecuperarValidacion> recuperarValidacionesUltimoReglamento(Integer idTipoReglamento);
	interface CustomObjectRecuperarValidacion{
		Integer getId();
		Integer getIdReglamento();
		String getCodigo();
		String getDescripcion();
		Integer getValor();
		String getEstado();
		String getUsuarioIngresoId();
		Integer getVersion();
	}

	@Query(value="select r.id as idReglamento, rv.codigo as codigo, rv.descripcion as descripcion, rv.valor as valor " + 
			" from ReglamentoValidacion rv " + 
			" inner join Reglamento r on r.id = rv.idReglamento " + 
			" inner join MatriculaGeneral mg on mg.idReglamento = r.id " + 
			" where mg.id = (?1) ")
	List<CustObjRecuperarValMat> recuperarValidacionesUltimaMatricula(Integer idMatriculaGeneral);
	interface CustObjRecuperarValMat{
		Integer getIdReglamento();
		String getCodigo();
		String getDescripcion();
		Integer getValor();
	}

}
