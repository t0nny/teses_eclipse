package ec.edu.upse.acad.model.pojo.evaluacion;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ec.edu.upse.acad.model.pojo.reglamento.Reglamento;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="eva", name="regla_calificacion")
@NoArgsConstructor
public class ReglaCalificacion {
		    		  
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_regla_calificacion")
	@Getter @Setter private Integer id;
	
	//@Column(name="id_reglamento")
	//@Getter @Setter private Integer idReglamento;
	
	//@Column(name="id_calificacion_escala")
	//@Getter @Setter private Integer idCalificacionDocEscala;

	@Column(name="valor_minimo")
	@Getter @Setter private Integer valorMinimo;
	
	@Column(name="valor_maximo")
	@Getter @Setter private Integer valorMaximo;

	@Getter @Setter private String estado;
	
//	@Column(name="fecha_ingreso")
//	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;
	
	//RELACIONES
	//bi-directional many-to-one association to 
	@ManyToOne
	@JoinColumn(name="id_reglamento")
	//@JsonIgnore
	@Getter @Setter private Reglamento reglamento;
	
	@ManyToOne(fetch=FetchType.LAZY,cascade = CascadeType.ALL)
	@JoinColumn(name="id_calificacion_escala")
	//@JsonIgnore
	@Getter @Setter private CalificacionEscala calificacionEscala;

	@PrePersist
	void preInsert() {
		if (this.estado == null)
			this.estado = "A";
	}


}
