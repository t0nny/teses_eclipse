package ec.edu.upse.acad.model.repository.distributivo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.distributivo.DistributivoGeneralVersion;
import ec.edu.upse.acad.model.repository.DocenteRepository.listarTodosDocenteRegistrados;
import ec.edu.upse.acad.model.repository.distributivo.DistributivoDedicacionRepository.CustomObjectJ;
@Repository
public interface DistributivoGeneralVersionRepository extends JpaRepository<DistributivoGeneralVersion, Integer>{

	@Query(value=" select d.idDistributivoGeneral as iddistributivodeneral from DistributivoGeneralVersion d "
			+" where d.idDistributivoGeneral=(?1) and d.estado = 'A' ")
	
	Integer buscarDistGenelVersion(Integer idDistributivoGeneral);
	interface CustomObjectJ{ 
	    Integer getiddistributivodeneral(); 
	    //String getJornada(); 
	} 
	
	@Query(value=" 	SELECT ma.id as idMallaAsignatura,p.id as idParalelo,	p.orden as orden," + 
			"case when n.orden is null then null else concat (cast(n.orden as text),'/', p.descripcionCorta) end as paralelo, dov.descripcion as descripcion " + 
			"FROM PeriodoMalla as pm " + 
			"INNER JOIN Malla as m on pm.idMalla=m.id " + 
			"INNER JOIN DepartamentoOferta doo on m.idDepartamentoOferta=doo.id "+
			"INNER JOIN MallaAsignatura ma on m.id=ma.idMalla " + 
			"INNER JOIN AsignaturaAprendizaje aa on ma.id=aa.idMallaAsignatura " + 
			"INNER JOIN PlanificacionParalelo pp on ma.id=pp.idMallaAsignatura " + 
			"										and pm.idPeriodoAcademico=pp.idPeriodoAcademico " + 
			"INNER JOIN Nivel n on ma.idNivel= n.id " + 
			"INNER JOIN Paralelo p on  p.orden<= isnull (pp.numParalelos,0) " + 
			"INNER JOIN ComponenteAprendizaje ca on aa.idComponenteAprendizaje=ca.id " + 
			"INNER JOIN ReglamentoCompAprendizaje rc on ca.id=rc.idCompAprendizaje " + 
			"INNER JOIN DocenteAsignaturaAprend daa on aa.id=daa.idAsignaturaAprendizaje and daa.estado='A' " + 
			"INNER JOIN DistributivoDocente dd on daa.idDistributivoDocente=dd.id and dd.estado='A' " + 
			"INNER JOIN DistributivoOfertaVersion dov on dd.idDistributivoOfertaVersion=dov.id and dov.estado in ('A') " + 
			"INNER JOIN DistributivoOferta do on dov.idDistributivoOferta=do.id and do.estado='A' " + 
			"INNER JOIN DistributivoGeneralVersion dgv on do.idDistributivoGeneralVersion=dgv.id and dgv.estado='A' " + 
			"INNER JOIN DistributivoGeneral dg on dgv.idDistributivoGeneral=dg.id and dg.idPeriodoAcademico=pm.idPeriodoAcademico and dg.estado='A' " + 
			"WHERE pm.estado='A' and m.estado='P' and ma.estado='A' and aa.estado='A' and pp.estado='A' " + 
			"and rc.asignarDocente=1 and pm.idPeriodoAcademico=?1 and doo.idOferta=?2 " + 
			"and dov.versionDistributivoOferta =(select max( dov1.versionDistributivoOferta) from DistributivoGeneral dg1 "+
			"inner join DistributivoGeneralVersion dgv1 on dg.id=dgv1.idDistributivoGeneral  "+
			"inner join DistributivoOferta do1 on dgv1.id=do.idDistributivoGeneralVersion "+
			"inner join DistributivoOfertaVersion dov1 on do.id=dov1.idDistributivoOferta "+
			"where dg1.estado='A' and dgv1.estado='A' and do1.estado='A' and dov1.estado in ('A') and dg1.idPeriodoAcademico=?1 and do1.idOferta=?2 ) "+
			"group by p.id,ma.id, pm.idPeriodoAcademico, n.orden, p.descripcionCorta,dov.descripcion,p.orden " + 
		
			"order by n.orden ")
	
	List <CustomObject> buscarParaleloUtil(Integer idPeriodoAcademico, Integer idOferta);
	interface CustomObject{ 
	    Integer getIdMallaAsignatura(); 
	    Integer getIdParalelo(); 
	    Integer getOrden();
	    String getParalelo();
	    String getDescripcion();
	    //String getJornada(); 
	} 


	
}
