package ec.edu.upse.acad.model.repository.distributivo;


import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.distributivo.DocenteAsignaturaAprend;
import ec.edu.upse.acad.model.repository.matricula.MatriculaGeneralRepository.CustomObjectCargarAsignaturasMatriculas;

@Repository
public interface DocenteAsignaturaAprendRepository extends JpaRepository<DocenteAsignaturaAprend, Integer> {
	
	@Query(value = "{call aca.sp_generar_detalle_distributivo_oferta_docente_asignatura(:idDistributivoOfertaVersion, :idDistributivoDocente,:idDocente)}", nativeQuery = true)
	public List<CustomObjectCargarDetalleDistributivoOfertaDocenteAsig> getSPDetalleDistributivoOfertaDocenteAsig(@Param("idDistributivoOfertaVersion") Integer idDistributivoOfertaVersion,
			@Param("idDistributivoDocente") Integer idDistributivoDocente,@Param("idDocente") Integer idDocente);
	public interface CustomObjectCargarDetalleDistributivoOfertaDocenteAsig{
		Integer getIdDistributivoGeneralVersion();
		Integer getIdDistributivoOferta();
		Integer getIdDistributivoOfertaVersion();
		Integer getIdDistributivoDocente();
		Integer getIdDocente();
		Integer getIdDocenteAsignaturaAprend();
		Integer getIdAsignaturaAprendizaje();
		Integer getIdParalelo();
		Integer getIdEspacioFisico();
		String getEstado();
		Integer getVersion();
		Integer getNumEstudiantes();
		Integer getIdMallaAsignatura();
		Integer getIdAsignatura();
		String getAsignatura();
		Boolean getAfinAsignatura();
		String getParalelo();
		Integer getIdOfertaAsignatura();
		String getOfertaAsignatura();
		Integer getIdComponenteAprendizaje();
		String getCodigo();
		String getCodigoComp();
		String getDescripcionCodigo();
		String getDescripcionCodigoComp();
		Integer getHorasClases();
		String getFusion();
		Integer getIdMallaAsignaturaFusion();
		Integer getIdDetalleMallaAsignaturaFusion();	
	}
	
	@Query(value = "{call aca.sp_generar_detalle_distributivo_oferta_docente_dedicacion(:idDistributivoOfertaVersion, :idDistributivoDocente,:idDocente)}", nativeQuery = true)
	public CustomObjectDedicacionDistributivoOfertaDoc getSPDedicacionDistributivoOfertaDocente(@Param("idDistributivoOfertaVersion") Integer idDistributivoOfertaVersion,
			@Param("idDistributivoDocente") Integer idDistributivoDocente,@Param("idDocente") Integer idDocente);
	public interface CustomObjectDedicacionDistributivoOfertaDoc{
		Integer getIdDistributivoGeneralVersion();
		Integer getIdDistributivoOferta();
		Integer getIdDistributivoOfertaVersion();
		Integer getIdDistributivoDocente();
		Integer getIdDocente();
		Integer getIdDocenteDedicacion();
		Integer getVersion();
		
	}
	
	

	
	@Query(value = "{call aca.sp_generar_cabecera_distributivo_oferta_docente(:idDistributivoOfertaVersion)}", nativeQuery = true)
	public List<CustomObjectCargarCabeceraDistributivoOfertaDocente> getSPCabeceraDistributivoOfertaDocente(@Param("idDistributivoOfertaVersion") Integer idDistributivoOfertaVersion);
	
	public interface CustomObjectCargarCabeceraDistributivoOfertaDocente{
		Integer getIdDocente();
		String getApellidosNombres();
		Integer getIdDistributivoDocente();
		Integer getIdDistributivoOferta();
		Integer getIdDistributivoOfertaVersion();
		Integer getIdDistributivoGeneralVersion();
		Date getFechaDesde();
		String getDedicacion();
		String getDedicacionDescripcion();
		Integer getTotalHorasClases();
		Integer getHorasActividad();
		Integer getTotalHoras();
		
	}

	
	@Query(value = "{call aca.sp_generar_detalle_distributivo_oferta_docente(:idDistributivoOfertaVersion)}", nativeQuery = true)
	public List<CustomObjectCargarDetalleDistributivoOfertaDocente> getSPDetalleDistributivoOfertaDocente(@Param("idDistributivoOfertaVersion") Integer idDistributivoOfertaVersion);

	public interface CustomObjectCargarDetalleDistributivoOfertaDocente{
		Integer getIdDocente();
		String getApellidosNombres();
		Integer getIdDistributivoDocente();
		Integer getIdDistributivoDocenteU();
		Integer getIdDistributivoOferta();
		Integer getIdDistributivoGeneralVersion();
		String getAsignatura();
		String getParalelo();
		Integer getIdParalelo();
		Integer getHorasClases();
		String getCodigoCarrera();
		Integer getNumEstudiante();
		Date getFechaDesde();
		String getFusion();
		Integer getIdMallaAsignaturaFusion();
		Integer getIdDetalleMallaAsignaturaFusion();
	}

	@Query(value=" SELECT  mas.id as idMallaAsignatura,ofe.id as idOfertaAsignatura,"+
			"ofe.descripcionCorta as ofertaAsignatura,"+
			"asi.descripcion as asignatura,count (par.id) as cantParalelos "+
			"FROM PeriodoMalla as pma  " + 
			"INNER JOIN Malla as ma on pma.idMalla=ma.id " + 
			"INNER JOIN DepartamentoOferta as do on ma.idDepartamentoOferta=do.id " + 
			"INNER JOIN MallaAsignatura as mas on ma.id=mas.idMalla  " + 
			"INNER JOIN PlanificacionParalelo pp on mas.id=pp.idMallaAsignatura " + 
			"INNER JOIN Asignatura as asi on mas.idAsignatura=asi.id " + 
			"INNER JOIN OfertaAsignatura as oas on asi.id=oas.asignatura.id " + 
			"INNER JOIN Oferta as ofe on oas.oferta.id=ofe.id " + 
			"INNER JOIN Nivel as n on mas.idNivel=n.id " + 
			"INNER JOIN Paralelo as par on par.orden<= isnull (pp.numParalelos,0)  " + 
			"WHERE pma.idPeriodoAcademico=?1 and do.idOferta=?2 and pp.idPeriodoAcademico=?1 and " + 
			"	isnull (pp.numParalelos,0)>0   " + 
			"and n.id>=ma.idNivelMinAperturado and n.id<=ma.idNivelMaxAperturado " + 
			"and mas.id not in ( select  mas1.id " +
			"					from DistributivoOfertaVersion as dov " + 
			"					inner join DistributivoDocente as d on dov.id=d.idDistributivoOfertaVersion  " + 
			"					inner join DocenteAsignaturaAprend as daa on d.id=daa.idDistributivoDocente  " + 
			"					inner join AsignaturaAprendizaje as aa on daa.idAsignaturaAprendizaje=aa.id " + 
			"					inner join MallaAsignatura as mas1 on aa.idMallaAsignatura=mas1.id " + 
			"					where mas1.estado='A' and aa.estado='A' and daa.estado='A'  and d.estado='A' and  dov.estado in ('D', 'A', 'V','P') " + 
			"					and dov.id=?3  and aa.multidocente=0" + 
			"					and (d.id != ?4  or ?4 is null) " + 
			"					and mas.id=mas1.id and aa.valor>0" + 
			"					and par.id=daa.idParalelo " + 
			"					group by mas1.id "+
			"					having count (aa.idComponenteAprendizaje)=(select count (cc.id)"+
			" 															  from AsignaturaAprendizaje aa2"+
			"															  inner join ComponenteAprendizaje cc on aa2.idComponenteAprendizaje=cc.id " + 
			"															   inner join ReglamentoCompAprendizaje rca1 on cc.id=rca1.idCompAprendizaje " + 
			"															   where cc.estado='A' and rca1.asignarDocente=1 and rca1.estado='A' and aa2.estado='A' " +
			"																and aa2.valor>0 and mas1.id=aa2.idMallaAsignatura )) "	+															
			" and pma.estado='A' and ma.estado='P' and mas.estado='A'  " + 
			"and asi.estado='A' and oas.estado='A' and ofe.estado='A' and pp.estado='A' " + 
			"and n.estado='A'  and par.estado='A' " +  
			"group by pma.idPeriodoAcademico, ma.id, mas.id, ofe.id, ofe.descripcionCorta, asi.id, asi.descripcion, pp.numParalelos " + 
			"order by asi.descripcion,mas.id "
			)
	List<CustomObjectOfertaMallaAsignatura> listarOfertaMallaAsignatura(Integer idPeridoAcademico,Integer IdOferta, Integer idDistOfertaVersion,Integer idDistributivoDoc);	
	interface CustomObjectOfertaMallaAsignatura  { 
	
		Integer getIdMallaAsignatura();
		Integer getIdOfertaAsignatura();
		String getOfertaAsignatura();
		String getAsignatura();
		Integer getCantParalelos();
	
		
	}
	
	
	
	@Query(value=" SELECT mas.id as idMallaAsignatura,aap.id as idAsignaturaAprendizaje, aap.idComponenteAprendizaje as idComponenteAprendizaje,ofe.id as idOfertaAsignatura," + 
			"			 cap1.codigo as codigo,cap1.descripcion as descripcionCodigo, "+
			" 			 cap.codigo as codigoComp,cap.descripcion as descripcionCodigoComp, "+
			"			aap.valor as valor, par.id as idParalelo,CONCAT(cast(n.orden as text),'/',par.descripcionCorta) as paralelo," + 
			"			asi.descripcion as asignatura ,0 as idDocenteAsignaturaAprend, 0 as version, 'A' as estado, false as valorCheck " + 		
			"			FROM PeriodoMalla  as pma  " + 
			"			INNER JOIN Malla as ma on pma.idMalla=ma.id " + 
			"			INNER JOIN DepartamentoOferta as do on ma.idDepartamentoOferta=do.id " + 
			"			INNER JOIN MallaAsignatura as mas on ma.id=mas.idMalla  " + 
			"			INNER JOIN PlanificacionParalelo pp on mas.id=pp.idMallaAsignatura " + 
			"			INNER JOIN AsignaturaAprendizaje as aap on mas.id=aap.idMallaAsignatura " + 
			"			INNER JOIN ComponenteAprendizaje as cap on aap.idComponenteAprendizaje=cap.id " + 
			"			INNER JOIN ComponenteAprendizaje as cap1 on cap.idComponenteAprendizajePadre=cap1.id " +
			"			INNER JOIN ReglamentoCompAprendizaje  as rca on cap.id=rca.idCompAprendizaje " + 
			"			INNER JOIN Asignatura as asi on mas.idAsignatura=asi.id " + 
			"			INNER JOIN OfertaAsignatura as oas on asi.id=oas.asignatura.id " + 
			"			INNER JOIN Oferta as ofe on oas.oferta.id=ofe.id " +
			"			INNER JOIN Nivel as n on mas.idNivel=n.id " + 
			"			INNER JOIN Paralelo as par on par.orden<= isnull (pp.numParalelos,0)  " + 
			"			WHERE pma.idPeriodoAcademico=?1 and do.idOferta=?2 and pp.idPeriodoAcademico=?1 and " + 
			"			 isnull (pp.numParalelos,0)>0 and aap.valor>0 " + 
			"			and n.id>=ma.idNivelMinAperturado and n.id<=ma.idNivelMaxAperturado " + 
			"			and aap.id not in ( SELECT  aa.id" + 
			"								FROM DistributivoOfertaVersion as dov " + 
			"								INNER JOIN DistributivoDocente as d on dov.id=d.idDistributivoOfertaVersion " + 
			"								INNER JOIN DocenteAsignaturaAprend as daa on d.id=daa.idDistributivoDocente " + 
			"								INNER JOIN AsignaturaAprendizaje as aa on daa.idAsignaturaAprendizaje=aa.id " + 
			"								INNER JOIN MallaAsignatura as mas1 on aa.idMallaAsignatura=mas1.id " + 
			"								WHERE mas1.estado='A' and aa.estado='A' and daa.estado='A'  and d.estado='A' and  dov.estado in ('D', 'A', 'V', 'P') " + 
			"								and dov.id=?3 and aa.multidocente=0 " + 
			"								and (d.id != ?4  or ?4 is null) and aa.valor>0 " + 
			"								and mas.id=mas1.id" + 
			"								and par.id =daa.idParalelo " + 
			"								group by aa.id ) " +
			"			and rca.asignarDocente=1 " + 
				"			and rca.estado='A' and pma.estado='A' and ma.estado='P' and mas.estado='A' and aap.estado='A' " + 
			"			and asi.estado='A' and oas.estado='A' and ofe.estado='A' and n.estado='A'  and par.estado='A' and pp.estado='A' " + 
			"	group by pma.idPeriodoAcademico , ma.id, mas.id ,ofe.id,ofe.descripcionCorta, par.id," +  
			"	n.orden,par.descripcionCorta,asi.descripcion ,  n.orden ,pp.numParalelos ,ofe.id," + 
			"	aap.id , aap.idComponenteAprendizaje ,	cap.codigo ,cap.descripcion , cap1.codigo, cap1.descripcion ,aap.valor " + 
			"	Order by asi.descripcion, mas.id " 
			)


	List<CustomObjectAsignaturaComp> listarOfertaMallaAsigComp(Integer idPeridoAcademico,Integer IdOferta, Integer idDistOfertaVersion,Integer idDistributivoDoc);	
	interface CustomObjectAsignaturaComp  { 
		Integer getIdMallaAsignatura();
		Integer getIdAsignaturaAprendizaje();
		Integer getIdComponenteAprendizaje();
		Integer getIdOfertaAsignatura();
		String getCodigo();
		String getDescripcionCodigo();
		String getCodigoComp();
		String getDescripcionCodigoComp();
		Integer getValor();
		Integer getIdParalelo();
		String getParalelo();
		String  getAsignatura();
		Integer getIdDocenteAsignaturaAprend();
		Integer getVersion();
		String  getEstado();
		Boolean getValorCheck();
	
	}
	
	
	
	
	@Query(value=" SELECT mas.id as idMallaAsignatura,aap.id as idAsignaturaAprendizaje, aap.idComponenteAprendizaje as idComponenteAprendizaje,ofe.id as idOfertaAsignatura," + 
			"			 cap1.codigo as codigo,cap1.descripcion as descripcionCodigo, cap.codigo as codigoComp,cap.descripcion as descripcionCodigoComp,asi.descripcion as asignatura ," + 
			" 			 aap.valor as valor, aap.multidocente as multidocente,"+
			" 			aap.estado as estado, aap.version as version " + 
			"			FROM PeriodoMalla  as pma  " + 
			"			INNER JOIN Malla as ma on pma.idMalla=ma.id " + 
			"			INNER JOIN DepartamentoOferta as do on ma.idDepartamentoOferta=do.id " + 
			"			INNER JOIN MallaAsignatura as mas on ma.id=mas.idMalla  " + 
			"			INNER JOIN AsignaturaAprendizaje as aap on mas.id=aap.idMallaAsignatura " + 
			"			INNER JOIN ComponenteAprendizaje as cap on aap.idComponenteAprendizaje=cap.id " + 
			"			INNER JOIN ComponenteAprendizaje as cap1 on cap.idComponenteAprendizajePadre=cap1.id " +
			"			INNER JOIN ReglamentoCompAprendizaje  as rca on cap.id=rca.idCompAprendizaje " + 
			"			INNER JOIN Asignatura as asi on mas.idAsignatura=asi.id " + 
			"			INNER JOIN OfertaAsignatura as oas on asi.id=oas.asignatura.id " + 
			"			INNER JOIN Oferta as ofe on oas.oferta.id=ofe.id " + 
			"			WHERE pma.idPeriodoAcademico=?1 and do.idOferta=?2  " + 
			"			and aap.valor>0 " + 
			"			and rca.asignarDocente=1 " + 
			"			and mas.id>=ma.idNivelMinAperturado and mas.id<=ma.idNivelMaxAperturado "+ 
			"			and rca.estado='A' and pma.estado='A' and ma.estado in('P') and mas.estado='A' and aap.estado='A' " + 
			"			and asi.estado='A' and oas.estado='A' and ofe.estado='A'  " + 
			"			group by pma.idPeriodoAcademico , ma.id, mas.id ,ofe.id,ofe.descripcionCorta,asi.descripcion  ,ofe.id,aap.id ,"+
			" 			aap.idComponenteAprendizaje ,	cap.codigo ,cap.descripcion , cap1.codigo, cap1.descripcion, aap.valor, aap.multidocente, aap.estado, aap.version  " + 
			"			Order by asi.descripcion, mas.id " 
			)


	List<CustomObjectAsigComp> listarOfertaMallaAsigCompMul(Integer idPeridoAcademico,Integer IdOferta);	
	interface CustomObjectAsigComp  { 
		Integer getIdMallaAsignatura();
		Integer getIdAsignaturaAprendizaje();
		Integer getIdComponenteAprendizaje();
		Integer getIdOfertaAsignatura();
		String getCodigo();
		String getDescripcionCodigo();
		String getCodigoComp();
		String getDescripcionCodigoComp();
		String  getAsignatura();
		Integer getValor();
		Boolean getMultidocente();
		String getEstado();
		Integer getVersion();
	}
	
	@Query(value=" SELECT mas.id as idMallaAsignatura,aap.id as idAsignaturaAprendizaje, cap1.codigo as codigo,cap1.descripcion as descripcionCodigo, "+
			"			cap.codigo as codigoComp,cap.descripcion as descripcionCodigoComp, asi.descripcion as asignatura, " + 		
			" 			aap.idComponenteAprendizaje as idComponenteAprendizaje, aap.valor as valor, aap.multidocente as multidocente,"+
			" 			aap.estado as estado, aap.version as version, 1 as edit " + 
			"			FROM PeriodoMalla  as pma "+
			"			INNER JOIN Malla as ma on pma.idMalla=ma.id " + 
			"			INNER JOIN DepartamentoOferta as do on ma.idDepartamentoOferta=do.id " + 
			"			INNER JOIN MallaAsignatura as mas on ma.id=mas.idMalla  " + 
			"			INNER JOIN AsignaturaAprendizaje as aap on mas.id=aap.idMallaAsignatura " + 
			"			INNER JOIN ComponenteAprendizaje as cap on aap.idComponenteAprendizaje=cap.id " + 
			"			INNER JOIN ComponenteAprendizaje as cap1 on cap.idComponenteAprendizajePadre=cap1.id " +
			"			INNER JOIN ReglamentoCompAprendizaje  as rca on cap.id=rca.idCompAprendizaje " + 
			"			INNER JOIN Asignatura as asi on mas.idAsignatura=asi.id " + 
			"			INNER JOIN OfertaAsignatura as oas on asi.id=oas.asignatura.id " + 
			"			INNER JOIN Oferta as ofe on oas.oferta.id=ofe.id " + 
			"			WHERE pma.idPeriodoAcademico=?1 and do.idOferta=?2  " + 
			"			and mas.id>=ma.idNivelMinAperturado and mas.id<=ma.idNivelMaxAperturado "+ 
			"			and aap.valor>0 " + 
			"			and rca.asignarDocente=1 " + 
			"			and aap.multidocente=1 "+			
			"			and rca.estado='A' and pma.estado='A' and mas.estado='A' and ma.estado in('P') and aap.estado='A' " + 
			"			and asi.estado='A' and oas.estado='A' and ofe.estado='A'  " + 
			"			group by pma.idPeriodoAcademico , ma.id, mas.id ,ofe.id,ofe.descripcionCorta,asi.descripcion  ,ofe.id,aap.id ,"+
			" 			aap.idComponenteAprendizaje ,	cap.codigo ,cap.descripcion , cap1.codigo, cap1.descripcion,  " + 
			"			aap.multidocente , aap.estado , aap.version, aap.valor, aap.idComponenteAprendizaje "	+		
			"			Order by asi.descripcion, mas.id " 
			)


	List<CustomObjectAsigCompMult> listarMallaAsigCompMultidocente(Integer idPeridoAcademico,Integer IdOferta);	
	interface CustomObjectAsigCompMult  { 
		Integer getIdMallaAsignatura();
		Integer getIdAsignaturaAprendizaje();
		String getCodigo();
		String getDescripcionCodigo();
		String getCodigoComp();
		String getDescripcionCodigoComp();
		String  getAsignatura();
		Integer getIdComponenteAprendizaje();
		Integer getValor();
		Boolean getMultidocente();
		String getEstado();
		Integer getVersion();
		Integer getEdit();
		
	}
	
	
	
	
	
	
	
	
	
	@Query(value=" SELECT mas.id as idMallaAsignatura,par.id as idParalelo, CONCAT(cast(n.orden as text),'/',par.descripcionCorta) as paralelo ,"+
			"   CASE WHEN ISNULL( (SELECT maf.idMallaAsignatura " + 
			"										FROM MallaAsignaturaFusion as maf " + 
			"										WHERE pma.idPeriodoAcademico=maf.idPeriodoAcademico " + 
			"										and maf.idMallaAsignatura=mas.id and maf.idParalelo=par.id " + 
			"										) , 0)>0 THEN  'P'  WHEN   ISNULL( (SELECT dmaf.idMallaAsignatura " + 
			"										FROM MallaAsignaturaFusion as maf " + 
			"										INNER JOIN DetalleMallaAsignaturaFusion dmaf on maf.id=dmaf.idMallaAsignaturaFusion " + 
			"										and dmaf.idMallaAsignatura=mas.id and dmaf.idParalelo=par.id " + 
			"										where pma.idPeriodoAcademico=maf.idPeriodoAcademico " + 
			"										) , 0)>0 THEN  'F' else ''  END as fusion," + 
			"	ISNULL( (SELECT maf.id 	FROM MallaAsignaturaFusion as maf " + 
			"										INNER JOIN DetalleMallaAsignaturaFusion as dmaf on maf.id=dmaf.idMallaAsignaturaFusion" + 
			"										where pma.idPeriodoAcademico=maf.idPeriodoAcademico" + 
			"										and( ( maf.idMallaAsignatura=mas.id and maf.idParalelo=par.id) " + 
			"										or ( dmaf.idMallaAsignatura=mas.id and dmaf.idParalelo=par.id) )" + 
			"										group by maf.id " + 
			"										) , 0) as idMallaAsignaturaFusion, " + 
			"	isnull(	(SELECT dmaf.id FROM MallaAsignaturaFusion as maf " + 
			"										INNER JOIN DetalleMallaAsignaturaFusion dmaf on maf.id=dmaf.idMallaAsignaturaFusion " + 
			"										and dmaf.idMallaAsignatura=mas.id and dmaf.idParalelo=par.id " + 
			"										where pma.idPeriodoAcademico=maf.idPeriodoAcademico), 0) as idDetalleMallaAsignaturaFusion " + 
			"FROM PeriodoMalla as pma  " + 
			"INNER JOIN Malla as ma on pma.idMalla=ma.id " + 
			"INNER JOIN DepartamentoOferta as do on ma.idDepartamentoOferta=do.id " + 
			"INNER JOIN MallaAsignatura as mas on ma.id=mas.idMalla  " + 
			"INNER JOIN PlanificacionParalelo pp on mas.id=pp.idMallaAsignatura " + 
			"INNER JOIN AsignaturaAprendizaje as aap on mas.id=aap.idMallaAsignatura " + 
			"INNER JOIN Nivel as n on mas.idNivel=n.id " + 
			"INNER JOIN ComponenteAprendizaje as cap on aap.idComponenteAprendizaje=cap.id " + 
			"INNER JOIN ComponenteAprendizaje as cap1 on cap.idComponenteAprendizajePadre=cap1.id " +
			"INNER JOIN ReglamentoCompAprendizaje  as rca on cap.id=rca.idCompAprendizaje " + 
			"INNER JOIN Paralelo as par on par.orden<= isnull (pp.numParalelos,0)  " + 
			"WHERE pma.idPeriodoAcademico=?1 and do.idOferta=?2 and pp.idPeriodoAcademico=?1 and " + 
			"	isnull (pp.numParalelos,0)>0 and aap.valor>0 " + 
			"	and rca.asignarDocente=1 and (aap.multidocente=1 or aap.multidocente=0 )"+	
			"and n.id>=ma.idNivelMinAperturado and n.id<=ma.idNivelMaxAperturado " + 
			"and aap.id not in ( select  aa.id " + 
			"					from DistributivoOfertaVersion as dov " + 
			"					inner join DistributivoDocente as d on dov.id=d.idDistributivoOfertaVersion  " + 
			"					inner join DocenteAsignaturaAprend as daa on d.id=daa.idDistributivoDocente  " + 
			"					inner join AsignaturaAprendizaje as aa on daa.idAsignaturaAprendizaje=aa.id " + 
			"					inner join MallaAsignatura as mas1 on aa.idMallaAsignatura=mas1.id " + 
			"					inner join Malla as ma1 on mas1.idMalla=ma1.id " + 
			"					where mas1.estado='A' and aa.estado='A' and daa.estado='A'  and d.estado='A' and  dov.estado in ('D', 'A', 'V', 'P') " + 
			"					and dov.id=?3 and aa.multidocente=0 and ma1.estado='P' " + 
			"					and (d.id != ?4  or ?4 is null) " + 
			"					and aap.id=daa.idAsignaturaAprendizaje " + 
			"					and par.id=daa.idParalelo " + 
			"					group by aa.id ) " + 
			"and par.id not in ( select  daa.idParalelo " + 
			"					from DistributivoOfertaVersion as dov " + 
			"					inner join DistributivoDocente as d on dov.id=d.idDistributivoOfertaVersion  " + 
			"					inner join DocenteAsignaturaAprend as daa on d.id=daa.idDistributivoDocente  " + 
			"					inner join AsignaturaAprendizaje as aa on daa.idAsignaturaAprendizaje=aa.id " + 
			"					inner join MallaAsignatura as mas1 on aa.idMallaAsignatura=mas1.id " + 
			"					inner join Malla as ma1 on mas1.idMalla=ma1.id " + 
			"					where mas1.estado='A' and aa.estado='A' and daa.estado='A'  and d.estado='A' and  dov.estado in ('D', 'A', 'V', 'P') " + 
			"					and dov.id=?3 and aa.multidocente=0 and ma1.estado='P' " + 
			"					and (d.id != ?4  or ?4 is null) " + 
			"					and aap.id=daa.idAsignaturaAprendizaje " + 
			"					and par.id=daa.idParalelo " + 
			"					group by daa.idParalelo ) " + 
			" and pma.estado='A' and ma.estado='P' and mas.estado='A' and pp.estado='A' and aap.estado='A' " +  
			"and n.estado='A'  and par.estado='A' " +  
			"group by pma.idPeriodoAcademico,  mas.id , par.id , n.orden,par.descripcionCorta " + 
			"order by mas.id, par.id "
			
			)


	List<CustomObjectOfertaMallaAsigParalelo> listarOfertaMallaAsigParalelo(Integer idPeridoAcademico,Integer IdOferta, Integer idDistOfertaVersion,Integer idDistributivoDoc);	
	interface CustomObjectOfertaMallaAsigParalelo  { 
		Integer getIdMallaAsignatura();
		Integer getIdParalelo();
		String getParalelo();	
		String getFusion();
		Integer getIdMallaAsignaturaFusion();
		Integer getIdDetalleMallaAsignaturaFusion();
	}
	
	
	
	
	
	
	
	
	
	
	@Query(value=" SELECT pma.idPeriodoAcademico as idPeriodoAcademico, ma.id as idMalla, mas.id as idMallaAsignatura,ofe.id as idOfertaAsignatura,"+
			" ofe.descripcionCorta as ofertaAsignatura,aap.id as idAsignaturaAprendizaje, aap.idComponenteAprendizaje as idComponenteAprendizaje,"+
			" cap.codigo as codigo,cap.descripcion as descripcionCodigo, cap1.codigo as codigoComp, cap1.descripcion as descripcionCodigoComp,  "+
			"aap.valor as valor,asi.id as idAsignatura, par.id as idParalelo,CONCAT(cast(n.orden as text),'/',par.descripcionCorta) as paralelo,"+
			"asi.descripcion as asignatura, asi.descripcionCorta as asignaturaCorta, n.orden as nivel "+
			"FROM PeriodoMalla as pma " + 
			"INNER JOIN Malla as ma on pma.idMalla=ma.id " + 
			"INNER JOIN DepartamentoOferta as do on ma.idDepartamentoOferta=do.id "+
			"INNER JOIN MallaAsignatura as mas on ma.id=mas.idMalla " + 
			"INNER JOIN PlanificacionParalelo pp on mas.id=pp.idMallaAsignatura "+
			"INNER JOIN AsignaturaAprendizaje as aap on mas.id=aap.idMallaAsignatura " +
			"INNER JOIN ComponenteAprendizaje as cap on aap.idComponenteAprendizaje=cap.id "+
			"INNER JOIN ComponenteAprendizaje as cap1 on cap.idComponenteAprendizajePadre=cap1.id "+
			"INNER JOIN ReglamentoCompAprendizaje as rca on cap.id=rca.idCompAprendizaje "+
			"INNER JOIN Asignatura as asi on mas.idAsignatura=asi.id " + 
			"INNER JOIN OfertaAsignatura as oas on asi.id=oas.asignatura.id " + 
			"INNER JOIN Oferta as ofe on oas.oferta.id=ofe.id " + 
			"INNER JOIN Nivel as n on mas.idNivel=n.id "+
			"INNER JOIN  Paralelo as par on par.orden<= isnull (pp.numParalelos,0)  "+
			"WHERE pma.idPeriodoAcademico=(?1) and do.idOferta=(?2) and pp.idPeriodoAcademico=(?1) and "+
			" isnull (pp.numParalelos,0)>0 and aap.valor>0 " + 
			"and n.id>=ma.idNivelMinAperturado and n.id<=ma.idNivelMaxAperturado  " +
			"and aap.id not in ( select  aa.id " + 
								"from DistributivoOfertaVersion as dov " + 
								"inner join DistributivoDocente as d on dov.id=d.idDistributivoOfertaVersion " + 
								"inner join DocenteAsignaturaAprend as daa on d.id=daa.idDistributivoDocente " + 
								"inner join AsignaturaAprendizaje as aa on daa.idAsignaturaAprendizaje=aa.id " + 
								"inner join MallaAsignatura as ma on aa.idMallaAsignatura=ma.id " + 
								"inner join Malla as m on ma.idMalla=ma.id " + 
								"where m.estado='P' and ma.estado='A' and aa.estado='A' and daa.estado='A'  and d.estado='A' and  dov.estado in ('D', 'A', 'V','P') " + 
								"and dov.id=(?3) " + 
								"and (d.id != (?4)  or (?4) is null) " + 
								"and mas.id=ma " + 
								"and par.id=daa.idParalelo " + 
								"group by aa.id ) "+
			"and rca.asignarDocente=1 "+
			"and rca.estado='A' and pma.estado='A' and ma.estado='P' and mas.estado='A' and aap.estado='A' " + 
			"and asi.estado='A' and oas.estado='A' and ofe.estado='A' and n.estado='A'  and par.estado='A' "+
			"order by asi.descripcion, mas.id, par.id ")


	List<CustomObjectOfertaAsignaturaAprendizaje> listarOfertaAsignaturaAprendizaje(Integer idPeridoAcademico,Integer IdOferta, Integer idDistOfertaVersion,Integer idDistributivoDoc);	
	interface CustomObjectOfertaAsignaturaAprendizaje  { 
		Integer getIdPeriodoAcademico(); 
		Integer getIdMalla();
		Integer getIdMallaAsignatura();
		Integer getIdOfertaAsignatura();
		String getOfertaAsignatura();
		Integer getIdAsignaturaAprendizaje();
		Integer getIdComponenteAprendizaje();
		String getCodigo();
		String getDescripcionCodigo();
		String getCodigoComp();
		String getDescripcionCodigoComp();
		Integer getValor();
		Integer getIdAsignatura();
		Integer getIdParalelo();
		String getParalelo();
		String getAsignatura();
		String getAsignaturaCorta();
		Integer getNivel();
		
	}

	@Query(value="SELECT dov.id as idDistributivoOfertaVersion, ddo.idDocente as idDocente,ddo.id as idDistributivoDocente , "+
			"daa.id as idDocenteAsignaturaAprend, par.id as idParalelo,CONCAT(cast(niv.orden as text),'/',par.descripcionCorta)  as paralelo, "+
			"ofe.id as idOfertaAsignatura,ofe.descripcionCorta as ofertaAsignatura,daa.numEstudiantes as numEstudiantes,mas.id as idMallaAsignatura,asi.descripcion as asignatura, "+
			"asi.id as idAsignatura, aap.id as idAsignaturaAprendizaje,cap.id as idComponenteAprendizaje,cap1.codigo as codigoComp, "+
			"cap1.descripcion as descripcionCodigo, "+
			"aap.valor as valor,"+
			"daa.version as version ,dov.fechaDesde as fechaDesde, max (dov.versionDistributivoOferta) as versionDistributivoOferta "+
			"FROM PeriodoAcademico as per "+
			"INNER JOIN DistributivoGeneral as dg on per.id=dg.idPeriodoAcademico "+
			"INNER JOIN DistributivoGeneralVersion as dgv on dg.id=dgv.idDistributivoGeneral "+
			"INNER JOIN DistributivoOferta as do on dgv.id=do.idDistributivoGeneralVersion "+
			"INNER JOIN DistributivoOfertaVersion as dov on do.id=dov.idDistributivoOferta "+
			"INNER JOIN DistributivoDocente as ddo on dov.id=ddo.idDistributivoOfertaVersion "+
			"INNER JOIN DocenteAsignaturaAprend as daa  on ddo.id=daa.idDistributivoDocente "+
			"INNER JOIN Paralelo as par on daa.idParalelo=par.id "+
			"INNER JOIN AsignaturaAprendizaje as aap on daa.idAsignaturaAprendizaje=aap.id "+
			"INNER JOIN MallaAsignatura as mas on aap.idMallaAsignatura=mas.id "+
			"INNER JOIN Nivel as niv on mas.idNivel=niv.id "+
			"INNER JOIN Asignatura as asi on mas.idAsignatura=asi.id "+
			"INNER JOIN OfertaAsignatura as oas on asi.id=oas.asignatura.id "+
			"INNER JOIN Oferta as ofe on oas.oferta.id=ofe.id "+
			"INNER JOIN ComponenteAprendizaje as cap on aap.idComponenteAprendizaje=cap.id "+
			"INNER JOIN ComponenteAprendizaje as cap1 on cap.idComponenteAprendizajePadre=cap1.id " +
			"WHERE ddo.estado='A' and daa.estado='A' and par.estado='A' and aap.estado='A' and mas.estado='A' "+
			"and niv.estado='A' and asi.estado='A' and oas.estado='A' and ofe.estado='A' and cap.estado='A' and  dov.estado in ('D', 'A', 'V') "+
			"and ddo.idDocente=(?1) and dgv.id=(?3) and ddo.id not in ((?2))  "+
			"and  dov.fechaDesde>=(?4) and ( dov.fechaDesde <=(?5) OR (?5) is null ) "+
			"GROUP BY dov.id, ddo.idDocente,ddo.id,daa.id , par.id ,niv.orden ,par.descripcionCorta, " + 
			"ofe.id ,ofe.descripcionCorta, daa.numEstudiantes, mas.id,asi.descripcion , " + 
			"asi.id, aap.id ,cap.id ,cap1.codigo, cap1.descripcion, cap.codigo ,cap.descripcion, aap.valor ,daa.version,dov.fechaDesde "+
			"ORDER BY mas.id,par.id ")
	//Date fechaDesde,Date fechaHasta,
	List<CustomObjectDocenteAsignaturaAprendizajeOtroDist> listarDocenteAsigAprendOtroDistr(Integer idDocente, Integer IdDistributivoDoc,Integer IdDistributivoGeneralVer, java.sql.Date  fechaDesde, java.sql.Date fechaHasta );	
	interface CustomObjectDocenteAsignaturaAprendizajeOtroDist  { 
		Integer getIdDistributivoOfertaVersion();
		Integer getIdDocente();
		Integer getIdDistributivoDocente();
		Integer getIdDocenteAsignaturaAprend();
		Integer getIdParalelo();
		String getParalelo();
		Integer getIdOfertaAsignatura();
		String getOfertaAsignatura();
		Integer getNumEstudiantes();
		Integer getIdMallaAsignatura();
		String getAsignatura();
		Integer getIdAsignatura();
		Integer getIdAsignaturaAprendizaje();
		Integer getIdComponenteAprendizaje();
		String getCodigoComp();
		String getDescripcionCodigoComp();
		String getCodigo();
		String getDescripcionCodigo();
		Integer getValor();
		Integer getVersion();
		Date getFechaDesde();
		Integer getVersionDistributivoOferta();
	
	}
	
	

	
	@Query(value="SELECT dov.id as idDistributivoOfertaVersion,ddo.idDocente as idDocente,ddo.id as idDistributivoDocente,da.id as idDocenteActividad ,"+
			"da.idActividadDetalle as idActividadDetalle, da.valor as valor,da.version as version, max( dov.versionDistributivoOferta) as versionDistributivoOferta "+
			"FROM PeriodoAcademico as per "+
			"INNER JOIN DistributivoGeneral as dg on per.id=dg.idPeriodoAcademico "+
			"INNER JOIN DistributivoGeneralVersion as dgv on dg.id=dgv.idDistributivoGeneral "+
			"INNER JOIN DistributivoOferta as do on dgv.id=do.idDistributivoGeneralVersion "+
			"INNER JOIN DistributivoOfertaVersion as dov on do.id=dov.idDistributivoOferta "+
			"INNER JOIN DistributivoDocente as ddo on dov.id=ddo.idDistributivoOfertaVersion "+
			"INNER JOIN DocenteActividad as da  on ddo.id=da.idDistributivoDocente "+
			"WHERE per.estado='A' and dg.estado='A' and dgv.estado='A' and do.estado='A' and  dov.estado in ('D', 'A', 'V') and  ddo.estado='A' and da.estado='A' "+
			"and ddo.idDocente=(?1) and dgv.id=(?5)  and  ddo.id not in ((?2))    "+
			"and  ((ddo.fechaDesde>= (?3) AND ddo.fechaHasta <=  (?4)) "+
			"OR (ddo.fechaDesde <= (?3) AND ddo.fechaHasta >= (?3) AND ddo.fechaHasta<=  (?4) ) "+
			"OR (ddo.fechaDesde >= (?3) AND ddo.fechaDesde<=  (?4) AND ddo.fechaHasta >= (?4) ) ) "+
			"GROUP BY dov.id ,ddo.idDocente ,ddo.id ,da.id ,da.idActividadDetalle ,da.valor,da.version ")
	List<CustomObjectDistActnOtroDist> listarActividadOtroDistr(Integer idDocente, Integer IdDistributivoDoc,Date fechaDesde,Date fechaHasta,Integer IdDistributivoGeneralVer);	
	interface CustomObjectDistActnOtroDist  { 
		Integer getIdDistributivoOfertaVersion();
		Integer getIdDocente();
		Integer getIdDistributivoDocente();
		Integer getIdDocenteActividad();
		Integer getIdActividadDetalle();
		Integer getValor();
		Integer getVersion();
		Integer getVersionDistributivoOferta();
	}




	@Query(value="SELECT doc.id as idDocente,ddo.id as idDistributivoDocente, " + 
			"daa.id as idDocenteAsignaturaAprend, daa.idParalelo as idParalelo, " + 
			"aap.idMallaAsignatura as idMallaAsignatura,aap.id as idAsignaturaAprendizaje, " + 
			"cap.id as idComponenteAprendizaje, aap.valor as valor, daa.idEspacioFisico as idEspacioFisico " + 
			"FROM Docente as doc " + 
			"LEFT JOIN DistributivoDocente as ddo on doc.id=ddo.idDocente " + 
			"LEFT JOIN DocenteAsignaturaAprend as daa on ddo.id=daa.idDistributivoDocente " + 
			"LEFT JOIN AsignaturaAprendizaje as aap on daa.idAsignaturaAprendizaje=aap.id " + 
			"LEFT JOIN ComponenteAprendizaje as cap on aap.idComponenteAprendizaje=cap.id " + 
			"WHERE (doc.id=(?1)  AND  ddo.id=(?2)  and ddo.estado='A' and daa.estado='A' ) "+
			" or  (doc.id=(?1)  AND ddo.id is null)  and doc.estado='A'"	)
	List<CustomObjectDocenteAsignaturaAprendizaje> listarDocenteAsignaturaAprendizaje(Integer idDocente, Integer idDistributivoDocente);	
	interface CustomObjectDocenteAsignaturaAprendizaje  { 
		Integer getIdDocente();
		Integer getIdDistributivoDocente();
		Integer getIdDocenteAsignaturaAprend();
		Integer getIdParalelo();
		Integer getIdMallaAsignatura();
		Integer getIdAsignaturaAprendizaje();
		Integer getValor();
		Integer getIdEspacioFisico();
	
	}


	@Query(value = "select d.idd as idd, d.id_malla as idMalla, d.id_malla_asignatura as idMallaAsignatura, d.id_asignatura as idAsignatura,"+
			" d.asignatura as asignatura, d.codigo_comp as codigoComp, id_componente_aprendizaje as idComponenteAprendizaje, d.total_hora as totalHora " + 
			"from aca.fn_asignatura_componente_aprendizaje(:pi_id_malla_asignatura) as d", nativeQuery = true)
	public List<CustomObjectAsignaturaComponenteAprendizaje> getFnAsignaturaComponenteAprendizaje(@Param("pi_id_malla_asignatura") Integer pi_id_malla_asignatura);
	public interface CustomObjectAsignaturaComponenteAprendizaje{
		Integer getIdd();
		Integer getIdMalla();
		Integer getIdMallaAsignatura();
		Integer getIdAsignatura();
		String getAsignatura();
		String getCodigoComp();
		Integer getIdComponenteAprendizaje();
		String gettotalHora();


	}


	@Query(value = "select d.id_malla as idMalla, d.id_malla_asignatura as idMallaAsignatura, d.id_asignatura as idAsignatura,"+
			" d.asignatura as asignatura, d.id_pre as idPre,  d.id_vir as idVir, d.id_nap as idNap, d.id_apr as idApr, d.id_tau  as idTau, "+
			"d.pre, d.vir, d.nap, d.apr,d.tau " + 
			"from aca.fn_asignatura_componente_aprendizaje_pivot(:pi_id_malla_asignatura) as d", nativeQuery = true)
	public List<CustomObjectAsignaturaComponenteAprendizajePivot> getFnAsignaturaComponenteAprendizajePivot(@Param("pi_id_malla_asignatura") Integer pi_id_malla_asignatura);
	public interface CustomObjectAsignaturaComponenteAprendizajePivot{
		Integer getIdMalla();
		Integer getIdMallaAsignatura();
		Integer getIdAsignatura();
		String getAsignatura();
		Integer getIdPre();
		Integer getIdVir();
		Integer getIdNap();
		Integer getIdApr();
		Integer getIdTau();	
		Integer getPre();
		Integer getVir();
		Integer getNap();
		Integer getApr();
		Integer getTau();
	}

	///metodo para evaluacion docente
		@Query(value=" SELECT d "
				   + " FROM DocenteAsignaturaAprend  d "  
				   + " WHERE d.id = ?1  ")
		DocenteAsignaturaAprend findByIdDAAA(Integer id);
		
		
		

@Query(value=" SELECT aa.id as idAsignaturaAprendizaje,ma.id as idMallaAsignatura,p.id as idParalelo ,	" + 
		"case when n.orden is null then null else CONCAT(cast(n.orden as text),'/', p.descripcionCorta) end as paralelo, " + 
		"count(daa.idAsignaturaAprendizaje)	as cantidad,dov.id as idDistributivoOfertaVersion, dov.descripcion as descripcion " + 
		"from PeriodoMalla as pm " + 
		"inner join Malla as m on pm.idMalla=m.id " + 
		"inner join DepartamentoOferta as doo on doo.id=m.idDepartamentoOferta " + 
		"inner join MallaAsignatura as ma on m.id=ma.idMalla " + 
		"inner join Nivel as n on ma.idNivel= n.id " + 
		"inner join AsignaturaAprendizaje as aa on ma.id=aa.idMallaAsignatura " + 
		"inner join PlanificacionParalelo as pp on aa.idMallaAsignatura=pp.idMallaAsignatura " + 
		"										and pm.idPeriodoAcademico=pp.idPeriodoAcademico " + 	
		"inner join Paralelo as p on  p.orden<= isnull (pp.numParalelos,0) " +
		"inner join ComponenteAprendizaje as ca on aa.idComponenteAprendizaje=ca.id " + 
		"inner join ReglamentoCompAprendizaje as rc on ca.id=rc.idCompAprendizaje " +
		"inner join DocenteAsignaturaAprend as daa on aa.id=daa.idAsignaturaAprendizaje and daa.estado='A' and p.id=daa.idParalelo " +
		"inner join DistributivoDocente as dd on daa.idDistributivoDocente=dd.id and dd.estado='A' "+
		"inner join DistributivoOfertaVersion as dov on dd.idDistributivoOfertaVersion=dov.id and dov.estado in ('A','D','V','P') " + 
		"inner join DistributivoOferta as do on dov.idDistributivoOferta=do.id and do.estado='A' " + 
		"inner join DistributivoGeneralVersion as dgv on do.idDistributivoGeneralVersion=dgv.id and dgv.estado='A' " +
		"inner join DistributivoGeneral as dg on dgv.idDistributivoGeneral=dg.id and dg.idPeriodoAcademico=pm.idPeriodoAcademico and dg.estado='A' " + 
		"where pm.estado='A' and m.estado='P' and ma.estado='A' and aa.estado='A' and pp.estado='A' " +
		"and rc.asignarDocente=1 and pm.idPeriodoAcademico=?1 and doo.idOferta=?2 " + 
		"group by aa.id, p.id, ma.id, pm.idPeriodoAcademico, n.orden, p.descripcionCorta,dov.id, dov.descripcion "	+
		"order by dov.id")
List<CustomObjectAsigUt> listarAsigUtil(Integer idPeriodoAcademico, Integer idOferta);	
interface CustomObjectAsigUt  { 
	Integer getIdAsignaturaAprendizaje();
	Integer getIdMallaAsignatura(); 
	Integer getIdParalelo();
	String getParalelo();
	Integer getCantidad();
	Integer getIdDistributivoOfertaVersion();
	String getDescripcion();
}
	
}
