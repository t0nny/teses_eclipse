package ec.edu.upse.acad.model.pojo.evaluacion;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="eva", name="instrumento_pregunta")
@NoArgsConstructor
public class InstrumentoPregunta {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_instrumento_pregunta")
	@Getter @Setter private Integer id;
	
	//@Column(name="id_instrumento")
	//@Getter @Setter private Integer idInstrumento;
	
	//@Column(name="id_pregunta")
	//@Getter @Setter private Integer idPregunta;

	@Getter @Setter private String estado;
	
//	@Column(name="fecha_ingreso")
//	@Getter @Setter private Timestamp fechaIngreso;
	
	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;
	
	@Version
	@Getter @Setter private Integer version;
	
	//RELACIONES
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_instrumento", insertable=false, updatable = false)
	//@JsonIgnore
	@Getter @Setter private Instrumento instrumento;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_pregunta", insertable=false, updatable = false)
	//@JsonIgnore
	@Getter @Setter private Pregunta pregunta;
	
	@PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }
		

}
