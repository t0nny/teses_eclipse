package ec.edu.upse.acad.ws;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.upse.acad.model.pojo.Asignatura;
import ec.edu.upse.acad.model.pojo.OfertaAsignatura;
import ec.edu.upse.acad.model.repository.AsignaturaRepository;
import ec.edu.upse.acad.model.repository.OfertaAsignaturaRepository;
import ec.edu.upse.acad.model.repository.PeriodoDocenteAsignaturaRepository;
import ec.edu.upse.acad.model.service.AsignaturaService;

/* servicios de asignatura
 * servicios de oferta_asignatura
 * servicios de asignatura_relaciones
 * servicios de malla_general
 * servicios de tipo_componente_organizacion
 */

@RestController
@RequestMapping("/api/asignaturas")
@CrossOrigin
public class AsignaturaController {
	// private static final Integer CantidadProductos = null;
	@Autowired
	private OfertaAsignaturaRepository ofertaAsignaturaRepository;
	@Autowired
	private AsignaturaRepository asignaturaRepository;
	@Autowired
	private AsignaturaService asignaturaService;
	@Autowired
	private PeriodoDocenteAsignaturaRepository periodoDocenteAsignaturaRepository;

	// ****************** SERVICIOS PARA ASIGNATURAS************************//
	// Buscar todos las asignatura, para listar en angular

	@RequestMapping(value = "/buscarAsignaturas", method = RequestMethod.GET)
	public ResponseEntity<?> buscarAsignaturas() {
		return ResponseEntity.ok(asignaturaRepository.findAll());
	}

	@RequestMapping(value = "/buscarAsignaturaId/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarAsignaturaId(@PathVariable("id") Integer id) {
		Asignatura _asignatura;
		if (asignaturaRepository.findById(id).isPresent()) {
			_asignatura = asignaturaRepository.findById(id).get();
			return ResponseEntity.ok(_asignatura);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@RequestMapping(value = "/buscarAsignaturaVersion", method = RequestMethod.GET)
	public ResponseEntity<?> buscarAsignaturaVersion() {
		return ResponseEntity.ok(asignaturaRepository.findAll());
	}

	@RequestMapping(value = "/buscarAsignaturaVersionId/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarAsignaturaVersionId(@PathVariable("id") Integer id) {
		Asignatura _asignatura;
		if (asignaturaRepository.findById(id).isPresent()) {
			_asignatura = asignaturaRepository.findById(id).get();
			return ResponseEntity.ok(_asignatura);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	// Buscar todos las asignatura, para listar en angular
	@RequestMapping(value = "/buscarOfertaAsignatura", method = RequestMethod.GET)
	public ResponseEntity<?> buscarOfertaAsignatura() {
		return ResponseEntity.ok(ofertaAsignaturaRepository.findAll());
	}

	// buscar por id de OfertaAsignatura
	@RequestMapping(value = "/buscar/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> buscar(@PathVariable("id") Integer id) {
		OfertaAsignatura ofertaAsignatura;
		if (ofertaAsignaturaRepository.findById(id).isPresent()) {
			ofertaAsignatura = ofertaAsignaturaRepository.findById(id).get();
			return ResponseEntity.ok(ofertaAsignatura);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@RequestMapping(value = "/buscaDocenteAsignatura/{idPeriodo}/{asignatura}", method = RequestMethod.GET)
	public ResponseEntity<?> buscaDocenteAsignatura(@PathVariable("idPeriodo") Integer idPeriodo, @PathVariable("asignatura") String asignatura) {
		return ResponseEntity.ok(asignaturaRepository.rptDocenteAsignaturaPeriodoDesde(idPeriodo, asignatura));

	}

	// busca docente por periodo
	@RequestMapping(value = "/buscaDocenteByPeriodoAndDescripcion/{asignatura}", method = RequestMethod.GET)
	public ResponseEntity<?> buscaDocenteByPeriodoAndDescripcion(@PathVariable("asignatura") String asignatura) {
		if (asignatura.equals("0")) {
			asignatura = " ";
		}
		System.out.println(periodoDocenteAsignaturaRepository.buscarPeriodoDocente(asignatura));
		return ResponseEntity.ok(periodoDocenteAsignaturaRepository.buscarPeriodoDocente(asignatura));
	}

	@RequestMapping(value = "/buscaDocenteByPeriodoAndAsignatura/{idPeriodo}/{asignatura}", method = RequestMethod.GET)
	public ResponseEntity<?> buscaDocenteByPeriodoAndAsignatura(@PathVariable("idPeriodo") Integer idPeriodo,
			@PathVariable("asignatura") String asignatura) {
		if (asignatura.equals("0")) {
			asignatura = " ";
		}
		return ResponseEntity.ok(periodoDocenteAsignaturaRepository.buscarPeriodoAsignaturaDocente(idPeriodo, asignatura));
	}

	// -----------------------------------------------------------------------------------------------------------//
	// ----------------- INICIO SERVICIOS PARA EL MODULO Asignaturas EN ANGULAR
	// -----------------//
	// -----------------------------------------------------------------------------------------------------------//

	// ********************************* Servicios para el componente oferta-asignatura *********************************
	/**
	 * Devuelve una lista de todas las asignaturas que pertenecen al menos a una
	 * oferta vigente.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/buscarOfertaAsignaturas/{idOferta}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarOfertaAsignaturas(@PathVariable(value = "idOferta") Integer idOferta) {
		return ResponseEntity.ok(asignaturaService.buscarOfertaAsignaturas(idOferta));// consulta revisada
	}

	// comentariodsssss
	// ********************************* Servicios para el componente
	// nueva-asignatura *********************************
	/**
	 * Buscar el objeto Asignatura de acuerdo a un identificador => idAsignatura
	 * 
	 * @param idAsignatura Identificador para poder filtrar
	 * @return
	 */
	@RequestMapping(value = "/buscarAsignatura/{idAsignatura}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarAsignatura(@PathVariable("idAsignatura") Integer idAsignatura) {
		return ResponseEntity.ok(asignaturaRepository.buscarAsignatura(idAsignatura));// consulta revisada
	}

	// ********************************* Servicios para el componente
	// asignatura-vicerrectorado *********************************
	/**
	 * Devuelve una lista de todas las asignaturas que pertenecen al menos a una
	 * oferta vigente.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/buscarOfertaAsignaturasComuns", method = RequestMethod.GET)
	public ResponseEntity<?> buscarOfertaAsignaturasComuns() {
		return ResponseEntity.ok(asignaturaService.buscarOfertaAsignaturasComun());// consulta revisada
	}

	// ********************************* Servicios para el componente
	// nueva-asignatura-comun *********************************
	/**
	 * Busca una asignatura en especifico para la edición de una asignatura común
	 * 
	 * @param id Identificador para poder filtrar
	 * @return
	 */
	@RequestMapping(value = "/buscaAsignaturaById/{id}", method = RequestMethod.GET) // corregir
	public ResponseEntity<?> buscaAsignaturaById(@PathVariable("id") Integer id) {
		Asignatura asignatura = asignaturaService.filterFindAsignaturaById(id);
//			Asignatura _asignatura = new Asignatura();
//			BeanUtils.copyProperties(asignatura, _asignatura,"mallaAsignaturas","compatibilidadAsignaturas2","compatibilidadAsignaturas1");
		return ResponseEntity.ok(asignatura);
	}

	// ********************************* Servicios en comun entre todos los
	// componentes del modulo de Asignatura *********************************
	/**
	 * Consulta para verificar si la asignatura esta publicada en una Malla =>
	 * 'mallaversion'
	 * 
	 * @param idAsignatura Identificador para poder filtrar
	 * @return
	 */
	@RequestMapping(value = "/consultaAsignaturaPublicada/{idAsignatura}", method = RequestMethod.GET)
	public ResponseEntity<?> consultaAsignaturaPublicada(@PathVariable("idAsignatura") Integer idAsignatura) {
		{
			return ResponseEntity.ok(asignaturaRepository.consultarAsignaturaPublicada(idAsignatura));
		}
	}

	/**
	 * Cambio de estado de 'A' => 'I' en la entidad OfertaAsignatura de acuerdo al
	 * identificador que se recibe como parámetro
	 * 
	 * @param id Identificador (idAsignatura) para poder realizar el borrado
	 * @return
	 */
	@RequestMapping(value = "/borrarAsignatura/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> borrarAsignatura(@PathVariable("id") Integer id) {
		asignaturaService.borraAsignatura(id);
		return ResponseEntity.ok().build();
	}

	// Vicerrector Academico
	/**
	 * listar las carreras de acuerdo a la facultad a la que pertenece
	 * @return
	 */
	@RequestMapping(value = "/listarOfertas/{idFacultad}", method = RequestMethod.GET)
	public ResponseEntity<?> listarOfertas(@PathVariable("idFacultad") Integer idFacultad) {
		return ResponseEntity.ok(asignaturaRepository.listarOfertasPorFacultad(idFacultad));
	}

	@RequestMapping(value = "/listarCarreras", method = RequestMethod.GET)
	public ResponseEntity<?> listarCarreras() {
		return ResponseEntity.ok(asignaturaRepository.listarCarreras());
	}

	/**
	 * Graba o edita una asignatura
	 * 
	 * @param asignatura recibe un objeto de tipo Asignatura
	 * @return
	 */
	@RequestMapping(value = "/grabarAsignatura", method = RequestMethod.POST)
	public ResponseEntity<?> grabarAsignatura(@RequestBody Asignatura asignatura) {
		asignaturaService.grabaAsignaturaOferta(asignatura);
		return ResponseEntity.ok(asignatura);
	}

	@RequestMapping(value = "/grabarAsignaturaV2", method = RequestMethod.POST)
	public ResponseEntity<?> grabarAsignaturaV2(@RequestBody Asignatura asignatura) {
		asignaturaService.grabaAsignatura(asignatura);
		return ResponseEntity.ok(asignatura);
	}

	/**
	 * Busca una asignatura por el nombre y la carrera a la que pertenece.
	 * 
	 * @param descripcion Nombre de la asignatura
	 * @param idOferta    Identificador de la oferta
	 * @return
	 */
	@RequestMapping(value = "/buscarDescripcionAsignatura/{descripcion}/{idOferta}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarDescripcionAsignatura(@PathVariable("descripcion") String descripcion,
			@PathVariable("idOferta") Integer idOferta) {
		return ResponseEntity.ok(asignaturaRepository.buscarDescripcionAsignatura(descripcion, idOferta));
	}

	/**
	 * Busca una asignatura por su código
	 * 
	 * @param codigo de la asignatura a buscar.
	 * @return
	 */
	@RequestMapping(value = "/buscarCodigoAsignatura/{codigo}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarCodigoAsignatura(@PathVariable("codigo") String codigo) {
		return ResponseEntity.ok(asignaturaRepository.buscarCodigoAsignatura(codigo));
	}

	@RequestMapping(value = "/buscarOfertaById/{idOferta}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarOfertaById(@PathVariable("idOferta") Integer idOferta) {
		return ResponseEntity.ok(asignaturaRepository.buscarOfertaById(idOferta));
	}
	/**
	 * Servicio para validar que la asignatura comun esta siendo usada en una malla
	 * **/
	@RequestMapping(value="/listaAsignaturasComunUtilizadaMalla/{idAsignatura}", method=RequestMethod.GET)
	public ResponseEntity<?> listaAsignaturasComunUtilizadaMalla(@PathVariable("idAsignatura") Integer idAsignatura)
	{
		return ResponseEntity.ok(asignaturaRepository.listaAsignaturasComunUtilizadaMalla(idAsignatura));
	}
	
	// -----------------------------------------------------------------------------------------------------------//
	// ----------------- FIN SERVICIOS PARA EL MODULO Asignaturas EN ANGULAR -------------------------------------//
	// -----------------------------------------------------------------------------------------------------------//
}