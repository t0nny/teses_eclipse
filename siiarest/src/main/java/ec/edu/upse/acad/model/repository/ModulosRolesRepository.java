package ec.edu.upse.acad.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.seguridad.ModuloRol;



@Repository

public interface ModulosRolesRepository 
       extends JpaRepository<ModuloRol, Integer>{
	
	@Query(value="SELECT mr.id as id,  r.nombre as nombre, m.nombre as modulo, m.id as modulo_id "
			+ "FROM ModuloRol AS mr "
			+ "INNER JOIN Rol r ON mr.rol.id=r.id "
			+ "INNER JOIN Modulo m ON mr.modulo.id=m.id ")
	 List<CustomObject> listaRolesModulos();
	
	
	@Query(value="SELECT mr.id as id,  r.nombre as nombre, m.nombre as modulo, m.id as modulo_id, r.id as rol_id, mr.estado as estado, mr.version as version "
			+ "FROM ModuloRol AS mr "
			+ "INNER JOIN Rol r ON mr.rol.id=r.id "
			+ "INNER JOIN Modulo m ON mr.modulo.id=m.id "
			+ "WHERE mr.modulo.id=?1 ")
	 List<CustomObject> listaRolesModulos(Integer modulo_id);
}



interface CustomObject { 
    Integer getId(); 
    String getNombre(); 
    String getModulo(); 
    String getModulo_id(); 
    String getRol_id();
    String getEstado();
    Integer getVersion();
} 
