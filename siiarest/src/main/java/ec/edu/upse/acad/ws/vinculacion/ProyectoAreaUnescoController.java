package ec.edu.upse.acad.ws.vinculacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.upse.acad.model.repository.vinculacion.ProyectoAreaUnescoRepository;
import ec.edu.upse.acad.model.service.SecurityService;

@RestController
@RequestMapping("/api/areaUnesco")
@CrossOrigin
public class ProyectoAreaUnescoController {
	
	@Autowired
	private SecurityService securityService;
	@Autowired
	private ProyectoAreaUnescoRepository areaUnescoRepository;
	
	// buscar solo activos
		@RequestMapping(value = "/buscarListaAreaUnesco", method = RequestMethod.GET)
		public ResponseEntity<?> buscarListaAreaGeografica(@RequestHeader(value = "Authorization") String authorization) {
			if (!securityService.isTokenValido(authorization)) {
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
			}
			return ResponseEntity.ok(areaUnescoRepository.buscarListaAreaUnesco());
		}

		// buscar por el idPadre de areaGeografica
		@RequestMapping(value = "/ListarPorIdAreaUnescoPadre/{id}", method = RequestMethod.GET)
		public ResponseEntity<?> ListarPorIdAreaGeoPadre(@RequestHeader(value = "Authorization") String authorization,
				@PathVariable("id") Integer id) {
			if (!securityService.isTokenValido(authorization)) {
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
			}
			return ResponseEntity.ok(areaUnescoRepository.ListarPorIdAreaUnescoPadre(id).get());
		}


}
