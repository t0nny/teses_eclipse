package ec.edu.upse.acad.ws;

import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.upse.acad.model.pojo.MallaAsignatura;
import ec.edu.upse.acad.model.pojo.PlanificacionParalelo;
import ec.edu.upse.acad.model.pojo.distributivo.DistributivoGeneral;
import ec.edu.upse.acad.model.pojo.distributivo.DistributivoOfertaVersion;
import ec.edu.upse.acad.model.repository.ParaleloRepository;
import ec.edu.upse.acad.model.repository.PeriodoAcademicoRepository;
import ec.edu.upse.acad.model.repository.distributivo.DistributivoGeneralRepository;
import ec.edu.upse.acad.model.repository.distributivo.DistributivoGeneralVersionRepository;
import ec.edu.upse.acad.model.repository.distributivo.DistributivoOfertaRepository;
import ec.edu.upse.acad.model.repository.distributivo.DistributivoOfertaVersionRepository;
import ec.edu.upse.acad.model.service.DistributivoDocenteService;
import ec.edu.upse.acad.model.service.SecurityService;

@RestController
@RequestMapping("/api/distributivoGeneral")
@CrossOrigin
public class DistributivoGeneralController {

	@Autowired private SecurityService securityService;
	@Autowired private DistributivoGeneralRepository distributivoGeneralRepository;
	@Autowired private DistributivoGeneralVersionRepository distributivoGeneralVersionRepository;
	@Autowired private DistributivoOfertaRepository distributivoOfertaRepository;
	@Autowired private DistributivoOfertaVersionRepository distributivoOfertaVersionRepository;
	@Autowired private PeriodoAcademicoRepository periodoAcademicoRepository;
	@Autowired private ParaleloRepository paraleloRepository;
	@Autowired private DistributivoDocenteService  distributivoDocenteService;


	//****************** SERVICIOS PARA DISTRIBUTIVO GENERAL************************//

	/**
	 * lista los distributivos generales por carrera
	 * @param Authorization
	 * @return
	 */
	@RequestMapping(value="/listarDistributivosGenerales", method=RequestMethod.GET)
	public ResponseEntity<?> listarDistributivosGenerales(@RequestHeader(value="Authorization") String Authorization) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(distributivoGeneralRepository.findAll());
	}


	/**
	 * permite obtener el ultimo periodo academico vigente
	 * @param Authorization
	 * @return
	 */
	@RequestMapping(value="/recuperarUltimoPeriodoVigente", method=RequestMethod.GET)
	public ResponseEntity<?> recuperarUltimoPeriodoVigente(@RequestHeader(value="Authorization") String Authorization) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(periodoAcademicoRepository.recuperarUltimoPeriodoVigente());
	}


	/**
	 * recupera nombre del distributivo general
	 * @param authorization
	 * @return
	 */
	@RequestMapping(value="/recuperarDistributivoGeneral", method=RequestMethod.GET)
	public ResponseEntity<?> recuperarDistributivoGeneral(@RequestHeader(value="Authorization") String authorization) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(distributivoGeneralRepository.listarDistributivoGeneral());
	}


	/**
	 * Buscar todos los distributivo general, para listar en angular
	 * @param Authorization
	 * @return
	 */
	@RequestMapping(value="/buscarDistributivoGeneral", method=RequestMethod.GET)
	public ResponseEntity<?> buscarDistributivoGeneral(@RequestHeader(value="Authorization") String Authorization) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(distributivoGeneralRepository.findAll());
	}

	/***
	 * 	servicio que crea o actualiza un distributivo general	
	 * @param authorization
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/buscarDistributivoGeneralId/{id}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarDistributivoGeneralId(@RequestHeader(value="Authorization") String authorization, 
			@PathVariable("id") Integer id) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}

		DistributivoGeneral distributivosGeneralsig;
		if (distributivoGeneralRepository.findById(id).isPresent()) {
			distributivosGeneralsig = distributivoGeneralRepository.findById(id).get();
			return ResponseEntity.ok(distributivosGeneralsig);
		}else {
			return ResponseEntity.notFound().build();
		}
	}

	/**
	 * servicio para borrar un distributivo actividad
	 * @param Authorization
	 * @param idDistributivoActividad
	 * @param parametro
	 * @return
	 */
	//
	@RequestMapping(value="/borrarDistributivoActividades/{idDistributivoActividad}/{parametro}", method=RequestMethod.DELETE)
	public ResponseEntity<?> borrarPreCorrequisito(@RequestHeader(value="Authorization") String Authorization, 
			@PathVariable("idDistributivoActividad") Integer idDistributivoActividad,@PathVariable("parametro") Integer parametro) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		//	eliminaDistributivoActividades.borrarDistributivoActividades(idDistributivoActividad,parametro);
		return ResponseEntity.ok().build();
	}

	/**
	 * Buscar todas los paralelos, para listar en angular
	 * @param Authorization
	 * @return
	 */
	@RequestMapping(value="/listaParalelo", method=RequestMethod.GET)
	public ResponseEntity<?> buscarParalelo(@RequestHeader(value="Authorization") String Authorization) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(paraleloRepository.listaParalelo());
	}
	/***
	 * replica el distributivo oferta version  filtrando por los siguuientes parametros
	 * @param authorization
	 * @param idOferta
	 * @param idDistributivo
	 * @param idPeriodoDuplicar
	 * @return
	 */
	//falta verificar
	@RequestMapping(value="/replicaDistributivoVersion/{idOferta}/{idDistributivo}/{idPeriodoDuplicar}", method=RequestMethod.GET)
	public ResponseEntity<?> replicaDistributivoVersion(@PathVariable("idOferta") Integer idOferta, @PathVariable("idDistributivo") Integer idDistributivo, 
			@PathVariable("idPeriodoDuplicar") Integer idPeriodoDuplicar) 
	{
		
		return ResponseEntity.ok(distributivoOfertaRepository.replicaDisributivo(idOferta,idDistributivo,idPeriodoDuplicar));
	}
	
	
	@RequestMapping(value="/cargarPeriodoVigente", method=RequestMethod.GET)
	public ResponseEntity<?> cargarPeriodoVigente(@RequestHeader(value="Authorization") String authorization) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(distributivoGeneralRepository.listaPeriodoVigent());
	}

	/***
	 * obtiene los periodo academicos por aÃ±o
	 * @param authorization
	 * @return
	 */
	
	
	@RequestMapping(value="/periodoDistributivo/{idDistributivoOfert}", method=RequestMethod.GET)
	public ResponseEntity<?> periodoDistributivo(@RequestHeader(value="Authorization") String authorization,
			@PathVariable("idDistributivoOfert") Integer idDistributivoOfert) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(distributivoGeneralRepository.periodoDistributivo(idDistributivoOfert));
	}
	@RequestMapping(value="/periodoValidoCrearDistributivo/{idDistributivoOferta}", method=RequestMethod.GET)
	public ResponseEntity<?> periodoValidoCrearDistributivo(@RequestHeader(value="Authorization") String authorization,
			@PathVariable("idDistributivoOferta") Integer idDistributivoOferta) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(distributivoGeneralRepository.periodoValidoCrearDistributivo(idDistributivoOferta));
	}
	
	
	
	@RequestMapping(value="/cargarPeriodosVigentes/{idOferta}", method=RequestMethod.GET)
	public ResponseEntity<?> cargarPeriodosVigentes(@RequestHeader(value="Authorization") String authorization,
			@PathVariable("idOferta") Integer idOferta) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(distributivoGeneralRepository.listaPeriodosVigentes(idOferta));
	}
	
	@RequestMapping(value="/cargarPeriodosVigentesFuturos/{idOferta}", method=RequestMethod.GET)
	public ResponseEntity<?> cargarPeriodosVigentesFuturos(@RequestHeader(value="Authorization") String authorization,
			@PathVariable("idOferta") Integer idOferta) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(distributivoGeneralRepository.listaPeriodosVigenteFuturo(idOferta));
	}
//	@RequestMapping(value="/cargaUltimoDistVigente", method=RequestMethod.GET)
//	public ResponseEntity<?> cargaUltimoDistVigente(@RequestHeader(value="Authorization") String authorization) {
//		if (!securityService.isTokenValido(authorization)) {
//			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
//		}
//		return ResponseEntity.ok(distributivoGeneralRepository.listaPeriodosVigentes());
//	}

	/**
	 * 	graba la edicion del distributivo oferta version	
	 * @param Authorization
	 * @param editaDistributivoOfertaVersion
	 * @return
	 */
	//proceso de grabado de la tabla distributivo oferta version
	@RequestMapping(value="/grabarEdicionDistributivoOfertaVersion", method=RequestMethod.POST)
	public ResponseEntity<?> grabarEdicionDistributivoOfertaVersion(@RequestHeader(value="Authorization") String Authorization, //definimos que tenga autorizacion
			@RequestBody DistributivoOfertaVersion editaDistributivoOfertaVersion) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		DistributivoOfertaVersion _editaDistributivoOfertaVersion = new DistributivoOfertaVersion();
		if (editaDistributivoOfertaVersion.getId() != null) {
			_editaDistributivoOfertaVersion = distributivoOfertaVersionRepository.findById(editaDistributivoOfertaVersion.getId()).get();
		}
	//	JSONObject json=new JSONObject(_editaDistributivoOfertaVersion);
		//System.out.println(json);
		System.out.println(_editaDistributivoOfertaVersion.getId());
		BeanUtils.copyProperties(editaDistributivoOfertaVersion, _editaDistributivoOfertaVersion, "distributivoDocentes");
		distributivoOfertaVersionRepository.save(_editaDistributivoOfertaVersion);
		return ResponseEntity.ok(_editaDistributivoOfertaVersion);
	}
//	
//	@RequestMapping(value="/grabarNUevoDistributivoOfertaVersion", method=RequestMethod.POST)
//	public ResponseEntity<?> grabarNUevoDistributivoOfertaVersion(@RequestHeader(value="Authorization") String Authorization, //definimos que tenga autorizacion
//			@RequestBody DistributivoOfertaVersion distributivoOfertaVersion) {
//		if (!securityService.isTokenValido(Authorization)) {
//			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
//		}
//		JSONObject json=new JSONObject(_editaDistributivoOfertaVersion);
//		System.out.println(json);
//		System.out.println(_editaDistributivoOfertaVersion.getId());
//		BeanUtils.copyProperties(editaDistributivoOfertaVersion, _editaDistributivoOfertaVersion, "distr
//		distributivoOfertaVersionRepository.save(distributivoOfertaVersion );
//		return ResponseEntity.ok(distributivoOfertaVersion);
//	}
	@RequestMapping(value="/grabarElimancionDistributivoOfertaVersion", method=RequestMethod.POST)
	public ResponseEntity<?> grabarElimancionDistributivoOfertaVersion(@RequestHeader(value="Authorization") String Authorization, //definimos que tenga autorizacion
			@RequestBody DistributivoOfertaVersion distributivoOfertaVersion) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		distributivoOfertaVersionRepository.save(distributivoOfertaVersion);
		return ResponseEntity.ok(distributivoOfertaVersion);
	}
	/*Buscar en distributivogeneralversion por id distributivo general*/
	
	@RequestMapping(value="/buscaDistributivoGeneralVersion/{iddistgeneral}", method=RequestMethod.GET)
	public ResponseEntity<?> buscaDistributivoGeneralVersion(@RequestHeader(value="Authorization") String authorization,
			@PathVariable("iddistgeneral") Integer iddistgeneral) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		
		return ResponseEntity.ok(distributivoGeneralVersionRepository.buscarDistGenelVersion(iddistgeneral));
	}
	
	
	@RequestMapping(value="/buscaAsigParaleloUtil/{idPeriodoAcademico}/{idOferta}", method=RequestMethod.GET)
	public ResponseEntity<?> buscaAsigParaleloUtil(@RequestHeader(value="Authorization") String authorization,
			@PathVariable("idPeriodoAcademico") Integer idPeriodoAcademico,@PathVariable("idOferta") Integer idOferta) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		
		return ResponseEntity.ok(distributivoGeneralVersionRepository.buscarParaleloUtil(idPeriodoAcademico,idOferta));
	}

	//****************** FIN DE SERVICIOS PARA DISTRIBUTIVO GENERAL ************************//

	//****************** SERVICIOS PARA CONFIGURAR PARALELO************************//	

	/***
	 * 	recupera las asignatura y paralelo dependiendo del nivel max y min aperturado en la malla		
	 * @param authorization
	 * @param idOferta
	 * @return
	 */
	//Servicio para filtrar por la mallaasignatura por oferta//
	//
	@RequestMapping(value="/recuperarConfiguracionParalelo/{idPeriodo}/{idOferta}",method=RequestMethod.GET)
	public ResponseEntity<?> recuperarConfiguracionParalelo(@RequestHeader(value="Authorization") String authorization,
			@PathVariable("idPeriodo") Integer idPeriodo,@PathVariable("idOferta") Integer idOferta) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(distributivoGeneralRepository.recuperarConfiguracionParalelo(idPeriodo,idOferta));
	}
	
	@RequestMapping(value="/grabarPlanificacionParalelo", method=RequestMethod.POST)
	public ResponseEntity<?> grabarPlanificacionParalelo(@RequestHeader(value="Authorization") String Authorization, //definimos que tenga autorizacion
			@RequestBody List<PlanificacionParalelo> planificacionParalelo) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		distributivoDocenteService.grabaPlanificacionParaleloService(planificacionParalelo);;

		return ResponseEntity.ok().build();
	}

	/****
	 * servicio que actualiza la malla asignatura										
	 * @param Authorization
	 * @param mallaasign
	 * @return
	 */
	@RequestMapping(value="/grabarMallaAsignatura", method=RequestMethod.POST)
	public ResponseEntity<?> grabarMallaVersion(@RequestHeader(value="Authorization") String Authorization, //definimos que tenga autorizacion
			@RequestBody MallaAsignatura mallaasign) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		distributivoDocenteService.grabaMallaAsignaturaService(mallaasign);

		return ResponseEntity.ok().build();
	}

	//****************** FIN DE SERVICIOS PARA CONFIGURAR PARALELO ************************//

}
