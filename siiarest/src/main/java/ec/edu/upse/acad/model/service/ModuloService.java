package ec.edu.upse.acad.model.service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.upse.acad.model.pojo.seguridad.ModuloRol;
import ec.edu.upse.acad.model.pojo.seguridad.ModuloRolOpcion;
import ec.edu.upse.acad.model.pojo.seguridad.RolDepartamento;
import ec.edu.upse.acad.model.pojo.seguridad.Subrol;
import ec.edu.upse.acad.model.repository.ModulosRolesOpcionesRepository;
import ec.edu.upse.acad.model.repository.ModulosRolesRepository;
import ec.edu.upse.acad.model.repository.RolesDepartamentosRepository;
import ec.edu.upse.acad.model.repository.SubrolesRepository;

@Service
@Transactional

public class ModuloService {
	@Autowired private ModulosRolesRepository modulosRolesRepository;
	@Autowired private ModulosRolesOpcionesRepository modulosRolesOpcionesRepository;
	@Autowired private RolesDepartamentosRepository rolesDepartamentosRepository;
	@Autowired private SubrolesRepository subrolrepository;
	
	@PersistenceContext
	private EntityManager em;
	
public void grabarModuloRol(ModuloRol moduloRol) {
		
		ModuloRol _moduloRol=new ModuloRol();
		
		if(moduloRol.getId()!=null) {
			_moduloRol = modulosRolesRepository.findById(moduloRol.getId()).get();
		}
		BeanUtils.copyProperties(moduloRol, _moduloRol);
		modulosRolesRepository.save(_moduloRol);
		
	}


	public void grabarModuloRolOpcion(ModuloRolOpcion moduloRolOpcion) {
		
		ModuloRolOpcion _moduloRolOpcion=new ModuloRolOpcion();
		if(moduloRolOpcion.getId()!=null) {
			//update
			_moduloRolOpcion = modulosRolesOpcionesRepository.findById(moduloRolOpcion.getId()).get();
		}
		BeanUtils.copyProperties(moduloRolOpcion, _moduloRolOpcion);
		modulosRolesOpcionesRepository.save(_moduloRolOpcion);
	}
	
	public void grabarRolDepartamento(RolDepartamento rolDepartamento) {
		
		RolDepartamento _rolDepartamento=new RolDepartamento();
		if(rolDepartamento.getId()!=null) {
			//update
			_rolDepartamento = rolesDepartamentosRepository.findById(rolDepartamento.getId()).get();
		}
		BeanUtils.copyProperties(rolDepartamento, _rolDepartamento);
		rolesDepartamentosRepository.save(_rolDepartamento);
	}
	
	public void grabarSubrol(Subrol subrol) {
		
		Subrol _subrol=new Subrol();
		if(subrol.getId()!=null) {
			//update
			_subrol = subrolrepository.findById(subrol.getId()).get();
		}
		BeanUtils.copyProperties(subrol, _subrol);
		subrolrepository.save(_subrol);
	}


	
	
}