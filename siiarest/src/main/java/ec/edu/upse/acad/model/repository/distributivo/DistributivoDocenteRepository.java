package ec.edu.upse.acad.model.repository.distributivo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.distributivo.DistributivoDocente;

@Repository
public interface DistributivoDocenteRepository extends JpaRepository<DistributivoDocente, Integer>  {

	

	
	/****
	 * recupera de un distributivo oferta version un docente con los datos del detalle  horas y dedicacion
	 * filtrando por el id del  docente
	 * @param authorization
	 * @param idDocente
	 * @return
	 */
	
	@Query(value=" select distinct d.id as idDocente,ddo.id as idDistributivoDocente, CONCAT(p.apellidos ,' ',p.nombres) as nombresCompletos, " + 
			" ded.descripcion as dedicacion, isnull (sum( aa.valor), 0) as horas " +
			" from Persona p inner join Docente d " + 
			" on p.id = d.idPersona inner join DistributivoDocente ddo " + 
			" on ddo.idDocente = d.id inner join DistributivoDedicacion dde " + 
			" on dde.idDistributivoDocente= ddo.id inner join DocenteDedicacion ded " + 
			" on dde.idDocenteDedicacion = ded.id inner join DocenteAsignaturaAprend daa " + 
			" on dde.idDistributivoDocente = daa.idDistributivoDocente inner join AsignaturaAprendizaje aa " + 
			" on daa.idAsignaturaAprendizaje = aa.id inner join MallaAsignatura ma " + 
			" on aa.idMallaAsignatura = ma.id inner join Asignatura a " + 
			" on ma.idAsignatura = a.id inner join Paralelo pl " + 
			" on daa.idParalelo = pl.id inner join ComponenteAprendizaje ca " + 
			" on aa.idComponenteAprendizaje = ca.id " + 
			" where d.id = (?1) and ddo.estado = 'A' and dde.estado = 'A' and daa.estado = 'A' " +
			" group by d.id,ddo.id, p.apellidos, p.nombres,ded.descripcion"
			)
	List<CustomObjectLD> recuperarDocenteDedicacion(Integer idDocente);


	interface CustomObjectLD{ 
	    Integer getIdDocente(); 
	    Integer getIdDistributivoDocente(); 
	    String getNombresCompletos(); 
	    String getDedicacion(); 
	    Double getHoras(); 
	} 


	@Query(value=" select  distinct d.id as idDocente, a.id as idAsignatura,a.descripcion as asignatura,"+
			" CONCAT(cast(n.orden as text),'/',pl.descripcionCorta) as paralelo, daa.numEstudiantes as numEstudiantes, " +
			" sum(case when ca1.codigo  = 'DOC'  then aa.valor else 0 end) as docencia, " + 
			" sum(case when ca1.codigo  = 'PAE'  then aa.valor else 0 end) as practicas, " + 
			" sum((case when ca1.codigo  = 'DOC'  then aa.valor else 0 end)  + sum(case when ca1.codigo  = 'PAE'  then aa.valor else 0 end)) as horasClase " + 
//			" sum(case when ca.codigoComp  = 'DOC' and (ca.codigo='PRE' or ca.codigo='VIR') and aa.aplicaDistributivo= 'True' then " + 
//			" aa.valor else 0 end) as docencia, " + 
//			" sum(case when ca.codigoComp  = 'PAE' and ca.codigo='APR' and aa.aplicaDistributivo= 'True' then " + 
//			" aa.valor else 0 end) as practicas,"+
//			" sum((case when ca.codigoComp  = 'DOC' and (ca.codigo='PRE' or ca.codigo='VIR')  and aa.aplicaDistributivo= 'True' then " + 
//			" aa.valor else 0 end)+(case when ca.codigoComp  = 'PAE' and ca.codigo='APR' and aa.aplicaDistributivo= 'True' then " + 
//			" aa.valor else 0 end)) as horasClase " + 
			" from Docente d "+
			"inner join DistributivoDocente ddo  on ddo.idDocente = d.id "+
			"inner join DistributivoDedicacion dde on dde.idDistributivoDocente= ddo.id "+
			"inner join DocenteDedicacion ded  on dde.idDocenteDedicacion = ded.id "+
			"inner join DocenteAsignaturaAprend daa  on dde.idDistributivoDocente = daa.idDistributivoDocente "+
			"inner join AsignaturaAprendizaje aa  on daa.idAsignaturaAprendizaje = aa.id "+
			"inner join MallaAsignatura ma  on aa.idMallaAsignatura = ma.id "+
			"inner join Asignatura a  on ma.idAsignatura = a.id "+
			"inner join Paralelo pl  on daa.idParalelo = pl.id "+
			"inner join ComponenteAprendizaje ca  on aa.idComponenteAprendizaje = ca.id "+
			"inner join ComponenteAprendizaje ca1  on ca.idComponenteAprendizajePadre = ca1.id "+
			" inner join Nivel n  on ma.idNivel = n.id " + 
			" where ddo.idDistributivoOfertaVersion=(?1) and ddo.estado = 'A' and dde.estado = 'A' and daa.estado = 'A'   " +
			" group by d.id, a.id,a.descripcion,n.orden,pl.descripcionCorta,daa.numEstudiantes" //, ca1.codigo, ca2.descripcion "
			)
	List<CustomObjectLDD> listarDetalleDocentesDistributivo(Integer idDistributivoOferta);
	
	interface CustomObjectLDD{ 
	    Integer getIdDocente(); 
	    Integer getIdAsignatura(); 
	    String getAsignatura(); 
	    String getParalelo(); 
	    Integer getNumEstudiantes(); 
	    Double getDocencia(); 
	    Double getPracticas();
	    Double getHorasClase();
	}

	
	/*@Query(value=" select  distinct d.id as idDocente, a.id as idAsignatura,a.descripcion as asignatura,"+
			" CONCAT(cast(n.orden as text),'/',pl.descripcionCorta) as paralelo, daa.numEstudiantes as numEstudiantes, " + 
			" sum(case when ca1.codigo  = 'DOC'  then aa.valor else 0 end) as docencia, " + 
			" sum(case when ca1.codigo  = 'PAE'  then aa.valor else 0 end) as practicas, " + 
			" sum((case when ca1.codigo  = 'DOC'  then aa.valor else 0 end)  + sum(case when ca1.codigo  = 'PAE'  then aa.valor else 0 end)) as horasClase " + 
//			" sum(case when ca.codigoComp  = 'DOC' and (ca.codigo='PRE' or ca.codigo='VIR') and aa.aplicaDistributivo= 'True' then " + 
//			" aa.valor else 0 end) as docencia, " + 
//			" sum(case when ca.codigoComp  = 'PAE' and ca.codigo='APR' and aa.aplicaDistributivo= 'True' then " + 
//			" aa.valor else 0 end) as practicas,"+
//			" sum((case when ca.codigoComp  = 'DOC' and (ca.codigo='PRE' or ca.codigo='VIR')  and aa.aplicaDistributivo= 'True' then " + 
//			" aa.valor else 0 end)+(case when ca.codigoComp  = 'PAE' and ca.codigo='APR' and aa.aplicaDistributivo= 'True' then " + 
//			" aa.valor else 0 end)) as horasClase " + 
			" from Docente d "+
			" inner join DistributivoDocente ddo  on ddo.idDocente = d.id "+
			" inner join DistributivoDedicacion dde  on dde.idDistributivoDocente= ddo.id "+
			" inner join DocenteDedicacion ded on dde.idDocenteDedicacion = ded.id "+
			" inner join DocenteAsignaturaAprend daa  on dde.idDistributivoDocente = daa.idDistributivoDocente "+
			" inner join AsignaturaAprendizaje aa  on daa.idAsignaturaAprendizaje = aa.id  and aa.aplicaDistributivo= 'True' "+
			" inner join MallaAsignatura ma on aa.idMallaAsignatura = ma.id "+
			" inner join Asignatura a  on ma.idAsignatura = a.id "+
			" inner join Paralelo pl  on daa.idParalelo = pl.id "+
			" inner join ComponenteAprendizaje ca  on aa.idComponenteAprendizaje = ca.id "+
			" inner join ComponenteAprendizaje ca1  on ca.idComponenteAprendizajePadre = ca1.id "+
			"join Nivel n  on ma.idNivel = n.id " + 
			" where ddo.idDistributivoOfertaVersion=(?1) and ( d.id = (?2) or (?2) is null)and ddo.estado = 'A' and dde.estado = 'A' and daa.estado = 'A' " +
			" group by d.id, a.id,a.descripcion,n.orden,pl.descripcionCorta,daa.numEstudiantes "
			)*/
//	List<CustomObjectLDD> listarDetalleDocentesDistributivoID(Integer idDistributivoOferta , Integer idDocente);
	

	
	
	
	@Query(value=" select ddo from DistributivoDocente ddo where ddo.idDistributivoOfertaVersion = (?1) and ddo.idDocente  = (?2) and  ddo.estado = 'A' ")
	DistributivoDocente docenteEnDistributivoAEliminar(Integer idOfertaVersion,Integer idDocente);

	
	
	/*
	 * query = "select id_docente, apellidos_nombres,horas_clases  "+
		 " from [aca].fn_docentes_no_asignado_distributivo(?1, ?2) as d")

	 * */
}
