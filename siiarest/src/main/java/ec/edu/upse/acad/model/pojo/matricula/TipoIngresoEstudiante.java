package ec.edu.upse.acad.model.pojo.matricula;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.annotations.Where;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="aca", name="tipo_ingreso_estudiante")
@Where(clause = "estado='A'")
@NoArgsConstructor
public class TipoIngresoEstudiante {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_tipo_ingreso_estudiante")
	@Getter @Setter private Integer id;

	@Getter @Setter private String descripcion;
	
	@Getter @Setter private String codigo;

	@Getter @Setter private String estado;

//	@Column(name="fecha_ingreso")
//	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private int version;

	//RELACIONES

	//bi-directional many-to-one association to EstudianteOferta
	@OneToMany(mappedBy="estudiante", cascade=CascadeType.ALL)
	@Getter @Setter private List<EstudianteOferta> estudianteOfertas;

	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }

}
