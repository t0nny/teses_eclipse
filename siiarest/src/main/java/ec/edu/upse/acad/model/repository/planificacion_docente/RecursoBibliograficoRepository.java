package ec.edu.upse.acad.model.repository.planificacion_docente;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.planificacion_docente.RecursoBibliografico;

@Repository
public interface RecursoBibliograficoRepository extends JpaRepository<RecursoBibliografico, Long>  {

	@Query(value="select b.id as idRecursoBibliografico, b.titulo as titulo, b.descripcion as descripcion "+
			" from RecursoBibliografico b where b.estado='A' ")
	List<CoListadoBibliografias> listaBibliografias();
	interface CoListadoBibliografias{ 
	    Long getIdRecursoBibliografico();  
	    String getTitulo();
	    String getDescripcion();
	}
	
	@Query(value="select b.id as idRecursoBibliografico, b.titulo as titulo, b.descripcion as descripcion,a.id as idAutor, "+
			" a.nombres as autor,e.id as idEditorial, e.nombre as editorial,tb.id as idTipoBibliografia,tb.descripcion as tipoBibliografia," +
			" b.edicion as edicion,b.anioEdicion as anioEdicion, b.isbn as isbn" +
			" from RecursoBibliografico b " +
			" inner join Autor a on a.id = b.idAutor " +
			" inner join Editorial e on e.id = b.idEditorial " +
			" inner join TipoBibliografia tb on tb.id = b.idTipoBibliografia ")
	List<CoListarTodasBibliografias> listaTodasBibliografias();
	interface CoListarTodasBibliografias{ 
	    Long getIdRecursoBibliografico();  
	    String getTitulo();
	    String getDescripcion();
	    Integer getIdAutor();
	    String getAutor();
	    Integer getIdEditorial();
	    String getEditorial();
	    Integer getIdTipoBibliografia();
	    String getTipoBibliografia();
	    String getEdicion();
	    String getAnioEdicion();
	    String getIsbn();
	}

	
}
