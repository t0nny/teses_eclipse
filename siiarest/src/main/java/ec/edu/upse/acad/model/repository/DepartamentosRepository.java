package ec.edu.upse.acad.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.seguridad.Departamento;
import ec.edu.upse.acad.model.repository.OfertaRepository.CustomObjectMateriaOfertaK;

@Repository
public interface DepartamentosRepository  extends JpaRepository<Departamento, Integer>{
	@Query("SELECT d.id as idDepartamento,d.nombre as departamento,d.estado as estado,d.version as version "+
			"from Departamento d " +
			"WHERE d.estado='AC' and d.tipo='FAC'")
	 List<CustomObjectMateriaOferta> listaDepartamento();
	
	interface CustomObjectMateriaOferta { 
		Integer getIdDepartamento();
		String getDepartamento();
		String getEstado();
		Integer getVersion();
	}

	@Query("SELECT d.id as idDepartamento,d.nombre as departamento,d.estado as estado,d.version as version "
			+ "FROM Departamento d "
			+ "INNER JOIN DepartamentoOferta dof on d.id=dof.idDepartamento "
			+ "INNER JOIN Oferta ofe on dof.idOferta=ofe.id "
			+ "WHERE ofe.idTipoOferta=(?1) and  d.estado='AC' and dof.estado='A' and ofe.estado='A' "
			+ "GROUP BY d.id ,d.nombre ,d.estado ,d.version  ")
	 List<CustomObjectMateriaOfertaPorTipoOferta> listaDepartamentoPorTipoOferta(Integer tipoOferta);
	
	interface CustomObjectMateriaOfertaPorTipoOferta { 
		Integer getIdDepartamento();
		String getDepartamento();
		String getEstado();
		Integer getVersion();
	}

	//consulta de oferta por id departamento y tipo de oferta para el Kanban
			@Query("SELECT d.id as dataField,d.nombre as text " + 
					"from Departamento d " + 
					"WHERE d.estado='AC' " + 
					"and tipo='FAC' ")
			 List<CustomObjectDepartamentoK> listaDepartamentoKanban();
			
			interface CustomObjectDepartamentoK { 
				String gettext();
				String getDataField();			
			}
}