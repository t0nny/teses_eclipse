package ec.edu.upse.acad.ws;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.upse.acad.model.pojo.calificaciones.ActaCalificacion;
import ec.edu.upse.acad.model.repository.calificaciones.CalificacionGeneralRepository;
import ec.edu.upse.acad.model.repository.matricula.EstudianteRepository;
import ec.edu.upse.acad.model.service.CalificacionService;

@RestController
@RequestMapping("/api/calificacion")
@CrossOrigin
public class CalificacionController {

	@Autowired
	private CalificacionGeneralRepository calificacionGeneralRepository;
	@Autowired
	private EstudianteRepository estudianteRepository;
	@Autowired
	private CalificacionService calificacionService;

	@RequestMapping(value = "/listaCiclosPorPeriodo", method = RequestMethod.GET)
	public ResponseEntity<?> listaCiclosPorPeriodo() {
		return ResponseEntity.ok(calificacionGeneralRepository.listarCiclosPorPeriodo());
	}

	@RequestMapping(value = "/listarEstudiantesPorPeriodoAsignaturaMateria/{idPeriodoAcademico}/{idAsignatura}/{idParalelo}", method = RequestMethod.GET)
	public ResponseEntity<?> listarEstudiantesPorPeriodoAsignaturaMateria(
			@PathVariable("idPeriodoAcademico") Integer idPeriodoAcademico,
			@PathVariable("idAsignatura") Integer idAsignatura, @PathVariable("idParalelo") Integer idParalelo) {
		return ResponseEntity.ok(calificacionGeneralRepository
				.recuperarEstudiantesPorPeriodoAsignaturaMateria(idPeriodoAcademico, idAsignatura, idParalelo));
	}

	@RequestMapping(value = "/listarComponenteAprendizaje/{idPeriodoAcademico}", method = RequestMethod.GET)
	public ResponseEntity<?> listarComponenteAprendizaje(@PathVariable("idPeriodoAcademico") Integer idPeriodoAcademico) {
		return ResponseEntity.ok(calificacionGeneralRepository.recuperarComponentesAprendizaje(idPeriodoAcademico));
	}

	@RequestMapping(value = "/listarEst/{idPeriodoAcademico}/{idAsignatura}/{idParalelo}/{idDocente}", method = RequestMethod.GET)
	public ResponseEntity<?> getEst(@PathVariable("idPeriodoAcademico") Integer idPeriodoAcademico,
			@PathVariable("idAsignatura") Integer idAsignatura, @PathVariable("idParalelo") Integer idParalelo,
			@PathVariable("idDocente") Integer idDocente) {
		return ResponseEntity.ok(calificacionGeneralRepository.recuperarEstudiantes(idPeriodoAcademico, idAsignatura,idParalelo, idDocente));
	}

	@RequestMapping(value = "/listaciclos/{idPeriodoAcademico}", method = RequestMethod.GET)
	public ResponseEntity<?> getciclos(@PathVariable("idPeriodoAcademico") Integer idPeriodoAcademico) {
		return ResponseEntity.ok(calificacionGeneralRepository.recuperarCiclos(idPeriodoAcademico));
	}

	@RequestMapping(value = "/cicloVigente/{idPeriodoAcademico}", method = RequestMethod.GET)
	public ResponseEntity<?> getCicloVigente(@PathVariable("idPeriodoAcademico") Integer idPeriodoAcademico) {
		return ResponseEntity.ok(calificacionGeneralRepository.recuperarCicloVigente(idPeriodoAcademico));
	}

	@RequestMapping(value = "/listacom/{idPeriodoAcademico}", method = RequestMethod.GET)
	public ResponseEntity<?> get(@PathVariable("idPeriodoAcademico") Integer idPeriodoAcademico) {
		return ResponseEntity.ok(calificacionGeneralRepository.recuperarCiclosComponentes(idPeriodoAcademico));
	}

	@RequestMapping(value = "/grabarActa", method = RequestMethod.POST)
	public ResponseEntity<?> grabarActa(@RequestBody ActaCalificacion actaCalificacion) {
		calificacionService.grabaActaCalificacion(actaCalificacion);
		return ResponseEntity.ok().build();
	}

}
