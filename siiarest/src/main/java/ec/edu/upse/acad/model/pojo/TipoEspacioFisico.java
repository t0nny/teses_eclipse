package ec.edu.upse.acad.model.pojo;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="aca", name="tipo_espacio_fisico")
@NoArgsConstructor
public class TipoEspacioFisico {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_tipo_espacio_fisico")
	@Getter @Setter private Integer id;
	
	@Getter @Setter private String codigo;

	@Getter @Setter private String descripcion;

	@Getter @Setter private String estado;
	
	@Column(name="fecha_ingreso")
	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;
	
	//RELACIONES
	
	//bi-directional many-to-one association to Distributivo Oferta Version
    @OneToMany(mappedBy="tipoEspacioFisico", cascade=CascadeType.ALL)
    @Getter @Setter private List<EspacioFisico> espacioFisicos;

}
