/**
 * 
 */
package ec.edu.upse.acad.model.repository.evaluacion;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.evaluacion.Pregunta;

/**
 * @author dengi
 *
 */
@Repository
public interface PreguntaRepository extends JpaRepository<Pregunta, Integer>{
	//consulta para buscar las preguntas

	@Query(value="SELECT p "
			+ "FROM Pregunta p "
			+ "join CategoriaPregunta cp on p.categoriaPregunta.id = cp.id "
			+ "WHERE p.estado = 'A' and cp.estado = 'A' and p.categoriaPregunta.id = ?1 ")
	List<Pregunta> listarPreguntaCategoria(Integer id);
	
	@Query(value="SELECT p "
			+ " FROM Pregunta p " 
			+ " WHERE p.estado = 'A' and p.id = ?1 ")
	Pregunta findByIdPregunta(Integer id);
	
	@Query(value="SELECT p "
			+ " FROM Pregunta p " 
			+ " WHERE p.estado = 'A' and lower(p.descripcion) = lower(?1) ")
	Pregunta findByDescripcionPregunta(String descrpcion);
	/*
	@Query(value="SELECT p "
			+ "FROM Pregunta p "
			+ "join TipoPregunta tp on p.tipoPregunta.id = tp.id "
			+ "join CategoriaPregunta cp on p.categoriaPregunta.id = cp.id "
			+ "WHERE p.estado = 'A' and cp.estado = 'A' ")
	List<Pregunta> listarPregunta();
	
	/*
	interface CustomObjectEscala{
		Integer getId();
		Integer getIdTipoPregunta();
		String getCodigo();
		String getEstado();
	}*/
	
	@Query(value=" select p.id as id, p.tipoPregunta.id as idTipoPregunta, tp.descripcion as descripcionTipo, p.categoriaPregunta.id as idCategoria, "
			+ "cp.descripcion as descripcionCateg, p.codigo as codigo, p.descripcion as descripcion " 
			+ "from Pregunta p " 
			+ "join TipoPregunta tp on p.tipoPregunta.id = tp.id "
			+ "join CategoriaPregunta cp on p.categoriaPregunta.id = cp.id "
			+ "WHERE p.estado = 'A' and cp.estado = 'A' and tp.estado = 'A' ") 
	List<CustomObjectPregunta> listarPregunta();
	public interface CustomObjectPregunta{
		Integer getId();
		Integer getIdTipoPregunta();
		String getDescripcionTipo();
		Integer getIdCategoria();
		String getDescripcionCateg();
		String getCodigo();
		String getDescripcion();	
	} 
	
	/*select o.id_opcion_respuesta as idOpcion, p.descripcion as descripcion, p.estado as estado 
		from eva.pregunta p 
		join eva.opcion_pregunta op  on op.id_pregunta = p.id_pregunta 
		join eva.opcion_respuesta o on o.id_opcion_respuesta =op.id_opcion_respuesta
		where  p.estado ='A' 
		and p.descripcion  = 'El profesor presentó el sílabo de la asignatura, al inicio del ciclo académico.' and o.id_opcion_respuesta=6*/
//BUSCA la descripcion de una pregunta con respecto a una opcion
	@Query(value="select o.id as idOpcion, p.descripcion as descripcion, p.estado as estado "
			+ "from Pregunta p "
			+ "join OpcionPregunta op on op.pregunta.id = p.id "
			+ "join OpcionRespuesta o on o.id = op.idOpcionRespuesta.id "
			+ "where p.estado ='A' and p.descripcion = ?1 and o.id = ?2")
	List<CustomObjetctDescipcionPre> buscarDescripcionPregunta(String descripcion, Integer idOpcion);
	interface CustomObjetctDescipcionPre{
		Integer getIdOpcion();
		String getDescripcion();
		String  getEstado();
	}
}
