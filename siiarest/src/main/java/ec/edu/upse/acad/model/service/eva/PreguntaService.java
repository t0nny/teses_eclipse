package ec.edu.upse.acad.model.service.eva;

import java.sql.Date;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.upse.acad.model.pojo.Asignatura;
import ec.edu.upse.acad.model.pojo.OfertaAsignatura;
import ec.edu.upse.acad.model.pojo.evaluacion.InstrumentoPregunta;
import ec.edu.upse.acad.model.pojo.evaluacion.Pregunta;
import ec.edu.upse.acad.model.repository.evaluacion.InstrumentoPreguntaRepository;
import ec.edu.upse.acad.model.repository.evaluacion.PreguntaRepository;

@Service
@Transactional
public class PreguntaService {
	
	@Autowired private PreguntaRepository preguntaRepository;
	@Autowired private InstrumentoPreguntaRepository instrumentoPreguntaRepository;
	@PersistenceContext
	private EntityManager em;
	//GRABAR
	/*public void grabarInstrumentoPregunta(Pregunta pregunta) {
		if(pregunta.getId() != null) {
			this.editInstrumentoPregunta(pregunta);
		}else {
			this.newInstrumentoPregunta(pregunta);
		}
	}*/

	//GRABA UNA NUEVA Pregunta
	/*public void newInstrumentoPregunta(Pregunta pregunta){
		Pregunta _pregunta = new Pregunta();
		BeanUtils.copyProperties(pregunta, _pregunta, "categoriaPregunta", "tipoPregunta", "instrumentoPreguntas");
		Integer idPregunta = preguntaRepository.saveAndFlush(_pregunta).getId();
		
		//iteracion para sincronizar relacion con InstrumentoPregunta
		for (int i = 0; i < pregunta.getInstrumentoPreguntas().size(); i++) {
			Integer idOferta=asignatura.getOfertaAsignatura().get(i).getIdOferta();
			
			Integer idInstrumento = pregunta.getInstrumentoPreguntas().get(i).getId();
			String usuarioIngresoId = pregunta.getInstrumentoPreguntas().get(i).getUsuarioIngresoId();
			InstrumentoPregunta _instrumentoPregunta = new InstrumentoPregunta();
			_instrumentoPregunta.setId(idPregunta); 
			_instrumentoPregunta.setIdOferta(idInstrumento);
			_instrumentoPregunta.setUsuarioIngresoId(usuarioIngresoId);
			instrumentoPreguntaRepository.saveAndFlush(_instrumentoPregunta);
		}
		em.refresh(_pregunta);
	}

	//EDITA UNA Pregunta
	public void editInstrumentoPregunta(Pregunta pregunta) {
		Integer _idPregunta = pregunta.getId();
		Pregunta _pregunta = preguntaRepository.findById(_idPregunta).get();
		String _codigo = pregunta.getCodigo();
		String _tipo = pregunta.getTipoPregunta().;
		String _descripcion = pregunta.getDescripcion();
		Integer _usuarioIngresoId = pregunta.getUsuarioIngresoId();
		if(_pregunta != null) {
			_pregunta.setCodigo(_codigo);
			_pregunta.setTipoAsignatura(_tipo);
			_pregunta.setUsuarioIngresoId(_usuarioIngresoId);
			_pregunta.setDescripcion(_descripcion);
			_pregunta.setDescripcionCorta(__descripcionCorta);
			
			instrumentoPreguntaRepository.saveAndFlush(_pregunta);
		}	
	}*/

}
