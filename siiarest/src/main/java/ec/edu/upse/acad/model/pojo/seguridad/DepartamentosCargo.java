package ec.edu.upse.acad.model.pojo.seguridad;

import javax.persistence.*;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(schema="man", name="departamentos_cargos")
@NoArgsConstructor
public class DepartamentosCargo{
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Getter @Setter private int id;

	@Getter @Setter private String descripcion;

	@Getter @Setter private String estado;

	@Column(name="fecha_ing")
	@Getter @Setter private Timestamp fechaIng;

	@Column(name="fecha_mod")
	@Getter @Setter private Timestamp fechaMod;

	@Getter @Setter private String nombre;

	@Column(name="usuario_ing")
	@Getter @Setter private String usuarioIng;

	@Column(name="usuario_mod")
	@Getter @Setter private String usuarioMod;

	@Version
	@Getter @Setter private int version;

	//bi-directional many-to-one association to Departamento
	@ManyToOne
	private Departamento departamento;

	//bi-directional many-to-one association to PersonasCargo
	@OneToMany(mappedBy="departamentosCargo")
	private List<PersonasCargo> personasCargos;

	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "AC";
	 }
}