package ec.edu.upse.acad.model.pojo;


import java.sql.Timestamp;
import javax.persistence.*;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Table(schema="aca", name="asignatura_requisito")
@Where(clause = "estado='A'")
@NoArgsConstructor
public class AsignaturaRequisito {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_asignatura_requisito")
	@Getter @Setter private Integer id;

	@Getter @Setter private String estado;
	
	@Column(name="num_creditos")
	@Getter @Setter private Integer numCreditos;

	@Getter @Setter private Integer horas;
	
	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;
	
	@Column(name="id_requisito")
	@Getter @Setter private Integer idRequisito;

	@Column(name="id_malla_asignatura")
	@Getter @Setter private Integer idMallaAsignatura;
	

	//RELACIONES
	//bi-directional many-to-one association to MallaAsignatura
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_malla_asignatura", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private MallaAsignatura mallaAsignatura;

	//bi-directional many-to-one association to Requisito
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_requisito", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Requisito requisito;

	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }

}