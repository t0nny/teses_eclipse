package ec.edu.upse.acad.model.pojo.planificacion_docente;

import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="aca", name="contenidos")
@Where(clause = "estado='A'")
@NoArgsConstructor
public class Contenido {
	//Pojo recursivo
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_contenidos")
	@Getter @Setter private Integer id;
	
	@Column(name="id_silabo")
	@Getter @Setter private Integer idSilabo;
	
	@Column(name="id_contenido_padre")
	@Getter @Setter private Integer idContenidoPadre;
	
	@Getter @Setter private Integer orden;
	
	@Column(name="resultado_aprendizaje")
	@Getter @Setter private String resultadoAprendizaje;
	
	@Getter @Setter private String descripcion;
	
	@Column(name="horas_doc")
	@Getter @Setter private Integer horasDocencia;
	
	@Column(name="horas_aea")
	@Getter @Setter private Integer horasAEA;
	
	@Column(name="horas_ta")
	@Getter @Setter private Integer horasTA;
	
	@Getter @Setter private String estado;
	
	@Column(name="usuario_ingreso_id")
	@Getter @Setter private Integer usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;
	
	
	//RELACIONES
	@ManyToOne
	@JoinColumn(name="id_contenido_padre", insertable = false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Contenido _contenido;
	
	//bi-directional many-to-one association to Silabo
	@ManyToOne
	@JoinColumn(name="id_silabo", insertable = false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Syllabus syllabus;
	
	@OneToMany(mappedBy="contenido", cascade=CascadeType.ALL)
	//si se daña se quita este json ignore
	//si, porque en el json que le envio tiene esa caracteristica
	//un contenido tiene mas contenidos
	//algo asi
	//syllabus-->lista de contenidos y contenidos una lista de contenidos como es recursivo
	// pero si necesitas hacer una consulta a la tabla contenidos te sugiero que uses el otro pojo 
	//ese no tiene relacion con sigo misma por eso lo cree porque cuando trataba de recuperar se me iba de largo y no paraba nunca
	//entonces para eso sirve el otro pojo ....entonces quitalenoomas
	//@JsonIgnore
	@Getter @Setter private List<Contenido> contenido;
	
	//bi-directional many-to-one association to ContenidoComponenteAprendizaje
	@OneToMany(mappedBy = "contenido", cascade = CascadeType.ALL)
	@Getter @Setter private List<ContenidosCompAprendizaje> contenidoCompAprendizaje;
	
	//bi-directional many-to-one association to PlanClase
	@OneToMany(mappedBy="contenidol", cascade = CascadeType.ALL)
	@Getter @Setter private List<PlanClase> planClases;

	@PrePersist
	void preInsert() {
	   if (this.estado == null)
	       this.estado = "A";
 }
}
