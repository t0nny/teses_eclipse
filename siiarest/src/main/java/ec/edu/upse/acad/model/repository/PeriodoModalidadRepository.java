package ec.edu.upse.acad.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ec.edu.upse.acad.model.pojo.PeriodoModalidad;


public interface PeriodoModalidadRepository extends JpaRepository<PeriodoModalidad, Integer>{

	@Query(value="SELECT pm.id,pm.idPeriodoAcademico,pm.idModalidad,pm.estado,pm.version " + 
			"FROM PeriodoModalidad pm " + 
			"WHERE pm.estado = 'A' ")
	List<CustomObjectPeriodoModalidad> buscarperiodomodalidad();
	
	interface CustomObjectPeriodoModalidad {
		Integer getId();
		Integer getidPeriodoAcademico();
		Integer getidModalidad();
		String getEstado();
		Integer getVersion();
	}

	
	@Query(value="SELECT pm.id,pm.idPeriodoAcademico,pm.idModalidad,pm.estado,pm.version " + 
			"FROM PeriodoModalidad pm " + 
			"WHERE pm.estado = 'A' and pm.id = (?1) ")
	List<CustomObjectPeriodoModalidadId> buscarperiodomodalidadId(Integer id);
	
	interface CustomObjectPeriodoModalidadId {
		Integer getId();
		Integer getidPeriodoAcademico();
		Integer getidModalidad();
		String getEstado();
		Integer getVersion();
	}
}
