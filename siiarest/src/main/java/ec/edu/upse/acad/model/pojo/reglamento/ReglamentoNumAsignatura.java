package ec.edu.upse.acad.model.pojo.reglamento;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="aca", name="reglamento_num_asignatura")
@NoArgsConstructor
public class ReglamentoNumAsignatura {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_reglam_num_asignatura")
	@Getter @Setter private Integer id;
	
	@Column(name="id_reglamento")
	@Getter @Setter private Integer idReglamento;
	
	@Column(name="num_asignatura_max")
	@Getter @Setter private Integer numAsignaturaMax;

	@Getter @Setter  private String estado;
		
	@Column(name="fecha_ingreso")
	@Getter @Setter  private Date fechaIngreso;
		
	@Column(name="usuario_ingreso_id")
	@Getter @Setter  private String usuarioIngresoId;
		
	@Version
	@Getter @Setter  private Integer version;
	
	//RELACIONES
	//bi-directional many-to-one association to Reglamento
		@ManyToOne(fetch=FetchType.LAZY)
		@JoinColumn(name="id_reglamento" , insertable=false, updatable = false)
		@JsonIgnore
		@Getter @Setter private Reglamento reglamento;
				
	@PrePersist
		void preInsert() {
			if (this.estado == null)
				this.estado = "A";
		}
	
}
