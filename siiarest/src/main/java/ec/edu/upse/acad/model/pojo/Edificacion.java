package ec.edu.upse.acad.model.pojo;


import javax.persistence.*;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.List;


@Entity
@Table(schema="aca", name="edificacion")
@NoArgsConstructor
public class Edificacion{

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_edificacion")
	@Getter @Setter private Integer id;

	@Getter @Setter private String codigo;

	@Getter @Setter private String descricpion;

	@Getter @Setter private String estado;
	
	@Column(name="fecha_ingreso")
	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;

	//bi-directional many-to-one association to Aula
	@OneToMany(mappedBy="edificacion", cascade=CascadeType.ALL)
	@Getter @Setter private List<EspacioFisico> espaciofisicos;

	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }
}