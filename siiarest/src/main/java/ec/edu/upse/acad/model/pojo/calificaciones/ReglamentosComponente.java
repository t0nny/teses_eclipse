package ec.edu.upse.acad.model.pojo.calificaciones;

import javax.persistence.*;
import com.fasterxml.jackson.annotation.JsonIgnore;

import ec.edu.upse.acad.model.pojo.ComponenteAprendizaje;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="aca", name="reglamentos_componentes")
@NoArgsConstructor
public class ReglamentosComponente {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_reglamento_componente")
	@Getter @Setter private Integer id;

	@Getter @Setter private String estado;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;

	//RELACIONES
	//bi-directional many-to-one association to ComponenteAprendizaje
	@ManyToOne
	@JoinColumn(name="id_componente_aprendizaje")
	@JsonIgnore
	@Getter @Setter private ComponenteAprendizaje componenteAprendizaje;

	@PrePersist
	void preInsert() {
	   if (this.estado == null)
	       this.estado = "A";
	}

}