package ec.edu.upse.acad.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ec.edu.upse.acad.model.pojo.AsignaturaRequisito;

public interface AsignaturaRequisitoRepository extends JpaRepository<AsignaturaRequisito, Integer> {

}
