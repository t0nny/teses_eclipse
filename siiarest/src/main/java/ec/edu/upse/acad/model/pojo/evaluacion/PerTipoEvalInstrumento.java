package ec.edu.upse.acad.model.pojo.evaluacion;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="eva", name="per_tipo_eval_instrumento")
@NoArgsConstructor
public class PerTipoEvalInstrumento {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_per_tipo_eval_instrumento")
	@Getter @Setter private Integer id;
	
	@Column(name="id_tipo_evaluacion")
	@Getter @Setter private Integer idTipoEvaluacion;
	
	@Column(name="id_instrumento")
	@Getter @Setter private Integer idInstrumento;
	
	@Column(name="id_periodo_evaluacion")
	@Getter @Setter private Integer idPeriodoEvaluacion;
	
	@Getter @Setter private String descripcion;
	
	@Column(name="fecha_desde")
	@Getter @Setter private Date fechaDesde;

	@Column(name="fecha_hasta")
	@Getter @Setter private Date fechaHasta;
	
	@Getter @Setter private String estado; 

//	@Column(name="fecha_ingreso")
//	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;
	
	@Version
	@Getter @Setter private Integer version;

	//RELACIONES

	//bi-directional many-to-one association to Periodo
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_periodo_evaluacion", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private PeriodoEvaluacion periodoEvaluacion;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_tipo_evaluacion", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private TipoEvaluacion tipoEvaluacion;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_instrumento", insertable=false, updatable = false)
	//@JsonIgnore
	@Getter @Setter private Instrumento instrumento;
	
	
	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }
		
}
