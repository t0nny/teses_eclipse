package ec.edu.upse.acad.model.pojo.distributivo;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@SqlResultSetMapping(
		name = "MappingFnDocentesNoAsignadoDistributivo",
		classes = @ConstructorResult(
				targetClass = FnDocenteAsignaturaOfertaVersion.class,
				columns = {
						@ColumnResult(name = "idDocente"),
						@ColumnResult(name = "apellidosNombres"),
						@ColumnResult(name = "horasClases"),
						
						}
				)
		)

@NamedNativeQuery(name = "getFnDocentesNoAsignadoDistributivo",
resultClass = FnDocentesNoAsignadoDistributivo.class,

resultSetMapping ="MappingFnDocentesNoAsignadoDistributivo", 
query = "select id_docente, apellidos_nombres,horas_clases  "+
		 " from [aca].fn_docentes_no_asignado_distributivo(?1, ?2) as d")

@Entity
@NoArgsConstructor
public class FnDocentesNoAsignadoDistributivo {
	
	@Id
	@Column(name="id_docente")
	@Getter @Setter private Integer idDocente;
	
	@Column(name="apellidos_nombres")
	@Getter @Setter private String apellidosNombres;
	
	@Column(name="horas_clases")
	@Getter @Setter private Integer horasClases;
	

}
