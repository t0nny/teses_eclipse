package ec.edu.upse.acad.model.pojo.matricula;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import org.eclipse.persistence.annotations.AdditionalCriteria;
import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="aca", name="movilidad")
@Where(clause = "estado='A'")
@NoArgsConstructor
public class Movilidad {


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_movilidad")
	@Getter @Setter private Integer id;


	@Column(name="id_estudiante_oferta")
	@Getter @Setter private Integer idEstudianteOferta;

	@Column(name="id_subtipo_movilidad")
	@Getter @Setter private Integer idSubtipoMovilidad;

	@Column(name="numero_documento")	
	@Getter @Setter private String numeroDocumento;

	@Getter @Setter private String estado;

	//	@Column(name="fecha_ingreso")	
	//	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private int version;

	//RELACIONES
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_estudiante_oferta" , insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private EstudianteOferta estudianteOferta;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_subtipo_movilidad" , insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private SubtipoMovilidad subtipoMovilidad;

	//bi-directional many-to-one association to DetalleMovilidad
	@OneToMany(mappedBy="movilidad", cascade=CascadeType.ALL)
	@Getter @Setter private List<DetalleMovilidad> detalleMovilidad;

	@PrePersist
	void preInsert() {
		if (this.estado == null)
			this.estado = "A";
	}

}
