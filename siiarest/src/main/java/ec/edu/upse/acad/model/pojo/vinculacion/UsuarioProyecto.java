package ec.edu.upse.acad.model.pojo.vinculacion;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema = "vin", name = "usuario_proyecto")

@NoArgsConstructor //un constructorsin argumentos
public class UsuarioProyecto {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_usuario_proyecto")
	@Getter	@Setter	private Integer id;

	@Column(name = "id_tipo_usuario_proyecto")
	@Getter	@Setter	private Integer idTipoUsuarioProyecto;
	
	@Column(name = "id_usuario")
	@Getter	@Setter	private Integer idUsuario;
	
	@Column(name = "fecha_asigancion")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", locale = "es_EC", timezone = "America/Guayaquil")
	@Getter	@Setter	private Date fechaAsignacion;

	@Column(name = "usuario_ingreso_id")
	@Getter	@Setter	private String usuarioIngresoId;

	@Version
	@Getter	@Setter	private Integer version;

	@Getter	@Setter	private String estado;


}
