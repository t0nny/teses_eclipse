package ec.edu.upse.acad.model.repository.vinculacion;

import java.sql.Timestamp;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.vinculacion.ItemMarcoLogico;

@Repository
public interface ItemMarcoLogicoRepository extends JpaRepository<ItemMarcoLogico, Integer>{

	@Transactional
	// solo Listado de obtetivos componentes
	@Query(value = "SELECT iml.id as idItemMarcoLogico,iml.idTipoObjetivoProyecto as idTipoObjetivoProyecto,"
			+ " iml.proyecto.id as idProyecto,iml.descripcion as imlDescripcion,iml.indicador as indicador, "
			+ " iml.tiempoPlaneado as tiempoPlaneado,iml.lineaBase as lineaBase,iml.descripcionCorta as imlDescripcionCorta,"
			+ " iml.metaProgramada as metaProgramada,iml.estado as mlEstado,oa.id as idObjetivoActividad,"
			+ " oa.itemMarcoLogico.id as idOaItemMarcoLogico,oa.descripcion as oaDescripcion,oa.estado as oaEstado FROM ItemMarcoLogico iml "
			+ "INNER JOIN ObjetivoActividad oa on iml.id=oa.itemMarcoLogico.id "
			+ "WHERE iml.proyecto.id=(?1) AND iml.estado='A' AND oa.estado='A' ")
	List<customObjetObjetivoComponente> buscarObjetivoComponente(Integer idProyecto);
	
	interface customObjetObjetivoComponente {
		Integer getIdItemMarcoLogico();
		Integer getIdTipoObjetivoProyecto();
		Integer getIdProyecto();
		String getImlDescripcion();
		String getImlDescripcionCorta();
		String getIndicador();
		String getTiempoPlaneado();
		String getLineaBase();
		String getMetaProgramada();
		Integer getIdObjetivoActividad();
		Integer getIdOaItemMarcoLogico();		
		String getOaDescripcion();		
	}
	
	@Query(value = "SELECT iml.id as idItemMarcoLogico,"
			+ " iml.descripcion as imlDescripcion,iml.indicador as indicador, "
			+ " iml.tiempoPlaneado as tiempoPlaneado,iml.lineaBase as lineaBase,iml.descripcionCorta as imlDescripcionCorta,"
			+ " iml.metaProgramada as metaProgramada,iml.estado as mlEstado,oa.id as idObjetivoActividad,tiop.id as idTipoObjetivoProyecto,"
			+ " oa.itemMarcoLogico.id as idOaItemMarcoLogico,oa.descripcion as oaDescripcion,oa.estado as oaEstado FROM ItemMarcoLogico iml "
			+ "LEFT JOIN ObjetivoActividad oa on iml.id=oa.itemMarcoLogico.id "
			+ "INNER JOIN TipoObjetivoProyecto tiop on iml.idTipoObjetivoProyecto=tiop.id "
			+ "WHERE iml.id=(?1) AND iml.estado='A'")
	List<customActividades> buscarActividades(Integer idItemMarcoLogico);
	interface customActividades {
		Integer getIdItemMarcoLogico();
		String getImlDescripcion();
		String getImlDescripcionCorta();
		String getIndicador();
		String getTiempoPlaneado();
		String getLineaBase();
		String getMetaProgramada();
		Integer getIdObjetivoActividad();
		Integer getIdOaItemMarcoLogico();		
		String getOaDescripcion();
		Integer getIdTipoObjetivoProyecto();
	}
	
	@Query(value = "SELECT iml.id as idItemMarcoLogico,iml.idItemMarcoLogicoPadre as idItemMarcoLogicoPadre,iml.idTipoObjetivoProyecto as idTipoObjetivoProyecto,"
			+ " iml.proyecto.id as idProyecto,iml.descripcion as imlDescripcion,iml.indicador as indicador, "
			+ " iml.tiempoPlaneado as tiempoPlaneado,iml.lineaBase as lineaBase,iml.descripcionCorta as imlDescripcionCorta,"
			+ " iml.metaProgramada as metaProgramada,iml.estado as mlEstado,"
			+ " tiop.descripcion as tiopDescripcion,tiop.codigo as codigo FROM ItemMarcoLogico iml "
			+ "INNER JOIN TipoObjetivoProyecto tiop on iml.idTipoObjetivoProyecto=tiop.id "
			+ "WHERE iml.proyecto.id=(?1) AND iml.estado='A' AND tiop.estado='A' ")
	List<customObjetObjetivoGlobal> buscarObjetivoGlobal(Integer idProyecto);
	
	interface customObjetObjetivoGlobal {
		Integer getIdItemMarcoLogico();
		Integer getIdItemMarcoLogicoPadre();
		Integer getIdTipoObjetivoProyecto();
		Integer getIdProyecto();
		String getImlDescripcion();
		String getImlDescripcionCorta();
		String getIndicador();
		String getTiempoPlaneado();
		String getLineaBase();
		String getMetaProgramada();
		String getTiopDescripcion();	
		String getCodigo();	
	}
	
	//obtiene las tareas apar mostrarlas en el calendario
	
	@Query(value = "SELECT ot.id as idObjetivoTarea, ot.descripcion as subject,ot.fechaDesde as fechaDesde,"
			+ " ot.fechaHasta as fechaHasta, ot.creditoExt as credExt, ot.creditoInt as credInt, ot.cooperacion as coop, "
			+ " ot.autogestion as auto, ot.fiscales as fiscales, ot.aporteComunit as apotCom, iml.descripcionCorta as resourceId,"
			+ " ot.estado as estado, oa.id as idObjetivoActividad FROM ItemMarcoLogico iml "
			+ "INNER JOIN ObjetivoActividad oa on  iml.id=oa.itemMarcoLogico.id "
			+ "INNER JOIN ObjetivoTarea ot on oa.id= ot.idObjetivoActividad "
			+ "WHERE iml.proyecto.id=(?1) AND iml.estado='A' AND oa.estado='A' AND ot.estado='A'")
	List<customObjetObjetivoTareas> buscarObjetivoTareas(Integer idProyecto);
	
	interface customObjetObjetivoTareas {
		Integer getIdObjetivoTarea();
		Integer getIdObjetivoActividad();
		Timestamp getFechaDesde();
		Timestamp getFechaHasta();
		String getSubject();
		String getCredExt();
		String getCredInt();
		String getCoop();
		String getAuto();
		String getFiscales();
		String getApotCom();	
		String getResourceId();
		String getEstado();
	}
	
	//obtiene el presupuesto de las tareas
	
		@Query(value = "SELECT oa.id as idObjetivoTarea,oa.descripcion as descripcion,sum(ot.aporteComunit) as aportComunit, "
				+ " sum(ot.autogestion) as autogestion,sum(ot.cooperacion) as cooperacion,sum(ot.creditoExt)as creditoExt,sum(ot.fiscales)as fiscales, "
				+ " sum(ot.creditoInt) as creditoInt,(sum(ot.aporteComunit)+ sum(ot.autogestion)+sum(ot.cooperacion)+sum(ot.creditoExt)"
				+ " +sum(ot.creditoInt)+sum(ot.fiscales))as total from ObjetivoActividad oa "
				+ "INNER JOIN ObjetivoTarea ot on oa.id=ot.idObjetivoActividad "
				+ "INNER JOIN ItemMarcoLogico iml on iml.id=oa.itemMarcoLogico.id "
				+ "WHERE iml.proyecto.id=(?1) AND iml.estado='A' AND oa.estado='A' AND ot.estado='A' "
				+ "GROUP BY oa.descripcion,oa.id")
		List<customObjetPresupuestoTareas> buscarPresupuestoTareas(Integer idProyecto);
		
		interface customObjetPresupuestoTareas {
			Integer getIdObjetivoTarea();
			String getDescripcion();
			String getCreditoExt();
			String getCreditoInt();
			String getCooperacion();
			String getAutogestion();
			String getFiscales();
			String getAportComunit();
			String getTotal();
		}
		
		@Query(value = "SELECT ot.estadoTarea as estadoTarea, iml.id as idItemMarcoLogico,iml.descripcion as descripcionItemMarcoLogico,"
				+ " oa.id as idObjetivoActividad, oa.descripcion as descripcionActividad,"
				+ " ot.id as idObjetivoTarea,ot.idObjetivoActividad as otIdObjetivoActividad, "
				+ " ot.descripcion as descripcionTarea,ot.fechaDesde as fechaDesde, ot.fechaHasta as fechaHasta"
				+ " FROM ItemMarcoLogico iml "
				+ "INNER JOIN ObjetivoActividad oa on iml.id=oa.itemMarcoLogico.id "
				+ "INNER JOIN ObjetivoTarea ot on oa.id= ot.idObjetivoActividad "
				+ "WHERE iml.proyecto.id=(?1) AND iml.estado='A' AND oa.estado='A' AND ot.estado='A'")
		List<customObjetObjetivoActividadYTareas> buscarObjetivoActividadYTareas(Integer idProyecto);
		
		interface customObjetObjetivoActividadYTareas {
			Integer getIdObjetivoTarea();
			Integer getIdItemMarcoLogico();
			Integer getIdObjetivoActividad();
			Integer getOtIdObjetivoActividad();
			Timestamp getFechaDesde();
			Timestamp getFechaHasta();
			String getDescripcionActividad();
			String getDescripcionTarea();	
			String getDescripcionItemMarcoLogico();
			String getEstadoTarea();
		}
}
