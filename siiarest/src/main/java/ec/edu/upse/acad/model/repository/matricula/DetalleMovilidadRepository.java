package ec.edu.upse.acad.model.repository.matricula;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.matricula.DetalleMovilidad;

@Repository
public interface DetalleMovilidadRepository extends JpaRepository<DetalleMovilidad, Integer>{


	@Query(value=	" SELECT mas.id as idMallaAsignatura, asi.id as idAsignatura, asi.descripcion as asignatura,n.descripcion as nivel," +
					" mas.numCreditos as numCreditos,n.orden as orden," + 
					" (select dmo.id from DetalleMovilidad as dmo  inner JOIN Movilidad as mov on dmo.idMovilidad=mov.id "+
					" where mov.idEstudianteOferta = (?1) and dmo.idMallaAsignatura = mas.id  and dmo.estado='A') as idDetalleMovilidad,"+
					" (select dmo.calificacion from DetalleMovilidad as dmo inner JOIN Movilidad as mov on dmo.idMovilidad=mov.id "+
					" where mov.idEstudianteOferta = (?1) and dmo.idMallaAsignatura = mas.id  and dmo.estado='A') as calificacion,"+
					" (select mov.idSubtipoMovilidad from DetalleMovilidad as dmo inner JOIN Movilidad as mov on dmo.idMovilidad=mov.id "+
					" where mov.idEstudianteOferta = (?1) and dmo.idMallaAsignatura = mas.id  and dmo.estado='A') as idSubtipoMovilidad, " +
					" (coalesce((select stm.descripcion from DetalleMovilidad as dmo  " + 
					" inner JOIN Movilidad as mov on dmo.idMovilidad=mov.id " + 
					" inner join SubtipoMovilidad stm on stm.id = mov.idSubtipoMovilidad " + 
					" where mov.idEstudianteOferta = (?1) and dmo.idMallaAsignatura = mas.id and dmo.estado='A'),'No registrado')) as descripcionSubtipo"+
					" FROM MallaAsignatura as mas" + 
					" INNER JOIN Asignatura as asi on mas.idAsignatura=asi.id " + 
					" INNER JOIN Nivel as n on mas.idNivel=n.id " + 
					" WHERE mas.idMalla=(select eo.idMalla from EstudianteOferta eo where eo.id = (?1)) order by idSubtipoMovilidad desc, n.orden asc")

	List<CustomObjectfiltrarDetalleMovilidad> filtrarDetalleMovilidad(Integer idEstudianteOferta);
	
	interface CustomObjectfiltrarDetalleMovilidad{
		Integer getIdMallaAsignatura();
		Integer getIdAsignatura();
		String getAsignatura();
		String getNivel();
		Integer getNumCreditos();
		Integer getOrden();
		Integer getIdDetalleMovilidad();
		Double getCalificacion();
		Integer getIdSubtipoMovilidad();
		String getDescripcionSubtipo();
	}

}

