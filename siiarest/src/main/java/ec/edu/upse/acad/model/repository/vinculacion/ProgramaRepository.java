package ec.edu.upse.acad.model.repository.vinculacion;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.vinculacion.Programa;

@Repository
public interface ProgramaRepository extends JpaRepository<Programa, Integer> {

	
	@Transactional
	// solo Listado de Programas
	@Query(value = "SELECT pg.id as idPrograma, pg.codigo as codigo, pg.titulo as tituloPrograma,"
			+ " pg.fechaDesde as fechaDesde, pg.fechaHasta as fechaHasta, "
			+ " pg.estadoPrograma as estadoProyecto, pg.estado as estado FROM Programa pg "
			+ "INNER JOIN DepOfertaPrograma dop on pg.id=dop.idPrograma "
			+ "WHERE dop.idDepartamentoOferta=(?1) AND pg.estado='A' ")
	List<customObjetbuscarProgramasIdDepOferta> buscarProgramasIdDepOferta(Integer idDepartamentoOferta);
	
	interface customObjetbuscarProgramasIdDepOferta {
		Integer getIdPrograma();
		String getTituloPrograma();
		String getCodigo();
		Date getFechaDesde();
		Date getFechaHasta();
		String getEstadoPrograma();
		String getEstado();
	}
	
	// solo Listado de Proyectos
		@Query(value = "SELECT pg.id as idPrograma, pg.codigo as codigoPrograma, pg.titulo as tituloPrograma,"
				+ " pg.estadoPrograma as estadoPrograma, pa.codigo as descripcion, "
				+ " py.id as idProyecto, py.titulo as tituloProyecto, py.codigo as codigoProyecto, "
				+ " py.fechaDesde as fechaDesde, py.fechaHasta as fechaHasta, py.estadoProyecto as estadoProyecto,"
				+ " py.estado as estado" + " FROM Proyecto py "
				+ "LEFT JOIN Programa pg on py.idPrograma= pg.id "
				+ "INNER JOIN DepOfertaProyecto dop on py.id=dop.proyecto.id "				
				+ "INNER JOIN ProyectoConvocatoria cpy on cpy.id= py.idProyectoConvocatoria "
				+ "INNER JOIN PeriodoAcademico pa on pa.id=cpy.idPeriodoAcademico " 
				+ "WHERE dop.idDepartamentoOferta=(?1) AND py.estado='A' ")
		List<customObjetbuscarProyectosIdDepOferta> buscarProyectosIdDepOferta(Integer idDepartamentoOferta);
		
		interface customObjetbuscarProyectosIdDepOferta {
			Integer getIdProyecto();
			String getTituloProyecto();
			String getCodigoProyecto();
			String getEstadoProyecto();
			Integer getIdPrograma();
			String getTituloPrograma();
			String getCodigoPrograma();
			String getEstadoPrograma();
			Date getFechaDesde();
			Date getFechaHasta();
			String getEstado();
			String getDescripcion();
		}

}
