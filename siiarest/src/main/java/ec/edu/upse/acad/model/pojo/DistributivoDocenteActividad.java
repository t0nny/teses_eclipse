package ec.edu.upse.acad.model.pojo;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="aca", name="vw_rpt_distributivo_docente_actividad")
@NoArgsConstructor
public class DistributivoDocenteActividad {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	@Getter @Setter private Integer id;

	@Column(name="id_periodo_academico")
	@Getter @Setter private Integer idPeriodoAcademico;


	@Column(name="periodo_academico")
	@Getter @Setter private String periodoAcademico;
	

	@Column(name="identificacion")
	@Getter @Setter private String identificacion;
	
	@Column(name="apellidos_nombres")
	@Getter @Setter private String apellidosNombres;

	@Column(name="id_docente")
	@Getter @Setter private Integer idDocente;
	
	@Column(name="genero")
	@Getter @Setter private String genero;

	@Column(name="tipo_oferta")
	@Getter @Setter private String tipoOferta;
	
	@Column(name="id_carrera_distributivo")
	@Getter @Setter private Integer idCarreraDistributivo;
	
	@Column(name="carrera_distributivo")
	@Getter @Setter private String carreraDistributivo;
	
	@Column(name="id_departamento_distributivo")
	@Getter @Setter private Integer idDepartamentoDistributivo;
	
	@Column(name="id_tipo_oferta")
	@Getter @Setter private Integer idTipoOferta;

	@Column(name="departamento_distributivo")
	@Getter @Setter private String departamentoDistributivo;
	
		
	@Column(name="dedicacion")
	@Getter @Setter private String dedicacion;
	
	@Column(name="id_docente_dedicacion")
	@Getter @Setter private Integer idDocenteDedicacion;

	@Column(name="categoria")
	@Getter @Setter private String categoria;
	
	@Column(name="oferta_asignatura")
	@Getter @Setter private String ofertaAsignatura;
	
	@Column(name="departamento_asignatura")
	@Getter @Setter private String departamentoAsignatura;


	@Column(name="id_distributivo_docente")
	@Getter @Setter private Integer idDistributivoDocente;

	@Column(name="paralelo")
	@Getter @Setter private String paralelo;
	
	@Column(name="id_paralelo")
	@Getter @Setter private Integer idParalelo;
	
	@Column(name="num_estudiantes")
	@Getter @Setter private Integer numEstudiantes;
	
	@Column(name="asignatura")
	@Getter @Setter private String asignatura;
	
	@Column(name="id_asignatura")
	@Getter @Setter private Integer idAsignatura;
	
	@Column(name="docencia")
	@Getter @Setter private Integer docencia;
	
	@Column(name="practicas")
	@Getter @Setter private Integer practicas;
	
	@Column(name="horas_clases")
	@Getter @Setter private Integer horasClases;
	
	@Column(name="actividad")
	@Getter @Setter private String actividad;
	
	@Column(name="actividad_valor")
	@Getter @Setter private Integer actividadValor;

}
