package ec.edu.upse.acad.model.repository.matricula;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.matricula.CostoAsignatura;

@Repository
public interface CostoAsignaturaRepository extends JpaRepository<CostoAsignatura, Integer>{
	
	@Query(value=" select tm.id as id ,tm.codigo as codigo,tm.descripcion as descripcion, tm.estado as estado from TipoMatricula tm ")
	List<CustObjParametrica> listadoTiposMatricula();
	
	@Query(value="select tm.id as id, tm.codigo as codigo, tm.descripcion as descripcion, tm.estado as estado from TipoMovilidad tm ")
	List<CustObjParametrica> listadoTiposMovilidad();
	
	@Query(value=" select tr.id as id, tr.codigo as codigo, tr.descripcion as descripcion, tr.estado as estado  from TipoReglamento tr ")
	List<CustObjParametrica> listadoTiposReglamentos();
	
	@Query(value=" 	select te.id as id,te.codigo as codigo, te.descripcion as descripcion, te.estado as estado from TipoEstudiante te ")
	List<CustObjParametrica> listadoTiposEstudiantes();
	
	@Query(value=" 	select ti.id as id,ti.codigo as codigo, ti.descripcion as descripcion, ti.estado as estado from TipoIngresoEstudiante ti ")
	List<CustObjParametrica> listadoTiposIngresoEstudiante();
	
	interface CustObjParametrica{
		Integer getId(); 
		String getCodigo(); 
		String getDescripcion();
		String getEstado();
	} 
	
	@Query(value="select sm.id as id,sm.codigo as codigo , sm.descripcion as descripcion, sm.estado as estado, tm.descripcion as tipoMovilidad" +
			" from SubtipoMovilidad sm inner join TipoMovilidad tm on tm.id = sm.idTipoMovilidad")
	List<CustObjSubtipoMovilidad> listadoSubtiposMovilidad();
	interface CustObjSubtipoMovilidad{
		Integer getId();
		String getCodigo();
		String getDescripcion();
		String getEstado();
		String getTipoMovilidad();
	}
	
	@Query(value=" select tip.id as id,tip.codigo as codigo,tip.descripcion as descripcion,tip.descripcionCorta as descripcionCorta,tip.estado  as estado from TipoOferta tip ")
	List<CustObjTipoOferta> listadoTiposOferta();

	interface CustObjTipoOferta{ 
		Integer getId();
		String getCodigo();
		String getDescripcion();
		String getEstado();
		String getDescripcionCorta();
	} 
	
	@Query(value=" select ca.id as id, ca.codigo as codigo,ca.descripcion as descripcion,ca.estado as estado, ca.valor as valor from CostoAsignatura ca ")
	List<CustObjCostosAsignatura> listadoCostosAsignatura();

	interface CustObjCostosAsignatura{ 
		Integer getId();
		String getCodigo();
		String getDescripcion();
		String getEstado();
		Double getValor();
	} 


}
