package ec.edu.upse.acad.model.repository.evaluacion;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
 
import ec.edu.upse.acad.model.pojo.evaluacion.PeriodoEvaluacion;

@Repository
public interface PeriodoEvaluacionRepository extends JpaRepository<PeriodoEvaluacion, Integer>{

	/*@Query(value=" SELECT niv.id as idSemestre," 
			+ " niv.descripcion as semestre," 
			+ " asig.id as idMateria," 
			+ " asig.codigo as codigo," 
			+ " CONCAT(asig.codigo,' = ', asig.descripcion,' ==> ', p.nombres,' ', p.apellidos) as materia," 
			+ " CONCAT(p.nombres,' ', p.apellidos) as docente" 
			+ " FROM EstudianteMatricula estmat " 
			+ " inner join EstudianteAsignatura estasig on estasig.idEstudianteMatricula = estmat.id " 
			+ " inner join EstudianteOferta eo on estmat.idEstudianteOferta = eo.id " 
			+ " inner join DocenteAsignaturaAprend daa on daa.id = estasig.idDocenteAsignaturaAprend " 
			+ " inner join Paralelo par on par.id = daa.idParalelo " 
			+ " inner join AsignaturaAprendizaje aa on aa.id = daa.idAsignaturaAprendizaje " 
			+ " inner join MallaAsignatura ma on ma.id = aa.idMallaAsignatura " 
			+ " inner join MatriculaGeneral matgen on estmat.idMatriculaGeneral = matgen.id " 
			+ " inner join PeriodoAcademico peracad on matgen.idPeriodoAcademico = peracad.id " 
			+ " inner join Periodo periodo on peracad.idPeriodo = periodo.id " 
			+ " inner join Asignatura asig on ma.idAsignatura = asig.id " 
			+ " inner join Nivel niv on ma.idNivel = niv.id " 
			+ " inner join DistributivoDocente dd on daa.idDistributivoDocente = dd.id " 
			+ " inner join Docente doc on dd.idDocente = doc.id " 
			+ " inner join Persona p on doc.idPersona = p.id " 
			+ " inner join PeriodoEvaluacion pereva on pereva.idPeriodoAcademico = peracad.idPeriodoAcademico "
			+ " inner join EvaluacionDocenteGeneral edg on edg.id = pereva.idEvaluacionDocenteGeneral "
			+ " where estmat.estado = 'A' " 
			+ " and periodo.estado = 'A' " 
			+ " and estasig.estado = 'A' " 
			+ " and daa.estado = 'A' " 
			+ " and eo.idEstudiante = ?1 and niv.id = ?2 " 
			+ " and (GETDATE() >= pereva.fechaDesde ) and (GETDATE() <= pereva.fechaHasta) " 
			+ " GROUP BY " 
			+ " niv.id, " 
			+ " niv.descripcion, asig.id, " 
			+ " asig.codigo, asig.descripcion, " 
			+ " p.nombres, p.apellidos ")*/
	@Query(value = "{call eva.pa_evaluacion_semestre_materia_docente_estudiante(:idEstudiante,:idSemestre)}", nativeQuery = true)
	List<CustomObjectMateriaDocente> listarMateriaDocentePorEstudiante(@Param("idEstudiante") Integer idEstudiante,@Param("idSemestre") Integer idSemestre);
	
	interface CustomObjectMateriaDocente{
		Integer getIdSemestre();
		String getSemestre();
		Integer getIdMateria();
		String getCodigo();
		String getMateria();
		String getDocente();
	}
	
	/*@Query(value=" select est.id as idEstudiante, " + 
			" per.identificacion as identificacion, " + 
			" CONCAT(per.nombres, ' ' ,per.apellidos) as estudiante, " + 
			" estof.numeroMatricula as matricula , " + 
			" peracad.codigo as periodo_academico, " + 
			" niv.id as idNivel, " + 
			" niv.descripcion as semestre " + 
			" from Usuario usu " + 
			" inner join Persona per on usu.persona.id = per.id " + 
			" inner join Estudiante est on est.idPersona = per.id " + 
			" inner join EstudianteOferta estof on estof.idEstudiante = est.id " + 
			" inner join Oferta oferta on estof.idOferta = oferta.id " + 
			" inner join TipoOferta tipof on tipof.id = oferta.idTipoOferta " + 
			" inner join Malla malla on estof.idMalla = malla.id " + 
			" inner join EstudianteMatricula estmat on estmat.idEstudianteOferta = estof.id " + 
			" inner join MatriculaGeneral matgen on estmat.idMatriculaGeneral = matgen.id " + 
			" inner join PeriodoAcademico peracad on matgen.idPeriodoAcademico = peracad.id " + 
			" inner join Periodo periodo on peracad.idPeriodo = periodo.id " + 
			" inner join EstudianteAsignatura estasig on estasig.idEstudianteMatricula = estmat.id " + 
			" inner join DocenteAsignaturaAprend daa on daa.id = estasig.idDocenteAsignaturaAprend " +  
			" inner join Paralelo par on par.id = daa.idParalelo " + 
			" inner join AsignaturaAprendizaje aa on aa.id = daa.idAsignaturaAprendizaje " + 
			" inner join MallaAsignatura ma on ma.id = aa.idMallaAsignatura " + 
			" inner join Asignatura asig on ma.idAsignatura = asig.id " + 
			" inner join Nivel niv on ma.idNivel = niv.id " + 
			" inner join EstudianteMatricula em on matgen.id = em.idMatriculaGeneral " + 
			" inner join EstudianteOferta eo on estmat.idEstudianteOferta = eo.id " + 
			" where usu.estado = 'AC' and per.estado = 'AC' " + 
			" and est.estado = 'A' and estof.estado = 'A' and estmat.estado = 'A' " + 
			" and periodo.estado  = 'A' " + 
			" and estasig.estado  = 'A' " + 
			" and peracad.id = 4 " + 
			" and usu.usuario = ?1 " + 
			" GROUP BY " + 
			" est.id, per.identificacion, " + 
			" per.nombres, per.apellidos, " + 
			" estof.numeroMatricula, peracad.codigo, " + 
			" niv.id, niv.descripcion ")*/
	@Query(value = "{call eva.pa_semestre_evaluacion_estudiante(:usuario)}", nativeQuery = true)
	List<CustomObjectSemestreEstudiante> listarSemestrePorUsuarioEstudiante(@Param("usuario") String usuario);
	
	interface CustomObjectSemestreEstudiante{
		Integer getIdEstudiante();
		String getIdentificacion();
		String getEstudiante();
		String getmatricula();
		String getPeriodoAcademico();
		Integer getIdNivel();
		String getSemestre();
		String getEvaluado();
	}
	
	@Query(value=" select est.id as idEstudiante, " +  
			" niv.id as idNivel, " +  
			" daa.id as idDocenteAsignaturaAprendizaje, "+
			" aa.id as idAsignarutaAprendizaje, "+
			" dd.id as idDistributivoDocente, "+
			" pa.id as idParalelo, "+
			" asig.id as idAsignatura, " + 
			" asig.codigo as codigo, " +
			" asig.descripcion  as descripcion " +
			" from Usuario usu " + 
			" inner join Persona per on usu.persona.id = per.id " + 
			" inner join Estudiante est on est.idPersona = per.id " + 
			" inner join EstudianteOferta estof on estof.idEstudiante = est.id " + 
			" inner join DepartamentoOferta doferta on estof.idDepartamentoOferta = doferta.id " +  
			" inner join Malla malla on estof.idMalla = malla.id " + 	
			" inner join EstudianteMatricula estmat on estmat.idEstudianteOferta = estof.id " + 			
			" inner join MatriculaGeneral matgen on estmat.idMatriculaGeneral = matgen.id " + 
			" inner join EstudianteAsignatura estasig on estasig.idEstudianteMatricula = estmat.id " + 
			" inner join DocenteAsignaturaAprend daa on daa.id = estasig.idDocenteAsignaturaAprend " +  
			" inner join DistributivoDocente dd on daa.idDistributivoDocente = dd.id "+
			" inner join AsignaturaAprendizaje aa on daa.idAsignaturaAprendizaje = aa.id " + 
			" inner join Paralelo pa on daa.idParalelo = pa.id "+
			" inner join ComponenteAprendizaje ca on aa.idComponenteAprendizaje = ca.id "+
			" inner join ComponenteAprendizaje ca1 on ca.idComponenteAprendizajePadre = ca1.id "+
			" inner join MallaAsignatura ma on ma.id = aa.idMallaAsignatura " + 
			" inner join Asignatura asig on ma.idAsignatura = asig.id " + 
			" inner join Nivel niv on ma.idNivel = niv.id " + 
			" inner join Malla malla on ma.idMalla = malla.id " + 
			" inner join PeriodoAcademico peracad on matgen.idPeriodoAcademico = peracad.id " + 
			" inner join Reglamento reg on matgen.idReglamento = reg.id "+
			" inner join Periodo periodo on peracad.idPeriodo = periodo.id " +  
			" inner join PeriodoEvaluacion pe on pe.periodoAcademico.id = peracad.id "+
			" inner join EvaluacionDocenteGeneral edg on edg.id = pe.evaluacionDocenteGeneral.id "+ 
			" where usu.estado = 'AC' and per.estado = 'AC' " + 
			" and est.estado = 'A' and estof.estado = 'A' and estmat.estado = 'A' " + 
			" and periodo.estado  = 'A' " + 
			" and estasig.estado  = 'A' " + 
			" and daa.estado = 'A' " + 
			" and pe.estado = 'A' " +
			" and ca1.codigo = 'DOCENCIA' " + 
			" and usu.usuario = ?1 " +
			" and (GETDATE() >= pe.fechaDesde) and (GETDATE() <= pe.fechaHasta) " + 
			" GROUP BY  est.id, niv.id, daa.id, aa.id, " + 
			" dd.id, pa.id, asig.id, asig.codigo, asig.descripcion")
	List<CustomObjectMateriaSemestreEstudiante> listarMateriaSemestrePorUsuarioEstudiante(String usuario);
	
	public interface CustomObjectMateriaSemestreEstudiante{
		Integer getIdEstudiante(); 
		Integer getIdNivel();
		Integer getIdDocenteAsignaturaAprendizaje();
		Integer getIdAsignarutaAprendizaje();
		Integer getIdDistributivoDocente();
		Integer getidParalelo();
		Integer getIdAsignatura();
		String getCodigo();
		String getDescripcion();
	}
	
	
	
	@Query(value = "select pe.id as id, pe.descripcion as descripcion, pe.fechaDesde as fechaDesde, pe.fechaHasta as fechaHasta "
			+ "from PeriodoEvaluacion pe "
			+ "where pe.estado = 'A' AND (pe.fechaHasta != null OR pe.fechaHasta > current_date())")//consulta
	List<CO_PerEvaluacion> getAllPeriodoEvaluacion();//periodo academico???
	interface CO_PerEvaluacion{
		Integer getId();
		String getDescripcion();
		Date getFechaDesde();
		Date getFechaHasta();
	}
	
	
//	@Query(value = "select "
//			+ "from PeriodoEvaluacion pe "
//			+ "JOIN PerTipoEvalInstrumento pcf on "
//			+ "")
//	List<CO_FindObject> findOnPeriodoCompFuncion(Integer idPeriodoEvaluacion);//revisar
//	interface CO_FindObject{
//		Integer getId();
//		String getDescripcion();
//		String getEstado();
//	}
	
	
	@Query(value = "Select pe.id as id, pe.evaluacionDocenteGeneral.id as idEvaluacionDocenteGeneral, "
			+ "pe.descripcion as descripcion, pe.fechaDesde as fechaDesde, pe.fechaHasta as fechaHasta "
			+ "From PeriodoEvaluacion pe "
			+ "JOIN EvaluacionDocenteGeneral edg on edg.id = pe.evaluacionDocenteGeneral.id "
			+ "WHERE pe.estado = 'A' AND pe.id = ?1 AND pe.evaluacionDocenteGeneral.id = ?2")
	List<CO_getPeriodoEvaluacion> getPeriodoEvaluacionById(Integer idPeriodoEvaluacion, Integer idEvaluacionDocenteGeneral);
	interface CO_getPeriodoEvaluacion{
		Integer getId();
		//Integer getIdPeriodoAcademico();
		Integer getIdEvaluacionDocenteGeneral();
		String getDescripcion();
		Date getFechaDesde();
		Date getFechaHasta();
	}
	

}
