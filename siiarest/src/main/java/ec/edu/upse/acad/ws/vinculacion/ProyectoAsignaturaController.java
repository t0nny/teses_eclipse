package ec.edu.upse.acad.ws.vinculacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.upse.acad.model.service.SecurityService;
import ec.edu.upse.acad.model.service.vinculacion.ProyectoAsignaturaService;


@RestController
@RequestMapping("/api/proyectoAsignatura")
@CrossOrigin
public class ProyectoAsignaturaController {

	@Autowired
	private SecurityService securityService;
	@Autowired
	private ProyectoAsignaturaService proyectoAsignaturaService;
	
	// buscar por el id de proyecto
	@RequestMapping(value = "/borrarAsignaturaGrid/{idProyectoAsignatura}", method = RequestMethod.GET)
	public ResponseEntity<?> borrarAsignaturaGrid(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable("idProyectoAsignatura") Integer idProyectoAsignatura) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		proyectoAsignaturaService.borrarAsignatura(idProyectoAsignatura);
		return ResponseEntity.ok().build();
	}

}
