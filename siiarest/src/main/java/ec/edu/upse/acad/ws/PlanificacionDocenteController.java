package ec.edu.upse.acad.ws;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ec.edu.upse.acad.model.pojo.planificacion_docente.Contenidos;
import ec.edu.upse.acad.model.pojo.planificacion_docente.PlanClase;
import ec.edu.upse.acad.model.pojo.planificacion_docente.RecursoBibliografico;
import ec.edu.upse.acad.model.repository.AreaConocimientoRepository;
import ec.edu.upse.acad.model.repository.calificaciones.OpcionesAsignaturaRepository;
import ec.edu.upse.acad.model.repository.planificacion_docente.AutorRepositiry;
import ec.edu.upse.acad.model.repository.planificacion_docente.ContenidosRepository;
import ec.edu.upse.acad.model.repository.planificacion_docente.ContenidossRepository;
import ec.edu.upse.acad.model.repository.planificacion_docente.EditorialRepository;
import ec.edu.upse.acad.model.repository.planificacion_docente.EstrategiaEvaluacionRepository;
import ec.edu.upse.acad.model.repository.planificacion_docente.MetodologiaEnsenanzaRepository;
import ec.edu.upse.acad.model.repository.planificacion_docente.PlanClaseRepository;
import ec.edu.upse.acad.model.repository.planificacion_docente.RecursoBibliograficoRepository;
import ec.edu.upse.acad.model.repository.planificacion_docente.TipoBibliografiaRepository;
import ec.edu.upse.acad.model.service.ReportService;
import ec.edu.upse.acad.model.service.SyllabusService;
import lombok.Getter;

@RestController
@RequestMapping("/api/planificacionDocente")
@CrossOrigin


public class PlanificacionDocenteController {
	@Autowired private OpcionesAsignaturaRepository opcionesAsignaturaRepository;
	@Autowired private ContenidosRepository contenidosRepository;
	
	@Autowired private ContenidossRepository _contenidosRepository;
	@Autowired private MetodologiaEnsenanzaRepository metodologiaEnsenanzaRepository;
	@Autowired private EstrategiaEvaluacionRepository estrategiaEvaluacionRepository;
	@Autowired private AutorRepositiry autorRepositiry;
	@Autowired private EditorialRepository editorialRepository;
	@Autowired private TipoBibliografiaRepository tipoBibliografiaRepository;
	@Autowired private RecursoBibliograficoRepository recursoBibliograficoRepository;
	@Autowired private AreaConocimientoRepository areaConocimientoRepository;
	@Autowired private PlanClaseRepository planClaseRepository;
	@Autowired private SyllabusService syllabusService;
	
	
	@RequestMapping(value="/listaDocente", method=RequestMethod.GET)
	public ResponseEntity<?> listaDocente() {
		return ResponseEntity.ok(opcionesAsignaturaRepository.listaDocente());
	}
	
	@RequestMapping(value="/listaDocenteAsignatura/{idPeriodo}/{idDocente}", method=RequestMethod.GET)
	public ResponseEntity<?> listaDocenteAsignatura(@PathVariable("idPeriodo") Integer idPeriodo ,
			@PathVariable("idDocente") Integer idDocente) {
		return ResponseEntity.ok(opcionesAsignaturaRepository.listaDocenteAsignatura(idPeriodo, idDocente));
	}
	
	@RequestMapping(value="/listaResumenSilaboAsignatura/{idPeriodo}/{idOferta}/{idMallaAsignatura}", method=RequestMethod.GET)
	public ResponseEntity<?> listaResumenSilaboAsignatura(@PathVariable("idPeriodo") Integer idPeriodo ,
			  @PathVariable("idOferta") Integer idOferta,@PathVariable("idMallaAsignatura") Integer idMallaAsignatura) {
		return ResponseEntity.ok(opcionesAsignaturaRepository.listaResumenSilaboAsig(idPeriodo,idOferta, idMallaAsignatura));
	}
	
	@RequestMapping(value="/listaSilaboAsignaturaCab/{idMallaAsignatura}", method=RequestMethod.GET)
	public ResponseEntity<?> listaSilaboAsignaturaCab(@PathVariable("idMallaAsignatura") Integer idMallaAsignatura) {
		return ResponseEntity.ok(opcionesAsignaturaRepository.listaSilaboAsigCab(idMallaAsignatura));
	}
	
	@RequestMapping(value="/listaSilaboAsignaturaDet/{idMallaAsignatura}", method=RequestMethod.GET)
	public ResponseEntity<?> listaSilaboAsignaturaDet(@PathVariable("idMallaAsignatura") Integer idMallaAsignatura) {
		return ResponseEntity.ok(opcionesAsignaturaRepository.listaSilaboAsigDet(idMallaAsignatura));
	}
	
	/**********************PARA PANTALLA DE REGISTRO DE CLASES***************************/
	
	//Lista metodologias de enseñanza
	@RequestMapping(value="/listaMetodologiasEnsenanza", method=RequestMethod.GET)
	public ResponseEntity<?> listaMetodologiasEnsenanza() {
		return ResponseEntity.ok(metodologiaEnsenanzaRepository.listaMetodologiasEnsenanza());
	}
	
	//lista las evaluciones
	@RequestMapping(value="/listaEstrategiasEvaluacion", method=RequestMethod.GET)
	public ResponseEntity<?> listaEstrategiasEvaluacion() {
		return ResponseEntity.ok(estrategiaEvaluacionRepository.listaEstrategiasEvaluacion());
	}
	
	//lista las recursos bibliograficos
	@RequestMapping(value="/listaBibliografias", method=RequestMethod.GET)
	public ResponseEntity<?> listaBibliografias() {
		return ResponseEntity.ok(recursoBibliograficoRepository.listaBibliografias());
	}
	
	//lista todos los recursos bibliograficos
	@RequestMapping(value="/listaTodasBibliografias", method=RequestMethod.GET)
	public ResponseEntity<?> listaTodasBibliografias() {
		return ResponseEntity.ok(recursoBibliograficoRepository.listaTodasBibliografias());
	}
	//recuperar Contenido Sylabus por id
	@RequestMapping(value="/recuperarContenidoId/{idContenido}", method=RequestMethod.GET)
	public ResponseEntity<?> recuperarContenidoId(@PathVariable("idContenido") Integer idContenido) {
		Contenidos _contenido;
		if (contenidosRepository.findById(idContenido).isPresent()) {
			_contenido = _contenidosRepository.findById(idContenido).get();
			return ResponseEntity.ok(_contenido);
		}else {
			return ResponseEntity.notFound().build();
		}
	}
	
	//recuperar plan de clase por id
	@RequestMapping(value="/recuperarPlanClaseId/{idPlanClase}", method=RequestMethod.GET)
	public ResponseEntity<?> recuperarPlanClaseId(@PathVariable("idPlanClase") Long idPlanClase) {
		PlanClase _planClase;
		if (planClaseRepository.findById(idPlanClase).isPresent()) {
			_planClase = planClaseRepository.findById(idPlanClase).get();
			return ResponseEntity.ok(_planClase);
		}else {
			return ResponseEntity.notFound().build();
		}
	}
	
	// servicio para grabar un plan de Clases
	@RequestMapping(value = "/grabarPlanClase", method = RequestMethod.POST)
	public ResponseEntity<?> grabarPlanClase(@RequestBody PlanClase planClase) {
		syllabusService.grabarPlanClase(planClase);
		return ResponseEntity.ok().build();
	}
	
	//lista los silabos
	@RequestMapping(value="/listaPlanesClase/{idMallaAsignatura}", method=RequestMethod.GET)
	public ResponseEntity<?> listaPlanesClase(@PathVariable("idMallaAsignatura") Integer idMallaAsignatura) {
		return ResponseEntity.ok(planClaseRepository.listaPlanesClase(idMallaAsignatura));
	}
	/**********************PARA PANTALLA DE REGISTRO DE CLASES***************************/
	
	/**********************PARA PANTALLA DE BIBLIOGRAFIA*********************************/
	
	//lista las autores
	@RequestMapping(value="/listaAutores", method=RequestMethod.GET)
	public ResponseEntity<?> listaAutores() {
		return ResponseEntity.ok(autorRepositiry.listaAutores());
	}
	
	//lista las editoriales
	@RequestMapping(value="/listaEditoriales", method=RequestMethod.GET)
	public ResponseEntity<?> listaEditoriales() {
		return ResponseEntity.ok(editorialRepository.listaEditoriales());
	}
	
	//lista las tipos de bibliografia
	@RequestMapping(value="/listaTiposBibliografias", method=RequestMethod.GET)
	public ResponseEntity<?> listaTiposBibliografias() {
		return ResponseEntity.ok(tipoBibliografiaRepository.listaTiposBibliografias());
	}
	
	//lista las areas del conocimiento
	@RequestMapping(value="/listarAreasConocimiento", method=RequestMethod.GET)
	public ResponseEntity<?> listarAreasConocimiento() {
		return ResponseEntity.ok(areaConocimientoRepository.listarAreas());
	}
	
	//lista las asignaturas
	@RequestMapping(value="/listarAsignaturas", method=RequestMethod.GET)
	public ResponseEntity<?> listarAsignaturas() {
		return ResponseEntity.ok(areaConocimientoRepository.listarAsignaturas());
	}
	
	//recuperar recurso bibliografico por id
	@RequestMapping(value="/recuperarRecursoBibliograficoId/{idRecursoBibliografico}", method=RequestMethod.GET)
	public ResponseEntity<?> recuperarRecursoBibliograficoId(@PathVariable("idRecursoBibliografico") Long idRecursoBibliografico) {
		RecursoBibliografico _recursoBibliografico;
		if (recursoBibliograficoRepository.findById(idRecursoBibliografico).isPresent()) {
			_recursoBibliografico = recursoBibliograficoRepository.findById(idRecursoBibliografico).get();
			return ResponseEntity.ok(_recursoBibliografico);
		}else {
			return ResponseEntity.notFound().build();
		}
	}
	
	//recupera las horas validas
	@RequestMapping(value="/recuperarHoras/{idContenido}", method=RequestMethod.GET)
	public ResponseEntity<?> recuperarHoras(@PathVariable("idContenido") Integer idContenido) {
		return ResponseEntity.ok(planClaseRepository.recuperarHoras(idContenido));
	}
	
	//servicio para borrar una bibliografía
	@RequestMapping(value="/borrarBibliografia/{idRecursoBibliografico}", method=RequestMethod.DELETE)
	public ResponseEntity<?> borrarBibliografia(@PathVariable("idRecursoBibliografico") Long idRecursoBibliografico) {
		syllabusService.borrarBibliografia(idRecursoBibliografico);
		return ResponseEntity.ok().build();
	}
	
	//servicio para borrar una bibliografía
	@RequestMapping(value="/borrarPlanClase/{idPlanClase}", method=RequestMethod.DELETE)
	public ResponseEntity<?> borrarPlanClase(@PathVariable("idPlanClase") Long idPlanClase) {
		syllabusService.borrarPlanClase(idPlanClase);
		return ResponseEntity.ok().build();
	}
	
	// servicio para grabar un recurso bibliografico
	@RequestMapping(value = "/grabarBibliografia", method = RequestMethod.POST)
	public ResponseEntity<?> grabarBibliografia(@RequestBody RecursoBibliografico recursoBibliografico) {
		syllabusService.grabarRecursoBibliografico(recursoBibliografico);
		return ResponseEntity.ok().build();
	}
	
	/**********************PARA PANTALLA DE BIBLIOGRAFIA*********************************/
	
	
	@Getter private String imagen="logoUpse.png";
	@Getter private String rutaLogoUpse="";
	
	private static final String PATH_IMAGEN = "/src/main/resources/img";
	private static final String PATH_REPORT = File.separator+"src"+File.separator+"main"+File.separator+"resources"+File.separator+"rep";
	@Autowired private ReportService reportService;
	
	//reporte de sylabos
		@RequestMapping(value="/getReporteSylabos/{idMallaAsignatura}", method=RequestMethod.GET)
		public ResponseEntity<?> getComprobanteMatriculaAnteriores(@PathVariable("idMallaAsignatura") Integer idMallaAsignatura, 
				HttpServletRequest request) throws Exception {
			Map<String, Object> parametros = new HashMap<String, Object>();
			parametros.put("pi_id_malla_asignatura", idMallaAsignatura);
			File file =new File(imagen);
			String ruta =file.getAbsoluteFile().getParent();
			rutaLogoUpse  = ruta+PATH_IMAGEN+File.separator+imagen; 

			parametros.put("rutaImagen", rutaLogoUpse);
			// Carga el archivo
			Resource resource = reportService.generaReporteParametros("rpt_sylabo.jasper",parametros);

			// Determina el contenido
			String contentType = null;
			try {
				contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
			} catch (IOException ex) {
				ex.printStackTrace();
			}

			// Si no se determina el tipo, asjjume uno por defecto.
			if(contentType == null) {
				contentType = "application/octet-stream";
			}
			return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
					.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"").body(resource);
		}

}
