package ec.edu.upse.acad.model.pojo.seguridad;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="seg", name="modulos_roles")
@NoArgsConstructor

public class ModuloRol {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Getter @Setter private Integer id;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="modulo_id", nullable = false, insertable=false, updatable = false)
	@Getter @Setter private Modulo modulo;

		
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="rol_id", nullable = false, insertable=false, updatable = false)
	@Getter @Setter private Rol rol;

	
	
	@OneToMany(mappedBy="moduloRol", cascade=CascadeType.ALL)
	@Getter @Setter private List<ModuloRolOpcion> moduloRolOpcion;
	
	@OneToMany(mappedBy="moduloRol", cascade=CascadeType.ALL)
    @JsonIgnore
	@Getter @Setter private List<ModuloRolUsuario> moduloRolUsuario;
	

	@Getter @Setter private String estado;
	
	
	@Version
	@Getter @Setter private Integer version;
	
	@PrePersist
	void preInsert() {
	   if (this.estado == null)
	       this.estado = "AC";
	}
}
