package ec.edu.upse.acad.model.pojo.vinculacion;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="vin", name="proyecto_persona")
@NoArgsConstructor

public class ProyectoPersona {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id_proyecto_persona")
	@Getter	@Setter	private Integer id;

	@Getter @Setter private String identificacion;

	@Getter @Setter private String nombres;

	@Getter @Setter private String apellidos;

	@Getter @Setter private String genero;

	@Getter @Setter private Date fecha_nace;

	@Getter @Setter private String direccion;

	@Getter @Setter private String telefono;
	
	@Getter @Setter private String ciudad;

	@Getter @Setter private String celular;

	@Getter @Setter private String email_personal;

	@Getter @Setter private String estado;

	@Version
	@Getter @Setter private Integer version;
	
	@PrePersist
	void preInsert() {
		if (this.estado == null)
			this.estado = "A";
	}
}
