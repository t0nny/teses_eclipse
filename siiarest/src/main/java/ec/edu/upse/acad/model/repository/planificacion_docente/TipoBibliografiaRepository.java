package ec.edu.upse.acad.model.repository.planificacion_docente;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.planificacion_docente.TipoBibliografia;

@Repository
public interface TipoBibliografiaRepository extends JpaRepository<TipoBibliografia, Integer> {
	
	@Query(value="select t.id as idTipoBibliografia, t.codigo as codigo, t.descripcion as tiposBiblio "+
			" from TipoBibliografia t ")
	List<CoListadoTiposBiblio> listaTiposBibliografias();
	interface CoListadoTiposBiblio{ 
	    Integer getIdTipoBibliografia();  
	    String getCodigo();
	    String getTiposBiblio();
	}
}
