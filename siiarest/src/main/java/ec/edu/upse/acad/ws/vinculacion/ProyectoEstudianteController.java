package ec.edu.upse.acad.ws.vinculacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.upse.acad.model.repository.vinculacion.ProyectoEstudianteRepository;
import ec.edu.upse.acad.model.service.SecurityService;
import ec.edu.upse.acad.model.service.vinculacion.ProyectoEstudianteService;

@RestController
@RequestMapping("/api/proyectoEstudiante")
@CrossOrigin
public class ProyectoEstudianteController {
	@Autowired
	private SecurityService securityService;
	@Autowired
	private ProyectoEstudianteService proyectoEstudianteService;
	@Autowired
	private ProyectoEstudianteRepository proyectoEstudianteRepository;
	
	// buscar por el id de proyectoEstudiante
	@RequestMapping(value = "/borrarProyectoEstudianteGrid/{idProyectoEstudiante}", method = RequestMethod.GET)
	public ResponseEntity<?> borrarProyectoEstudianteGrid(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable("idProyectoEstudiante") Integer idProyectoEstudiante) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		proyectoEstudianteService.borrarProyectoEstudiante(idProyectoEstudiante);
		return ResponseEntity.ok().build();
	}
	
	@RequestMapping(value = "/estudianteProyecto/{idProyecto}", method = RequestMethod.GET)
	public ResponseEntity<?> estudianteProyecto(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable("idProyecto") Integer idProyecto) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(proyectoEstudianteRepository.estudianteProyecto(idProyecto));
	}


}
