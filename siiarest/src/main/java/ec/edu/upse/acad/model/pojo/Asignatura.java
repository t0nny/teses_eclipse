package ec.edu.upse.acad.model.pojo;


import javax.persistence.*;

import org.hibernate.annotations.Where;

import ec.edu.upse.acad.model.pojo.planificacion_docente.AsignaturaBibliografia;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Date;
import java.util.List;


@Entity
@Table(schema="aca", name="asignatura")
@Where(clause = "estado='A'")
@NoArgsConstructor
public class Asignatura{

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_asignatura")
	@Getter @Setter private Integer id;

	@Getter @Setter private String codigo;
	
	@Column(name="tipo_asignatura")
	@Getter @Setter private String tipoAsignatura;

	@Getter @Setter private String descripcion;

	@Column(name="fecha_desde")
	@Getter @Setter private Date fechaDesde;

	@Column(name="fecha_hasta")
	@Getter @Setter private Date fechaHasta;

	@Column(name="descripcion_corta")
	@Getter @Setter private String descripcionCorta;

	@Getter @Setter private String estado;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;

	//RELACIONES
	//bi-directional many-to-one association to CompatibilidadAsignatura
	@OneToMany(mappedBy="asignatura1", cascade=CascadeType.ALL)
	@Getter @Setter private List<CompatibilidadAsignatura> compatibilidadAsignaturas1;

	//bi-directional many-to-one association to CompatibilidadAsignatura
	@OneToMany(mappedBy="asignatura2", cascade=CascadeType.ALL)
	@Getter @Setter private List<CompatibilidadAsignatura> compatibilidadAsignaturas2;

	//bi-directional many-to-one association to OfertaAsignatura
	@OneToMany(mappedBy="asignatura", cascade=CascadeType.ALL)
	@Getter @Setter private List<OfertaAsignatura> ofertaAsignaturas;

	//bi-directional many-to-one association to MallaAsignatura
	@OneToMany(mappedBy="asignatura", cascade=CascadeType.ALL)
	@Getter @Setter private List<MallaAsignatura> mallaAsignaturas;
	
	//bi-directional many-to-one association to AsignaturaBibliografia
	@OneToMany(mappedBy="asignatura", cascade=CascadeType.ALL)
	private List<AsignaturaBibliografia> asignaturaBibliografias;


	@PrePersist
	void preInsert() {
		if (this.estado == null)
			this.estado = "A";
	}
	 @Transient // solo existe aqui el atributo es transitorios 
	 @Getter @Setter private Integer listaMallasAsignaturas;
}