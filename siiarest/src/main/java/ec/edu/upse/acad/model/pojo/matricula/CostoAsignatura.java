package ec.edu.upse.acad.model.pojo.matricula;

import javax.persistence.*;

import org.hibernate.annotations.Where;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


import java.sql.Timestamp;
import java.util.List;


@Entity
@Table(schema="aca", name="costo_asignatura")
@Where(clause = "estado='A'")
@NoArgsConstructor
public class CostoAsignatura{

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_costo_asignatura")
	@Getter @Setter private Integer id;

	@Getter @Setter private String codigo;

	@Getter @Setter private String descripcion;

	@Getter @Setter private String estado;

//	@Column(name="fecha_ingreso")
//	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Getter @Setter private Double valor;

	@Version
	@Getter @Setter private Integer version;

	//RELACIONES
	//bi-directional many-to-one association to EstudianteAsignatura
	@OneToMany(mappedBy="costoAsignatura", cascade=CascadeType.ALL)
	private List<EstudianteAsignatura> estudianteAsignaturas;

	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }

}