package ec.edu.upse.acad.model.repository.distributivo;


import java.sql.Date;
import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ec.edu.upse.acad.model.pojo.distributivo.DistributivoGeneral;


import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface DistributivoGeneralRepository extends JpaRepository<DistributivoGeneral, Integer>{
	
	   @Query(value = "EXECUTE pa_crear_distributivos_carreras ?", nativeQuery = true)
	   List<DistributivoGeneral> crearDistributivosCarreras(@Param("idDistributivoGeneral") Integer idDistributivoGeneral); 
	  

		/**
		 * lista el distributivo general de las carreras
		 * @return
		 */
	   @Query(value=" select dg.id as idDistributivoGeneral, dg.descripcion as observaciones "+
				" from DistributivoGeneral as dg " 				
				)
		List<CustomObject2> listarDistributivoGeneral();
		interface CustomObject2  { 
		    Integer getIdDistributivoGeneral(); ; 
		    String getObservaciones(); 
			  
		}
		
		
		@Query(value="SELECT  pa.id as id,pa.codigo as codigo,pa.descripcion as descripcion "+
					"FROM PeriodoAcademico pa " + 
					"WHERE pa.estado='A' and (GETDATE()>=pa.fechaDesde) and (GETDATE()<=pa.fechaHasta) order by pa.descripcion desc")
		List<CustomObjectPeriodoVigente> listaPeriodoVigent();		
		interface CustomObjectPeriodoVigente  { 
		    Integer getId(); 
		    String getCodigo(); 
		    String getDescripcion(); 
	  
		}
		
		@Query(value="SELECT  pa.id as id,pa.codigo as codigo,pa.descripcion as descripcion, do.id as idDistributivoOferta "+
				" FROM PeriodoAcademico pa " + 
				" inner join DistributivoGeneral dg on dg.idPeriodoAcademico=pa.id  "+
				" inner join DistributivoGeneralVersion dgv on dg.id=dgv.idDistributivoGeneral "+
				" inner join DistributivoOferta do on dgv.id=do.idDistributivoGeneralVersion "+
			    " WHERE pa.estado='A' and dg.estado='A' and dgv.estado='A' and do.estado='A' "+
			    " and do.id=(?1) and (((GETDATE()>=pa.fechaDesde) and  GETDATE()<=pa.fechaHasta) or (pa.fechaDesde>=GETDATE()) )")
		List<periodoValidoCreacionDistributivo> periodoValidoCrearDistributivo(Integer idDistributivoOferta);		
		interface periodoValidoCreacionDistributivo  { 
		    Integer getId(); 
		    String getCodigo(); 
		    String getDescripcion(); 
		    Integer getIdDistributivoOferta();  
		}
		
		@Query(value="SELECT  pa.id as id,pa.codigo as codigo,pa.descripcion as descripcion"+
				",isnull ((select do.id from DistributivoGeneral dg "+
				" inner join DistributivoGeneralVersion dgv on dg.id=dgv.idDistributivoGeneral "+
				" inner join DistributivoOferta do on dgv.id=do.idDistributivoGeneralVersion "+
				" where dg.idPeriodoAcademico=pa.id "+
				" and do.idOferta=(?1) "+
				" group by do.id ),0) as idDistributivoOferta "+
				",isnull ((select isnull( max (dov.versionDistributivoOferta),0) from DistributivoGeneral dg "+
				" inner join DistributivoGeneralVersion dgv on dg.id=dgv.idDistributivoGeneral "+
				" inner join DistributivoOferta do on dgv.id=do.idDistributivoGeneralVersion "+
				" inner join DistributivoOfertaVersion dov on do.id=dov.idDistributivoOferta "+
				" where dg.idPeriodoAcademico=pa.id "+
				" and do.idOferta=(?1) "+
				" group by do.id ),0) as versionDistributivoOferta "+
				"FROM PeriodoAcademico pa " + 
			   "WHERE pa.estado='A' and (GETDATE()>=pa.fechaDesde) and (GETDATE()<=pa.fechaHasta) order by pa.descripcion desc")

		List<CustomObjectPeriodosVigentes> listaPeriodosVigentes(Integer idOferta);		
		interface CustomObjectPeriodosVigentes  { 
		    Integer getId(); 
		    String getCodigo(); 
		    String getDescripcion(); 
		    Integer getIdDistributivoOferta();
		    Integer getVersionDistributivoOferta();	  
		}
		
		@Query(value="SELECT pa.codigo as codigo,o.descripcionCorta as codigoOferta ,do.id as idDistributivoOferta,"+
				"ISNULL( (SELECT max( dov.versionDistributivoOferta) "+
							"FROM DistributivoOferta do1  "+
							 "INNER JOIN DistributivoOfertaVersion dov on do1.id=dov.idDistributivoOferta"+
							" WHERE dov.estado in ('A', 'V', 'R' ,'D') and do1.estado='A' and do1.id=do.id "+
							"GROUP BY do1.id),0) as versionDistributivoOferta ,"+
				" CASE WHEN  (SELECT max( dov.fechaDesde) FROM DistributivoOferta do1  "+
							 "INNER JOIN DistributivoOfertaVersion dov on do1.id=dov.idDistributivoOferta"+
							" WHERE dov.estado in ('A', 'V', 'R','D') and do1.estado='A' and do1.id=do.id "+
							"GROUP BY do1.id )  IS NULL "+
							"then dg.fechaDesde else "+
							"(SELECT max( dov.fechaDesde) FROM DistributivoOferta do1  " +
							"INNER JOIN DistributivoOfertaVersion dov on do1.id=dov.idDistributivoOferta " + 
							" WHERE dov.estado in ('A', 'V', 'R','D') and do1.estado='A' and do1.id=do.id " + 
							"GROUP BY do1.id )  END as fechaDesde , pa.fechaHasta as fechaHasta, "+
				"ISNULL( (SELECT max( dov.versionDistributivoOferta) "+
							"FROM DistributivoOferta do1  "+
							 "INNER JOIN DistributivoOfertaVersion dov on do1.id=dov.idDistributivoOferta"+
							" WHERE dov.estado='P' and do1.estado='A' and do1.id=do.id "+
							"GROUP BY do1.id ) ,0) as versionDistributivoOfertaProy ,"+
				" CASE WHEN  (SELECT max( dov.fechaDesde) FROM DistributivoOferta do1  "+
							 "INNER JOIN DistributivoOfertaVersion dov on do1.id=dov.idDistributivoOferta"+
							" WHERE dov.estado in ('P') and do1.estado='A' and do1.id=do.id "+
							"GROUP BY do1.id )  IS NULL "+
							"then dg.fechaDesde else "+
							"(SELECT max( dov.fechaDesde) FROM DistributivoOferta do1  " +
							"INNER JOIN DistributivoOfertaVersion dov on do1.id=dov.idDistributivoOferta " + 
							" WHERE dov.estado in ('P') and do1.estado='A' and do1.id=do.id " + 
							"GROUP BY do1.id )  END as fechaDesdeProy ,"+
			
				" CASE WHEN  (SELECT max( dov.fechaDesde) FROM DistributivoOferta do1  "+
							 "INNER JOIN DistributivoOfertaVersion dov on do1.id=dov.idDistributivoOferta"+
							" WHERE dov.estado in ('A', 'V', 'R','D') and do1.estado='A' and do1.id=do.id "+
							"GROUP BY do1.id )  IS NULL "+
							"then '' else "+
							"(SELECT max(dov2.estado) from DistributivoOfertaVersion dov2 "+
							"where dov2.fechaDesde in (SELECT max( dov.fechaDesde) "
							+ 						"FROM DistributivoOferta do1  " +
													"INNER JOIN DistributivoOfertaVersion dov on do1.id=dov.idDistributivoOferta " + 
													" WHERE dov.estado in ('A', 'V', 'R','D') and do1.estado='A' and do1.id=do.id " + 
													"GROUP BY do1.id )"
							+ "and dov2.idDistributivoOferta=do.id and dov2.estado in ('A', 'V', 'R','D')"
							+ " ) END as estado  "+
				"FROM DistributivoOferta do "+
				"INNER JOIN Oferta o on do.idOferta=o.id "+
				"INNER JOIN DistributivoGeneralVersion dgv on do.idDistributivoGeneralVersion=dgv.id "+
				"INNER JOIN DistributivoGeneral dg on dgv.idDistributivoGeneral=dg.id "+
				"INNER JOIN PeriodoAcademico pa on dg.idPeriodoAcademico=pa.id "+
				"WHERE do.estado='A' and o.estado='A' and dgv.estado='A' and dg.estado='A' and pa.estado='A'  and do.id=(?1) "+
				"GROUP BY pa.codigo, o.descripcionCorta, do.id,pa.fechaHasta,dg.fechaDesde ")
		CustomObjectPeriodoDistributivo periodoDistributivo(Integer idDistributivoOferta);		
		interface CustomObjectPeriodoDistributivo  { 
			String getCodigo();
			String getCodigoOferta();
		    Integer getIdDistributivoOferta();
		    Integer getVersionDistributivoOferta();	  
		    Date getFechaDesde();
		    Date getFechaHasta();
		    Integer getVersionDistributivoOfertaProy();	  
		    Date getFechaDesdeProy();
		    String getEstado();
		}
		
		
		@Query(value="SELECT  pa.id as id,pa.codigo as codigo,pa.descripcion as descripcion"+
				",isnull ((select do.id from DistributivoGeneral dg "+
				" inner join DistributivoGeneralVersion dgv on dg.id=dgv.idDistributivoGeneral "+
				" inner join DistributivoOferta do on dgv.id=do.idDistributivoGeneralVersion "+
				" where dg.idPeriodoAcademico=pa.id "+
				" and do.idOferta=(?1) "+
				" group by do.id ),0) as idDistributivoOferta "+
				",isnull ((select isnull( max (dov.versionDistributivoOferta),0) from DistributivoGeneral dg "+
				" inner join DistributivoGeneralVersion dgv on dg.id=dgv.idDistributivoGeneral "+
				" inner join DistributivoOferta do on dgv.id=do.idDistributivoGeneralVersion "+
				" inner join DistributivoOfertaVersion dov on do.id=dov.idDistributivoOferta "+
				" where dg.idPeriodoAcademico=pa.id "+
				" and do.idOferta=(?1) "+
				" group by do.id ),0) as versionDistributivoOferta "+
				" FROM PeriodoAcademico pa " + 
				" WHERE pa.estado='A' "+
				" and pa.fechaDesde>=(select pa1.fechaDesde from PeriodoAcademico pa1 where (GETDATE()>=pa1.fechaDesde) and (GETDATE()<=pa1.fechaHasta))"+
				" order by pa.descripcion desc")

		List<CustomObjectPeriodosVigenteFuturos> listaPeriodosVigenteFuturo(Integer idOferta);		
		interface CustomObjectPeriodosVigenteFuturos  { 
		    Integer getId(); 
		    String getCodigo(); 
		    String getDescripcion(); 
		    Integer getIdDistributivoOferta();
		    Integer getVersionDistributivoOferta();	  
		}
					
		   @Query(value=" SELECT ma.id as idMallaAsignatura ,m.id as idMalla,n.id as idNivel,n.descripcion as nivel, "+
		   		" a.id as idAsignatura, a.descripcion as descripcion, pp.id as idPlanificacionParalelo,pm.idPeriodoAcademico as idPeriodoAcademico, "+
		   		" isnull( pp.numParalelos,0) as numParalelos, pp.version as version, pp.estado as estado "+
		   		" FROM Malla m "+
		   		" INNER JOIN DepartamentoOferta dof on m.idDepartamentoOferta=dof.id "+
		   		" INNER JOIN PeriodoMalla pm on m.id=pm.idMalla "+
		   		" INNER JOIN MallaAsignatura ma on m.id=ma.idMalla " + 
		   		" INNER JOIN Asignatura a on ma.idAsignatura= a.id " + 
		   		" INNER JOIN Nivel n on n.id=ma.idNivel " +
		   		" LEFT JOIN PlanificacionParalelo pp on ma.id=pp.idMallaAsignatura and pp.idPeriodoAcademico=pm.idPeriodoAcademico and pp.estado='A' " + 
		   		" WHERE m.estado in ('A', 'P') and pm.estado='A' and ma.estado='A' and a.estado='A' and dof.estado='A'   " + 
		   		" and ma.idNivel >= m.idNivelMinAperturado "+// ( SELECT m1.idNivelMinAperturado "+
			//						" FROM DepartamentoOferta as do1  "+
			//						" INNER JOIN Malla as m1 on do1.id=m1.idDepartamentoOferta "+
			//						" INNER JOIN PeriodoMalla as pm1 on m1.id=pm1.idMalla "+
			//						" WHERE  pm1.idPeriodoAcademico=(?1) and do1.idOferta=(?2)  "+
			//						" and do1.estado='A' and m1.estado in ('A', 'P') and pm1.estado='A' ) "+
				" and ma.idNivel <= m.idNivelMaxAperturado "+// ( SELECT m1.idNivelMaxAperturado "+
			//						" FROM  DepartamentoOferta do1  "+
			//						" INNER JOIN Malla m1 on do1.id=m1.idDepartamentoOferta "+
			//						" INNER JOIN PeriodoMalla pm1 on m1.id=pm1.idMalla "+
			//						" WHERE  pm1.idPeriodoAcademico=(?1) and do1.idOferta=(?2) "+
			//						"and do1.estado='A' and m1.estado in ('A', 'P') and pm1.estado='A') "+
		   		" and pm.idPeriodoAcademico=(?1) and dof.idOferta =(?2) " +
				"Group by ma.id,m.id, n.id,a.id,a.descripcion,pp.id,pm.idPeriodoAcademico,pp.numParalelos,pp.version,pp.estado,n.descripcion ,m.idNivelMaxAperturado , m.idNivelMinAperturado "+
				" ORDER BY n.id ")
			List<CustomObject3> recuperarConfiguracionParalelo(Integer idPeriodo,Integer IdOferta);	

			interface CustomObject3  { 
				Integer getIdMallaAsignatura();
			    Integer getIdMalla(); 
			    Integer getIdNivel(); 
			    String getNivel();
			    Integer getIdAsignatura(); 
			    String getDescripcion();
			    Integer getIdPlanificacionParalelo();
			    Integer getIdPeriodoAcademico();
			    Integer getNumParalelos(); 
			    Integer getVersion(); 
			    String getEstado();
				  
			}
			
			/**
			 * funcion que retorna una lista del detalle de las asignatura asignada a un distributivo oferta version
			 * @param pi_id_periodo_academico
			 * @param pi_id_departamento
			 * @param pi_id_oferta
			 * @param pi_id_distributivo_oferta_version
			 * @param pi_id_docente
			 * @return
			 */
			/*
			@Query(value = "select d.id as id, d.id_distributivo_oferta_version as idDistributivoOfertaVersion, d.id_distributivo_docente as idDistributivoDocente,"+
					" d.id_docente as idDocente, d.oferta_principal as ofertaPrincipal, d.oferta_asignatura as ofertaAsignatura, "+
					" d.id_asignatura as idAsignatura, d.asignatura as asignatura, d.paralelo as paralelo, d.id_paralelo as idParalelo,"+
					" d.num_estudiantes as numEstudiantes, d.docencia as docencia, d.practicas as practicas, d.horas_clases as horasClases,"+
					" d.id_dedicacion as idDedicacion, d.dedicacion as dedicacion " + 
					" from aca.fn_docente_asignatura_oferta_version"+
					" (:pi_id_periodo_academico, :pi_id_departamento, :pi_id_oferta, :pi_id_distributivo_oferta_version,:pi_id_docente) as d ", nativeQuery = true)*/
			
			@Query(value = "select d.id_distributivo_oferta_version as idDistributivoOfertaVersion, d.id_distributivo_docente as idDistributivoDocente, " + 
					"d.id_docente_asignatura_aprendizaje as idDocenteAsignaturaAprendizaje, d.id_docente as idDocente, d.oferta_principal as ofertaPrincipal, " + 
					" d.id_oferta_asignatura as idOfertaAsignatura, d.oferta_asignatura as ofertaAsignatura, d.id_asignatura_aprendizaje as idAsignaturaAprendizaje, " + 
					" d.id_malla_asignatura as idMallaAsignatura, d.id_asignatura as idAsignatura, d.asignatura as asignatura, d.paralelo as paralelo, d.id_paralelo as idParalelo, " + 
					" d.num_estudiantes as numEstudiantes,d.id_componente_aprendizaje as  idComponenteAprendizaje ,d.codigo_comp as codigoComp,"+
					"d.descripcion_codigo_comp as descripcionCodigoComp, d.codigo as codigo, d.descripcion_codigo as descripcionCodigo,d.valor as valor, " + 
					"id_distributivo_dedicacion as idDistributivoDedicacion, d.id_dedicacion as idDedicacion, d.dedicacion as dedicacion, " + 
					"d.version_dde as versionDde, d.version_daa as versionDaa"+
					" from aca.fn_docente_distributivo_asignatura_oferta "+
					" (:pi_id_periodo_academico, :pi_id_departamento, :pi_id_oferta, :pi_id_distributivo_oferta_version,:pi_id_docente) as d "
					+ " order by d.id_distributivo_oferta_version,idDistributivoDocente,idAsignatura ", nativeQuery = true)
			public List<CustomObjectDocenteAsignaturaOfertaVersion> getFnDocenteAsignaturaOfertaVersion(
					@Param("pi_id_periodo_academico") Integer pi_id_periodo_academico,
					@Param("pi_id_departamento") Integer pi_id_departamento,
					@Param("pi_id_oferta") Integer pi_id_oferta,
					@Param("pi_id_distributivo_oferta_version") Integer pi_id_distributivo_oferta_version,
					@Param("pi_id_docente") Integer pi_id_docente);
			
			public interface CustomObjectDocenteAsignaturaOfertaVersion{
		
				Integer getIdDistributivoOfertaVersion();
				Integer getIdDistributivoDocente();
				Integer getIdDocenteAsignaturaAprendizaje();
				Integer getIdDocente();
				Boolean getOfertaPrincipal();
				Integer getIdOfertaAsignatura();
				String getOfertaAsignatura();
				Integer getIdAsignaturaAprendizaje();
				Integer getIdMallaAsignatura();
				Integer getIdAsignatura();
				String getAsignatura();
				String getParalelo();
				Integer getIdParalelo();
				Integer getNumEstudiantes();
				Integer getIdComponenteAprendizaje();
				String getCodigoComp();
				String getDescripcionCodigoComp();
				String getCodigo();
				String getDescripcionCodigo();
				Integer getIdDistributivoDedicacion();
				Integer getValor();
				Integer getIdDedicacion();
				String getDedicacion();
				Integer getVersionDde();
				Integer getVersionDaa();
				
			}
			
		@Query (value= "select id_docente as idDocente , apellidos_nombres as apellidosNombres,horas_clases as horasClases  "+
				 " from [aca].fn_docentes_no_asignado_distributivo(:pi_id_periodo_academico, :pi_id_distributivo_oferta_version) as d ", nativeQuery = true)
		public List<CustomObjectFnDocentesNoAsignadoDistributivo> getFnDocentesNoAsignadoDistributivo(
		@Param("pi_id_periodo_academico") Integer pi_id_periodo_academico,
		@Param("pi_id_distributivo_oferta_version") Integer pi_id_distributivo_oferta_version);
		public interface CustomObjectFnDocentesNoAsignadoDistributivo{
			Integer getIdDocente();
			String getApellidosNombres();
			Integer getHorasClases();
			
			
		}
		
		@Query (value= "SELECT CASE WHEN CONCAT (cast(per.apellidos as text),' ', cast(per.nombres as text)) =' ' "+
		"THEN 'POR DEFINIR' ELSE CONCAT (per.apellidos,'',per.nombres) END as apellidosNombres, "+
		"CASE WHEN dod.descripcion IS NULL THEN	 'DOCENTE DE OTRA CARRERA' else dod.descripcion end as dedicacion ,"+
				"0 as totalHorasClases, 0 as horasActividad, 0 as horasTotal, "+
	    " do.idDistributivoGeneralVersion as idDistributivoGeneralVersion, dov.fechaDesde as fechaDesde, "+	
	    " (SELECT min( dov3.fechaDesde ) "+
	    " FROM DistributivoOferta do3 "+
	    " INNER JOIN DistributivoOfertaVersion dov3 on do3.id=dov3.idDistributivoOferta "+
	    " WHERE dov3.idDistributivoOferta=dov.idDistributivoOferta and "+
		"	dov3.versionDistributivoOferta>dov.versionDistributivoOferta and dov3.estado in ('A' , 'V', 'R') and do3.estado='A' "+
		" )  as fechaHasta, "+
	    " dd.id as idDistributivoDocente, dov.idDistributivoOferta as idDistributivoOferta, dov.versionDistributivoOferta as versionDistributivoOferta"+
		"	FROM PeriodoAcademico p "+
		"	inner join DistributivoGeneral dg on p.id=dg.idPeriodoAcademico "+
		"	inner join DistributivoGeneralVersion dgv on dg.id=dgv.idDistributivoGeneral "+
		"	inner join DistributivoOferta do on dgv.id=do.idDistributivoGeneralVersion "+
		"	inner join DistributivoOfertaVersion dov on do.id=dov.idDistributivoOferta "+
		"	inner join DistributivoDocente dd on dov.id=dd.idDistributivoOfertaVersion "+
		"	left join DistributivoDedicacion dde on dd.id=dde.idDistributivoDocente and dde.estado='A' "+
		"	left join DocenteDedicacion dod on dde.idDocenteDedicacion=dod.id and dod.estado='A' "+
		"	left join Docente d on dd.idDocente=d.id and d.estado='A' "+
		"	left join Persona per on d.idPersona=per.id and per.estado='AC' "+
		"	where do.estado='A' and dov.estado in ('A' , 'V', 'R') and dd.estado='A'  "+
		"	and dov.id=(?1) ")
			
		List<CustomObjDocenteDistributivoOferta> listaDistributivoOfertaVersion(Integer idDistributivoOfertaVersion);	
		public interface CustomObjDocenteDistributivoOferta{
			String getApellidosNombres();
			String getDedicacion();
			Integer getTotalHorasClases();
			Integer getHorasActividad();
			Integer getHorasTotal();
			Integer getIdDistributivoGeneralVersion();
			Date getFechaDesde();
			Date getFechaHasta();
			Integer getIdDistributivoDocente();
			Integer getIdDistributivoOferta();
			Integer getVersionDistributivoOferta();
			
//				Integer getIdOfertaDocente();
//				String getOfertaDocente();
//				Integer getIdDocente();
//				String getIdentificacion();
//				String getApellidosNombres();
//				Integer getIdDistributivoDocente();
//				String getDedicacion();
//				Integer getIdParalelo();
//				String getParalelo();
//				String getOfertaAsignatura();
//				String getAsignatura();
//				Integer getNumEstudiantes();
//				Integer getHorasClases();
//				Integer getTotalHorasClases();
//				Integer getHorasActividad();
		

			}
		
		@Query (value= "select d.id_oferta_docente as idOfertaDocente , d.oferta_docente as ofertaDocente, d.id_docente as idDocente, " + 
				"d.identificacion as identificacion, d.apellidos_nombres as apellidosNombres, d.id_distributivo_docente as idDistributivoDocente ," + 
				"d.dedicacion as dedicacion , d.id_paralelo as idParalelo , d.paralelo as paralelo, d.oferta_asignatura as ofertaAsignatura," + 
				"d.asignatura as asignatura, d.num_estudiantes as numEstudiantes, d.horas_clases as horasClases, d.horas_clases_total as totalHorasClases ," + 
				"d.horas_actividad as horasActividad " + 
				" from [aca].[fn_distributivo_oferta_version](:pi_id_periodo_academico, :pi_id_distributivo_oferta_version) as d ", nativeQuery = true)
		public List<CustomObjectDocenteDistributivoOferta> getFnDistributivoOfertaVersion(
		@Param("pi_id_periodo_academico") Integer pi_id_periodo_academico,
		@Param("pi_id_distributivo_oferta_version") Integer pi_id_distributivo_oferta_version);
		public interface CustomObjectDocenteDistributivoOferta{
			Integer getIdOfertaDocente();
			String getOfertaDocente();
			Integer getIdDocente();
			String getIdentificacion();
			String getApellidosNombres();
			Integer getIdDistributivoDocente();
			String getDedicacion();
			Integer getIdParalelo();
			String getParalelo();
			String getOfertaAsignatura();
			String getAsignatura();
			Integer getNumEstudiantes();
			Integer getHorasClases();
			Integer getTotalHorasClases();
			Integer getHorasActividad();
	

		}
//		@Query(value = "{call  aca.sp_rpt_distributivo_docente_departamento(:pi_id_periodo_academico,:pi_id_departamento,:pi_id_oferta, :pi_id_distributivo_oferta_version)}", nativeQuery = true)
//		public List<CustomObjectCargarDocenteDistributivo> getSPDistributivoDocente(@Param("pi_id_periodo_academico") Integer pi_id_periodo_academico,
//				@Param("pi_id_departamento") Integer pi_id_departamento, @Param("pi_id_oferta") Integer pi_id_oferta, @Param("pi_id_distributivo_oferta_version") Integer pi_id_distributivo_oferta_version);
//		
		
		
		/**
		 * interfaz para recuperar las variables de procedimiento almacenado CustomObjectCargarDocenteDistributivo
		 * @author msoriano
		 *
		 */
//		public interface CustomObjectCargarDocenteDistributivo{
//			String getIdentificacion();
//			String getApellidos_nombres();
//			String getGenero();
//			Integer getId_dedicacion();
//			String getDedicacion();
//			Integer getId_categoria();
//			String getCategoria();
//			Integer getId_docente();
//			Integer getId_distributivo_docente();
//			Integer getTotal_horas_clases();
//			String getActividad();
//			Integer getActividad_valor();
//	
//
//		}
			
}
		
