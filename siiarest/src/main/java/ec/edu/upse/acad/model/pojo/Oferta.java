package ec.edu.upse.acad.model.pojo;


import javax.persistence.*;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;


@Entity
@Table(schema="aca", name="oferta")
@Where(clause = "estado='A'")
@NoArgsConstructor
public class Oferta {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_oferta")
	@Getter @Setter private Integer id;
	
	@Column(name="id_tipo_oferta")
	@Getter @Setter private Integer idTipoOferta;
	
	@Getter @Setter private String descripcion;

	@Column(name="descripcion_corta")
	@Getter @Setter private String descripcionCorta;
	
	@Getter @Setter private String prefijo;

	@Getter @Setter private String estado;

	@Column(name="fecha_aprobacion")
	@Getter @Setter private Date fechaAprobacion;

	@Column(name="fecha_cierre")
	@Getter @Setter private Date fechaCierre;

	@Column(name="fecha_fin_oferta")
	@Getter @Setter private Date fechaFinOferta;

	@Column(name="fecha_ingreso")
	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="fecha_inicio_oferta")
	@Getter @Setter private Date fechaInicioOferta;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;
	
	//RELACIONES
	//bi-directional many-to-one association to DepartamentoOferta
	@OneToMany(mappedBy="oferta", cascade=CascadeType.ALL)
	//@JsonIgnore
	@Getter @Setter private List<DepartamentoOferta> departamentoOfertas;
	
	//bi-directional many-to-one association to DepartamentoOferta
	@OneToMany(mappedBy="oferta", cascade=CascadeType.ALL)
	//@JsonIgnore
	@Getter @Setter private List<OfertaDocente> ofertaDocentes;
		
	//bi-directional many-to-one association to OfertaAsignatura
	@OneToMany(mappedBy="oferta", cascade=CascadeType.ALL)
//	@JsonIgnore
	@Getter @Setter private List<OfertaAsignatura> ofertaAsignatura;
	
	//bi-directional many-to-one association to Tipo de Oferta
	@ManyToOne
	@JoinColumn(name="id_tipo_oferta" , insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private TipoOferta tipoOferta;
	@ManyToOne

	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }

}