package ec.edu.upse.acad.model.pojo.vinculacion;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema = "vin", name = "asignacion_actividad")

@NoArgsConstructor //un constructorsin argumentos
public class AsignacionActividad {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_asignacion_actividad")
	@Getter	@Setter	private Integer id;

	@Column(name = "id_proyecto")
	@Getter	@Setter	private Integer idProyecto;
	
	@Column(name = "id_persona")
	@Getter	@Setter	private Integer idPersona;
	
	@Column(name = "id_objetivo_actividad")
	@Getter	@Setter	private Integer idObjetivoActividad;
	
	@Column(name = "id_objetivo_tarea")
	@Getter	@Setter	private Integer idObjetivoTarea;
		
	@Column(name = "tipo_participante")
	@Getter	@Setter	private String tipoParticipante;
	
	@Column(name = "usuario_ingreso_id")
	@Getter	@Setter	private String usuarioIngresoId;

	@Version
	@Getter	@Setter	private Integer version;

	@Getter	@Setter	private String estado;

}
