package ec.edu.upse.acad.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ec.edu.upse.acad.model.pojo.seguridad.PersonaDepartamento;


public interface PersonasDepartamentosRepository 
	  extends JpaRepository<PersonaDepartamento, Integer>{
}
