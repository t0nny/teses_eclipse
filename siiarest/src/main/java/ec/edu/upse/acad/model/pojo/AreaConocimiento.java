package ec.edu.upse.acad.model.pojo;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ec.edu.upse.acad.model.pojo.planificacion_docente.AreaBibliografia;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="dbo", name="area_conocimiento")
@NoArgsConstructor
public class AreaConocimiento {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_area_conocimiento")
	@Getter @Setter private Integer id;
	
	@Getter @Setter private String codigo;
	
	@Getter @Setter private String descripcion;
	
//	@Column(name="fecha_ingreso")
//	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Getter @Setter private String estado;

	@Version
	@Getter @Setter private Integer version;
	
	//Relaciones
	//bi-directional many-to-one association to FormacionProfesional
	@OneToMany(mappedBy="areaConocimiento", cascade=CascadeType.ALL)
	@JsonIgnore
	@Getter @Setter private List<FormacionProfesional> formacionProfesionales;
	
	//bi-directional many-to-one association to FormacionProfesional
	@OneToMany(mappedBy="areaConocimiento", cascade=CascadeType.ALL)
	@JsonIgnore
	@Getter @Setter private List<ActualizacionProfesional> actualizacionProfesionales;
	
	//bi-directional many-to-one association to AreaBibliografia
	@OneToMany(mappedBy="areaConocimiento", cascade=CascadeType.ALL)
	@Getter @Setter private List<AreaBibliografia> areaBibliografias;

	@PrePersist
	void preInsert() {
	   if (this.estado == null)
	       this.estado = "A";
	}
}
