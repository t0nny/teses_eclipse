package ec.edu.upse.acad.model.pojo;


import javax.persistence.*;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(schema="aca", name="tipo_documento")
@NoArgsConstructor
public class TipoDocumento {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_tipo_documento")
	@Getter @Setter private Integer id;

	@Getter @Setter private String descripcion;

	@Getter @Setter private String estado;
	
	@Column(name="fecha_ingreso")
	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;
	
	
	//RELACIONES

	//bi-directional many-to-one association to Archivo
	@OneToMany(mappedBy="tipoDocumento", cascade=CascadeType.ALL)
	@Getter @Setter private List<Archivo> archivos;

	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }
}