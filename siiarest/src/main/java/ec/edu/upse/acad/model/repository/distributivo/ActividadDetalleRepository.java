package ec.edu.upse.acad.model.repository.distributivo;



import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


import ec.edu.upse.acad.model.pojo.reglamento.ActividadDocenteDetalle;
import ec.edu.upse.acad.model.repository.distributivo.ActividadPersonalRepository.CustomObject;


@Repository
public interface ActividadDetalleRepository extends JpaRepository<ActividadDocenteDetalle, Integer> {

	
	@Query(value = "{call aca.sp_detalle_actividad_docente(:id_distributivo_docente)}", nativeQuery = true)
	public List<CustomObjectDetalleaActividadDocente> getSpDetalleActividadDocente(@Param("id_distributivo_docente") Integer id_distributivo_docente);
	/**
	 * interfaz para recuperar las variables de procedimiento almacenado CustomObjectCargarAsignaturasMatriculas
	 * @author msoriano
	 */
	public interface CustomObjectDetalleaActividadDocente{
		
		Integer getId();
		Integer getIdDocenteActividad();
		Integer getIdActividadPersonal();
		String getDescripcionActividad();	
		Integer getIdActividadDetalle();
		String getDescripcionDetalleAct();
		Integer getValor();
		
		
	}
	@Query(value=" SELECT apd.id as idActividadPersonal, apd.descripcion as descripcionActividad , COUNT (adde.id ) as cantidad" + 
			" FROM ActividadPersonalDocente apd " + 
			" INNER JOIN ReglamentoActividadDocente rad on apd.id=rad.idActividadPersonal" + 
			" INNER JOIN ReglamentoActividadDetalle rade on rad.id= rade.idReglamentoActividad " + 
			" INNER JOIN ActividadDocenteDetalle adde on rade.idActividadDetalle = adde.id " + 
			" WHERE rad.estado='A' and rade.estado='A' and adde.estado='A' "+
			" and apd.estado='A' "+
			" GROUP BY  apd.id , apd.descripcion "+
			" ORDER BY apd.descripcion "	)
	List<CustomObjectActividad> listarActividadPersonal();
	interface CustomObjectActividad  { 
	    Integer getIdActividadPersonal(); 
	    String getDescripcionActividad(); 
	    Integer getCantidad(); 
	}
	
	
	@Query(value=" SELECT rad.id as idReglamentoActividad, rad.idActividadPersonal as idActividadPersonal, " + 
			" adde.id as idActividadDetalle, adde.descripcion as descripcionDetalleAct " + 
			" FROM  ReglamentoActividadDocente rad " + 
			" INNER JOIN ReglamentoActividadDetalle rade on rad.id= rade.idReglamentoActividad " + 
			" INNER JOIN ActividadDocenteDetalle adde on rade.idActividadDetalle = adde.id " + 
			" WHERE rad.estado='A' and rade.estado='A' and adde.estado='A' "+
			" ORDER BY rad.idActividadPersonal, adde.descripcion "	)
	List<CustomObjectActividadDetalle> listarActividadDetalle();
	interface CustomObjectActividadDetalle  { 
	    Integer getIdReglamentoActividad(); 
	    Integer getIdActividadPersonal(); 
	    Integer getIdActividadDetalle(); 
	    String getDescripcionDetalleAct(); 
	}
	
	@Query(value=" SELECT adde.id as idActividadDocenteDetalle, adde.descripcion as actividadDocenteDetalle,"+
			" adde.orden as orden, adde.estado as estado, adde.version as version , adde.usuarioIngresoId as usuarioIngresoId,	 " + 
			" (select count (rad.id) from ReglamentoActividadDetalle rad where rad.estado='A' and adde.id=rad.idActividadDetalle ) as cantReglAct, "+
			" (select count (da.id) from DocenteActividad da where da.estado='A' and adde.id=da.idActividadDetalle ) as cantDistributivo"+
			" FROM ActividadDocenteDetalle adde  " + 
			" WHERE adde.estado='A' "+
			" ORDER BY  adde.orden "	)
	List<CustomObjectTodoActividadDetalle> listarTodoActividadDetalle();
	interface CustomObjectTodoActividadDetalle  { 
	    Integer getIdActividadDocenteDetalle(); 
	    String getActividadDocenteDetalle(); 
	    Integer getOrden(); 
	    String getEstado(); 
	    Integer getVersion(); 
	    Integer getUsuarioIngresoId(); 
	    Integer getCantReglAct(); 
	    Integer getCantDistributivo(); 
	}
	
	
}
