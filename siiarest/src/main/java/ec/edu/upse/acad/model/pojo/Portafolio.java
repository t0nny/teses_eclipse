package ec.edu.upse.acad.model.pojo;

import java.sql.Date;

import javax.persistence.*;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Entity
@Table(schema="proy", name="portafolios")
@NoArgsConstructor
public class Portafolio{

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Getter @Setter private Integer id;

	@Getter @Setter private String codigo;

	@Getter @Setter private String descripcion;

	@Getter @Setter private String estado;

	@Column(name="fecha_aprobacion")
	@Getter @Setter private Date fechaAprobacion;

	@Column(name="fecha_ing")
	@Getter @Setter private String fechaIng;

	@Column(name="fecha_mod")
	@Getter @Setter private String fechaMod;

	@Column(name="fecha_recepcion")
	@Getter @Setter private Date fechaRecepcion;

	@Getter @Setter private String grupo;

	@Getter @Setter private String imagen;

	@Column(name="imagen_tamano")
	@Getter @Setter private Integer imagenTamano;

	@Getter @Setter private String item;

	@Column(name="proceso_id")
	@Getter @Setter private Integer procesoId;

	@Getter @Setter private String resumen;

	@Column(name="tipo_id")
	@Getter @Setter private Integer tipoId;

	@Getter @Setter private String titulo;

	@Column(name="usuario_ing")
	@Getter @Setter private String usuarioIng;

	@Column(name="usuario_mod")
	@Getter @Setter private String usuarioMod;

	@Getter @Setter private Integer version;

	@Getter @Setter private boolean vigente;
	
	//RELACIONES

	//bi-directional many-to-one association to Miembro
	@OneToMany(mappedBy="portafolio", cascade=CascadeType.ALL)
	@Getter @Setter private List<Miembro> miembros;
	
	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "AC";
	 }
}