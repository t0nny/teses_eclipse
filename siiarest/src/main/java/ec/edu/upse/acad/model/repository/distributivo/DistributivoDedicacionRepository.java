package ec.edu.upse.acad.model.repository.distributivo;



import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.distributivo.DistributivoDedicacion;

@Repository
public interface DistributivoDedicacionRepository extends JpaRepository<DistributivoDedicacion, Integer> {
	/***
	 * Consulta que retorna el id dedicacion del docente que este en otro distributivo dedicacion
	 */
	@Query(value=" SELECT de.idDocenteDedicacion as idDocenteDedicacion"+
			" from DistributivoDocente as dd "+
			" INNER JOIN DistributivoDedicacion as de on dd.id=de.idDistributivoDocente "+
			" WHERE dd.idDocente =(?1) and "+
			" ((de.fechaDesde>= (?2)) AND (de.fechaHasta <=(?3)) " + 
			" OR (de.fechaDesde <=(?2)) AND (de.fechaHasta >=(?2) AND de.fechaHasta<= (?3))" + 
			" OR (de.fechaDesde >= (?2) AND de.fechaDesde<= (?3)) AND (de.fechaHasta >=(?3))) "+
			" and dd.estado = 'A' and de.estado='A' ")
	List<CustomObjectDedicacionDocente> cargarDedicacionDocente(Integer idDocente, Date fechaDesde, Date fechaHasta);
	interface CustomObjectDedicacionDocente{ 
	    Integer getIdDocenteDedicacion();     
	}
	
	/**
	 * consulta que retorna la lista de la jordana academica obteniendo el id y la descripción de la jornada
	 * @return
	 */
	@Query(value=" select j.id as idJornada,j.descripcion as jornada from Jornada j  where j.estado = 'A' ")
	List<CustomObjectJ> cargarJornadas();
	interface CustomObjectJ{ 
	    Integer getIdJornada(); 
	    String getJornada(); 
	} 
	
	/**
	 * consulta que retorna la lista de docente dedicacion obteniendo el id y la descripcion de la dedicación
	 * @return
	 */
	@Query(value=" select dd.id as idDocenteDedicacion, dd.descripcion as dedicacion from DocenteDedicacion dd where dd.estado = 'A' ")
	List<CustomObjectD> cargarDedicaciones();
	interface CustomObjectD{ 
	    Integer getIdDocenteDedicacion(); 
	    String getDedicacion(); 
	} 
	

	@Query(value=" select d.id as idFacultad,d.nombre as facultad  "+
			 " from Departamento d "+	 
			 " where  d.tipo = 'FAC' and d.estado = 'AC' "+
			 " group by d.id, d.nombre ")
	List<CustomObjectF> cargarFacultad();
	
	@Query(value=" select d.id as idFacultad,d.nombre as facultad  "+
			 " from Usuario u" + 
			 "	INNER JOIN Persona p on u.persona.id=p.id " + 
			 "	INNER JOIN PersonaDepartamento pd on p.id=pd.persona.id "+
			 " INNER JOIN Departamento d on pd.departamento.id=d.id "+
			 " INNER JOIN DepartamentoOferta do  on d.id = do.idDepartamento "+
			 " INNER JOIN Oferta o on do.idOferta=o.id "+
			 " INNER JOIN DistributivoOferta dof on o.id=dof.idOferta "+
			 " INNER JOIN DistributivoGeneralVersion dgv on dof.idDistributivoGeneralVersion=dgv.id "+
			 " where dgv.id=?1 and  d.tipo = 'FAC' and d.estado = 'AC' and do.estado='A' and o.estado='A' "+
			 "and dof.estado='A' and dgv.estado='A' and  u.usuario=?2  and  u.estado='AC' and p.estado='AC' and pd.estado='AC' "+
			 " group by d.id, d.nombre ")
	List<CustomObjectF> cargarFacultad(Integer idDistributivoGeneralVersion,String usuario);
	@Query(value=" select d.id as idFacultad,d.nombre as facultad  "+
			 " from  Departamento d  "+
			 " INNER JOIN DepartamentoOferta do  on d.id = do.idDepartamento "+
			 " INNER JOIN Oferta o on do.idOferta=o.id "+
			 " INNER JOIN DistributivoOferta dof on o.id=dof.idOferta "+
			 " INNER JOIN DistributivoGeneralVersion dgv on dof.idDistributivoGeneralVersion=dgv.id "+
			 " where dgv.id=?1 and  d.tipo = 'FAC' and d.estado = 'AC' and do.estado='A' and o.estado='A' "+
			 "and dof.estado='A' and dgv.estado='A'  "+
			 " group by d.id, d.nombre ")
	List<CustomObjectF> cargarFacultades(Integer idDistributivoGeneralVersion);
	interface CustomObjectF{ 
	    Integer getIdFacultad(); 
	    String getFacultad(); 
	}
	
	
	
	/**
	 * consulta que retorna la lista de las carreras filtrando por la facultad y departamento oferta 
	 * @param idFacultad
	 * @param idOferta
	 * @return
	 */
	@Query(value=" SELECT dof.id as idDistributivoOferta,do.id as idDepartamentoOferta, o.id as idCarrera , o.descripcion as carrera , ma.estado as estadoMalla "+
			" FROM Oferta o "+
			" INNER JOIN DepartamentoOferta do  on o.id = do.idOferta  "+
			" INNER JOIN DistributivoOferta dof on dof.id.idOferta=do.idOferta "+
			" INNER JOIN DistributivoGeneralVersion dgv on dof.idDistributivoGeneralVersion=dgv.id "+
			" INNER JOIN DistributivoGeneral dg on dgv.idDistributivoGeneral=dg.id "+
			" INNER JOIN Malla ma on ma.idDepartamentoOferta=do.id "+
			" INNER JOIN PeriodoMalla pm on ma.id=pm.idMalla and dg.idPeriodoAcademico=pm.idPeriodoAcademico "+
			" WHERE do.idDepartamento = (?1) and dgv.id=(?2) and o.estado='A' "+
			" and do.estado='A' and dof.estado='A' and dgv.estado='A' and dg.estado='A' and ma.estado in ('A', 'P') and pm.estado='A' "+
			" GROUP BY dof.id, do.id ,o.id, o.descripcion, ma.estado	")
	List<CustomObjectC> cargarCarreras(Integer idFacultad,Integer idDistributivoGeneralVersion);
	interface CustomObjectC{
		Integer getIdDistributivoOferta();
		Integer getIdDepartamentoOferta();
	    Integer getIdCarrera();
	    String getCarrera();
	    String getEstadoMalla();

	}
	
	@Query(value=" SELECT dgv.id as idDistributivoGeneralVersion,concat ( dg.descripcion  , ' - ' ,cast(dgv.versionDistributivoGeneral as text) ) as descripcion"+
			" FROM  DistributivoGeneralVersion dgv  "+
			" INNER JOIN DistributivoGeneral dg on dgv.idDistributivoGeneral=dg.id "+
			" WHERE  dg.idPeriodoAcademico=(?1) "+
			" and  dgv.estado='A' and dg.estado='A' "+
			" GROUP BY dgv.id,dg.descripcion, dg.fechaDesde,dgv.versionDistributivoGeneral")
	List<CustomObjectDistributivoG> cargarDistributivoGeneral(Integer idPeriodo);
	interface CustomObjectDistributivoG{
		Integer getIdDistributivoGeneralVersion();
	    String getDescripcion();

	}
	
	
	/**
	 * consulta que retorna la lsita de docente que no incluyen en un distributvo oferta version 
	 * @param idDistributivoOfertaVersion
	 * @return
	 */
	@Query(value="select d.id as idDocente, CONCAT(p.apellidos ,' ',p.nombres) as nombresCompletos "+
			",dc.id as idDocenteCategoria, dc.descripcion as docenteCategoria, od.idOferta as idOferta " + 
			" from Persona as p "+
			" inner join Docente as d on p.id = d.idPersona "+
			" inner join OfertaDocente as od on d.id=od.idDocente "+
			" inner join PeriodoAcademico as pa on od.idPeriodoAcademico=pa.id "+
			" inner join DocenteHistorial as dh on d.id=dh.idDocente " +
			" inner join DocenteCategoria as dc on dh.idDocenteCategoria=dc.id "+
			" where d.id=?2 and d.id  in  (select  isnull(dd.idDocente,0) from DistributivoDocente dd where dd.estado = 'A' and  dd.idDistributivoOfertaVersion =?1 )"+
			" and od.idPeriodoAcademico=?3 "+
			" and (dh.fechaHasta is null or dh.fechaHasta>=pa.fechaDesde) "+
			" and p.estado='AC' and d.estado='A' and dh.estado='A' and dc.estado='A' and od.estado='A' "+
			" group by d.id,p.apellidos,p.nombres ,dc.id , dc.descripcion,od.idOferta "
			)
	List<CustomObjectDOC> cargarDocentes(Integer idDistributivoOfertaVersion, Integer idDocente, Integer idPeriodo);
	
	/**
	 * consulta que retorna la lsita de docente 
	 * @param idDistributivoOfertaVersion
	 * @return
	 */
	@Query(value="select d.id as idDocente, CONCAT(p.apellidos ,' ',p.nombres) as nombresCompletos "+
			",dc.id as idDocenteCategoria, dc.descripcion as docenteCategoria, od.idOferta as idOferta " + 
			" from Persona as p "+
			" inner join Docente as d on p.id = d.idPersona "+
			" inner join OfertaDocente as od on d.id=od.idDocente "+
			" inner join PeriodoAcademico as pa on od.idPeriodoAcademico=pa.id "+
			" inner join DocenteHistorial as dh on d.id=dh.idDocente " +
			" inner join DocenteCategoria as dc on dh.idDocenteCategoria=dc.id "+
			" where  d.id not in (select isnull(dd.idDocente,0) "+
			"					  from DistributivoDocente dd "+
			"					  where dd.estado = 'A' and  dd.idDistributivoOfertaVersion =?1 )"+
			" and od.idPeriodoAcademico=?2 "+
			" and (dh.fechaHasta is null or dh.fechaHasta>=pa.fechaDesde) "+
			" and p.estado='AC' and d.estado='A' and dh.estado='A' and dc.estado='A' and od.estado='A'"+
			" group by d.id,p.apellidos,p.nombres ,dc.id , dc.descripcion,od.idOferta "
			)
	List<CustomObjectDOC> cargarTodosDocentes(Integer idDistributivoOfertaVersion, Integer idPeriodo);
	interface CustomObjectDOC{ 
	    Integer getIdDocente(); 
	    String getNombresCompletos(); 
	    Integer getIdDocenteCategoria(); 
	    String getDocenteCategoria(); 
	    Integer getIdOferta(); 
	}
	
	
	@Query(value=" select a.id as idAsignatura, a.descripcion as asignatura " + 
			" from Asignatura a "+
			" inner join MallaAsignatura ma on ma.idAsignatura = a.id "+
			" inner join PlanificacionParalelo pp on ma.id=pp.idMallaAsignatura "+
			" inner join Malla m on m.id = ma.idMalla "+
			" inner join DepartamentoOferta do on do.id = m.idDepartamentoOferta  "+
			" where pp.idPeriodoAcademico=(?1) and do.idOferta = (?2) and a.estado = 'A' and " +
			" (ma.idNivel>=m.idNivelMinAperturado and ma.idNivel<=m.idNivelMaxAperturado) and pp.numParalelos> " +
			" (select count(daa1.idParalelo) from DocenteAsignaturaAprend daa1, AsignaturaAprendizaje aa1, MallaAsignatura ma1 " + 
			" where daa1.idAsignaturaAprendizaje = aa1.id and ma1.id = aa1.idMallaAsignatura and ma1.idAsignatura = ma.idAsignatura "+
			" and daa1.estado = 'A') order by a.descripcion" 
			)
	List<CustomObjectM> cargarAsignaturasPorCarrera(Integer periodoAcademico,Integer idOferta);
	
	interface CustomObjectM{ 
	    Integer getIdAsignatura(); 
	    String getAsignatura(); 
	}
	
	/*@Query(value=" select ma.idAsignatura as idAsignatura, sum((case when aa.idComponenteAprendizaje  = 1  " + 
			" then aa.valor when aa.idComponenteAprendizaje  = 2    then aa.valor else 0 end)) as horas, " + 
			" ( case when aa.idComponenteAprendizaje  = 1  and aa.aplicaDistributivo= 'True'  then co.descripcion " + 
			" when aa.idComponenteAprendizaje  = 2  and aa.aplicaDistributivo= 'True'  then co.descripcion  else null end) as componentes " + 
			" from AsignaturaAprendizaje aa inner join MallaAsignatura ma " + 
			" on ma.id = aa.idMallaAsignatura inner join ComponenteAprendizaje co " + 
			" on co.id = aa.idComponenteAprendizaje " + 
			" where ma.idAsignatura = (?1) " + 
			" group by ma.idAsignatura,aa.idComponenteAprendizaje,aa.aplicaDistributivo, co.descripcion " + 
			" having sum((case when aa.idComponenteAprendizaje  = 1  and aa.aplicaDistributivo= 'True' " + 
			" then aa.valor when aa.idComponenteAprendizaje  = 2  and aa.aplicaDistributivo= 'True'  then aa.valor else 0 end))<>0 "
			)
	List<CustomObjectHC> generarHorasComponentesAsignatura(Integer idAsignatura);
	
	interface CustomObjectHC{ 
	    Integer getIdAsignatura(); 
	    Double getHoras();
	    String getComponentes(); 
	}*/
	
	
	@Query(value=" select aa.id as idAsignaturaAprendizaje, a.descripcion as asignatura, a.id as idAsignatura, aa.valor as horas, " + 
			" ca.id as idComponenteAprendizaje , ca.descripcion as componente " + 
			" from MallaAsignatura ma inner join AsignaturaAprendizaje aa " + 
			" on ma.id = aa.idMallaAsignatura inner join Asignatura a " + 
			" on a.id = ma.idAsignatura inner join ComponenteAprendizaje ca " + 
			" on ca.id = aa.idComponenteAprendizaje " + 
			" where  a.id =  (?1) " 
			)
	List<CustomObjectCA> recuperarComponentesAsignaturaId(Integer idAsignatura);
	
	interface CustomObjectCA{ 
		Integer getIdAsignaturaAprendizaje(); 
	    String getAsignatura();
	    Integer getIdAsignatura(); 
	    Double getHoras();
	    Integer getIdComponenteAprendizaje(); 
	    String getComponente(); 
 
	}
	
//	@Query(value=" select daa.id as idDocenteAsignaturaAprend, daa.estado as estado, daa.usuarioIngresoId as usuarioIngresoId," + 
//			" daa.fechaIngreso as fechaIngreso, daa.version as version from DocenteAsignaturaAprend daa inner join DistributivoDocente do " + 
//			" on daa.idDistributivoDocente = do.id " +
//			" where daa.idAsignaturaAprendizaje = (?1) and do.idDocente = (?2) " 
//			)
//	List<CustomObjectAA> recuperarIdDocenteAsignaturaAprend(Integer idAsignaturaAprendizaje,Integer idDocente);
//	
//	interface CustomObjectAA{ 
//		Integer getIdDocenteAsignaturaAprend(); 
//		String getEstado();
//		String getUsuarioIngresoId();
//		String getFechaIngreso();
//		Integer getVersion(); 
//	}
	
	
	@Query(value=" SELECT dhc.id as idDedicacionHorasClases, dd.numeroHoras as numeroHoras, dd.id as idDocentedDedicacion, "+
			" dc.id as idDocenteCategoria, "+
			" dhc.horasMaximo as horasMaximo, dhc.horasMinimo as horasMinimo " + 
			" FROM DocenteDedicacion as  dd " + 
			" INNER JOIN DedicacionHorasClase as dhc on dd.id=dhc.idDocenteDedicacion " + 
			" INNER JOIN DocenteCategoria as dc on dhc.idDocenteCategoria = dc.id " +  
			" WHERE  dd.id=(?1) and  dc.id=(?2) " + 
			" and dd.estado='A' and dc.estado='A' and dhc.estado='A'  ")
	List<CustomObjectDedicacionHorasClases> recuperarDedicacionHorasClases(Integer idDedicacion,Integer idCategoria);
	interface CustomObjectDedicacionHorasClases{ 
		Integer getIdDedicacionHorasClases(); 
		Integer getNumeroHoras();
		Integer getIdDocentedDedicacion();
		Integer getIdDocenteCategoria();
		Integer getHorasMaximo();
		Integer getHorasMinimo();	
	}
	
	@Query(value="SELECT dov.id as idDistributivoOfertaVersion, ddo.idDocente as idDocente, ddo.id as idDistributivoDocente, dde.idDocenteDedicacion as idDocenteDedicacion "+
			"FROM DistributivoGeneralVersion as dgv  "+
			"INNER JOIN DistributivoOferta as do on dgv.id=do.idDistributivoGeneralVersion "+
			"INNER JOIN DistributivoOfertaVersion as dov on do.id=dov.idDistributivoOferta "+
			"INNER JOIN DistributivoDocente as ddo on dov.id=ddo.idDistributivoOfertaVersion "+
			"INNER JOIN DistributivoDedicacion as dde on ddo.id=dde.idDistributivoDocente " +
			"WHERE dgv.estado='A' and do.estado='A' and dde.estado='A'and  ddo.estado='A' and dov.estado='A' "+
			"and ddo.idDocente=(?1) and dgv.id=(?3) and ddo.id not in ((?2))  "+
			"and dov.fechaDesde>=(?4) and ( dov.fechaDesde <=(?5) OR (?5) is null ) ")
	List<CustomObjectDedicacionDocenteOtroDist> DedicacionOtroDistr(Integer idDocente, Integer IdDistributivoDoc,Integer IdDistributivoGeneralVer, java.sql.Date  fechaDesde, java.sql.Date fechaHasta );	
	interface CustomObjectDedicacionDocenteOtroDist  { 
		Integer getIdDistributivoOfertaVersion();
		Integer getIdDocente();
		Integer getIdDistributivoDocente();
		Integer getIdDocenteDedicacion();
	
	}

}
