package ec.edu.upse.acad.ws;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.upse.acad.model.pojo.DepartamentoOferta;
import ec.edu.upse.acad.model.pojo.Oferta;
import ec.edu.upse.acad.model.pojo.TipoOferta;
import ec.edu.upse.acad.model.pojo.seguridad.Departamento;
import ec.edu.upse.acad.model.repository.DepartamentoOfertaRepository;
import ec.edu.upse.acad.model.repository.OfertaDocenteRepository;
import ec.edu.upse.acad.model.repository.OfertaRepository;
import ec.edu.upse.acad.model.repository.TipoOfertaRepository;
import ec.edu.upse.acad.model.service.SecurityService;

@RestController
@RequestMapping("/api/oferta")
@CrossOrigin

public class OfertaController {

	@Autowired private OfertaRepository ofertasRepository;
	@Autowired private OfertaDocenteRepository ofertaDocenteRepository;
	@Autowired private DepartamentoOfertaRepository departamentoOfertaRepository;
	@Autowired private TipoOfertaRepository tipoOfertaRepository;
	@Autowired private SecurityService securityService;
	//****************** SERVICIOS PARA OFERTA************************//
	@RequestMapping(value = "/buscarTipoOfertaAll", method = RequestMethod.GET)
	public ResponseEntity<?> buscarTipoOfertaALL(@RequestHeader(value="Authorization") String Authorization) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		List<TipoOferta> oferta= tipoOfertaRepository.findAll();
		return ResponseEntity.ok(oferta);
	}
	@RequestMapping(value = "/buscarDepartamentoOfertaALL", method = RequestMethod.GET)
	public ResponseEntity<?> buscarDepartamentoOfertaALL(@RequestHeader(value="Authorization") String Authorization) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		List<DepartamentoOferta> departamentoOferta=departamentoOfertaRepository.findAll();
		for (int i = 0; i < departamentoOferta.size(); i++) {
			for (int j = 0; j < departamentoOferta.get(i).getMalla().size(); j++) {
				departamentoOferta.get(i).getMalla().get(j).setDepartamentoOferta(null);
				departamentoOferta.get(i).getMalla().get(j).setEstudianteOfertas(null);
				departamentoOferta.get(i).getMalla().get(j).setListaMallaAsignaturas(null);
				departamentoOferta.get(i).getMalla().get(j).setPeriodoMallaVersion(null);
				departamentoOferta.get(i).getMalla().get(j).setMallaAsignaturas(null);
				departamentoOferta.get(i).getMalla().get(j).setMallaRequisitos(null);
				//departamentoOferta.get(i).getMalla().get(j).set
				
			}
			
		}
		return ResponseEntity.ok(departamentoOferta);
	}
	
	@RequestMapping(value = "/buscarOfertaAll", method = RequestMethod.GET)
	public ResponseEntity<?> buscarOfertaALL(@RequestHeader(value="Authorization") String Authorization) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		List<Oferta> oferta= ofertasRepository.findAll();
		return ResponseEntity.ok(oferta);
	}
	
	@RequestMapping(value = "/buscarDepartamentoOfertaK/{idTipoOferta}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarDepartamentoOfertaK(@PathVariable("idTipoOferta") Integer idTipoOferta,
			@RequestHeader(value="Authorization") String Authorization) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		
		return ResponseEntity.ok(ofertasRepository.listaOfertaDepartTipoId(idTipoOferta));
	}
	
	
	@RequestMapping(value = "/buscarOfertaId/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarOfertaId(@PathVariable("id") Integer id,
			@RequestHeader(value="Authorization") String Authorization) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		Oferta oferta= ofertasRepository.findById(id).get();
		return ResponseEntity.ok(oferta);
	}
	
	@RequestMapping(value = "/buscarOfertaTipoOf/{idTipoOferta}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarOferta(@RequestHeader(value="Authorization") String Authorization,
			@PathVariable("idTipoOferta") Integer idTipoOferta) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}

		return ResponseEntity.ok( ofertasRepository.listarOferta(idTipoOferta));
	}
	@RequestMapping(value = "/buscarOfertaTipoNoAsig/{idTipoOferta}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarOfertaTipoNoAsig(@RequestHeader(value="Authorization") String Authorization,
			@PathVariable("idTipoOferta") Integer idTipoOferta) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}

		return ResponseEntity.ok( ofertasRepository.listarOfertaNoAsignado(idTipoOferta));
	}
	@RequestMapping(value = "/buscarOfertaTipoMalla/{idTipoOferta}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarOfertaTipoMalla(@RequestHeader(value="Authorization") String Authorization,
			@PathVariable("idTipoOferta") Integer idTipoOferta) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}

		return ResponseEntity.ok( ofertasRepository.listarDepartOfertaMalla(idTipoOferta));
	}
	
	@RequestMapping(value="/grabarDepartamentoOferta", method=RequestMethod.POST)
	public ResponseEntity<?> grabarDepartamentoOferta(@RequestHeader(value="Authorization") String Authorization,
			@RequestBody List<DepartamentoOferta> listDepartamentoOferta) {
		System.err.println("debes entrar");
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		
		for (int i = 0; i < listDepartamentoOferta.size(); i++) {
			DepartamentoOferta _departamentoOferta = new DepartamentoOferta();
			if (listDepartamentoOferta.get(i).getId() != null) {
				BeanUtils.copyProperties(listDepartamentoOferta.get(i), _departamentoOferta,"malla");
				departamentoOfertaRepository.save(_departamentoOferta);
				//_oferta = ofertasRepository.findById(ofertasiga.getId()).get();
			}else {
				BeanUtils.copyProperties(listDepartamentoOferta.get(i), _departamentoOferta,"malla");
				departamentoOfertaRepository.save(_departamentoOferta);
			}	
		}
		
		
		
		return ResponseEntity.ok(listDepartamentoOferta);
	}

	/*
	@RequestMapping(value="/buscarOfertasPre", method=RequestMethod.GET)
	public ResponseEntity<?> buscarOfertasPre() {
		return ResponseEntity.ok(ofertasRepository.listaOfertaPre());
	}*/

	//Buscar todos las ofertas, para listar en angular
	@RequestMapping(value="/buscarOfertas/{tipos}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarOfertas(@PathVariable("tipos") String tipos) {
		return ResponseEntity.ok(ofertasRepository.listaOfertasTipos(tipos));
	}

	@RequestMapping(value="/buscarTipoOfertas", method=RequestMethod.GET)
	public ResponseEntity<?> buscarTipoOfertas() {
		return ResponseEntity.ok(tipoOfertaRepository.findAll());
	}


	//para probar servicio sin necesidad de Token en Reset Client
	@RequestMapping(value="/buscarTodosSinAut", method=RequestMethod.GET)
	public ResponseEntity<?> buscarTodos() {
		return ResponseEntity.ok(ofertasRepository.findAll());
	}

	//servicio que actualiza o crea un nuevo OFERTA	
	@RequestMapping(value="/buscarOferta/{id}", method=RequestMethod.GET)
	public ResponseEntity<?> buscar(@PathVariable("id") Integer id) {
		Oferta ofertassig;
		if (ofertasRepository.findById(id).isPresent()) {
			ofertassig = ofertasRepository.findById(id).get();
			return ResponseEntity.ok(ofertassig);
		}else {
			return ResponseEntity.notFound().build();
		}
	}

	@RequestMapping(value="/grabarOferta", method=RequestMethod.POST)
	public ResponseEntity<?> grabarOferta(@RequestBody Oferta ofertasiga) {
		Oferta _oferta = new Oferta();
		if (ofertasiga.getId() != null) {
			BeanUtils.copyProperties(ofertasiga, _oferta,"ofertaDocentes");
			ofertasRepository.save(_oferta);
			//_oferta = ofertasRepository.findById(ofertasiga.getId()).get();
		}else {
			BeanUtils.copyProperties(ofertasiga, _oferta,"ofertaDocentes");
			ofertasRepository.save(_oferta);
		}
		
		
		return ResponseEntity.ok(_oferta);
	}
	//***********************FIN SERVICIOS PARA OFERTA ***********************


	//****************** SERVICIOS PARA OFERTA VERSION************************//



	//filtra por el id del departamento
	@RequestMapping(value="/filtrarPorDepartamento/{idDepartamento}",method=RequestMethod.GET)
	public ResponseEntity<?> filtrarPorDepartamento(@PathVariable("idDepartamento") Integer idDepartamento) {
		return ResponseEntity.ok(ofertasRepository.listaOfertaDepartamento(idDepartamento));
	}

	@RequestMapping(value="/filtrarOfertaDepatamento", method=RequestMethod.GET)
	public ResponseEntity<?> filtrarOfertaDepatamento() {
		return ResponseEntity.ok(ofertasRepository.listaOfertaDepart());
	}

	/***
	 * retorna la lista de carreras filtrando por departamento y tipo de oferta 
	 * utilizado en la interfaz de reporte
	 * 
	 * @param authorization
	 * @param idDepartamento
	 * @param idTipoOferta
	 * @return
	 */
	//filtra por el id del departamento y id tipoOferta
	@RequestMapping(value="/filtrarPorDepartamentoTO/{idDepartamento}/{idTipoOferta}", method=RequestMethod.GET)
	public ResponseEntity<?> filtrarPorDepartamentoTipoOferta(@PathVariable("idDepartamento") Integer idDepartamento,
			@PathVariable("idTipoOferta") Integer idTipoOferta) {
		return ResponseEntity.ok(ofertasRepository.listaOfertaDepartamentoTipoOferta(idDepartamento,idTipoOferta));
	}

	/***
	 * retorna la lista de carreras filtrando portipo de oferta 
	 * utilizado en la interfaz de reporte
	 * 
	 * @param authorization
	 * @param idTipoOferta
	 * @return
	 */

	//listar carreras para usar como columanas en el kanban
	@RequestMapping(value="/listarCarrerascolumnas/{idDepartamento}/{idTipoOferta}", method=RequestMethod.GET)
	public ResponseEntity<?> listarCarrerascolumnas(@PathVariable("idDepartamento") Integer idDepartamento,@PathVariable("idTipoOferta") Integer idTipoOferta) {
		return ResponseEntity.ok(ofertasRepository.listaOfertaDepartamentoKanban(idDepartamento,idTipoOferta));
	}

	//filtra por el id del departamento y id tipoOferta
	@RequestMapping(value="/filtrarPorTipoOferta/{idTipoOferta}", 	method=RequestMethod.GET)
	public ResponseEntity<?> filtrarPorTipoOferta(@PathVariable("idTipoOferta") Integer idTipoOferta) {
		return ResponseEntity.ok(ofertasRepository.listaOfertaDelTipoOferta(idTipoOferta));
	}

	/***
	 * retorna una lista del distributivo oferta filtrando por tipo oferta y periodo academico
	 * utilizado para la interfaz de reporte
	 * @param authorization
	 * @param idPeriodo
	 * @return
	 */
	@RequestMapping(value="/filtrarDistributivoPorTipoOferta/{idTipoOferta}/{idPeriodo}", method=RequestMethod.GET)
	public ResponseEntity<?> 
	filtrarDistributivoPorTipoOferta(@PathVariable("idTipoOferta") Integer idTipoOferta,@PathVariable("idPeriodo") Integer idPeriodo) {
		return ResponseEntity.ok(ofertasRepository.listaDistributivoOfertaDelTipoOferta(idTipoOferta,idPeriodo ));
	}

	/***
	 * retorna una lista del distributivo oferta filtrando por periodo academico
	 * utilizado para la interfaz de reporte
	 * @param authorization
	 * @param idPeriodo
	 * @return
	 */
	@RequestMapping(value="/filtrarDistributivoOferta/{idPeriodo}", method=RequestMethod.GET)
	public ResponseEntity<?> filtrarDistributivoOferta(@PathVariable("idPeriodo") Integer idPeriodo) {
		return ResponseEntity.ok(ofertasRepository.listaDistributivoOferta(idPeriodo ));
	}


	@RequestMapping(value="/filtrarDocenteDistributivo/{idDepartamento}/{idPeriodoAcademico}/{idTipoOferta}", method=RequestMethod.GET)
	public ResponseEntity<?> filtrarDocenteDistributivo(@PathVariable("idDepartamento") Integer idDepartamento,
			@PathVariable("idPeriodoAcademico") Integer idPeriodoAcademico,
			@PathVariable("idTipoOferta") Integer idTipoOferta) {
		return ResponseEntity.ok(ofertaDocenteRepository.listarDocenteDistribu(idDepartamento, idPeriodoAcademico ,idTipoOferta));
	}
	//***********************FIN SERVICIOS PARA OFERTA VERSION***********************


}
