package ec.edu.upse.acad.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.seguridad.Modulo;




@Repository

public interface ModulosRepository 
       extends JpaRepository<Modulo, Integer>{

	
	@Query(value="SELECT e "
			+ "FROM Modulo AS e "
			+ "WHERE e.nombre LIKE ?1 "
			+ "OR e.descripcion LIKE ?1 ")
	List<Modulo> buscarPorNombre(String nombre);
	
	@Query(value="SELECT m "
			+ "FROM  Modulo AS m "
			+ "INNER JOIN ModuloRol AS mr ON m.id=mr.modulo.id "
			+ "INNER JOIN ModuloRolUsuario AS mru on mr.id=mru.moduloRol.id "
			+ "JOIN Usuario AS u ON mru.usuario.id=u.id "
			+ "WHERE u.usuario = ?1 "
			+ "GROUP BY m ")
	List<Modulo> buscarUsuarioModulos(String usuario);
}