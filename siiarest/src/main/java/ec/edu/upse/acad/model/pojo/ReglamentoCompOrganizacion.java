package ec.edu.upse.acad.model.pojo;


import java.sql.Timestamp;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ec.edu.upse.acad.model.pojo.reglamento.Reglamento;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="aca", name="reglamento_comp_organizacion")
@NoArgsConstructor
public class ReglamentoCompOrganizacion  {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_reglamento_comp_organizacion")
	@Getter @Setter private Integer id;
	
	@Column(name="id_comp_organizacion")
	@Getter @Setter private Integer idCompOrganizacion;
	
	@Column(name="id_reglamento")
	@Getter @Setter private Integer idReglamento;

	@Getter @Setter private String estado;
	
	@Column(name="fecha_ingreso")
	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;


	//RELACIONES
	//bi-directional many-to-one association to ComponenteOrganizacion
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_comp_organizacion", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private ComponenteOrganizacion componenteOrganizacion;

    //bi-directional many-to-one association to Reglamento
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="id_reglamento", insertable=false, updatable = false)
    @JsonIgnore
    @Getter @Setter private Reglamento reglamento;

	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }
}