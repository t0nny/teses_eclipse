package ec.edu.upse.acad.model.pojo.seguridad;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

//import org.eclipse.persistence.annotations.AdditionalCriteria;

import javax.persistence.Transient;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="man", name="opciones")
@NoArgsConstructor
//@AdditionalCriteria("this.estado='AC'")

public class Opcion {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)

	@Getter @Setter private Integer id;
	
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="modulo_id", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Modulo modulo;
	
	@Getter @Setter private Integer modulo_id;

	@Getter @Setter private String nombre;
	
	@Getter @Setter private String descripcion;
	
	@Getter @Setter private String tipo;
	
	@Getter @Setter private String url;
	
	@Getter @Setter private String icono;
	
	@Getter @Setter private Integer orden;
	@Column(name="padre_id")
	@Getter @Setter private Integer padreId ;
	
	@Getter @Setter private String estado;
	
	@PrePersist
	void preInsert() {
	   if (this.estado == null)
	       this.estado = "AC";
	   
	}

	@Version
	@Getter @Setter private Integer version;
}
