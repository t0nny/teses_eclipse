package ec.edu.upse.acad.model.pojo;

import javax.persistence.*;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="aca", name="oferta_asignatura")
@Where(clause = "estado='A'")
@NoArgsConstructor
public class OfertaAsignatura  {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_oferta_asignatura")
	@Getter @Setter private Integer id;
	
	@Column(name="id_asignatura")
	@Getter @Setter private Integer idAsignatura;
		
	@Column(name="id_oferta")
	@Getter @Setter private Integer idOferta;
	
	@Getter @Setter private String estado;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;

	//bi-directional many-to-one association to Asignatura
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_asignatura" ,insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Asignatura asignatura;
	
	//bi-directional many-to-one association to Oferta
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_oferta" , insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Oferta oferta;
	
	
	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }
}