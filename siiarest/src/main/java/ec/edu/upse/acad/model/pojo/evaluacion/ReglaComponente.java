package ec.edu.upse.acad.model.pojo.evaluacion;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ec.edu.upse.acad.model.pojo.reglamento.Reglamento;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="eva", name="regla_componente")
@NoArgsConstructor
public class ReglaComponente {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_regla_componente")
	@Getter @Setter private Integer id;
	
	@Column(name="id_reglamento")
	@Getter @Setter private Integer idReglamento;
	
	@Column(name="id_componente")
	@Getter @Setter private Integer idComponente;

	@Getter @Setter private String estado;
	
	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;
	
	@Version
	@Getter @Setter private Integer version;	

	//RELACIONES
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_reglamento", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Reglamento reglamento;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_componente", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Componente componente;
	
	@OneToMany(mappedBy="reglaComponente", cascade=CascadeType.ALL)
	@JsonIgnore
	@Getter @Setter private List<TipoEvaluacion> tipoEvaluacion;

	
	@PrePersist
	void preInsert() {
		if (this.estado == null)
			this.estado = "A";
	}

}
