package ec.edu.upse.acad.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.seguridad.Opcion;

@Repository

public interface OpcionesRepository
       extends JpaRepository<Opcion, Integer>{

	Opcion findById(String id);

	@Query(value="SELECT e "
			+ "FROM Opcion AS e "
		//	+ "WHERE e.nombre LIKE ?1 "
		//	+ "OR e.descripcion LIKE ?1 "
			+ "WHERE e.modulo_id = ?1 ")
	List<Opcion> buscarPorModuloId(Integer modulo_id);
	
	@Query(value="SELECT o "
			+ "FROM ModuloRolOpcion AS mro "
			+ "JOIN ModuloRol AS mr ON mro.moduloRol.id=mr.id  "
			+ "JOIN Opcion AS o ON mro.opcion.id=o.id "
		    + "WHERE mro.moduloRol.id = ?1 "
		    + "AND mro.estado = 'AC' "
		    + "AND o.tipo = 'Main Menu' ")
	List<Opcion> buscarPorModulosRolesOpcionesUsuario(Integer modulo_rol_id);
	
	
	
}