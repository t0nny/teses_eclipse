package ec.edu.upse.acad.ws;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ec.edu.upse.acad.model.pojo.seguridad.Modulo;
import ec.edu.upse.acad.model.pojo.seguridad.ModuloRol;
import ec.edu.upse.acad.model.pojo.seguridad.ModuloRolOpcion;
import ec.edu.upse.acad.model.pojo.seguridad.Opcion;
import ec.edu.upse.acad.model.pojo.seguridad.Rol;
import ec.edu.upse.acad.model.pojo.seguridad.RolDepartamento;
import ec.edu.upse.acad.model.pojo.seguridad.Subrol;
import ec.edu.upse.acad.model.repository.DepartamentosRepository;
import ec.edu.upse.acad.model.repository.ModulosRepository;
import ec.edu.upse.acad.model.repository.OpcionesRepository;
import ec.edu.upse.acad.model.repository.RolesDepartamentosRepository;
import ec.edu.upse.acad.model.repository.ModulosRolesOpcionesRepository;
import ec.edu.upse.acad.model.repository.ModulosRolesRepository;
import ec.edu.upse.acad.model.repository.RolesRepository;
import ec.edu.upse.acad.model.repository.SubrolesRepository;
import ec.edu.upse.acad.model.service.ModuloService;
import ec.edu.upse.acad.model.service.UpdateForIdService;

@RestController
@RequestMapping("/api/modulos")
@CrossOrigin

public class ModulosController {

	@Autowired
	private ModulosRepository modulosRepository;
	@Autowired
	private RolesRepository rolesRepository;
	@Autowired
	private ModulosRolesOpcionesRepository rolOpcionesRepository;
	@Autowired
	private ModulosRolesRepository moduloRolRepository;
	@Autowired
	private OpcionesRepository opcionesRepository;
	@Autowired
	private UpdateForIdService eliminaModulo;
	@Autowired
	private UpdateForIdService eliminaOpcion;
	@Autowired
	private UpdateForIdService eliminaRol;
	@Autowired
	private ModuloService moduloService;

	@Autowired
	private DepartamentosRepository departamentoRepository;
	@Autowired
	private RolesDepartamentosRepository rolDepartamentoRepository;
	@Autowired
	private SubrolesRepository subrolrepository;

	//****************** SERVICIOS PARA MODULOS************************//

	// Buscar todos los modulos, para listar en angular
	@RequestMapping(value = "/buscarModulos", method = RequestMethod.GET)
	public ResponseEntity<?> buscarModulos() {
		return ResponseEntity.ok(modulosRepository.findAll());
	}

	// para probar servicio sin necesidad de Token en Reset Client
	@RequestMapping(value = "/buscarTodosSinAut", method = RequestMethod.GET)
	public ResponseEntity<?> buscarTodos() {
		return ResponseEntity.ok(modulosRepository.findAll());
	}

	//servicio que actualiza o crea un nuevo modulo	
	@RequestMapping(value = "/buscarModulo/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> buscar(@PathVariable("id") Integer id) {
		Modulo modulossig;
		if (modulosRepository.findById(id).isPresent()) {
			modulossig = modulosRepository.findById(id).get();
			return ResponseEntity.ok(modulossig);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@RequestMapping(value = "/buscarPorNombre/{nombre}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarPorNombre(@PathVariable("nombre") String nombre) {
		return ResponseEntity.ok(modulosRepository.buscarPorNombre("%" + nombre + "%"));
	}

	@RequestMapping(value = "/grabarModulo", method = RequestMethod.POST)
	public ResponseEntity<?> grabarModulo(@RequestBody Modulo modulosiga) {
		Modulo _modulosiga = new Modulo();
		if (modulosiga.getId() != null) {
			_modulosiga = modulosRepository.findById(modulosiga.getId()).get();
		}
		BeanUtils.copyProperties(modulosiga, _modulosiga);
		modulosRepository.save(_modulosiga);
		return ResponseEntity.ok(_modulosiga);
	}

	//***********************FIN SERVICIOS PARA MODULOS***********************

	//****************** SERVICIOS PARA OPCIONES************************//
	@RequestMapping(value = "/buscarOpciones", method = RequestMethod.GET)
	public ResponseEntity<?> buscarOpciones() {
		return ResponseEntity.ok(opcionesRepository.findAll());
	}

	@RequestMapping(value = "/buscarOpciones/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarOpciones(@PathVariable("id") Integer id) {
		return ResponseEntity.ok(opcionesRepository.buscarPorModuloId(id));
	}

	@RequestMapping(value = "/buscarOpcion/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarOpcion(@PathVariable("id") Integer id) {
		Opcion opciones;
		if (opcionesRepository.findById(id).isPresent()) {
			opciones = opcionesRepository.findById(id).get();
			return ResponseEntity.ok(opciones);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	// Grabar Opciones de Modulos
	@RequestMapping(value = "/grabarModuloOpcion", method = RequestMethod.POST)
	public ResponseEntity<?> grabarModuloOpcion(@RequestBody Opcion moduloopcion) {
		Opcion _moduloopcion = new Opcion();
		if (moduloopcion.getId() != null) {
			_moduloopcion = opcionesRepository.findById(moduloopcion.getId()).get();
		}
		BeanUtils.copyProperties(moduloopcion, _moduloopcion);
		opcionesRepository.save(_moduloopcion);
		return ResponseEntity.ok(_moduloopcion);
	}

	// eliminar modulo
	@RequestMapping(value = "/borrar/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> borrar(@PathVariable("id") Integer id) {
		eliminaModulo.borraModulo(id);
		return ResponseEntity.ok().build();
	}

	// eliminar opcion
	@RequestMapping(value = "/borrarOpcion/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> borrarOpcion(@PathVariable("id") Integer id) {
		eliminaOpcion.borraOpcion(id);
		return ResponseEntity.ok().build();
	}
	//****************** FIN SERVICIOS PARA OPCIONES************************//	

	//****************** SERVICIOS PARA ROLES************************//

	/**
	 * 
	 * //Buscar todos los modulos, para listar en angular Buscar todos los modulos,
	 * para listar en angular
	 * 
	 * @param authorization de acuerdo al rol aasignado
	 * @return visualiza los tipos de roles
	 */
	@RequestMapping(value = "/buscarRoles", method = RequestMethod.GET)
	public ResponseEntity<?> buscarRoles() {
		return ResponseEntity.ok(rolesRepository.findAll());
	}

	@RequestMapping(value = "/buscarNivelAlto", method = RequestMethod.GET)
	public ResponseEntity<?> buscarNivelAlto() {
		return ResponseEntity.ok(rolesRepository.buscarNivelAlto());
	}

	@RequestMapping(value = "/buscarPorNivel/{nivel}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarPorNivel(@PathVariable("nivel") Integer nivel) {
		return ResponseEntity.ok(rolesRepository.buscarPorNivel(nivel));

	}

	@RequestMapping(value = "/buscarPorRolId/{rol_id}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarPorRolId(@PathVariable("rol_id") Integer rol_id) {
		return ResponseEntity.ok(subrolrepository.buscarPorRolId(rol_id));
	}

	@RequestMapping(value = "/buscarRol/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarRol(@PathVariable("id") Integer id) {
		Rol rol;
		if (rolesRepository.findById(id).isPresent()) {
			rol = rolesRepository.findById(id).get();
			return ResponseEntity.ok(rol);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	// Grabar Rol
	@RequestMapping(value = "/grabarRol", method = RequestMethod.POST)
	public ResponseEntity<?> grabarRol(@RequestBody Rol rol) {
		Rol _rol = new Rol();
		if (rol.getId() != null) {
			_rol = rolesRepository.findById(rol.getId()).get();
		}
		BeanUtils.copyProperties(rol, _rol);
		rolesRepository.save(_rol);
		return ResponseEntity.ok(_rol);
	}

	// eliminar modulo
	@RequestMapping(value = "/borrarRol/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> borrarRol(@PathVariable("id") Integer id) {
		eliminaRol.borraRol(id);
		return ResponseEntity.ok().build();
	}

	// grabar modulos_roles
	@RequestMapping(value = "/grabarModuloRol", method = RequestMethod.POST)
	public ResponseEntity<?> grabarModuloRol(@RequestBody ModuloRol moduloRol) {
		moduloService.grabarModuloRol(moduloRol);
		return ResponseEntity.ok().build();

	}

	// grabar modulos_roles
	@RequestMapping(value = "/grabarModuloRolOpcion", method = RequestMethod.POST)
	public ResponseEntity<?> grabarModuloRolOpcion(@RequestBody ModuloRolOpcion moduloRolOpcion) {
		moduloService.grabarModuloRolOpcion(moduloRolOpcion);
		return ResponseEntity.ok().build();
	}

	// grabar modulos_roles
	@RequestMapping(value = "/grabarSubrol", method = RequestMethod.POST)
	public ResponseEntity<?> grabarSubrol(@RequestBody Subrol subrol) {
		moduloService.grabarSubrol(subrol);
		return ResponseEntity.ok().build();
	}

	// lista roles_modulos
	@RequestMapping(value = "/listaRolesModulos/{modulo_id}", method = RequestMethod.GET)
	public ResponseEntity<?> listaRolesModulos(@PathVariable("modulo_id") Integer modulo_id) {
		return ResponseEntity.ok(moduloRolRepository.listaRolesModulos(modulo_id));
	}
	//****************** FIN SERVICIOS PARA ROLES************************//	

	//****************** SERVICIOS PARA ROLES OPCIONES************************//
	// Buscar todos los modulos, para listar en angular
	@RequestMapping(value = "/buscarRolesOpciones", method = RequestMethod.GET)
	public ResponseEntity<?> buscarRolesOpciones() {
		return ResponseEntity.ok(rolOpcionesRepository.findAll());
	}

	//****************** FIN SERVICIOS PARA ROLES OPCIONES************************//
	// lista roles_modulos
	@RequestMapping(value = "/buscarModulosRolesOpciones/{modulo_rol_id}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarModulosRolesOpciones(@PathVariable("modulo_rol_id") Integer modulo_rol_id) {
		return ResponseEntity.ok(rolOpcionesRepository.buscarModulosRolesOpciones(modulo_rol_id));
	}

	// ****************** SERVICIOS PARA ROLES -DEPARTAMENTOS************************//
	// lista departamentos
	@RequestMapping(value = "/buscarDepartamentos", method = RequestMethod.GET)
	public ResponseEntity<?> listaDepartamentos() {
		return ResponseEntity.ok(departamentoRepository.findAll());
	}

	// grabar roles_departamentos
	@RequestMapping(value = "/grabarRolDepartamento", method = RequestMethod.POST)
	public ResponseEntity<?> grabarRolDepartamento(@RequestBody RolDepartamento rolDepartamento) {
		moduloService.grabarRolDepartamento(rolDepartamento);
		return ResponseEntity.ok().build();
	}

	// lista roles_departamentos
	@RequestMapping(value = "/buscarRolesDepartamentos/{rol_id}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarRolesDepartamentos(@PathVariable("rol_id") Integer rol_id) {
		return ResponseEntity.ok(rolDepartamentoRepository.buscarRolesDepartamentos(rol_id));
	}
	// ****************** SERVICIOS PARA ROLES -DEPARTAMENTOS************************//
}
