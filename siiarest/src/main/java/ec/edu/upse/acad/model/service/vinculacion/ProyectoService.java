package ec.edu.upse.acad.model.service.vinculacion;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.jboss.logging.Logger;
import org.jfree.util.Log;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ec.edu.upse.acad.model.pojo.vinculacion.InstitucionBeneficiaria;
import ec.edu.upse.acad.model.pojo.vinculacion.Proyecto;
import ec.edu.upse.acad.model.pojo.vinculacion.ProyectoAreaUnesco;
import ec.edu.upse.acad.model.pojo.vinculacion.ProyectoAsignatura;
import ec.edu.upse.acad.model.pojo.vinculacion.ProyectoDocente;
import ec.edu.upse.acad.model.pojo.vinculacion.ProyectoDominiosAcademicos;
import ec.edu.upse.acad.model.pojo.vinculacion.ProyectoEstudiante;
import ec.edu.upse.acad.model.pojo.vinculacion.ProyectoLineasInv;
import ec.edu.upse.acad.model.pojo.vinculacion.ProyectoPresupuesto;
import ec.edu.upse.acad.model.pojo.vinculacion.ProyectoVersion;
import ec.edu.upse.acad.model.repository.vinculacion.InstitucionBeneficiariaRepository;
import ec.edu.upse.acad.model.repository.vinculacion.ProyectoAreaUnescoRepository;
import ec.edu.upse.acad.model.repository.vinculacion.ProyectoAsignaturaRepository;
import ec.edu.upse.acad.model.repository.vinculacion.ProyectoDocenteRepository;
import ec.edu.upse.acad.model.repository.vinculacion.ProyectoDominiosAcademicosRepository;
import ec.edu.upse.acad.model.repository.vinculacion.ProyectoEstudianteRepository;
import ec.edu.upse.acad.model.repository.vinculacion.ProyectoLineasInvRepository;
import ec.edu.upse.acad.model.repository.vinculacion.ProyectoPresupuestoRepository;
import ec.edu.upse.acad.model.repository.vinculacion.ProyectoRepository;
import ec.edu.upse.acad.model.repository.vinculacion.ProyectoVersionRepository;
import ec.edu.upse.acad.ws.vinculacion.ProyectoController;

@Service
@Transactional
public class ProyectoService {
	@Autowired
	private ProyectoRepository proyectoRepository;
	@Autowired
	private ProyectoVersionRepository proyectoVersionRepository;
	@Autowired
	private ProyectoAsignaturaRepository proyectoAsignaturaRepository;
	@Autowired
	private ProyectoPresupuestoRepository proyectoPresupuestoRepository;
	@Autowired
	private ProyectoDocenteRepository proyectoDocenteRepository;
	@Autowired
	private ProyectoEstudianteRepository proyectoEstudianteRepository;
	@Autowired
	private ProyectoLineasInvRepository proyectoLineasInvRepository;
	@Autowired
	private ProyectoDominiosAcademicosRepository proyectoDominiosAcademicosRepository;
	@Autowired
	private ProyectoAreaUnescoRepository proyectoAreaUnescoRepository;
	@Autowired
	private InstitucionBeneficiariaRepository institucionBeneficiariaRepository;
	@PersistenceContext
	private EntityManager em;

	static Logger logger = Logger.getLogger(ProyectoController.class);

	public void grabarProyecto(Proyecto proyecto) {
		System.out.println(proyecto.getId());
		if (proyecto.getId() != null) {
			System.out.println("editar registro");
			editarProyecto(proyecto);
		} else {
			System.out.println("ingresar registro");
			nuevoProyecto(proyecto);
		}		
	}

	public void nuevoProyecto(Proyecto proyecto) {
		//Integer idProyecto=null;
		proyectoRepository.save(proyecto);
		//idProyecto=proyectoRepository.saveAndFlush(proyecto).getId();
		em.getEntityManagerFactory().getCache().evict(Proyecto.class);
	}

	public void nuevoProyectoVersion(Proyecto proyecto) {
		//Integer idProyecto=null;
		Proyecto _proyecto =proyectoRepository.findById(proyecto.getId()).get();
		_proyecto.setEstadoProyecto(proyecto.getEstadoProyecto());
		proyectoRepository.save(_proyecto);
		for (ProyectoVersion proyectoVersion : proyecto.getProyectoVersion()) {
			if(proyectoVersion.getId() != null) {
				System.out.println("editando Proyecto Version");				
				ProyectoVersion _proyectoVersion = proyectoVersionRepository.findById(proyectoVersion.getId()).get();
				BeanUtils.copyProperties(proyectoVersion, _proyectoVersion, "proyecto");
				_proyectoVersion.setProyecto(_proyecto);
				proyectoVersionRepository.save(_proyectoVersion);
			}else {
				System.out.println("Nueva Proyecto Version");
				ProyectoVersion __proyectoVersion=new ProyectoVersion();
				BeanUtils.copyProperties(proyectoVersion, __proyectoVersion, "proyecto");
				__proyectoVersion.setProyecto(_proyecto);
				proyectoVersionRepository.save(__proyectoVersion);
			}
		}
		//idProyecto=proyectoRepository.saveAndFlush(proyecto).getId();
		em.getEntityManagerFactory().getCache().evict(Proyecto.class);
	}
	
	public void editarProyecto(Proyecto proyecto) {
		try {
			Log.info(proyecto);
			Proyecto _proyecto =proyectoRepository.findById(proyecto.getId()).get();
			BeanUtils.copyProperties(proyecto, _proyecto, "proyectoVersion","proyectoAsignatura","proyectoPresupuesto","institucionBeneficiaria","proyectoEstudiante"
					,"proyectoDocente","proyectoLineasInv","proyectoAreaUnesco","proyectoDominiosAcademicos");
			proyectoRepository.save(_proyecto);
			for (ProyectoVersion proyectoVersion : proyecto.getProyectoVersion()) {
				if(proyectoVersion.getId() != null) {
					ProyectoVersion _proyectoVersion = proyectoVersionRepository.findById(proyectoVersion.getId()).get();
					BeanUtils.copyProperties(proyectoVersion, _proyectoVersion, "proyecto");
					_proyectoVersion.setProyecto(_proyecto);
					proyectoVersionRepository.save(_proyectoVersion);
				}else {
					System.out.println("Nueva Proyecto Version");
					ProyectoVersion __proyectoVersion=new ProyectoVersion();
					BeanUtils.copyProperties(proyectoVersion, __proyectoVersion, "proyecto");
					__proyectoVersion.setProyecto(_proyecto);
					proyectoVersionRepository.save(__proyectoVersion);
				}
			}
			for (ProyectoAsignatura proyectoAsiganatura : proyecto.getProyectoAsignatura()) {
				if(proyectoAsiganatura.getId() != null) {
					System.out.println("entre 2");
					ProyectoAsignatura _proyectoAsiganatura  = proyectoAsignaturaRepository.findById(proyectoAsiganatura.getId()).get();
					BeanUtils.copyProperties(proyectoAsiganatura, _proyectoAsiganatura, "proyecto");
					_proyectoAsiganatura.setProyecto(_proyecto);
					proyectoAsignaturaRepository.save(_proyectoAsiganatura);			
				}else {
					System.out.println("Nueva Proyecto Asignatura");
					ProyectoAsignatura __proyectoAsiganatura=new ProyectoAsignatura();
					BeanUtils.copyProperties(proyectoAsiganatura, __proyectoAsiganatura, "proyecto");
					__proyectoAsiganatura.setProyecto(_proyecto);
					proyectoAsignaturaRepository.save(__proyectoAsiganatura);		
				}
			}
			for (ProyectoPresupuesto proyectoPresupuesto : proyecto.getProyectoPresupuesto()) {
				if(proyectoPresupuesto.getId() != null) {
					System.out.println("entre 3");
					ProyectoPresupuesto _proyectoPresupuesto  = proyectoPresupuestoRepository.findById(proyectoPresupuesto.getId()).get();
					BeanUtils.copyProperties(proyectoPresupuesto, _proyectoPresupuesto, "proyecto");
					_proyectoPresupuesto.setProyecto(_proyecto);
					proyectoPresupuestoRepository.save(_proyectoPresupuesto);			
				}else {
					System.out.println("Nueva Proyecto Presupuesto");
					ProyectoPresupuesto __proyectoPresupuesto=new ProyectoPresupuesto();
					BeanUtils.copyProperties(proyectoPresupuesto, __proyectoPresupuesto, "proyecto");
					__proyectoPresupuesto.setProyecto(_proyecto);
					proyectoPresupuestoRepository.save(__proyectoPresupuesto);		
				}
			}
			for (InstitucionBeneficiaria institucionBeneficiaria : proyecto.getInstitucionBeneficiaria()) {
				if(institucionBeneficiaria.getId() != null) {
					System.out.println("entre 4");
					InstitucionBeneficiaria _institucionBeneficiaria  = institucionBeneficiariaRepository.findById(institucionBeneficiaria.getId()).get();
					BeanUtils.copyProperties(institucionBeneficiaria, _institucionBeneficiaria, "proyecto","proyectoAreaGeografica","persona");
					_institucionBeneficiaria.setProyecto(_proyecto);
					_institucionBeneficiaria.setProyectoAreaGeografica(institucionBeneficiaria.getProyectoAreaGeografica());
					_institucionBeneficiaria.setProyectoPersona(institucionBeneficiaria.getProyectoPersona());
					institucionBeneficiariaRepository.save(_institucionBeneficiaria);			
				}else {
					System.out.println("Nueva Institucion Beneficiaria");
					InstitucionBeneficiaria __institucionBeneficiaria=new InstitucionBeneficiaria();
					BeanUtils.copyProperties(institucionBeneficiaria, __institucionBeneficiaria, "proyecto","proyectoAreaGeografica");
					__institucionBeneficiaria.setProyecto(_proyecto);
					__institucionBeneficiaria.setProyectoAreaGeografica(institucionBeneficiaria.getProyectoAreaGeografica());
					__institucionBeneficiaria.setProyectoPersona(institucionBeneficiaria.getProyectoPersona());
					institucionBeneficiariaRepository.save(__institucionBeneficiaria);		
				}
				
			}
			for (ProyectoDocente proyectoDocente : proyecto.getProyectoDocente()) {
				if(proyectoDocente.getId() != null) {
					System.out.println("entre 5");
					ProyectoDocente _proyectoDocente  = proyectoDocenteRepository.findById(proyectoDocente.getId()).get();
					BeanUtils.copyProperties(proyectoDocente, _proyectoDocente, "proyecto");
					_proyectoDocente.setProyecto(_proyecto);
					proyectoDocenteRepository.save(_proyectoDocente);			
				}else {
					System.out.println("Nueva Proyecto Docente");
					ProyectoDocente __proyectoDocente=new ProyectoDocente();
					BeanUtils.copyProperties(proyectoDocente, __proyectoDocente, "proyecto");
					__proyectoDocente.setProyecto(_proyecto);
					proyectoDocenteRepository.save(__proyectoDocente);		
				}
			}
			for (ProyectoEstudiante proyectoEstudiante : proyecto.getProyectoEstudiante()) {
				if(proyectoEstudiante.getId() != null) {
					System.out.println("entre 6");
					ProyectoEstudiante _proyectoEstudiante  = proyectoEstudianteRepository.findById(proyectoEstudiante.getId()).get();
					BeanUtils.copyProperties(proyectoEstudiante, _proyectoEstudiante, "proyecto");
					_proyectoEstudiante.setProyecto(_proyecto);
					proyectoEstudianteRepository.save(_proyectoEstudiante);			
				}else {
					System.out.println("Nueva Proyecto Estudiante");
					ProyectoEstudiante __proyectoEstudiante=new ProyectoEstudiante();
					BeanUtils.copyProperties(proyectoEstudiante, __proyectoEstudiante, "proyecto");
					__proyectoEstudiante.setProyecto(_proyecto);
					proyectoEstudianteRepository.save(__proyectoEstudiante);		
				}
			}
			for (ProyectoLineasInv proyectoLineasInv : proyecto.getProyectoLineasInv()) {
				if(proyectoLineasInv.getId() != null) {
					System.out.println("entre 7");
					ProyectoLineasInv _proyectoLineasInv  = proyectoLineasInvRepository.findById(proyectoLineasInv.getId()).get();
					BeanUtils.copyProperties(proyectoLineasInv, _proyectoLineasInv, "proyecto");
					_proyectoLineasInv.setProyecto(_proyecto);
					proyectoLineasInvRepository.save(_proyectoLineasInv);			
				}else {
					System.out.println("Nueva Proyecto LineasInv");
					ProyectoLineasInv __proyectoLineasInv=new ProyectoLineasInv();
					BeanUtils.copyProperties(proyectoLineasInv, __proyectoLineasInv, "proyecto");
					__proyectoLineasInv.setProyecto(_proyecto);
					proyectoLineasInvRepository.save(__proyectoLineasInv);		
				}
			}
			for (ProyectoDominiosAcademicos proyectoDominiosAcademicos : proyecto.getProyectoDominiosAcademicos()) {
				if(proyectoDominiosAcademicos.getId() != null) {
					System.out.println("entre 8");
					ProyectoDominiosAcademicos _proyectoDominiosAcademicos  = proyectoDominiosAcademicosRepository.findById(proyectoDominiosAcademicos.getId()).get();
					BeanUtils.copyProperties(proyectoDominiosAcademicos, _proyectoDominiosAcademicos, "proyecto");
					_proyectoDominiosAcademicos.setProyecto(_proyecto);
					proyectoDominiosAcademicosRepository.save(_proyectoDominiosAcademicos);			
				}else {
					System.out.println("Nueva Proyecto DominiosAcademicos");
					ProyectoDominiosAcademicos __proyectoDominiosAcademicos=new ProyectoDominiosAcademicos();
					BeanUtils.copyProperties(proyectoDominiosAcademicos, __proyectoDominiosAcademicos, "proyecto");
					__proyectoDominiosAcademicos.setProyecto(_proyecto);
					proyectoDominiosAcademicosRepository.save(__proyectoDominiosAcademicos);		
				}
			}
			for (ProyectoAreaUnesco proyectoAreaUnesco : proyecto.getProyectoAreaUnesco()) {
				if(proyectoAreaUnesco.getId() != null) {
					System.out.println("entre 9");
					ProyectoAreaUnesco _proyectoAreaUnesco  = proyectoAreaUnescoRepository.findById(proyectoAreaUnesco.getId()).get();
					BeanUtils.copyProperties(proyectoAreaUnesco, _proyectoAreaUnesco, "proyecto");
					_proyectoAreaUnesco.setProyecto(_proyecto);
					proyectoAreaUnescoRepository.save(_proyectoAreaUnesco);			
				}else {
					System.out.println("Nueva Proyecto AreaUnesco");
					ProyectoAreaUnesco __proyectoAreaUnesco=new ProyectoAreaUnesco();
					BeanUtils.copyProperties(proyectoAreaUnesco, __proyectoAreaUnesco, "proyecto");
					__proyectoAreaUnesco.setProyecto(_proyecto);
					proyectoAreaUnescoRepository.save(__proyectoAreaUnesco);		
				}
			}
			em.getEntityManagerFactory().getCache().evict(Proyecto.class);	
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("aqui" + e.getMessage());
			extractStackTrace(e, logger);
		}
		
	}
	
	public static void extractStackTrace(Exception exception, Logger _logger) {
	     StackTraceElement[] elements = exception.getStackTrace();
	     String log = null;
	     for (StackTraceElement element : elements) {
	    	 log = (log == null) ? element.toString() : log + ";" + element.toString();

	     }
	     _logger.error(log);

	}
	
	public void borrarProyecto(Proyecto proyecto) {
		Log.info(proyecto);
		Proyecto _proyecto =proyectoRepository.findById(proyecto.getId()).get();
		proyecto.setEstado("I");
		BeanUtils.copyProperties(proyecto, _proyecto, "proyectoVersion");
		proyectoRepository.save(_proyecto);
		for (ProyectoVersion proyectoVersion : proyecto.getProyectoVersion()) {
			ProyectoVersion _proyectoVersion = proyectoVersionRepository.findById(proyectoVersion.getId()).get();
			proyectoVersion.setEstado("E");
			BeanUtils.copyProperties(proyectoVersion, _proyectoVersion, "proyecto");
			proyectoVersionRepository.save(_proyectoVersion);
		}
		em.getEntityManagerFactory().getCache().evict(Proyecto.class);
	}
	
	//pasar de estado inicial a estado revisar en proyecto
	public void updateEstadoProyecto(Integer idProyecto) {
		Proyecto _proyecto =proyectoRepository.findById(idProyecto).get();
		_proyecto.setEstadoProyecto("REVISIÓN");
		proyectoRepository.save(_proyecto);
		em.getEntityManagerFactory().getCache().evict(Proyecto.class);
	}
	
	public void updateCodigoProyecto(Proyecto proyecto) {
		ProyectoVersion _proyectoVersion = proyectoVersionRepository.findById(proyecto.getId()).get();
		Proyecto _proyecto =proyectoRepository.findById(_proyectoVersion.getProyecto().getId()).get();
		System.out.println("estado1"+ proyecto.getEstadoProyecto());
		if(proyecto.getCodigo()!= null) {
			_proyecto.setCodigo(proyecto.getCodigo());
		}
		if(proyecto.getEstadoProyecto() != null) {
			System.out.println("estado"+ proyecto.getEstadoProyecto());
		_proyecto.setEstadoProyecto(proyecto.getEstadoProyecto());
		}
		proyectoRepository.save(_proyecto);
		em.getEntityManagerFactory().getCache().evict(Proyecto.class);
	}
	
}
