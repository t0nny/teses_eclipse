package ec.edu.upse.acad.ws;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import ec.edu.upse.acad.model.pojo.vinculacion.Proyecto;
import ec.edu.upse.acad.model.service.FileStorageService;
import ec.edu.upse.acad.model.service.SecurityService;


@RestController
@RequestMapping("/api/files")
@CrossOrigin
@MultipartConfig
public class FileController {

	@Autowired private SecurityService securityService;
	@Autowired private FileStorageService fileStorageService;
    
    @PostMapping("/uploadFile")
    public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile file) {
    	UploadFileResponse carga = upload(file);
    	return ResponseEntity.ok(carga);
    }
    
    @PostMapping("/uploadMultipleFiles")
    public ResponseEntity<?> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files) {
    	List<UploadFileResponse> retorno = Arrays.asList(files)
                .stream()
                .map(file -> upload(file))
                .collect(Collectors.toList());
    	
    	return ResponseEntity.ok(retorno);
    }

    @GetMapping("/uploadFile/{fileName:.+}")
    public ResponseEntity<?> downloadFile(@PathVariable String fileName,HttpServletRequest request) throws Exception {
        // Carga el archivo
        Resource resource = fileStorageService.loadFileAsResource(fileName);
        // Determina el contenido
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        // Si no se determina el tipo, asume uno por defecto.
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"").body(resource);
    }

    private UploadFileResponse upload(MultipartFile file) {
    	try {
            String fileName = fileStorageService.storeFile(file);

            String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
            		.path("/files/uploadFile/")
                    .path(fileName)
                    .toUriString();

            return new UploadFileResponse(fileName, fileDownloadUri,
                    file.getContentType(), file.getSize());
		} catch (Exception e) {
			return null;
		}
    }
    
    @PostMapping("/uploadFileP")
    public ResponseEntity<?> uploadFileP(@RequestHeader(value = "Authorization") String authorization,
    		@RequestBody MultipartFile file) throws IOException {
    	if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
    	
    	if(file== null || file.isEmpty()){
    		return ResponseEntity.badRequest().build();
    	}
    	
    	StringBuilder builder= new StringBuilder();// creamos la ruta donde guardaremos el archivo
		builder.append(System.getProperty("user.home"));//OBTINE LA RUTA HOME del cualquier SO win linux mac
		builder.append(File.separator); //para no tener erros con los separadores
		builder.append("proyecto_vinculacion");// nombre de la carpeta que crearemos en la ruta home
		builder.append(File.separator);
		builder.append(file.getOriginalFilename());
		
		byte[] filesBytes=file.getBytes();
		Path path=Paths.get(builder.toString());
		Files.write(path,filesBytes);
		
		return ResponseEntity.ok().build();
		
    }
    
    @GetMapping("/uploadFileP/{fileName:.+}")
    public ResponseEntity<?> downloadFileP(@PathVariable String fileName,HttpServletRequest request) throws Exception {
        // Carga el archivo
        Resource resource = fileStorageService.cargarFileDelServer(fileName);
        // Determina el contenido
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        // Si no se determina el tipo, asume uno por defecto.
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"").body(resource);
    }
    
    
}