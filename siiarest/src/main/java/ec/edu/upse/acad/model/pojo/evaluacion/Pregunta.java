package ec.edu.upse.acad.model.pojo.evaluacion;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="eva", name="pregunta")
@NoArgsConstructor
public class Pregunta {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_pregunta")
	@Getter @Setter private Integer id;
	
	//@Column(name="id_categoria_pregunta")
	//@Getter @Setter private Integer idCategoriaPregunta;
	
	//@Column(name="id_tipo_pregunta")
	//@Getter @Setter private Integer idTipoPregunta;

	@Column(name="codigo")
	@Getter @Setter private String codigo;

	@Column(name="descripcion")
	@Getter @Setter private String descripcion;
	
	
	@Getter @Setter private String estado;

//	@Column(name="fecha_ingreso")
//	@Getter @Setter private Timestamp fechaIngreso;
	
	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;
	
	@Version
	@Getter @Setter private Integer version;

	//RELACIONES
	//bi-directional many-to-one association to 
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_categoria_pregunta", insertable=false, updatable = false)
	@Getter @Setter private CategoriaPregunta categoriaPregunta;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_tipo_pregunta", insertable=false, updatable = false)
	@Getter @Setter private TipoPregunta tipoPregunta;
	
	@OneToMany(mappedBy="pregunta", cascade=CascadeType.ALL)
	@Getter @Setter private List<OpcionPregunta> opcionPregunta;

	
	//bi-directional many-to-one association to 
	@OneToMany(mappedBy="pregunta", cascade=CascadeType.ALL)
	@JsonIgnore
	@Getter @Setter private List<InstrumentoPregunta> instrumentoPreguntas;

	
	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }
}
