package ec.edu.upse.acad.model.pojo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;



@Entity
@Table(schema="aca", name="jornada")
@NoArgsConstructor
public class Jornada {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_jornada")
	@Getter @Setter private Integer id;

	@Getter @Setter private String codigo;

	@Getter @Setter private String descripcion;

	@Getter @Setter private String estado;
	
	@Column(name="fecha_ingreso")
	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;

	//bi-directional many-to-one association to DocenteAsignaturaAprend
	

	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }
}

