package ec.edu.upse.acad.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.NivelAcademico;

@Repository
public interface NivelAcademicoRepository  extends JpaRepository<NivelAcademico, Integer>{

	@Query(value=" SELECT u.id as idNivelAcademico, u.descripcion as nivelAcademico "+
			"FROM NivelAcademico as u " ) 
	List<CustomObject> listarNiveles();	
	interface CustomObject  { 
		Integer getIdNivelAcademico(); 
		String getNivelAcademico();

	}
}
