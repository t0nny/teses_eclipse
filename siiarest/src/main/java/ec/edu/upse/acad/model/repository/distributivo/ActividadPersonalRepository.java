package ec.edu.upse.acad.model.repository.distributivo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ec.edu.upse.acad.model.pojo.reglamento.ActividadPersonalDocente;


@Repository
public interface ActividadPersonalRepository extends JpaRepository<ActividadPersonalDocente, Integer> {
	
	@Query(value=" SELECT ad.id as idActividadDocenteDetalle, ap.descripcion as actividad, ad.descripcion as descripcion "+
			" FROM ActividadDocenteDetalle ad " + 
			" INNER JOIN ReglamentoActividadDetalle ra " + 
			" on ad.id = ra.idActividadDetalle " + 
			" INNER JOIN ReglamentoActividadDocente rd " + 
			" on rd.id = ra.idReglamentoActividad " + 
			" INNER JOIN ActividadPersonalDocente ap " + 
			" on ap.id = rd.idActividadPersonal " + 
			" WHERE ra.obligatorioDistributivo = 'True' and ra.estado = 'A'"+
			" ORDER BY ap.descripcion "	)
	List<CustomObject> listarActividadPersonal();

	interface CustomObject  { 
	    Integer getIdActividadDocenteDetalle(); 
	    String getActividad(); 
	    String getDescripcion();
	}
	

	

}
