package ec.edu.upse.acad.model.pojo.vinculacion;

import java.sql.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema = "vin", name = "proyecto")

@NoArgsConstructor //un constructorsin argumentos
public class Proyecto {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_proyecto")
	@Getter	@Setter	private Integer id;

	@Column(name = "id_programa")
	@Getter	@Setter	private Integer idPrograma;
	
	@Column(name = "id_proyecto_convocatoria")
	@Getter	@Setter	private Integer idProyectoConvocatoria;
	
	@Getter	@Setter	private String titulo;
	
	@Getter	@Setter	private String descripcion;

	@Getter	@Setter	private String codigo;

	@Column(name = "fecha_desde")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", locale = "es_EC", timezone = "America/Guayaquil")
	@Getter	@Setter	private Date fechaDesde;

	@Column(name = "fecha_hasta")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", locale = "es_EC", timezone = "America/Guayaquil")
	@Getter	@Setter	private Date fechaHasta;

	@Column(name = "estado_proyecto")
	@Getter	@Setter	private String estadoProyecto;

	@Column(name = "usuario_ingreso_id")
	@Getter	@Setter	private String usuarioIngresoId;

	@Version
	@Getter	@Setter	private Integer version;

	@Getter	@Setter	private String estado;

	// bi-directional many-to-one association to versionProyecto
	@OneToMany(mappedBy = "proyecto", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JsonManagedReference
	@Getter	@Setter	private List<ProyectoVersion> proyectoVersion;
	
	// bi-directional many-to-one association to proyectoAsignatura
	@OneToMany(mappedBy = "proyecto", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JsonManagedReference
	@Getter	@Setter	private List<ProyectoAsignatura> proyectoAsignatura;

	// bi-directional many-to-one association to proyectoPresupuesto
	@OneToMany(mappedBy = "proyecto", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JsonManagedReference
	@Getter	@Setter	private List<ProyectoPresupuesto> proyectoPresupuesto;
	
	// bi-directional many-to-one association to proyectoPresupuesto funciona
	//@JsonIdentityInfo(generator=ObjectIdGenerators.UUIDGenerator.class, property="@id")
	@OneToMany(mappedBy="proyecto", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JsonManagedReference
	@Getter	@Setter	private List<InstitucionBeneficiaria> institucionBeneficiaria;
	
	@OneToMany(mappedBy="proyecto", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JsonManagedReference
	@Getter	@Setter	private List<DepOfertaProyecto> depOfertaProyecto;
	    
	@OneToMany(mappedBy="proyecto", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JsonManagedReference
	@Getter	@Setter	private List<ProyectoEstudiante> proyectoEstudiante;
	
	@OneToMany(mappedBy="proyecto", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JsonManagedReference
	@Getter	@Setter	private List<ProyectoDocente> proyectoDocente;
	
	@OneToMany(mappedBy="proyecto", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JsonManagedReference
	@Getter	@Setter	private List<ProyectoAreaUnesco> proyectoAreaUnesco;
	
	@OneToMany(mappedBy="proyecto", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JsonManagedReference
	@Getter	@Setter	private List<ProyectoDominiosAcademicos> proyectoDominiosAcademicos;
	
	@OneToMany(mappedBy="proyecto", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JsonManagedReference
	@Getter	@Setter	private List<ProyectoLineasInv> proyectoLineasInv;
	
	@OneToMany(mappedBy="proyecto", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JsonManagedReference
	@Getter	@Setter	private List<ItemMarcoLogico> itemMarcoLogico;
	
	/*@ManyToOne//(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "id_programa", nullable = false, insertable=true, updatable=true)
	//@JsonBackReference
	@Getter	@Setter	private Programa programa;*/
	
	
	// bi-directional one_to_many association to proyecto
	/*@ManyToOne
	@JoinColumn(name = "id_convocatoria_proyecto", nullable = false, updatable = false)
	@JsonBackReference
	@Getter	@Setter	private ConvocatoriaProyecto ConvocatoriaProyecto;*/
	
	@PrePersist
	void preInsert() {
		if (this.estado == null)
			this.estado = "A";
	}

	@Override
	public String toString() {
		return "Proyecto [id=" + id + ", titulo=" + titulo + ", codigo=" + codigo + ", fechaDesde=" + fechaDesde
				+ ", fechaHasta=" + fechaHasta + ", estadoProyecto=" + estadoProyecto + ", usuarioIngresoId="
				+ usuarioIngresoId + ", version=" + version + ", estado=" + estado + ", proyectoVersion="
				+ proyectoVersion + ", proyectoAsignatura=" + proyectoAsignatura + ", proyectoPresupuesto="
				+ proyectoPresupuesto + ", institucionBeneficiaria=" + institucionBeneficiaria + "]";
	}	
	
	

}
