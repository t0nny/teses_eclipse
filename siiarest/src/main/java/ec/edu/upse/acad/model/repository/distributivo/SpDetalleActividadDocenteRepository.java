package ec.edu.upse.acad.model.repository.distributivo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import ec.edu.upse.acad.model.pojo.distributivo.SpDetalleActividadDocente;


public interface SpDetalleActividadDocenteRepository extends JpaRepository<SpDetalleActividadDocente, Integer> {
	@Query(nativeQuery = true, name = "getSPDetalleActividadDocente" )
	public List<SpDetalleActividadDocente> getSPDetalleActividadDocente(@Param("id_distributivo_docente ") Integer idDistributivoDocente);
}
