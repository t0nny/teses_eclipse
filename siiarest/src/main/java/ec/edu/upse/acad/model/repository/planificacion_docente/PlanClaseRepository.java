package ec.edu.upse.acad.model.repository.planificacion_docente;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.planificacion_docente.PlanClase;

@Repository
public interface PlanClaseRepository  extends JpaRepository<PlanClase, Long> {

	@Query(value="SELECT con.idContenidoPadre as idContenidoPadre,con.id as idContenido,con.descripcion as contenido," + 
			" pc.id as idPlanClase,pc.contenido as contenidoPlan, con.resultadoAprendizaje as resultadoAprendizaje, "+
			" pc.horasDoc as horasDoc,pc.horasAea as horasPrac, pc.horasTa as horasTa,(pc.horasDoc + pc.horasAea + pc.horasTa ) as totalHoras," + 
			" con.orden as orden FROM MallaAsignatura ma " + 
			" INNER JOIN Syllabus si on ma.id=si.idMallaAsignatura " + 
			" INNER JOIN Contenido con on si.id=con.idSilabo " + 
			" INNER JOIN PlanClase pc on con.id=pc.idContenidos " + 
			" WHERE ma.id=(?1) and ma.estado='A' and con.estado='A' and si.estado='A' and pc.estado='A'" + 
			" ORDER BY con.orden  ")
	List<CoPlanesClase> listaPlanesClase(Integer idMallaAsignatura);
	interface CoPlanesClase{ 
	    Integer getIdContenidoPadre();
	    Integer getIdContenido();
	    String getContenido();
	    Long getIdPlanClase();
	    String getContenidoPlan();
	    String getResultadoAprendizaje();
	    Integer getHorasDoc();
	    Integer getHorasPrac();
	    Integer getHorasTa();
	    Integer getTotalHoras();
	    Integer getOrden();
	}
	
	@Query(value="SELECT con.id as idContenido,sum(pc.horasDoc) as horasDocT,sum(pc.horasAea) as horasPracT, sum(pc.horasTa) as horasTaT " + 
			" FROM Contenido con " + 
			" INNER JOIN PlanClase pc on con.id=pc.idContenidos " + 
			" WHERE con.id=(?1) and pc.estado ='A' and con.estado ='A' " + 
			" GROUP BY con.id ")
	List<CoHorasContenido> recuperarHoras(Integer idContenido);
	interface CoHorasContenido{ 
	    Integer getIdContenido();
	    Integer getHorasDocT();
	    Integer getHorasPracT();
	    Integer getHorasTaT();
	}
}
