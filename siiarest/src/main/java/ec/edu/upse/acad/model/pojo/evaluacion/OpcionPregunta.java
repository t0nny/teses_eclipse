package ec.edu.upse.acad.model.pojo.evaluacion;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ec.edu.upse.acad.model.pojo.calificaciones.ActaCalificacion;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Entity
@Table(schema="eva", name="opcion_pregunta")
@NoArgsConstructor
public class OpcionPregunta {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_opcion_pregunta")
	@Getter @Setter private Integer id;
	
//	@Column(name="id_pregunta")
//	@Getter @Setter private Integer idPregunta;
//
//	@Column(name="id_opcion_respuesta")
//	@Getter @Setter private Integer idOpcionRespuesta;
	
	@Getter @Setter private String estado;
	
//	@Column(name="fecha_ingreso")
//	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;
	
	@Version
	@Getter @Setter private Integer version;

	//RELACIONES
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_pregunta", nullable = false, insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Pregunta pregunta;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_opcion_respuesta", insertable=false, updatable = false)
	//@JsonIgnore
	@Getter @Setter private OpcionRespuesta idOpcionRespuesta;
	
	
  	@OneToMany(mappedBy="opcionPregunta")
	@JsonIgnore
  	@Getter @Setter private List<DetEvaluacionDocente> detEvaluacionDocente;
	
	
	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }
		

}
