package ec.edu.upse.acad.model.pojo.seguridad;

import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import ec.edu.upse.acad.model.pojo.Docente;
import ec.edu.upse.acad.model.pojo.Miembro;
import ec.edu.upse.acad.model.pojo.matricula.Estudiante;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="man", name="personas")
@NoArgsConstructor

public class Persona {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Getter @Setter private Integer id;
	@Column(nullable = false)

	@Getter @Setter private String identificacion;

	@Getter @Setter private String nombres;

	@Getter @Setter private String apellidos;

	@Getter @Setter private String genero;

	@Getter @Setter private Date fecha_nace;

	@Getter @Setter private String direccion;

	@Getter @Setter private String telefono;
	
	@Getter @Setter private String ciudad;

	@Getter @Setter private String celular;

	@Getter @Setter private String email_personal;

	@Getter @Setter private String email_institucional;

	@Getter @Setter private String foto;
	@Getter @Setter private String usuario_ing;

	@Getter @Setter private String estado;

	@Version
	@Getter @Setter private Integer version;


	//RELACIONES
	//Relacion uno a uno tabla persona con tabla usuario
	@OneToOne(mappedBy="persona", cascade=CascadeType.ALL)
	@Getter @Setter private Usuario usuario;

	//Relacion uno a uno tabla persona con tabla docente
	@OneToOne(mappedBy="persona", cascade=CascadeType.ALL)
	@Getter @Setter private Docente docente;
	
	//Relacion uno a uno tabla persona con tabla Estudiante
	@OneToOne(mappedBy="persona", cascade=CascadeType.ALL)
	@Getter @Setter private Estudiante estudiante;

	//Relacion uno a uno tabla persona con tabla personaDepartamento
	@OneToMany(mappedBy="persona", cascade=CascadeType.ALL)
	@Getter @Setter private List<PersonaDepartamento> personaDepartamento;

	//Relacion uno a uno tabla persona con tabla miembro
	@OneToMany(mappedBy="persona", cascade=CascadeType.ALL)
	@Getter @Setter private List<Miembro> miembro;

	//Relacion uno a uno tabla persona con tabla personaDepartamento
	@OneToMany(mappedBy="persona", cascade=CascadeType.ALL)
	@Getter @Setter private List<PersonasCargo> personasCargo;


	@PrePersist
	void preInsert() {
		if (this.estado == null)
			this.estado = "AC";
	}	
}


