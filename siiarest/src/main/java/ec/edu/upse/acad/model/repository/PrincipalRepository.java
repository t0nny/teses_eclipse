package ec.edu.upse.acad.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.seguridad.Modulo;

@Repository

public interface PrincipalRepository 
       extends JpaRepository<Modulo, Integer>{

//	RolUsuario findByRol(Integer rol_id);

	@Query(value="SELECT m "
			+ "FROM Usuario as e "
		//	+ " JOIN RolUsuario u "
			+ " JOIN Modulo m "		
			+ "WHERE e.usuario = ?1 ")
		//	+ "OR e.descripcion LIKE ?1 ")
	List<Modulo> buscarPorNombre(String usuario);
}