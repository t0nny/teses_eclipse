package ec.edu.upse.acad.model.pojo.distributivo;

import javax.persistence.*;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ec.edu.upse.acad.model.pojo.Docente;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Entity
@Table(schema="aca", name="distributivo_docente")
@Where(clause = "estado='A'")
@NoArgsConstructor
public class DistributivoDocente {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_distributivo_docente")
	@Getter @Setter  private Integer id;

	@Column(name="id_docente")
	@Getter @Setter  private Integer idDocente;
	
	@Column(name="id_distributivo_oferta_version")
	@Getter @Setter  private Integer idDistributivoOfertaVersion;
	
	@Column(name="oferta_principal")
	@Getter @Setter private String ofertaPrincipal;
	
	@Column(name="fecha_desde")
	@Getter @Setter private Date fechaDesde;

	@Column(name="fecha_hasta")
	@Getter @Setter private Date fechaHasta;
	
	@Getter @Setter private String estado;
	
	//@Column(name="fecha_ingreso")
	//@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;
	
	
	//RELACIONES
	
	//bi-directional many-to-one association to Docente
		@ManyToOne(fetch=FetchType.LAZY)
		@JoinColumn(name="id_docente", insertable=false, updatable = false)
		@JsonIgnore
		@Getter @Setter private Docente docente;
		
	//bi-directional many-to-one association to DistributivoOfertaVersion
	    @ManyToOne(fetch=FetchType.LAZY)
	    @JoinColumn(name="id_distributivo_oferta_version", insertable=false, updatable = false)
	    @JsonIgnore
	    @Getter @Setter private DistributivoOfertaVersion distributivoOfertaVersion;

	//bi-directional many-to-one association to Docente Asignatura Aprendizaje
		@OneToMany(mappedBy="distributivoDocente", cascade=CascadeType.ALL)
		@Getter @Setter private List<DocenteAsignaturaAprend> docenteAsignaturaAprends;

		
	//bi-directional many-to-one association to distributivo Dedicacion
	   @OneToMany(mappedBy="distributivoDocente", cascade=CascadeType.ALL)
	    @Getter @Setter private List<DistributivoDedicacion> distributivoDedicaciones;
	   
	 //bi-directional many-to-one association to distributivo Dedicacion
	   @OneToMany(mappedBy="distributivoDocente", cascade=CascadeType.ALL)
	    @Getter @Setter private List<DocenteActividad> docenteActividades;
	
	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }

}