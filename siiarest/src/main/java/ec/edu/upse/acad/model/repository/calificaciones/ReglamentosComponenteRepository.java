package ec.edu.upse.acad.model.repository.calificaciones;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ec.edu.upse.acad.model.pojo.calificaciones.ReglamentosComponente;

@Repository
public interface ReglamentosComponenteRepository extends JpaRepository<ReglamentosComponente, Integer> {

}
