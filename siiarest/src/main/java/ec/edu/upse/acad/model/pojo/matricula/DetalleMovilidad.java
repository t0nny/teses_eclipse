package ec.edu.upse.acad.model.pojo.matricula;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;
import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ec.edu.upse.acad.model.pojo.MallaAsignatura;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="aca", name="detalle_movilidad")
@Where(clause = "estado='A'")
@NoArgsConstructor
public class DetalleMovilidad {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_detalle_movilidad")
	@Getter @Setter private Integer id;
	
	@Column(name="id_movilidad")
	@Getter @Setter  private Integer idMovilidad;
	
	@Column(name="id_malla_asignatura")
	@Getter @Setter  private Integer idMallaAsignatura;
	
	@Column(name="calificacion")
	@Getter @Setter  private Double calificacion;
	
	@Getter @Setter private String estado;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;
	
	@Version
	@Getter @Setter private int version;
	
	//bi-directional many-to-one association to Movilidad
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_movilidad" , insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Movilidad movilidad;

	//bi-directional many-to-one association to MallaAsignatura
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_malla_asignatura" , insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private MallaAsignatura mallaAsignatura;

	
	@PrePersist
	void preInsert() {
		if (this.estado == null) {
			this.estado = "A";
		}		
	}
	
}
