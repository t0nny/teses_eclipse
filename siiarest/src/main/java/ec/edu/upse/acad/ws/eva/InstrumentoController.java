package ec.edu.upse.acad.ws.eva;

import java.util.ArrayList;
import java.util.List;

import org.jfree.util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.upse.acad.model.pojo.evaluacion.InstrumentoPregunta;
import ec.edu.upse.acad.model.pojo.evaluacion.Pregunta;
import ec.edu.upse.acad.model.repository.evaluacion.CategoriaPreguntaRepository;
import ec.edu.upse.acad.model.repository.evaluacion.InstrumentoRepository;
import ec.edu.upse.acad.model.repository.evaluacion.InstrumentoPreguntaRepository;
import ec.edu.upse.acad.model.repository.evaluacion.PreguntaRepository;
import ec.edu.upse.acad.model.repository.evaluacion.TipoPreguntaRepository;
import ec.edu.upse.acad.model.service.SecurityService;
import ec.edu.upse.acad.util.RestUtil;

@RestController
@RequestMapping("/api/instrumentoPorEvaluacion")
@CrossOrigin
public class InstrumentoController {
	@Autowired private InstrumentoPreguntaRepository instrumentoPreguntaRepository;
	@Autowired private PreguntaRepository preguntaRepository;
	@Autowired private InstrumentoRepository instrumentoRepository;
	@Autowired private SecurityService securityService;

	//Buscar todos laoss instrumentosPreguntas, para listar en angular
	@RequestMapping(value="/listarInstrumentos", method=RequestMethod.GET)
		public ResponseEntity<?> listarInstrumentos(@RequestHeader(value="Authorization") String Authorization) {
			if (!securityService.isTokenValido(Authorization)) {
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
			}
			return ResponseEntity.ok(instrumentoRepository.listarInstrumento());
		}
	
	
	//Buscar todos los instrumentos para listar en angular
	@RequestMapping(value="/listarPeguntaInstrumento/{id}", method=RequestMethod.GET)
	public ResponseEntity<?> listarPreguntaInstrumento(@RequestHeader(value="Authorization") String Authorization, @PathVariable Integer id) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(instrumentoPreguntaRepository.listarInstrumentoPregunta(id));
	}
	public static final String ALREADY_REPORTED = "ERROR! base de datos";
	/**
	 * Graba o edita una pregunta
	 * 
	 * @param asignatura recibe un objeto de tipo Asignatura
	 * @return
	 */
	// servicio que graba un CategoriaPregunta
/*	@RequestMapping(value="/grabarPregunta", method=RequestMethod.POST)
	public ResponseEntity<?> grabarReglaCalificacionEscala(@RequestHeader(value="Authorization") 
														   String Authorization, 
														   @RequestBody InstrumentoPregunta instrumentoPregunta) {
		try {
			if (!securityService.isTokenValido(Authorization)) {
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
			}
			Log.debug(instrumentoPregunta.getPregunta().getDescripcion());
			 
			InstrumentoPregunta _instrumentoPregunta = new InstrumentoPregunta();
			if (instrumentoPregunta.getId() != null) {
				_instrumentoPregunta = instrumentoPreguntaRepository.findById(instrumentoPregunta.getId()).get();
				_instrumentoPregunta.getPregunta().setDescripcion(instrumentoPregunta.getPregunta().getDescripcion());
			_instrumentoPregunta.getInstrumento().setId(instrumentoPregunta.getInstrumento().getId());
				instrumentoPreguntaRepository.save(_instrumentoPregunta);
				return ResponseEntity.ok(_instrumentoPregunta);
			}else {
				
				Pregunta pregunta = new Pregunta();
				pregunta.setDescripcion(instrumentoPregunta.getPregunta().getDescripcion());
				pregunta.setUsuarioIngresoId(instrumentoPregunta.getUsuarioIngresoId());
				List<InstrumentoPregunta> listaInstrumentoPregunta = new ArrayList<>();
				
		
				_instrumentoPregunta.setPregunta(pregunta);
				_instrumentoPregunta.setUsuarioIngresoId(instrumentoPregunta.getUsuarioIngresoId());
				listaInstrumentoPregunta.add(_instrumentoPregunta);
				pregunta.setInstrumentoPreguntas(listaInstrumentoPregunta);
				preguntaRepository.save(pregunta);
				return ResponseEntity.ok(pregunta);
			}
			//BeanUtils.copyProperties(reglaCalificacion, _reglaCalificacion);
			
		} catch (Exception e) {
			e.printStackTrace();
			return RestUtil.preconditionFailedError(ALREADY_REPORTED);			
		}		
	} */
	
	/**
	 * Busca un instrimento la descripcion y el instrumento al que pertenece.
	 * 
	 * @param descripcion  de la pregunta
	 * @param idInstrumento Identificador instrumento
	 * @return
	 */
	@RequestMapping(value="/buscarDescripcionPregunta/{descripcion}/{idInstrumento}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarDescripcionPregunta(
			@PathVariable("descripcion") String descripcion,
			@PathVariable("idInstrumento") Integer idInstrumento) 
	{
		return ResponseEntity.ok(preguntaRepository.buscarDescripcionPregunta(descripcion,idInstrumento));//revisar consulta
	}
}
