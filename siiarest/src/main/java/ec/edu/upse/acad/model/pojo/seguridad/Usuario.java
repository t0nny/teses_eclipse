
package ec.edu.upse.acad.model.pojo.seguridad;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;


import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="seg", name="usuarios")
@NoArgsConstructor

public class Usuario {

	@Id

	@GeneratedValue(strategy=GenerationType.IDENTITY)
	
	@Getter @Setter private Integer id;
	
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="persona_id", nullable = false, insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Persona persona;
		
	@Getter @Setter private Integer persona_id;

	@Getter @Setter private String usuario;
	
	@JsonIgnore
	@Getter @Setter private String clave;
	
	@Getter @Setter private String token;
	
	@OneToMany(mappedBy="usuario", cascade=CascadeType.ALL)
	@Getter @Setter private List<ModuloRolUsuario> moduloRolUsuario;

	@Getter @Setter private String estado;
	
	@Version
	@Getter @Setter private Integer version;
	
//	atributos transitorio
	
    @Transient // solo existe aqui el atributo es transitorios 
    @Getter @Setter	private String claveAuxiliar;
    
    @Transient 
    @Getter @Setter	private Boolean enviaEmail;
    
    
    @PrePersist
	void preInsert() {
	   if (this.estado == null)
	       this.estado = "AC";
	}
    
  
}
