package ec.edu.upse.acad.model.pojo;

import javax.persistence.*;
import org.hibernate.annotations.Where;
import com.fasterxml.jackson.annotation.JsonIgnore;
import ec.edu.upse.acad.model.pojo.matricula.EstudianteOferta;
import ec.edu.upse.acad.model.pojo.reglamento.Reglamento;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Entity
@Table(schema="aca", name="malla")
@Where(clause = "estado='A' or estado='P'")
@NoArgsConstructor
public class Malla  {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_malla")
	@Getter @Setter private Integer id;

	
	@Column(name="id_nivel_min_aperturado")
	@Getter @Setter private Integer idNivelMinAperturado;

	@Column(name="id_nivel_max_aperturado")
	@Getter @Setter private Integer idNivelMaxAperturado;	
	
	@Column(name="id_reglamento")
	@Getter @Setter private Integer idReglamento;
	
	@Column(name="id_departamento_oferta")
	@Getter @Setter private Integer idDepartamentoOferta;

	@Getter @Setter private String descripcion;
	
	@Column(name="version_malla")
	@Getter @Setter private Integer versionMalla;

	@Column(name="num_creditos")
	@Getter @Setter private Integer numCreditos;
	
	@Column(name="num_horas_maximo")
	@Getter @Setter private Integer numeroHorasMaximo;

	@Column(name="num_asignaturas")
	@Getter @Setter private Integer numAsignaturas;

	@Column(name="num_asignaturas_aprobar")
	@Getter @Setter private Integer numAsignaturasAprobar;
	
	@Column(name="num_niveles")
	@Getter @Setter private Integer numNiveles;
	
	@Column(name="factor_mult_componente_docencia")
	@Getter @Setter private Double factorMultComponenteDocencia;

	@Column(name="fecha_aprobacion")
	@Getter @Setter private String fechaAprobacion;

	@Column(name="fecha_desde")
	@Getter @Setter private String fechaDesde;

	@Column(name="fecha_hasta")
	@Getter @Setter private String fechaHasta;

	@Getter @Setter private String estado;

//	@Column(name="fecha_ingreso")
//	@Getter @Setter private String fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;

	//RELACIONES

	//bi-directional many-to-one association to Nivel
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_nivel_min_aperturado", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Nivel nivel;

	//bi-directional many-to-one association to Nivel
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_nivel_max_aperturado", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Nivel nivel1;		
	
	//bi-directional many-to-one association to Reglamento
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_reglamento", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Reglamento reglamento;

	//bi-directional many-to-one association to DepartamentoOferta
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_departamento_oferta", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private DepartamentoOferta departamentoOferta;
	
	//bi-directional many-to-one association to EstudianteOferta
	@OneToMany(mappedBy="malla", cascade=CascadeType.ALL)
//	@JsonIgnore
	@Getter @Setter private List<EstudianteOferta> estudianteOfertas;

	//bi-directional many-to-one association to MallaAsignatura
	@OneToMany(mappedBy="malla", cascade=CascadeType.ALL)
	@Getter @Setter private List<MallaAsignatura> mallaAsignaturas;

	//bi-directional many-to-one association to MallaRequisito
	@OneToMany(mappedBy="malla", cascade=CascadeType.ALL)
	@Getter @Setter private List<MallaRequisito> mallaRequisitos;

	//bi-directional many-to-one association to Periodo Malla Version
	@OneToMany(mappedBy="malla", cascade=CascadeType.ALL)
	@Getter @Setter private List<PeriodoMalla> periodoMallaVersion;

	@PrePersist
	void preInsert() {
		if (this.estado == null)
			this.estado = "A";
	}

	
    @Transient // solo existe aqui el atributo es transitorios 
    @Getter @Setter	private Integer listaPeriodoMalla;
    
    @Transient 
    @Getter @Setter	private Integer listaEstudianteOfertas;
    
    @Transient 
    @Getter @Setter	private Integer listaMallaAsignaturas;
    
    @Transient 
    @Getter @Setter	private Integer listaMallaRequisitos;

}