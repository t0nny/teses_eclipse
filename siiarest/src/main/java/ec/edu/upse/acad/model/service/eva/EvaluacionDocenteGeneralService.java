package ec.edu.upse.acad.model.service.eva;



import java.sql.Date;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.upse.acad.model.pojo.evaluacion.EvaluacionDocenteGeneral;
import ec.edu.upse.acad.model.repository.evaluacion.EvaluacionDocenteGeneralRepository;

@Service
@Transactional
public class EvaluacionDocenteGeneralService {

	@Autowired private EvaluacionDocenteGeneralRepository evaluacionDocenteGeneralRepository;
	//@Autowired private ReglamentoRepository reglamentoRepository;
	
	@PersistenceContext private EntityManager em;
	
	public void saveEvalDocenteGeneral(EvaluacionDocenteGeneral evaluacionDocenteGeneral) {
		if(evaluacionDocenteGeneral.getId() != null) {
			//metodo para editar
			this.editaEvaluacionDocenteGeneral(evaluacionDocenteGeneral);
		}else {
			//metodo para nuevo
			this.nuevoEvaluacionDocenteGeneral(evaluacionDocenteGeneral);
		}
	}
	
	public void nuevoEvaluacionDocenteGeneral(EvaluacionDocenteGeneral evaluacionDocenteGeneral) {
		EvaluacionDocenteGeneral _evaluacionDocenteGeneral = new EvaluacionDocenteGeneral();
		BeanUtils.copyProperties(evaluacionDocenteGeneral, _evaluacionDocenteGeneral, "periodoEvaluacion");
		evaluacionDocenteGeneralRepository.saveAndFlush(_evaluacionDocenteGeneral);
		em.refresh(_evaluacionDocenteGeneral);
	}
	

/*	public void newEvaluacionDocenteGeneral(EvaluacionDocenteGeneral evaluacionDocenteGeneral) {
		EvaluacionDocenteGeneral _evaluacionDocenteGeneral = new EvaluacionDocenteGeneral();
		BeanUtils.copyProperties(evaluacionDocenteGeneral, _evaluacionDocenteGeneral, "periodoEvaluacion");
		Integer idEvalDocGeneral = evaluacionDocenteGeneralRepository.saveAndFlush(_evaluacionDocenteGeneral).getId();
		List<PeriodoEvaluacion> periodoEvaluacion = evaluacionDocenteGeneral.getPeriodoEvaluacion();
		//RECORRIDO PARA LA INSERCIÓN EN PERIODO EVALUACIÓN
		for (PeriodoEvaluacion __periodoEvaluacion : periodoEvaluacion) {
			this.addPeriodoEvaluacion(__periodoEvaluacion, idEvalDocGeneral);
		}		
		em.refresh(_evaluacionDocenteGeneral);
	}*/
	
	/**
	 * Agrega un nuevo Periodo de Evaluación 
	 * @param periodoEvaluacion Objeto de tipo PeriodoEvaluacion
	 * @param idEvalDocGeneral Identificador de Evaluación Docente General
	 * @return Identificador de un nuevo Periodo de Evaluación
	 */
/*	public void addPeriodoEvaluacion(PeriodoEvaluacion periodoEvaluacion, Integer idEvalDocGeneral) {
		PeriodoEvaluacion __periodoEvaluacion = new PeriodoEvaluacion();
		BeanUtils.copyProperties(periodoEvaluacion, __periodoEvaluacion, "perTipoEvalInstrumento");
		Integer idPeriodoEvaluacion = periodoEvaluacionRepository.saveAndFlush(__periodoEvaluacion).getId();
		List<PerTipoEvalInstrumento> perTipoEvalInstrumento = periodoEvaluacion.getPerTipoEvalInstrumento();
		for (PerTipoEvalInstrumento __perTipoEvalInstrumento : perTipoEvalInstrumento) {
			this.addPeriodoTipoEvaluacionInstrumento(__perTipoEvalInstrumento, idPeriodoEvaluacion);
		}
	}*/
	
	/**
	 * Agrega un nuevo Periodo Tipo Evaluacion Instrumento
	 * @param perTipoEvalInstrumento Objeto de tipo PerTipoEvalInstrumento
	 * @param idPeriodoEvaluacion Identificador de Periodo de Evaluación
	 */
/*	public void addPeriodoTipoEvaluacionInstrumento(PerTipoEvalInstrumento perTipoEvalInstrumento, Integer idPeriodoEvaluacion) {
		PerTipoEvalInstrumento __perTipoEvalInstrumento = new PerTipoEvalInstrumento();
		BeanUtils.copyProperties(perTipoEvalInstrumento, __perTipoEvalInstrumento);
		perTipoEvalInstrumentoRepository.saveAndFlush(__perTipoEvalInstrumento);
	}
	*/

	public void editaEvaluacionDocenteGeneral(EvaluacionDocenteGeneral evaluacionDocenteGeneral) {
		Integer idEvalDocGral = evaluacionDocenteGeneral.getId();
		EvaluacionDocenteGeneral _evaluacionDocenteGeneral = evaluacionDocenteGeneralRepository.findById(idEvalDocGral).get();
		String _descripcion = evaluacionDocenteGeneral.getDescripcion();
		Date _fechaDesde = evaluacionDocenteGeneral.getFechaDesde();
		String _usuarioIngresoId = evaluacionDocenteGeneral.getUsuarioIngresoId();
		if(evaluacionDocenteGeneral.getFechaHasta() != null) {
			Date _fechaHasta = evaluacionDocenteGeneral.getFechaHasta();
			_evaluacionDocenteGeneral.setFechaHasta(_fechaHasta);
		}
		_evaluacionDocenteGeneral.setDescripcion(_descripcion);
		_evaluacionDocenteGeneral.setFechaDesde(_fechaDesde);
		_evaluacionDocenteGeneral.setUsuarioIngresoId(_usuarioIngresoId);
		evaluacionDocenteGeneralRepository.save(_evaluacionDocenteGeneral);
	}
	
	public void deleteEvaluacionDocenteGeneral(Integer idEvalDocGral) {
		EvaluacionDocenteGeneral evaluacionDocenteGeneral = evaluacionDocenteGeneralRepository.findById(idEvalDocGral).get();
		java.util.Date fecha = new java.util.Date();
		Date fecha2 = new Date(fecha.getTime());
		if(evaluacionDocenteGeneral != null) {
			
			if(evaluacionDocenteGeneral.getFechaHasta() == null) {
				evaluacionDocenteGeneral.setFechaHasta(fecha2);
			}
			evaluacionDocenteGeneral.setEstado("I");
			evaluacionDocenteGeneralRepository.save(evaluacionDocenteGeneral);
		}		
	}
}
