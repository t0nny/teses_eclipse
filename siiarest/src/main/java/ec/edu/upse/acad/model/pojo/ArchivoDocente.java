package ec.edu.upse.acad.model.pojo;

import java.sql.Timestamp;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="aca", name="archivo_docente")
@NoArgsConstructor
public class ArchivoDocente{

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_archivo_docente")
	@Getter @Setter private Integer id;

	@Getter @Setter private String estado;
	
	@Column(name="fecha_ingreso")
	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;
	
	@Column(name="id_docente")
	@Getter @Setter private Integer idDocente;
	
	@Column(name="id_archivo")
	@Getter @Setter private Integer idArchivo;
	
	
	//RELACIONES
	//bi-directional many-to-one association to Archivo
	@ManyToOne
	@JoinColumn(name="id_archivo" , insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Archivo archivo;

	//bi-directional many-to-one association to Docente
	@ManyToOne
	@JoinColumn(name="id_docente", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Docente docente;
	
	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }
}