package ec.edu.upse.acad.model.pojo.planificacion_docente;



import javax.persistence.*;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The persistent class for the plan_estrategia database table.
 * 
 */
@Entity
@Table(schema="aca", name="plan_estrategia")
@Where(clause = "estado='A'")
@NoArgsConstructor
public class PlanEstrategia  {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_plan_estrategia")
	@Getter @Setter private Long id;
	
	@Column(name="id_estrategia_evaluacion")
	@Getter @Setter private Integer idEstrategiaEvaluacion;
	
	@Column(name="id_plan_clase")
	@Getter @Setter private Long idPlanClase;

	@Getter @Setter private String estado;

//	@Column(name="fecha_ingreso")
//	private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;

	//RELACIONES
	//bi-directional many-to-one association to EstrategiaEvaluacion
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_estrategia_evaluacion", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private EstrategiaEvaluacion estrategiaEvaluacion;

	//bi-directional many-to-one association to PlanClase
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_plan_clase", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private PlanClase planClase;

	@PrePersist
	void preInsert() {
	   if (this.estado == null)
	       this.estado = "A";
	}
	

}