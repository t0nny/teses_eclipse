package ec.edu.upse.acad.model.pojo.planificacion_docente;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ec.edu.upse.acad.model.pojo.ComponenteAprendizaje;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="aca", name="contenido_componente_aprendizaje")
@NoArgsConstructor
public class ContenidosCompAprendizaje {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_contenido_componente_aprendizaje")
	@Getter @Setter private Integer id;

	@Column(name = "id_contenidos")
	@Getter @Setter private Integer idContenidos;
	
	@Column(name = "id_componente_aprendizaje")
	@Getter @Setter private Integer idComponenteAprendizaje;
	
	@Getter @Setter private String estado;
	
	@Column(name = "usuario_ingreso_id")
	@Getter @Setter private Integer usuarioIngresoId;
	
	@Version
	@Getter @Setter private Integer version;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_contenidos" , insertable=false, updatable = false)
	@JsonIgnore 
	@Getter @Setter private Contenido contenido;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_componente_aprendizaje" , insertable=false, updatable = false)
	@JsonIgnore 
	@Getter @Setter private ComponenteAprendizaje componenteAprendizaje;
	
	@PrePersist
	void preInsert() {
		if (this.estado == null)
			this.estado = "A";
	}
	
}
