package ec.edu.upse.acad.model.pojo;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


import java.sql.Timestamp;
import java.util.Date;


@Entity
@Table(schema="aca", name="docente_historial")
@NoArgsConstructor
public class DocenteHistorial{

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_docente_historial")

	@Getter @Setter private Integer id;
	
	@Column(name="id_docente")
	@Getter @Setter private Integer idDocente;
	
	@Column(name="id_docente_categoria")
	@Getter @Setter private Integer idDocenteCategoria;

	@Getter @Setter private String estado;

	@Column(name="fecha_desde")
	@Getter @Setter private Date fechaDesde;

	@Column(name="fecha_hasta")
	@Getter @Setter private Date fechaHasta;

	@Getter @Setter private String observacion;
	
	@Column(name="fecha_ingreso")
	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;
	
	

	
	//RELACIONES//

	//bi-directional many-to-one association to Docente
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_docente", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Docente docente;

	//bi-directional many-to-one association to Docente Categoria
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_docente_categoria", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private DocenteCategoria docenteCategoria;
	
	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }

}