package ec.edu.upse.acad.model.pojo.planificacion_docente;


import javax.persistence.*;

import org.hibernate.annotations.Where;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.util.List;


/**
 * The persistent class for the tipo_bibliografia database table.
 * 
 */
@Entity
@Table(schema="aca", name="tipo_bibliografia")
@Where(clause = "estado='A'")
@NoArgsConstructor
public class TipoBibliografia  {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_tipo_bibliografia")
	@Getter @Setter private Integer id;

	@Getter @Setter private String codigo;

	@Getter @Setter private String descripcion;

	@Getter @Setter private String estado;

//	@Column(name="fecha_ingreso")
//	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;

	//RELACIONES
	//bi-directional many-to-one association to RecursoBibliografico
	@OneToMany(mappedBy="tipoBibliografia", cascade=CascadeType.ALL)
	@Getter @Setter private List<RecursoBibliografico> recursoBibliograficos;

	@PrePersist
	void preInsert() {
	   if (this.estado == null)
	       this.estado = "A";
	}

}