package ec.edu.upse.acad.model.pojo.evaluacion;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import ec.edu.upse.acad.model.pojo.reglamento.Reglamento;
import ec.edu.upse.acad.model.pojo.seguridad.Persona;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Entity
@Table(schema="eva", name="comision_eva_aulica")
@NoArgsConstructor
public class ComisionEvaAulica {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_com_eval_aulica")
	@Getter @Setter private Integer id;
	
//	@Column(name="id_reglamento")
//	@Getter @Setter private Integer idReglamento;
//	
//	@Column(name="id_persona")
//	@Getter @Setter private Integer idPersona;
	
	@Getter @Setter private String descripcion;
	
	@Getter @Setter private String estado;
	
//	@Column(name="fecha_ingreso")
//	@Getter @Setter private Timestamp fechaIngreso;
//	
	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;
	
	@Version
	@Getter @Setter private Integer version;
	
	//RELACIONES
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_reglamento", insertable=false, updatable = false)
	//@JsonIgnore
	@Getter @Setter private Reglamento reglamento;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_persona", insertable=false, updatable = false)
	//@JsonIgnore
	@Getter @Setter private Persona aulPersona;
	
	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }
}
