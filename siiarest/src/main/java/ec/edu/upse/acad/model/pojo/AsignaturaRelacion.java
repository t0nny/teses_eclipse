package ec.edu.upse.acad.model.pojo;

import javax.persistence.*;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Table(schema="aca",name="asignatura_relacion")
@Where(clause = "estado='A'")
@NoArgsConstructor
public class AsignaturaRelacion {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_asignatura_relacion")
	@Getter @Setter private Integer id;

	@Getter @Setter private String estado;
	
//	@Column(name="fecha_ingreso")
//	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;
	
	@Column(name="tipo_relacion")
	@Getter @Setter private String tipoRelacion;
	
	@Column(name="id_malla_asignatura")
	@Getter @Setter private Integer idMallaAsignatura;
	
	@Column(name="id_malla_asignatura_relacion")
	@Getter @Setter private Integer idMallaAsignaturaRelacion;
	
    //RELACIONES
	//bi-directional many-to-one association to MallaAsignatura
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_malla_asignatura", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private MallaAsignatura mallaAsignatura;

	//bi-directional many-to-one association to MallaAsignatura
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="id_malla_asignatura_relacion", insertable=false, updatable = false)
//	@JsonIgnore
//	@Getter @Setter private MallaAsignatura mallaAsignatura2;

	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }

}