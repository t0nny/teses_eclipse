package ec.edu.upse.acad.model.pojo;

import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;



import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="aca", name="asignatura_paralelo")
@NoArgsConstructor
public class AsignaturaParalelo {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_asignatura_paralelo")
	@Getter @Setter private Integer id;
	
	@Column(name="id_malla_asignatura")
	@Getter @Setter private Integer idMallaAsignatura;
	
	@Column(name="id_paralelo")
	@Getter @Setter private Integer idParalelo;
	
	@Column(name="num_estudiantes_proyectado")
	@Getter @Setter private Integer numEstudiantesProyectado;
	
    @Getter @Setter  private String estado;
	
	@Column(name="fecha_ingreso")
	@Getter @Setter  private Date fechaIngreso;
	
	@Column(name="usuario_ingreso_id")
	@Getter @Setter  private String usuarioIngresoId;
	
	@Version
	@Getter @Setter  private Integer version;
	
	//RELACIONES
//		//bi-directional many-to-one association to Tipo de Reglamento
//				@ManyToOne(fetch=FetchType.LAZY)
//				@JoinColumn(name="id_paralelo" , insertable=false, updatable = false)
//				@JsonIgnore
//				@Getter @Setter private Paralelo paralelo;
//		
//		//bi-directional many-to-one association to Tipo de Reglamento
//				@ManyToOne(fetch=FetchType.LAZY)
//				@JoinColumn(name="id_malla_asignatura" , insertable=false, updatable = false)
//				@JsonIgnore
//				@Getter @Setter private MallaAsignatura mallaAsignatura;
//				
//		//bi-directional many-to-one association to Actividad Personal
//			    @OneToMany(mappedBy="asignaturaParalelo", cascade=CascadeType.ALL)
//			    @Getter @Setter private List<DocenteAsignaturaAprend> docenteComponente;	
	    
		 @PrePersist
			void preInsert() {
			   if (this.estado == null)
			       this.estado = "A";
		 }
	
}
