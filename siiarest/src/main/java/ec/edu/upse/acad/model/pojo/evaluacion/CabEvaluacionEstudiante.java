package ec.edu.upse.acad.model.pojo.evaluacion;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import ec.edu.upse.acad.model.pojo.matricula.EstudianteMatricula;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="eva", name="cab_evaluacion_estudiante")
@NoArgsConstructor
public class CabEvaluacionEstudiante {

    @Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_cab_evaluacion_estudiante")
	@Getter @Setter private Integer id;

	//@Column(name="id_estudiante_matricula")
	//@Getter @Setter private Integer idEstudianteMatricula;
	 
	@Column(name="fecha_realizada")
	@Getter @Setter private Timestamp fechaRealizada;

	@Getter @Setter private String estado;
	
	@Column(name="fecha_ingreso")
	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;
	
	//RELACIONES
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_estudiante_matricula", insertable=true, updatable = true)
	//@JsonIgnore
	@Getter @Setter private EstudianteMatricula idEstudianteMatricula;
	
	//bi-directional many-to-one association to DetEvaluacionDocente
	@OneToMany(mappedBy="idCabEvaluacionEstudiante", cascade=CascadeType.ALL)
	@Getter @Setter private List<DetEvaluacionEstudiante> detEvaluacionEstudiante;
	
	@PrePersist
	void preInsert() {
	   if (this.estado == null)
	       this.estado = "A";
	}	
 
}
