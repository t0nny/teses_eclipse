package ec.edu.upse.acad.model.pojo.reglamento;

import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ec.edu.upse.acad.model.pojo.Malla;
import ec.edu.upse.acad.model.pojo.ReglamentoCompAprendizaje;
import ec.edu.upse.acad.model.pojo.ReglamentoCompOrganizacion;
import ec.edu.upse.acad.model.pojo.calificaciones.CalificacionGeneral;
import ec.edu.upse.acad.model.pojo.calificaciones.ReglamentoCiclo;
import ec.edu.upse.acad.model.pojo.distributivo.DistributivoGeneral;
import ec.edu.upse.acad.model.pojo.evaluacion.ReglaComponente;
import ec.edu.upse.acad.model.pojo.matricula.MatriculaGeneral;
import ec.edu.upse.acad.model.pojo.matricula.ReglamentoTipoMatricula;
import ec.edu.upse.acad.model.pojo.matricula.ReglamentoTipoOferta;
import ec.edu.upse.acad.model.pojo.matricula.ValidacionGeneral;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="aca", name="reglamento")
@NoArgsConstructor
public class Reglamento {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_reglamento")
	@Getter @Setter private Integer id;

	@Column(name="id_tipo_reglamento")
	@Getter @Setter  private Integer idTipoReglamento;

	@Getter @Setter  private String codigo;
	
	@Getter @Setter  private String nombre;

	@Column(name="fecha_aprobacion_ies")
	@Getter @Setter  private Date fechaAprobacionIes;

	@Column(name="fecha_hasta")
	@Getter @Setter  private Date fechaHasta;

	@Column(name="reglamento_nacional")
	@Getter @Setter  private String reglamentoNacional;

	@Getter @Setter  private String estado;

	@Column(name="fecha_ingreso")
	@Getter @Setter  private Date fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter  private String usuarioIngresoId;

	@Version
	@Getter @Setter  private Integer version;

	//RELACIONES

	//bi-directional many-to-one association to Tipo de Reglamento
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_tipo_reglamento" , insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private TipoReglamento tipoReglamento;


	//bi-directional many-to-one association to ReglamentoActividadPersonal
	@OneToMany(mappedBy="reglamento", cascade=CascadeType.ALL)
	@Getter @Setter private List<ReglamentoActividadDocente> reglamentoActividadDocentes;

	//bi-directional many-to-one association to dedicacion horas clases
	@OneToMany(mappedBy="reglamento", cascade=CascadeType.ALL)
	@Getter @Setter private List<DedicacionHorasClase> dedicacionHorasClases;

	//bi-directional many-to-one association to Actividad Personal
	@OneToMany(mappedBy="reglamento", cascade=CascadeType.ALL)
	@Getter @Setter private List<ReglamentoNumAsignatura> reglamentoNumAsignaturas;

	//bi-directional many-to-one association to Actividad Personal
	@OneToMany(mappedBy="reglamento", cascade=CascadeType.ALL)
	@Getter @Setter private List<DistributivoGeneral> distributivoGenerales;

	//bi-directional many-to-one association to ConfiguracionCalificacione
//	@OneToMany(mappedBy="reglamento", cascade=CascadeType.ALL)
//	@Getter @Setter private List<ConfiguracionCalificaciones> configuracionCalificaciones;

	//bi-directional many-to-one association to Malla
	@OneToMany(mappedBy="reglamento", cascade=CascadeType.ALL)
	@Getter @Setter private List<Malla> mallas;

	//bi-directional many-to-one association to MallaCompAprendizaje
	@OneToMany(mappedBy="reglamento", cascade=CascadeType.ALL)
	@Getter @Setter private List<ReglamentoCompAprendizaje> mallaCompAprendizajes;

	//bi-directional many-to-one association to MallaCompOrganizacion
	@OneToMany(mappedBy="reglamento", cascade=CascadeType.ALL)
	@Getter @Setter private List<ReglamentoCompOrganizacion> mallaCompOrganizacions;
	
	//bi-directional many-to-one association to MatriculaGeneral
	@OneToMany(mappedBy="reglamento", cascade=CascadeType.ALL)
	@Getter @Setter private List<MatriculaGeneral> matriculaGenerals; 
	
	//bi-directional many-to-one association to ValidacionGeneral
	@OneToMany(mappedBy="reglamento", cascade=CascadeType.ALL)
	@Getter @Setter private List<ValidacionGeneral> validacionGenerals;
	
	//bi-directional many-to-one association to ReglamentoTipoMatricula
	@OneToMany(mappedBy="reglamento", cascade=CascadeType.ALL)
	@Getter @Setter private List<ReglamentoTipoMatricula> reglamentoTipoMatriculas;

	//bi-directional many-to-one association to ReglamentoTipoOferta
	@OneToMany(mappedBy="reglamento", cascade=CascadeType.ALL)
	@Getter @Setter private List<ReglamentoTipoOferta> reglamentoTipoOfertas;
	
	//bi-directional many-to-one association to CalificacionGeneral
	@OneToMany(mappedBy="reglamento")
	@Getter @Setter private List<CalificacionGeneral> calificacionGenerals;
	
	//bi-directional many-to-one association to ReglaCalificacion
	//relacion con modulo de evaluacion
//	@OneToMany(mappedBy="reglamento")
//	@Getter @Setter private List<ReglaCalificacion> reglaCalificacionEva;
//
//	
	//bi-directional one-to-many association to ReglamentoCiclo
	@OneToMany(mappedBy="reglamento")
	@Getter @Setter private List<ReglamentoCiclo> reglamentoCiclos;
	
	@OneToMany(mappedBy="reglamento")
	@JsonIgnore
	@Getter @Setter private List<ReglaComponente> reglaComponente;


	@PrePersist
	void preInsert() {
		if (this.estado == null)
			this.estado = "A";
	}

}
