package ec.edu.upse.acad.model.pojo.matricula;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.annotations.Where;

//import org.eclipse.persistence.annotations.AdditionalCriteria;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ec.edu.upse.acad.model.pojo.reglamento.Reglamento;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="aca", name="subtipo_movilidad")
@Where(clause = "estado='A'")
@NoArgsConstructor
public class SubtipoMovilidad {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_subtipo_movilidad")
	@Getter @Setter private Integer id;
	
	@Column(name="id_tipo_movilidad")
	@Getter @Setter  private Integer idTipoMovilidad;

	@Column(name="codigo")
	@Getter @Setter private String codigo;
	
	@Column(name="descripcion")
	@Getter @Setter private String descripcion;

	@Getter @Setter private String estado;

//	@Column(name="fecha_ingreso")	
//	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private int version;

	//RELACIONES
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_tipo_movilidad" , insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private TipoMovilidad tipoMovilidad;

	
	
	//bi-directional many-to-one association to ReglamentoTipoMatricula
	@OneToMany(mappedBy="subtipoMovilidad", cascade=CascadeType.ALL)
	@Getter @Setter private List<Movilidad> movilidad;
}
