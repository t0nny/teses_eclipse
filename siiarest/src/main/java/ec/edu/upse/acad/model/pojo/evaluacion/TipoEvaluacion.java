package ec.edu.upse.acad.model.pojo.evaluacion;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ec.edu.upse.acad.model.pojo.distributivo.DocenteDedicacion;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="eva", name="tipo_evaluacion")
@NoArgsConstructor
public class TipoEvaluacion {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_tipo_evaluacion")
	@Getter @Setter private Integer id;
	
	@Column(name="id_funcion_evaluacion")
	@Getter @Setter private Integer idFuncionEvaluacion;
	
	@Column(name="id_regla_componente")
	@Getter @Setter private Integer idReglaComponente;

	@Column(name="ponderacion")
	@Getter @Setter private Integer ponderacion;

	@Getter @Setter private String estado;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;
	
	//RELACIONES
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_funcion_evaluacion", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private FuncionEvaluacion funcionEvaluacion;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_regla_componente", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private ReglaComponente reglaComponente;
	
	@OneToMany(mappedBy="tipoEvaluacion", cascade=CascadeType.ALL)
	@JsonIgnore
	@Getter @Setter private List<PerTipoEvalInstrumento> perTipoEvalInstrumento;

	@PrePersist
	void preInsert() {
		if (this.estado == null)
			this.estado = "A";
	}

}
