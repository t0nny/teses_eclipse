package ec.edu.upse.acad.model.pojo.evaluacion;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Entity
@Table(schema="eva", name="instrumento")
@NoArgsConstructor
public class Instrumento {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_instrumento")
	@Getter @Setter private Integer id;

	@Column(name="descripcion")
	@Getter @Setter private String descripcion;
	
	@Getter @Setter private String estado;

//	@Column(name="fecha_ingreso")
//	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;
	
	@Version
	@Getter @Setter private Integer version;
	
	//RELACIONES
	@OneToMany(mappedBy="instrumento", cascade=CascadeType.ALL)
	@JsonIgnore
	@Getter @Setter private List<InstrumentoPregunta> instrumentoPregunta;

	
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="id_componente_funcion_dedicacion", insertable=false, updatable = false)
//	@JsonIgnore
//	@Getter @Setter private TipoEvaluacion componenteFuncionDedicacion;
	
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="id_componente", insertable=false, updatable = false)
//	@JsonIgnore
//	@Getter @Setter private Componente componente;

//	//bi-directional many-to-one association to 
//	@OneToMany(mappedBy="instrumento", cascade=CascadeType.ALL)
//	@Getter @Setter private List<OpcionPregunta> opcionPreguntas;

	@OneToMany(mappedBy="instrumento", cascade=CascadeType.ALL)
	@JsonIgnore
	@Getter @Setter private List<PerTipoEvalInstrumento> perTipoEvalInstrumento;

	@PrePersist
	void preInsert() {
		if (this.estado == null)
			this.estado = "A";
	}

	
}
