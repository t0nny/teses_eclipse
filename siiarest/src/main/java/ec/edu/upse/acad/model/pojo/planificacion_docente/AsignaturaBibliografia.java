package ec.edu.upse.acad.model.pojo.planificacion_docente;

import javax.persistence.*;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ec.edu.upse.acad.model.pojo.Asignatura;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The persistent class for the asignatura_bibliografia database table.
 * 
 */
@Entity
@Table(schema="aca", name="asignatura_bibliografia")
@Where(clause = "estado='A'")
@NoArgsConstructor

public class AsignaturaBibliografia  {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_asignatura_bibliografia")
	@Getter @Setter private Integer id;
	
	@Column(name="id_asignatura")
	@Getter @Setter private Integer idAsignatura;
	
	@Column(name="id_recurso_bibliografico")
	@Getter @Setter private Long idRecursoBibliografico;

	@Getter @Setter private String estado;

//	@Column(name="fecha_ingreso")
//	private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;

	//RELACIONES
	//bi-directional many-to-one association to Asignatura
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_asignatura", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Asignatura asignatura;

	//bi-directional many-to-one association to RecursoBibliografico
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_recurso_bibliografico", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private RecursoBibliografico recursoBibliografico;

	@PrePersist
	void preInsert() {
	   if (this.estado == null)
	       this.estado = "A";
	}
	
}