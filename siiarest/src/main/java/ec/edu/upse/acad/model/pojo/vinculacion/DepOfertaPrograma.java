package ec.edu.upse.acad.model.pojo.vinculacion;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema = "vin", name = "dep_oferta_programa")

@NoArgsConstructor //un constructorsin argumentos
public class DepOfertaPrograma {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_dep_oferta_programa")
	@Getter	@Setter	private Integer id;

	@Column(name = "id_programa")
	@Getter	@Setter	private Integer idPrograma;
	
	@Column(name = "id_departamento_oferta")
	@Getter	@Setter	private Integer idDepartamentoOferta;
	
		@Column(name = "usuario_ingreso_id")
	@Getter	@Setter	private String usuarioIngresoId;

	@Version
	@Getter	@Setter	private Integer version;

	@Getter	@Setter	private String estado;

}
