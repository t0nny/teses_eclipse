package ec.edu.upse.acad.model.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.seguridad.ModuloRolUsuario;


@Repository

public interface ModulosRolesUsuariosRepository 
extends JpaRepository<ModuloRolUsuario, Integer>{

//RolUsuario findByRol(Integer rol_id);
	
	@Query(value=" SELECT mru "
			   + " FROM ModuloRolUsuario as mru "
			   + " INNER JOIN ModuloRol mr ON mru.moduloRol.id = mr.id "
			   + " INNER JOIN Usuario u ON mru.usuario.id = u.id "
			   + " WHERE u.usuario = ?1 "
			   + " AND mru.estado = 'AC'"
			   + " AND mr.estado = 'AC'"
			   + " AND u.estado = 'AC'  ")
	ModuloRolUsuario findByUser(String usuario);




}