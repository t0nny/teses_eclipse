package ec.edu.upse.acad.model.pojo;


import javax.persistence.*;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(schema="aca", name="itinerario")
@NoArgsConstructor
public class Itinerario  {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_itinerario")
	@Getter @Setter private Integer id;

	@Getter @Setter private String codigo;

	@Getter @Setter private String descripcion;

	@Column(name="descripcion_corta")
	@Getter @Setter private String descripcionCorta;

	@Getter @Setter private String estado;
	
	@Column(name="fecha_ingreso")
	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;

	
	//RELACIONES
	//bi-directional many-to-one association to ItinerarioMateria
	@OneToMany(mappedBy="itinerario", cascade=CascadeType.ALL)
	@Getter @Setter private List<ItinerarioMateria> itinerarioMaterias;
	
	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }

}