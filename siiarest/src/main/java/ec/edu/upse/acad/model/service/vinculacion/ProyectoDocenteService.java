package ec.edu.upse.acad.model.service.vinculacion;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.upse.acad.model.pojo.vinculacion.ProyectoDocente;
import ec.edu.upse.acad.model.repository.vinculacion.ProyectoDocenteRepository;

@Service
@Transactional
public class ProyectoDocenteService {
	@PersistenceContext
	private EntityManager em;
	@Autowired
	private ProyectoDocenteRepository proyectoDocenteRepository;

	// borrar ProyectoDocente del grig
	public void borrarProyectoDocente(Integer idProyectoDocente) {
		ProyectoDocente _proyectoDocente = proyectoDocenteRepository.findById(idProyectoDocente).get();
		_proyectoDocente.setEstado("E");
		proyectoDocenteRepository.save(_proyectoDocente);
		em.getEntityManagerFactory().getCache().evict(ProyectoDocente.class);
	}

}
