package ec.edu.upse.acad.model.pojo;


import java.sql.Timestamp;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="aca", name="compatibilidad_asignatura")
@NoArgsConstructor
public class CompatibilidadAsignatura{

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_compatibilidad_asignatura")
	@Getter @Setter private Integer id;
	
	@Column(name="id_asignatura")
	@Getter @Setter private Integer idAsignatura;

	@Column(name="id_asignatura_comp")
	@Getter @Setter private Integer idAsignaturaComp;

	@Getter @Setter private String estado;
	
	@Column(name="fecha_ingreso")
	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;

	
	//RELACIONES
	//bi-directional many-to-one association to Asignatura
	@ManyToOne
	@JoinColumn(name="id_asignatura", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Asignatura asignatura1;

	//bi-directional many-to-one association to Asignatura
	@ManyToOne
	@JoinColumn(name="id_asignatura_comp", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Asignatura asignatura2;
	
	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }
}