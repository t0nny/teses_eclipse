package ec.edu.upse.acad.model.repository.evaluacion;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.evaluacion.CalificacionEscala;

@Repository
public interface CalificacionEscalaRepository extends JpaRepository<CalificacionEscala, Integer> {
	//consulta para buscar las escala de calificacion 
	@Query(value="SELECT ce "
			+ "FROM CalificacionEscala ce "
			+ "WHERE ce.estado = 'A' ")
	List<CalificacionEscala> listarCalificacionEscala();

}
