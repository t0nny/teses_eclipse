package ec.edu.upse.acad.model.pojo;


import java.sql.Timestamp;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ec.edu.upse.acad.model.pojo.calificaciones.CicloAprendizaje;

import ec.edu.upse.acad.model.pojo.reglamento.Reglamento;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="aca", name="reglamento_comp_aprendizaje")
@NoArgsConstructor
public class ReglamentoCompAprendizaje  {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_reglamento_comp_aprendizaje")
	@Getter @Setter private Integer id;
	
	@Column(name="id_comp_aprendizaje")
	@Getter @Setter private Integer idCompAprendizaje;
	
	@Column(name="id_reglamento")
	@Getter @Setter private Integer idReglamento;
	
	@Column(name="asignar_docente")
	@Getter @Setter private boolean asignarDocente;
	
	@Getter @Setter private String estado;
	
	@Column(name="fecha_ingreso")
	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngreso_id;

	@Version
	@Getter @Setter private Integer version;
	
	
	//RELACIONES
	//bi-directional many-to-one association to ComponenteAprendizaje
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_comp_aprendizaje", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private ComponenteAprendizaje componenteAprendizaje;

    //bi-directional many-to-one association to Reglamento
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="id_reglamento", insertable=false, updatable = false)
    @JsonIgnore
    @Getter @Setter private Reglamento reglamento;

  //bi-directional one-to-many association to ReglamentoCiclo
  	@OneToMany(mappedBy="reglamentoCompAprendizaje")
  	@Getter @Setter private List<CicloAprendizaje> cicloAprendizajes;
	
	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }

	
}