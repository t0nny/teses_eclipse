package ec.edu.upse.acad.ws;

import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ec.edu.upse.acad.model.repository.GenerarArchivoRepository;
import ec.edu.upse.acad.model.repository.PeriodoDocenteAsignaturaRepository;
import ec.edu.upse.acad.model.repository.PeriodoDocenteAsignaturaRepository.customObjetbuscarPeriodoDocenteAsignatura;
import ec.edu.upse.acad.model.service.ExcelService;

@RestController
@RequestMapping("/api/excel")
@CrossOrigin

public class ExcelController {
	@Autowired private ExcelService excelService;
	@Autowired private PeriodoDocenteAsignaturaRepository periodoDocenteAsignaturaRepository;
	@Autowired private GenerarArchivoRepository generarArchivoRepository;

	@RequestMapping(value="/buscarDistributivoDocente/{idPeriodo}/{idTipoOferta}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarDistributivoDocente(@PathVariable("idPeriodo") Integer idPeriodo,
			@PathVariable("idTipoOferta") Integer idTipoOferta){
		return ResponseEntity.ok(generarArchivoRepository.consultaDistributivoDocente(idPeriodo,idTipoOferta));
	}
	
	@RequestMapping(value="/getExportarExcel", method=RequestMethod.GET)
    public ResponseEntity<?> getExportarExcel(HttpServletRequest request) throws Exception {
		List<customObjetbuscarPeriodoDocenteAsignatura> listaDocenteAsignatura=periodoDocenteAsignaturaRepository.buscarPeriodoDocenteAsignatura(63, "s");
        Resource resource=excelService.generarExcel(listaDocenteAsignatura);

        // Determina el contenido
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        // Si no se determina el tipo, asjjume uno por defecto.
        if(contentType == null) {
            contentType = "application/vnd.ms-excel";
        }
        return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"").body(resource);

    }
}
