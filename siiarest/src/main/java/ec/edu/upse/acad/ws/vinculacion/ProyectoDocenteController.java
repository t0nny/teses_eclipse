package ec.edu.upse.acad.ws.vinculacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.upse.acad.model.repository.vinculacion.ProyectoDocenteRepository;
import ec.edu.upse.acad.model.service.SecurityService;
import ec.edu.upse.acad.model.service.vinculacion.ProyectoDocenteService;

@RestController
@RequestMapping("/api/proyectoDocente")
@CrossOrigin
public class ProyectoDocenteController {
	@Autowired
	private SecurityService securityService;
	@Autowired
	private ProyectoDocenteService proyectoDocenteService;
	@Autowired
	private ProyectoDocenteRepository proyectoDocenteRepository;
	
	// buscar por el id de proyecto
	@RequestMapping(value = "/borrarProyectoDocenteGrid/{idProyectoDocente}", method = RequestMethod.GET)
	public ResponseEntity<?> borrarProyectoDocenteGrid(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable("idProyectoDocente") Integer idProyectoDocente) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		proyectoDocenteService.borrarProyectoDocente(idProyectoDocente);
		return ResponseEntity.ok().build();
	}
	
	@RequestMapping(value = "/docenteProyecto/{idProyecto}", method = RequestMethod.GET)
	public ResponseEntity<?> docenteProyecto(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable("idProyecto") Integer idProyecto) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(proyectoDocenteRepository.docenteProyecto(idProyecto));
	}

}
