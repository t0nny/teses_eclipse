package ec.edu.upse.acad.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.ReglamentoCompAprendizaje;


@Repository
public interface MallaCompAprendizajeRepository extends JpaRepository<ReglamentoCompAprendizaje, Integer>{

//	@Query(value="SELECT mg.id as codigo,ca.descripcion as nombre from MallaGeneral as mg " 
//			+ "INNER JOIN  MallaCompAprendizaje mca on mca.idMallaGeneral=mg.id " 
//			+ "INNER JOIN ComponenteAprendizaje ca on mca.idCompAprendizaje=ca.id ")
//	 List<CustomObject> listaComponentesAprendizaje();
//	
//	
//	@Query(value="select mg.id as codigo,ca.descripcion AS nombre from MallaGeneral as mg " 
//			+ "inner join  MallaCompAprendizaje mca on mca.idMallaGeneral=mg.id " 
//			+ "inner join ComponenteAprendizaje ca on mca.idCompAprendizaje=ca.id "
//			+ "WHERE mg.id=?1 ")
//	 List<CustomObject> listaComponentesAprendizaje(Integer id);
//	
//	interface CustomObject { 
//	    Integer getCodigo(); 
//	    String getNombre();   
//	} 

}
