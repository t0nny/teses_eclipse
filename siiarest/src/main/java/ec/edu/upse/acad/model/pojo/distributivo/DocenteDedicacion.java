package ec.edu.upse.acad.model.pojo.distributivo;


import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.*;

import ec.edu.upse.acad.model.pojo.reglamento.DedicacionHorasClase;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="aca", name="docente_dedicacion")
@NoArgsConstructor
public class DocenteDedicacion {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_docente_dedicacion")
	@Getter @Setter private Integer id;
	
	@Getter @Setter private String descripcion;
	
	@Column(name="numero_horas")
	@Getter @Setter private Integer numeroHoras;

	@Column(name="fecha_desde")
	@Getter @Setter private Date fechaDesde;

	@Column(name="fecha_hasta")
	@Getter @Setter private Date fechaHasta;
	
	@Getter @Setter private String estado;
	
	@Column(name="fecha_ingreso")
	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;
	
	@Version
	@Getter @Setter private Integer version;
	
	
	
	//RELACIONES
	//bi-directional many-to-one association to distributivo Dedicacion
    @OneToMany(mappedBy="docenteDedicacion", cascade=CascadeType.ALL)
    @Getter @Setter private List<DistributivoDedicacion> distributivoDedicacion;
	
	
    @OneToMany(mappedBy="docenteDedicacion", cascade=CascadeType.ALL)
    @Getter @Setter private List<DedicacionHorasClase> dedicacionHorasClases;
	
	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }

}