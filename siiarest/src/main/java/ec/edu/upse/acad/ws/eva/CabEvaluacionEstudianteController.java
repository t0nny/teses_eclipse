package ec.edu.upse.acad.ws.eva;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.upse.acad.model.pojo.evaluacion.CabEvaluacionEstudiante;
import ec.edu.upse.acad.model.service.SecurityService;
import ec.edu.upse.acad.model.service.eva.CabEvaluacionEstudianteService;

@RestController
@RequestMapping("/api/evaluaciones")
@CrossOrigin
public class CabEvaluacionEstudianteController {
	
	@Autowired 
	private SecurityService securityService;
	
	@Autowired 
	private CabEvaluacionEstudianteService cabEvaluacionEstudianteService;
	
	//graba heteroevaluacion 
	@RequestMapping(value="/grabarHeteroevaluacion", method=RequestMethod.POST)
	public ResponseEntity<?> grabarHeteroevaluacion(@RequestHeader(value="Authorization") String authorization, 
			@RequestBody CabEvaluacionEstudiante cabEvaluacionEstudiante) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		cabEvaluacionEstudianteService.newEvaluacionEstudiante(cabEvaluacionEstudiante);
		return  ResponseEntity.ok(cabEvaluacionEstudiante); 
	}			

}
