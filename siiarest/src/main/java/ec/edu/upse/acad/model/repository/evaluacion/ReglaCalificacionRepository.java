package ec.edu.upse.acad.model.repository.evaluacion;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ec.edu.upse.acad.model.pojo.evaluacion.ReglaCalificacion;

public interface ReglaCalificacionRepository extends JpaRepository<ReglaCalificacion, Integer>{
	//consulta para buscar ReglaCalificacion
	@Query(value=" select rc.id as id, rc.reglamento.id as idReglamento, r.nombre as descripcionRegla, rc.calificacionEscala.id as idCalificacionEscala,"
			+ "ce.descripcion as descripcionCalif, rc.valorMinimo as valorMinimo, rc.valorMaximo as valorMaximo " 
			+ "from ReglaCalificacion rc " 
			+ "inner join CalificacionEscala ce on rc.calificacionEscala.id = ce.id "
			+ "inner join Reglamento r on r.id = rc.reglamento.id "
			+ "WHERE rc.estado = 'A' and ce.estado = 'A' and r.estado = 'A' ")
	List<CustomObjectReglaCalificacion> listarReglaCalificacion();
	
	interface CustomObjectReglaCalificacion{
		Integer getId();
		Integer getIdReglamento();
		String getDescripcionRegla(); 
		Integer getIdCalificacionEscala();
		String getDescripcionCalif();
		Integer getValorMinimo(); 
		Integer getValorMaximo();		
	}
	
	/*select rc.id_regla_calificacion, rc.id_reglamento, rc.id_calificacion_escala, 
	ce.descripcion, rc.valor_minimo, rc.valor_maximo,
	rc.estado,rc.fecha_ingreso,rc.usuario_ingreso_id,rc.version
	from eva.regla_calificacion rc 
join eva.calificacion_escala ce on rc.id_calificacion_escala = ce.id_calificacion_escala
join aca.reglamento r on r.id_reglamento = rc.id_reglamento*/
	
	@Query(value=" select rc.id as idReglaCalificacion, rc.reglamento.id as idReglamento, rc.calificacionEscala.id as idCalificacionEscala,"
			+ "ce.descripcion as descripcion, rc.valorMinimo as valorMinimo, rc.valorMaximo as valorMaximo " 
			+ "from ReglaCalificacion rc " 
			+ "inner join CalificacionEscala ce on rc.calificacionEscala.id = ce.id "
			+ "inner join Reglamento r on r.id = rc.reglamento.id "
			+ "WHERE rc.estado = 'A' and ce.estado = 'A' and r.estado = 'A' and rc.calificacionEscala.id = ?1 ")
	List<CustomObjectReglaCalificacion> listarReglaCalificacionEscala(Integer idReglaCalificacionEscala);
	interface CustomObjectReglaCalificacionId{
		Integer getIdReglaCalificacion();
		Integer getIdReglamento();
		Integer getIdCalificacionEscala();
		String getDescripcion();
		Integer getValorMinimo();
		Integer getValorMaximo();
		
	}
	
	
	@Query(value=" select rc.id as id, r.id as idReglamento, r.nombre as descripcionRegla, rc.calificacionEscala.id as idCalificacionEscala, "
			+ "ce.descripcion as descripcion, rc.valorMinimo as valorMinimo, rc.valorMaximo as valorMaximo " 
			+ "from ReglaCalificacion rc " 
			+ "inner join CalificacionEscala ce on rc.calificacionEscala.id = ce.id "
			+ "inner join Reglamento r on r.id = rc.reglamento.id "
			+ "WHERE rc.estado = 'A' and ce.estado = 'A' and r.estado = 'A' and rc.id = ?1 ") 
	CustomObjectReglaCalificacionEscala findByIdRegCalifEsc(Integer idReglaCalificacionEscala);
	interface CustomObjectReglaCalificacionEscala{
		Integer getId();
		Integer getIdReglamento();
		String getDescripcionRegla();
		Integer getIdCalificacionEscala();
		String getDescripcion();
		Integer getValorMinimo();
		Integer getValorMaximo();		
	} 

}
