package ec.edu.upse.acad.model.repository.distributivo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.distributivo.MallaAsignaturaFusion;
@Repository
public interface MallaAsignaturaFusionRepository  extends JpaRepository<MallaAsignaturaFusion, Integer>{
	
	@Query(value="SELECT  mas.id as idMallaAsignatura, asi.descripcion as descripcion, asi.descripcionCorta as descripcionCorta, pp.numParalelos as numParalelos "+
			"FROM PeriodoMalla pma	"+		
			"INNER JOIN Malla ma on pma.idMalla=ma.id "+
			"INNER JOIN DepartamentoOferta do on ma.idDepartamentoOferta=do.id "+
			"INNER JOIN MallaAsignatura  mas on ma.id=mas.idMalla "+
			"INNER JOIN AsignaturaAprendizaje  aa on aa.idMallaAsignatura=mas.id "+
			"INNER JOIN PlanificacionParalelo pp on mas.id=pp.idMallaAsignatura and pma.idPeriodoAcademico=pp.idPeriodoAcademico  "+
			"INNER JOIN Asignatura as asi on mas.idAsignatura=asi.id " +  
			"INNER JOIN Nivel as n on mas.idNivel=n.id "+
			"WHERE pma.idPeriodoAcademico=?1 and do.idOferta=?2 and pp.idPeriodoAcademico=?1 and "+
			" isnull (pp.numParalelos,0)>0  and aa.estado='A' and aa.valor>0"+
			"and n.id>=ma.idNivelMinAperturado and n.id<=ma.idNivelMaxAperturado "+ 
			"and pma.estado='A' and ma.estado in ('A', 'P') and  do.estado='A' and mas.estado='A' "+
			"and pp.estado='A' and asi.estado='A' and n.estado='A'"+
			"group by  mas.id, asi.descripcion, asi.descripcionCorta,  pp.numParalelos"
			)
	List<CustObjMallaAsignaturaFusion> buscarMallaAsigatura(Integer idPeriodoAcademico, Integer idOferta);
	interface CustObjMallaAsignaturaFusion  { 
		Integer getIdMallaAsignatura(); 
		String getDescripcion(); 
		String getDescripcionCorta(); 
		Integer getNumParalelos(); 
		//Integer getNivel();
	
	}
	
	
	@Query(value="SELECT  mas.id as idMallaAsignatura, asi.descripcion as descripcion, asi.descripcionCorta as descripcionCorta,  "+
			 "    pp.numParalelos - " + 
			"			isnull(( select count (maf.idParalelo) as paralelo " + 
			"			from PlanificacionParalelo pp1 " + 
			"			INNER JOIN  MallaAsignaturaFusion as maf on pp1.idPeriodoAcademico=maf.idPeriodoAcademico " + 
			"							and pp1.idMallaAsignatura=maf.idMallaAsignatura " + 
			"			where 	pp1.idPeriodoAcademico=?1 and pp1.idMallaAsignatura=mas.id " + 
			"			group by pp1.idPeriodoAcademico, pp1.idMallaAsignatura),0) - " +
			"	isnull ((select count (maf.idParalelo) " + 
			"				from PlanificacionParalelo pp1 " + 
			"				INNER JOIN  MallaAsignaturaFusion as maf on pp1.idPeriodoAcademico=maf.idPeriodoAcademico " + 
			"				and pp1.idMallaAsignatura=maf.idMallaAsignatura " + 
			"				inner join DetalleMallaAsignaturaFusion as dmaf on maf.id=dmaf.idMallaAsignaturaFusion " + 
			"				where pp1.idPeriodoAcademico=?1  and pp1.idMallaAsignatura=mas.id " + 
			"			group by pp1.idMallaAsignatura),0) as numParalelos " + 
			"FROM PeriodoMalla pma	"+		
			"INNER JOIN Malla ma on pma.idMalla=ma.id "+
			"INNER JOIN DepartamentoOferta do on ma.idDepartamentoOferta=do.id "+
			"INNER JOIN MallaAsignatura  mas on ma.id=mas.idMalla "+
			//"INNER JOIN AsignaturaAprendizaje  aa on aa.idMallaAsignatura=mas.id  "+
			"INNER JOIN PlanificacionParalelo pp on mas.id=pp.idMallaAsignatura and pma.idPeriodoAcademico=pp.idPeriodoAcademico  "+
			"INNER JOIN Asignatura as asi on mas.idAsignatura=asi.id " + 
			"INNER JOIN Nivel as n on mas.idNivel=n.id "+
			"WHERE pma.idPeriodoAcademico=?1 and do.idOferta=?2 and pp.idPeriodoAcademico=?1 and "+
			" isnull (pp.numParalelos,0)>0 "+//and aa.estado='A' and aa.valor>0"+
			" and mas.id in (select mas1.id from Malla ma1 "+
			" 			inner join MallaAsignatura mas1 on ma1.id=mas1.idMalla "+
			"			inner join AsignaturaAprendizaje aa1 on aa1.idMallaAsignatura=mas1.id "+
			"			where aa1.estado='A' and ma1.estado='P' and mas1.estado='A' and aa1.valor>0 and mas.id=mas1.id) "+
			"and n.id>=ma.idNivelMinAperturado and n.id<=ma.idNivelMaxAperturado "+ 
			"and pma.estado='A' and ma.estado='P' and  do.estado='A' and mas.estado='A' "+
			"and pp.estado='A' and asi.estado='A' and n.estado='A'"+
			"group by  mas.id, asi.descripcion, asi.descripcionCorta,  pp.numParalelos")
	List<CustObjMallaAsigFusion> buscarMallaAsigOtraCar(Integer idPeriodoAcademico, Integer idOferta);
	interface CustObjMallaAsigFusion  { 
		Integer getIdMallaAsignatura(); 
		String getDescripcion(); 
		String getDescripcionCorta(); 
		Integer getNumParalelos(); 
		
	
	}
	
	
	
	
	@Query(value="SELECT  mas.id as idMallaAsignatura,aa.id as idAsignaturaAprendizaje,cap.id as idComponenteAprendizaje,"+
			"cap1.codigo as codigoComp, cap1.descripcion as descripcionCodigoComp,"+
			"cap.codigo as codigo,cap.descripcion as  descripcionCodigo, aa.valor as valor "+
			"FROM PeriodoMalla pma	"+		
			"INNER JOIN Malla ma on pma.idMalla=ma.id "+
			"INNER JOIN DepartamentoOferta do on ma.idDepartamentoOferta=do.id "+
			"INNER JOIN MallaAsignatura  mas on ma.id=mas.idMalla "+
			"INNER JOIN PlanificacionParalelo pp on mas.id=pp.idMallaAsignatura and pma.idPeriodoAcademico=pp.idPeriodoAcademico  "+
			"INNER JOIN AsignaturaAprendizaje aa on mas.id=aa.idMallaAsignatura "+
			"INNER JOIN ComponenteAprendizaje as cap on aa.idComponenteAprendizaje=cap.id "+
			"INNER JOIN ComponenteAprendizaje as cap1 on cap.idComponenteAprendizajePadre=cap1.id "+
			"INNER JOIN ReglamentoCompAprendizaje as rc on cap.id=rc.idCompAprendizaje "+
			"INNER JOIN Asignatura as asi on mas.idAsignatura=asi.id " + 
			"INNER JOIN OfertaAsignatura as oas on asi.id=oas.asignatura.id " + 
			"INNER JOIN Oferta as ofe on oas.oferta.id=ofe.id " + 
			"INNER JOIN Nivel as n on mas.idNivel=n.id "+
			"WHERE pma.idPeriodoAcademico=?1 and do.idOferta=?2 and pp.idPeriodoAcademico=?1 and "+
			" isnull (pp.numParalelos,0)>0 "+
			"and n.id>=ma.idNivelMinAperturado and n.id<=ma.idNivelMaxAperturado "+ 
			"and pma.estado='A' and ma.estado = 'P' and  do.estado='A' and mas.estado='A' and pp.estado='A' and asi.estado='A'" + 
			"and oas.estado='A' and ofe.estado='A' and n.estado='A' and aa.estado='A' and cap.estado='A' and rc.estado='A' and aa.valor>0 "+
			" and rc.asignarDocente=1"+
			"group by  mas.id, aa.id, cap.id, cap1.codigo, cap1.descripcion, cap.codigo, cap.descripcion  , aa.valor ")
	List<CustObjMallaAsignaturaFusionCom> buscarMallaAsigaturaCom(Integer idPeriodoAcademico, Integer idOferta);
	interface CustObjMallaAsignaturaFusionCom  { 
		Integer getIdMallaAsignatura(); 
		Integer getIdAsignaturaAprendizaje();
		Integer getIdComponenteAprendizaje();
		String getCodigoComp();
		String getDescripcionCodigoComp();
		String getCodigo();
		String getDescripcionCodigo();
		Integer getValor();
	
	}
	
	
	
	@Query(value="select  pma.idPeriodoAcademico as idPeriodoAcademico,  mas.id as idMallaAsignatura,ofe.id as idOfertaAsignatura, " + 
			"			ofe.descripcionCorta as ofertaAsignatura, par.id as idParalelo,	CONCAT(cast(n.orden as text),'/',par.descripcionCorta) as paralelo, " + 
			"			asi.descripcion as asignatura, asi.descripcionCorta as asignaturaCorta, n.orden as nivel , " + 
			"			maf.id as idMallaAsignaturaFusion,  maf.estado as estado, maf.version as version " + 
			"			FROM PeriodoMalla pma			" + 
			"			INNER JOIN Malla ma on pma.idMalla=ma.id " + 
			"			INNER JOIN  DepartamentoOferta do on ma.idDepartamentoOferta=do.id " + 
			"			INNER JOIN  MallaAsignatura  mas on ma.id=mas.idMalla " + 
			"			INNER JOIN  PlanificacionParalelo pp on mas.id=pp.idMallaAsignatura and pma.idPeriodoAcademico=pp.idPeriodoAcademico " + 
			"			INNER JOIN Asignatura as asi on mas.idAsignatura=asi.id " + 
			"			INNER JOIN OfertaAsignatura as oas on asi.id=oas.asignatura.id  " + 
			"			INNER JOIN Oferta as ofe on oas.oferta.id=ofe.id  " + 
			"			INNER JOIN Nivel as n on mas.idNivel=n.id " + 
			"			INNER JOIN Paralelo as par on par.orden<= isnull (pp.numParalelos,0)  " + 
			"			INNER JOIN MallaAsignaturaFusion as maf on mas.id=maf.idMallaAsignatura and par.id=maf.idParalelo and pma.idPeriodoAcademico=maf.idPeriodoAcademico " + 
			"			where pma.idPeriodoAcademico=?1 and do.idOferta=?2 and pp.idPeriodoAcademico=?1 and " + 
			"			 isnull (pp.numParalelos,0)>0  " + 
			"			and n.id>=ma.idNivelMinAperturado and n.id<=ma.idNivelMaxAperturado " + 
			"			and pma.estado='A' and ma.estado in('A', 'P') and  do.estado='A' and mas.estado='A' and pp.estado='A' and asi.estado='A'" + 
			"			and oas.estado='A' and ofe.estado='A' and n.estado='A' and par.estado='A' and maf.estado='A' ")
	List<CustObjMallaAsignaturaFusionCab> buscarMallaAsigaturaFusionCab(Integer idPeriodoAcademico, Integer idOferta);
	interface CustObjMallaAsignaturaFusionCab { 
		Integer getIdPeriodoAcademico(); 
		Integer getIdMallaAsignatura(); 
		Integer getIdOfertaAsignatura(); 
		String getOfertaAsignatura(); 
		Integer getIdParalelo(); 
		String getParalelo(); 
		String getAsignatura(); 
		String getAsignaturaCorta(); 
		String getNivel(); 
		Integer getIdMallaAsignaturaFusion(); 
		String getEstado();
		Integer getVersion(); 	
	}
	
	
	@Query(value="select  pma.idPeriodoAcademico as idPeriodoAcademico,  mas.id as idMallaAsignatura,ofe.id as idOfertaAsignatura, " + 
			"			ofe.descripcionCorta as ofertaAsignatura, par.id as idParalelo,	CONCAT(cast(n.orden as text),'/',par.descripcionCorta) as paralelo, " + 
			"			asi.descripcion as asignatura, asi.descripcionCorta as asignaturaCorta, n.orden as nivel , " + 
			"			maf.id as idMallaAsignaturaFusion,  maf.estado as estado, maf.version as version, " + 
			"			dmaf.id as idDetalleMallaAsignaturaFusion, dmaf.idMallaAsignatura as idMallaAsignaturaF, asif.descripcion as asignaturaFusion, " + 
			"			dmaf.idParalelo as idParaleloF,case when nf.orden  is not  null then CONCAT(cast(nf.orden as text),'/',parf.descripcionCorta) else '' end  as paraleloF, nf.orden as nivelF, "+
			"			ofef.id as idOfertaAsignaturaF, ofef.descripcionCorta as ofertaAsignaturaF, "+
			" 			dmaf.estado as estadoF , dmaf.version as versionF " +  
			"			FROM PeriodoMalla pma			" + 
			"			INNER JOIN Malla ma on pma.idMalla=ma.id " + 
			"			INNER JOIN  DepartamentoOferta do on ma.idDepartamentoOferta=do.id " + 
			"			INNER JOIN  MallaAsignatura  mas on ma.id=mas.idMalla " + 
			"			INNER JOIN  PlanificacionParalelo pp on mas.id=pp.idMallaAsignatura and pma.idPeriodoAcademico=pp.idPeriodoAcademico " + 
			"			INNER JOIN Asignatura as asi on mas.idAsignatura=asi.id " + 
			"			INNER JOIN OfertaAsignatura as oas on asi.id=oas.asignatura.id  " + 
			"			INNER JOIN Oferta as ofe on oas.oferta.id=ofe.id  " + 
			"			INNER JOIN Nivel as n on mas.idNivel=n.id " + 
			"			INNER JOIN Paralelo as par on par.orden<= isnull (pp.numParalelos,0)  " + 
			"			INNER JOIN MallaAsignaturaFusion as maf on mas.id=maf.idMallaAsignatura and par.id=maf.idParalelo and pma.idPeriodoAcademico=maf.idPeriodoAcademico " + 
			"			INNER JOIN DetalleMallaAsignaturaFusion as dmaf on maf.id=dmaf.idMallaAsignaturaFusion " + 
			"			INNER JOIN MallaAsignatura as mafu on dmaf.idMallaAsignatura=mafu.id " + 
			"			INNER JOIN Asignatura as asif on mafu.idAsignatura=asif.id" + 
			"			INNER JOIN OfertaAsignatura oasf  on asif.id=oasf.asignatura.id  " + 
			"			INNER JOIN Oferta as ofef on oasf.oferta.id=ofef.id  " + 
			"			INNER join Paralelo as parf on dmaf.idParalelo=parf.id " + 
			"			INNER JOIN Nivel as nf on mafu.idNivel=nf.id " + 
			"			where pma.idPeriodoAcademico=?1 and do.idOferta=?2 and pp.idPeriodoAcademico=?1 and " + 
			"			 isnull (pp.numParalelos,0)>0  " + 
			"			and n.id>=ma.idNivelMinAperturado and n.id<=ma.idNivelMaxAperturado " + 
			"			and pma.estado='A' and ma.estado in('A','P') and  do.estado='A' and mas.estado='A' and pp.estado='A' and asi.estado='A'" + 
			"			and oas.estado='A' and ofe.estado='A' and n.estado='A' and par.estado='A' and maf.estado='A' and dmaf.estado='A' ")
	List<CustObjMallaAsignaturaFusionDet> buscarMallaAsigaturaFusionDet(Integer idPeriodoAcademico, Integer idOferta);
	interface CustObjMallaAsignaturaFusionDet { 
		Integer getIdPeriodoAcademico(); 
		Integer getIdMallaAsignatura(); 
		Integer getIdOfertaAsignatura(); 
		String getOfertaAsignatura(); 
		Integer getIdParalelo(); 
		String getParalelo(); 
		String getAsignatura(); 
		String getAsignaturaCorta(); 
		String getNivel(); 
		Integer getIdMallaAsignaturaFusion(); 
		String getEstado();
		Integer getVersion(); 	
		Integer getIdDetalleMallaAsignaturaFusion(); 
		Integer getIdMallaAsignaturaF(); 
		String getAsignaturaFusion();
		Integer getIdParaleloF(); 
		String getParaleloF();
		Integer getIdOfertaAsignaturaF(); 
		String getOfertaAsignaturaF(); 
		String getNivelF(); 
		String getEstadoF();
		Integer getVersionF(); 
	}
	
	@Query(value="select  pma.idPeriodoAcademico as idPeriodoAcademico,  mas.id as idMallaAsignatura,ofe.id as idOfertaAsignatura, " + 
			"			ofe.descripcionCorta as ofertaAsignatura, par.id as idParalelo,	CONCAT(cast(n.orden as text),'/',par.descripcionCorta) as paralelo, " + 
			"			asi.descripcion as asignatura, asi.descripcionCorta as asignaturaCorta, n.orden as nivel  " + 
			"			FROM PeriodoMalla pma			" + 
			"			INNER JOIN Malla ma on pma.idMalla=ma.id " + 
			"			INNER JOIN  DepartamentoOferta do on ma.idDepartamentoOferta=do.id " + 
			"			INNER JOIN  MallaAsignatura  mas on ma.id=mas.idMalla " + 
			"			INNER JOIN  PlanificacionParalelo pp on mas.id=pp.idMallaAsignatura and pma.idPeriodoAcademico=pp.idPeriodoAcademico " + 
			"			INNER JOIN Asignatura as asi on mas.idAsignatura=asi.id " + 
			"			INNER JOIN OfertaAsignatura as oas on asi.id=oas.asignatura.id  " + 
			"			INNER JOIN Oferta as ofe on oas.oferta.id=ofe.id  " + 
			"			INNER JOIN Nivel as n on mas.idNivel=n.id " + 
			"			INNER JOIN Paralelo as par on par.orden<= isnull (pp.numParalelos,0)  " + 
			"			where pma.idPeriodoAcademico=?1 and do.idOferta=?2 and pp.idPeriodoAcademico=?1 and " + 
			"			 isnull (pp.numParalelos,0)>0  " + 
			"			and n.id>=ma.idNivelMinAperturado and n.id<=ma.idNivelMaxAperturado " + 
			"			and pma.estado='A' and ma.estado in ('A','P') and  do.estado='A' and mas.estado='A' and pp.estado='A' and asi.estado='A'" + 
			"			and oas.estado='A' and ofe.estado='A' and n.estado='A' and par.estado='A' "+
			"			group by  pma.idPeriodoAcademico ,  mas.id ,ofe.id ," + 
			"			ofe.descripcionCorta , par.id ,n.orden ,par.descripcionCorta," + 
			"			asi.descripcion , asi.descripcionCorta ,n.orden  , pp.numParalelos" 
			
			)
	List<CustObjMallaAsignaturaParalelos> buscarMallaAsigaturaParalelos(Integer idPeriodoAcademico, Integer idOferta);
	interface CustObjMallaAsignaturaParalelos  { 
		Integer getIdPeriodoAcademico(); 
		Integer getIdMallaAsignatura(); 
		Integer getIdOfertaAsignatura(); 
		String getOfertaAsignatura(); 
		Integer getIdParalelo(); 
		String getParalelo(); 
		String getAsignatura(); 
		String getAsignaturaCorta(); 
		String getNivel(); 
	
	}
	
	
	@Query(value="select  pma.idPeriodoAcademico as idPeriodoAcademico,  mas.id as idMallaAsignatura,ofe.id as idOfertaAsignatura, " + 
			"			ofe.descripcionCorta as ofertaAsignatura, par.id as idParalelo,	CONCAT(cast(n.orden as text),'/',par.descripcionCorta) as paralelo, " + 
			"			asi.descripcion as asignatura, asi.descripcionCorta as asignaturaCorta, n.orden as nivel  " + 
			"			FROM PeriodoMalla pma			" + 
			"			INNER JOIN Malla ma on pma.idMalla=ma.id " + 
			"			INNER JOIN  DepartamentoOferta do on ma.idDepartamentoOferta=do.id " + 
			"			INNER JOIN  MallaAsignatura  mas on ma.id=mas.idMalla " + 
			"			INNER JOIN  PlanificacionParalelo pp on mas.id=pp.idMallaAsignatura and pma.idPeriodoAcademico=pp.idPeriodoAcademico " + 
			"			INNER JOIN Asignatura as asi on mas.idAsignatura=asi.id " + 
			"			INNER JOIN OfertaAsignatura as oas on asi.id=oas.asignatura.id  " + 
			"			INNER JOIN Oferta as ofe on oas.oferta.id=ofe.id  " + 
			"			INNER JOIN Nivel as n on mas.idNivel=n.id " + 
			"			INNER JOIN Paralelo as par on par.orden<= isnull (pp.numParalelos,0)  " + 
			"			where pma.idPeriodoAcademico=?1 and do.idOferta=?2 and pp.idPeriodoAcademico=?1 and " + 
			"			 isnull (pp.numParalelos,0)>0  "+
			"			 and  par.id not in (select maf.idParalelo " + 
			"				FROM PlanificacionParalelo pp1 " + 
			"				INNER JOIN  MallaAsignaturaFusion as maf on pp1.idPeriodoAcademico=maf.idPeriodoAcademico " + 
			"						and pp.idMallaAsignatura=maf.idMallaAsignatura " + 
			"				WHERE pp.idPeriodoAcademico=?1 and maf.id=pp.idMallaAsignatura " +
			"				and maf.estado='A' and ma.estado='A' "+
			"			) " + 
			"			and par.id not in (select dmaf.idParalelo " + 
			"				FROM PlanificacionParalelo pp1 " + 
			"				INNER JOIN  MallaAsignaturaFusion as maf on pp1.idPeriodoAcademico=maf.idPeriodoAcademico " + 
			"						and pp.idMallaAsignatura=maf.idMallaAsignatura " + 
			"				INNER JOIN DetalleMallaAsignaturaFusion as dmaf on maf.id=dmaf.idMallaAsignaturaFusion " + 
		
			"				where pp.idPeriodoAcademico=?1  and maf.id=pp1.idMallaAsignatura " +  
			"				and maf.estado='A' and dmaf.estado='A' 						)" + 
			
			"			and n.id>=ma.idNivelMinAperturado and n.id<=ma.idNivelMaxAperturado " + 
			"			and pma.estado='A' and ma.estado='A' and  do.estado='A' and mas.estado='A' and pp.estado='A' and asi.estado='A'" + 
			"			and oas.estado='A' and ofe.estado='A' and n.estado='A' and par.estado='A' "+
			"			group by  pma.idPeriodoAcademico ,  mas.id ,ofe.id ," + 
			"			ofe.descripcionCorta , par.id ,n.orden ,par.descripcionCorta," + 
			"			asi.descripcion , asi.descripcionCorta ,n.orden  , pp.numParalelos" 
			
			)
	List<CustObjMallaAsigParalelosOtraCar> buscarMallaAsigParalelosOtroCar(Integer idPeriodoAcademico, Integer idOferta);
	interface CustObjMallaAsigParalelosOtraCar  { 
		Integer getIdPeriodoAcademico(); 
		Integer getIdMallaAsignatura(); 
		Integer getIdOfertaAsignatura(); 
		String getOfertaAsignatura(); 
		Integer getIdParalelo(); 
		String getParalelo(); 
		String getAsignatura(); 
		String getAsignaturaCorta(); 
		String getNivel(); 
	
	}
	
	
	@Query(value="select  pma.idPeriodoAcademico as idPeriodoAcademico,  mas.id as idMallaAsignatura,ofe.id as idOfertaAsignatura, " + 
			"			ofe.descripcionCorta as ofertaAsignatura, par.id as idParalelo,	CONCAT(cast(n.orden as text),'/',par.descripcionCorta) as paralelo, " + 
			"			asi.descripcion as asignatura, asi.descripcionCorta as asignaturaCorta, n.orden as nivel , " + 
			"			maf.id as idMallaAsignaturaFusion,  maf.estado as estado, maf.version as version, " + 
			"			dmaf.id as idDetalleMallaAsignaturaFusion, dmaf.idMallaAsignatura as idMallaAsignaturaF, asif.descripcion as asignaturaFusion, " + 
			"			dmaf.idParalelo as idParaleloF,case when nf.orden  is not  null then CONCAT(cast(nf.orden as text),'/',par.descripcionCorta) else '' end  as paraleloF, nf.orden as nivelF, "+
			"			ofef.id as idOfertaAsignaturaF, ofef.descripcionCorta as ofertaAsignaturaF, "+
			" 			dmaf.estado as estadoF , dmaf.version as versionF " +  
			"			FROM PeriodoMalla pma			" + 
			"			INNER JOIN Malla ma on pma.idMalla=ma.id " + 
			"			INNER JOIN  DepartamentoOferta do on ma.idDepartamentoOferta=do.id " + 
			"			INNER JOIN  MallaAsignatura  mas on ma.id=mas.idMalla " + 
			"			INNER JOIN  PlanificacionParalelo pp on mas.id=pp.idMallaAsignatura and pma.idPeriodoAcademico=pp.idPeriodoAcademico " + 
			"			INNER JOIN Asignatura as asi on mas.idAsignatura=asi.id " + 
			"			INNER JOIN OfertaAsignatura as oas on asi.id=oas.asignatura.id  " + 
			"			INNER JOIN Oferta as ofe on oas.oferta.id=ofe.id  " + 
			"			INNER JOIN Nivel as n on mas.idNivel=n.id " + 
			"			INNER JOIN Paralelo as par on par.orden<= isnull (pp.numParalelos,0)  " + 
			"			LEFT JOIN MallaAsignaturaFusion as maf on mas.id=maf.idMallaAsignatura and par.id=maf.idParalelo and pma.idPeriodoAcademico=maf.idPeriodoAcademico " + 
			"			LEFT JOIN DetalleMallaAsignaturaFusion as dmaf on maf.id=dmaf.idMallaAsignaturaFusion " + 
			"			LEFT JOIN MallaAsignatura as mafu on dmaf.idMallaAsignatura=mafu.id " + 
			"			LEFT JOIN Asignatura as asif on mafu.idAsignatura=asif.id" + 
			"			LEFT JOIN OfertaAsignatura oasf  on asif.id=oasf.asignatura.id  " + 
			"			LEFT JOIN Oferta as ofef on oasf.oferta.id=ofef.id  " + 
			"			LEFT join Paralelo as parf on dmaf.idParalelo=parf.id " + 
			"			LEFT JOIN Nivel as nf on mafu.idNivel=nf.id " + 
			"			where pma.idPeriodoAcademico=?1 and do.idOferta=?2 and pp.idPeriodoAcademico=?1 and " + 
			"			 isnull (pp.numParalelos,0)>0  " + 
			"			and n.id>=ma.idNivelMinAperturado and n.id<=ma.idNivelMaxAperturado " + 
			"			and pma.estado='A' and ma.estado='A' and  do.estado='A' and mas.estado='A' and pp.estado='A' and asi.estado='A'" + 
			"			and oas.estado='A' and ofe.estado='A' and n.estado='A' and par.estado='A' ")
	List<CustObjMallaAsignaturaFusionParalelo> buscarMallaAsigaturaParalelo(Integer idPeriodoAcademico, Integer idOferta);
	interface CustObjMallaAsignaturaFusionParalelo  { 
		Integer getIdPeriodoAcademico(); 
		Integer getIdMallaAsignatura(); 
		Integer getIdOfertaAsignatura(); 
		String getOfertaAsignatura(); 
		Integer getIdParalelo(); 
		String getParalelo(); 
		String getAsignatura(); 
		String getAsignaturaCorta(); 
		String getNivel(); 
		Integer getIdMallaAsignaturaFusion(); 
		String getEstado();
		Integer getVersion(); 
		Integer getIdDetalleMallaAsignaturaFusion(); 
		Integer getIdMallaAsignaturaF(); 
		String getAsignaturaFusion();
		Integer getIdParaleloF(); 
		String getParaleloF();
		Integer getIdOfertaAsignaturaF(); 
		String getOfertaAsignaturaF(); 
		String getNivelF(); 
		String getEstadoF();
		Integer getVersionF(); 
		
	}

}
