package ec.edu.upse.acad.model.pojo.seguridad;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="seg", name="roles_departamentos")
@NoArgsConstructor

public class RolDepartamento {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Getter @Setter private Integer id;
	
	@Getter @Setter private Integer rol_id;
	
	@Getter @Setter private Integer departamento_id;
	
	@Getter @Setter private String estado;
	
	@Version
	@Getter @Setter private Integer version;
	
	
	//RELACIONES
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="rol_id", nullable = false, insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Rol rol;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="departamento_id", nullable = false, insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Departamento departamento;
	
	@PrePersist
	void preInsert() {
	   if (this.estado == null)
	       this.estado = "AC";
	}
}
