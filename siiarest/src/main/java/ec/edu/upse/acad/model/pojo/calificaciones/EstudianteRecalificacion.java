package ec.edu.upse.acad.model.pojo.calificaciones;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="aca", name="estudiante_recalificacion")
@NoArgsConstructor
public class EstudianteRecalificacion {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_estudiante_recalificacion")
	@Getter @Setter private Integer id;
	
	@Column(name="id_estudiante_calificacion")
	@Getter @Setter private Integer id_estudiante_calificacion;
	
	@Getter @Setter private Integer recalificacion;
	
	@Column(name="numero_oficio")
	@Getter @Setter private Integer numeroOficio;
	
	@Getter @Setter private String observacion;
	
	@Getter @Setter private String estado;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;
	
	//bi-directional many-to-one association to Reglamento
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_estudiante_calificacion" , insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private EstudianteCalificacion estudianteCalificacion;
	
	@PrePersist
	void preInsert() {
	   if (this.estado == null)
	       this.estado = "A";
	}
}
