package ec.edu.upse.acad.ws;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.upse.acad.model.pojo.ReglamentoCompAprendizaje;
import ec.edu.upse.acad.model.pojo.ReglamentoCompOrganizacion;
import ec.edu.upse.acad.model.repository.ComponenteAprendizajeRepository;
import ec.edu.upse.acad.model.repository.ComponenteOrganizacionRepository;
import ec.edu.upse.acad.model.repository.MallaCompAprendizajeRepository;
import ec.edu.upse.acad.model.repository.MallaCompOrganizacionRepository;

@RestController
@RequestMapping("/api/configurarMalla")
@CrossOrigin
public class MallaGeneralController {
	/*
	 * servicios de malla_comp_aprendizaje servivios de malla_comp_organizacion
	 * servicios de componente_organizacion servicios de componente_aprendizaje
	 * servicios de malla_general servicios de tipo_componente_organizacion
	 */

	@Autowired
	private MallaCompAprendizajeRepository mallaCompAprendizajeRepository;
	@Autowired
	private MallaCompOrganizacionRepository mallaCompOrganizacionRepository;
	@Autowired
	private ComponenteAprendizajeRepository ComponenteAprendizajeRepository;
	@Autowired
	private ComponenteOrganizacionRepository ComponenteOrganizacionRepository;

	// ****************** SERVICIOS PARA MALLA COMPONENTE APRENDIZAJE************************//

	// Buscar todos las departamentos_ofertas, para listar en angular
	@RequestMapping(value = "/listarCompAprendizaje", method = RequestMethod.GET)
	public ResponseEntity<?> listarCompAprendizaje() {
		return ResponseEntity.ok(ComponenteAprendizajeRepository.findAll());
	}

	// ***********************FIN SERVICIOS PARA MALLA COMPONENTE APRENDIZAJE***********************

	// ****************** SERVICIOS PARA MALLA COMPONENTE ORGANIZACIN***********************//

	// Buscar todos las malla_comp_organizacion, para listar en angular
	@RequestMapping(value = "/listarCompOrganizacion", method = RequestMethod.GET)
	public ResponseEntity<?> listarCompOrganizacion() {
		return ResponseEntity.ok(ComponenteOrganizacionRepository.findAll());
	}

	// ***********************FIN SERVICIOS PARA MALLA COMPONENTE ORGANIZACIN***********************


	// Buscar todos las departamentos_ofertas, para listar en angular
	@RequestMapping(value = "/buscarMallaCompAprendizaje", method = RequestMethod.GET)
	public ResponseEntity<?> buscarMallaCompAprendizaje() {
		return ResponseEntity.ok(mallaCompAprendizajeRepository.findAll());
	}

	@RequestMapping(value = "/grabarMallaCompAprendizaje", method = RequestMethod.POST)
	public ResponseEntity<?> grabarMallaCompAprendizaje(@RequestBody ReglamentoCompAprendizaje mallaaprend) {
		ReglamentoCompAprendizaje _mallaaprend = new ReglamentoCompAprendizaje();
		if (_mallaaprend.getId() != null) {
			_mallaaprend = mallaCompAprendizajeRepository.findById(mallaaprend.getId()).get();
		}
		BeanUtils.copyProperties(mallaaprend, _mallaaprend);
		mallaCompAprendizajeRepository.save(_mallaaprend);
		return ResponseEntity.ok(_mallaaprend);
	}

	// ***********************FIN SERVICIOS PARA MALLA COMPONENTE APRENDIZAJE***********************

	// ****************** SERVICIOS PARA MALLA COMPONENTE ORGANIZACIN***********************//

	// Buscar todos las malla_comp_organizacion, para listar en angular
	@RequestMapping(value = "/buscarMallaCompOrganizacion", method = RequestMethod.GET)
	public ResponseEntity<?> buscarMallaCompOrganizacion() {
		return ResponseEntity.ok(mallaCompOrganizacionRepository.findAll());
	}

	@RequestMapping(value = "/grabarMallaCompOrganizacion", method = RequestMethod.POST)
	public ResponseEntity<?> grabarMallaCompOrganizacion(@RequestBody ReglamentoCompOrganizacion mallacomporganiza) {
		ReglamentoCompOrganizacion _mallaorganiza = new ReglamentoCompOrganizacion();
		if (_mallaorganiza.getId() != null) {
			_mallaorganiza = mallaCompOrganizacionRepository.findById(mallacomporganiza.getId()).get();
		}
		BeanUtils.copyProperties(mallacomporganiza, _mallaorganiza);
		mallaCompOrganizacionRepository.save(_mallaorganiza);
		return ResponseEntity.ok(_mallaorganiza);
	}
	// ***********************FIN SERVICIOS PARA MALLA COMPONENTE ORGANIZACIN***********************

}
