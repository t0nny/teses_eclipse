package ec.edu.upse.acad.model.repository.calificaciones;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.calificaciones.ActividadesEvaluativa;

@Repository
public interface ActividadesEvaluativaRepository extends JpaRepository<ActividadesEvaluativa, Integer> {

}
