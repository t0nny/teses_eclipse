package ec.edu.upse.acad.ws;

import java.io.IOException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.upse.acad.model.pojo.AsignaturaAprendizaje;
import ec.edu.upse.acad.model.pojo.distributivo.DistributivoDocente;
import ec.edu.upse.acad.model.pojo.distributivo.DistributivoOferta;
import ec.edu.upse.acad.model.pojo.distributivo.DistributivoOfertaVersion;
import ec.edu.upse.acad.model.repository.PlanificacionParaleloRepository;
import ec.edu.upse.acad.model.repository.distributivo.ActividadDetalleRepository;
import ec.edu.upse.acad.model.repository.distributivo.ActividadPersonalRepository;
import ec.edu.upse.acad.model.repository.distributivo.DistributivoDedicacionRepository;
import ec.edu.upse.acad.model.repository.distributivo.DistributivoDocenteRepository;
import ec.edu.upse.acad.model.repository.distributivo.DistributivoGeneralRepository;
import ec.edu.upse.acad.model.repository.distributivo.DistributivoOfertaRepository;
import ec.edu.upse.acad.model.repository.distributivo.DistributivoOfertaVersionRepository;
import ec.edu.upse.acad.model.repository.distributivo.DocenteAsignaturaAprendRepository;

import ec.edu.upse.acad.model.service.DistributivoDocenteService;
import ec.edu.upse.acad.model.service.ReportService;
import ec.edu.upse.acad.model.service.SecurityService;
import ec.edu.upse.acad.model.service.UpdateForIdService;

@RestController
@RequestMapping("/api/configuracionDistributivo")
@CrossOrigin

public class ConfiguracionDistributivoController {


	@Autowired private SecurityService securityService;
	@Autowired private DistributivoOfertaRepository distributivoOfertaRepository;
	@Autowired private DistributivoOfertaVersionRepository distributivoOfertaVersionRepository;
	@Autowired private DistributivoDocenteRepository DistributivoDocenteRepository;
	@Autowired private DistributivoDedicacionRepository distributivoDedicacionRepository;
	@Autowired private DistributivoDocenteService distributivoDocenteService;
	@Autowired private PlanificacionParaleloRepository planificacionParaleloRepository;
	@Autowired private DistributivoGeneralRepository  distributivoGeneralRepository;
	@Autowired private DocenteAsignaturaAprendRepository  docenteAsignaturaAprendRepository;
	@Autowired private ActividadDetalleRepository actividadDetalleRepository;
	@Autowired private UpdateForIdService eliminarCompletoDocenteDistributivo;
	@Autowired private ReportService reportService;
	@Autowired private ActividadPersonalRepository actividadPersonalRepository;


	//****************** SERVICIOS PARA Distributivo Oferta************************//
/***retorna 
 * retorna todos los distributivo oferta version
 * @param Authorization
 * @return
 */
	@RequestMapping(value="/buscarDistributivoOfertas", method=RequestMethod.GET)
	public ResponseEntity<?> buscarOfertas(@RequestHeader(value="Authorization") String Authorization) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(distributivoOfertaRepository.findAll());
	}
/***
 * servicio que actualiza y crea un nuevo distributivo oferta version filtrando por el id distributivo oferta
 * @param authorization
 * @param id
 * @return
 */

	@RequestMapping(value="/buscarDistributivoOferta/{id}", method=RequestMethod.GET)
	public ResponseEntity<?> buscar(@RequestHeader(value="Authorization") String authorization, 
			@PathVariable("id") Integer id) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}

		DistributivoOferta distributivosofertassig;
		if (distributivoOfertaRepository.findById(id).isPresent()) {
			distributivosofertassig = distributivoOfertaRepository.findById(id).get();
			return ResponseEntity.ok(distributivosofertassig);
		}else {
			return ResponseEntity.notFound().build();
		}
	}
	

	@RequestMapping(value="/buscarDistributivoOfertaVersion/{id}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarDistributivoOfertaVersion(@RequestHeader(value="Authorization") String authorization, 
			@PathVariable("id") Integer id) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}

		DistributivoOfertaVersion distributivosofertasversion;
		if (distributivoOfertaVersionRepository.findById(id).isPresent()) {
			distributivosofertasversion = distributivoOfertaVersionRepository.findById(id).get();
			
			for (int i = 0; i < distributivosofertasversion.getDistributivoDocentes().size(); i++) {
				//distributivosofertasversion.getDistributivoDocentes().get(i).setDocenteAsignaturaAprends(null);
				distributivosofertasversion.getDistributivoDocentes().get(i).setDistributivoDedicaciones(null);
				distributivosofertasversion.getDistributivoDocentes().get(i).setDocenteActividades(null);;
			}
			return ResponseEntity.ok(distributivosofertasversion);
		}else {
			return ResponseEntity.notFound().build();
		}
	}
	
	
	@RequestMapping(value="/validarDistDocenteVacio/{id}", method=RequestMethod.GET)
	public ResponseEntity<?> validarDistDocenteVacio(@RequestHeader(value="Authorization") String authorization, 
			@PathVariable("id") Integer id) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok( distributivoOfertaVersionRepository.buscarDistributivoDocVacio(id));
	}
	
	@RequestMapping(value="/validarDistDocenteMallaAsig/{idPeriodoAcad}/{idDist}", method=RequestMethod.GET)
	public ResponseEntity<?> validarDistDocenteMallaAsig(@RequestHeader(value="Authorization") String authorization, 
			@PathVariable("idPeriodoAcad") Integer idPeriodoAcad,@PathVariable("idDist") Integer idDist) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok( distributivoOfertaVersionRepository.buscarMallaAsigFaltanAsignar(idPeriodoAcad,idDist));
	}
	
	
	
	
	/*buscar docente*/
	///{fechaDesde}/{fechaHasta}
	@RequestMapping(value="/buscarDocenteAsigAprendOtroDis/{idDocente}/{idDistributivoDoc}/{idDistributivoGeneralVer}/{fechaDesde}/{fechaHasta}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarDocenteAsigAprendOtroDis(@RequestHeader(value="Authorization") String authorization, 
			@PathVariable("idDocente") Integer idDocente,
			@PathVariable("idDistributivoDoc") Integer idDistributivoDoc,
			@PathVariable("idDistributivoGeneralVer") Integer idDistributivoGeneralVer,
			@PathVariable("fechaDesde") java.sql.Date fechaDesde,
			@PathVariable("fechaHasta") java.sql.Date fechaHasta) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		
		if(fechaDesde==fechaHasta) {
		}

		//Format formatter = new SimpleDateFormat("yyyy-MM-dd");
		//String s = formatter.format(fechaDesde);
		//, fechaDesde, fechaHasta
		return ResponseEntity.ok(docenteAsignaturaAprendRepository.listarDocenteAsigAprendOtroDistr(idDocente,idDistributivoDoc,idDistributivoGeneralVer,fechaDesde,fechaHasta));
	}
	@RequestMapping(value="/buscarDocenteDedicacionOtroDis/{idDocente}/{idDistributivoDoc}/{idDistributivoGeneralVer}/{fechaDesde}/{fechaHasta}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarDocenteDedicacionOtroDis(@RequestHeader(value="Authorization") String authorization, 
			@PathVariable("idDocente") Integer idDocente,
			@PathVariable("idDistributivoDoc") Integer idDistributivoDoc,
			@PathVariable("idDistributivoGeneralVer") Integer idDistributivoGeneralVer,
			@PathVariable("fechaDesde") java.sql.Date fechaDesde,
			@PathVariable("fechaHasta") java.sql.Date fechaHasta) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(distributivoDedicacionRepository.DedicacionOtroDistr(idDocente, idDistributivoDoc, idDistributivoGeneralVer, fechaDesde, fechaHasta));
	}
	
/*	@RequestMapping(value="/buscarDocenteDedicacionOtroDis/{idDocente}/{idDistributivoDoc}/{fechaDesde}/{fechaHasta}/{IdDistributivoGeneralVer}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarDocenteDedicacionOtroDis(@RequestHeader(value="Authorization") String authorization, 
			@PathVariable("idDocente") Integer idDocente,
			@PathVariable("idDistributivoDoc") Integer idDistributivoDoc,
			@PathVariable("fechaDesde") Date fechaDesde,
			@PathVariable("fechaHasta") Date fechaHasta,
			@PathVariable("IdDistributivoGeneralVer") Integer IdDistributivoGeneralVer) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(docenteAsignaturaAprendRepository.listarDedicacionOtroDistr(idDocente,idDistributivoDoc, fechaDesde, fechaHasta,IdDistributivoGeneralVer));
	}*/
	@RequestMapping(value="/buscarCantAsigParalelo/{idPeriodo}/{idDepartamentoOferta}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarCantAsigParalelo(@RequestHeader(value="Authorization") String authorization, 
			@PathVariable("idPeriodo") Integer idPeriodo,
			@PathVariable("idDepartamentoOferta") Integer idDepartamentoOferta) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(planificacionParaleloRepository.buscarCantParalelo(idPeriodo, idDepartamentoOferta));
	}
	
	@RequestMapping(value="/buscarDocenteActividadOtroDis/{idDocente}/{idDistributivoDoc}/{fechaDesde}/{fechaHasta}/{IdDistributivoGeneralVer}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarDocenteActividadOtroDis(@RequestHeader(value="Authorization") String authorization, 
			@PathVariable("idDocente") Integer idDocente,
			@PathVariable("idDistributivoDoc") Integer idDistributivoDoc,
			@PathVariable("fechaDesde") Date fechaDesde,
			@PathVariable("fechaHasta") Date fechaHasta,
			@PathVariable("IdDistributivoGeneralVer") Integer IdDistributivoGeneralVer) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(docenteAsignaturaAprendRepository.listarActividadOtroDistr(idDocente,idDistributivoDoc, fechaDesde, fechaHasta,IdDistributivoGeneralVer));
	}
	
/***
 * 
 * Enlista todos los distributivo oferta version 
 * @param authorization
 * @return
 */
	@RequestMapping(value="/listarDistributivosOfertas", method=RequestMethod.GET)
	public ResponseEntity<?> listarDistributivosOfertas(@RequestHeader(value="Authorization") String authorization) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(distributivoOfertaRepository.listarDistributivoOfertas());
	}
/*********
 * Enlista todos los distributivo oferta version de un periodo academico
 * @param authorization
 * @param idPeriodo
 * @return
 */
// o anteriormente llamado listarDistributivosOfertasIdPeriodo
	@RequestMapping(value="/listarDistributivosOfertasId/{idDistributivoGeneralVer}", method=RequestMethod.GET)
	public ResponseEntity<?> listarDistributivosOfertasId(@RequestHeader(value="Authorization") String authorization, 
			@PathVariable("idDistributivoGeneralVer") Integer idDistributivoGeneralVer) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(distributivoOfertaRepository.listarDistributivoOfertasIdCarrera(idDistributivoGeneralVer));
	}
	
	/****
	 * recupera la descripcion del distributivo oferta
	 * @param authorization
	 * @param idDistributivoOferta
	 * @return
	 */
	@RequestMapping(value="/recuperarNombreOfertaDistributivo/{idDistributivoOferta}", method=RequestMethod.GET)
	public ResponseEntity<?> recuperarNombreOfertaDistributivo(@RequestHeader(value="Authorization") String authorization, 
			@PathVariable("idDistributivoOferta") Integer idDistributivoOferta) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(distributivoOfertaRepository.listarDistributivoOfertasId(idDistributivoOferta));
	}
	
	@RequestMapping(value="/listarCantDocenteDed/{idDistributivoOfertaVersion}", method=RequestMethod.GET)
	public ResponseEntity<?> listarCantDocenteDed(@RequestHeader(value="Authorization") String authorization, 
			@PathVariable("idDistributivoOfertaVersion") Integer idDistributivoOfertaVersion) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(distributivoOfertaRepository.listarCantDocenteDed(idDistributivoOfertaVersion));
	}
	
	
	
	
	/****
	 * Recuperar la cabecera Distributivo Oferta para garbar los docentes y su dedicacion
	 * @param authorization
	 * @param idDistributivoOferta
	 * @return
	 */
	@RequestMapping(value="/recuperarCabezeraDistributivoOferta/{idDistributivoOferta}", method=RequestMethod.GET)
	public ResponseEntity<?> recuperarCabezeraDistributivoOferta(@RequestHeader(value="Authorization") String authorization, 
			@PathVariable("idDistributivoOferta") Integer idDistributivoOferta) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(distributivoOfertaRepository.recuperarCabezeraDistributivoOferta(idDistributivoOferta));
	}
	


	//***********************FIN SERVICIOS PARA DISTRIBUTIVO OFERTA ***********************

	//****************** SERVICIOS PARA Distributivo Docente************************//
	/*** servicio
	 * servicio para grabar DistributivoDocente, DocenteActividad, DistributivoDedicacion, DocenteAsignaturaAprend
	 * @param Authorization
	 * @param distributivoDocente
	 * @return
	 */
	@RequestMapping(value="/grabarDistributivo", method=RequestMethod.POST)
	public ResponseEntity<?> grabarDistributivo(@RequestHeader(value="Authorization") String Authorization, //definimos que tenga autorizacion
			@RequestBody DistributivoDocente distributivoDocente) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		distributivoDocenteService.grabarDistributivoDocenteDedicacion(distributivoDocente);
		return ResponseEntity.ok().build();
	}
	@RequestMapping(value="/grabarAsignaturaAprenMultidocente", method=RequestMethod.POST)
	public ResponseEntity<?> grabarDistributivo(@RequestHeader(value="Authorization") String Authorization, //definimos que tenga autorizacion
			@RequestBody List<AsignaturaAprendizaje> asignaturaAprendizaje) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		distributivoDocenteService.grabaAsignaturaAprendizajeMultService(asignaturaAprendizaje);
		return ResponseEntity.ok().build();
	}
	
	
	
	/***
	 * servicio que actualiza o crea un nuevo DISTRIBUITIVO DOCENTE	con ID FINDALL
	 * @param authorization
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/buscarDistributivoDocente/{id}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarDistributivoDocente(@RequestHeader(value="Authorization") String authorization, 
			@PathVariable("id") Integer id) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}

		DistributivoDocente distributivosDocentessig;
		if (DistributivoDocenteRepository.findById(id).isPresent()) {
			distributivosDocentessig = DistributivoDocenteRepository.findById(id).get();
			distributivosDocentessig.setDocenteAsignaturaAprends(null);
			
			return ResponseEntity.ok(distributivosDocentessig);
		}else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@RequestMapping(value="/listarDistributivoOfertaVersion/{idDistributivoOfertaVersion}", method=RequestMethod.GET)
	public ResponseEntity<?> listarDistributivoOfertaVersion(@RequestHeader(value="Authorization") String authorization,
		@PathVariable("idDistributivoOfertaVersion") Integer idDistributivoOfertaVersion) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		
		return ResponseEntity.ok(distributivoGeneralRepository.listaDistributivoOfertaVersion(idDistributivoOfertaVersion));
	}
		
	/**
	 * Lista todos los docentes  con detalle de horas y dedicacion de un distributivo oferta version
	 * @param authorization
	 * @param idDistributivoOfertaVersion
	 * @return
	 */
	
	@RequestMapping(value="/getSPDetalleDistributivoOfertaDocenteAsignatura/{idDistributivoOfertaVersion}/{idDistributivoDocente}/{idDocente}", method=RequestMethod.GET)
	public ResponseEntity<?> getSPDetalleDistributivoOfertaDocenteAsignatura(@RequestHeader(value="Authorization") String authorization,
			@PathVariable("idDistributivoOfertaVersion") Integer idDistributivoOfertaVersion,
			@PathVariable("idDistributivoDocente") Integer idDistributivoDocente,
			@PathVariable("idDocente") Integer idDocente) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		
		return ResponseEntity.ok(distributivoDocenteService.refreshSPDetalleDistributivoOfertaDocenteAsig(idDistributivoOfertaVersion, idDistributivoDocente, idDocente));
	}
	
	@RequestMapping(value="/getSPDedicacionDistributivoOfertaDocente/{idDistributivoOfertaVersion}/{idDistributivoDocente}/{idDocente}", method=RequestMethod.GET)
	public ResponseEntity<?> getSPDedicacionDistributivoOfertaDocenteAsignatura(@RequestHeader(value="Authorization") String authorization,
			@PathVariable("idDistributivoOfertaVersion") Integer idDistributivoOfertaVersion,
			@PathVariable("idDistributivoDocente") Integer idDistributivoDocente,
			@PathVariable("idDocente") Integer idDocente) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		
		return ResponseEntity.ok(distributivoDocenteService.refreshSPDedicacionDistributivoOfertaDocente(idDistributivoOfertaVersion, idDistributivoDocente, idDocente));
	}
	
	
	
	@RequestMapping(value="/getSPCabeceraDistributivoOfertaDocente/{idDistributivoOfertaVersion}", method=RequestMethod.GET)
	public ResponseEntity<?> getSPCabeceraDistributivoOfertaDocente(@RequestHeader(value="Authorization") String authorization,
			@PathVariable("idDistributivoOfertaVersion") Integer idDistributivoOfertaVersion) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		
		return ResponseEntity.ok(distributivoDocenteService.refreshSPGenerarCabeceraDistributivoOfertaDocente(idDistributivoOfertaVersion));
	}
	
	
	@RequestMapping(value="/getSPDetalleDistributivoOfertaDocente/{idDistributivoOfertaVersion}", method=RequestMethod.GET)
	public ResponseEntity<?> getSPDetalleDistributivoOfertaDocente(@RequestHeader(value="Authorization") String authorization,
			@PathVariable("idDistributivoOfertaVersion") Integer idDistributivoOfertaVersion) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		
		return ResponseEntity.ok(distributivoDocenteService.refreshSPGenerarDetalleDistributivoOfertaDocente(idDistributivoOfertaVersion));
	}
	
	@RequestMapping(value="/listarDocentesDedicacion/{idPeriodoAcademico}/{idDistributivoOfertaVersion}", method=RequestMethod.GET)
	public ResponseEntity<?> listarDocentesDedicacion(@RequestHeader(value="Authorization") String authorization,
			@PathVariable("idPeriodoAcademico") Integer idPeriodoAcademico,@PathVariable("idDistributivoOfertaVersion") Integer idDistributivoOfertaVersion) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		
		return ResponseEntity.ok(distributivoDocenteService.refreshgetSpDistributivoDocente(idPeriodoAcademico,idDistributivoOfertaVersion));
	}

	
	/****
	 * recupera de un distributivo oferta version un docente con los datos del detalle  horas y dedicacion
	 * filtrando por el id del  docente
	 * @param authorization
	 * @param idDocente
	 * @return
	 */

	@RequestMapping(value="/recuperarDocenteDedicacion/{idDocente}", method=RequestMethod.GET)
	public ResponseEntity<?> crearDistributivosCarreras(@RequestHeader(value="Authorization") String authorization, 
			@PathVariable("idDocente") Integer idDocente) 
	{
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(DistributivoDocenteRepository.recuperarDocenteDedicacion(idDocente));
	}
	
	/***
	 * recupera  detalle del distributivo obteniendo horas y dedicacion de los docente de un distributivo de una oferta version
	 * @param authorization
	 * @return
	 */
	@RequestMapping(value="/listarDetalleDocentesDistributivo/{idDistributivoOfertaVersion}", method=RequestMethod.GET)
	public ResponseEntity<?> listarDetalleDocentesDistributivo(@RequestHeader(value="Authorization") String authorization,
			@PathVariable("idDistributivoOfertaVersion") Integer idDistributivoOfertaVersion) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(DistributivoDocenteRepository.listarDetalleDocentesDistributivo(idDistributivoOfertaVersion));
	}
	

	
	/***
	 * recupera detalle del distributivo de un docente específico
	 * @param authorization
	 * @param idPeriodoAcademico
	 * @param idDepartamento
	 * @param idOferta
	 * @param idDistributivoOfertaVersion
	 * @param idDocente
	 * @return
	 */
	// recupera una lista Detalle Docentes Distributivo Oferta ID
	@RequestMapping(value="/listarDetalleDocentesDistributivoOfertaID/{idPeriodoAcademico}/{idDepartamento}/{idOferta}/{idDistributivoOfertaVersion}/{idDocente}", method=RequestMethod.GET)
	public ResponseEntity<?> listarDetalleDocentesDistributivoOfertaID(@RequestHeader(value="Authorization") String authorization, 
			@PathVariable("idPeriodoAcademico") Integer idPeriodoAcademico ,@PathVariable("idDepartamento") Integer idDepartamento ,
			@PathVariable("idOferta") Integer idOferta ,@PathVariable("idDistributivoOfertaVersion") Integer idDistributivoOfertaVersion,
			@PathVariable("idDocente") Integer idDocente) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(distributivoDocenteService.refreshgetFnDocenteAsignaturaOfertaVersion(idPeriodoAcademico, idDepartamento, idOferta, idDistributivoOfertaVersion, idDocente));
	}
	
	
	/***
	 * servicio para borrar una docente de una carrera y con una dedicacion dada
	 * @param Authorization
	 * @param idDistributivoOfertaVersion
	 * @param idDocente
	 * @return
	 */
	@RequestMapping(value="/borrarDocenteDeDistributivo/{idDistributivoOfertaVersion}/{idDocente}", method=RequestMethod.DELETE)
	public ResponseEntity<?> borrarDocenteDeDistributivo(@RequestHeader(value="Authorization") String Authorization, 
			@PathVariable("idDistributivoOfertaVersion") Integer idDistributivoOfertaVersion , @PathVariable("idDocente") Integer idDocente) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		eliminarCompletoDocenteDistributivo.borrarDocenteDeDistributivo(idDistributivoOfertaVersion,idDocente);
		return ResponseEntity.ok().build();
	}
	
	@RequestMapping(value="/eliminacionLogicaDistDoc/{idDistributivoDocente}", method=RequestMethod.DELETE)
	public ResponseEntity<?> borrarDocenteDeDistributivo(@RequestHeader(value="Authorization") String Authorization, 
			@PathVariable("idDistributivoDocente") Integer idDistributivoDocente ) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		eliminarCompletoDocenteDistributivo.eliminacionLogicaDistDoc(idDistributivoDocente);
		return ResponseEntity.ok().build();
	}
	
	
	/***
	 * servicio para borrar una asignatura ya asignada a un docente en un distributivo
	 * @param Authorization
	 * @param idAsignaturaVersion
	 * @param idDistributivoDocente
	 * @return
	 */
	@RequestMapping(value="/borrarDedicacionDocente/{idAsignaturaVersion}/{idDistributivoDocente}", method=RequestMethod.DELETE)
	public ResponseEntity<?> borrarDedicacionDocente(@RequestHeader(value="Authorization") String Authorization, 
			@PathVariable("idAsignaturaVersion") Integer idAsignaturaVersion,@PathVariable("idDistributivoDocente") Integer idDistributivoDocente) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		eliminarCompletoDocenteDistributivo.borrarAsignaturaDocenteDistributivo(idAsignaturaVersion, idDistributivoDocente);
		return ResponseEntity.ok().build();
	}
	
	
	/***
	 * servicio para reporte Distributivo
	 * @param authorization
	 * @param filterIdDocente
	 * @param request
	 * @return
	 * @throws Exception
	 */

	@RequestMapping(value="/getReportDistributivoPorDocente/{filterIdDocente}", method=RequestMethod.GET)
    public ResponseEntity<?> getReportMallaPorCarrera(@RequestHeader(value="Authorization") String authorization,				 
    		@PathVariable("filterIdDocente") Integer filterIdDocente,  
    		
    								   HttpServletRequest request) throws Exception {
		
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		Map<String, Object> parametros = new HashMap<String, Object>();
	     parametros.put("id_docente", filterIdDocente);
	  	  
	   
        // Carga el archivo
        Resource resource = reportService.generaReporteParametros("rptDistributivoPorDocente.jasper",parametros);

        // Determina el contenido
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        // Si no se determina el tipo, asume uno por defecto.
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }
	
	
	//***********************FIN SERVICIOS PARA DISTRIBUTIVO Docente ***********************//

	//****************** SERVICIOS PARA DistributivoDedicacion************************//
		/**
		 * retorna la lista de la jornada academica
		 * @param Authorization
		 * @return
		 */
		@RequestMapping(value="/cargarJornadas", method=RequestMethod.GET)
		public ResponseEntity<?> cargarJornadas(@RequestHeader(value="Authorization") String Authorization) {
			if (!securityService.isTokenValido(Authorization)) {
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
			}
			return ResponseEntity.ok(distributivoDedicacionRepository.cargarJornadas());
		}
		
		/***
		 * retorna la lista de las dedicaciones de un docente
		 * @param Authorization
		 * @return
		 */
		@RequestMapping(value="/cargarDedicaciones", method=RequestMethod.GET)
		public ResponseEntity<?> cargarDedicaciones(@RequestHeader(value="Authorization") String Authorization) {
			if (!securityService.isTokenValido(Authorization)) {
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
			}
			return ResponseEntity.ok(distributivoDedicacionRepository.cargarDedicaciones());
		}

		
		
		/****
		 * retorna la lista de las facultades
		 * @param Authorization
		 * @return
		 */
		@RequestMapping(value="/cargarFacultad", method=RequestMethod.GET)
		public ResponseEntity<?> cargarFacultade(@RequestHeader(value="Authorization") String Authorization//, 
				//@PathVariable("idDistributivoGeneralVer") Integer idDistributivoGeneralVer
				) {
			if (!securityService.isTokenValido(Authorization)) {
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
			}
			return ResponseEntity.ok(distributivoDedicacionRepository.cargarFacultad());
		}
		@RequestMapping(value="/cargarFacultad/{idDistributivoGeneralVer}/{usuario}", method=RequestMethod.GET)
		public ResponseEntity<?> cargarFacultad(@RequestHeader(value="Authorization") String Authorization, 
				@PathVariable("idDistributivoGeneralVer") Integer idDistributivoGeneralVer, @PathVariable("usuario") String usuario) {
			if (!securityService.isTokenValido(Authorization)) {
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
			}
				//System.err.println("usuario 1 " +usuario);
				return ResponseEntity.ok(distributivoDedicacionRepository.cargarFacultad(idDistributivoGeneralVer, usuario));
		}

		@RequestMapping(value="/cargarFacultades/{idDistributivoGeneralVer}", method=RequestMethod.GET)
		public ResponseEntity<?> cargarFacultades(@RequestHeader(value="Authorization") String Authorization, 
				@PathVariable("idDistributivoGeneralVer") Integer idDistributivoGeneralVer) {
			if (!securityService.isTokenValido(Authorization)) {
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
			}
		
				return ResponseEntity.ok(distributivoDedicacionRepository.cargarFacultades(idDistributivoGeneralVer));
			
		
			
		}
		
		/***
		 * retorna la lista de las carreras filtrando por la facultad y no incluyendo la carrerra de busqueda
		 * @param authorization
		 * @param idFacultad
		 * @param idOferta
		 * @return
		 */
		@RequestMapping(value="/cargarCarreras/{idFacultad}/{idDistributivoGeneralVer}", method=RequestMethod.GET)
		public ResponseEntity<?> cargarCarreras(@RequestHeader(value="Authorization") String authorization, 
				@PathVariable("idFacultad") Integer idFacultad,@PathVariable("idDistributivoGeneralVer") Integer idDistributivoGeneralVer) {
			if (!securityService.isTokenValido(authorization)) {
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
			}
			return ResponseEntity.ok(distributivoDedicacionRepository.cargarCarreras(idFacultad, idDistributivoGeneralVer));
		}

		@RequestMapping(value="/cargarDistributivoGen/{idPeriodo}", method=RequestMethod.GET)
		public ResponseEntity<?> cargarDistributivoGen(@RequestHeader(value="Authorization") String authorization, 
				@PathVariable("idPeriodo") Integer idPeriodo) {
			if (!securityService.isTokenValido(authorization)) {
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
			}
			return ResponseEntity.ok(distributivoDedicacionRepository.cargarDistributivoGeneral(idPeriodo));
		}

		
		/****
		 * retorna la lista de los docentes sin asignar a un distributivo en un periodo academico específico
		 * @param authorization
		 * @param idPeriodoAcademico
		 * @param idDistributivoOfertaVersion
		 * @return
		 */
		
		@RequestMapping(value = "/generarDocentesSinAsignar/{idPeriodoAcademico}/{idDistributivoOfertaVersion}", method = RequestMethod.GET,
				headers="Accept=application/json")
		public ResponseEntity<?> generarDocentesSinAsignar(@RequestHeader(value = "Authorization") String authorization,
				@PathVariable ("idPeriodoAcademico") Integer idPeriodoAcademico,
				@PathVariable ("idDistributivoOfertaVersion") Integer idDistributivoOfertaVersion){
			if (!securityService.isTokenValido(authorization)) {
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
			}
			return ResponseEntity.ok(distributivoDocenteService.refreshgetFnDocenteNoAsignadoDistributivo(idPeriodoAcademico, idDistributivoOfertaVersion));
		}
		
		/***
		 * retorna la lista de las asignaturas
		 * @param authorizationF
		 * @param idOferta
		 * @return
		 */
		@RequestMapping(value="/cargarAsignaturas/{idPeriodoAcademico}/{idOferta}", method=RequestMethod.GET)
		public ResponseEntity<?> cargarAsignaturas(@RequestHeader(value="Authorization") String authorization, 
				@PathVariable("idPeriodoAcademico") Integer idPeriodoAcademico,
				@PathVariable("idOferta") Integer idOferta) {
			if (!securityService.isTokenValido(authorization)) {
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
			}
			return ResponseEntity.ok(distributivoDedicacionRepository.cargarAsignaturasPorCarrera(idPeriodoAcademico,idOferta));
		}
		
		/****
		 * carga el combo de los docentes que no pertenecen aun distributivo
		 * @param Authorization
		 * @param idDistributivoOfertaVersion
		 * @return
		 */

		@RequestMapping(value="/cargarDocente/{idDistributivoOfertaVersion}/{idDocente}/{idPeriodo}", method=RequestMethod.GET)
		public ResponseEntity<?> cargarDocentes(@RequestHeader(value="Authorization") String Authorization,
				@PathVariable("idDistributivoOfertaVersion") Integer idDistributivoOfertaVersion,
				@PathVariable("idDocente") Integer idDocente,
				@PathVariable("idPeriodo") Integer idPeriodo) {
			if (!securityService.isTokenValido(Authorization)) {
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
			}
			return ResponseEntity.ok(distributivoDedicacionRepository.cargarDocentes(idDistributivoOfertaVersion, idDocente, idPeriodo));
		}

		/***
		 * retorna la lista de docente
		 * @param Authorization
		 * @return
		 */
		@RequestMapping(value="/cargarTodosDocentes/{idDistributivoOfertaVersion}/{idPeriodo}", method=RequestMethod.GET)
		public ResponseEntity<?> cargarTodosDocentes(@RequestHeader(value="Authorization") String Authorization,
				@PathVariable("idDistributivoOfertaVersion") Integer idDistributivoOfertaVersion,
				@PathVariable("idPeriodo") Integer idPeriodo) {
			if (!securityService.isTokenValido(Authorization)) {
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
			}

			return ResponseEntity.ok(distributivoDedicacionRepository.cargarTodosDocentes(idDistributivoOfertaVersion, idPeriodo));
		}
		
		
	
		

		
		/***
		 * recupera los componentes de una asignatura de la tabla asignatura_aprendizaje
		 * @param authorization
		 * @param idAsignatura
		 * @return
		 */
		@RequestMapping(value="/recuperarComponentesAsignaturaId/{idAsignatura}", method=RequestMethod.GET)
		public ResponseEntity<?> recuperarComponentesAsignaturaId(@RequestHeader(value="Authorization") String authorization, 
				@PathVariable("idAsignatura") Integer idAsignatura) {
			if (!securityService.isTokenValido(authorization)) {
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
			}
			return ResponseEntity.ok(distributivoDedicacionRepository.recuperarComponentesAsignaturaId(idAsignatura));
		}
		
		@RequestMapping(value="/DocenteDedicacion/{idDocente}/{fechaDesde}/{fechaHasta}", method=RequestMethod.GET)
		public ResponseEntity<?> DocenteDedicacion(@RequestHeader(value="Authorization") String authorization, 
				@PathVariable("idDocente") Integer idDocente, @PathVariable("fechaDesde") Date fechaDesde , @PathVariable("fechaHasta") Date fechaHasta) {
			if (!securityService.isTokenValido(authorization)) {
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
			}
		
			return ResponseEntity.ok(distributivoDedicacionRepository.cargarDedicacionDocente(idDocente,fechaDesde,fechaHasta));
		}
		
		/***
		 * recupera los datos restantes de la lista de docenteAsignaturaAprend
		 * @param authorization
		 * @param idAsignaturaAprendizaje
		 * @param idDocente
		 * @return
		 */
//		@RequestMapping(value="/recuperarIdDocenteAsignaturaAprend/{idAsignaturaAprendizaje}/{idDocente}", method=RequestMethod.GET)
//		public ResponseEntity<?> recuperarIdDocenteAsignaturaAprend(@RequestHeader(value="Authorization") String authorization, 
//				@PathVariable("idAsignaturaAprendizaje") Integer idAsignaturaAprendizaje,
//				@PathVariable("idDocente") Integer idDocente) {
//			if (!securityService.isTokenValido(authorization)) {
//				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
//			}
//			return ResponseEntity.ok(distributivoDedicacionRepository.recuperarIdDocenteAsignaturaAprend(idAsignaturaAprendizaje,idDocente));
//		}
	
		
		/***
		 * retorna todos las actividades personal
		 * @param Authorization
		 * @return
		 */
		@RequestMapping(value="/cargarTodasActividadesPersonal", method=RequestMethod.GET)
		public ResponseEntity<?> cargarTodasActividadesPersonal(@RequestHeader(value="Authorization") String Authorization) {
			if (!securityService.isTokenValido(Authorization)) {
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
			}
			return ResponseEntity.ok(actividadPersonalRepository.listarActividadPersonal());
	    }
		
		/**
		 * recupera lista de las actividades
		 * @param authorization
		 * @return
		 */
		@RequestMapping(value="/recuperaListaActividad", method=RequestMethod.GET)
		public ResponseEntity<?> recuperaListaActividad(@RequestHeader(value="Authorization") String authorization) {
			if (!securityService.isTokenValido(authorization)) {
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
			}
			return ResponseEntity.ok(actividadDetalleRepository.listarActividadPersonal());
		
		}
		
		/***
		 * 
		 * recupera la lista de los detalles de cada actividad
		 * @param authorization
		 * @return
		 */
		@RequestMapping(value="/recuperaListaActividadDetalle", method=RequestMethod.GET)
		public ResponseEntity<?> recuperaListaActividadDetalle(@RequestHeader(value="Authorization") String authorization) {
			if (!securityService.isTokenValido(authorization)) {
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
			}
			return ResponseEntity.ok(actividadDetalleRepository.listarActividadDetalle());
		
		}
		

		/***
		 * reupera los detalle de las actividades pasando por parametro el id del distrbutivo Docente
		 * @param authorization
		 * @param idDistributivoDocente
		 * @return
		 */
		@RequestMapping(value="/recuperaActividadDetalleId/{idDistributivoDocente}", method=RequestMethod.GET)
		public ResponseEntity<?> recuperaActividadDetalleId(@RequestHeader(value="Authorization") String authorization, 
				@PathVariable("idDistributivoDocente") Integer idDistributivoDocente) {
			if (!securityService.isTokenValido(authorization)) {
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
			}
			return ResponseEntity.ok(distributivoDocenteService.refreshDetalleActividadDocente(idDistributivoDocente));
		
		}
		
		
		/***
		 *  Procedimiento que genera las materias que el estudiante podr� ver dado su flujo
		 * @param authorization
		 * @param idAsignatura
		 * @return
		 */
		
		@RequestMapping(value = "/recuperaComponentesAsignaturaId/{idAsignatura}", method = RequestMethod.GET,
				headers="Accept=application/json")
		public ResponseEntity<?> getRecuperaComponentesAsignaturaId(@RequestHeader(value = "Authorization") String authorization,
				@PathVariable ("idAsignatura") Integer idAsignatura){
			if (!securityService.isTokenValido(authorization)) {
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
			}
			return ResponseEntity.ok(distributivoDocenteService.refreshFnAsignaturaComponenteAprendizaje(idAsignatura));
		}
		
		@RequestMapping(value = "/recuperaComponentesAsignaturaPivotId/{idAsignatura}", method = RequestMethod.GET,
				headers="Accept=application/json")
		public ResponseEntity<?> getRecuperaComponentesAsignaturaPivotId(@RequestHeader(value = "Authorization") String authorization,
				@PathVariable ("idAsignatura") Integer idAsignatura){
			if (!securityService.isTokenValido(authorization)) {
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
			}
			return ResponseEntity.ok(distributivoDocenteService.refreshFnAsignaturaComponenteAprendizajePivot(idAsignatura));
		}
		
		
	
		/***
		 * reupera la dedicacion horas clases con respecto a la dedicacion y categoria
		 * @param authorization
		 * @param idDocente
		 * @param idDedicacion
		 * @param fecha
		 * @return
		 */
	@RequestMapping(value="/recuperaDedicacionHorasClases/{idDedicacion}/{idCategoria}", method=RequestMethod.GET)
	public ResponseEntity<?> recuperaDedicacionHorasClases(@RequestHeader(value="Authorization") String authorization, 
		@PathVariable("idDedicacion") Integer idDedicacion ,@PathVariable("idCategoria") Integer idCategoria) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(distributivoDedicacionRepository.recuperarDedicacionHorasClases( idDedicacion,idCategoria));
	}
		
		
	/***
	 * SERVICIO QUE RECUPERA LAS ASIGNATURA CON LA OFERTA Y COMPONENTE APRENDIZAJE ASOCIADO A UN PERIODO ACADEMICO
	 * @param authorization
	 * @param idPeriodo
	 * @param idOferta
	 * @return
	 */
	
	@RequestMapping(value="/listaOfertaMallaAsig/{idPeriodo}/{idOferta}/{idDistributivoOfertaVersion}/{idDistributivoDocente}", method=RequestMethod.GET)
	public ResponseEntity<?> listaOfertaMallaAsig(@RequestHeader(value="Authorization") String authorization, 
			@PathVariable("idPeriodo") Integer idPeriodo ,
			@PathVariable("idOferta") Integer idOferta,
			@PathVariable("idDistributivoOfertaVersion") Integer idDistributivoOfertaVersion,
			@PathVariable("idDistributivoDocente") Integer idDistributivoDocente) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(docenteAsignaturaAprendRepository.listarOfertaMallaAsignatura(idPeriodo, idOferta,idDistributivoOfertaVersion,idDistributivoDocente));
	}
	
	@RequestMapping(value="/listaOfertaMallaAsigCompMul/{idPeriodo}/{idOferta}", method=RequestMethod.GET)
	public ResponseEntity<?> listaOfertaMallaAsigCompMul(@RequestHeader(value="Authorization") String authorization, 
			@PathVariable("idPeriodo") Integer idPeriodo ,
			@PathVariable("idOferta") Integer idOferta) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(docenteAsignaturaAprendRepository.listarOfertaMallaAsigCompMul(idPeriodo, idOferta));
	}
	
	@RequestMapping(value="/listaMallaAsigCompMultidocente/{idPeriodo}/{idOferta}", method=RequestMethod.GET)
	public ResponseEntity<?> listaMallaAsigCompMultidocente(@RequestHeader(value="Authorization") String authorization, 
			@PathVariable("idPeriodo") Integer idPeriodo ,
			@PathVariable("idOferta") Integer idOferta) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(docenteAsignaturaAprendRepository.listarMallaAsigCompMultidocente(idPeriodo, idOferta));
	}
	
	@RequestMapping(value="/listaOfertaMallaAsigComp/{idPeriodo}/{idOferta}/{idDistributivoOfertaVersion}/{idDistributivoDocente}", method=RequestMethod.GET)
	public ResponseEntity<?> listaOfertaMallaAsigComp(@RequestHeader(value="Authorization") String authorization, 
			@PathVariable("idPeriodo") Integer idPeriodo ,
			@PathVariable("idOferta") Integer idOferta,
			@PathVariable("idDistributivoOfertaVersion") Integer idDistributivoOfertaVersion,
			@PathVariable("idDistributivoDocente") Integer idDistributivoDocente) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(docenteAsignaturaAprendRepository.listarOfertaMallaAsigComp(idPeriodo, idOferta,idDistributivoOfertaVersion,idDistributivoDocente));
	}
	
	@RequestMapping(value="/listaOfertaMallaAsigParalelo/{idPeriodo}/{idOferta}/{idDistributivoOfertaVersion}/{idDistributivoDocente}", method=RequestMethod.GET)
	public ResponseEntity<?> listaOfertaMallaAsigParalelo(@RequestHeader(value="Authorization") String authorization, 
			@PathVariable("idPeriodo") Integer idPeriodo ,
			@PathVariable("idOferta") Integer idOferta,
			@PathVariable("idDistributivoOfertaVersion") Integer idDistributivoOfertaVersion,
			@PathVariable("idDistributivoDocente") Integer idDistributivoDocente) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(docenteAsignaturaAprendRepository.listarOfertaMallaAsigParalelo(idPeriodo, idOferta,idDistributivoOfertaVersion,idDistributivoDocente));
	}
	
	
		
		@RequestMapping(value="/listaOfertaAsignaturaAprendizaje/{idPeriodo}/{idOferta}/{idDistributivoOfertaVersion}/{idDistributivoDocente}", method=RequestMethod.GET)
		public ResponseEntity<?> listaOfertaAsignaturaAprendizaje(@RequestHeader(value="Authorization") String authorization, 
				@PathVariable("idPeriodo") Integer idPeriodo ,
				@PathVariable("idOferta") Integer idOferta,
				@PathVariable("idDistributivoOfertaVersion") Integer idDistributivoOfertaVersion,
				@PathVariable("idDistributivoDocente") Integer idDistributivoDocente) {
			if (!securityService.isTokenValido(authorization)) {
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
			}
			return ResponseEntity.ok(docenteAsignaturaAprendRepository.listarOfertaAsignaturaAprendizaje(idPeriodo, idOferta,idDistributivoOfertaVersion,idDistributivoDocente));
		}
		
		


		
		@RequestMapping(value="/listaDocenteAsignaturaAprendizaje/{idDocente}/{idDistributivoDocente}", method=RequestMethod.GET)
		public ResponseEntity<?> listaDocenteAsignaturaAprendizaje(@RequestHeader(value="Authorization") String authorization, 
				@PathVariable("idDocente") Integer idDocente, @PathVariable("idDistributivoDocente") Integer idDistributivoDocente ) {
			if (!securityService.isTokenValido(authorization)) {
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
			}
			if(idDistributivoDocente==0) {
				idDistributivoDocente= null;
			}
			return ResponseEntity.ok(docenteAsignaturaAprendRepository.listarDocenteAsignaturaAprendizaje(idDocente,idDistributivoDocente));
		}
		
		@RequestMapping(value="/listaAsignaturaNoUtilizadaEnDist/{idPeriodoAcademico}/{idDistributivoOfertaVersion}", method=RequestMethod.GET)
		public ResponseEntity<?> listaAsignaturaNoUtilizadaEnDist(@RequestHeader(value="Authorization") String authorization, 
				@PathVariable("idPeriodoAcademico") Integer idPeriodoAcademico, @PathVariable("idDistributivoOfertaVersion") Integer idDistributivoOfertaVersion ) {
			if (!securityService.isTokenValido(authorization)) {
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
			}
		
			return ResponseEntity.ok(distributivoOfertaRepository.asignaturaParaleloNoUtilizadaDist(idPeriodoAcademico, idDistributivoOfertaVersion));
		}
	
		@RequestMapping(value="/listaAsignaturaUtilDist/{idPeriodoAcademico}/{idMalla}", method=RequestMethod.GET)
		public ResponseEntity<?> listaAsignaturaUtilDist(@RequestHeader(value="Authorization") String authorization, 
				@PathVariable("idPeriodoAcademico") Integer idPeriodoAcademico, @PathVariable("idMalla") Integer idMalla ) {
			if (!securityService.isTokenValido(authorization)) {
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
			}
		
			return ResponseEntity.ok(docenteAsignaturaAprendRepository.listarAsigUtil(idPeriodoAcademico, idMalla));
		}
	
		

		
		//***********************FIN SERVICIOS PARA DistributivoDedicacion ***********************//
}
