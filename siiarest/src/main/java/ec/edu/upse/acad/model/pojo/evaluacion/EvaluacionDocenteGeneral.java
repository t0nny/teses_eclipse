package ec.edu.upse.acad.model.pojo.evaluacion;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ec.edu.upse.acad.model.pojo.reglamento.Reglamento;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="eva", name="evaluacion_docente_general")
@NoArgsConstructor
public class EvaluacionDocenteGeneral {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_evaluacion_docente_general")
	@Getter @Setter private Integer id;
	
	@Column(name="decripcion")
	@Getter @Setter private String descripcion;
	
	@Column(name="fecha_desde")
	@Getter @Setter private Date fechaDesde;

	@Column(name="fecha_hasta")
	@Getter @Setter private Date fechaHasta;
	
	@Getter @Setter private String estado;
	
	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;
	
	@Version
	@Getter @Setter private Integer version;

	//RELACIONES
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_reglamento", nullable = false)
	//@JsonIgnore
	@Getter @Setter private Reglamento reglamento;
		
	@OneToMany(mappedBy = "evaluacionDocenteGeneral", cascade = CascadeType.ALL)
	@Getter @Setter private List<PeriodoEvaluacion> periodoEvaluacion;
		
	 @PrePersist
	 void preInsert() {
	   if (this.estado == null)
	       this.estado = "A";
	 }

}
