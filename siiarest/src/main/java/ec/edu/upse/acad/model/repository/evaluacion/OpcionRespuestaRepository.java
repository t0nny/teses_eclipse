package ec.edu.upse.acad.model.repository.evaluacion;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.evaluacion.OpcionRespuesta;

@Repository
public interface OpcionRespuestaRepository extends JpaRepository<OpcionRespuesta, Integer>{
	//consulta para buscar las escalas
		@Query(value="SELECT o "
				+ "FROM OpcionRespuesta o "
				+ "WHERE o.estado = 'A' ")
		List<OpcionRespuesta> listarOpcion();
		
		//listar opciones de respuestas relacionadas con pregunta y asu ves el instrumento con el periodo actual
		@Query(value="SELECT o.valor as valor, o.descripcion as descripcion, "
				+ "CONCAT(cast(o.valor as text),' = ', o.descripcion) as escala "
				+ "FROM Pregunta p "
				+ "inner join InstrumentoPregunta ip on p.id = ip.pregunta.id "
				+ "inner join Instrumento i on i.id = ip.instrumento.id "
				+ "inner join OpcionPregunta op on op.pregunta.id = ip.pregunta.id "
				+ "inner join OpcionRespuesta o on o.id = op.idOpcionRespuesta.id "
				+ "inner join PerTipoEvalInstrumento ptei on ptei.instrumento.id = i.id "
				+ "inner join PeriodoEvaluacion pe on pe.id = ptei.periodoEvaluacion.id "
				+ "WHERE p.estado = 'A' and ip.estado = 'A' and i.estado = 'A' and op.estado = 'A' and o.estado = 'A' and ptei.estado = 'A' and pe.estado = 'A' "
				+ "and (GETDATE() >= pe.fechaDesde) and (GETDATE() <= pe.fechaHasta) " 
				+ "GROUP BY " 
				+ "o.valor, o.descripcion "
				+ "ORDER BY " 
				+ "o.valor, o.descripcion")
		List<COOpcionRespuestaPregunta> listarOpcionRespuestaPregunta();
		
		interface COOpcionRespuestaPregunta{
			Integer getValor();
			String getDescripcion(); 
			String getEscala(); 
		}
	
		/*select ore.valor, ore.descripcion
		from 
			eva.pregunta p
			
			inner join eva.instrumento_pregunta ipr on p.id_pregunta = ipr.id_pregunta
			inner join eva.instrumento i on i.id_instrumento = ipr.id_instrumento			
			inner join eva.opcion_pregunta op on ipr.id_pregunta = op.id_pregunta
			inner join eva.opcion_respuesta ore on op.id_opcion_respuesta = ore.id_opcion_respuesta
			inner join eva.per_tipo_eval_instrumento ptei on ptei.id_instrumento = i.id_instrumento
			inner join eva.periodo_evaluacion pe on ptei.id_periodo_evaluacion = pe.id_periodo_evaluacion
		where
			p.estado = 'A' and ipr.estado = 'A' and i.estado = 'A' and o.estado ='A'
			and (GETDATE() >= pe.fecha_desde ) and (GETDATE() <= pe.fecha_hasta ) 
			group by
			ore.valor, ore.descripcion*/
		
		
		//
}
