package ec.edu.upse.acad.ws.vinculacion;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ec.edu.upse.acad.model.pojo.vinculacion.AsignacionActividad;
import ec.edu.upse.acad.model.repository.vinculacion.AsignacionActividadesRepository;
import ec.edu.upse.acad.model.service.SecurityService;
import ec.edu.upse.acad.model.service.vinculacion.AsignacionActividadesService;

@RestController
@RequestMapping("/api/asignacionActividades")
@CrossOrigin
public class AsignacionActividadesController {
	@Autowired
	private SecurityService securityService;
	@Autowired
	private AsignacionActividadesService asignacionActividadesService;
	@Autowired
	private AsignacionActividadesRepository asignacionActividadesRepository;
	
	@RequestMapping(value = "/grabarAsignacionActividades", method = RequestMethod.POST)
	public ResponseEntity<?> grabarasignacionActividades(@RequestHeader(value = "Authorization") String Authorization, // definimos
			@RequestBody List<AsignacionActividad> asignacionActividades) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		try {
			asignacionActividadesService.guardarAsignacionActividades(asignacionActividades);
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(" ya" + e.getMessage() + "este " + e.getCause());
			// return ResponseEntity.status((HttpStatus.NOT_FOUND)).body(null);
		}
		return ResponseEntity.ok().build();
	}
	
	@RequestMapping(value = "/borrarAsignacionActividades/{idAsignacion}", method = RequestMethod.DELETE)
	public ResponseEntity<?> borrar(@RequestHeader(value = "Authorization") String Authorization,
			@PathVariable("idAsignacion") Integer idAsignacion) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		AsignacionActividad asignacionActividades = asignacionActividadesRepository.findById(idAsignacion).get();
		if (asignacionActividades != null) {
			asignacionActividadesService.borrarAsignacionActividades(asignacionActividades);
			return ResponseEntity.ok().build();
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@RequestMapping(value = "/buscarAsignacionActividades/{idProyecto}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarAsignacionActividades(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable("idProyecto") Integer idProyecto) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(asignacionActividadesRepository.buscarAsignacionActividades(idProyecto));
	}

}
