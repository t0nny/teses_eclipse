package ec.edu.upse.acad.model.pojo.reglamento;

import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="aca", name="tipo_reglamento")
@NoArgsConstructor


public class TipoReglamento {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_tipo_reglamento")
	@Getter @Setter private Integer id;
	
	@Getter @Setter  private String codigo;
	
	@Getter @Setter  private String descripcion;
	
	@Getter @Setter  private String estado;
	
	@Column(name="fecha_ingreso")
	@Getter @Setter  private Date fechaIngreso;
	
	@Column(name="usuario_ingreso_id")
	@Getter @Setter  private String usuarioIngresoId;
	
	@Version
	@Getter @Setter  private Integer version;
	
	//RELACIONES
	
	//bi-directional many-to-one association to Reglamentos
		@OneToMany(mappedBy="tipoReglamento", cascade=CascadeType.ALL)
		@Getter @Setter private List<Reglamento> reglamento;
	
	
	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }


}
