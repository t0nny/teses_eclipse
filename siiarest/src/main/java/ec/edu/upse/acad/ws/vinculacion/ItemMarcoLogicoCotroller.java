package ec.edu.upse.acad.ws.vinculacion;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.upse.acad.model.pojo.vinculacion.ObjetivoTarea;
import ec.edu.upse.acad.model.pojo.vinculacion.Proyecto;
import ec.edu.upse.acad.model.repository.vinculacion.ItemMarcoLogicoRepository;
import ec.edu.upse.acad.model.repository.vinculacion.ObjetivoTareaRepository;
import ec.edu.upse.acad.model.service.SecurityService;
import ec.edu.upse.acad.model.service.vinculacion.ItemMarcoLogicoService;

@RestController
@RequestMapping("/api/marcoLogico")
@CrossOrigin
public class ItemMarcoLogicoCotroller {
	@Autowired
	private SecurityService securityService;
	@Autowired
	private ItemMarcoLogicoRepository itemMarcoLogicoRepository;
	@Autowired
	private ItemMarcoLogicoService itemMarcoLogicoService;
	@Autowired
	private ObjetivoTareaRepository objetivoTareaRepository;

	// buscar solo activos
	@RequestMapping(value = "/buscarObjetivoGlobal/{idProyecto}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarObjetivoGlobal(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable("idProyecto") Integer idProyecto) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(itemMarcoLogicoRepository.buscarObjetivoGlobal(idProyecto));
	}
	
	@RequestMapping(value = "/buscarByIdItemMarcoLogico/{idItemMarcoLogico}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarByIdItemMarcoLogico(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable("idItemMarcoLogico") Integer idItemMarcoLogico) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(itemMarcoLogicoRepository.findById(idItemMarcoLogico).get());
	}
	
	@RequestMapping(value = "/buscarActividad/{idItemMarcoLogico}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarActividad(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable("idItemMarcoLogico") Integer idItemMarcoLogico) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(itemMarcoLogicoRepository.buscarActividades(idItemMarcoLogico));
	}
	
	@RequestMapping(value = "/buscarObjetivoTareas/{idProyecto}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarObjetivoTareas(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable("idProyecto") Integer idProyecto) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(itemMarcoLogicoRepository.buscarObjetivoTareas(idProyecto));
	}
	
	@RequestMapping(value = "/buscarObjetivoActividadYTareas/{idProyecto}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarObjetivoActividadYTareas(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable("idProyecto") Integer idProyecto) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(itemMarcoLogicoRepository.buscarObjetivoActividadYTareas(idProyecto));
	}
	
	@RequestMapping(value = "/buscarPresupuestoTareas/{idProyecto}", method = RequestMethod.GET)
	public ResponseEntity<?> buscarPresupuestoTareas(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable("idProyecto") Integer idProyecto) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(itemMarcoLogicoRepository.buscarPresupuestoTareas(idProyecto));
	}
	
	@RequestMapping(value = "/grabarItemMarcoLogico", method = RequestMethod.POST)
	public ResponseEntity<?> grabarItemMarcoLogico(@RequestHeader(value = "Authorization") String Authorization, // definimos
			// que tenga
			// autorizacion
			@RequestBody Proyecto proyecto) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		try {
			// System.out.println(proyecto.toString());
			itemMarcoLogicoService.proyectoItemMarcoLogico(proyecto);

		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(" ya" + e.getMessage() + "este " + e.getCause());
			// return ResponseEntity.status((HttpStatus.NOT_FOUND)).body(null);
		}
		return ResponseEntity.ok().build();
	}
	
	@RequestMapping(value = "/grabarObjetivoTarea", method = RequestMethod.POST)
	public ResponseEntity<?> grabarObjetivoTarea(@RequestHeader(value = "Authorization") String Authorization, // definimos
			// que tenga
			// autorizacion
			@RequestBody ObjetivoTarea objetivoTarea) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		try {
			// System.out.println(proyecto.toString());
			if (objetivoTarea.getId() != null) {
				System.out.println("editar registro");
				ObjetivoTarea _objetivoTarea =objetivoTareaRepository.findById(objetivoTarea.getId()).get();
				BeanUtils.copyProperties(objetivoTarea, _objetivoTarea);
				objetivoTareaRepository.save(_objetivoTarea);	
				
			} else {
				System.out.println("ingresar registro");
				objetivoTareaRepository.save(objetivoTarea);				
			}		

		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(" ya" + e.getMessage() + "este " + e.getCause());
			// return ResponseEntity.status((HttpStatus.NOT_FOUND)).body(null);
		}
		return ResponseEntity.ok().build();
	}
}
