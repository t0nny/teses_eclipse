package ec.edu.upse.acad.model.pojo;

import java.sql.Timestamp;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="aca", name="itinerario_materia")
@NoArgsConstructor
public class ItinerarioMateria  {
	

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_itinerario_materia")
	@Getter @Setter private Integer id;

	@Getter @Setter private String estado;
	
	@Column(name="fecha_ingreso")
	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;

	@Column(name="id_malla_asignatura")
	@Getter @Setter private Integer idMallaAsignatura;
	
	@Column(name="id_itinerario")
	@Getter @Setter private Integer idItinerario;
	
	
	//bi-directional many-to-one association to Itinerario
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_itinerario", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Itinerario itinerario;

	//bi-directional many-to-one association to MallaAsignatura
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_malla_asignatura", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private MallaAsignatura mallaAsignatura;

	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }

}