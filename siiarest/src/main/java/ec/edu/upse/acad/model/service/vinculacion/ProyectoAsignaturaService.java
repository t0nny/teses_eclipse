package ec.edu.upse.acad.model.service.vinculacion;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.upse.acad.model.pojo.vinculacion.ProyectoAsignatura;
import ec.edu.upse.acad.model.repository.vinculacion.ProyectoAsignaturaRepository;

@Service
@Transactional
public class ProyectoAsignaturaService {
	
	@PersistenceContext
	private EntityManager em;
	@Autowired
	private ProyectoAsignaturaRepository proyectoAsignaturaRepository;

	// borrar asignatura del grig
	public void borrarAsignatura(Integer idProyectoAsignatura) {
		ProyectoAsignatura _proyectoAsignatura = proyectoAsignaturaRepository.findById(idProyectoAsignatura).get();
		_proyectoAsignatura.setEstado("E");
		proyectoAsignaturaRepository.save(_proyectoAsignatura);	
		em.getEntityManagerFactory().getCache().evict(ProyectoAsignatura.class);
	}	

}
