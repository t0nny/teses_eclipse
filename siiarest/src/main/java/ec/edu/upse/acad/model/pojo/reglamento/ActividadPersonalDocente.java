package ec.edu.upse.acad.model.pojo.reglamento;

import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="aca", name="actividad_personal_docente")
@NoArgsConstructor
public class ActividadPersonalDocente {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_actividad_personal")
	@Getter @Setter private Integer id;
	
	@Getter @Setter  private String descripcion;
	
    @Getter @Setter  private String estado;

	
	@Column(name="usuario_ingreso_id")
	@Getter @Setter  private String usuarioIngresoId;
	
	@Version
	@Getter @Setter  private Integer version;
	
	
	//RELACIONES
	//bi-directional many-to-one association to Detalle Actividad
    @OneToMany(mappedBy="actividadPersonal", cascade=CascadeType.ALL)
    @Getter @Setter private List<ReglamentoActividadDocente> reglamentoActividadDetalle;

    //bi-directional many-to-one association to Actividad Valor
    @OneToMany(mappedBy="actividadPersonal", cascade=CascadeType.ALL)
    @Getter @Setter private List<ActividadValor> actividadValor;

	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }
	
	

}
