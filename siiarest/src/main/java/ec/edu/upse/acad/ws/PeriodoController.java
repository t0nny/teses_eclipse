package ec.edu.upse.acad.ws;

import org.json.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.upse.acad.model.pojo.Periodo;
import ec.edu.upse.acad.model.repository.PeriodoRepository;
import ec.edu.upse.acad.model.repository.distributivo.ReglamentoRepository;
import ec.edu.upse.acad.model.service.SecurityService;

@RestController
@RequestMapping("/api/periodo")
@CrossOrigin
public class PeriodoController {

	@Autowired private PeriodoRepository periodoRepository;
	@Autowired private ReglamentoRepository reglamentoRepository;
	@Autowired private SecurityService securityService;

	//****************** SERVICIOS PARA PERIODO************************//

	//Buscar todos los periodos, para listar en angular
	@RequestMapping(value="/buscarPeriodos", method=RequestMethod.GET)
	public ResponseEntity<?> buscarPeriodos() {
		return ResponseEntity.ok(periodoRepository.buscarPeriodo());
	}
	
	@RequestMapping(value="/buscarPeriodosAnio", method=RequestMethod.GET)
	public ResponseEntity<?> buscarPeriodos1() {
		return ResponseEntity.ok(periodoRepository.buscarPeriodo1());
	}
	@RequestMapping(value="/buscarPeriodosPorId/{id}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarPeriodos1(@RequestHeader(value="Authorization") String authorization,
			@PathVariable("id") Integer id) {
		if (!securityService.isTokenValido(authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(periodoRepository.buscarPeriodoPorId(id));
	}
	

	@RequestMapping(value="/buscarPeriodosId/{id}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarPeriodosId(@PathVariable("id") Integer id) {
		Periodo _periodo;
		if (periodoRepository.findById(id).isPresent()) {
			_periodo = periodoRepository.findById(id).get();
			return ResponseEntity.ok(_periodo);
		}else {
			return ResponseEntity.notFound().build();
		}
	}	

	@RequestMapping(value="/buscarPeriodosAnioId/{id}",method=RequestMethod.GET)
	public ResponseEntity<?> 
	buscarPeriodosAnioId(@PathVariable("id") Integer id) {
		return ResponseEntity.ok(periodoRepository.buscarPeriodoId(id));
	}


	//Servicio de grabar Periodo Academico
	@RequestMapping(value="/grabarPeriodo", method=RequestMethod.POST)
	public ResponseEntity<?> grabarPeriodo(@RequestBody Periodo periodo) {
		Periodo _periodo= new Periodo();
		if (periodo.getId() != null) {
			_periodo = periodoRepository.findById(periodo.getId()).get();
		}
		BeanUtils.copyProperties(periodo, _periodo);
		JSONObject json = new JSONObject(_periodo); // Convert text to object
		System.out.println(json.toString());			
		periodoRepository.saveAndFlush(_periodo);
		return ResponseEntity.ok(_periodo);
	}
	//***********************FIN SERVICIOS PARA PERIODO ***********************

	//BUSCAR REGLAMENTO
	
	@RequestMapping(value="/buscarPorReglamentoRegimenAca", method=RequestMethod.GET)
	public ResponseEntity<?> buscarPorReglamentoRegimenAca() {
		return ResponseEntity.ok(reglamentoRepository.reglamentoRegimenAcad());
	}
	
	@RequestMapping(value="/buscarPorTipoReglamento/{idTipo}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarPorTipoReglamento(@PathVariable("idTipo") Integer idTipo) {
		return ResponseEntity.ok(reglamentoRepository.buscaridTipo(idTipo));
	}

}	