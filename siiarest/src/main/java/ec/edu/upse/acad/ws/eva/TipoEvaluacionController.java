/**
 * 
 */
package ec.edu.upse.acad.ws.eva;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.upse.acad.model.pojo.evaluacion.ReglaComponente;
import ec.edu.upse.acad.model.repository.evaluacion.TipoEvaluacionRepository;
import ec.edu.upse.acad.model.service.eva.TipoEvaluacionService;

/**
 * @author SIIA_UPSE
 *
 */

@RestController
@RequestMapping("/api/tipoEvaluacion")
@CrossOrigin
public class TipoEvaluacionController {
	@Autowired private TipoEvaluacionRepository tipoEvaluacionRepository;
	@Autowired private TipoEvaluacionService tipoEvaluacionService;
	
	@RequestMapping(value="/findComponentes", method=RequestMethod.GET)
	public ResponseEntity<?> findComponente(){
		return ResponseEntity.ok(tipoEvaluacionService.filtrarComponentes());
	}
	
	@RequestMapping(value="/findFuncionEvaluacion", method=RequestMethod.GET)
	public ResponseEntity<?> findFuncionEvaluacion(){
		return ResponseEntity.ok(tipoEvaluacionRepository.findFuncionEvaluacion());
	}
	
	@RequestMapping(value = "/saveReglaComponente", method = RequestMethod.POST)
	public ResponseEntity<?> saveReglaComponente(@RequestBody List<ReglaComponente> reglaComponente){
		tipoEvaluacionService.saveReglaComponente(reglaComponente);
		return ResponseEntity.ok(reglaComponente);
	}
	
//	@RequestMapping(value="/findAllReglaComponente/{idReglamento}", method=RequestMethod.GET)
//	public ResponseEntity<?> findAllReglaComponente(
//			@PathVariable("idReglamento") Integer idReglamento){
//		return ResponseEntity.ok(tipoEvaluacionRepository.findByIdReglaComponente(idReglamento));
//	}
}
