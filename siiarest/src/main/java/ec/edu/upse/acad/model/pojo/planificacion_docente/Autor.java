package ec.edu.upse.acad.model.pojo.planificacion_docente;

import javax.persistence.*;

import org.hibernate.annotations.Where;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.util.List;

/**
 * The persistent class for the autor database table.
 * 
 */
@Entity
@Table(schema="aca", name="autor")
@Where(clause = "estado='A'")
@NoArgsConstructor
public class Autor  {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_autor")
	@Getter @Setter  private Integer id;

	@Getter @Setter  private String estado;

//	@Column(name="fecha_ingreso")
//	private Timestamp fechaIngreso;

	@Getter @Setter private String nombres;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;
	
	@Version
	@Getter @Setter private Integer version;

	//RELACIONES
	//bi-directional many-to-one association to RecursoBibliografico
	@OneToMany(mappedBy="autor", cascade=CascadeType.ALL)
	@Getter @Setter private List<RecursoBibliografico> recursoBibliograficos;
	
	@PrePersist
	void preInsert() {
	   if (this.estado == null)
	       this.estado = "A";
	}


}