package ec.edu.upse.acad.model.pojo.planificacion_docente;

import java.sql.Date;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ec.edu.upse.acad.model.pojo.MallaAsignatura;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="aca", name="silabo")
@NoArgsConstructor
public class Syllabus {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_silabo")
	@Getter @Setter private Integer id;

	@Column(name = "id_malla_asignatura")
	@Getter @Setter private Integer idMallaAsignatura;

	@Getter @Setter private String descripcion;

	@Column(name = "fecha_desde")
	@Getter @Setter private Date fechaDesde;

	@Column(name = "fecha_hasta")
	@Getter @Setter private Date fechaHasta;

	@Getter @Setter private String estado;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private Integer usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;

	//RELACIONES
	//bi-directional many-to-one association to CompatibilidadAsignatura
	@ManyToOne
	@JoinColumn(name="id_malla_asignatura", insertable = false, updatable = false)
	@JsonIgnore
	@Getter @Setter private MallaAsignatura mallaAsignatura;

	@OneToMany(mappedBy = "syllabus", cascade = CascadeType.ALL)
	@Getter @Setter private List<Contenido> contenido;

	@OneToMany(mappedBy = "syllabus", cascade = CascadeType.ALL)
	@Getter @Setter private List<Contenidos> contenidos;
	@PrePersist
	void preInsert() {
		if (this.estado == null)
			this.estado = "A";
	}
}
