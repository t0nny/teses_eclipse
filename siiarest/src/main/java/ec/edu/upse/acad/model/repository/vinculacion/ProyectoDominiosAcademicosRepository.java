package ec.edu.upse.acad.model.repository.vinculacion;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.vinculacion.DominiosAcademicos;
import ec.edu.upse.acad.model.pojo.vinculacion.ProyectoDominiosAcademicos;

@Repository
public interface ProyectoDominiosAcademicosRepository extends JpaRepository<ProyectoDominiosAcademicos, Integer>{
	
	@Transactional
	@Query(value = "SELECT da "
			+ "FROM DominiosAcademicos da " + " WHERE da.estado='A' ")
	List<DominiosAcademicos> buscarDominiosAcademicos();

	
	@Query(value = "SELECT da "
			+ "FROM DominiosAcademicos da " + " WHERE da.id=(?1)  and da.estado='A' ")
	Optional<DominiosAcademicos> ListarPorIdDominiosAcademicos(Integer idAreaUnesco);

}
