package ec.edu.upse.acad.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.AsignaturaAprendizaje;
import ec.edu.upse.acad.model.repository.distributivo.MallaAsignaturaFusionRepository.CustObjMallaAsignaturaFusionCom;
import ec.edu.upse.acad.model.repository.distributivo.MallaAsignaturaFusionRepository.CustObjMallaAsignaturaParalelos;


@Repository
public interface AsignaturaAprendizajeRepository extends JpaRepository<AsignaturaAprendizaje, Integer>{
	
	@Query(value="SELECT cg.id, cg.estado, cg.version, cg.valor,  cg.idComponenteAprendizaje, cg.idMallaAsignatura "
			+ "FROM AsignaturaAprendizaje AS cg "
			+ "WHERE cg.id LIKE ?1 ")
	List<AsignaturaAprendizaje> buscarPorComponenteAsignatura(Integer usuarioIngresoId);
	
	@Query(value="SELECT e from AsignaturaAprendizaje as e where e.id = ?1 ")
	 AsignaturaAprendizaje listaAsignaturaAprendizajeCompId(Integer id);


	
	
	
}
	




