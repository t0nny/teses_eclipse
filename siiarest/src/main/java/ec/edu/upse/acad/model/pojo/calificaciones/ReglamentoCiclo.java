package ec.edu.upse.acad.model.pojo.calificaciones;


import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ec.edu.upse.acad.model.pojo.reglamento.Reglamento;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="aca", name="reglamento_ciclo")
@NoArgsConstructor
public class ReglamentoCiclo {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_reglamento_ciclo")
	@Getter @Setter private Integer id;
	
	@Column(name="id_ciclo")
	@Getter @Setter private Integer idCiclo;
	
	@Column(name="id_reglamento")
	@Getter @Setter private Integer idReglamento;

	@Getter @Setter private String estado;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;
	
	//bi-directional many-to-one association to Reglamento
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_ciclo" , insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Ciclo ciclo;
	
	//bi-directional many-to-one association to Reglamento
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_reglamento" , insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Reglamento reglamento;

	//bi-directional one-to-many association to ReglamentoCiclo
	@OneToMany(mappedBy="reglamentoCiclo")
	@Getter @Setter private List<CicloAprendizaje> cicloAprendizajes;
	
	@PrePersist
	void preInsert() {
	   if (this.estado == null)
	       this.estado = "A";
	}
}
