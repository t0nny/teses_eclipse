package ec.edu.upse.acad.model.pojo.reglamento;

import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="aca", name="reglamento_actividad_docente")
@NoArgsConstructor
public class ReglamentoActividadDocente {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_reglamento_actividad")
	@Getter @Setter private Integer id;
	
	@Column(name="id_reglamento")
	@Getter @Setter private Integer idReglamento;
	
	@Column(name="id_actividad_personal")
	@Getter @Setter private Integer idActividadPersonal;

	@Getter @Setter  private String estado;
		
	@Column(name="fecha_ingreso")
	@Getter @Setter  private Date fechaIngreso;
		
	@Column(name="usuario_ingreso_id")
	@Getter @Setter  private String usuarioIngresoId;
		
	@Version
	@Getter @Setter  private Integer version;
	
	 
	//RELACIONES
	//bi-directional many-to-one association to Reglamento
		@ManyToOne(fetch=FetchType.LAZY)
		@JoinColumn(name="id_reglamento" , insertable=false, updatable = false)
		@JsonIgnore
		@Getter @Setter private Reglamento reglamento;
			
	//bi-directional many-to-one association to ActividadPersonal
		@ManyToOne(fetch=FetchType.LAZY)
		@JoinColumn(name="id_actividad_personal" , insertable=false, updatable = false)
		@JsonIgnore
		@Getter @Setter private ActividadPersonalDocente actividadPersonal;
		
	//bi-directional many-to-one association to ActividadPersonal	
		@OneToMany(mappedBy="reglamentoActividadDocente", cascade=CascadeType.ALL)
		@Getter @Setter private List<ReglamentoActividadDetalle> reglamentoActividadDetalles;
			
	
	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }
	
}
