package ec.edu.upse.acad.model.repository;


import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.Malla;

@Repository
public interface MallaRepository extends JpaRepository<Malla, Integer>{
	@Query(value="SELECT  pa.id as id,pa.codigo as codigo,pa.descripcion as descripcion "+
			"FROM PeriodoAcademico pa " + 
			"WHERE pa.estado='A' and (pa.fechaDesde>=(?1) or  pa.fechaHasta>=(?1)) order by pa.descripcion desc")
List<CustomObjectPeriodoDesde> listaPeriodoDesde(Date fechaDesde);		
interface CustomObjectPeriodoDesde  { 
    Integer getId(); 
    String getCodigo(); 
    String getDescripcion(); 

}

	//		@Query(value="select pmv.idPeriodoAcademico,mv.descripcion,mv.idNivelMinAperturado,mv.idNivelMaxAperturado, " + 
	//				"mv.fechaAprobacion,mv.fechaDesde,mv.fechaHasta,mv.estado from MallaVersion mv " + 
	//				"inner join PeriodoMallaVersion pmv on pmv.idMallaVersion = mv.id " + 
	//				"where mv.fechaHasta > CURRENT_DATE and mv.estado = 'A' ")
	//		 List<CustomObject> filtrarMallasVersion();
	interface CustomObject {
		Integer getId();
		String getDescripcion();
		Integer getNivelMin();
		Integer getNivelMax();
		Date getFechaAprobacion();
		Date getFechaDesde();
		Date getFechaHasta();
		String getEstado();
	}	

	// malla version por idmallaversion
	@Query(value=" select m.id as id,m.descripcion as descripcion,m.idNivelMinAperturado as nivelMin,m.idNivelMaxAperturado as nivelMax, " + 
			" m.fechaAprobacion as fechaAprobacion,m.fechaDesde as fechaDesde,m.fechaHasta as fechaHasta,m.estado as estado,m.numCreditos as numCred,m.version as version,m.versionMalla as versionMalla, " +
			" m.idDepartamentoOferta as idDepartamentoOferta,m.idReglamento as idReglamento,m.numeroHorasMaximo as numeroHorasMaximo,m.numAsignaturas as numAsignaturas, "+ 
			" m.numNiveles as numNiveles,m.numAsignaturasAprobar as numAsignaturasAprobar "+
			" from Malla m " + 
//			" inner join PeriodoMalla pm on pm.idMalla = m.id " + 
			" where m.id=?1")
	List<CustomObjectMv> filtrarMallasVersionId(Integer idMallaVersion);
	interface CustomObjectMv {
		Integer getId();
//		Integer getIdPeriodo();
		String getDescripcion();
		Integer getNivelMin();
		Integer getNivelMax();
		String getFechaAprobacion();
		String getFechaDesde();
		String getFechaHasta();
		String getEstado();
		Integer getNumCred();
		Integer getVersion();	
		Integer getVersionMalla();
		Integer getidDepartamentoOferta();
		Integer getidReglamento();
		Integer getnumeroHorasMaximo();
		Integer getnumAsignaturas();
		Integer getnumNiveles();
		Integer getnumAsignaturasAprobar();

	}


	
	/***
	 * retorna la lista de mallas version por carrera
	 * @param idOferta
	 * @param idPeriodoAcademico
	 * @return
	 */

	@Query(value="SELECT distinct mv.id as id, CONCAT( mv.descripcion,'-',cast(mv.versionMalla as text)) as descripcion,"+
			"mv.estado as estado "+
			"FROM Malla mv " + 
			"INNER JOIN PeriodoMalla pmv on pmv.idMalla = mv.id " + 
			"INNER JOIN DepartamentoOferta do on mv.idDepartamentoOferta = do.id " + 
			"INNER JOIN Oferta o on do.idOferta = o.id " + 
			"WHERE o.id = (?1) and pmv.idPeriodoAcademico = (?2) and mv.estado = 'A' ")
	List<CustomObjectMallaVersion> filtrarMallasVersionyPeriodo(Integer idOferta, Integer idPeriodoAcademico);
	interface CustomObjectMallaVersion {
		Integer getId();
		String getDescripcion();
		String getEstado();
	}


	@Query(value="select distinct m.id as idMalla,o.id as idOferta,o.descripcion as oferta,m.versionMalla as versionMalla, "+
			"m.numCreditos as numCreditos, m.numeroHorasMaximo as numHorasMaximo,m.numAsignaturas as numAsignaturas, m.numAsignaturasAprobar as numAsignaturasAprobar, "+
			"m.numNiveles as numNiveles, m.fechaAprobacion as fechaAprobacion, "+
			"(SELECT n.orden FROM Nivel n where n.id=m.idNivelMinAperturado ) as nivelMinAperturado,   "+
			"(SELECT n.orden FROM Nivel n where n.id=m.idNivelMaxAperturado ) as nivelMaxAperturado,   "+
			"CONCAT( m.descripcion,'-', cast(m.versionMalla as text)) as descripMallaVersion,"+
			"m.estado as estado, pmv.idPeriodoAcademico as idPeriodoAcademico, d.id as idDepartamento,50 as porcentaje "+
			"from PeriodoMalla pmv " + 
			"inner join Malla m on pmv.idMalla= m.id " + 
			"inner join DepartamentoOferta do on m.idDepartamentoOferta = do.id " + 
			"inner join Oferta o on do.idOferta = o.id " +
			"inner join Departamento d on do.idDepartamento=d.id "+
			"where o.idTipoOferta = (?1) and pmv.idPeriodoAcademico = (?2) and d.id=(?3)"+
			"and m.estado in('A','P')   and pmv.estado='A' and do.estado='A' and o.estado='A' and d.estado='AC' ")
	List<CustomObjectMallaVersionPorPeriodoTipoOferta> filtrarMallasVersionPorPeriodoTipoOferta(Integer idTipoOferta, Integer idPeriodoAcademico, Integer idDepartamento);
	interface CustomObjectMallaVersionPorPeriodoTipoOferta {
		Integer getIdMalla();
		Integer getIdOferta();
		String getOferta();
		Integer getVersionMalla();
		Integer getNumCreditos();
		Integer getNumHorasMaximo();
		Integer getNumAsignaturas();
		Integer getNumAsignaturasAprobar();
		Integer getNumNiveles();
		String getFechaAprobacion();
		Integer getNivelMinAperturado();
		Integer getNivelMaxAperturado();
		String getDescripMallaVersion();
		String getEstado();
		Integer getIdPeriodoAcademico();
		Integer getIdDepartamento();
		Integer getPorcentaje();
	}

	@Query(value=" select m from Malla m where m.id = (?1) ")
	Malla buscarMallasId(Integer id);
	
	
	@Query(value="SELECT rv.codigo as codigo, rv.descripcion as descripcion, rv.valor as valor  "
			+ "FROM TipoReglamento tr "
			+ "INNER JOIN Reglamento r on tr.id=r.idTipoReglamento "
			+ "INNER JOIN ReglamentoValidacion rv on r.id =rv.idReglamento "
			+ "WHERE tr.estado='A' and r.estado='A' and rv.estado='A' and tr.codigo='RRA'")
	List<customObjetr> buscarReglamento();
	interface customObjetr{
		String getCodigo();
		String getDescripcion();
		Integer getValor();

	}
}

