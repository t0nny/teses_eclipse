package ec.edu.upse.acad.model.repository.evaluacion;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.evaluacion.InstrumentoPregunta;
import ec.edu.upse.acad.model.pojo.evaluacion.Pregunta;
import ec.edu.upse.acad.model.repository.AsignaturaRepository.CustomObjetctDescipcion; 

@Repository
public interface InstrumentoPreguntaRepository extends JpaRepository<InstrumentoPregunta, Integer>{

	@Query(value="SELECT p "
			+ "FROM Pregunta p "
			+ "INNER join InstrumentoPregunta ip on p.id = ip.pregunta.id "
			+ "INNER join Instrumento i on i.id = ip.instrumento.id "
			+ "WHERE ip.instrumento.id = ?1 and p.estado = 'A' and ip.estado = 'A' and i.estado = 'A' ")
	List<Pregunta> listarInstrumentoPregunta(Integer id);	
	
	
}
