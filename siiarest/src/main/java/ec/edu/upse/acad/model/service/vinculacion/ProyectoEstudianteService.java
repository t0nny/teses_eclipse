package ec.edu.upse.acad.model.service.vinculacion;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.upse.acad.model.pojo.vinculacion.ProyectoEstudiante;
import ec.edu.upse.acad.model.repository.vinculacion.ProyectoEstudianteRepository;

@Service
@Transactional
public class ProyectoEstudianteService {
	@PersistenceContext
	private EntityManager em;
	@Autowired
	private ProyectoEstudianteRepository proyectoEstudianteRepository;

	// borrar ProyectoEstudiante del grig
	public void borrarProyectoEstudiante(Integer idProyectoEstudiante) {
		ProyectoEstudiante _proyectoEstudiante = proyectoEstudianteRepository.findById(idProyectoEstudiante).get();
		_proyectoEstudiante.setEstado("E");
		proyectoEstudianteRepository.save(_proyectoEstudiante);
		em.getEntityManagerFactory().getCache().evict(ProyectoEstudiante.class);
	}

}
