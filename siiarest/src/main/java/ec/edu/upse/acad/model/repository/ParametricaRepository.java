package ec.edu.upse.acad.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ec.edu.upse.acad.model.pojo.seguridad.Parametrica;


public interface ParametricaRepository extends JpaRepository<Parametrica, Integer>{

		@Query(value="SELECT p "
				+ "FROM Parametrica AS p "
				+ "WHERE p.nombre LIKE ?1 "
				+ "OR p.descripcion LIKE ?1 ")
		List<Parametrica> buscarPorParametricas(String nombre);
}
