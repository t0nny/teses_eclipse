package ec.edu.upse.acad.model.repository.distributivo;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.reglamento.ReglamentoActividadDocente;


@Repository
public interface ReglamentoActividadDocenteRepository extends JpaRepository<ReglamentoActividadDocente, Integer>{
	
	@Query(value=" SELECT apd.id as idActividadPersonalDocente, apd.descripcion as actividadPersonalDocente,"+
			" apd.estado as estadoActividadPersonal,apd.version as versionActividadPersonal, rad.id as idReglamentoActividadDocente,"+
			" rad.estado as estadoReglamentoActividadDocente,isnull(rad.version,0) as versionReglamentoActividadDocente,"+
			" case when rad.id > 0 then 1 else 0 end as reglamentoActividad "+
			" FROM ActividadPersonalDocente as apd " + 
			" LEFT JOIN ReglamentoActividadDocente as rad on apd.id=rad.idActividadPersonal and rad.estado='A' and rad.idReglamento=?1 "+
			" where apd.estado='A'  ")
	List<CustomObjectActividadPersonal> listarActividadPersonal(Integer idReglamento);

	interface CustomObjectActividadPersonal{ 
		Integer getIdActividadPersonalDocente(); 
		String getActividadPersonalDocente(); 
		String getEstadoActividadPersonal();
		Integer getVersionActividadPersonal();
		Integer getIdReglamentoActividadDocente(); 
		String getEstadoReglamentoActividadDocente(); 
		Integer getVersionReglamentoActividadDocente();
		Integer getReglamentoActividad();
		
	} 
	
}
