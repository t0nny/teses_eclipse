package ec.edu.upse.acad.ws;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.upse.acad.model.pojo.reglamento.ActividadDocenteDetalle;
import ec.edu.upse.acad.model.pojo.reglamento.ActividadPersonalDocente;
import ec.edu.upse.acad.model.pojo.reglamento.Reglamento;
import ec.edu.upse.acad.model.pojo.reglamento.ReglamentoActividadDetalle;
import ec.edu.upse.acad.model.pojo.reglamento.ReglamentoActividadDocente;
import ec.edu.upse.acad.model.repository.distributivo.ActividadDetalleRepository;
import ec.edu.upse.acad.model.repository.distributivo.ActividadPersonalRepository;
import ec.edu.upse.acad.model.repository.distributivo.ReglamentoActividadDetalleRepository;
import ec.edu.upse.acad.model.repository.distributivo.ReglamentoActividadDocenteRepository;
import ec.edu.upse.acad.model.repository.distributivo.ReglamentoRepository;
import ec.edu.upse.acad.model.service.SecurityService;

@RestController
@RequestMapping("/api/reglamentos")
@CrossOrigin
public class ReglamentoController {

	@Autowired private ReglamentoRepository reglamentoRepository;
	@Autowired private ReglamentoActividadDocenteRepository reglamentoActividadDocenteRepository;
	@Autowired private ReglamentoActividadDetalleRepository reglamentoActividadDetalleRepository;
	@Autowired private ActividadPersonalRepository actividadPersonalRepository;
	@Autowired private ActividadDetalleRepository actividadDetalleRepository;
	@Autowired private SecurityService securityService;

	// ************************* SERVICIOS PARA REGLAMENTOS MATRICULA// *********************************************//

	//lista todos los reglamentos existentes
	@RequestMapping(value="/listarTodosReglamentos", method=RequestMethod.GET)
	public ResponseEntity<?> listarTodosReglamentos() {
		return ResponseEntity.ok(reglamentoRepository.listarTodosReglamentos());	
	}

	//Lista todos los tipos de reglamentos para cargar combo
	@RequestMapping(value="/listarTipoReglamentos", method=RequestMethod.GET)
	public ResponseEntity<?> listarTipoReglamentos() {
		return ResponseEntity.ok(reglamentoRepository.listarTipoReglamentos());
	}
	
	//recupera el json Reglamento y sus dependencias
	@RequestMapping(value="/recuperarReglamento/{idReglamento}", method=RequestMethod.GET)
	public ResponseEntity<?> recuperarReglamento(@PathVariable("idReglamento") Integer idReglamento) {

		Reglamento _reglamento;
		if (reglamentoRepository.findById(idReglamento).isPresent()) {
			_reglamento = reglamentoRepository.findById(idReglamento).get();
			Reglamento reglamento = new Reglamento();
			BeanUtils.copyProperties(_reglamento, reglamento, "reglamentoActividadDocentes","dedicacionHorasClases","reglamentoNumAsignaturas",
					"distributivoGenerales","mallas","mallaCompAprendizajes","mallaCompOrganizacions","matriculaGenerals","reglamentoTipoMatriculas",
					"validacionGenerals");
			return ResponseEntity.ok(reglamento);
		}else {
			return ResponseEntity.notFound().build();
		}
	}
	//Lista todos los tipos de reglamentos para cargar combo
	@RequestMapping(value="/listarReglamentoActividad/{idReglamento}", method=RequestMethod.GET)
	public ResponseEntity<?> listarTipoReglamentos(@PathVariable("idReglamento") Integer idReglamento) {
		return ResponseEntity.ok(reglamentoActividadDocenteRepository.listarActividadPersonal(idReglamento));
	}
	
	@RequestMapping(value="/grabarActividadPersonal", method=RequestMethod.POST)
	public ResponseEntity<?> grabarActividadPersonal(@RequestHeader(value="Authorization") String Authorization,
			@RequestBody ActividadPersonalDocente actividadPersonalDocente) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		System.err.println(actividadPersonalDocente.getId());
		ActividadPersonalDocente _actividadPersonalDocente = new ActividadPersonalDocente();
		BeanUtils.copyProperties(actividadPersonalDocente, _actividadPersonalDocente,"reglamentoActividadDetalle","actividadValor");
		actividadPersonalRepository.save(_actividadPersonalDocente);
		return ResponseEntity.ok(_actividadPersonalDocente);
	}

	@RequestMapping(value="/grabarReglamentoActividadDocente", method=RequestMethod.POST)
	public ResponseEntity<?> grabarReglamentoActividadDocente(@RequestHeader(value="Authorization") String Authorization,
			@RequestBody ReglamentoActividadDocente reglamentoActividadDocente) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
			
		ReglamentoActividadDocente _reglamentoActividadDocente = new ReglamentoActividadDocente();
		BeanUtils.copyProperties(reglamentoActividadDocente, _reglamentoActividadDocente,"reglamentoActividadDetalles");
		reglamentoActividadDocenteRepository.save(_reglamentoActividadDocente);
		return ResponseEntity.ok(_reglamentoActividadDocente);
	}
	
	@RequestMapping(value="/listarActividadDetalle/{idReglamento}", method=RequestMethod.GET)
	public ResponseEntity<?> listarActividadDetalle(@RequestHeader(value="Authorization") String Authorization,
			@PathVariable("idReglamento") Integer idReglamento) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(reglamentoActividadDetalleRepository.listarActividadDetalle(idReglamento));
	}
	
	@RequestMapping(value="/listarActividadDetallePorActPersonal/{idReglamentoActividad}", method=RequestMethod.GET)
	public ResponseEntity<?> listarActividadDetallePorActPersonal(@RequestHeader(value="Authorization") String Authorization,
			@PathVariable("idReglamentoActividad") Integer idReglamentoActividad) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build(); 
		}
		return ResponseEntity.ok(reglamentoActividadDetalleRepository.listarActividadDetallePorActPersonal(idReglamentoActividad));
	}
	
	@RequestMapping(value="/grabarReglamentoActividadDetalle", method=RequestMethod.POST)
	public ResponseEntity<?> grabarReglamentoActividadDocente(@RequestHeader(value="Authorization") String Authorization,
			@RequestBody List< ReglamentoActividadDetalle> reglamentoActividadDetalle) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		for (int i = 0; i < reglamentoActividadDetalle.size(); i++) {
			ReglamentoActividadDetalle _reglamentoActividadDetalle = new ReglamentoActividadDetalle();
			BeanUtils.copyProperties(reglamentoActividadDetalle.get(i), _reglamentoActividadDetalle);
			reglamentoActividadDetalleRepository.save(_reglamentoActividadDetalle);
		}
		
		return ResponseEntity.ok(reglamentoActividadDetalle);
	}
	@RequestMapping(value="/listarTodoActividadDetalle", method=RequestMethod.GET)
	public ResponseEntity<?> listarTodoActividadDetalle(@RequestHeader(value="Authorization") String Authorization) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build(); 
		}
		return ResponseEntity.ok(actividadDetalleRepository.listarTodoActividadDetalle());
	}
	
	@RequestMapping(value="/grabarActividadDocDetalle", method=RequestMethod.POST)
	public ResponseEntity<?> grabarActividadDocDetalle(@RequestHeader(value="Authorization") String Authorization,
			@RequestBody List< ActividadDocenteDetalle> actividadDocenteDetalle) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		for (int i = 0; i < actividadDocenteDetalle.size(); i++) {
			ActividadDocenteDetalle _actividadDocenteDetalle = new ActividadDocenteDetalle();
			BeanUtils.copyProperties(actividadDocenteDetalle.get(i), _actividadDocenteDetalle);
			actividadDetalleRepository.save(_actividadDocenteDetalle);
		}
		
		return ResponseEntity.ok(actividadDocenteDetalle);
	}
	
	
	
}
