package ec.edu.upse.acad.model.pojo;


import java.sql.Timestamp;

import javax.persistence.*;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="aca", name="asignatura_organizacion")
@Where(clause = "estado='A'")
@NoArgsConstructor

public class AsignaturaOrganizacion{

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_asignatura_organizacion")
	@Getter @Setter private Integer id;

	@Getter @Setter private String estado;
	
//	@Column(name="fecha_ingreso")
//	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;
	
	@Column(name="id_malla_asignatura")
	@Getter @Setter private Integer idMallaAsignatura;
	
	@Column(name="id_comp_organizacion")
	@Getter @Setter private Integer idCompOrganizacion;
	
	
   //RELACIONES
	//bi-directional many-to-one association to ComponenteOrganizacion
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_comp_organizacion", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private ComponenteOrganizacion componenteOrganizacion;

	//bi-directional many-to-one association to MallaAsignatura
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_malla_asignatura", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private MallaAsignatura mallaAsignatura;

	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }
}