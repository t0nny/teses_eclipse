package ec.edu.upse.acad.model.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.upse.acad.model.pojo.Asignatura;
import ec.edu.upse.acad.model.pojo.Malla;
import ec.edu.upse.acad.model.pojo.Oferta;
import ec.edu.upse.acad.model.pojo.OfertaAsignatura;
import ec.edu.upse.acad.model.pojo.planificacion_docente.PlanClase;
import ec.edu.upse.acad.model.repository.AsignaturaRepository;
import ec.edu.upse.acad.model.repository.MallaRepository;
import ec.edu.upse.acad.model.repository.OfertaAsignaturaRepository;
import ec.edu.upse.acad.model.repository.OfertaAsignaturaRepository.CustomObjectOfertaAsignatura;

@Service
@Transactional
public class AsignaturaService {

	@Autowired
	private AsignaturaRepository asignaturaRepository;
	@Autowired
	private OfertaAsignaturaRepository ofertaAsignaturaRepository;

	@PersistenceContext
	private EntityManager em;

	/**
	 * Elimina una asignatura de la entidad Asignatura y OfertaAsignatura cambiando
	 * el estado a 'I' y estableciendo la fechaHasta con la fecha actual del
	 * servidor.
	 * 
	 * @param idAsignatura Identificador de Asignatura
	 */
	public void borraAsignatura(Integer idAsignatura) {
		try {
			Asignatura asignatura = asignaturaRepository.findById(idAsignatura).get();
			java.util.Date fecha = new java.util.Date();
			Date fecha2 = new Date(fecha.getTime());

			if (asignatura != null) {
				List<OfertaAsignatura> ofetaAsignatura = new ArrayList<>();
				for (OfertaAsignatura _ofertaAsignatura : asignatura.getOfertaAsignaturas()) {
					OfertaAsignatura __ofertaAsignatura = ofertaAsignaturaRepository.findById(_ofertaAsignatura.getId())
							.get();
					if (__ofertaAsignatura != null) {
						__ofertaAsignatura.setEstado("I");
					}
					ofetaAsignatura.add(__ofertaAsignatura);
				}

				if (asignatura.getFechaHasta() == null) {
					asignatura.setFechaHasta(fecha2);
				}
				asignatura.setOfertaAsignaturas(ofetaAsignatura);
				asignatura.setEstado("I");
				asignaturaRepository.save(asignatura);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Método que identifica si la asignatura es nueva o es una edición
	 * 
	 * @param asignatura Objeto de tipo Asignatura recibido desde el front-end con
	 *                   los datos para guardar
	 */
	public void grabaAsignaturaOferta(Asignatura asignatura) {
		try {
//			if (asignatura.getId() != null) {
//				this.editAsignaturaOferta(asignatura);
//			} else {
//				// this.newAsignaturaOferta(asignatura);
				this.nuevaAsignatura(asignatura);
//			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Edita una asignatura
	 * 
	 * @param asignatura Objeto de tipo Asignatura recibido desde el front-end con
	 *                   los datos para guardar
	 */
	public void editAsignaturaOferta(Asignatura asignatura) {
		try {
			Asignatura __asignatura = asignaturaRepository.findById(asignatura.getId()).get();
			if (__asignatura != null) {
				__asignatura.setCodigo(asignatura.getCodigo().trim());
				__asignatura.setTipoAsignatura(asignatura.getTipoAsignatura());
				__asignatura.setUsuarioIngresoId(asignatura.getUsuarioIngresoId());
				__asignatura.setDescripcion(asignatura.getDescripcion().trim());
				__asignatura.setDescripcionCorta(asignatura.getDescripcionCorta().trim());
				__asignatura.setFechaDesde(asignatura.getFechaDesde());
				if (asignatura.getFechaHasta() != null) {
					__asignatura.setFechaHasta(asignatura.getFechaHasta());
				}
				asignaturaRepository.saveAndFlush(__asignatura);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Crea una nueva asignatura y las relaciona con un determinado número de
	 * ofertas para la inserción de una nueva ofertaAsignatura
	 * 
	 * @param asignatura Objeto de tipo Asignatura recibido desde el front-end con
	 *                   los datos para guardar
	 */
	public void nuevaAsignatura(Asignatura asignatura) {
		Asignatura _asignatura = new Asignatura();
		BeanUtils.copyProperties(asignatura, _asignatura, "ofertaAsignatura", "mallaAsignaturas",
				"compatibilidadAsignaturas1", "compatibilidadAsignaturas2");
		if (asignatura.getFechaHasta() != null) {
			_asignatura.setFechaHasta(asignatura.getFechaHasta());
		}
		List<OfertaAsignatura> ofertaAsignatura = new ArrayList<>();
		for (OfertaAsignatura _ofertaAsignatura : asignatura.getOfertaAsignaturas()) {
			Oferta _oferta = asignaturaRepository.findByIdOferta(_ofertaAsignatura.getOferta().getId());
			Oferta oferta = new Oferta();
			BeanUtils.copyProperties(_oferta, oferta, "departamentoOfertas", "ofertaDocentes", "ofertaAsignatura",
					"tipoOferta");
			OfertaAsignatura __ofertaAsignatura = new OfertaAsignatura();
			__ofertaAsignatura.setOferta(oferta);
			__ofertaAsignatura.setAsignatura(_asignatura);
			__ofertaAsignatura.setUsuarioIngresoId(_ofertaAsignatura.getUsuarioIngresoId());
			ofertaAsignatura.add(__ofertaAsignatura);
		}
		_asignatura.setOfertaAsignaturas(ofertaAsignatura);
		asignaturaRepository.save(_asignatura);
//		em.refresh(_asignatura);
		em.getEntityManagerFactory().getCache().evict(Asignatura.class);
	}

	public Asignatura filterFindAsignaturaById(Integer idAsignatura) {
		Asignatura a = asignaturaRepository.findById(idAsignatura).get();
		a.setCompatibilidadAsignaturas1(null);
		a.setCompatibilidadAsignaturas2(null);
		a.setListaMallasAsignaturas(a.getMallaAsignaturas().size());
		a.setMallaAsignaturas(null);
		if (a.getOfertaAsignaturas().size() > 0) {
			for (OfertaAsignatura ofertaAsignatura : a.getOfertaAsignaturas()) {
				Asignatura asi = new Asignatura();
				BeanUtils.copyProperties(ofertaAsignatura.getAsignatura(), asi, "ofertaAsignaturas", "mallaAsignaturas",
						"compatibilidadAsignaturas1", "compatibilidadAsignaturas2");
				ofertaAsignatura.setAsignatura(asi);
				Oferta ofer = new Oferta();
				BeanUtils.copyProperties(ofertaAsignatura.getOferta(), ofer, "departamentoOfertas", "ofertaDocentes",
						"ofertaAsignatura");
				ofertaAsignatura.setOferta(ofer);
			}
		}
		return a;
	}

	public List<CustomObjectOfertaAsignatura> buscarOfertaAsignaturas(Integer idOferta) {
		List<CustomObjectOfertaAsignatura> coOfertaAsignatura = ofertaAsignaturaRepository.buscarOfertaAsignaturas(idOferta);
		List<CustomObjectOfertaAsignatura> OferAsi = new ArrayList<>();
		for (CustomObjectOfertaAsignatura _oferAsig : coOfertaAsignatura) {
			if(!_oferAsig.getCodigo().trim().contains("COM")) {
				OferAsi.add(_oferAsig);
			}
		}
		return OferAsi;
	}
	
	public List<Asignatura> buscarOfertaAsignaturasComun() {
		List<Asignatura> a = asignaturaRepository.findAll();
		List<Asignatura> newAsignatura = new ArrayList<>();
		for (Asignatura asignatura : a) {
			if (!asignatura.getEstado().trim().equals("I")) {
				String codigo = asignatura.getCodigo().trim();
				String com = "COM"; 
				if(codigo.contains(com)) {
					asignatura.setCompatibilidadAsignaturas1(null);
					asignatura.setCompatibilidadAsignaturas2(null);
					asignatura.setMallaAsignaturas(null);
					if (asignatura.getOfertaAsignaturas().size() > 0) {
						for (OfertaAsignatura oferAsi : asignatura.getOfertaAsignaturas()) {
							Asignatura asi = new Asignatura();
							BeanUtils.copyProperties(oferAsi.getAsignatura(), asi, "ofertaAsignatura", "mallaAsignaturas",
									"compatibilidadAsignaturas1", "compatibilidadAsignaturas2");
							oferAsi.setAsignatura(asi);
							Oferta ofer = new Oferta();
							BeanUtils.copyProperties(oferAsi.getOferta(), ofer, "departamentoOfertas", "ofertaDocentes",
									"ofertaAsignatura");
							oferAsi.setOferta(ofer);
						}
					}
					newAsignatura.add(asignatura);
				}
			}
		}
		return newAsignatura;
	}
	/**
	 * Graba una nueva asignatura o la actualiza
	 * **/
	public void grabaAsignatura(Asignatura asignatura_) {
		Integer idAsignatura = asignatura_.getId();
		
		if (idAsignatura == null) {
			Asignatura asignatura = new Asignatura();
			BeanUtils.copyProperties(asignatura_, asignatura, "ofertaAsignaturas", "mallaAsignaturas","compatibilidadAsignaturas1", "compatibilidadAsignaturas2");	
			idAsignatura = asignaturaRepository.saveAndFlush(asignatura).getId();
		}

		for (int i = 0; i < asignatura_.getOfertaAsignaturas().size(); i++) {
			asignatura_.getOfertaAsignaturas().get(i).setIdAsignatura(idAsignatura);
		}
//		asignatura_.setId(idAsignatura);
//		asignaturaRepository.save(asignatura_);
		Asignatura _asignatura = new Asignatura();
		_asignatura = asignaturaRepository.findById(idAsignatura).get();
		Integer version = _asignatura.getVersion();
		asignatura_.setId(idAsignatura);
		asignatura_.setVersion(version);
		asignaturaRepository.save(asignatura_);
		em.getEntityManagerFactory().getCache().evict(Asignatura.class);		
	}
}
