package ec.edu.upse.acad.model.repository.matricula;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ec.edu.upse.acad.model.pojo.matricula.Movilidad;

@Repository
public interface MovilidadRepository  extends JpaRepository<Movilidad, Integer>{
	
	
  Movilidad findByIdEstudianteOfertaAndIdSubtipoMovilidad(Integer idEstudianteOferta,Integer idSubtipoMovilidad);
  
}
