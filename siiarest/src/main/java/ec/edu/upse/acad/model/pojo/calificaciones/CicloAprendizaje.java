package ec.edu.upse.acad.model.pojo.calificaciones;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ec.edu.upse.acad.model.pojo.ReglamentoCompAprendizaje;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="aca", name="ciclo_aprendizaje")
@NoArgsConstructor
public class CicloAprendizaje {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_ciclo_aprendizaje")
	@Getter @Setter private Integer id;
	
	@Column(name="id_reglamento_ciclo")
	@Getter @Setter private Integer idReglamentoCiclo;
	
	@Column(name="id_reglamento_comp_aprendizaje")
	@Getter @Setter private Integer idReglamentoCompAprendizaje;
	
	@Getter @Setter private Integer ponderacion_calificacion;

	@Getter @Setter private String estado;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;
	
	//bi-directional many-to-one association to Reglamento
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_reglamento_ciclo" , insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private ReglamentoCiclo reglamentoCiclo;
	
	//bi-directional many-to-one association to Reglamento
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_reglamento_comp_aprendizaje" , insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private ReglamentoCompAprendizaje reglamentoCompAprendizaje;

	
	@PrePersist
	void preInsert() {
	   if (this.estado == null)
	       this.estado = "A";
	}
}
