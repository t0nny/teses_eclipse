package ec.edu.upse.acad.model.repository.evaluacion;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.evaluacion.DetEvaluacionDocente;
@Repository
public interface DetEvaluacionDocenteRepository extends JpaRepository<DetEvaluacionDocente, Integer>{

}
