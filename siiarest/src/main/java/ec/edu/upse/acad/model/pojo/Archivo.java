package ec.edu.upse.acad.model.pojo;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ec.edu.upse.acad.model.pojo.seguridad.Departamento;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Table(schema="aca", name="archivo")
@NoArgsConstructor
public class Archivo {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_archivo")
	@Getter @Setter private Integer id;

	@Getter @Setter private String autor;

	@Getter @Setter private String descripcion;

	@Getter @Setter private String directorio;

	@Getter @Setter private String estado;

	@Getter @Setter private Date fecha;

	@Getter @Setter private String nombre;

	@Column(name="num_documento")
	@Getter @Setter private String numDocumento;

	@Column(name="fecha_ingreso")
	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;
	
	@Column(name="id_tipo_archivo")
	@Getter @Setter private Integer idTipoArchivo;
	
	@Column(name="id_tipo_documento")
	@Getter @Setter private Integer idTipoDocumento;
	
	@Column(name="id_departamento")
	@Getter @Setter private Integer idDepartamento;
	
	
	//RELACIONES
	//bi-directional many-to-one association to Departamento
	@ManyToOne
	@JoinColumn(name="id_departamento", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Departamento departamento;

	//bi-directional many-to-one association to TipoArchivo
	@ManyToOne
	@JoinColumn(name="id_tipo_archivo", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private TipoArchivo tipoArchivo;

	//bi-directional many-to-one association to TipoDocumento
	@ManyToOne
	@JoinColumn(name="id_tipo_documento", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private TipoDocumento tipoDocumento;

	//bi-directional many-to-one association to ArchivoCategoria
	@OneToMany(mappedBy="archivo", cascade=CascadeType.ALL)
	@Getter @Setter private List<ArchivoCategoria> archivoCategorias;

	//bi-directional many-to-one association to ArchivoDistributivo
	@OneToMany(mappedBy="archivo", cascade=CascadeType.ALL)
	@Getter @Setter private List<ArchivoDistributivo> archivoDistributivos;

	//bi-directional many-to-one association to ArchivoDocente
	@OneToMany(mappedBy="archivo", cascade=CascadeType.ALL)
	@Getter @Setter private List<ArchivoDocente> archivoDocentes;
	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }
}