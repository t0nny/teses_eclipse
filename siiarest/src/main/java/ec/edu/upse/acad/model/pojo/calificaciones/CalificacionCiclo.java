package ec.edu.upse.acad.model.pojo.calificaciones;

import java.util.Date;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="aca", name="calificacion_ciclo")
@NoArgsConstructor
public class CalificacionCiclo {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_calificacion_ciclo")
	@Getter @Setter private Integer id;

	@Column(name="id_calificacion_general")
	@Getter @Setter private Integer idCalificacionGeneral;

	@Column(name="id_reglamento_ciclo")
	@Getter @Setter private Integer idReglamentoCiclo;

	@Column(name="fecha_desde")
	@Getter @Setter private Date fechaDesde;

	@Column(name="fecha_hasta")
	@Getter @Setter private Date fechaHasta;

	@Column(name="fecha_cierre_actas")
	@Getter @Setter private Date fechaCierreActas;

	@Getter @Setter private String estado;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;

	//bi-directional many-to-one association to Reglamento
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_calificacion_general" , insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private CalificacionGeneral calificacionGeneral;

	//bi-directional many-to-one association to Reglamento
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_reglamento_ciclo" , insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private ReglamentoCiclo reglamentoCiclo;

	//bi-directional one-to-many association to EstudianteCalificacion
	/*@OneToMany(mappedBy="calificacionCiclo")
	@Getter @Setter private List<EstudianteCalificacion> estudianteCalificaciones;*/

	
	@PrePersist
	void preInsert() {
		if (this.estado == null)
			this.estado = "A";
	}
}
