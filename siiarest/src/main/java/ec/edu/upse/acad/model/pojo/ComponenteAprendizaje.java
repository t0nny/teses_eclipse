package ec.edu.upse.acad.model.pojo;

import javax.persistence.*;

import org.hibernate.annotations.Where;

import ec.edu.upse.acad.model.pojo.calificaciones.EstudianteCalificacion;
import ec.edu.upse.acad.model.pojo.calificaciones.ReglamentosComponente;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(schema="aca", name="componente_aprendizaje")
@Where(clause = "estado='A'")
@NoArgsConstructor
public class ComponenteAprendizaje{

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_componente_aprendizaje")
	@Getter @Setter private Integer id;
	
	@Column(name="id_componente_aprendizaje_padre")
	@Getter @Setter private Integer idComponenteAprendizajePadre;

	@Getter @Setter private String codigo;
	
	@Getter @Setter private String abreviatura;
	
	@Getter @Setter private String descripcion;

	@Getter @Setter private String estado;
	
	@Getter @Setter private Integer orden;
	
	@Column(name="fecha_ingreso")
	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;

	
	//RELACIONES
	//bi-directional many-to-one association to ComponentesAsignatura
	@OneToMany(mappedBy="componenteAprendizaje", cascade=CascadeType.ALL)
	@Getter @Setter private List<AsignaturaAprendizaje> componentesAsignaturas;

	//bi-directional many-to-one association to MallaCompAprendizaje
	@OneToMany(mappedBy="componenteAprendizaje", cascade=CascadeType.ALL)
	@Getter @Setter private List<ReglamentoCompAprendizaje> mallaCompAprendizajes;
	
	//bi-directional many-to-one association to ReglamentosComponente
	@OneToMany(mappedBy="componenteAprendizaje", cascade=CascadeType.ALL)
	@Getter @Setter private List<ReglamentosComponente> reglamentosComponentes;

	//bi-directional one-to-many association to EstudianteRecalificacion
  	@OneToMany(mappedBy="componenteAprendizaje", cascade=CascadeType.ALL)
  	@Getter @Setter private List<EstudianteCalificacion> estudianteCalificaciones;


	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }
}