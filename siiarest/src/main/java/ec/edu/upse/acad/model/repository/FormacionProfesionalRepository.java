package ec.edu.upse.acad.model.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.FormacionProfesional;
import ec.edu.upse.acad.model.pojo.Malla;
import ec.edu.upse.acad.model.repository.UniversidadRepository.CustomObject;



@Repository
public interface FormacionProfesionalRepository extends JpaRepository<FormacionProfesional, Integer>{

	@Query(value=" SELECT u.id as idFormacion, u.titulo as formacion "+
			"FROM FormacionProfesional as u " ) 
	List<CustomObject> listarFormaciones();	
	interface CustomObject  { 
		Integer getIdFormacion(); 
		String getFormacion();

	}
	
	

}