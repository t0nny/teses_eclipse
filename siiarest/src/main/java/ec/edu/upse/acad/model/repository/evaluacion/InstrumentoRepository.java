/**
 * 
 */
package ec.edu.upse.acad.model.repository.evaluacion;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ec.edu.upse.acad.model.pojo.evaluacion.Instrumento;

/**
 * @author dengi
 *
 */
@Repository
public interface InstrumentoRepository extends JpaRepository<Instrumento, Integer>{
	//consulta para buscar las escalas
			@Query(value="SELECT i "
					+ "FROM Instrumento i "
					+ "WHERE i.estado = 'A' ")
			List<Instrumento> listarInstrumento();
}
