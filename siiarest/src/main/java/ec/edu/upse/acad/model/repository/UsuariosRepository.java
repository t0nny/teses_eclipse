package ec.edu.upse.acad.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.seguridad.Usuario;


@Repository
public interface UsuariosRepository   extends JpaRepository<Usuario, Integer>{
//comentario de prueba
	@Query(value="SELECT u "
			+ "FROM  Usuario as u "
			+ "WHERE u.usuario = ?1 "
			)
	Usuario findByUsuari(String usuario); //sql que busca por atributo

	//Usuario findByToken(String token);
	
	
	@Query(value="SELECT mo.id as id, mo.ambiente as ambiente, mo.nombre as nombre, mo.url as url "
			+ "FROM  Modulo as mo "
			+ "INNER JOIN ModuloRol as mr on mo.id.id=mr.modulo.id "
			+" INNER JOIN  ModuloRolUsuario as mru on mr.id=mru.moduloRol.id "
			+ "INNER JOIN Usuario as u on mru.usuario.id=u.id "
			+ "WHERE u.usuario = ?1 "
			)
	List<CustomObject> buscarUsuarioModulos(String usuario);
	
	interface CustomObject { 
	    Integer getId(); 
	    String getAmbiente(); 
	    String getNombre(); 
	    String getUrl(); 
	  
	} 
	
	
	@Query(value="SELECT r.id as idRol, r.codigo as codigo, r.nombre as nombre "
			+ "FROM  Modulo as mo "
			+ "INNER JOIN ModuloRol as mr on mo.id.id=mr.modulo.id "
			+" INNER JOIN  ModuloRolUsuario as mru on mr.id=mru.moduloRol.id "
			+" INNER JOIN Rol as r on mr.rol.id=r.id "
			+ "INNER JOIN Usuario as u on mru.usuario.id=u.id "
			+ "WHERE u.id = ?1  and mo.estado='AC' and mr.estado='AC' and mru.estado='AC' and r.estado='AC' and u.estado='AC'"
			)
	List<CustomObjectRol> buscarUsuarioRol(Integer usuario);
	
	interface CustomObjectRol { 
	    Integer getIdRol(); 
	    String getCodigo(); 
	    String getNombre(); 
	   
	  
	} 
	
	
	
	@Query(value="select o.id as idOpcion, o.nombre as nombreOpcion, o.url as urlOpcion, o.icono as iconoOpcion,o.orden as ordenPadre,o.padreId as idPadre , "+
	"case when (select count (  op1.padreId) from  Opcion op1 where op1.padreId=o.id and op1.estado='AC' )>0 then 'true' else 'false' end as hijo , r.id as idRol "+
			"from Modulo as mo "+
			"inner join ModuloRol as mr on mo.id=mr.modulo.id "+
			"inner join ModuloRolOpcion as mop on mr.id=mop.moduloRol.id  "+
			"inner join Opcion as o on mop.opcion.id=o.id "+
			"inner join Opcion as op on o.id=op.padreId "+
			"inner join Rol as r on mr.rol.id=r.id "+
			"inner join ModuloRolUsuario as mru on mr.id=mru.moduloRol.id "+
			"inner join Usuario as u on mru.usuario.id=u.id  "+
			"where mo.estado='AC' and mr.estado='AC' and mop.estado='AC' and o.estado='AC' "+
			" and op.estado='AC' and r.estado='AC' and mru.estado='AC' and u.estado='AC' and u.estado='AC' "+
			"and u.id=?1 "+
			"Group by o.id , o.nombre , o.url , o.icono ,o.orden, o.padreId , r.id "+
			"order by o.orden "
			)
	
	List<CustomObjectUsuarioO> buscarUsuarioOpcionPa(Integer idUsuario);
	
	 interface CustomObjectUsuarioO { 
	    Integer getIdOpcion(); 
	    String getNombreOpcion(); 
	    String getUrlOpcion(); 
	    String getIconoOpcion(); 
	    Integer getOrdenPadre(); 
	    Integer getIdPadre(); 
	    boolean getHijo();
	    Integer getIdRol(); 
	  
	  
	} 
	
	@Query(value="select o.id as idOpcionPadre, o.nombre as nombreOpcion, o.url as urlOpcion, o.icono as iconoOpcion ,o.orden as ordenPadre,"+
			"op.id as idOpcionHijo, op.nombre as nombreOpcionHijo, op.url as urlOpcionHijo, op.icono as iconoOpcionHijo, op.orden as ordenHijo "+
			"from Modulo as mo "+
			"inner join ModuloRol as mr on mo.id=mr.modulo.id "+
			"inner join ModuloRolOpcion as mop on mr.id=mop.moduloRol.id  "+
			"inner join Opcion as o on mop.opcion.id=o.id "+
			"inner join Opcion as op on o.id=op.padreId "+
			"inner join Rol as r on mr.rol.id=r.id "+
			"inner join ModuloRolUsuario as mru on mr.id=mru.moduloRol.id "+
			"inner join Usuario as u on mru.usuario.id=u.id  "+
			"where mo.estado='AC' and mr.estado='AC' and mop.estado='AC' and o.estado='AC' "+
			" and op.estado='AC' and r.estado='AC' and mru.estado='AC' and u.estado='AC' "+
			"and u.id=?1 "+
			"order by o.orden, op.orden "
			
			)
	List<CustomObjectUsuarioOp> buscarUsuarioRolOpcion(Integer idUsuario);
	
	 interface CustomObjectUsuarioOp { 
	    Integer getIdOpcionPadre(); 
	    String getNombreOpcion(); 
	    String getUrlOpcion(); 
	    Integer getOrdenPadre();
	    String getIconoOpcion(); 
	    Integer getIdOpcionHijo(); 
	    String getNombreOpcionHijo(); 
	    String getUrlOpcionHijo(); 
	    String getIconoOpcionHijo(); 
	    Integer getOrdenHijo();
	  
	} 
	
	
}


