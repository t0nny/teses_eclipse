package ec.edu.upse.acad.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ec.edu.upse.acad.model.pojo.Jornada;

public interface JornadaRepository extends JpaRepository<Jornada, Integer> {

}
