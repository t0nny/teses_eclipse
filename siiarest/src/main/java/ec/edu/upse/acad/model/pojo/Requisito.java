package ec.edu.upse.acad.model.pojo;


import javax.persistence.*;

import org.hibernate.annotations.Where;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(schema="aca", name="requisito")
@Where(clause = "estado='A'")
@NoArgsConstructor
public class Requisito  {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_requisito")
	@Getter @Setter private Integer id;

	@Getter @Setter private String abreviatura;

	@Getter @Setter private String descripcion;

	@Getter @Setter private String estado;
	
	@Column(name="fecha_ingreso")
	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;
	
	//RELACIONES
	
	//bi-directional many-to-one association to AsignaturaRequisito
	@OneToMany(mappedBy="requisito", cascade=CascadeType.ALL)
	@Getter @Setter private List<AsignaturaRequisito> asignaturaRequisitos;

	//bi-directional many-to-one association to MallaRequisito
	@OneToMany(mappedBy="requisito", cascade=CascadeType.ALL)
	@Getter @Setter private List<MallaRequisito> mallaRequisitos;

	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }

}