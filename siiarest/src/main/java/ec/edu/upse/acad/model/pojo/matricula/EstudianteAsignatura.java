package ec.edu.upse.acad.model.pojo.matricula;

import javax.persistence.*;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ec.edu.upse.acad.model.pojo.distributivo.DocenteAsignaturaAprend;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;



@Entity
@Table(schema="aca", name="estudiante_asignatura")
@Where(clause = "estado='A' or estado='X' or estado='R' or estado='P' or estado='Q'")
@NoArgsConstructor

public class EstudianteAsignatura {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_estudiante_asignatura")
	@Getter @Setter private Long id;

	@Column(name="id_docente_asignatura_aprend")
	@Getter @Setter private Integer idDocenteAsignaturaAprend;

	@Column(name="id_estudiante_matricula")
	@Getter @Setter private Long idEstudianteMatricula;

	@Column(name="id_costo_asignatura")
	@Getter @Setter private Integer idCostoAsignatura;
	
	@Column(name="codigo_estado_matricula")
	@Getter @Setter private String codigoEstadoMatricula;

	@Getter @Setter private String estado;
	
	@Getter @Setter private Boolean aprobado;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Column(name="valor_asignatura")
	@Getter @Setter private Double valorAsignatura;
	
//	@Column(name="fecha_ingreso")
//	@Getter @Setter private Timestamp fechaIngreso;

	@Version
	@Getter @Setter private int version;

	//RELACIONES
	//bi-directional many-to-one association to DocenteAsignaturaAprend
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_docente_asignatura_aprend", insertable=false, updatable = false)
	//@JsonIgnore
	@Getter @Setter private DocenteAsignaturaAprend docenteAsignaturaAprend;

	//bi-directional many-to-one association to EstudianteMatricula
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_estudiante_matricula", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private EstudianteMatricula estudianteMatricula;

	//bi-directional many-to-one association to CostoAsignature
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_costo_asignatura", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private CostoAsignatura costoAsignatura;
	
	//bi-directional many-to-one association to DetalleEstudianteAsignatura
	@OneToMany(mappedBy="estudianteAsignatura", cascade=CascadeType.ALL)
	@Getter @Setter private List<DetalleEstudianteAsignatura> detalleEstudianteAsignaturas;


	@PrePersist
	void preInsert() {
		if (this.estado == null) {
			this.estado = "A";
		}
		
//		if (this.fechaDesde == null) {
//			Date fechaDesde = new Date();
//			this.fechaDesde = fechaDesde;
//		}
	}


}