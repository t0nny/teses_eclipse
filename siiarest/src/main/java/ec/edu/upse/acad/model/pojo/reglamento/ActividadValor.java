package ec.edu.upse.acad.model.pojo.reglamento;

import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Table(schema="aca", name="actividad_valor")
@NoArgsConstructor

public class ActividadValor {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_actividad_valor")
	@Getter @Setter private Integer id;

	@Column(name="id_actividad_personal")
	@Getter @Setter private Integer idActividadPersonal;
	
	@Column(name="tipo_relacion_laboral")
	@Getter @Setter  private String tipoRelacionLaboral;
	
	@Column(name="valor_minimo")
	@Getter @Setter private Integer valorMinimo;
	
	@Column(name="valor_maximo")
	@Getter @Setter private Integer valorMaximo;
	
	@Column(name="unidad_medida")
	@Getter @Setter private String unidadMedida;
	
	@Column(name="horas_permitidas")
	@Getter @Setter private Integer horasPermitidas;

    @Getter @Setter  private String estado;
	
	@Column(name="usuario_ingreso_id")
	@Getter @Setter  private String usuarioIngresoId;
	
	@Version
	@Getter @Setter  private Integer version;
	

	//bi-directional many-to-one association to activida Personal
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_actividad_personal" , insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private ActividadPersonalDocente actividadPersonal;
	
	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }
}
