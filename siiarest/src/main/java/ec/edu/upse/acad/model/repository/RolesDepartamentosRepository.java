package ec.edu.upse.acad.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ec.edu.upse.acad.model.pojo.seguridad.RolDepartamento;


public interface RolesDepartamentosRepository 
extends JpaRepository<RolDepartamento, Integer>{
	RolDepartamento findById(String id);

	@Query(value="SELECT e "
			+ "FROM RolDepartamento AS e "
		//	+ "WHERE e.nombre LIKE ?1 "
		//	+ "OR e.descripcion LIKE ?1 "
			+ "WHERE e.rol_id = ?1 ")
	List<RolDepartamento> buscarRolesDepartamentos(Integer rol_id);
}
