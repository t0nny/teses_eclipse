package ec.edu.upse.acad.model.pojo.distributivo;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;
import javax.persistence.QueryHint;
import javax.persistence.SqlResultSetMapping;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@SqlResultSetMapping(
		name = "MappingSPDetalleActividadDocente",
		classes = @ConstructorResult(
				targetClass = SpDetalleActividadDocente.class,
				columns = {
						@ColumnResult(name = "id_docente_actividad"),
						@ColumnResult(name = "actividad"),
						@ColumnResult(name = "id_actividad_detalle"),
						@ColumnResult(name = "actividad_descripcion"),
						@ColumnResult(name = "valor")
						
				}
				)
		)

@NamedNativeQuery(name = "getSPDetalleActividadDocente",
resultClass = SpDetalleActividadDocente.class,
hints = { @QueryHint(name = "org.hibernate.callable", value = "true")},
resultSetMapping ="MappingSPDetalleActividadDocente", 
query = "{call aca.sp_detalle_actividad_docente(?1)}"

		)

@Entity
@NoArgsConstructor
public class SpDetalleActividadDocente {
	@Id
	@Getter @Setter private Integer id;
	@Column(name="id_docente_actividad")
	@Getter @Setter private Integer idDocenteActividad;
	@Column(name="actividad")
	@Getter @Setter private String actividad;
	@Column(name="id_actividad_detalle")
	@Getter @Setter private Integer idActividadDetalle;
	@Column(name="actividad_descripcion")
	@Getter @Setter private String actividadDescripcion;
	@Getter @Setter private Integer valor;


}
