package ec.edu.upse.acad.ws;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.upse.acad.model.pojo.PeriodoTipoOferta;
import ec.edu.upse.acad.model.repository.PeriodoTipoOfertaRepository;


@RestController
@RequestMapping("/api/periodotipooferta")
@CrossOrigin

public class PeriodoTipoOfertaController {
	@Autowired private PeriodoTipoOfertaRepository periodotipoofertaRepository;

	//****************** SERVICIOS PARA PERIODO TIPO OFERTA************************//

	//Buscar todos los Periodos Tipo Oferta, para listar en angular
	@RequestMapping(value="/buscarPeriodoTipoOferta", method=RequestMethod.GET)
	public ResponseEntity<?> buscarPeriodoTipoOferta() {
		return ResponseEntity.ok(periodotipoofertaRepository.buscarperiodotipooferta());
	}
	//servicio que actualiza o crea un nuevo Periodos Tipo Oferta	
	//buscar por el id del Periodo Tipo Oferta
	@RequestMapping(value="/buscarPeriodoTipoOfertaId/{idpertipoofer}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarPeriodoTipoOfertaId(@PathVariable("id") Integer id) {
		return ResponseEntity.ok(periodotipoofertaRepository.buscarperiodotipoofertaId(id));
	}

	//Servicio de grabar los Periodos Tipo Oferta
	@RequestMapping(value="/grabarPeriodoTipooferta", method=RequestMethod.POST)
	public ResponseEntity<?> grabarPeriodoTipooferta(@RequestBody PeriodoTipoOferta pertipooferta) {
		PeriodoTipoOferta _pertipooferta = new PeriodoTipoOferta();
		if (pertipooferta.getId() != null) {
			_pertipooferta = periodotipoofertaRepository.findById(pertipooferta.getId()).get();
		}
		BeanUtils.copyProperties(pertipooferta, _pertipooferta);
		periodotipoofertaRepository.save(_pertipooferta);
		return ResponseEntity.ok(_pertipooferta);
	}

	//Servicio de borrar los tipos de oferta
	@RequestMapping(value="/borrar/{idpertipofer}", method=RequestMethod.DELETE)
	public ResponseEntity<?> borrar(@PathVariable("idpertipofer") Integer id) {
		PeriodoTipoOferta PerTiOfer = periodotipoofertaRepository.findById(id).get();
		if (PerTiOfer !=null) {
			PerTiOfer.setEstado("I");
			periodotipoofertaRepository.save(PerTiOfer);
		}
		return ResponseEntity.ok().build();
	}
}
