package ec.edu.upse.acad.model.pojo.planificacion_docente;


import javax.persistence.*;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;



@Entity
@Table(schema="aca", name="contenidos")
@Where(clause = "estado='A'")
@NoArgsConstructor
public class Contenidos {
	//Pojo solo para consultas

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_contenidos")
	@Getter private Integer id;
	
	@Column(name="id_silabo")
	@Getter private Integer idSilabo;
	
//	@Column(name="id_componente_aprendizaje")
//	@Getter private Integer idComponenteAprendizaje;
	
	@Column(name="id_contenido_padre")
	@Getter private Integer idContenidoPadre;
	
	@Getter private Integer orden;
	
	@Column(name="resultado_aprendizaje")
	@Getter private String resultadoAprendizaje;
	
	@Getter private String descripcion;
	
	@Column(name="horas_doc")
	@Getter private Integer horasDocencia;
	
	@Column(name="horas_aea")
	@Getter private Integer horasAEA;
	
	@Column(name="horas_ta")
	@Getter private Integer horasTA;
	
	@Getter private String estado;
	
	@Column(name="usuario_ingreso_id")
	@Getter private Integer usuarioIngresoId;
		
	//RELACIONES
	@ManyToOne
	@JoinColumn(name="id_silabo", insertable = false, updatable = false)
	@JsonIgnore
	@Getter private Syllabus syllabus;

}
