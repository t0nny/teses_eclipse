package ec.edu.upse.acad.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.Itinerario;

@Repository
public interface ItinerarioRepository extends JpaRepository<Itinerario, Integer>{

}
