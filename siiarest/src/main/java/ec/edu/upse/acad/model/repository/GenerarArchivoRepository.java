package ec.edu.upse.acad.model.repository;

import java.util.List;


import org.springframework.data.repository.CrudRepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.DistributivoDocenteActividad;


@Repository
public interface GenerarArchivoRepository  extends JpaRepository<DistributivoDocenteActividad, Integer> {
	
	/*@Procedure(value  ="aca.sp_rpt_distributivo_docente_departamento_exportacion" )
	public  List<MallaAsignatura> consultaDistributivoDocente(@Param("pi_id_periodo_academico") Integer idPeriodoAcademico,@Param("pi_id_departamento") Integer idDepartamento,@Param("pi_id_Tipo_Oferta") Integer idTipoOferta);
	*/
	/* @Query(value="{call aca.sp_rpt_distributivo_docente_departamento_exportacion(63,5,2) }",  nativeQuery = true)
	 List<CustomObjectDist> consultaDistributivoDocente(Integer idPeriodoAcademico,Integer idDepartamento,Integer idTipoOferta);*/
	
	@Query(value="SELECT dda.id as id ,dda.identificacion as identificacion,dda.apellidosNombres as apellidosNombres,dda.genero as genero,"
			+ "dda.idDepartamentoDistributivo as idDepartamentoDistributivo,dda.departamentoDistributivo as departamentoDistributivo,"
			+ "dda.idCarreraDistributivo as idCarreraDistributivo,dda.carreraDistributivo as carreraDistributivo,"
			+ "dda.idTipoOferta as idTipoOferta,dda.tipoOferta as tipoOferta,dda.dedicacion as dedicacion,"
			+ "dda.idDocenteDedicacion as idDocenteDedicacion,dda.categoria as categoria ,"
			+ " dda.idPeriodoAcademico as idPeriodoAcademico, dda.periodoAcademico as periodoAcademico, "
			+ "dda.idDistributivoDocente as idDistributivoDocente,dda.actividad as actividad,"
			+ "dda.actividadValor as actividadValor,dda.ofertaAsignatura as ofertaAsignmatura,"
			+ " dda.departamentoAsignatura as departamentoAsignatura,dda.asignatura as asignatura,"
			+ "dda.paralelo as paralelo,dda.numEstudiantes as numEstudiantes,dda.horasClases as horasClases,dda.docencia as docencia,dda.practicas as practicas"
			+ " FROM DistributivoDocenteActividad dda"
			+ " WHERE dda.idPeriodoAcademico=(?1) and dda.idTipoOferta=(?2)"
			+ " order by dda.idDepartamentoDistributivo, dda.apellidosNombres")
	 List<CustomObjectDist> consultaDistributivoDocente(Integer idPeriodo, Integer idTipoOferta);
	interface CustomObjectDist { 
		Integer getId();
		String getIdentificacion(); 
		String getApellidosNombres(); 
	    String getGenero();
	    Integer getIdDepartamentoDistributivo();
	    String getDepartamentoDistributivo();
	    Integer getIdCarreraDistributivo();
	    String getCarreraDistributivo();
	    Integer getIdTipoOferta();
	    String getTipoOfertaDistributivo();
	    String getDedicacion();
	    Integer getIdDocenteDedicacion();
	    String getCategoria();
	    Integer getIdPeriodoAcademico();
	    String getPeriodoAcademico();
	    Integer getIdDistributivoDocente();
	    String getActividad();
	    Integer getActividadValor();
	    String getOfertaAsignatura();
		String getDepartamentoAsignatura();
		String getAsignatura();
		String getParalelo();
		Integer getNumEstudiantes();
		Integer getHorasClases ();
		Integer getDocencia();
		Integer getPracticas();
	}
}
