package ec.edu.upse.acad.model.pojo.evaluacion;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="persona_cargo_funcion")
@NoArgsConstructor
public class PersonaCargoFuncion {	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_persona_cargo_funcion")
	@Getter @Setter private Integer id;
	
//	@Column(name="id_funcion_evaluacion")
//	@Getter @Setter private Integer idFuncionEvaluacion;
//	
//	@Column(name="id_persona_cargo")
//	@Getter @Setter private Integer idPersonaCargo;

	@Column(name="descripcion")
	@Getter @Setter private String descripcion;
	
	@Getter @Setter private String estado;
	
//	@Column(name="fecha_ingreso")
//	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;
	
	@Version
	@Getter @Setter private Integer version;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_funcion_evaluacion", nullable = false, insertable=false, updatable = false)
	//@JsonIgnore
	@Getter @Setter private FuncionEvaluacion funcionEvaluacion;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_persona_cargo", nullable = false, insertable=false, updatable = false)
	//@JsonIgnore
	@Getter @Setter private PersonaCargo personaCargo;
	
	@PrePersist
	void preInsert() {
		if (this.estado == null)
			this.estado = "A";
	}
}
