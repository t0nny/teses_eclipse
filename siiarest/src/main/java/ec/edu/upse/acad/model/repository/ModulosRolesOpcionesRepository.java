package ec.edu.upse.acad.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.seguridad.ModuloRolOpcion;


@Repository

public interface ModulosRolesOpcionesRepository
       extends JpaRepository<ModuloRolOpcion, Integer>{

	ModuloRolOpcion findById(String id);
	@Query(value="SELECT e "
			+ "FROM ModuloRolOpcion AS e "
		//	+ "join Opciones o "
		//	+ "join Modulo m "
			+ "WHERE e.moduloRol.id = ?1 ")
	List<ModuloRolOpcion> buscarModulosRolesOpciones(Integer modulo_rol_id);
}