package ec.edu.upse.acad.model.pojo.distributivo;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ec.edu.upse.acad.model.pojo.MallaAsignatura;
import ec.edu.upse.acad.model.pojo.Paralelo;
import ec.edu.upse.acad.model.pojo.PeriodoAcademico;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="aca", name="malla_asignatura_fusion")
@NoArgsConstructor
public class MallaAsignaturaFusion {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_malla_asignatura_fusion")
	@Getter @Setter private Integer id;

	@Getter @Setter private String estado;
	
	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;

	@Column(name="id_malla_asignatura")
	@Getter @Setter	private Integer idMallaAsignatura;
	
	@Column(name="id_paralelo")
	@Getter @Setter	private Integer idParalelo;
	
	@Column(name="id_periodo_academico")
	@Getter @Setter	private Integer idPeriodoAcademico;
	
	
	//RELACIONES
	//bi-directional many-to-one association to ComponenteAprendizaje
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_malla_asignatura", nullable = false, insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private MallaAsignatura mallaAsignatura;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_paralelo", nullable = false, insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Paralelo paralelo;
	
	//bi-directional many-to-one association to DistributivoDocente
	@OneToMany(mappedBy="mallaAsignaturaFusion", cascade=CascadeType.ALL)
	@Getter @Setter private List<DetalleMallaAsignaturaFusion> detalleMallaAsignaturaFusion;

	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_periodo_academico", nullable = false, insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private PeriodoAcademico periodoAcademico;
	

	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }


}
