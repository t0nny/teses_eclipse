package ec.edu.upse.acad.model.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.OfertaAsignatura;

@Repository
public interface OfertaAsignaturaRepository 

extends JpaRepository<OfertaAsignatura, Integer>{

	//listar ofertaAsignatura 
//		@Query(value="SELECT oa.id, o.id, o.descripcion, a.id, a.codigo, av.id, av.descripcion, "  
//				+ "a.tipoAsignatura, av.fechaDesde, av.fechaHasta " 
//				+ "FROM OfertaAsignatura AS oa " 
//				+ "JOIN Asignatura AS a ON oa.idAsignatura = a.id " 
//				+ "JOIN Oferta AS o ON oa.idOferta = o.id " 
//				+ "JOIN AsignaturaVersion AS av ON a.id = av.idAsignatura " 
//				+ "WHERE oa.estado = 'A' AND (av.fechaHasta is null OR av.fechaHasta > CURRENT_DATE)"
//				+ "ORDER BY o.descripcion"
//				)
//		List<CustomObjectOfertaAsignatura> listaOfertaAsignatura();
//
//		interface CustomObjectOfertaAsignatura { 
//			Integer getIdOferAsig();
//			Integer getIdOferta();
//			String getCarrera();
//			Integer getIdAsignatura();
//			String getCodigo();
//			Integer getIdAsignaturaVersion();
//			String getDescripcion();
//			String getTipo();
//			Date getfechaDesdeAsigVersion();
//			Date getFechaHastaAsigVersion();
//		}


//	@Query(value="SELECT a.id, a.codigo, av.id, av.descripcion, av.descripcionCorta, "
//			+ "oa.id, o.id, do.id, mv.id, m.id, mv.estado " 
//			+ "FROM OfertaAsignatura AS oa " 
//			+ "JOIN Asignatura AS a ON oa.idAsignatura = a.id " 
//			+ "JOIN AsignaturaVersion AS av ON a.id = av.idAsignatura " 
//			+ "JOIN Oferta AS o ON oa.idOferta = o.id "  
//			+ "JOIN DepartamentoOferta AS do ON o.id = do.idOferta " 
//			+ "JOIN Malla AS m ON m.idOferta = o.id "
//			+ "JOIN MallaVersion AS mv ON mv.idMalla = m.id "
//			+ "WHERE mv.estado = 'P' AND a.id=?1 " )
//	List<CustomObjectConsultaOfertaAsignatura> consultarOfertaAsignatura(Integer id, Integer idMod);
//	interface CustomObjectConsultaOfertaAsignatura {
//		Integer getIdAsignatura();
//		String getCodigo();
//		Integer getIdAsigVersion();
//		String getDescripcion();
//		String getDescripcionCorta();
//		Integer getIdOferAsig();
//		Integer getIdOferta();
//		//Integer getIdOferVersion();
//		Integer getIdDeparOferta();
//		//Integer getIdModalidad();
//		Integer getIdMallaVersion();
//		Integer getIdMalla();
//		String getEstadoMalla();
//	}

	@Query(value="SELECT ca "
			+ "FROM Oferta ca")
	List<OfertaAsignatura> buscarPorComponenteAprendizaje(String descripcion);

	//********************************** INICIO DE CONSULTAS USADAS EN EL MODULO DE ASIGNATURAS EN ANGULAR **********************************************
	//---------------------------------- CONSULTAS USADAS EN oferta-asignatura --------------------------------------------
		//LISTAR ASIGNATURAS 
		@Query(value="SELECT "
				+ "oa.id as idOfertaAsignatura, "
				+ "oa.oferta.id as idOferta, "
				+ "oa.oferta.descripcion as carrera, "
				+ "oa.asignatura.id as idAsignatura, "
				+ "oa.asignatura.codigo as codigo, "
				+ "oa.asignatura.descripcion as descripcion, "
				+ "oa.asignatura.descripcionCorta as descripcionCorta, "  
				+ "oa.asignatura.tipoAsignatura as tipo, "
				+ "oa.asignatura.fechaDesde as fechaDesde, "
				+ "oa.asignatura.fechaHasta as fechaHasta,"
				+ "oa.asignatura.estado as estado "  
				+ "FROM OfertaAsignatura oa " 
				+ "JOIN Asignatura a ON oa.asignatura.id = a.id " 
				+ "JOIN Oferta o ON oa.oferta.id = o.id " 
				+ "WHERE oa.oferta.id = ?1 AND oa.estado = 'A' AND (a.fechaHasta is null OR a.fechaHasta > current_date()) and (a.estado='A' or a.estado = 'N') "
				+ "ORDER BY o.descripcion"
				)
		List<CustomObjectOfertaAsignatura> buscarOfertaAsignaturas(Integer idOferta);
	
		interface CustomObjectOfertaAsignatura {
			Integer getIdOfertaAsignatura();
			Integer getIdOferta();
			String getCarrera();
			Integer getIdAsignatura();
			String getCodigo();
			String getDescripcion();
			String getDescripcionCorta();
			String getTipo();
			Date getFechaDesde();
			Date getFechaHasta();
			String getEstado();
		}
	
	//---------------------------------- CONSULTAS USADAS EN asignatura-vicerrectorado ------------------------------------
		//Listar ofertas-asignaturas comun
//				@Query(value="select distinct "
//						+ "a.codigo as codigo,"
//						+ "a.descripcion as asignatura,"
//						+ "a.tipoAsignatura as tipo,"
//						+ "a.id as idAsignatura,"
//						+ "oa.id as idOfertaAsignatura,"
//						+ "a.fechaDesde as fechaDesde,"
//						+ "a.fechaHasta as fechaHasta "
//						+ "FROM Asignatura a "
//						+ "JOIN OfertaAsignatura oa on a.id = oa.asignatura.id "
//						+ "JOIN Oferta o on o.id = oa.oferta.id "
//						+ "WHERE oa.estado = 'A' AND (a.fechaHasta is null OR a.fechaHasta > current_date()) AND a.estado = 'A' AND o.estado = 'A' "
//						+ "order by a.codigo ")
//				List<CO_ofertaAsignaturasComun> buscarOfertaAsignaturasComun();
//				interface CO_ofertaAsignaturasComun{
//					String getCodigo();
//					String getAsignatura();
//					String getTipo();
//					Integer getIdAsignatura();
//					Integer getIdOfertaAsignatura();
//					Date getFechaDesde();
//					Date getFechaHasta();
//				}

	//************************************** FIN DE CONSULTAS USADAS EN EL MODULO DE ASIGNATURAS EN ANGULAR ************************************************	
}



