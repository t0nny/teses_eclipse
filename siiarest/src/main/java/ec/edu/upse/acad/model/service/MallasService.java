package ec.edu.upse.acad.model.service;

import java.sql.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.upse.acad.model.pojo.AsignaturaAprendizaje;
import ec.edu.upse.acad.model.pojo.AsignaturaRelacion;
import ec.edu.upse.acad.model.pojo.ComponenteAprendizaje;
import ec.edu.upse.acad.model.pojo.ComponenteOrganizacion;
import ec.edu.upse.acad.model.pojo.DepartamentoOferta;
import ec.edu.upse.acad.model.pojo.Itinerario;
import ec.edu.upse.acad.model.pojo.ItinerarioMateria;
import ec.edu.upse.acad.model.pojo.Malla;
import ec.edu.upse.acad.model.pojo.MallaAsignatura;
import ec.edu.upse.acad.model.pojo.Modalidad;

import ec.edu.upse.acad.model.pojo.Nivel;

import ec.edu.upse.acad.model.pojo.PeriodoAcademico;
import ec.edu.upse.acad.model.pojo.PeriodoMalla;

import ec.edu.upse.acad.model.pojo.TipoCompOrganizacion;
import ec.edu.upse.acad.model.pojo.distributivo.DistributivoDedicacion;
import ec.edu.upse.acad.model.pojo.distributivo.DistributivoDocente;
import ec.edu.upse.acad.model.pojo.distributivo.DocenteActividad;
import ec.edu.upse.acad.model.pojo.distributivo.DocenteAsignaturaAprend;
import ec.edu.upse.acad.model.pojo.matricula.EstudianteAsignatura;
import ec.edu.upse.acad.model.pojo.matricula.EstudianteMatricula;
import ec.edu.upse.acad.model.pojo.matricula.EstudianteOferta;
import ec.edu.upse.acad.model.pojo.seguridad.Modulo;
import ec.edu.upse.acad.model.pojo.seguridad.Opcion;
import ec.edu.upse.acad.model.pojo.seguridad.Parametrica;
import ec.edu.upse.acad.model.pojo.seguridad.Persona;
import ec.edu.upse.acad.model.pojo.seguridad.Rol;
import ec.edu.upse.acad.model.repository.AsignaturasRelacionesRepository;
import ec.edu.upse.acad.model.repository.ComponenteAprendizajeRepository;
import ec.edu.upse.acad.model.repository.ComponenteOrganizacionRepository;
import ec.edu.upse.acad.model.repository.DepartamentoOfertaRepository;
import ec.edu.upse.acad.model.repository.ItinerarioMateriaRepository;
import ec.edu.upse.acad.model.repository.ItinerarioRepository;
import ec.edu.upse.acad.model.repository.MallaAsignaturaRepository;
import ec.edu.upse.acad.model.repository.MallaRepository;

import ec.edu.upse.acad.model.repository.ModalidadRepository;
import ec.edu.upse.acad.model.repository.ModulosRepository;
import ec.edu.upse.acad.model.repository.NivelRepository;
import ec.edu.upse.acad.model.repository.OpcionesRepository;
import ec.edu.upse.acad.model.repository.ParametricaRepository;
import ec.edu.upse.acad.model.repository.PeriodoAcademicoRepository;
import ec.edu.upse.acad.model.repository.PersonasRepository;
import ec.edu.upse.acad.model.repository.RolesRepository;
import ec.edu.upse.acad.model.repository.TipoCompOrganizacionRepository;

@Service
@Transactional
public class MallasService {

	@Autowired
	private PersonasRepository estudianteRepository;

	@PersistenceContext
	private EntityManager em;

	public void borraEstudiante(Integer id) {
		Persona persona = estudianteRepository.findById(id).get();
		if (persona != null) {
			persona.setEstado("X");
			estudianteRepository.save(persona);
			/* Codigo adicinal */
		}
	}

	@Autowired
	private ModulosRepository modulosRepository;

	public void borraModulo(Integer id) {
		Modulo modulos = modulosRepository.findById(id).get();
		if (modulos != null) {
			modulos.setEstado("IN");
			modulosRepository.save(modulos);

		}
	}

	@Autowired
	private RolesRepository rolRepository;

	public void borraRol(Integer id) {
		Rol rol = rolRepository.findById(id).get();
		if (rol != null) {
			rol.setEstado("IN");
			rolRepository.save(rol);

		}
	}

	@Autowired
	private OpcionesRepository opcionRepository;

	public void borraOpcion(Integer id) {
		Opcion opcion = opcionRepository.findById(id).get();
		if (opcion != null) {
			opcion.setEstado("IN");
			opcionRepository.save(opcion);

		}
	}

	@Autowired
	private TipoCompOrganizacionRepository tipocomporganizacionRepository;

	public void borraTipoCompOrganizacion(Integer id) {
		TipoCompOrganizacion tipocomporganizacion = tipocomporganizacionRepository.findById(id).get();
		if (tipocomporganizacion != null) {
			tipocomporganizacion.setEstado("I");
			tipocomporganizacionRepository.save(tipocomporganizacion);

		}
	}

	@Autowired
	private ComponenteOrganizacionRepository componenteorganizacionRepository;

	public void borraComponenteOrganizacion(Integer id) {
		ComponenteOrganizacion componenteorganizacion = componenteorganizacionRepository.findById(id).get();
		if (componenteorganizacion != null) {
			componenteorganizacion.setEstado("I");
			componenteorganizacionRepository.save(componenteorganizacion);

		}
	}

	@Autowired
	private ComponenteAprendizajeRepository componenteaprendizajeRepository;

	public void borraComponenteAprendizaje(Integer idComponenteAprendizaje) {
		ComponenteAprendizaje componenteaprendizaje = componenteaprendizajeRepository.findById(idComponenteAprendizaje)
				.get();
		if (componenteaprendizaje != null) {
			componenteaprendizaje.setEstado("I");
			componenteaprendizajeRepository.save(componenteaprendizaje);

		}
	}

	@Autowired
	private ParametricaRepository parametricaRepository;

	public void borraParametrica(Integer id) {
		Parametrica parametrica = parametricaRepository.findById(id).get();
		if (parametrica != null) {
			parametrica.setEstado("X");
			parametricaRepository.save(parametrica);

		}
	}

	@Autowired
	private PeriodoAcademicoRepository periodoacademicoRepository;

	public void borraPeriodoAcademico(Integer idPeriodoAcademico) {

		PeriodoAcademico periodoacademico = periodoacademicoRepository.findById(idPeriodoAcademico).get();
		if (periodoacademico != null) {
			periodoacademico.setEstado("I");
			periodoacademicoRepository.save(periodoacademico);
		}
	}

	// Funci�n para borrar itinerarios
	@Autowired
	private ItinerarioRepository itinerarioRepository;

	public void borrarItinerario(Integer idItinerario) {
		Itinerario itinerario = itinerarioRepository.findById(idItinerario).get();
		if (itinerario != null) {
			itinerario.setEstado("I");
			itinerarioRepository.save(itinerario);
		}

	}

	// Funci�n para borrar itinerarioMateria
	@Autowired
	private ItinerarioMateriaRepository itinerarioMateriaRepository;

	public void borrarItinerarioMateria(Integer idItinerarioMateria) {
		ItinerarioMateria itinerarioMateria = itinerarioMateriaRepository.findById(idItinerarioMateria).get();
		if (itinerarioMateria != null) {
			itinerarioMateria.setEstado("I");
			itinerarioMateriaRepository.save(itinerarioMateria);
		}

	}

	// Funci�n para borrar modalidad
	@Autowired
	private ModalidadRepository modalidadRepository;

	public void borrarModalidad(Integer idModalidad) {
		Modalidad modalidad = modalidadRepository.findById(idModalidad).get();
		if (modalidad != null) {
			modalidad.setEstado("I");
			modalidadRepository.save(modalidad);
		}

	}

	// Funci�n para borrar nivel
	@Autowired
	private NivelRepository nivelRepository;

	public void borrarNivel(Integer idNivel) {
		Nivel nivel = nivelRepository.findById(idNivel).get();
		if (nivel != null) {
			nivel.setEstado("I");
			nivelRepository.save(nivel);
		}

	}

	// Funci�n para borrar departamento_oferta
	@Autowired
	private DepartamentoOfertaRepository departamentoOfertaRepository;

	public void borrarDepartamentoOferta(Integer idDepartamentoOferta) {
		DepartamentoOferta departamentoOferta = departamentoOfertaRepository.findById(idDepartamentoOferta).get();
		if (departamentoOferta != null) {
			departamentoOferta.setEstado("I");
			departamentoOfertaRepository.save(departamentoOferta);
		}

	}

	// Funci�n para borrar Malla
	@Autowired
	private MallaRepository mallaRepository;

	public void borrarMalla(Integer idMalla) {
		Malla malla = mallaRepository.findById(idMalla).get();
		if (malla != null) {
			malla.setEstado("I");
			mallaRepository.save(malla);
		}

	}

	// Funci�n para borrar Malla Version
	@Autowired
	private MallaRepository mallaVersionRepository;

	public void borrarMallaVersion(Integer idMallaVersion) {
		Malla mallaVersion = mallaVersionRepository.findById(idMallaVersion).get();
		if (mallaVersion != null) {
			mallaVersion.setEstado("I");
			mallaVersionRepository.save(mallaVersion);
		}

	}

	@Autowired
	private MallaRepository mallaERepository;

	public void estadoMalla(Malla malla) {
		Malla malla_ = mallaVersionRepository.findById(malla.getId()).get();
		BeanUtils.copyProperties(malla, malla_, "periodoMallaVersion", "mallaRequisitos", "mallaAsignaturas", "estudianteOfertas");		
		if (malla_ !=null) {
			mallaERepository.save(malla_);
		}
		em.getEntityManagerFactory().getCache().evict(Malla.class);
	}
	
	public void eliminarMalla(Malla malla) {	
		Malla malla_ = mallaVersionRepository.findById(malla.getId()).get();
		BeanUtils.copyProperties(malla, malla_, "periodoMallaVersion", "mallaRequisitos", "mallaAsignaturas", "estudianteOfertas");		
		if (malla_ !=null) {
			if(malla_.getPeriodoMallaVersion().size()>0) {
				for (int i = 0; i < malla_.getPeriodoMallaVersion().size(); i++) {
					malla_.getPeriodoMallaVersion().get(i).setEstado("I");
				}
			}
			mallaERepository.save(malla_);
		}
		em.getEntityManagerFactory().getCache().evict(Malla.class);
	}

	// Funci�n para borrar MallaAsignatura
	@Autowired
	private MallaAsignaturaRepository mallaAsignaturaRepository;

	public void borrarMallaAsignatura(Integer idMallaAsignatura) {
		MallaAsignatura mallaAsignatura = mallaAsignaturaRepository.findById(idMallaAsignatura).get();
		if (mallaAsignatura != null) {
			mallaAsignatura.setEstado("I");
			mallaAsignaturaRepository.save(mallaAsignatura);
		}

	}

	// Servicio Graba/Edita y Elimina registros en la tabla asignatura_organizacion,tabla asignatura_aprendizaje,asignaturaRequisitos,asignaturaRelaciones
	public void grabaComponentesAsignatura(List<MallaAsignatura> listaMallaAsignatura) {
		System.out.println(listaMallaAsignatura);
		if (listaMallaAsignatura.size() > 0) {
			for (int i = 0; i < listaMallaAsignatura.size(); i++) {
				MallaAsignatura mallaAsignatura = listaMallaAsignatura.get(i);
				mallaAsignaturaRepository.save(mallaAsignatura);
			}
		}
	}
	
	/**
	 * Graba una nueva malla o actualiza
	 * **/
	@Autowired private MallaRepository mallaGrabaRepository;
	public void grabarMalla(Malla malla_) {
		Malla malla = new Malla();
		if (malla_.getId() != null) {
			malla = mallaGrabaRepository.findById(malla_.getId()).get();
		}
		BeanUtils.copyProperties(malla_, malla, "periodoMallaVersion", "mallaRequisitos", "mallaAsignaturas", "estudianteOfertas");		
		Integer IdMalla = mallaGrabaRepository.saveAndFlush(malla).getId();
		for (int i = 0; i < malla_.getPeriodoMallaVersion().size(); i++) {
			malla_.getPeriodoMallaVersion().get(i).setIdMalla(IdMalla);		
		}
		
		Malla _mallaC = new Malla();
		_mallaC = mallaGrabaRepository.findById(IdMalla).get();
		Integer version = _mallaC.getVersion();
		malla_.setId(IdMalla);
		malla_.setVersion(version);
		mallaGrabaRepository.save(malla_);
		em.getEntityManagerFactory().getCache().evict(Malla.class);		
	}

}
