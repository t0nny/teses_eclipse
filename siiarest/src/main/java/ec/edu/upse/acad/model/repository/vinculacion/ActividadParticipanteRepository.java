package ec.edu.upse.acad.model.repository.vinculacion;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.vinculacion.ActividadParticipante;

@Repository
public interface ActividadParticipanteRepository extends JpaRepository<ActividadParticipante, Integer> {
	@Transactional
	@Query(value = "SELECT ap.id as id,ap.idPersona as idPersona,ap.fechaDesde as fechaDesde,"
			+ " ap.fechaHasta as fechaHasta,ap.horasTrabajadas as horas,ap.lugar as lugar,ap.idTipoUsuarioProyecto"
			+ " as idTipoUsuarioProyecto, oa.descripcion as descripcionActividad,ot.id as idObjetivoTarea,"
			+ " ot.descripcion as descripcionTarea,(concat(per.nombres ,' ',per.apellidos))as nombres from ActividadParticipante ap"
			+ " INNER JOIN ObjetivoTarea ot on ap.idObjetivoTarea=ot.id "
			+ "INNER JOIN ObjetivoActividad oa on ot.idObjetivoActividad=oa.id "
			+ "INNER JOIN ItemMarcoLogico iml on oa.itemMarcoLogico.id=iml.id "
			+ "INNER JOIN Persona per on ap.idPersona=per.id "
			+ "WHERE iml.proyecto.id=(?1) AND iml.estado='A' AND ot.estado='A' AND per.estado='AC' AND ap.estado='A'")
	List<customObjetActividadesRealizadas> buscarActividadesRealizadas(Integer idProyecto);
	
	interface customObjetActividadesRealizadas {
		Integer getId();
		Integer getIdObjetivoTarea();
		Integer getIdPersona();
		Timestamp getFechaDesde();
		Timestamp getFechaHasta();
		Integer getHoras();
		String getLugar();
		Integer getIdTipoUsuarioProyecto();
		String getDescripcionActividad();		
		String getDescripcionTarea();	
		String getNombres();	
	}
	
	@Query(value = "SELECT ap.id as id,ap.idPersona as idPersona,ap.fechaDesde as fechaDesde,"
			+ " ap.fechaHasta as fechaHasta,ap.horasTrabajadas as horas,ap.lugar as lugar,ap.idTipoUsuarioProyecto"
			+ " as idTipoUsuarioProyecto, oa.id as idObjetivoActividad,ot.id as idObjetivoTarea,"
			+ " iml.id as idObjetivo,(concat(per.nombres ,' ',per.apellidos))as nombres,"
			+ "	ap.observaciones as observaciones,ap.documentoSoporteUrl as documentoSoporteUrl from ActividadParticipante ap"
			+ " INNER JOIN ObjetivoTarea ot on ap.idObjetivoTarea=ot.id "
			+ "INNER JOIN ObjetivoActividad oa on ot.idObjetivoActividad=oa.id "
			+ "INNER JOIN ItemMarcoLogico iml on oa.itemMarcoLogico.id=iml.id "
			+ "INNER JOIN Persona per on ap.idPersona=per.id "
			+ "WHERE ap.id=(?1) AND iml.estado='A' AND ot.estado='A' AND per.estado='AC' AND ap.estado='A'")
	List<customObjetEditarActividades> editarActividadesRealizadas(Integer idActividadParticipante);
	
	interface customObjetEditarActividades {
		Integer getId();
		Integer getIdObjetivoTarea();
		Integer getIdObjetivo();
		Integer getIdPersona();
		Timestamp getFechaDesde();
		Timestamp getFechaHasta();
		Integer getHoras();
		String getLugar();
		Integer getIdTipoUsuarioProyecto();
		String getIdObjetivoActividad();		
		String getObservaciones();
		String getDocumentoSoporteUrl();	
		String getNombres();	
	}
}
