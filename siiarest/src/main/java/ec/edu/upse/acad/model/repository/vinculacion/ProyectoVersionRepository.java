package ec.edu.upse.acad.model.repository.vinculacion;

import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ec.edu.upse.acad.model.pojo.vinculacion.ProyectoVersion;

@Repository
public interface ProyectoVersionRepository extends JpaRepository<ProyectoVersion, Integer>{
	
	@Transactional
	// OBTIENE EL ESTADO DEL PROYECTO VERSION 
	@Query(value = "select top 1 pv.estado_proyecto_version as estado " + "from vin.proyecto p "
			+ "inner join vin.proyecto_version pv on p.id_proyecto=pv.id_proyecto "
			+ "where p.id_proyecto=(:idProyecto) and p.estado='A' and pv.estado='A'"
			+ "order by pv.id_proyecto_version desc ", nativeQuery = true)
	public Optional<objetoProyectoVersion> buscarEstadoProyectoVersion(@Param("idProyecto") Integer idProyecto);

	interface objetoProyectoVersion {
		String getEstado();
	}
	
	@Query(value = "select top 1 pv.estado_proyecto_version as estado " + "from vin.proyecto p "
			+ "inner join vin.proyecto_version pv on p.id_proyecto=pv.id_proyecto "
			+ "where pv.id_proyecto_version=(:idProyectoVersion) and p.estado='A' and pv.estado='A'"
			+ "order by pv.id_proyecto_version desc ", nativeQuery = true)
	public Optional<objetoProyectoVersion1> buscarEstadoProyectoVersion1(@Param("idProyectoVersion") Integer idProyectoVersion);

	interface objetoProyectoVersion1 {
		String getEstado();
	}

}
