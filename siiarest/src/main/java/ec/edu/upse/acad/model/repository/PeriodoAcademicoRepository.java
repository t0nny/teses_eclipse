package ec.edu.upse.acad.model.repository;


import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ec.edu.upse.acad.model.pojo.PeriodoAcademico;

@Repository
public interface PeriodoAcademicoRepository extends JpaRepository<PeriodoAcademico, Integer> {
	@Transactional	
	@Query(value="SELECT pa "
			+ "FROM PeriodoAcademico AS pa "
			+ "WHERE pa.descripcion LIKE ?1 "
			+ "OR pa.codigo LIKE ?1 ")
	List<PeriodoAcademico> buscarPorPeriodoAcademico(String descripcion);
	

	@Query(value="SELECT pa.id as id, pa.idPeriodo as idPeriodo, pa.codigoTipoPeriodo as codigoTipoPeriodo,pa.codigo as codigo,pa.descripcion as descripcion, " +
			"pa.fechaDesde as fechaDesde, pa.fechaHasta as fechaHasta, pa.estado as estado "+
			"FROM PeriodoAcademico pa " + 
			"WHERE pa.estado='A' "+
			"ORDER BY pa.codigo desc")
	List<customObjetbuscarPeriodoAcademico> buscarPeriodoAcademico();
	interface customObjetbuscarPeriodoAcademico{
		Integer getId();
		Integer getIdPeriodo();
		String getCodigoTipoPeriodo();
		String getCodigo();
		String getDescripcion();
		Date getFechaDesde();
		Date getFechaHasta();
		String getEstado();
		
	}
	@Query(value="SELECT pa.id,pa.idPeriodo, pa.descripcion, pa.codigo, pa.estado  "
			+ "FROM PeriodoAcademico AS pa "
			+ "WHERE pa.id=(?1)  and pa.estado='A'")
	List<customObjetPeriodoAcademico> buscarDesPeriodoAcademico(Integer idPeriodo);
	interface customObjetPeriodoAcademico{
		Integer getIdPeriodoAcadmeico();
		Integer getIdPeriodo();
		String getCodigo();
		String getDescripcion();
		String getEstado();

	}
	
	
	@Query(value="SELECT pa.id,pa.idPeriodo, pa.descripcion, pa.codigo, pa.estado "
			+ "FROM PeriodoAcademico AS pa "
			+ "WHERE pa.id> ?1  and pa.estado='A'")
	List<customObjetbuscarPeriodoAcademicoDesde> buscarPorPeriodoAcademicoDesde(Integer descripcion);
	interface customObjetbuscarPeriodoAcademicoDesde{
		Integer getId();
		Integer getIdPeriodo();
		String getCodigo();
		String getDescripcion();
		String getEstado();
		
	}
	
	
	@Query(value= " select pa.id,pa.descripcion,pa.estado from PeriodoAcademico pa where  (GETDATE()>=pa.fechaDesde) and (GETDATE()<=pa.fechaHasta) " )
	List<customObjectrecuperarUltimoPeriodoVigente> recuperarUltimoPeriodoVigente();
	interface customObjectrecuperarUltimoPeriodoVigente {
		Integer getId();
		String getDescripcion();
		String getEstado();
	}
	
	@Query(value="SELECT pa.id,p.id,p.codigo,pa.codigo,p.fechaDesde,p.fechaHasta,p.estado,p.version " + 
			"FROM Periodo p " + 
			"INNER JOIN PeriodoAcademico pa on pa.idPeriodo = p.id " + 
			"where pa.estado = 'A' ")
	List<customObjetbuscarPeriodoAcademicoPeriodo> buscarPeriodoAcademicovsperiodo();
	interface customObjetbuscarPeriodoAcademicoPeriodo{
		Integer getId();
		Integer getIdPeriodo();
		String getCodigoPeriodo();
		String getCodigo();
		Date getFechaDesde();
		Date getFechaHasta();
	
		String getEstado();
		Integer getVersion();
	}
	
	//Consulta para cargar un nuevo periodo academico
	
	@Query(value="SELECT distinct pa.id,pa.idPeriodo,pa.descripcion,p.codigo,pa.fechaDesde,pa.fechaHasta,pa.extraordinario," +
			"m.id,m.descripcion,tof.id," +
			"tof.descripcion,r.idTipoReglamento,r.id,r.nombre, pa.estado from  PeriodoAcademico pa " + 
			"inner join Periodo p on pa.idPeriodo = p.id " +
			"inner join PeriodoModalidad pm on pm.idPeriodoAcademico = pa.id " + 
			"inner join Modalidad m on pm.idModalidad = m.id " + 
			"inner join DistributivoGeneral dg on dg.idPeriodoAcademico = pa.id " + 
			"inner join Reglamento r on dg.idReglamento = r.id " + 
			"inner join DistributivoOferta do on do.idDistributivoGeneralVersion = dg.id " + 
			"inner join Oferta o on do.idOferta = o.id " + 
			"inner join TipoOferta tof on o.idTipoOferta = tof.id " +
			"where pa.id = (?1) and pa.estado = 'A' ")
	List<customObjectNuevoPeriodoAcademicoPeriodo> NuevoPeriodoAcademicoModalidadTipOferta(Integer id);
	interface customObjectNuevoPeriodoAcademicoPeriodo{
		Integer getId();
		Integer getIdPeriodo();
		String getDescripcion();
		String getCodigo();
		Date getFechaDesde();
		Date getFechaHasta();
		Boolean getExtraordinario();
		Integer getIdModalidad();
		String getModalidadDescripcion();
		Integer getIdTipoOferta();
		String getTipoOfertaDescripcion();
		Integer getIdTipoReglamento();
		Integer getIdReglamento();
		String getIdRegNombre();
		String getEstado();
	}
	
	//consulta para Llenar combo
	@Query(value="SELECT pa.id,p.descripcion FROM Periodo p " + 
			"inner join PeriodoAcademico pa on pa.idPeriodo = p.id " + 
			"where pa.estado = 'A' and pa.fechaDesde > CURRENT_DATE ")
	List<customObjectfiltrarPeriodoAcademicoperiodo> filtrarperiodoacademico();
	interface customObjectfiltrarPeriodoAcademicoperiodo{
		Integer getId();
		String getDescripcion();
	}	
	
	//consulta ultimo id de periodo academico//
	@Query(value="SELECT pa.id as idPeridoAcademico FROM PeriodoAcademico pa "
			+ " WHERE pa.id=(SELECT MAX(pa.id) FROM PeriodoAcademico pa) "
			+ "GROUP BY pa.id ")
	List<PeriodoAcademico> ultimoPeriodoAcademico();

	
	//Procedimiento Almacenado de Periodo Academico afecta tablas distributivo_general, periodo_modalidad, periodo_tipo_oferta
	
	@Procedure(procedureName  ="GrabaPeriodoAcademicodmto")
	public  List<PeriodoAcademico> spPeriodoAcademico(@Param("idPeriodo") Integer idPeriodo,@Param("Codigo") String codigo,@Param("Extraordinario") Boolean extraordinario,@Param("FechaDesde") Date fechaDesde,
			@Param("FechaHasta") Date fechaHasta,@Param("idPeriodoAcademico") Integer id,@Param("Descripcion") String descripcion);
	
}
