package ec.edu.upse.acad.model.service;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.upse.acad.model.pojo.ActualizacionProfesional;
import ec.edu.upse.acad.model.pojo.Docente;
import ec.edu.upse.acad.model.pojo.DocenteHistorial;
import ec.edu.upse.acad.model.pojo.FormacionProfesional;
import ec.edu.upse.acad.model.pojo.Malla;
import ec.edu.upse.acad.model.pojo.MallaAsignatura;
import ec.edu.upse.acad.model.pojo.OfertaDocente;
import ec.edu.upse.acad.model.pojo.seguridad.ModuloRolUsuario;
import ec.edu.upse.acad.model.pojo.seguridad.Persona;
import ec.edu.upse.acad.model.pojo.seguridad.PersonaDepartamento;
import ec.edu.upse.acad.model.pojo.seguridad.Usuario;
import ec.edu.upse.acad.model.repository.ActualizacionProfesionalRepository;
import ec.edu.upse.acad.model.repository.DocenteHistorialRepository;
import ec.edu.upse.acad.model.repository.DocenteRepository;
import ec.edu.upse.acad.model.repository.FormacionProfesionalRepository;
import ec.edu.upse.acad.model.repository.MallaRepository;
import ec.edu.upse.acad.model.repository.ModulosRolesUsuariosRepository;
import ec.edu.upse.acad.model.repository.OfertaDocenteRepository;
import ec.edu.upse.acad.model.repository.PersonasDepartamentosRepository;
import ec.edu.upse.acad.model.repository.PersonasRepository;
import ec.edu.upse.acad.model.repository.UsuariosRepository;

@Service
@Transactional
public class PersonaService{
	
	
	
	@Autowired private PersonasRepository personaRepository;
	@Autowired private UsuariosRepository usuarioRepository;
	@Autowired private PersonasDepartamentosRepository personaDepartamentoReposirory;
	@Autowired private ModulosRolesUsuariosRepository moduloRolUsuarioRepository;
	@Autowired private MallaRepository mallaversionRepository;
	@Autowired private FormacionProfesionalRepository formacionProfesionalRepository;
	@Autowired private DocenteHistorialRepository docenteHistorialRepository;
	@Autowired private ActualizacionProfesionalRepository actualizacionProfesionalRepository;
	//@Autowired private ActividadesDocenciaRepository actividadesDocenciaRepository;
	@Autowired private DocenteRepository docenteRepository;
	@Autowired private OfertaDocenteRepository ofertaDocenteRepository;
	@PersistenceContext
	private EntityManager em;
	
		public void borraAsign(Integer id) {
				Malla mallaversi = mallaversionRepository.findById(id).get();
				if (mallaversi !=null) {
					mallaversi.setEstado("A");
					mallaversionRepository.save(mallaversi);
				
				
			}
		}
/*	
	public void borraPersona(Integer id) {
		Persona persona = personaRepository.findById(id).get();
		if (persona !=null) {
			persona.setEstado("IN");
			personaRepository.save(persona);
	
		}
	}
	
	public void borraUsuario(Integer id) {
		Usuario usuario = usuarioRepository.findById(id).get();
		if (usuario !=null) {
			usuario.setEstado("IN");
			usuarioRepository.save(usuario);
	
		}
	}
	
	
	public void grabaPersonaUsuario(Persona persona) {
				
	    if (persona.getId() != null) {
	      
			this.editPersonaUsuario(persona);
		}
		//caso contrario se trata de un registro nuevo
		else {
		 this.newPersonaUsuario(persona);
		}
	   
	   
	  }
	
	public void editPersonaUsuario(Persona persona) {
		//se intancia objeto auxiliar de personas
		Persona _persona = new Persona();
		//se instancia un objeto de BCryptPasswordEncoder para funciones de encriptado
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		//si id de persona es diferente de null actualiza registro;
		_persona = personaRepository.findById(persona.getId()).get();
		if (_persona.getUsuario()!=null) {
			//persona.getUsuario().setPersona_id(persona.getId());	
		if (persona.getUsuario().getClaveAuxiliar()!=null) {
			//para actualizar la clave en el caso de que este en null o sea diferente de la almacenada
			if ((_persona.getUsuario().getClave() == null)||(!encoder.matches(persona.getUsuario().getClaveAuxiliar(), _persona.getUsuario().getClave()))) {
				//encripta la clave
				persona.getUsuario().setClave(encoder.encode(persona.getUsuario().getClaveAuxiliar()));
			}
		 }else {
			
			 persona.getUsuario().setClave(_persona.getUsuario().getClave());
		 }
		 BeanUtils.copyProperties(persona, _persona);
		
		}
		
		else {
			if (persona.getUsuario().getClaveAuxiliar()!=null) {
				  persona.getUsuario().setClave(encoder.encode(persona.getUsuario().getClaveAuxiliar()));
			 	 }
			
			BeanUtils.copyProperties(persona, _persona,"usuario");  
			//personaRepository.save(_persona);
			persona.getUsuario().setPersona_id(persona.getId());
			Usuario _usuario = new Usuario();
			//_usuario.setPersona_id(persona.getId());
			BeanUtils.copyProperties(persona.getUsuario(), _usuario, "moduloRolUsuario");
			Integer usuario_id=usuarioRepository.saveAndFlush(_usuario).getId();
			 //em.getEntityManagerFactory().getCache().evict(Usuario.class);
			//iteracion para sincronizar relacion uno a varios con moduloRolUsuario
			for (int i = 0; i < persona.getUsuario().getModuloRolUsuario().size(); i++) {
			//	Integer modulo_rol_id = persona.getUsuario().getModuloRolUsuario().get(i).getModulo_rol_id();
				ModuloRolUsuario _moduloRolUsuario= new ModuloRolUsuario();
				//_moduloRolUsuario.setUsuario_id(usuario_id);
			//	_moduloRolUsuario.setModulo_rol_id(modulo_rol_id);
				moduloRolUsuarioRepository.saveAndFlush(_moduloRolUsuario);
			}
			em.refresh(_usuario);
						
		}
		personaRepository.saveAndFlush(_persona);
		em.getEntityManagerFactory().getCache().evict(Persona.class);
		
	}
	 
	public void newPersonaUsuario(Persona persona) {
		//se instancia un objeto de BCryptPasswordEncoder para funciones de encriptado
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		Persona _persona = new Persona();
		//encripto la clave
		if (persona.getUsuario()!=null) {
			 if (persona.getUsuario().getClaveAuxiliar()!=null) {
			  persona.getUsuario().setClave(encoder.encode(persona.getUsuario().getClaveAuxiliar()));
		 	 }
	        }
		//copia propiedades de persona en objeto auxuliar _persona excluye clase usuario para grabado de cabecera inicial
		BeanUtils.copyProperties(persona, _persona,"usuario","personaDepartamento");
		//afecto persona y obtengo id
		Integer persona_id=personaRepository.saveAndFlush(_persona).getId();
		persona.getUsuario().setPersona_id(persona_id);
		Usuario _usuario = new Usuario();
		BeanUtils.copyProperties(persona.getUsuario(), _usuario, "moduloRolUsuario");
		Integer usuario_id=usuarioRepository.saveAndFlush(_usuario).getId();
		//iteracion para sincronizar relacion una a varios con personaDepartamento
		
		for (int i = 0; i < persona.getPersonaDepartamento().size(); i++) {
			Integer departamento_id=persona.getPersonaDepartamento().get(i).getDepartamento_id();
			PersonaDepartamento _personaDepartamento=new PersonaDepartamento();
			_personaDepartamento.setPersona_id(persona_id);
			_personaDepartamento.setDepartamento_id(departamento_id);
			personaDepartamentoReposirory.saveAndFlush(_personaDepartamento);
		}
		
		//iteracion para sincronizar relacion uno a varios con moduloRolUsuario
		for (int i = 0; i < persona.getUsuario().getModuloRolUsuario().size(); i++) {
	//		Integer modulo_rol_id = persona.getUsuario().getModuloRolUsuario().get(i).getModulo_rol_id();
			ModuloRolUsuario _moduloRolUsuario= new ModuloRolUsuario();
			//_moduloRolUsuario.setUsuario_id(usuario_id);
			//_moduloRolUsuario.setModulo_rol_id(modulo_rol_id);
			moduloRolUsuarioRepository.saveAndFlush(_moduloRolUsuario);
		}
		em.refresh(_persona);	
	}

	


	

	*/
		
		public Persona buscarDocente(Integer idPersona) {
			Persona _persona = personaRepository.findById(idPersona).get();
			Persona persona = new Persona();
			BeanUtils.copyProperties(_persona, persona);
			//System.out.println(persona.getDocente());
			
			if(persona!=null) {
				return persona;
			}
			return persona;
		}	
		
		
		public void grabaPersonaDocente(Persona persona) {	
			if (persona.getId() != null) {  
				this.editarDocente(persona);
			}
			else {
				System.out.println("************************************NUEVO********************************************");
				
				this.nuevoDocente(persona);
			}
		}
		
		public void nuevoDocente(Persona persona) {
			Persona _persona = new Persona();
			BeanUtils.copyProperties(persona, _persona,
					"docente"
							);
			Integer idPersona=personaRepository.saveAndFlush(_persona).getId();
			persona.getDocente().setIdPersona(idPersona);
			Docente _docente = new Docente();
			BeanUtils.copyProperties(persona.getDocente(), _docente,"formacionProfesionales","docentesHistorial","actualizacionProfesionales","archivoDocentes","distributivoDocentes");
			Integer idDocente=docenteRepository.saveAndFlush(_docente).getId();
			
			if(persona.getDocente().getFormacionProfesionales()!=null) {
				for(int i=0 ; i<persona.getDocente().getFormacionProfesionales().size() ; i++) {
					
					persona.getDocente().getFormacionProfesionales().get(i).setIdDocente(idDocente);
					FormacionProfesional _formacionProfesional = new FormacionProfesional();
					BeanUtils.copyProperties(persona.getDocente().getFormacionProfesionales().get(i), _formacionProfesional);
					formacionProfesionalRepository.saveAndFlush(_formacionProfesional);
				}
			}
			if(persona.getDocente().getDocentesHistorial()!=null) {
				for(int i=0 ; i<persona.getDocente().getDocentesHistorial().size() ; i++) {
					DocenteHistorial _docenteHistorial = new DocenteHistorial();
					persona.getDocente().getDocentesHistorial().get(i).setIdDocente(idDocente);
					BeanUtils.copyProperties(persona.getDocente().getDocentesHistorial().get(i), _docenteHistorial);
					docenteHistorialRepository.saveAndFlush(_docenteHistorial);
				}
				
			}
			if(persona.getDocente().getActualizacionProfesionales()!=null) {
				for(int i=0 ; i<persona.getDocente().getActualizacionProfesionales().size() ; i++) {
					ActualizacionProfesional _actualizacionProfesional = new ActualizacionProfesional();
					persona.getDocente().getActualizacionProfesionales().get(i).setIdDocente(idDocente);
					BeanUtils.copyProperties(persona.getDocente().getActualizacionProfesionales().get(i), _actualizacionProfesional);
					actualizacionProfesionalRepository.saveAndFlush(_actualizacionProfesional);
				}
			}
			
			
			
			
			//iteracion para sincronizar relacion una a varios con estudianteOferta
			em.getEntityManagerFactory().getCache().evict(Persona.class);
			em.getEntityManagerFactory().getCache().evict(Docente.class);
			em.getEntityManagerFactory().getCache().evict(FormacionProfesional.class);
			em.getEntityManagerFactory().getCache().evict(DocenteHistorial.class);
			em.getEntityManagerFactory().getCache().evict(ActualizacionProfesional.class);
			
		}
		
		public void editarDocente(Persona persona) {
			System.out.println(persona.getDocente().getFormacionProfesionales());
			Persona _persona = new Persona();
			
			//si id de persona es diferente de null actualiza registro;
			_persona = personaRepository.findById(persona.getId()).get();
					
			if (_persona.getDocente()!=null) {
				BeanUtils.copyProperties(persona, _persona);
				
				if(persona.getDocente().getFormacionProfesionales()!=null) {
					for(int i=0; i<persona.getDocente().getFormacionProfesionales().size();i++) {
						FormacionProfesional _formacionProfesional = new FormacionProfesional();
						//si id de persona es diferente de null actualiza registro;
						if(persona.getDocente().getFormacionProfesionales().get(i).getId()>0) {
							_formacionProfesional = formacionProfesionalRepository.findById(persona.getDocente().getFormacionProfesionales().get(i).getId()).get();
							Integer id = persona.getDocente().getFormacionProfesionales().get(i).getId();
							Integer idDocente = persona.getDocente().getFormacionProfesionales().get(i).getIdDocente();
							Integer idUniversidad = persona.getDocente().getFormacionProfesionales().get(i).getIdUniversidad();
							Integer idNivelAcademico = persona.getDocente().getFormacionProfesionales().get(i).getIdNivelAcademico();
							Integer idAreaConocimiento = persona.getDocente().getFormacionProfesionales().get(i).getIdAreaConocimiento();
							String titulo = persona.getDocente().getFormacionProfesionales().get(i).getTitulo();
							Date fechaObtencion = persona.getDocente().getFormacionProfesionales().get(i).getFechaObtencion();
							String codigoSenescyt = persona.getDocente().getFormacionProfesionales().get(i).getCodigoSenescyt();
							Integer tiempoEstudio = persona.getDocente().getFormacionProfesionales().get(i).getTiempoEstudio();
							//String usuario = persona.getDocente().getFormacionProfesionales().get(i).getUsuarioIngresoId();
							Integer version = persona.getDocente().getFormacionProfesionales().get(i).getVersion();
							String estado = persona.getDocente().getFormacionProfesionales().get(i).getEstado();
							_formacionProfesional.setId(id);
							_formacionProfesional.setIdDocente(idDocente);
							_formacionProfesional.setIdUniversidad(idUniversidad);
							_formacionProfesional.setIdAreaConocimiento(idAreaConocimiento);
							_formacionProfesional.setIdNivelAcademico(idNivelAcademico);
							_formacionProfesional.setTitulo(titulo.trim());
							_formacionProfesional.setCodigoSenescyt(codigoSenescyt.trim());
							_formacionProfesional.setFechaObtencion(fechaObtencion);
							_formacionProfesional.setTiempoEstudio(tiempoEstudio);
							_formacionProfesional.setUsuarioIngresoId(persona.getDocente().getFormacionProfesionales().get(i).getUsuarioIngresoId());
							_formacionProfesional.setVersion(version);
							_formacionProfesional.setEstado(estado.trim());
							formacionProfesionalRepository.save(_formacionProfesional);	
						}
						
					}
				}
				//DOCENTE HISTORIAL
				if(persona.getDocente().getDocentesHistorial()!=null) {
					for(int i=0; i<persona.getDocente().getDocentesHistorial().size();i++) {
						DocenteHistorial _docenteHistorial = new DocenteHistorial();
						//si id de persona es diferente de null actualiza registro;
						if(persona.getDocente().getDocentesHistorial().get(i).getId()>0) {
							_docenteHistorial = docenteHistorialRepository.findById(persona.getDocente().getDocentesHistorial().get(i).getId()).get();
							Integer id = persona.getDocente().getDocentesHistorial().get(i).getId();
							Integer idDocente = persona.getDocente().getDocentesHistorial().get(i).getIdDocente();
							Integer idDocenteCategoria = persona.getDocente().getDocentesHistorial().get(i).getIdDocenteCategoria();
							Date fechaDesde = persona.getDocente().getDocentesHistorial().get(i).getFechaDesde();
							Date fechaHasta = persona.getDocente().getDocentesHistorial().get(i).getFechaHasta();
							String observacion = persona.getDocente().getDocentesHistorial().get(i).getObservacion();
						//	String usuario = persona.getDocente().getDocentesHistorial().get(i).getUsuarioIngresoId();
							Integer version = persona.getDocente().getDocentesHistorial().get(i).getVersion();
							String estado = persona.getDocente().getDocentesHistorial().get(i).getEstado();
							_docenteHistorial.setId(id);
							_docenteHistorial.setIdDocente(idDocente);
							_docenteHistorial.setIdDocenteCategoria(idDocenteCategoria);
							_docenteHistorial.setFechaDesde(fechaDesde);
							_docenteHistorial.setFechaHasta(fechaHasta);
							_docenteHistorial.setObservacion(observacion);
							_docenteHistorial.setUsuarioIngresoId(persona.getDocente().getDocentesHistorial().get(i).getUsuarioIngresoId());
							_docenteHistorial.setVersion(version);
							_docenteHistorial.setEstado(estado.trim());
							docenteHistorialRepository.save(_docenteHistorial);	
						
						}
						
					}
				}
				//DOCENTE HISTORIAL
				if(persona.getDocente().getActualizacionProfesionales()!=null) {
					for(int i=0; i<persona.getDocente().getActualizacionProfesionales().size();i++) {
						ActualizacionProfesional _actualizacionProfesional = new ActualizacionProfesional();
						//si id de persona es diferente de null actualiza registro;
						if(persona.getDocente().getActualizacionProfesionales().get(i).getId()>0) {
							_actualizacionProfesional = actualizacionProfesionalRepository.findById(persona.getDocente().getActualizacionProfesionales().get(i).getId()).get();
							Integer id = persona.getDocente().getActualizacionProfesionales().get(i).getId();
							Integer idDocente = persona.getDocente().getActualizacionProfesionales().get(i).getIdDocente();
							Integer idAreaConocimiento = persona.getDocente().getActualizacionProfesionales().get(i).getIdAreaConocimiento();
							String institucion = persona.getDocente().getActualizacionProfesionales().get(i).getInstitucion();
							String nombreCurso = persona.getDocente().getActualizacionProfesionales().get(i).getNombreCurso();
							String tipoEvento = persona.getDocente().getActualizacionProfesionales().get(i).getTipoEvento();
							String tipoCertificado = persona.getDocente().getActualizacionProfesionales().get(i).getTipoCertificado();
							Integer numeroDias = persona.getDocente().getActualizacionProfesionales().get(i).getNumeroDias();
							Integer numeroHoras  = persona.getDocente().getActualizacionProfesionales().get(i).getNumeroHoras();
							String tipoCapacitacion = persona.getDocente().getActualizacionProfesionales().get(i).getTipoCapacitacion();
							Date fechaDesde = persona.getDocente().getActualizacionProfesionales().get(i).getFechaDesde();
							Date fechaHasta = persona.getDocente().getActualizacionProfesionales().get(i).getFechaHasta();
							//String usuario = persona.getDocente().getActualizacionProfesionales().get(i).getUsuarioIngresoId();
							Integer version = persona.getDocente().getActualizacionProfesionales().get(i).getVersion();
							String estado = persona.getDocente().getActualizacionProfesionales().get(i).getEstado();
							
							_actualizacionProfesional.setId(id);
							_actualizacionProfesional.setIdDocente(idDocente);
							_actualizacionProfesional.setIdAreaConocimiento(idAreaConocimiento);
							_actualizacionProfesional.setInstitucion(institucion);
							_actualizacionProfesional.setNombreCurso(nombreCurso);
							_actualizacionProfesional.setTipoEvento(tipoEvento);
							_actualizacionProfesional.setTipoCertificado(tipoCertificado);
							_actualizacionProfesional.setNumeroDias(numeroDias);
							_actualizacionProfesional.setNumeroHoras(numeroHoras);
							_actualizacionProfesional.setTipoCapacitacion(tipoCapacitacion);
							_actualizacionProfesional.setFechaDesde(fechaDesde);
							_actualizacionProfesional.setFechaHasta(fechaHasta);
							_actualizacionProfesional.setUsuarioIngresoId(persona.getDocente().getActualizacionProfesionales().get(i).getUsuarioIngresoId());
							_actualizacionProfesional.setVersion(version);
							_actualizacionProfesional.setEstado(estado.trim());
							actualizacionProfesionalRepository.save(_actualizacionProfesional);
						}
						
					}
				}
				personaRepository.save(_persona);
			}
			else {
				BeanUtils.copyProperties(persona, _persona,"usuario","personaDepartamento","docente","estudiante","miembro","personasCargo"	);  
				persona.getDocente().setIdPersona(persona.getId());
				Docente _docente = new Docente();
				BeanUtils.copyProperties(persona.getDocente(), _docente,"formacionProfesionales"
						);
				//Integer idDocente=_docente.getId();
				Integer idDocente=docenteRepository.saveAndFlush(_docente).getId();
				//iteracion para sincronizar relacion uno a varios con moduloRolUsuario
				//iteracion para sincronizar relacion una a varios con estudianteOferta
				for (int i = 0; i < persona.getDocente().getFormacionProfesionales().size(); i++) {
					Integer id = persona.getDocente().getFormacionProfesionales().get(i).getId();
					Integer idUniversidad = persona.getDocente().getFormacionProfesionales().get(i).getIdUniversidad();
					Integer idNivelAcademico = persona.getDocente().getFormacionProfesionales().get(i).getIdNivelAcademico();
					Integer idAreaConocimiento = persona.getDocente().getFormacionProfesionales().get(i).getIdAreaConocimiento();
					String titulo = persona.getDocente().getFormacionProfesionales().get(i).getTitulo();
					Date fechaObtencion = persona.getDocente().getFormacionProfesionales().get(i).getFechaObtencion();
					String codigoSenescyt = persona.getDocente().getFormacionProfesionales().get(i).getCodigoSenescyt();
					Integer tiempoEstudio = persona.getDocente().getFormacionProfesionales().get(i).getTiempoEstudio();
					String usuario = persona.getDocente().getFormacionProfesionales().get(i).getUsuarioIngresoId();
					Integer version = persona.getDocente().getFormacionProfesionales().get(i).getVersion();
					if(id==0) {
						FormacionProfesional _formacionProfesional= new FormacionProfesional();
						_formacionProfesional.setId(id);
						_formacionProfesional.setIdDocente(idDocente);
						_formacionProfesional.setIdUniversidad(idUniversidad);
						_formacionProfesional.setIdAreaConocimiento(idAreaConocimiento);
						_formacionProfesional.setIdNivelAcademico(idNivelAcademico);
						_formacionProfesional.setTitulo(titulo);
						_formacionProfesional.setCodigoSenescyt(codigoSenescyt);
						_formacionProfesional.setFechaObtencion(fechaObtencion);
						_formacionProfesional.setTiempoEstudio(tiempoEstudio);
						_formacionProfesional.setUsuarioIngresoId(usuario);
						_formacionProfesional.setVersion(version);
						//formacionProfesionalRepository.save(_formacionProfesional);	
					}
					
				}	
				if(persona.getDocente().getActualizacionProfesionales()!=null) {
					for(int i=0; i<persona.getDocente().getActualizacionProfesionales().size();i++) {
						ActualizacionProfesional _actualizacionProfesional = new ActualizacionProfesional();
						//si id de persona es diferente de null actualiza registro;
						if(persona.getDocente().getActualizacionProfesionales().get(i).getId()>0) {
							//_actualizacionProfesional = actualizacionProfesionalRepository.findById(persona.getDocente().getActualizacionProfesionales().get(i).getId()).get();
							Integer id = persona.getDocente().getActualizacionProfesionales().get(i).getId();
					
							Integer idAreaConocimiento = persona.getDocente().getActualizacionProfesionales().get(i).getIdAreaConocimiento();
							String institucion = persona.getDocente().getActualizacionProfesionales().get(i).getInstitucion();
							String nombreCurso = persona.getDocente().getActualizacionProfesionales().get(i).getNombreCurso();
							String tipoEvento = persona.getDocente().getActualizacionProfesionales().get(i).getTipoEvento();
							String tipoCertificado = persona.getDocente().getActualizacionProfesionales().get(i).getTipoCertificado();
							Integer numeroDias = persona.getDocente().getActualizacionProfesionales().get(i).getNumeroDias();
							Integer numeroHoras  = persona.getDocente().getActualizacionProfesionales().get(i).getNumeroHoras();
							String tipoCapacitacion = persona.getDocente().getActualizacionProfesionales().get(i).getTipoCapacitacion();
							Date fechaDesde = persona.getDocente().getActualizacionProfesionales().get(i).getFechaDesde();
							Date fechaHasta = persona.getDocente().getActualizacionProfesionales().get(i).getFechaHasta();
							//String usuario = persona.getDocente().getActualizacionProfesionales().get(i).getUsuarioIngresoId();
							Integer version = persona.getDocente().getActualizacionProfesionales().get(i).getVersion();
							String estado = persona.getDocente().getActualizacionProfesionales().get(i).getEstado();
							
							_actualizacionProfesional.setId(id);
							_actualizacionProfesional.setIdDocente(idDocente);
							_actualizacionProfesional.setIdAreaConocimiento(idAreaConocimiento);
							_actualizacionProfesional.setInstitucion(institucion);
							_actualizacionProfesional.setNombreCurso(nombreCurso);
							_actualizacionProfesional.setTipoEvento(tipoEvento);
							_actualizacionProfesional.setTipoCertificado(tipoCertificado);
							_actualizacionProfesional.setNumeroDias(numeroDias);
							_actualizacionProfesional.setNumeroHoras(numeroHoras);
							_actualizacionProfesional.setTipoCapacitacion(tipoCapacitacion);
							_actualizacionProfesional.setFechaDesde(fechaDesde);
							_actualizacionProfesional.setFechaHasta(fechaHasta);
							_actualizacionProfesional.setUsuarioIngresoId(persona.getDocente().getActualizacionProfesionales().get(i).getUsuarioIngresoId());
							_actualizacionProfesional.setVersion(version);
							_actualizacionProfesional.setEstado(estado.trim());
							actualizacionProfesionalRepository.save(_actualizacionProfesional);
						}
						
					}
				}
				//DOCENTE HISTORIAL
				if(persona.getDocente().getDocentesHistorial()!=null) {
					for(int i=0; i<persona.getDocente().getDocentesHistorial().size();i++) {
						DocenteHistorial _docenteHistorial = new DocenteHistorial();
						//si id de persona es diferente de null actualiza registro;
						if(persona.getDocente().getDocentesHistorial().get(i).getId()>0) {
							
							Integer id = persona.getDocente().getDocentesHistorial().get(i).getId();
						
							Integer idDocenteCategoria = persona.getDocente().getDocentesHistorial().get(i).getIdDocenteCategoria();
							Date fechaDesde = persona.getDocente().getDocentesHistorial().get(i).getFechaDesde();
							Date fechaHasta = persona.getDocente().getDocentesHistorial().get(i).getFechaHasta();
							String observacion = persona.getDocente().getDocentesHistorial().get(i).getObservacion();
						//	String usuario = persona.getDocente().getDocentesHistorial().get(i).getUsuarioIngresoId();
							Integer version = persona.getDocente().getDocentesHistorial().get(i).getVersion();
							String estado = persona.getDocente().getDocentesHistorial().get(i).getEstado();
							_docenteHistorial.setId(id);
							_docenteHistorial.setIdDocente(idDocente);
							_docenteHistorial.setIdDocenteCategoria(idDocenteCategoria);
							_docenteHistorial.setFechaDesde(fechaDesde);
							_docenteHistorial.setFechaHasta(fechaHasta);
							_docenteHistorial.setObservacion(observacion);
							_docenteHistorial.setUsuarioIngresoId(persona.getDocente().getDocentesHistorial().get(i).getUsuarioIngresoId());
							_docenteHistorial.setVersion(version);
							_docenteHistorial.setEstado(estado.trim());
							docenteHistorialRepository.save(_docenteHistorial);	
						
						}
						
					}
				}
			}
			em.getEntityManagerFactory().getCache().evict(Persona.class);
			em.getEntityManagerFactory().getCache().evict(Docente.class);
			em.getEntityManagerFactory().getCache().evict(FormacionProfesional.class);
			}
		//Servicio grabar/editar y eliminar Componentes de Organizacion y Aprendizaje, ambos al mismo tiempo
		public void grabaOfertaDocente(List<OfertaDocente> listaOfertaDoc) {
			System.out.println(listaOfertaDoc);
			if (listaOfertaDoc.size()>0) {
				for (int i = 0; i < listaOfertaDoc.size(); i++) {
					OfertaDocente ofertaDocente = listaOfertaDoc.get(i);

					ofertaDocenteRepository.save(ofertaDocente);			
				}
			}
		}
}
	
	
	    

	
