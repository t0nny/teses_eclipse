package ec.edu.upse.acad.model.pojo.matricula;

import javax.persistence.*;

import org.eclipse.persistence.annotations.AdditionalCriteria;
import org.hibernate.annotations.Where;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.List;


@Entity
@Table(schema="aca", name="tipo_matricula")
@Where(clause = "estado='A'")
@NoArgsConstructor
public class TipoMatricula{

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_tipo_matricula")
	@Getter @Setter private Integer id;

	@Getter @Setter private String descripcion;
	
	@Getter @Setter private String codigo;

	@Getter @Setter private String estado;

//	@Column(name="fecha_ingreso")
//	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private int version;

	//RELACIONES
	//bi-directional many-to-one association to ReglamentoTipoMatricula
	@OneToMany(mappedBy="tipoMatricula", cascade=CascadeType.ALL)
	@Getter @Setter private List<ReglamentoTipoMatricula> reglamentoTipoMatriculas;

	//bi-directional many-to-one association to TipoMatriculaFecha
	@OneToMany(mappedBy="tipoMatricula", cascade=CascadeType.ALL)
	@Getter @Setter private List<TipoMatriculaFecha> tipoMatriculaFechas;

	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }
	 
	 
}