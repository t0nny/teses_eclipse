package ec.edu.upse.acad.ws;


import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.upse.acad.model.pojo.planificacion_docente.Syllabus;
import ec.edu.upse.acad.model.repository.planificacion_docente.SyllabusRepository;
import ec.edu.upse.acad.model.service.SyllabusService;

@RestController
@RequestMapping("/api/syllabus")
@CrossOrigin
public class SyllabusController {

	@Autowired private SyllabusRepository syllabusRepository;
	@Autowired private SyllabusService syllabusService;

	/**
	 * Servicio que me permite obtener la facultad a la cual este a cargo un usuario predeterminado.
	 * 
	 * @param idUsuario
	 * @return
	 */
	@RequestMapping(value = "/getFacultades/{idUsuario}", method = RequestMethod.GET)
	public ResponseEntity<?> getSyllabus(@PathVariable("idUsuario") Integer idUsuario){
		return ResponseEntity.ok(syllabusRepository.getFacultades(idUsuario));
	}

	/**
	 * Servicio que me permite obtener todas las carreras dependiendo de una facultad.
	 * 
	 * @param idDepartamento
	 * @return
	 */
	@RequestMapping(value = "/getOfertas/{idDepartamento}", method = RequestMethod.GET)
	public ResponseEntity<?> getOfertas(@PathVariable("idDepartamento") Integer idDepartamento){
		return ResponseEntity.ok(syllabusRepository.getOfertas(idDepartamento));
	}

	/**
	 * Servicio que me permite obtener las asignaturas pertenecientes a una carrera.
	 * 
	 * @param idOferta
	 * @return
	 */
	@RequestMapping(value = "/getAsignaturas/{idOferta}", method = RequestMethod.GET)
	public ResponseEntity<?> getAsignaturas(@PathVariable("idOferta") Integer idOferta){
		return ResponseEntity.ok(syllabusRepository.getAsignaturas(idOferta));
	}

	/**
	 * Servicio que me permite obtener una asignatura filtrando por la carrera a la que pertenece.
	 * 
	 * @param idAsignatura
	 * @param idOferta
	 * @return
	 */
	@RequestMapping(value = "/getAsiSyllabus/{idAsignatura}/{idOferta}", method = RequestMethod.GET)
	public ResponseEntity<?> getAsiSyllabus(@PathVariable("idAsignatura") Integer idAsignatura,
			@PathVariable("idOferta") Integer idOferta){
		return ResponseEntity.ok(syllabusRepository.getAsiSyllabus(idAsignatura,idOferta));
	}

	/**
	 * Servicio que muestra los detalles de la asignatura al momento de crear un sylabos
	 * 
	 * @param idMallaAsignatura
	 * @return
	 */
	@RequestMapping(value = "/getSilabus/{idMallaAsignatura}", method = RequestMethod.GET)
	public ResponseEntity<?> getSilabus(@PathVariable("idMallaAsignatura") Integer idMallaAsignatura) {
		return ResponseEntity.ok(syllabusRepository.getSilabus(idMallaAsignatura));
	}

	/**
	 * Graba o edita un syllabus
	 * 
	 * @param asignatura recibe un objeto de tipo Asignatura
	 * @return
	 */
	@RequestMapping(value="/grabarSyllabus", method=RequestMethod.POST)
	public ResponseEntity<?> grabarSyllabus(@RequestBody Syllabus syllabus) {
		syllabusService.grabarSyllabus(syllabus);
		return ResponseEntity.ok(syllabus);
	}


	/**
	 * Consulta si existe un Syllabus mediante un identificador
	 * 
	 * @param idSyllabus identificador de syllabus
	 * @return
	 */
	@RequestMapping(value = "/getSyllabusContenidos/{idSyllabus}", method = RequestMethod.GET)
	public ResponseEntity<?> getSyllabusContenidos( @PathVariable("idSyllabus") Integer idSyllabus){
		Syllabus syllabus = syllabusRepository.findById(idSyllabus).get();
		Syllabus __syllabus = new Syllabus();
		BeanUtils.copyProperties(syllabus, __syllabus, "contenido");
		return ResponseEntity.ok(__syllabus);
	}

}
