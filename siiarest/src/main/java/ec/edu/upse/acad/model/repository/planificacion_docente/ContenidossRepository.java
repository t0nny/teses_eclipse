package ec.edu.upse.acad.model.repository.planificacion_docente;

import org.springframework.data.jpa.repository.JpaRepository;

import ec.edu.upse.acad.model.pojo.planificacion_docente.Contenidos;

public interface ContenidossRepository extends JpaRepository<Contenidos, Integer>{

}
