package ec.edu.upse.acad.model.pojo.distributivo;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ec.edu.upse.acad.model.pojo.reglamento.ActividadDocenteDetalle;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="aca", name="docente_actividad")
@Where(clause = "estado='A'")
@NoArgsConstructor

public class DocenteActividad {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_docente_actividad")
	@Getter @Setter  private Integer id;
	
	@Column(name="id_distributivo_docente")
	@Getter @Setter  private Integer idDistributivoDocente;
	
	@Column(name="id_actividad_detalle")
	@Getter @Setter  private Integer idActividadDetalle;
	
	@Getter @Setter  private Integer valor;
	
    @Getter @Setter private String estado;
	
//	@Column(name="fecha_ingreso")
//	@Getter @Setter private Timestamp fechaIngreso;
	
	@Column(name="fecha_desde")
	@Getter @Setter private Date fechaDesde;
	@Column(name="fecha_hasta")
	@Getter @Setter private Date fechaHasta;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;
	
	@Version
	@Getter @Setter private Integer version;
	
	
	
	//RELACIONES
	 //bi-directional many-to-one association to Distributivo Docente
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_distributivo_docente" , insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private DistributivoDocente distributivoDocente;
	
	 //bi-directional many-to-one association to Actividad Personale
		@ManyToOne(fetch=FetchType.LAZY)
		@JoinColumn(name="id_actividad_detalle" , insertable=false, updatable = false)
		@JsonIgnore
		@Getter @Setter private ActividadDocenteDetalle actividadDocenteDetalle;
	
	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }

}
