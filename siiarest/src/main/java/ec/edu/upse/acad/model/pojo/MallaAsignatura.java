	package ec.edu.upse.acad.model.pojo;


import javax.persistence.*;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.util.List;

@Entity
@Table(schema="aca", name="malla_asignatura")
@Where(clause = "estado='A'")
@NoArgsConstructor

public class MallaAsignatura  {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_malla_asignatura")
	@Getter @Setter private Integer id;
	
	@Column(name="id_malla")
	@Getter @Setter private Integer idMalla;
	
	@Column(name="id_nivel")
	@Getter @Setter private Integer idNivel;
	
	@Column(name="id_asignatura")
	@Getter @Setter private Integer idAsignatura;

	@Column(name="num_horas")
	@Getter @Setter private Integer numHoras;
	
	@Column(name="num_creditos")
	@Getter @Setter private Integer numCreditos;
	
	@Getter @Setter private String estado;
	
	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;
	
	@Version
	@Getter @Setter private Integer version;

	//RELAIONES
	//bi-directional many-to-one association to AsignaturaOrganizacion
	@OneToMany(mappedBy="mallaAsignatura", cascade=CascadeType.ALL)
	@Getter @Setter private List<AsignaturaOrganizacion> asignaturaOrganizacions;

	//bi-directional many-to-one association to AsignaturaRelacione
	@OneToMany(mappedBy="mallaAsignatura", cascade=CascadeType.ALL)
	@Getter @Setter private List<AsignaturaRelacion> asignaturaRelaciones;

	//bi-directional many-to-one association to AsignaturaRequisito
	@OneToMany(mappedBy="mallaAsignatura", cascade=CascadeType.ALL)
	@Getter @Setter private List<AsignaturaRequisito> asignaturaRequisitos;

	//bi-directional many-to-one association to ComponentesAsignatura
	@OneToMany(mappedBy="mallaAsignatura", cascade=CascadeType.ALL)
	@Getter @Setter private List<AsignaturaAprendizaje> asignaturaAprendizaje;

	//bi-directional many-to-one association to ItinerarioMateria
	@OneToMany(mappedBy="mallaAsignatura", cascade=CascadeType.ALL)
	@Getter @Setter private List<ItinerarioMateria> itinerarioMaterias;
	
	//bi-directional many-to-one association to PlanificacionParalelo
	@OneToMany(mappedBy="mallaAsignatura", cascade=CascadeType.ALL)
	@Getter @Setter private List<PlanificacionParalelo> planificacionParalelo;
	
	//bi-directional many-to-one association to Asignatura
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_asignatura", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Asignatura asignatura;

	//bi-directional many-to-one association to Malla
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_malla", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Malla malla;

	//bi-directional many-to-one association to Nivel
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_nivel", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Nivel nivel;
	
	
	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }

}