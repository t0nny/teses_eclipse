package ec.edu.upse.acad.model.pojo.distributivo;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="aca", name="distributivo_dedicacion")
@Where(clause = "estado='A'")
@NoArgsConstructor

public class DistributivoDedicacion {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_distributivo_dedicacion")
	@Getter @Setter  private Integer id;

	@Column(name="id_distributivo_docente")
	@Getter @Setter  private Integer idDistributivoDocente;

	@Column(name="id_docente_dedicacion")
	@Getter @Setter  private Integer idDocenteDedicacion;

	@Column(name="fecha_desde")
	@Getter @Setter private Date fechaDesde;

	@Column(name="fecha_hasta")
	@Getter @Setter private Date fechaHasta;

	@Getter @Setter private String estado;
//
//	@Column(name="fecha_ingreso")
//	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;

	//RELACIONES

	//bi-directional many-to-one association to distributivo Docente
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_distributivo_docente" , insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private DistributivoDocente distributivoDocente;

	//bi-directional many-to-one association to doecente dedicacion
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_docente_dedicacion" , insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private DocenteDedicacion docenteDedicacion;

	@PrePersist
	void preInsert() {
		if (this.estado == null)
			this.estado = "A";
	}


}
