package ec.edu.upse.acad.model.service;

import java.util.List;


import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.json.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.upse.acad.model.pojo.Asignatura;
import ec.edu.upse.acad.model.pojo.AsignaturaAprendizaje;
import ec.edu.upse.acad.model.pojo.ComponenteAprendizaje;
import ec.edu.upse.acad.model.pojo.Docente;
import ec.edu.upse.acad.model.pojo.Malla;
import ec.edu.upse.acad.model.pojo.MallaAsignatura;
import ec.edu.upse.acad.model.pojo.Oferta;
import ec.edu.upse.acad.model.pojo.OfertaAsignatura;

import ec.edu.upse.acad.model.pojo.PlanificacionParalelo;
import ec.edu.upse.acad.model.pojo.ReglamentoCompAprendizaje;

import ec.edu.upse.acad.model.pojo.distributivo.DistributivoDedicacion;
import ec.edu.upse.acad.model.pojo.distributivo.DistributivoDocente;
import ec.edu.upse.acad.model.pojo.distributivo.DistributivoOfertaVersion;
import ec.edu.upse.acad.model.pojo.distributivo.DocenteActividad;
import ec.edu.upse.acad.model.pojo.distributivo.DocenteAsignaturaAprend;
import ec.edu.upse.acad.model.pojo.reglamento.ActividadDocenteDetalle;
import ec.edu.upse.acad.model.pojo.reglamento.ActividadPersonalDocente;
import ec.edu.upse.acad.model.pojo.reglamento.ReglamentoActividadDetalle;
import ec.edu.upse.acad.model.pojo.reglamento.ReglamentoActividadDocente;
import ec.edu.upse.acad.model.pojo.seguridad.Persona;
import ec.edu.upse.acad.model.repository.AsignaturaAprendizajeRepository;
import ec.edu.upse.acad.model.repository.MallaAsignaturaRepository;
import ec.edu.upse.acad.model.repository.PlanificacionParaleloRepository;
import ec.edu.upse.acad.model.repository.distributivo.ActividadDetalleRepository;
import ec.edu.upse.acad.model.repository.distributivo.ActividadDetalleRepository.CustomObjectDetalleaActividadDocente;

import ec.edu.upse.acad.model.repository.distributivo.DistributivoDocenteRepository;
import ec.edu.upse.acad.model.repository.distributivo.DistributivoGeneralRepository;
import ec.edu.upse.acad.model.repository.distributivo.DistributivoGeneralRepository.CustomObjectDocenteAsignaturaOfertaVersion;
import ec.edu.upse.acad.model.repository.distributivo.DistributivoGeneralRepository.CustomObjectDocenteDistributivoOferta;
import ec.edu.upse.acad.model.repository.distributivo.DistributivoGeneralRepository.CustomObjectFnDocentesNoAsignadoDistributivo;
import ec.edu.upse.acad.model.repository.distributivo.DocenteActividadRepository;
import ec.edu.upse.acad.model.repository.distributivo.DocenteAsignaturaAprendRepository;
import ec.edu.upse.acad.model.repository.distributivo.DocenteAsignaturaAprendRepository.CustomObjectAsignaturaComponenteAprendizaje;
import ec.edu.upse.acad.model.repository.distributivo.DocenteAsignaturaAprendRepository.CustomObjectAsignaturaComponenteAprendizajePivot;
import ec.edu.upse.acad.model.repository.distributivo.DocenteAsignaturaAprendRepository.CustomObjectCargarCabeceraDistributivoOfertaDocente;
import ec.edu.upse.acad.model.repository.distributivo.DocenteAsignaturaAprendRepository.CustomObjectCargarDetalleDistributivoOfertaDocente;
import ec.edu.upse.acad.model.repository.distributivo.DocenteAsignaturaAprendRepository.CustomObjectCargarDetalleDistributivoOfertaDocenteAsig;
import ec.edu.upse.acad.model.repository.distributivo.DocenteAsignaturaAprendRepository.CustomObjectDedicacionDistributivoOfertaDoc;


@Service
@Transactional
public class DistributivoDocenteService {
	@Autowired private DistributivoGeneralRepository distributivoGeneralRepository ;

	@Autowired private DistributivoDocenteRepository distributivoDocenteRepository;
	@Autowired private DocenteActividadRepository docenteActividadRepository;
	@Autowired private DocenteAsignaturaAprendRepository docenteAsignaturaAprendRepository;
	@Autowired private MallaAsignaturaRepository mallaAsignaturaRepository;
	@Autowired private PlanificacionParaleloRepository planificacionParaleloRepository;
	@Autowired private ActividadDetalleRepository actividadDetalleRepository;
	@Autowired private AsignaturaAprendizajeRepository asignaturaAprendizajeRepository;
	
//	@Autowired private FnAsignaturaComponenteAprendizajeRepository fnAsignaturaComponenteAprendizajeRepository;
//	@Autowired private FnDocenteAsignaturaOfertaVersionRepository fnDocenteAsignaturaOfertaVersionRepository;
//	@Autowired private FnDocentesNoAsignadoDistributivoRepository fnDocentesNoAsignadoDistributivoRepository;
//	FnDocenteAsignaturaOfertaVersion
	
	@PersistenceContext
	private EntityManager em;

	//refresca la funcion
	public List<CustomObjectCargarDetalleDistributivoOfertaDocente> refreshSPGenerarDetalleDistributivoOfertaDocente(Integer idDistributivoOfertaVersion) {
		em.getEntityManagerFactory().getCache().evict(AsignaturaAprendizaje.class);
		em.getEntityManagerFactory().getCache().evict(ComponenteAprendizaje.class);
		em.getEntityManagerFactory().getCache().evict(MallaAsignatura.class);
		em.getEntityManagerFactory().getCache().evict(ReglamentoCompAprendizaje.class);
		
		
		
		List<CustomObjectCargarDetalleDistributivoOfertaDocente> listaCargarAsignaturaComponenteAprendizaje = docenteAsignaturaAprendRepository.getSPDetalleDistributivoOfertaDocente(idDistributivoOfertaVersion);
			if(listaCargarAsignaturaComponenteAprendizaje != null) {
				return listaCargarAsignaturaComponenteAprendizaje;
			}
	
		return listaCargarAsignaturaComponenteAprendizaje;
	}
	public List<CustomObjectCargarCabeceraDistributivoOfertaDocente> refreshSPGenerarCabeceraDistributivoOfertaDocente(Integer idDistributivoOfertaVersion) {
		em.getEntityManagerFactory().getCache().evict(AsignaturaAprendizaje.class);
		em.getEntityManagerFactory().getCache().evict(ComponenteAprendizaje.class);
		em.getEntityManagerFactory().getCache().evict(MallaAsignatura.class);
		em.getEntityManagerFactory().getCache().evict(ReglamentoCompAprendizaje.class);
		
		
		
		List<CustomObjectCargarCabeceraDistributivoOfertaDocente> listaCargarAsignaturaComponenteAprendizaje = docenteAsignaturaAprendRepository.getSPCabeceraDistributivoOfertaDocente(idDistributivoOfertaVersion);
			if(listaCargarAsignaturaComponenteAprendizaje != null) {
				return listaCargarAsignaturaComponenteAprendizaje;
			}
	
		return listaCargarAsignaturaComponenteAprendizaje;
	}
	
	public List<CustomObjectCargarDetalleDistributivoOfertaDocenteAsig> refreshSPDetalleDistributivoOfertaDocenteAsig(Integer idDistributivoOfertaVersion, Integer idDistributivoDocente, Integer idDocente ) {
		em.getEntityManagerFactory().getCache().evict(AsignaturaAprendizaje.class);
		em.getEntityManagerFactory().getCache().evict(ComponenteAprendizaje.class);
		em.getEntityManagerFactory().getCache().evict(MallaAsignatura.class);
		em.getEntityManagerFactory().getCache().evict(ReglamentoCompAprendizaje.class);
		
		
		
		List<CustomObjectCargarDetalleDistributivoOfertaDocenteAsig> listaCargarAsignaturaComponenteAprendizaje = docenteAsignaturaAprendRepository.getSPDetalleDistributivoOfertaDocenteAsig(idDistributivoOfertaVersion, idDistributivoDocente, idDocente);
			if(listaCargarAsignaturaComponenteAprendizaje != null) {
				return listaCargarAsignaturaComponenteAprendizaje;
			}
	
		return listaCargarAsignaturaComponenteAprendizaje;
	}
	public CustomObjectDedicacionDistributivoOfertaDoc refreshSPDedicacionDistributivoOfertaDocente(Integer idDistributivoOfertaVersion, Integer idDistributivoDocente, Integer idDocente ) {
		em.getEntityManagerFactory().getCache().evict(AsignaturaAprendizaje.class);
		em.getEntityManagerFactory().getCache().evict(ComponenteAprendizaje.class);
		em.getEntityManagerFactory().getCache().evict(MallaAsignatura.class);
		em.getEntityManagerFactory().getCache().evict(ReglamentoCompAprendizaje.class);
		
		
		
		CustomObjectDedicacionDistributivoOfertaDoc listaCargarAsignaturaComponenteAprendizaje = docenteAsignaturaAprendRepository.getSPDedicacionDistributivoOfertaDocente(idDistributivoOfertaVersion, idDistributivoDocente, idDocente);
			if(listaCargarAsignaturaComponenteAprendizaje != null) {
				return listaCargarAsignaturaComponenteAprendizaje;
			}
	
		return listaCargarAsignaturaComponenteAprendizaje;
	}
	
	/***
	 * servicio para grabar DistributivoDocente, DocenteActividad, DistributivoDedicacion, DocenteAsignaturaAprend
	 * @param Authorization
	 * @param distributivoDocente
	 * @return
	 */
	public void grabarDistributivoDocenteDedicacion(DistributivoDocente distributivoDocente) {

		em.getEntityManagerFactory().getCache().evict(DistributivoDocente.class);
		em.getEntityManagerFactory().getCache().evict(DistributivoDedicacion.class);
		em.getEntityManagerFactory().getCache().evict(DocenteAsignaturaAprend.class);
		em.getEntityManagerFactory().getCache().evict(DocenteActividad.class);
		  JSONObject obj = new JSONObject(distributivoDocente);
		    System.out.println(obj);
			Integer idDistributivoDocente=distributivoDocente.getId();
			
			System.out.println("idDistributivoDocente "+idDistributivoDocente);
			if(idDistributivoDocente==null) {
				//System.out.println("distributivo docente"+ idDistributivoDocente);
				DistributivoDocente _distributivoDocente = new DistributivoDocente();
				BeanUtils.copyProperties(distributivoDocente, _distributivoDocente, "distributivoDedicaciones","docenteActividades","docenteAsignaturaAprends");
				idDistributivoDocente  = distributivoDocenteRepository.saveAndFlush(_distributivoDocente).getId();
			}
		
			
			if (distributivoDocente.getDistributivoDedicaciones().isEmpty()==false) {
			//iteracion para sincronizar relacion una a varios con DistributivoDedicacion
				for (int i = 0; i < distributivoDocente.getDistributivoDedicaciones().size(); i++) {
					distributivoDocente.getDistributivoDedicaciones().get(i).setIdDistributivoDocente(idDistributivoDocente);
				}
				distributivoDocente.setDistributivoDedicaciones(distributivoDocente.getDistributivoDedicaciones());
			}
	
			if (distributivoDocente.getDocenteAsignaturaAprends().isEmpty()==false) {
			//iteracion para sincronizar relacion una a varios con DocenteAsignaturaAprend
				for (int i = 0; i < distributivoDocente.getDocenteAsignaturaAprends().size(); i++) {
					distributivoDocente.getDocenteAsignaturaAprends().get(i).setIdDistributivoDocente(idDistributivoDocente);
					Integer idDocenteAsignatAprend=distributivoDocente.getDocenteAsignaturaAprends().get(i).getId();
					if(idDocenteAsignatAprend==null) {
						DocenteAsignaturaAprend _docenteAsignaturaAprend = new DocenteAsignaturaAprend();
						BeanUtils.copyProperties(distributivoDocente.getDocenteAsignaturaAprends().get(i), _docenteAsignaturaAprend, "asignaturaAprendizajeFusion", "asignaturaAprendizajeFusio");
						idDocenteAsignatAprend  = docenteAsignaturaAprendRepository.saveAndFlush(_docenteAsignaturaAprend).getId();
						distributivoDocente.getDocenteAsignaturaAprends().get(i).setId(idDocenteAsignatAprend);	
					}
					
				}		
			}
			
			distributivoDocente.setDocenteAsignaturaAprends(distributivoDocente.getDocenteAsignaturaAprends());
			//iteracion para sincronizar relacion una a varios con DocenteActividad
			//System.out.println("trae registro "+distributivoDocente.getDocenteActividades().isEmpty());
			if (distributivoDocente.getDocenteActividades().isEmpty()==false) {
				for (int i = 0; i < distributivoDocente.getDocenteActividades().size(); i++) {
					/*Integer idDetalleActividad=distributivoDocente.getDocenteActividades().get(i).getId();
					if(idDetalleActividad==null) {
						DocenteActividad _docenteActividad=new DocenteActividad();
						BeanUtils.copyProperties(distributivoDocente.getDocenteActividades().get(i), _docenteActividad);
						idDetalleActividad  = docenteActividadRepository.saveAndFlush(_docenteActividad).getId();
						distributivoDocente.getDocenteActividades().get(i).setId(idDetalleActividad);	
						
					}*/
					distributivoDocente.getDocenteActividades().get(i).setIdDistributivoDocente(idDistributivoDocente);
				}
				
				distributivoDocente.setDocenteActividades(distributivoDocente.getDocenteActividades());
			}
			
			JSONObject json2 = new JSONObject(distributivoDocente); // Convert text to object
			System.out.println(json2.toString());	
		
			DistributivoDocente _DistributivoDocenteC = new DistributivoDocente();
			_DistributivoDocenteC = distributivoDocenteRepository.findById(idDistributivoDocente).get();
			Integer version = _DistributivoDocenteC.getVersion();
			distributivoDocente.setId(idDistributivoDocente);
			distributivoDocente.setVersion(version);
		
			distributivoDocenteRepository.save(distributivoDocente);
			JSONObject json = new JSONObject(distributivoDocente); // Convert text to object
			System.out.println(json.toString());	
		
			em.getEntityManagerFactory().getCache().evict(DistributivoDocente.class);
			em.getEntityManagerFactory().getCache().evict(DistributivoDedicacion.class);
			em.getEntityManagerFactory().getCache().evict(DocenteAsignaturaAprend.class);
			em.getEntityManagerFactory().getCache().evict(DocenteActividad.class);
		
	}
	
	public void grabaAsignaturaAprendizajeMultService(List<AsignaturaAprendizaje> asignaturaAprendizaje) {
		
		for (int i = 0; i < asignaturaAprendizaje.size(); i++) {		
			if(asignaturaAprendizaje.get(i).getId()!=null) {
				asignaturaAprendizajeRepository.save(asignaturaAprendizaje.get(i));
			}
				
		}	
		em.getEntityManagerFactory().getCache().evict(AsignaturaAprendizaje.class);
			
	}


	
	//Actualiza Malla Asignatura cuando Configura paralelo
	public void grabaPlanificacionParaleloService(List<PlanificacionParalelo> planificacionParalelo) {
		for (int i = 0; i < planificacionParalelo.size(); i++) {		
			if(planificacionParalelo.get(i).getId()!=null) {
				planificacionParaleloRepository.save(planificacionParalelo.get(i));
			}else {
				PlanificacionParalelo planiParalelo=new PlanificacionParalelo();
				planiParalelo=planificacionParalelo.get(i);
				planificacionParaleloRepository.save(planiParalelo);
				
			}
				
		}	
		em.getEntityManagerFactory().getCache().evict(PlanificacionParalelo.class);

	}
	
	
	//Actualiza Malla Asignatura cuando Configura paralelo
	public void grabaMallaAsignaturaService(MallaAsignatura mallaasign) {
		MallaAsignatura _mallaasign = new MallaAsignatura();
		if (mallaasign.getId() != null) {
			_mallaasign = mallaAsignaturaRepository.findById(mallaasign.getId()).get();
		}
		BeanUtils.copyProperties(mallaasign, _mallaasign);
		mallaAsignaturaRepository.save(_mallaasign);
	//	return ResponseEntity.ok(_mallaasign);
	}
	
	
	//refresca el procedimineto Detalle Actividad
	public List<CustomObjectDetalleaActividadDocente> refreshDetalleActividadDocente(Integer idDistributivoDocente) {
		em.getEntityManagerFactory().getCache().evict(ActividadPersonalDocente.class);
		em.getEntityManagerFactory().getCache().evict(ReglamentoActividadDocente.class);
		em.getEntityManagerFactory().getCache().evict(ReglamentoActividadDetalle.class);
		em.getEntityManagerFactory().getCache().evict(ActividadDocenteDetalle.class);
		em.getEntityManagerFactory().getCache().evict(DocenteActividad.class);
		
		
		List<CustomObjectDetalleaActividadDocente> listaCargarDetalleActividadDocente = actividadDetalleRepository.getSpDetalleActividadDocente(idDistributivoDocente);
			if(listaCargarDetalleActividadDocente != null) {
				return listaCargarDetalleActividadDocente;
			}
	
		return listaCargarDetalleActividadDocente;
	}
	
	//refresca la funcion
		public List<CustomObjectAsignaturaComponenteAprendizaje> refreshFnAsignaturaComponenteAprendizaje(Integer idAsignatura) {
			em.getEntityManagerFactory().getCache().evict(AsignaturaAprendizaje.class);
			em.getEntityManagerFactory().getCache().evict(ComponenteAprendizaje.class);
			em.getEntityManagerFactory().getCache().evict(MallaAsignatura.class);
			em.getEntityManagerFactory().getCache().evict(ReglamentoCompAprendizaje.class);
			
			
			
			List<CustomObjectAsignaturaComponenteAprendizaje> listaCargarAsignaturaComponenteAprendizaje = docenteAsignaturaAprendRepository.getFnAsignaturaComponenteAprendizaje(idAsignatura);
				if(listaCargarAsignaturaComponenteAprendizaje != null) {
					return listaCargarAsignaturaComponenteAprendizaje;
				}
		
			return listaCargarAsignaturaComponenteAprendizaje;
		}
		
		
		public List<CustomObjectAsignaturaComponenteAprendizajePivot> refreshFnAsignaturaComponenteAprendizajePivot(Integer idAsignatura) {
			em.getEntityManagerFactory().getCache().evict(AsignaturaAprendizaje.class);
			em.getEntityManagerFactory().getCache().evict(ComponenteAprendizaje.class);
			em.getEntityManagerFactory().getCache().evict(MallaAsignatura.class);
			em.getEntityManagerFactory().getCache().evict(ReglamentoCompAprendizaje.class);
			
			
			
			List<CustomObjectAsignaturaComponenteAprendizajePivot> listaCargarAsignaturaComponenteAprendizaje = docenteAsignaturaAprendRepository.getFnAsignaturaComponenteAprendizajePivot(idAsignatura);
				if(listaCargarAsignaturaComponenteAprendizaje != null) {
					return listaCargarAsignaturaComponenteAprendizaje;
				}
		
			return listaCargarAsignaturaComponenteAprendizaje;
		}
		
		public List<CustomObjectDocenteDistributivoOferta> refreshgetSpDistributivoDocente(Integer idPeriodoAcademico,Integer idDistributivoOfertaVersion) {
			em.getEntityManagerFactory().getCache().evict(DistributivoOfertaVersion.class);
			em.getEntityManagerFactory().getCache().evict(OfertaAsignatura.class);
			em.getEntityManagerFactory().getCache().evict(Oferta.class);
			em.getEntityManagerFactory().getCache().evict(DistributivoDocente.class);
			em.getEntityManagerFactory().getCache().evict(DocenteAsignaturaAprend.class);

			List<CustomObjectDocenteDistributivoOferta> listaCargarDocenteDistributivo = distributivoGeneralRepository.getFnDistributivoOfertaVersion(idPeriodoAcademico, idDistributivoOfertaVersion);
				if(listaCargarDocenteDistributivo != null) {
					return listaCargarDocenteDistributivo;
				}
		
				
			return listaCargarDocenteDistributivo;
		}
		

		
		
		/**
		 * refresca la funcion getFnDocenteAsignaturaOfertaVersion 
		 * @param idPeriodoAcademico
		 * @param idDepartamento
		 * @param idOferta
		 * @param idDistributivoOfertaVersion
		 * @param idDocente
		 * @return
		 */
		public List<CustomObjectDocenteAsignaturaOfertaVersion> refreshgetFnDocenteAsignaturaOfertaVersion(Integer idPeriodoAcademico,Integer idDepartamento,Integer idOferta,Integer idDistributivoOfertaVersion, Integer idDocente) {
			em.getEntityManagerFactory().getCache().evict(DistributivoOfertaVersion.class);
			em.getEntityManagerFactory().getCache().evict(OfertaAsignatura.class);
			em.getEntityManagerFactory().getCache().evict(Oferta.class);
			em.getEntityManagerFactory().getCache().evict(DistributivoDocente.class);
			em.getEntityManagerFactory().getCache().evict(DocenteAsignaturaAprend.class);
			em.getEntityManagerFactory().getCache().evict(ComponenteAprendizaje.class);
			em.getEntityManagerFactory().getCache().evict(Asignatura.class);
			//em.getEntityManagerFactory().getCache().evict(FnDocenteAsignaturaOfertaVersion.class);
			em.getEntityManagerFactory().getCache().evict(AsignaturaAprendizaje.class);
			em.getEntityManagerFactory().getCache().evict(MallaAsignatura.class);
			em.getEntityManagerFactory().getCache().evict(ReglamentoCompAprendizaje.class);
			List<CustomObjectDocenteAsignaturaOfertaVersion> listaCargarFnDocenteAsignaturaOfertaVersion = distributivoGeneralRepository.getFnDocenteAsignaturaOfertaVersion(idPeriodoAcademico,idDepartamento,idOferta,idDistributivoOfertaVersion, idDocente);
				if(listaCargarFnDocenteAsignaturaOfertaVersion != null) {
					return listaCargarFnDocenteAsignaturaOfertaVersion;
				}
		
				
			return listaCargarFnDocenteAsignaturaOfertaVersion;
		}
		
		
		public List<CustomObjectFnDocentesNoAsignadoDistributivo> refreshgetFnDocenteNoAsignadoDistributivo(Integer idPeriodoAcademico,Integer idDistributivoOfertaVersion) {
			em.getEntityManagerFactory().getCache().evict(Docente.class);
			em.getEntityManagerFactory().getCache().evict(Persona.class);
			em.getEntityManagerFactory().getCache().evict(Oferta.class); 
			em.getEntityManagerFactory().getCache().evict(DistributivoDocente.class);
			em.getEntityManagerFactory().getCache().evict(DocenteAsignaturaAprend.class);
			em.getEntityManagerFactory().getCache().evict(ComponenteAprendizaje.class);
			em.getEntityManagerFactory().getCache().evict(Asignatura.class);
			//em.getEntityManagerFactory().getCache().evict(FnDocentesNoAsignadoDistributivo.class);
						
			List<CustomObjectFnDocentesNoAsignadoDistributivo> listaCargarFnDocentesNoAsignadoDistributivo = distributivoGeneralRepository.getFnDocentesNoAsignadoDistributivo(idPeriodoAcademico, idDistributivoOfertaVersion);
				if(listaCargarFnDocentesNoAsignadoDistributivo != null) {
					return listaCargarFnDocentesNoAsignadoDistributivo;
				}
		
				
			return listaCargarFnDocentesNoAsignadoDistributivo;
		}

		
}
