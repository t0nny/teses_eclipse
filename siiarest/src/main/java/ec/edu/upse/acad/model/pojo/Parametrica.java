package ec.edu.upse.acad.model.pojo;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;

//import org.eclipse.persistence.annotations.AdditionalCriteria;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="man", name="parametricas")
@NoArgsConstructor
//@AdditionalCriteria("this.estado = 'AC'")

public class Parametrica {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Getter @Setter private Integer id;
	
	@Getter @Setter private Integer opcion_id;
		
	@Getter @Setter private String codigo;
	
	@Getter @Setter private Integer correlativo;
	
	@Getter @Setter private String nombre;
	
	@Getter @Setter private String descripcion;
	
	@Getter @Setter private String estado;
		
	@Getter @Setter private Date fecha_ing;
	
	@Getter @Setter private Date fecha_mod;
	
	@Getter @Setter private String usuario_ing;
	
	@Getter @Setter private String usuario_mod;
	
	@Getter @Setter private Integer padre_id;
	
	@PrePersist
	void preInsert() {
	   if (this.estado == null)
	       this.estado = "AC";
	}			
}
