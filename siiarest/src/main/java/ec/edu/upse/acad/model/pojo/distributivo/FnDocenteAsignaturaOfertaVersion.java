package ec.edu.upse.acad.model.pojo.distributivo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@SqlResultSetMapping(
		name = "MappingFnDocenteAsignaturaOfertaVersion",
		classes = @ConstructorResult(
				targetClass = FnDocenteAsignaturaOfertaVersion.class,
				columns = {
						@ColumnResult(name = "id"),
						@ColumnResult(name = "idDistributivoOfertaVersion"),
						@ColumnResult(name = "idDistributivoDocente"),
						@ColumnResult(name = "idDocente"),
						@ColumnResult(name = "ofertaPrincipal"),
						@ColumnResult(name = "ofertaAsignatura"),
						@ColumnResult(name = "idAsignatura"),
						@ColumnResult(name = "asignatura"),
						@ColumnResult(name = "paralelo"),
						@ColumnResult(name = "idParalelo"),
						@ColumnResult(name = "numEstudiante"),
						@ColumnResult(name = "docencia"),
						@ColumnResult(name = "practica"),
						@ColumnResult(name = "horasClases"),
						@ColumnResult(name = "idDedicacion"),
						@ColumnResult(name = "dedicacion"),
				}
				)
		)

@NamedNativeQuery(name = "getFnDocenteAsignaturaOfertaVersion",
resultClass = FnDocenteAsignaturaOfertaVersion.class,

resultSetMapping ="MappingFnDocenteAsignaturaOfertaVersion", 
query = "select id, id_distributivo_oferta_version, id_distributivo_docente, id_docente, oferta_principal, oferta_asignatura, id_asignatura, asignatura " + 
		",paralelo, id_paralelo, num_estudiantes, docencia, practicas, horas_clases, id_dedicacion, dedicacion"
		+ " from aca.fn_docente_asignatura_oferta_version(?1, ?2, ?3, ?4,?5) as d")

@Entity
@Table(schema="aca", name="fn_docente_asignatura_oferta_version")
@NoArgsConstructor
@IdClass(FnDocenteAsignaturaOfertaVersion.class)

public class FnDocenteAsignaturaOfertaVersion implements Serializable {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	@Getter @Setter private Integer id;

	@Id
	@Column(name="id_distributivo_oferta_version")
	@Getter @Setter private Integer idDistributivoOfertaVersion;
	
	@Column(name="id_distributivo_docente")
	@Getter @Setter private Integer idDistributivoDocente;
	
	@Column(name="id_docente")
	@Getter @Setter private Integer idDocente;
	
	@Column(name="oferta_Principal")
	@Getter @Setter private boolean ofertaPrincipal;
	
	@Column(name="oferta_asignatura")
	@Getter @Setter private String ofertaAsignatura;
	
	@Id
	@Column(name="id_asignatura")
	@Getter @Setter private Integer idAsignatura;
	
	@Column(name="asignatura")
	@Getter @Setter private String asignatura;
	
	@Column(name="paralelo")
	@Getter @Setter private String paralelo;
	
	@Column(name="id_paralelo")
	@Getter @Setter private Integer idParalelo;
	
	@Column(name="num_estudiantes")
	@Getter @Setter private Integer numEstudiante;
	
	@Column(name="docencia")
	@Getter @Setter private Integer docencia;
	
	@Column(name="practica")
	@Getter @Setter private Integer practica;
	
	@Column(name="horas_clases")
	@Getter @Setter private Integer horasClases;
	
	@Column(name="id_dedicacion")
	@Getter @Setter private Integer idDedicacion;
	
	@Column(name="dedicacion")
	@Getter @Setter private String dedicacion;
}
