package ec.edu.upse.acad.model.pojo.evaluacion;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import ec.edu.upse.acad.model.pojo.distributivo.DocenteAsignaturaAprend;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Entity
@Table(schema="eva", name="det_evaluacion_docente")
@NoArgsConstructor
public class DetEvaluacionDocente {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_det_evaluacion_docente")
	@Getter @Setter private Integer id;
	
	//@Column(name="id_cab_evaluacion_docente")
	//@Getter @Setter private Integer idCabEvaluacionDocente;
	
	//@Column(name="id_opcion_pregunta")
	//@Getter @Setter private Integer idOpcionPregunta;

	@Getter @Setter private Integer calificacion;
	
	@Getter @Setter private String estado;
	
//	@Column(name="fecha_ingreso")
//	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;
	
	@Version
	@Getter @Setter private Integer version;

	//RELACIONES
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_cab_evaluacion_docente", insertable=true, updatable = true)
	//@JsonIgnore
	@Getter @Setter private CabEvaluacionDocente cabEvaluacionDocente;
	
	/*@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_docente_asignatura_aprend", insertable=true, updatable = true)
	//@JsonIgnore
	@Getter @Setter private DocenteAsignaturaAprend docenteAsignaturaAprend;*/

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_opcion_pregunta", nullable = false, insertable=true, updatable = true)
	//@JsonIgnore
	@Getter @Setter private OpcionPregunta opcionPregunta;
	
	@PrePersist
	void preInsert() {
	   if (this.estado == null)
	       this.estado = "A";
	}	
	
}
