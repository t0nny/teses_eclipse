package ec.edu.upse.acad.model.repository.distributivo;



import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.upse.acad.model.pojo.Docente;
import ec.edu.upse.acad.model.pojo.distributivo.DistributivoOferta;


@Repository
public interface DistributivoOfertaRepository extends JpaRepository<DistributivoOferta, Integer> {
	@Query(value=" select dov.id as idDistributivoOfertaVersion,pa.descripcion as periodo, pa.id as idPeriodoAcademico,d.id as idDepartamento, " + 
			" o.descripcion as carrera, dov.descripcion as descripcionDistributivo, d.nombre as facultad ,o.id as idOferta, "+
			" dov.fechaDesde as fechaDesde , dov.fechaHasta as fechaHasta " + 
			" from DistributivoOferta do inner join DistributivoOfertaVersion dov  " + 
			" on do.id = dov.idDistributivoOferta inner join Oferta o " + 
			" on do.idOferta = o.id inner join DistributivoGeneralVersion dgv " + 
			" on dgv.id = do.idDistributivoGeneralVersion inner join DistributivoGeneral dg " +
			" on dg.id = dgv.idDistributivoGeneral inner join PeriodoAcademico  pa " +  
			" on pa.id = dg.idPeriodoAcademico inner join DepartamentoOferta dof " + 
			" on dof.idOferta = o.id inner join Departamento d " + 
			" on d.id = dof.idDepartamento " 
			)
	List<CustomObject> listarDistributivoOfertas();

	
	@Transactional

	/**
	 * LLAMADA A PROCEDIMIENTO QUE REPLICA EL DISTRIBUTIVO OFERTA VERSION 
	 * @param idOferta
	 * @param idDistributivoOferta
	 * @param idPeriodoDuplicar
	 * @return
	 */
	@Procedure(procedureName  ="aca.sp_replica_distributivo")
	public  List<DistributivoOferta> replicaDisributivo(@Param("idOferta") Integer idOferta,@Param("idDistributivoOferta") Integer idDistributivoOferta,@Param("idPeriodoDuplicar") Integer idPeriodoDuplicar);	
	
/***
 * consulta que retorna la lista del distributivo oferta version de un distributivo general version
 * @return
 */

	
	
	/***
	 * Enlista todos los distributivo oferta filtrando por el periodo academico
	 * @param idPeriodo
	 * @return
	 * 
	 */

	@Query(value=" select dov.id as id,pa.descripcion as periodo, o.descripcion as carrera, "+
			"dov.descripcion as descripcion, d.nombre as facultad , o.id as idOferta,"+
			"dov.idDistributivoOferta as idDistributivoOferta, dov.fechaDesde as fechaDesde,  "+
			"case when (select min( dov1.fechaDesde)  from DistributivoOfertaVersion dov1 "+
			"where dov1.estado in ('A', 'D', 'V','R', 'P') and dov1.idDistributivoOferta=dov.idDistributivoOferta "+
			"and dov1.versionDistributivoOferta>dov.versionDistributivoOferta   ) is not null then (select min( dov1.fechaDesde)  from DistributivoOfertaVersion dov1 " + 
			"			where dov1.estado in ('A', 'D', 'V', 'R','P') and dov1.idDistributivoOferta=dov.idDistributivoOferta " + 
			"			and dov1.versionDistributivoOferta>dov.versionDistributivoOferta   ) else pa.fechaHasta end as fechaHasta,"+
			" dov.versionDistributivoOferta as versionDistributivoOferta,dov.estado as estado, " +   
			" dov.usuarioIngresoId as usuarioIngresoId,dov.version as version , dgv.id as idDistributivoGeneralVersion"+
			" FROM DistributivoOferta do "+
			" INNER JOIN DistributivoOfertaVersion dov on do.id = dov.idDistributivoOferta "+
			" INNER JOIN Oferta o on do.idOferta = o.id "+
			" INNER JOIN DistributivoGeneralVersion dgv  on dgv.id = do.idDistributivoGeneralVersion "+
			" INNER JOIN DistributivoGeneral dg on dg.id = dgv.idDistributivoGeneral "+
			" INNER JOIN PeriodoAcademico  pa  on pa.id = dg.idPeriodoAcademico "+
			" INNER JOIN DepartamentoOferta dof on dof.idOferta = o.id "+
			" INNER JOIN Departamento d on d.id = dof.idDepartamento "+
			" where dgv.id in (?1) and do.estado='A' and dov.estado in ('A','D', 'V', 'R','P') and o.estado='A' "+
			" and dgv.estado='A' and dg.estado='A' and pa.estado='A' and dof.estado='A' and d.estado='AC'"+ 
			" order by dov.id desc " )
	List<CustomObjectDistriCarrera> listarDistributivoOfertasIdCarrera(Integer idDistributivoGeneralVer);
	
	interface CustomObjectDistriCarrera  { 
	    Integer getId(); 
	    String getPeriodo(); 
	    String getCarrera();
	    String getDescripcion();
	    String getFacultad(); 
	    Integer getIdOferta();
	    Integer getIdDistributivoOferta();
	    Date getfechaDesde(); 
	    Date getfechaHasta();
	    String getversionDistributivoOferta();
	    String getEstado(); 
	    String getusuarioIngresoId();
	    String getVersion();
	    Integer getIdDistributivoGeneralVersion();
	} 	
	
	
	@Query(value=" select dov.id as idDistributivoOfertaVersion,pa.descripcion as periodo, " + 
			" o.descripcion as carrera, dov.descripcion as descripcionDistributivo, d.nombre as facultad ,"+
			"o.id as idOferta,dov.idDistributivoOferta, dov.fechaDesde as fechaDesde, "+
			"dov.fechaHasta as fechaHasta,dov.versionDistributivoOferta,dov.estado, " +   
			" dov.usuarioIngresoId,dov.version from DistributivoOferta do inner join DistributivoOfertaVersion dov " + 
			" on do.id = dov.idDistributivoOferta inner join Oferta o " + 
			" on do.idOferta = o.id inner join DistributivoGeneralVersion dgv " + 
			" on dgv.id = do.idDistributivoGeneralVersion inner join DistributivoGeneral dg " +
			" on dg.id = dgv.idDistributivoGeneral inner join PeriodoAcademico  pa " +  
			" on pa.id = dg.idPeriodoAcademico inner join DepartamentoOferta dof " + 
			" on dof.idOferta = o.id inner join Departamento d" + 
			" on d.id = dof.idDepartamento where pa.id in (?1 - 1, ?1) order by dov.id desc " 
			)
	List<CustomObjectDistriCarrera> listarDistributivoOfertasIdCarreraPeriodo(Integer idPeriodo);
	

/***
 * retorna la descripcion del distributivo oferta version
 * @param idDistributivoOfertaVersion
 * @return
 */

	@Query(value=" select dg.idReglamento as idReglamento, do.idDistributivoGeneralVersion as idDistributivoGeneralVersion,"+
			" dov.id as idDistributivoOfertaVersion,pa.descripcion as periodo, "+
			"pa.id as idPeriodoAcademico,d.id as idDepartamento, " + 
			" o.descripcion as carrera, dov.descripcion as descripcionDistributivo, d.nombre as facultad,o.id as idOferta, dov.fechaDesde as fechaDesde, "+
			" case when (select min( d.fechaDesde) from DistributivoOfertaVersion as d where d.idDistributivoOferta=do.id "+
			" and d.versionDistributivoOferta>dov.versionDistributivoOferta and d.estado='A') is null then pa.fechaHasta else  "+
			"(select min(d.fechaDesde) from DistributivoOfertaVersion as d where d.idDistributivoOferta=do.id " + 
			" and d.versionDistributivoOferta>dov.versionDistributivoOferta and d.estado= dov.estado) end  as fechaHasta , dov.estado as estado " + 
			"from  DistributivoOferta do  "+
			"inner join DistributivoOfertaVersion dov on do.id = dov.idDistributivoOferta "+
			"inner join Oferta o on do.idOferta = o.id "+
			"inner join DistributivoGeneralVersion dgv on dgv.id = do.idDistributivoGeneralVersion "+
			"inner join DistributivoGeneral dg on dg.id = dgv.idDistributivoGeneral "+
			"inner join PeriodoAcademico  pa on pa.id = dg.idPeriodoAcademico "+
			"inner join DepartamentoOferta dof  on dof.idOferta = o.id "+
			"inner join Departamento d  on d.id = dof.idDepartamento " +
			"where dov.id = (?1) and dov.estado in('A','D' ,'V', 'P')  and do.estado='A' and o.estado='A' and dg.estado='A' "+
			"and dgv.estado='A' and pa.estado='A' and dof.estado='A' "+
			"group by dg.idReglamento , do.idDistributivoGeneralVersion , dov.id ,pa.descripcion,pa.id "+
			",d.id,o.descripcion , dov.descripcion , d.nombre ,o.id , dov.fechaDesde,dov.versionDistributivoOferta,do.id ,pa.fechaHasta,dov.estado "
			)
	CustomObject listarDistributivoOfertasId(Integer idDistributivoOfertaVersion);
	interface  CustomObject  { 
		Integer getIdReglamento();
		Integer getIdDistributivoGeneralVersion();
	    Integer getIdDistributivoOfertaVersion(); 
	    String getPeriodo(); 
	    Integer getIdPeriodoAcademico();
	    Integer getIdDepartamento(); 
	    String getCarrera();
	    String getDescripcionDistributivo();
	    String getFacultad(); 
	    Integer getIdOferta();
	    Date getFechaDesde();
	    Date getFechaHasta();
	    String getEstado();
	} 
	@Query(value=" select  ded.id as idDocenteDedicacion, ded.descripcion as dedicacion,count (dde.idDocenteDedicacion) as cantidad "+
			"from  DistributivoOferta do  "+
			"inner join DistributivoOfertaVersion dov on do.id = dov.idDistributivoOferta "+
			"inner join DistributivoDocente ddo on dov.id = ddo.idDistributivoOfertaVersion "+
			"inner join DistributivoDedicacion dde on ddo.id = dde.idDistributivoDocente "+
			"inner join DocenteDedicacion ded on dde.idDocenteDedicacion=ded.id "+
			"where dov.id = (?1) and dov.estado in('A','D' ,'V', 'P')  and do.estado='A' and ddo.estado='A' and dde.estado='A'   and ded.estado='A' "+
			"group by  ded.id, ded.descripcion"
			)
	List<CustomObjectCantDocDed> listarCantDocenteDed(Integer idDistributivoOfertaVersion);
	interface  CustomObjectCantDocDed  { 
		Integer getIdDocenteDedicacion();
		String getDedicacion();
	    Integer getCantidad(); 
	}


	
	@Query(value=" select do.id as idDistributivoOferta,do.idDistributivoGeneralVersion as idDistributivoGeneralVersion, "+
			" do.idOferta as idOferta,do.estado as estado " + 
			"  from DistributivoOferta do where do.id = (?1)"
			)
	List<CustomObjectRC> recuperarCabezeraDistributivoOferta(Integer idDistributivoOferta);
	
	interface CustomObjectRC  { 
	    Integer getIdDistributivoOferta(); 
	    Integer getIdDistributivoGeneralVersion(); 
	    Integer getIdOferta(); 
	    String getEstado(); 
	  
	} 
	
	
	
	
	@Query(value="SELECT ma.id as idMallaAsignatura, a.descripcion as asignatura, par.id as idParalelo " + 
			" FROM PlanificacionParalelo pp "+
			"INNER JOIN MallaAsignatura ma on pp.idMallaAsignatura=ma.id " + 
			"INNER JOIN Asignatura a on ma.idAsignatura=a.id " + 
			"INNER JOIN Malla m on ma.idMalla=m.id " + 
			"INNER JOIN PeriodoMalla pm on m.id=pm.idMalla and pp.idPeriodoAcademico=pm.idPeriodoAcademico " + 
			"INNER JOIN Paralelo as par on par.orden<= isnull (pp.numParalelos,0) " + 
			"where pp.idPeriodoAcademico=?1 " + 
			"and pp.estado='A' and ma.estado='A' and a.estado='A' and m.estado in ('P') and pm.estado='A' " + 
			"and pp.idMallaAsignatura not in (SELECT aa.idMallaAsignatura from DistributivoOfertaVersion dov " + 
												"INNER JOIN DistributivoDocente dd on dov.id=dd.idDistributivoOfertaVersion " + 
												"INNER JOIN DocenteAsignaturaAprend daa on dd.id=daa.idDistributivoDocente " + 
												"INNER JOIN AsignaturaAprendizaje aa on daa.idAsignaturaAprendizaje=aa.id and dov.id=?2 " + 
												"where dov.estado in ('A', 'D', 'V', 'P') and dd.estado ='A' and daa.estado='A' and aa.estado='A' " + 
												"GROUP BY aa.idMallaAsignatura  ) " + 
			"and par.id NOT in(SELECT daa.idParalelo from DistributivoOfertaVersion dov " + 
							"INNER JOIN DistributivoDocente dd on dov.id=dd.idDistributivoOfertaVersion " + 
							"INNER JOIN DocenteAsignaturaAprend daa on dd.id=daa.idDistributivoDocente " + 
							"INNER JOIN AsignaturaAprendizaje aa on daa.idAsignaturaAprendizaje=aa.id " + 
							"INNER JOIN MallaAsignatura ma1 on aa.idMallaAsignatura=ma1.id " + 
							"INNER JOIN Malla m1 on m1.id=ma1.idMalla " + 
							"where dov.estado in ('A', 'D', 'V', 'P') and dd.estado ='A' and daa.estado='A' and m1.estado='P' " + 
							"and aa.estado='A' and aa.idMallaAsignatura=ma.id and dov.id=?2 "+
							"GROUP BY ma1.id, daa.idParalelo ) " + 
			"group by ma.id, a.descripcion, par.id"
			)
	List<AsignaturaParaleloNoUtilizadaDist> asignaturaParaleloNoUtilizadaDist(Integer idPeriodoAcademico,Integer idDistributivoOfertaVersion);
	
	interface AsignaturaParaleloNoUtilizadaDist  { 
	    Integer getIdDistributivoOferta(); 
	    Integer getIdDistributivoGeneralVersion(); 
	    Integer getIdOferta(); 
	    String getEstado(); 
	  
	} 
	

}
