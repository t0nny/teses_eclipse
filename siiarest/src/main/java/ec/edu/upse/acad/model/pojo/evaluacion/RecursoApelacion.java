package ec.edu.upse.acad.model.pojo.evaluacion;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="eva", name="recurso_apelacion")
@NoArgsConstructor
public class RecursoApelacion {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_recurso_apelacion")
	@Getter @Setter private Integer id;

	@Column(name="nombre_recurso_apelacion")
	@Getter @Setter private String nombreRecursoApelacion;

	@Column(name="num_acta")
	@Getter @Setter private Integer numActa;
	
	@Column(name="fecha_acta")
	@Getter @Setter private Date fechaActa;
	
	@Column(name="ubicacion_archivo")
	@Getter @Setter private String ubicacionArchivo;

	@Getter @Setter private String estado;

//	@Column(name="fecha_ingreso")
//	@Getter @Setter private Timestamp fechaIngreso;
	
	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;
	
	@Version
	@Getter @Setter private Integer version;
	
	//RELACIONES
	//bi-directional many-to-one association to 
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_tipo_recurso_apelacion", insertable=false, updatable = false)
	@Getter @Setter private TipoRecursoApelacion tipoRecursoApelacion;
//	
	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }
}
