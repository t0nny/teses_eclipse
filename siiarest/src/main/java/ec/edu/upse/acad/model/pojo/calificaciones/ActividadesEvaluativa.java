package ec.edu.upse.acad.model.pojo.calificaciones;

import javax.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.util.List;


@Entity
@Table(schema="aca", name="actividades_evaluativas")
@NoArgsConstructor
public class ActividadesEvaluativa {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_actividades_evaluativas")
	@Getter @Setter private Integer id;

	@Getter @Setter private String descripcion;

	@Getter @Setter private String estado;


	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;

	//RELACIONES
	//bi-directional many-to-one association to ActividadesPonderada
	@OneToMany(mappedBy="actividadesEvaluativa")
	@Getter @Setter private List<ActividadesPonderada> actividadesPonderadas;
	
	@PrePersist
	void preInsert() {
	   if (this.estado == null)
	       this.estado = "A";
	}
}