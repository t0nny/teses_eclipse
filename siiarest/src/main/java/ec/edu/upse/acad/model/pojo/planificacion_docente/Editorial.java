package ec.edu.upse.acad.model.pojo.planificacion_docente;

import javax.persistence.*;
import org.hibernate.annotations.Where;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.util.List;


/**
 * The persistent class for the editorial database table.
 * 
 */
@Entity
@Table(schema="aca", name="editorial")
@Where(clause = "estado='A'")
@NoArgsConstructor
public class Editorial  {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_editorial")
	@Getter @Setter private Integer id;

	@Column(name="correo_electronico")
	@Getter @Setter private String correoElectronico;

	@Getter @Setter private String direccion;
  
	@Getter @Setter private String estado;

//	@Column(name="fecha_ingreso")
//	private Timestamp fechaIngreso;

	@Getter @Setter private String nombre;

	@Column(name="nombre_corto")
	@Getter @Setter private String nombreCorto;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;

	//RELACIONES
	//bi-directional many-to-one association to RecursoBibliografico
	@OneToMany(mappedBy="editorial", cascade=CascadeType.ALL)
	private List<RecursoBibliografico> recursoBibliograficos;

	@PrePersist
	void preInsert() {
	   if (this.estado == null)
	       this.estado = "A";
	}


}