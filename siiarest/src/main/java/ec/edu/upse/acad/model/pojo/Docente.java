package ec.edu.upse.acad.model.pojo;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;


import ec.edu.upse.acad.model.pojo.distributivo.DistributivoDocente;
import ec.edu.upse.acad.model.pojo.seguridad.Persona;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(schema="aca", name="docente")
@NoArgsConstructor
public class Docente {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_docente")
	@Getter @Setter private Integer id;
	
	@Column(name="id_persona")
	@Getter @Setter private Integer idPersona;

	@Getter @Setter private String estado;
	
	@Column(name="fecha_ingreso")
	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;
	
	//RELACIONES
	//bi-directional many-to-one association to Persona
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="id_persona", nullable = false, insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Persona persona;
	
	//bi-directional many-to-one association to ArchivoDocente
	@OneToMany(mappedBy="docente", cascade=CascadeType.ALL)
	@Getter @Setter private List<ArchivoDocente> archivoDocentes;

	//bi-directional many-to-one association to DistributivoDocente
	@OneToMany(mappedBy="docente", cascade=CascadeType.ALL)
	@Getter @Setter private List<DistributivoDocente> distributivoDocentes;

	//bi-directional many-to-one association to HistorialDocente
	@OneToMany(mappedBy="docente", cascade=CascadeType.ALL)
//	@Getter @Setter private List<DocenteHistorial> historialDocentes;

	@Getter @Setter private List<DocenteHistorial> docentesHistorial;
	
	//bi-directional many-to-one association to FormacionProfesional
	@OneToMany(mappedBy="docente", cascade=CascadeType.ALL)
	@Getter @Setter private List<FormacionProfesional> formacionProfesionales;
	
	//bi-directional many-to-one association to ActualizacionProfesional
	@OneToMany(mappedBy="docente", cascade=CascadeType.ALL)
	@Getter @Setter private List<ActualizacionProfesional> actualizacionProfesionales;
	
	//bi-directional many-to-one association to ActualizacionProfesional
	@OneToMany(mappedBy="docente", cascade=CascadeType.ALL)
	@Getter @Setter private List<OfertaDocente> ofertaDocente;

	//bi-directional many-to-one association to CalificacionGeneral
//	@OneToMany(mappedBy="docente")
//	@Getter @Setter  private List<CalificacionGeneral> calificacionGenerals;
	
	//bi-directional one-to-many association to EstudianteRecalificacion
  	//@OneToMany(mappedBy="docente")
  	//@Getter @Setter private List<ActaCalificacion> actaCalificaciones;


	
	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }
	
}