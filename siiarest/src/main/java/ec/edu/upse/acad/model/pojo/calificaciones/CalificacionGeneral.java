package ec.edu.upse.acad.model.pojo.calificaciones;

import java.util.Date;
import java.util.List;

import javax.persistence.*;
import com.fasterxml.jackson.annotation.JsonIgnore;


import ec.edu.upse.acad.model.pojo.PeriodoAcademico;

import ec.edu.upse.acad.model.pojo.reglamento.Reglamento;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="aca", name="calificacion_general")
@NoArgsConstructor
public class CalificacionGeneral {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_calificacion_general")
	@Getter @Setter private Integer id;
	
	@Column(name="id_periodo_academico")
	@Getter @Setter private Integer idPeriodoAcademico;

	@Column(name="id_reglamento")
	@Getter @Setter private Integer idReglamento;

	@Getter @Setter private String descripcion;

	@Column(name="fecha_desde")
	@Getter @Setter private Date fechaDesde;

	@Column(name="fecha_hasta")
	@Getter @Setter private Date fechaHasta;
	
	@Getter @Setter private String estado;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;
	
	/*//RELACIONES
	//bi-directional many-to-one association to ConfiguracionCalificacione
	@ManyToOne
	@JoinColumn(name="id_configuracion_calificaciones")
	@JsonIgnore
	@Getter @Setter private ConfiguracionCalificaciones configuracionCalificaciones;*/

	//bi-directional many-to-one association to Reglamento
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_reglamento" , insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Reglamento reglamento;
	
	//bi-directional many-to-one association to Reglamento
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_periodo_academico" , insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private PeriodoAcademico periodoAcademico;
	
	//bi-directional one-to-many association to CalificacionCiclo
	@OneToMany(mappedBy="calificacionGeneral")
	@Getter @Setter private List<CalificacionCiclo> calificacionCiclos;
	
	//bi-directional one-to-many association to CalificacionCiclo
	@OneToMany(mappedBy="calificacionGeneral")
	@Getter @Setter private List<ActaCalificacion> ActaCalificaciones;
		
	@PrePersist
	void preInsert() {
	   if (this.estado == null)
	       this.estado = "A";
	}
}