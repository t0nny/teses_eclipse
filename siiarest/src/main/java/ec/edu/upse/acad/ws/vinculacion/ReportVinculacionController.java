package ec.edu.upse.acad.ws.vinculacion;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.upse.acad.model.service.ReportService;
import ec.edu.upse.acad.model.service.SecurityService;
import lombok.Getter;

@RestController
@RequestMapping("/api/reportsVinculacion")
@CrossOrigin
@MultipartConfig
public class ReportVinculacionController {
	@Autowired private SecurityService securityService;
	@Autowired private ReportService reportService;
	
	//private static final String PATH_IMAGEN = "/src/main/resources/img";
	private static final String PATH_IMAGEN = File.separator+"src"+File.separator+"main"+File.separator+"resources"+File.separator+"img";
	private static final String PATH_REPORT = File.separator+"src"+File.separator+"main"+File.separator+"resources"+File.separator+"rep";
	@Getter private String imagen="logoUpse.png";
	@Getter private String rutaImagenBanerLocal="";
	@Getter private String imagen1="vinculacionLogo1.png";
	@Getter private String rutaImagenBanerLocal1="";
	@Getter private String rutaArchivoLocal="";	
	@Getter private String carpetaVinculacion="vinculacion";
	
	//reporte de evaluacion matriz ex-ante
		@RequestMapping(value="/getReportEvaluacionMatrizExAnte/{filterIdProyectoVersion}/{formato}", method=RequestMethod.GET)
		public ResponseEntity<?> getReportEvaluacionMatrizExAnte(@RequestHeader(value="Authorization") String authorization,
				@PathVariable("filterIdProyectoVersion") Integer filterIdProyectoVersion, 
				@PathVariable("formato") String formato, 
				HttpServletRequest request) throws Exception {
			if (!securityService.isTokenValido(authorization)) {
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
			}
			Map<String, Object> parametros = new HashMap<String, Object>();
			parametros.put("idProyectoVersion", filterIdProyectoVersion);
			File file =new File(imagen);
			String ruta =file.getAbsoluteFile().getParent();
			rutaImagenBanerLocal  = ruta+PATH_IMAGEN+File.separator+imagen;
			rutaImagenBanerLocal1  = ruta+PATH_IMAGEN+File.separator+imagen1;
			rutaArchivoLocal  = ruta+PATH_REPORT+File.separator+carpetaVinculacion+File.separator;
			parametros.put("rutaImagen", rutaImagenBanerLocal);
			parametros.put("rutaImagen1", rutaImagenBanerLocal1);
			parametros.put("rutaArchivo", rutaArchivoLocal);
			// Carga el archivo
			System.out.println("oa"+parametros);
			Resource resource = reportService.generaReporteParametrosFormatoCarpeta(carpetaVinculacion,"rptEvaluacionMatrizExAnte.jasper",parametros,formato);
			// Determina el contenido
			String contentType = null;
			try {
				contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
			} catch (IOException ex) {
				ex.printStackTrace();
			}

			// Si no se determina el tipo, asume uno por defecto.
			if(contentType == null) {
				if(formato=="pdf") {
					contentType = "application/octet-stream";
				}else if(formato=="xlsx") {
					contentType ="application/vnd.ms-excel";
				}
			}

			return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
					.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"").body(resource);
		}

		@RequestMapping(value="/getReportDetallePresupuestoTareas/{idProyecto}", method=RequestMethod.GET)
		public ResponseEntity<?> getReportDetallePresupuestoTareas(@RequestHeader(value="Authorization") String authorization,
				@PathVariable("idProyecto") Integer idProyecto, 
				HttpServletRequest request) throws Exception {
			if (!securityService.isTokenValido(authorization)) {
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
			}
			Map<String, Object> parametros = new HashMap<String, Object>();
			parametros.put("idProyecto", idProyecto);
			File file =new File(imagen);
			String ruta =file.getAbsoluteFile().getParent();
			rutaImagenBanerLocal  = ruta+PATH_IMAGEN+File.separator+imagen; 
			parametros.put("rutaImagen", rutaImagenBanerLocal);
			System.out.println("oa1"+parametros);
			
			Resource resource = reportService.generaReporteParametrosCarpeta(carpetaVinculacion,"rptPresupuestoTareasProyecto.jasper",parametros);

			// Determina el contenido
			String contentType = null;
			try {
				contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
			} catch (IOException ex) {
				ex.printStackTrace();
			}

			// Si no se determina el tipo, asume uno por defecto.
			if(contentType == null) {
				contentType = "application/octet-stream";
			}

			return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
					.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"").body(resource);
		}

}
