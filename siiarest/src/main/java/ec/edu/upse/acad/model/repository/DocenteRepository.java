package ec.edu.upse.acad.model.repository;

import java.util.Date;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.Docente;

import ec.edu.upse.acad.model.repository.distributivo.DocenteAsignaturaAprendRepository.CustomObjectAsignaturaComponenteAprendizaje;

/**
 * @author Mishell
 *
 */
@Repository
public interface DocenteRepository extends JpaRepository<Docente,Integer> {
	
	@Query(value=" select p.id as idPersona,concat(p.apellidos,' ',p.nombres) as nombresCompletos,"
			+ " p.identificacion as identificacion,d.id as idDocente , p.email_institucional as emailInstitucional" + 
			" from Docente d "+
			" inner join Persona p on p.id = d.idPersona " + 
			" inner join DistributivoDocente dd on dd.idDocente =  d.id " + 
			" inner join DistributivoOfertaVersion dov on dd.idDistributivoOfertaVersion = dov.id " + 
			" inner join DistributivoOferta do on dov.idDistributivoOferta = do.id " + 
			" where do.idOferta = ?1 ")
	List<ListDocentesRegistrados> listarDocenteRegistrados(Integer idOferta);
	interface ListDocentesRegistrados{ 
	    Integer getIdPersona(); 
	    String getNombresCompletos(); 
	    String getIdentificacion(); 
	    Integer getIdDocente();
	    String getEmailInstitucional();
	    
	}
	
	

	
	
	
	
	
	
	@Query(value=" select  p.id as idPersona,concat(p.apellidos,' ',p.nombres) as nombresCompletos,p.identificacion as identificacion,"+
			"d.id as idDocente , p.email_institucional as emailInstitucional, d.estado as estado " + 
			" from Docente d "+
			" inner join Persona p on p.id = d.idPersona  where d.estado='A' and p.estado='AC'" //+


			)
	List<listarTodosDocenteRegistrados> listarTodosDocenteRegistrados();
	interface listarTodosDocenteRegistrados{ 
	    Integer getIdPersona(); 
	    String getNombresCompletos(); 
	    String getIdentificacion(); 
	    Integer getIdDocente();
	    String getEmailInstitucional();
	    String getEstado();
	}
	
	@Query(value=" select p.id as idPersona,p.apellidos as apellidos,p.nombres as nombres,"
			+ " p.identificacion as identificacion, p.genero as genero, p.fecha_nace as fechaNace, "
			+ " p.ciudad as ciudad, p.direccion as direccion, p.telefono as telefono, p.celular as celular, "
			+ " p.email_personal as emailPersonal, p.email_institucional as emailInstitucional,"
			+ "	p.foto as foto, p.usuario as usuario, p.estado as estado, "
			+ " d.id as idDocente " + 
			" from Docente d "+
			" inner join Persona p on p.id = d.idPersona " + 
			" where d.id = ?1 and d.estado='A' and p.estado='AC'")
	Objecto buscarDocente(Integer idOferta);
	interface Objecto{ 
		Integer getIdPersona(); 
		String getApellidos(); 
	    String getNombres(); 
	    String getIdentificacion(); 
	    String getGenero(); 
	    String getFechaNace();
	    String getCiudad();
	    String getDireccion();
	    String getTelefono();
	    String getCelular();
	    String getEmailPersonal();
	    String getEmailInstitucional();
	    String getFoto();
	    String getUsuario();
	    String getEstado();
	    Integer getIdDocente();
		
	}	
	
	@Query(value=" select p.id as idPersona,p.apellidos as apellidos,p.nombres as nombres,"
			+ " p.identificacion as identificacion, p.genero as genero, p.fecha_nace as fechaNace, "
			+ " p.ciudad as ciudad, p.direccion as direccion, p.telefono as telefono, p.celular as celular, "
			+ " p.email_personal as emailPersonal, p.email_institucional as emailInstitucional,"
			+ "	p.foto as foto, p.usuario as usuario, p.estado as estado, "
			+ " d.id as idDocente " + 
			" from Docente d "+
			" inner join Persona p on p.id = d.idPersona " + 
			" where p.id = ?1")
	Objecto2 buscarDocente2(Integer idPersona);
	interface Objecto2{ 
		Integer getIdPersona(); 
		String getApellidos(); 
	    String getNombres(); 
	    String getIdentificacion(); 
	    String getGenero(); 
	    String getFechaNace();
	    String getCiudad();
	    String getDireccion();
	    String getTelefono();
	    String getCelular();
	    String getEmailPersonal();
	    String getEmailInstitucional();
	    String getFoto();
	    String getUsuario();
	    String getEstado();
	    Integer getIdDocente();
		
	}	
		
	@Query(value=" select fp.id as idFormacionProfesional,fp.idDocente as idDocente,per.id as idPersona,"
			+ " fp.idUniversidad as idUniversidad, u.institucion as universidad,u.idPais as idPais,p.descripcion as pais,"
			+ " fp.titulo as titulo, fp.idNivelAcademico as idNivelAcademico, na.descripcion as nivelAcademico, "
			+ " fp.idAreaConocimiento as idAreaConocimiento, ac.descripcion as areaConocimiento, fp.tiempoEstudio as tiempoEstudio, " + 
			" fp.fechaObtencion as fechaObtencion,fp.codigoSenescyt as codigoSenescyt ,fp.estado as estado,fp.tituloPrincipal as tituloPrincipal " + 
			" from Docente d" + 
			" inner join Persona per on per.id = d.idPersona " + 
			" inner join FormacionProfesional fp on fp.idDocente = d.id " + 
			" inner join Universidad u on u.id = fp.idUniversidad " + 
			" inner join Pais p on p.id = u.idPais " + 
			" inner join NivelAcademico na on na.id = fp.idNivelAcademico " + 
			" inner join AreaConocimiento ac on ac.id=fp.idAreaConocimiento " + 
			" where per.id = ?1 and fp.estado='A'")
	List<ObjectFormacionProfesional> listarFormacionProfesional(Integer id);
	interface ObjectFormacionProfesional{ 
	    Integer getIdFormacionProfesional(); 
	    Integer getIdDocente(); 
	    Integer getIdPersona(); 
	    Integer getIdUniversidad();
	    String getUniversidad();
	    Integer getIdPais();
	    String getPais();
	    String getTitulo();
	    Integer getIdNivelAcademico();
	    String getNivelAcademico();
	    Integer getIdAreaConocimiento();
	    String getAreaConocimiento();
	    Integer getTiempoEstudio();
	    Date getFechaObtencion();
	    String getCodigoSenescyt();  
	    String getEstado();  
	    Boolean getTituloPrincipal(); 
	}

	
	@Query(value= " select d.idPersona as idPersona,d.id as idDocente,dh.id as id,dc.id as idDocenteCategoria,concat(dc.tipoRelacionLaboral,', ',dc.descripcion) as docenteCategoria,"
			+ " dh.fechaDesde as fechaDesde, dh.fechaHasta as fechaHasta,dh.observacion as observacion, " +
			 " dh.estado as estado " +
			" from Docente d" + 
			" inner join Persona per on per.id = d.idPersona " + 
			" inner join DocenteHistorial dh on dh.idDocente=d.id " + 
			" inner join DocenteCategoria dc on dh.idDocenteCategoria =dc.id" +  
			" where per.id = ?1 and dh.estado='A'")
	List<listarDocenteHistorial> listarDocenteHistorial(Integer id);
	interface listarDocenteHistorial{ 

	    Integer getIdPersona(); 
	    Integer getIdDocente(); 
	    Integer getId(); 
	    Integer getIdDocenteCategoria();
	    
	    String getDocenteCategoria();
	    Date getFechaDesde();
	    Date getFechaHasta();
	    String getObservacion();  
	    String getEstado();  
	}
	
	@Query(value=" select ap.id as id,ap.idDocente as idDocente,per.id as idPersona, ap.idAreaConocimiento as idAreaConocimiento, " +
			" ac.descripcion as areaConocimiento, ap.institucion as institucion, ap.nombreCurso as nombreCurso,ap.tipoEvento as tipoEvento, "+
			" ap.tipoCertificado as tipoCertificado,ap.numeroDias as numeroDias,ap.numeroHoras as numeroHoras," + 
			
			 "(CASE   WHEN ap.tipoCapacitacion='PRO' THEN 'Profesional' WHEN ap.tipoCapacitacion='PED' THEN 'Pedagógico' END) as tipoCapacitacion, "
			+ " ap.fechaDesde as fechaDesde, ap.fechaHasta as fechaHasta," + 
			" ap.estado as estado,ap.version as version "+
			" from Docente d" + 
			" inner join Persona per on per.id = d.idPersona " + 
			" inner join ActualizacionProfesional ap on ap.idDocente = d.id " + 
			" inner join AreaConocimiento ac on ac.id=ap.idAreaConocimiento " + 
			" where per.id = ?1 and ap.estado='A'")
	List<listarActualizacionProfesionalPorIdPersona> listarActualizacionProfesionalPorIdPersona(Integer id);
	interface listarActualizacionProfesionalPorIdPersona{ 
	    Integer getId(); 
	    Integer getIdDocente(); 
	    Integer getIdPersona(); 
	    Integer getIdAreaConocimiento();
	    String getAreaConocimiento();
	    String getInstitucion();
	    String getNombreCurso();
	    String getTipoEvento();
	    String getTipoCertificado();
	    Integer getNumeroDias();
	    Integer getNumeroHoras();
	    String getTipoCapacitacion();
	    Date getFechaDesde();
	    Date getFechaHasta();
	    String getEstado();  
	    Integer getVersion(); 
	}
	
	
	//listarDocenteHistorial
	
	@Query(value=" select fp.id as idFormacionProfesional,fp.idDocente as idDocente,per.id as idPersona,"
	
			+ " fp.titulo as titulo, fp.tiempoEstudio as tiempoEstudio, " + 
			" fp.codigoSenescyt as codigoSenescyt " + 
			" from Docente d" + 
			" inner join Persona per on per.id = d.idPersona " + 
			" inner join FormacionProfesional fp on fp.idDocente = d.id " + 
			" where d.idPersona = ?1")
	List<ObjectFormacionProfesional1> listarFormacionProfesional1(Integer id);
	interface ObjectFormacionProfesional1{ 
	    Integer getIdFormacionProfesional(); 
	    Integer getIdDocente(); 
	    Integer getIdPersona(); 
	    String getTitulo();
	    Integer getTiempoEstudio();
	    
	    String getCodigoSenescyt();  
	}
	
	@Query(value=" SELECT doc.id as idDocente,concat (per.nombres,' ', per.apellidos) as nombresCompletos , doc.estado as estado " + 
			"FROM Persona as per " + 
			"INNER JOIN Docente as doc on per.id=doc.idPersona " + 
			"INNER JOIN DocenteHistorial as dhi on doc.id=dhi.idDocente " + 
			"WHERE doc.id not in (SELECT odo.idDocente " + 
			"FROM OfertaDocente as odo " + 
			"INNER JOIN Oferta o on odo.idOferta=o.id "+
			"INNER JOIN TipoOferta tof on o.idTipoOferta=tof.id "+
			"WHERE odo.idPeriodoAcademico=(?1) and tof.id=?2 and odo.estado='A' and o.estado='A' and tof.estado='A' ) " + 
			"and per.estado='AC' and doc.estado='A' and dhi.estado='A' " + 
			"GROUP BY doc.id,per.nombres, per.apellidos , doc.estado ")
	List<ObjectDocenteSinAsignar> listarDocenteSinAsignarOfertaDocente(Integer idPeriodo, Integer idTipoOferta);
	interface ObjectDocenteSinAsignar{ 
		
	    Integer getIdDocente(); 
	    String getNombresCompletos(); 
	    String getEstado(); 
	}
	
	
	@Query(value="SELECT doc.id as idDocente, concat (per.nombres,' ', per.apellidos) as nombresCompletos,ofe.descripcion as oferta, " + 
			"ofe.descripcionCorta as ofertaCorta,odo.idOferta as idOferta, odo.idPeriodoAcademico as idPeriodoAcademico, "+
			"odo.id as idOfertaDocente, odo.estado as estado, odo.version as version " + 
			"FROM Persona as per " + 
			"INNER JOIN Docente as doc on per.id=doc.idPersona " + 
			"INNER JOIN OfertaDocente as odo on doc.id=odo.idDocente " + 
			"INNER JOIN Oferta as ofe on odo.idOferta=ofe.id " + 
			"INNER JOIN TipoOferta as tof on ofe.idTipoOferta=tof.id " + 
			"INNER JOIN DepartamentoOferta as dof on ofe.id=dof.idOferta " + 
			"INNER JOIN PeriodoAcademico as pac on odo.idPeriodoAcademico=pac.id " + 
			"WHERE  pac.id=(?1) and dof.idDepartamento=(?2) and odo.estado='A'" + 
			"and per.estado='AC' and doc.estado='A' and ofe.estado='A' and dof.estado='A' and pac.estado='A' ")
	List<ObjectOfertaDocente> listarOfertaDocente(Integer idPeriodo, Integer idDepartamento,Integer idTipoOferta);
	interface ObjectOfertaDocente{ 
	    Integer getIdDocente(); 
	    String getNombresCompletos(); 
	    String getOferta(); 
	    String getOfertaCorta();
	    Integer getIdOferta();
	    Integer getIdPeriodoAcademico();
	    Integer getIdOfertaDocente();
	    String getEstado();
	    Integer getVersion();
	}
	
}
