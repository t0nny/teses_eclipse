package ec.edu.upse.acad.model.pojo.matricula;

import javax.persistence.*;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ec.edu.upse.acad.model.pojo.DepartamentoOferta;
import ec.edu.upse.acad.model.pojo.Malla;
import ec.edu.upse.acad.model.pojo.Oferta;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Entity
@Table(schema="aca", name="estudiante_oferta")
@Where(clause = "estado='A'")
@NoArgsConstructor

public class EstudianteOferta {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_estudiante_oferta")
	@Getter @Setter private Integer id;
	
	@Column(name="id_estudiante")
	@Getter @Setter private Integer idEstudiante;

	@Column(name="id_malla")
	@Getter @Setter private Integer idMalla;
	
	@Column(name="id_tipo_ingreso_estudiante")
	@Getter @Setter  private Integer idTipoIngresoEstudiante;
	

	@Column(name="id_departamento_oferta")
	@Getter @Setter private Integer idDepartamentoOferta;
	
	@Column(name="numero_matricula")
	@Getter @Setter private String numeroMatricula;

	@Column(name="fecha_desde")
	@Getter @Setter private Date fechaDesde;

	@Column(name="fecha_hasta")
	@Getter @Setter private Date fechaHasta;
	
	@Column(name="estado_estudiante_oferta")
	@Getter @Setter private String estadoEstudianteOferta;
	
	@Getter @Setter private String estado;

//	@Column(name="tipo_ingreso")
//	@Getter @Setter private String tipoIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private int version;

	//RELACIONES
	//bi-directional many-to-one association to EstudianteMatricula
	@OneToMany(mappedBy="estudianteOferta", cascade=CascadeType.ALL)
	@Getter @Setter private List<EstudianteMatricula> estudianteMatriculas;
	
	@OneToMany(mappedBy="estudianteOferta", cascade=CascadeType.ALL)
	@Getter @Setter private List<Movilidad> movilidad;

	//bi-directional many-to-one association to Estudiante
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_estudiante", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Estudiante estudiante;

	//bi-directional many-to-one association to MallaVersion
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_malla", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Malla malla;

	//bi-directional many-to-one association to TipoIngresoEstudiante
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_tipo_ingreso_estudiante", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private TipoIngresoEstudiante tipoIngresoEstudiante;

	//bi-directional many-to-one association to DepartamentoOferta
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_departamento_oferta", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private DepartamentoOferta departamentoOferta;

	@PrePersist
	void preInsert() {
	   if (this.estado == null)
	       this.estado = "A";
	}

}