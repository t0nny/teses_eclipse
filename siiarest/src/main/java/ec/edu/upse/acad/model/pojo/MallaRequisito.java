package ec.edu.upse.acad.model.pojo;


import java.sql.Timestamp;

import javax.persistence.*;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="aca", name="malla_requisito")
@Where(clause = "estado='A'")
@NoArgsConstructor
public class MallaRequisito {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_malla_requisito")
	@Getter @Setter private Integer id;
	
	@Column(name="id_malla")
	@Getter @Setter private Integer idMalla;
	
	@Column(name="id_requisito")
	@Getter @Setter private Integer idRequisito;

	@Getter @Setter private String estado;
	
	@Column(name="horas_total")
	@Getter @Setter private Integer horasTotal;

	@Column(name="num_creditos")
	@Getter @Setter private Integer numCreditos;
	
	@Column(name="aplica_asignatura")
	@Getter @Setter private Boolean aplicaAsignatura;
	
//	@Column(name="fecha_ingreso")
//	@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;
	

	
	//RELACIONES
	//bi-directional many-to-one association to Malla
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_malla", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Malla malla;

	//bi-directional many-to-one association to Requisito
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_requisito", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Requisito requisito;

	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }

}