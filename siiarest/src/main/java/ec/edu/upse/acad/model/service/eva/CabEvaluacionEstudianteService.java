package ec.edu.upse.acad.model.service.eva;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.upse.acad.model.pojo.distributivo.DocenteAsignaturaAprend;
import ec.edu.upse.acad.model.pojo.evaluacion.CabEvaluacionDocente;
import ec.edu.upse.acad.model.pojo.evaluacion.CabEvaluacionEstudiante;
import ec.edu.upse.acad.model.pojo.evaluacion.DetEvaluacionDocente;
import ec.edu.upse.acad.model.pojo.evaluacion.DetEvaluacionEstudiante;
import ec.edu.upse.acad.model.pojo.evaluacion.OpcionPregunta;
import ec.edu.upse.acad.model.pojo.evaluacion.Pregunta;
import ec.edu.upse.acad.model.repository.ModulosRolesUsuariosRepository;
import ec.edu.upse.acad.model.repository.distributivo.DocenteAsignaturaAprendRepository;
import ec.edu.upse.acad.model.repository.evaluacion.CabEvaluacionDocenteRepository;
import ec.edu.upse.acad.model.repository.evaluacion.CabEvaluacionEstudianteRepository;
import ec.edu.upse.acad.model.repository.evaluacion.OpcionPreguntaRepository;
import ec.edu.upse.acad.model.repository.evaluacion.PeriodoEvaluacionRepository;
import ec.edu.upse.acad.model.repository.evaluacion.PeriodoEvaluacionRepository.CustomObjectMateriaSemestreEstudiante;
import ec.edu.upse.acad.model.repository.matricula.EstudianteMatriculaRepository;
import ec.edu.upse.acad.model.repository.evaluacion.PreguntaRepository;

@Service
@Transactional
public class CabEvaluacionEstudianteService {
	
	@Autowired 
	private CabEvaluacionEstudianteRepository cabEvaluacionEstudianteRepository;
	
	@Autowired 
	private EstudianteMatriculaRepository estudianteMatriculaRepository;
	
	@Autowired 
	private OpcionPreguntaRepository opcionPreguntaRepository;
	
	@Autowired 
	private PeriodoEvaluacionRepository periodoEvaluacionRepository;
	
	@PersistenceContext
	private EntityManager em;
	
	
	public void newEvaluacionEstudiante( CabEvaluacionEstudiante cabEvaluacionEstudiante) {
		Timestamp fecha = new Timestamp(System.currentTimeMillis());
		List<CustomObjectMateriaSemestreEstudiante> listaMateriaEstudiante = periodoEvaluacionRepository.listarMateriaSemestrePorUsuarioEstudiante(cabEvaluacionEstudiante.getUsuarioIngresoId());
		CabEvaluacionEstudiante objeto = new CabEvaluacionEstudiante();		 
		objeto.setIdEstudianteMatricula(estudianteMatriculaRepository.findEstudianteMatriculaByUsuarioEstudiante(cabEvaluacionEstudiante.getUsuarioIngresoId())); 
		objeto.setUsuarioIngresoId(cabEvaluacionEstudiante.getUsuarioIngresoId());
		objeto.setFechaIngreso(fecha);
		objeto.setFechaRealizada(fecha);
		List<DetEvaluacionEstudiante> detEvaluacionEstudiantes = new ArrayList<>();
		for(DetEvaluacionEstudiante dee:cabEvaluacionEstudiante.getDetEvaluacionEstudiante()) {
			if(dee.getId() != null && dee.getIdOpcionPregunta().getIdOpcionRespuesta().getId() != null) { 
				DetEvaluacionEstudiante detEvaluacionEstudiante = new DetEvaluacionEstudiante();
				detEvaluacionEstudiante.setIdCabEvaluacionEstudiante(objeto);	
				OpcionPregunta  opcionPregunta = opcionPreguntaRepository.findOpcionPreguntaByIdPreguntaAndIdOpcionRespuesta(dee.getId(), dee.getIdOpcionPregunta().getIdOpcionRespuesta().getId());
				detEvaluacionEstudiante.setIdOpcionPregunta(opcionPregunta);			
				if(opcionPregunta.getIdOpcionRespuesta().getCodigo() == null) {
					detEvaluacionEstudiante.setValor(opcionPregunta.getIdOpcionRespuesta().getValor());
				}else {
					detEvaluacionEstudiante.setRespuestaSiNo(opcionPregunta.getIdOpcionRespuesta().getCodigo().trim());
				}
				
				for(CustomObjectMateriaSemestreEstudiante custom:listaMateriaEstudiante) {
					if(dee.getRespuestaSiNo().trim().equals(custom.getCodigo().trim())) {
						detEvaluacionEstudiante.setIdAsignaturaAprendizaje(custom.getIdAsignarutaAprendizaje());
						detEvaluacionEstudiante.setIdParalelo(custom.getidParalelo());
						detEvaluacionEstudiante.setIdDistributivoDocente(custom.getIdDistributivoDocente());
						break;
					}
				}
				
				detEvaluacionEstudiante.setUsuarioIngresoId(cabEvaluacionEstudiante.getUsuarioIngresoId());
				detEvaluacionEstudiante.setFechaIngreso(fecha);
				detEvaluacionEstudiantes.add(detEvaluacionEstudiante);
		
			}
		}	
		objeto.setDetEvaluacionEstudiante(detEvaluacionEstudiantes);
		cabEvaluacionEstudianteRepository.saveAndFlush(objeto); 	
		em.refresh(objeto);
	}
	
	/*public void saveOrUpdate(CabEvaluacionDocente cabEvaluacionDocente) {
		if(cabEvaluacionDocente.getId() != null) {
			 
		}else {
			
			this.newEvaluacionDocente(cabEvaluacionDocente);
		}
	}
	
	//GRABA UN NUEVO EVALUACION DOCENTE
	public void newEvaluacionDocente(CabEvaluacionDocente cabEvaluacionDocente){
		Timestamp fecha = new Timestamp(System.currentTimeMillis());
		CabEvaluacionDocente _cabEvaluacionDocente = new CabEvaluacionDocente();
		ModuloRolUsuario moduloRolUsuario = modulosRolesUsuariosRepository.findByUser(cabEvaluacionDocente.getUsuarioIngresoId());
		System.out.println(moduloRolUsuario.getId());
		List<CustomObjectMateriaSemestreEstudiante> listaMaterias = periodoEvaluacionRepository.listarMateriaSemestrePorUsuarioEstudiante(cabEvaluacionDocente.getUsuarioIngresoId());
		 
		_cabEvaluacionDocente.setDistributivoDocente(null);
		_cabEvaluacionDocente.setModuloRolUsuario(moduloRolUsuario);
		_cabEvaluacionDocente.setUsuarioIngresoId(cabEvaluacionDocente.getUsuarioIngresoId());
		//_cabEvaluacionDocente.setFechaIngreso(fecha);
		_cabEvaluacionDocente.setFechaRealizada(fecha);
		//_cabEvaluacionDocente.setEstado("A");
		List<DetEvaluacionDocente> lista = new ArrayList<>();
		for(DetEvaluacionDocente detEvaluacionDocente : cabEvaluacionDocente.getDetEvaluacionDocentes()) {
			DetEvaluacionDocente _detEvaluacionDocente = new DetEvaluacionDocente();
			if(detEvaluacionDocente.getPregunta() != null) { 
				Pregunta pregunta = preguntaRepository.findByIdPregunta(detEvaluacionDocente.getPregunta().getId());
				_detEvaluacionDocente.setCabEvaluacionDocente(_cabEvaluacionDocente);
				Integer idDocenteAsignaturaAprendizaje = 0;
				for(CustomObjectMateriaSemestreEstudiante objMateriaSemestreEst: listaMaterias) {				
					if(objMateriaSemestreEst.getCodigo().trim().equals(detEvaluacionDocente.getDocenteAsignaturaAprend().getAsignaturaAprendizaje().getMallaAsignatura().getAsignatura().getCodigo().trim())) {
						idDocenteAsignaturaAprendizaje = objMateriaSemestreEst.getIdDocenteAsignaturaAprendizaje();
						break;
					}
				}
				DocenteAsignaturaAprend docenteAsignaturaAprend = docenteAsignaturaAprendRepository.findByIdDAAA(idDocenteAsignaturaAprendizaje);
				_detEvaluacionDocente.setDocenteAsignaturaAprend(docenteAsignaturaAprend);
				_detEvaluacionDocente.setPregunta(pregunta);
				//System.out.println(_detEvaluacionDocente.getValor());
				_detEvaluacionDocente.setCalificacion(detEvaluacionDocente.getCalificacion());
				
				_detEvaluacionDocente.setUsuarioIngresoId(cabEvaluacionDocente.getUsuarioIngresoId());
				//_detEvaluacionDocente.setFechaIngreso(fecha);
				//_detEvaluacionDocente.setEstado("A");
				lista.add(_detEvaluacionDocente);
			}	
		}
		_cabEvaluacionDocente.setDetEvaluacionDocentes(lista);
		cabEvaluacionDocenteRepository.saveAndFlush(_cabEvaluacionDocente);
		em.refresh(_cabEvaluacionDocente);
	}
*/
}
