package ec.edu.upse.acad.model.pojo.evaluacion;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="eva", name="componente")
@NoArgsConstructor
public class Componente {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_componente")
	@Getter @Setter private Integer id;
	
	@Column(name="id_componente_padre")
	@Getter @Setter private Integer idComponentePadre;

	@Getter @Setter private String descripcion;
	
	@Getter @Setter private String estado;
		
	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;
	
	@Getter @Setter private String codigo;
	
	@Version
	@Getter @Setter private Integer version;
	
	//RELACIONES
	//bi-directional many-to-one association to Instrumento
	@OneToMany(mappedBy="componente", cascade=CascadeType.ALL)
	@JsonIgnore
	@Getter @Setter private List<ReglaComponente> reglaComponentes;

//	//bi-directional many-to-one association to AsignaturaRequisito
//	@OneToMany(mappedBy="componente", cascade=CascadeType.ALL)
//	@Getter @Setter private List<Instrumento> instrumentos;

	@PrePersist
	void preInsert() {
		if (this.estado == null)
			this.estado = "A";
	}

}
