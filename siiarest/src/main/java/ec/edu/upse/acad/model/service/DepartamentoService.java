package ec.edu.upse.acad.model.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.upse.acad.model.pojo.seguridad.Departamento;
import ec.edu.upse.acad.model.repository.DepartamentosRepository;

@Service
@Transactional
public class DepartamentoService {
	
	@Autowired private DepartamentosRepository departamentosRepository;
	
	public void borraDepartamento(Integer id) {
		
		Departamento departamento = departamentosRepository.findById(id).get();
		if (departamento !=null) {
			departamento.setEstado("IN");
			departamentosRepository.save(departamento);
			
		}
	}

}
