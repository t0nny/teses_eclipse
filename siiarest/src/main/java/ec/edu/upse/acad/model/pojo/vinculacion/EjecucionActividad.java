package ec.edu.upse.acad.model.pojo.vinculacion;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Entity
@Table(schema = "vin", name = "ejecucion_actividad")

@NoArgsConstructor //un constructorsin argumentos
public class EjecucionActividad {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_ejecucion_actividad")
	@Getter	@Setter	private Integer id;

	@Column(name = "id_asignacion_actividad")
	@Getter	@Setter	private Integer idAsignacionActividad;
	
	@Column(name = "fecha_desde")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", locale = "es_EC", timezone = "America/Guayaquil")
	@Getter	@Setter	private Date fechaDesde;

	@Column(name = "fecha_hasta")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", locale = "es_EC", timezone = "America/Guayaquil")
	@Getter	@Setter	private Date fechaHasta;
	
	@Column(name = "horas_trabajadas")
	@Getter	@Setter	private Integer horasTrabajadas;

	@Column(name = "usuario_ingreso_id")
	@Getter	@Setter	private String usuarioIngresoId;

	@Version
	@Getter	@Setter	private Integer version;

	@Getter	@Setter	private String estado;
	
	@PrePersist
	void preInsert() {
		if (this.estado == null)
			this.estado = "A";
	}


}
