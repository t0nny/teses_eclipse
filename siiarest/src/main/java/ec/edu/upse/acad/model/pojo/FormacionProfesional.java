package ec.edu.upse.acad.model.pojo;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema="dbo",name="formacion_profesional")
@NoArgsConstructor
public class FormacionProfesional {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_formacion_profesional")
	@Getter @Setter private Integer id;
	
	@Column(name="id_docente")
	@Getter @Setter private Integer idDocente;
	
	@Column(name="id_universidad")
	@Getter @Setter private Integer idUniversidad;
	
	@Column(name="id_nivel_academico")
	@Getter @Setter private Integer idNivelAcademico;
	
	@Column(name="id_area_conocimiento")
	@Getter @Setter private Integer idAreaConocimiento;
	
	@Getter @Setter private String titulo;
	
	@Column(name="tiempo_estudio")
	@Getter @Setter private Integer tiempoEstudio;
	
	@Column(name="fecha_obtencion")
	@Getter @Setter private Date fechaObtencion;
	
	@Column(name="codigo_senescyt")
	@Getter @Setter private String codigoSenescyt;

	@Getter @Setter private String estado;
	
	//@Column(name="fecha_ingreso")
	//@Getter @Setter private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Column(name="titulo_principal")
	@Getter @Setter private Boolean tituloPrincipal;
	
	@Version
	@Getter @Setter private Integer version;
	
	//RELACIONES
	//bi-directional many-to-one association to Docente
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_docente", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Docente docente;
	
	//bi-directional many-to-one association to Universidad
	@ManyToOne(fetch=FetchType.LAZY)
	@JsonIgnore
	@JoinColumn(name="id_universidad", insertable=false, updatable = false)
	@Getter @Setter private Universidad universidad;
	
	//bi-directional many-to-one association to NivelAcademico
	@ManyToOne(fetch=FetchType.LAZY)
	@JsonIgnore
	@JoinColumn(name="id_nivel_academico", insertable=false, updatable = false)
	@Getter @Setter private NivelAcademico nivelAcademico;
	
	//bi-directional many-to-one association to AreaConocimiento
	@ManyToOne(fetch=FetchType.LAZY)
	@JsonIgnore
	@JoinColumn(name="id_area_conocimiento", insertable=false, updatable = false)
		@Getter @Setter private AreaConocimiento areaConocimiento;

	@PrePersist
	void preInsert() {
	   if (this.estado == null)
	       this.estado = "A";
 }

}
