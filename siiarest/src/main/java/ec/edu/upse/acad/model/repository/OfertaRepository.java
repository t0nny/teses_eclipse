package ec.edu.upse.acad.model.repository;



import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.Oferta;


@Repository
public interface OfertaRepository  extends JpaRepository<Oferta, Integer>{
	//consulta de asignaturas version
	@Query("SELECT to.id as idTipoOferta,to.descripcion as tipoOferta,o.id as idOferta, o.descripcion as oferta,"+
			"m.id as idMalla, m.descripcion as malla, dof.id as idDepartamentoOferta, dof.idDepartamento as idDepartamento "+
			"from TipoOferta to " + 
			"INNER JOIN Oferta o on to.id = o.idTipoOferta " + 
			"INNER JOIN DepartamentoOferta dof on o.id=dof.idOferta "+
			"INNER JOIN Malla m on dof.id=m.idDepartamentoOferta "+	
			"WHERE to.estado='A' and o.estado='A' and dof.estado='A' and m.estado in ('A','P') and to.id=?1  ")
	 List<CustomObjectDepartOfertaMalla> listarDepartOfertaMalla(Integer idTipoOferta);
	interface CustomObjectDepartOfertaMalla { 
		Integer getIdTipoOferta();
		String getTipoOferta();
		Integer getIdOferta();
		String getOferta();
		Integer getIdMalla();
		String getMalla();
		Integer getIdDepartamentoOferta();
		Integer getIdDepartamento();
	}
	
	@Query("SELECT to.id as idTipoOferta,to.descripcion as tipoOferta,o.id as idOferta, "+
			"o.descripcion as oferta, o.descripcionCorta as descripcionCorta,o.estado as estado,"+
			"o.fechaAprobacion as fechaAprobacion,o.fechaInicioOferta as fechaInicioOferta, o.fechaFinOferta as fechaFinOferta,"+
			" o.fechaCierre as fechaCierre, o.usuarioIngresoId,o.version as version  " + 
			"from TipoOferta to " + 
			"INNER JOIN Oferta o on to.id = o.idTipoOferta " + 
			"WHERE to.estado='A' and o.estado='A' and to.id=?1 ")
	 List<CustomObjectOfertaAll> listarOferta(Integer idTipoOferta);
	
	interface CustomObjectOfertaAll { 
		Integer getIdTipoOferta();
		String getTipoOferta();
		Integer getIdOferta();
		String getOferta();
		String getDescripcionCorta();
		String getEstado();
		Date getFechaAprobacion();
		Date getFechaInicioOferta();
		Date getFechaFinOferta();
		Date getFechaCierre();
		Integer getUsuarioIngresoId();
		Integer getVersion();
		
	}
	
	@Query("SELECT to.id as idTipoOferta,to.descripcion as tipoOferta,o.id as idOferta, "+
			"o.descripcion as oferta, o.descripcionCorta as descripcionCorta,o.estado as estado,"+
			"o.fechaAprobacion as fechaAprobacion,o.fechaInicioOferta as fechaInicioOferta, o.fechaFinOferta as fechaFinOferta,"+
			" o.fechaCierre as fechaCierre, o.usuarioIngresoId,o.version as version  " + 
			"from TipoOferta to " + 
			"INNER JOIN Oferta o on to.id = o.idTipoOferta " + 
			"WHERE to.estado='A' and o.estado='A' and to.id=?1 and "+
			" o.id not in (SELECT do.idOferta FROM Departamento d "+
			" INNER JOIN DepartamentoOferta do on d.id=do.idDepartamento "+
			" WHERE d.estado='AC' and do.estado='A' "+
			" GROUP BY do.idOferta ) ")
	 List<CustomObjectOfertaAll> listarOfertaNoAsignado(Integer idTipoOferta);

	
	@Query("SELECT d.id as idDepartamento,do.id as idDepartamentoOferta,o.id as idOferta, "+
			" o.descripcion as oferta,do.estado as estado,do.version as version " + 
			"from Departamento d " + 
			"INNER JOIN DepartamentoOferta do ON d.id = do.idDepartamento " + 
			"INNER JOIN Oferta o ON do.idOferta=o.id " + 
			"WHERE d.estado='AC' and do.estado='A' and o.estado='A' and o.idTipoOferta=?1 ")
	 List<CustomObjectDepartOferta> listaOfertaDepartTipoId(Integer idTipoOferta);
	interface CustomObjectDepartOferta { 
		Integer getIdDepartamento();
		Integer getIdDepartamentoOferta();
		Integer getIdOferta();
		String getOferta();
		String getEstado();
		Integer getVersion();
		
	}
	
	@Query("SELECT d.id as idDepartamento,d.nombre as departamento,o.id as idOferta, " + 
			"o.descripcion as oferta,do.estado as estado,do.id as id,d.version as versionDepartamento,"+
			"do.version as versionDepartamentoOferta,o.version as versionOferta " + 
			"from Departamento d " + 
			"INNER JOIN DepartamentoOferta do ON d.id = do.idDepartamento " + 
			"INNER JOIN Oferta o ON do.idOferta=o.id " + 
			"WHERE d.estado='AC' and do.estado='A' and o.estado='A'  ")
	 List<CustomObjectMateriaOferta> listaOfertaDepart();
	
		@Query("SELECT d.id as idDepartamento,d.nombre as departamento,o.id as idOferta, " + 
				"o.descripcion as oferta,do.estado as estado,do.id as id,d.version as versionDepartamento,"+
				"do.version as versionDepartamentoOferta,o.version as versionOferta " + 
				"from Departamento d " + 
				"INNER JOIN DepartamentoOferta do ON d.id = do.idDepartamento " + 
				"INNER JOIN Oferta o ON do.idOferta=o.id " + 
				"WHERE d.estado='AC' and do.estado='A' and o.estado='A'  " + 
				"and d.id = (?1) ")
		 List<CustomObjectMateriaOferta> listaOfertaDepartamento(Integer id);
		
		interface CustomObjectMateriaOferta { 
			Integer getIdDepartamento();
			String getDepartamento();
			Integer getIdOferta();
			String getOferta();
			String getEstado();
			Integer getId();
			Integer getVersionDepartamento();
			Integer getVersionDepartamentoOferta();
			Integer getVersionOferta();
			
		}
		
		//Listar Ofertas
		@Query("SELECT 0 as id, o.id as idOferta, 0 as idDistributivoGeneralVersion, o.estado as estado, "+
				" o.usuarioIngresoId as usuarioIngresoId from Oferta o " + 
				"WHERE o.estado='A' and idTipoOferta in(?1)")
		List<CustomObjectOferta> listaOfertasTipos(String tipos);
		
		interface CustomObjectOferta { 
			Integer getId();
			Integer getIdOferta();
			Integer getIdDistributivoGeneralVersion();
			String getEstado();
			Integer getUsuarioIngresoId();
		//	Integer getversion();
			
		}
		
		//consulta de oferta por id departamento y tipo de oferta
		@Query("SELECT d.id as idDepartamento,d.nombre as departamento,o.id as idOferta, " + 
				"o.descripcion as oferta,do.estado as estado,do.id as id,d.version as versionDepartamento,"+
				"do.version as versionDepartamentoOferta,o.version as versionOferta " + 
				"from Departamento d " + 
				"INNER JOIN DepartamentoOferta do ON d.id = do.idDepartamento " + 
				"INNER JOIN Oferta o ON do.idOferta=o.id " + 
				"INNER JOIN TipoOferta to ON o.idTipoOferta=to.id " + 
				"WHERE d.estado='AC' and do.estado='A' and o.estado='A' " + 
				"and d.id = (?1) and to.id= (?2)")
		 List<CustomObjectMateriaOferta> listaOfertaDepartamentoTipoOferta(Integer idDepartamento,Integer idTipoOferta);
		
		interface CustomObjectOfertaDepartamentoTipoOferta { 
			Integer getIdDepartamento();
			String getDepartamento();
			Integer getIdOferta();
			String getOferta();
			String getEstado();
			Integer getId();
			Integer getVersionDepartamento();
			Integer getVersionDepartamentoOferta();
			Integer getVersionOferta();
			
		}
		
		//consulta de oferta por id departamento y tipo de oferta para el Kanban
		@Query("SELECT o.id as dataField,o.descripcion as text " + 
				"from Departamento d " + 
				"INNER JOIN DepartamentoOferta do ON d.id = do.idDepartamento " + 
				"INNER JOIN Oferta o ON do.idOferta=o.id " + 
				"INNER JOIN TipoOferta to ON o.idTipoOferta=to.id " + 
				"WHERE d.estado='AC' and do.estado='A' and o.estado='A' " + 
				"and d.id = (?1) and to.id= (?2) "+
				"GROUP BY o.id,o.descripcion ")
		 List<CustomObjectMateriaOfertaK> listaOfertaDepartamentoKanban(Integer idDepartamento,Integer idTipoOferta);
		
		interface CustomObjectMateriaOfertaK { 
			String gettext();
			String getDataField();			
		}
		
		@Query("SELECT d.id as idDepartamento,d.nombre as departamento,o.id as idOferta, o.descripcion as oferta,"+
				" dov.id as idDistributivoOfertaVersion , dov.versionDistributivoOferta as versionDistributivoOferta,"+
				"dov.fechaDesde as fechaDesde, dov.fechaHasta as fechaHasta ,pac.id as idPeriodo, do.estado as estado,"+
				"d.version as versionDepartamento,do.version as versionDepartamentoOferta,o.version as versionOferta " + 
				"from Departamento d " + 
				"INNER JOIN DepartamentoOferta do ON d.id = do.idDepartamento " + 
				"INNER JOIN Oferta o ON do.idOferta=o.id " + 
				"INNER JOIN DistributivoOferta dio ON o.id=dio.idOferta " +
				"INNER JOIN DistributivoOfertaVersion dov ON dio.id=dov.idDistributivoOferta "+
				"INNER JOIN DistributivoGeneralVersion dgv ON dio.idDistributivoGeneralVersion=dgv.id "+
				"INNER JOIN DistributivoGeneral dge ON dgv.idDistributivoGeneral=dge.id "+
				"INNER JOIN PeriodoAcademico pac ON dge.idPeriodoAcademico=pac.id "+
				"WHERE d.estado='AC' and do.estado='A' and o.estado='A' and do.version = 0 and dge.estado='A' "+
				"and pac.id=(?1) "+
				"ORDER BY o.id")
		 List<CustomObjectDistributivosOferta> listaDistributivoOferta(	Integer idPeriodo);
		
		interface CustomObjectDistributivosOferta { 
			Integer getIdDepartamento();
			String getDepartamento();
			Integer getIdOferta();
			String getOferta();
			Integer getIdDistributivoOfertaVersion();
			Integer getVersionDistributivoOferta();
			String getFechaDesde();
			String getFechaHasta();
			Integer getIdPeriodo();
			String getEstado();
			Integer getVersionDepartamento();
			Integer getVersionDepartamentoOferta();
			Integer getVersionOferta();
			
		}
		
		
		@Query("SELECT d.id as idDepartamento,d.nombre as departamento,o.id as idOferta, " + 
				"o.descripcion as oferta,do.estado as estado,do.id as id,d.version as versionDepartamento,"+
				"do.version as versionDepartamentoOferta,o.version as versionOferta " + 
				"from Departamento d " + 
				"INNER JOIN DepartamentoOferta do ON d.id = do.idDepartamento " + 
				"INNER JOIN Oferta o ON do.idOferta=o.id " + 
				"INNER JOIN TipoOferta to ON o.idTipoOferta=to.id " + 
				"WHERE d.estado='AC' and do.estado='A' and o.estado='A' and do.version = 0 " + 
				" and to.id= (?1)")
		 List<CustomObjectMateriaOferta> listaOfertaDelTipoOferta(Integer idTipoOferta);
		interface CustomObjectOfertaDelTipoOferta { 
			Integer getIdDepartamento();
			String getDepartamento();
			Integer getIdOferta();
			String getOferta();
			String getEstado();
			Integer getId();
			Integer getVersionDepartamento();
			Integer getVersionDepartamentoOferta();
			Integer getVersionOferta();
			
		}
		
		@Query("SELECT d.id as idDepartamento,d.nombre as departamento,o.id as idOferta, o.descripcion as oferta, "+
				"dov.id as idDistributivoOfertaVersion , dov.versionDistributivoOferta as versionDistributivoOferta,"+
				"dov.fechaDesde as fechaDesde, dov.fechaHasta as fechaHasta,pac.id as idPeriodo,"+
				"do.estado as estado,d.version as versionDepartamento ,do.version as versionDepartamentoOferta,o.version as versionOferta " + 
				"from Departamento d " + 
				"INNER JOIN DepartamentoOferta do ON d.id = do.idDepartamento " + 
				"INNER JOIN Oferta o ON do.idOferta=o.id " + 
				"INNER JOIN TipoOferta to ON o.idTipoOferta=to.id "	+ 
				"INNER JOIN DistributivoOferta dio ON o.id=dio.idOferta " +
				"INNER JOIN DistributivoOfertaVersion dov ON dio.id=dov.idDistributivoOferta "+
				"INNER JOIN DistributivoGeneralVersion dgv ON dio.idDistributivoGeneralVersion=dgv.id "+
				"INNER JOIN DistributivoGeneral dge ON dgv.idDistributivoGeneral=dge.id "+
				"INNER JOIN PeriodoAcademico pac ON dge.idPeriodoAcademico=pac.id "+
				"WHERE d.estado='AC' and do.estado='A' and o.estado='A' and do.version = 0 and dge.estado='A' "+
				"and to.id= (?1) and pac.id=(?2) "+
				"ORDER BY o.id")
		 List<CustomObjectDistributivoOfertaDelTipoOferta> listaDistributivoOfertaDelTipoOferta(Integer idTipoOferta, 
				 											Integer idPeriodo);
		interface CustomObjectDistributivoOfertaDelTipoOferta { 
			Integer getIdDepartamento();
			String getDepartamento();
			Integer getIdOferta();
			String getOferta();
			Integer getIdDistributivoOfertaVersion();
			Integer getVersionDistributivoOferta();
			String getFechaDesde();
			String getFechaHasta();
			Integer getIdPeriodo();
			String getEstado();
			Integer getVersionDepartamento();
			Integer getVersionDepartamentoOferta();
			Integer getVersionOferta();
			
		}
		
		
		@Query("SELECT ofe FROM TipoOferta tof "	+ 
				"INNER JOIN Oferta ofe ON tof.id=ofe.idTipoOferta " +
				"INNER JOIN DepartamentoOferta dof on ofe.id=dof.idOferta "+
				"WHERE tof.estado='A' and ofe.estado='A' and dof.estado='A' and tof.id=?1 ")
		 List<Oferta> listaOfertaPre(Integer idTipoOferta);
	/*	@Query("SELECT ofe FROM TipoOferta tof "	+ 
				"INNER JOIN Oferta ofe ON tof.id=ofe.idTipoOferta " +
				"INNER JOIN DepartamentoOferta dof on ofe.id=dof.idOferta "+
				"WHERE tof.estado='A' and ofe.estado='A' and dof.estado='A' and tof.id=?1 ")
		 List<Oferta> listaOfertaPre(Integer idTipoOferta);*/
		
		/*
		 * Servicio que recupera el nombre de la facultad y la carrera a través del id_oferta
		 * */
		@Query(value=" select o.id as idOferta,do.idDepartamento as idDepartamento, d.nombre as facultad,o.descripcion as carrera " +
				" from DepartamentoOferta do " + 
				" inner join Oferta o on o.id = do.idOferta " + 
				" inner join Departamento d on d.id = do.idDepartamento " + 
				" inner join Malla m on do.id = m.idDepartamentoOferta " + 
				" where m.id = (?1)")
		List<CustObjCarrera> recuperarNombresCarreraFacultad(Integer idMalla);
		interface CustObjCarrera{
			Integer getIdOferta();
			Integer getIdDepartamento();
			String getFacultad();
			String getCarrera();
		}
		
		
	
}
