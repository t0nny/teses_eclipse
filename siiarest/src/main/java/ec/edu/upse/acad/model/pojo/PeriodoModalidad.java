package ec.edu.upse.acad.model.pojo;




import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import ec.edu.upse.acad.model.pojo.PeriodoAcademico;


@Entity
@Table(schema="aca", name="periodo_modalidad")
//@Where(clause = "estado = A")

@NoArgsConstructor
public class PeriodoModalidad {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_periodo_modalidad")
	@Getter @Setter private Integer id;
	
	@Column(name="id_periodo_academico")
	@Getter @Setter private Integer idPeriodoAcademico;
	
	@Column(name="id_modalidad")
	@Getter @Setter private Integer idModalidad;
	
//	@Column(name="fecha_ingreso")
//	@Getter @Setter  private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter  private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;
	
	@Getter @Setter  private String estado;
	
	//RELACIONES

	//bi-directional many-to-one association to Periodo
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_periodo_academico", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private PeriodoAcademico periodoAcademico;
	
	//bi-directional many-to-one association to Modalidad
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_modalidad", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Modalidad modalidad;
	
		
	@PrePersist
	void preInsert() {
	   if (this.estado == null)
	       this.estado = "A";
 }
}

