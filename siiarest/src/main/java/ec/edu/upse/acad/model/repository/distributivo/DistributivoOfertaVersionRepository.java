package ec.edu.upse.acad.model.repository.distributivo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.distributivo.DistributivoOfertaVersion;
@Repository
public interface DistributivoOfertaVersionRepository extends JpaRepository<DistributivoOfertaVersion, Integer>{
	
	@Query(value="SELECT e "
			+ "FROM DistributivoOfertaVersion AS e "
			+ "WHERE e.id=?1 ")
	List<DistributivoOfertaVersion> buscarPorId(Integer id);
	
	@Query(value="SELECT  COUNT(dd.id) as cantDistrVacio "+
			"from DistributivoOfertaVersion dov " + 
			"inner join DistributivoDocente dd on dov.id=dd.idDistributivoOfertaVersion " + 
			"where dd.idDocente is null  and dov.id=?1 " + 
			"and dov.estado in ('A','D','V','P') and dd.estado='A' " + 
			"GROUP BY dov.id")
	CustObjCantDistributivoVacio buscarDistributivoDocVacio(Integer id);
	interface CustObjCantDistributivoVacio  { 
		Integer getCantDistrVacio(); 
		
	}
	
	@Query(value="SELECT  ma.id as idMallaAsignatura, a.descripcion as asignatura , par.id as idParalelo," +
			" case when niv.orden is null then null else concat (cast(niv.orden as text),'/', par.descripcionCorta) end as paralelo"+
			" FROM PlanificacionParalelo pp " + 
			"INNER JOIN MallaAsignatura ma on pp.idMallaAsignatura=ma.id " + 
			"INNER JOIN Asignatura a on ma.idAsignatura=a.id " + 
			"INNER JOIN Malla m on ma.idMalla=m.id " + 
			"INNER JOIN PeriodoMalla pm on m.id=pm.idMalla and pp.idPeriodoAcademico=pm.idPeriodoAcademico " + 
			"INNER JOIN Paralelo as par on par.orden<= isnull (pp.numParalelos,0) " + 
			"INNER JOIN Nivel  niv on ma.idNivel=niv.id "+
			"where pp.idPeriodoAcademico=?1 " + 
			"and pp.estado='A' and ma.estado='A' and a.estado='A' and m.estado in ('A', 'P') and pm.estado='A' and par.estado='A' and niv.estado='A' " + 
			"and pp.idMallaAsignatura not in (SELECT aa.idMallaAsignatura "+
			"								FROM DistributivoOfertaVersion dov " + 
			"								INNER JOIN DistributivoDocente dd on dov.id=dd.idDistributivoOfertaVersion " + 
			"								INNER JOIN DocenteAsignaturaAprend daa on dd.id=daa.idDistributivoDocente " + 
			"								INNER JOIN AsignaturaAprendizaje aa on daa.idAsignaturaAprendizaje=aa.id  " + 
			"								WHERE dov.estado in ('A', 'D', 'V', 'P') and dd.estado ='A' and daa.estado='A' and aa.estado='A' "+
			"								and dov.id=?2" + 
			"								GROUP BY aa.idMallaAsignatura  ) " + 
			"and par.id not in(select daa.idParalelo FROM DistributivoOfertaVersion dov " + 
			"								INNER JOIN DistributivoDocente dd on dov.id=dd.idDistributivoOfertaVersion " + 
			"								INNER JOIN DocenteAsignaturaAprend daa on dd.id=daa.idDistributivoDocente " + 
			"								INNER JOIN AsignaturaAprendizaje aa on daa.idAsignaturaAprendizaje=aa.id " + 
			"								INNER JOIN MallaAsignatura ma1 on aa.idMallaAsignatura=ma1.id " + 
			"								WHERE dov.estado in ('A', 'D', 'V', 'P') and dd.estado ='A' and daa.estado='A'  " + 
			"								and aa.estado='A' and aa.idMallaAsignatura=ma.id and dov.id=?2 " + 
			"								GROUP BY ma1.id, daa.idParalelo )" + 
			"group by ma.id, a.descripcion, par.id, niv.orden,par.descripcionCorta")
	List<CustObjMallaAsigFaltanAsignar> buscarMallaAsigFaltanAsignar(Integer idPeriodoAca,Integer idDistOferVer);
	interface CustObjMallaAsigFaltanAsignar  { 
		Integer getIdMallaAsignatura(); 
		String getAsignatura();
		String getIdParalelo();
		String getParalelo();
		
	}
}
