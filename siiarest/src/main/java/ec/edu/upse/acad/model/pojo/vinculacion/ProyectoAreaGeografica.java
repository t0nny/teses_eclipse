package ec.edu.upse.acad.model.pojo.vinculacion;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema = "vin", name = "proyecto_area_geografica")

@NoArgsConstructor //un constructorsin argumentos
public class ProyectoAreaGeografica {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_proyecto_area_geo")
	@Getter	@Setter	private Integer id;

	@Column(name = "id_area_geografica")
	@Getter	@Setter	private String idAreaGeografica;
	
	@Column(name = "barrio_comuna")
	@Getter	@Setter	private boolean barrioComuna;
	
	@Getter	@Setter	private String descripcion;
	
	@Column(name = "usuario_ingreso_id")
	@Getter	@Setter	private String usuarioIngresoId;

	@Version
	@Getter	@Setter	private Integer version;

	@Getter	@Setter	private String estado;
	
	// bi-directional many-to-one association to InstitucionBeneficiaria
	/*@OneToMany(mappedBy = "proyectoAreaGeografica", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JsonManagedReference
	@Getter	@Setter	private List<InstitucionBeneficiaria> institucionBeneficiaria;*/
	
	@PrePersist
	void preInsert() {
		if (this.estado == null)
			this.estado = "A";
	}

	
	@Override
	public String toString() {
		return "ProyectoAreaGeografica [id=" + id + ", idAreaGeografica=" + idAreaGeografica  + ", descripcion=" + descripcion + ", usuarioIngresoId=" + usuarioIngresoId
				+ ", version=" + version + ", estado=" + estado + "]";
	}

	
}
