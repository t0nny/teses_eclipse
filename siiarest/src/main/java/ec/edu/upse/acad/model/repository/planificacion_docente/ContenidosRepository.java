package ec.edu.upse.acad.model.repository.planificacion_docente;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ec.edu.upse.acad.model.pojo.planificacion_docente.Contenido;

public interface ContenidosRepository extends JpaRepository<Contenido, Integer>{

//	@Query(value="select distinct a "
//			+ "from Asignatura a "
//			+ "join OfertaAsignatura oa on oa.idAsignatura = a.id "
//			+ "WHERE a.id = ?1 and oa.estado='A' and a.estado='A' ")
//	List<CO_As> buscaAsignaturaById(Integer id);
//	interface CO_As{
//		Integer getId();
//		String getCodigo();
//		String getTipoAsignatura();
//		String getDescripcion();
//		String getDescripcionCorta();
//		Date getFechaDesde();
//		Date getFechaHasta();
//		ArrayList<CO_Oa> getOfertaAsignatura();
//	}
//	interface CO_Oa{
//		Integer getIdOferta();
//		String getEstado();
//	}
}
