package ec.edu.upse.acad.model.service;

import java.util.List;

//import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.upse.acad.model.pojo.planificacion_docente.AreaBibliografia;
import ec.edu.upse.acad.model.pojo.planificacion_docente.AsignaturaBibliografia;
import ec.edu.upse.acad.model.pojo.planificacion_docente.Contenido;
import ec.edu.upse.acad.model.pojo.planificacion_docente.ContenidosCompAprendizaje;
import ec.edu.upse.acad.model.pojo.planificacion_docente.PlanBibliografia;
import ec.edu.upse.acad.model.pojo.planificacion_docente.PlanClase;
import ec.edu.upse.acad.model.pojo.planificacion_docente.PlanEstrategia;
import ec.edu.upse.acad.model.pojo.planificacion_docente.PlanMetodologia;
import ec.edu.upse.acad.model.pojo.planificacion_docente.RecursoBibliografico;
import ec.edu.upse.acad.model.pojo.planificacion_docente.Syllabus;
import ec.edu.upse.acad.model.repository.planificacion_docente.ContenidoCompAprendizajeRepository;
import ec.edu.upse.acad.model.repository.planificacion_docente.ContenidosRepository;
import ec.edu.upse.acad.model.repository.planificacion_docente.PlanClaseRepository;
import ec.edu.upse.acad.model.repository.planificacion_docente.RecursoBibliograficoRepository;
import ec.edu.upse.acad.model.repository.planificacion_docente.SyllabusRepository;

@Service
@Transactional
public class SyllabusService {

	@Autowired private SyllabusRepository syllabusRepository;
	@Autowired private ContenidosRepository contenidosRepository;
	@Autowired private ContenidoCompAprendizajeRepository contenidoCompAprendizajeRepository;
	@Autowired private PlanClaseRepository planClaseRepository;
	@Autowired private RecursoBibliograficoRepository recursoBibliograficoRepository;

	@PersistenceContext
	private EntityManager em;

	public void grabarSyllabus(Syllabus syllabus){
		if(syllabus.getId()!=null) {
			//colocar proceso para edicion
			this.editaSyllabus(syllabus);
		}else {
			//colocar proceso para nuevo
			this.nuevoSyllabus(syllabus);
		}
	}

	/**
	 * Inserta un nuevo registro en la tabla silabo y contenidos.
	 * 
	 * @param syllabus Objeto que se recibe desde el front-end
	 */
	public void nuevoSyllabus(Syllabus syllabus) {
		Syllabus _syllabus = new Syllabus();
		BeanUtils.copyProperties(syllabus, _syllabus, "contenido","contenidos");
		Integer id_syllabus = syllabusRepository.saveAndFlush(_syllabus).getId();		
		//iteracion para insertar contenido
		List<Contenido> __contenido = syllabus.getContenido();
		for(int l=0;l<__contenido.size();l++) {
			Integer aux_idContenido = this.addContenido(__contenido, l, id_syllabus,0);
			//this.addContenidoComponenteAprendizaje(__contenido, l, aux_idContenido);
			List<Contenido> __subContenido = __contenido.get(l).getContenido();
			for(int j=0;j<__subContenido.size();j++) {//iteracion para insertar subcontenidos
				Integer aux_idSubContenido = this.addContenido(__subContenido, j, id_syllabus, aux_idContenido);
				//this.addContenidoComponenteAprendizaje(__contenido, j, aux_idSubContenido);
			}
		}
		em.refresh(_syllabus);
	}//revisar el agregado a la tabla contenido componente aprendizaje('aun no inserta en esta tabla)
	//revisar el metodo --> addContenidoComponenteAprendizaje()


	/**
	 * Método que me permite editar un syllabus y sus contenidos
	 * 
	 * @param syllabus
	 */
	public void editaSyllabus(Syllabus syllabus) {
		Integer idSyllabus = syllabus.getId();
		List<Contenido> _contenidoUnidades = syllabus.getContenido();
		for(int l=0;l<_contenidoUnidades.size();l++) {//iteracion para el recorrido de unidades
			Integer idContenido = _contenidoUnidades.get(l).getId();			
			if(idContenido != null) {//en tal caso que un contenido principal se haya solo modificado, este vendra con su respectivo id
				this.modifContenidos(idContenido, _contenidoUnidades, l);
				List<Contenido> rContenido = _contenidoUnidades.get(l).getContenido();
				for(int j=0;j<rContenido.size();j++) {//iteracion para el recorrido de sub unidades
					Integer idSubContenido = rContenido.get(j).getId();
					if(idSubContenido != null) {//en tal caso que un subcontenido se haya modificado, este vendra con su respectivo id
						this.modifContenidos(idSubContenido, rContenido, j);
					}else {//en tal caso que se haya agregado un nuevo sub contenido
						//this.addContenido(rContenido, j, idSyllabus, idSubContenido);
						this.addContenido(rContenido, j, idSyllabus, idContenido);
					}
				}
			}else {//si se agrego un nuevo contenido
				Integer idNewContenido =  this.addContenido(_contenidoUnidades, l, idSyllabus, 0);
				List<Contenido> newSubContenido = _contenidoUnidades.get(l).getContenido();
				for(int j=0;j<newSubContenido.size();j++) {
					this.addContenido(newSubContenido, j, idSyllabus, idNewContenido);
				}
			}
		}
	}//revisar el agregado a la tabla contenido componente aprendizaje('aun no inserta en esta tabla)
	//revisar el metodo --> addContenidoComponenteAprendizaje()

	/**
	 * Método que me permite agregar nuevo contenido a un syllabus
	 * 
	 * @param __contenido Lista de contenidos
	 * @param cont Posición de la lista de contenidos
	 * @param idSyllabus Identificador del Syllabus como cabecera
	 * @param idPadre Identificador del padre para los sub contenidos
	 * @return Devuelve un identificador en caso de que el contenido guardado sea padre, caso contrario solo devuelve 0.
	 */
	public Integer addContenido(List<Contenido> __contenido,Integer cont,Integer idSyllabus, Integer idPadre) {
		//Integer idComponenteAprendizaje = syllabus.getContenido().get(l).getIdComponenteAprendizaje(); //habilitar cuando se inserte el valor desde el front end
		Integer horasDocencia = __contenido.get(cont).getHorasDocencia();
		Integer horasAEA = __contenido.get(cont).getHorasAEA();
		Integer horasTA = __contenido.get(cont).getHorasTA();
		//Integer orden = syllabus.getContenido().get(l).getOrden(); //habilitar cuando se inserte el valor desde el front end
		Integer orden = 1;
		String descripcion = __contenido.get(cont).getDescripcion();
		String resultadoAprendizaje = __contenido.get(cont).getResultadoAprendizaje();
		Contenido contenido = new Contenido();

		contenido.setHorasDocencia(horasDocencia);
		contenido.setHorasAEA(horasAEA);
		contenido.setHorasTA(horasTA);
		contenido.setOrden(orden);
		contenido.setDescripcion(descripcion);
		contenido.setResultadoAprendizaje(resultadoAprendizaje);
		contenido.setIdSilabo(idSyllabus);
		if(idPadre != 0) {
			contenido.setIdContenidoPadre(idPadre);
		}
		return contenidosRepository.saveAndFlush(contenido).getId();
	}

	/**
	 * Método que me permite modificar contenidos de un syllabus determinado de acuerdo a un identificador
	 *  
	 * @param idContenido Identificador de contenido.
	 * @param lst_Contenidos Lista de contenidos.
	 * @param cont Posición de la lista de contenidos.
	 */
	public void modifContenidos(Integer idContenido,List<Contenido> lst_Contenidos, Integer cont) {
		Contenido myContenido = contenidosRepository.findById(idContenido).get();//objeto recuperado
		String rDescripcion = lst_Contenidos.get(cont).getDescripcion();
		String rResultadoAprendizaje = lst_Contenidos.get(cont).getResultadoAprendizaje();
		Integer rHorasDocencia = lst_Contenidos.get(cont).getHorasDocencia();
		Integer rHorasAEA = lst_Contenidos.get(cont).getHorasAEA();
		Integer rHorasTA = lst_Contenidos.get(cont).getHorasTA();
		String rEstado = lst_Contenidos.get(cont).getEstado();
		Integer rUsuarioIngresoId = lst_Contenidos.get(cont).getUsuarioIngresoId();		
		//setear valores al objeto recuperado para la respectiva actualizacion
		myContenido.setDescripcion(rDescripcion);
		myContenido.setResultadoAprendizaje(rResultadoAprendizaje);
		myContenido.setHorasDocencia(rHorasDocencia);
		myContenido.setHorasAEA(rHorasAEA);
		myContenido.setHorasTA(rHorasTA);
		myContenido.setEstado(rEstado);
		myContenido.setUsuarioIngresoId(rUsuarioIngresoId);		
		contenidosRepository.saveAndFlush(myContenido);		
	}

	//revisar este metodo(ultimos ajustes)
	public void addContenidoComponenteAprendizaje(List<Contenido> __contenido, Integer cont, Integer idContenido) {
		ContenidosCompAprendizaje contenidosCompAprendizaje = new ContenidosCompAprendizaje();		
		if(__contenido.get(cont).getContenidoCompAprendizaje() != null) {
			for(int t=0;t<__contenido.get(cont).getContenidoCompAprendizaje().size();t++) {
				if(__contenido.get(cont).getHorasDocencia() != 0) {
					contenidosCompAprendizaje.setIdContenidos(idContenido);
					contenidosCompAprendizaje.setIdComponenteAprendizaje(1);//para docencia el id es 1
					//verificar que se cumpla tambien si es virtual o presencial.
				}

				if(__contenido.get(cont).getHorasAEA() != 0) {
					contenidosCompAprendizaje.setIdContenidos(idContenido);
					contenidosCompAprendizaje.setIdComponenteAprendizaje(3);//para pae el id es 3
					//verificar que se cumpla tambien si es asistido por el docente o no.
				}

				if(__contenido.get(cont).getHorasTA() != 0) {
					contenidosCompAprendizaje.setIdContenidos(idContenido);
					contenidosCompAprendizaje.setIdComponenteAprendizaje(5);//para ta el id es 5
				}

				//aqui el repositorio
				contenidoCompAprendizajeRepository.save(contenidosCompAprendizaje);
			}
		}
	}

	public void grabarPlanClase(PlanClase planClase) {

		Long idPlanClase = planClase.getId();
		if (idPlanClase == null) {
			PlanClase _planClase = new PlanClase();
			BeanUtils.copyProperties(planClase, _planClase,"planBibliografias","planEstrategias","planMetodologias");
			idPlanClase  = planClaseRepository.saveAndFlush(_planClase).getId();
		}
		//Iteración para planBibliografias
		for (int i = 0; i < (planClase.getPlanBibliografias().size()); i++) {
			planClase.getPlanBibliografias().get(i).setIdPlanClase(idPlanClase);
		}

		//Iteración para planEstrategias
		for (int i = 0; i < (planClase.getPlanEstrategias().size()); i++) {
			planClase.getPlanEstrategias().get(i).setIdPlanClase(idPlanClase);
		}

		//Iteración para planMetodologias
		for (int i = 0; i < (planClase.getPlanMetodologias().size()); i++) {
			planClase.getPlanMetodologias().get(i).setIdPlanClase(idPlanClase);
		}

		PlanClase _planClaseV = new PlanClase();
		_planClaseV = planClaseRepository.findById(idPlanClase).get();
		Integer version = _planClaseV.getVersion();
		planClase.setId(idPlanClase);
		planClase.setVersion(version);
		planClaseRepository.save(planClase);

		em.getEntityManagerFactory().getCache().evict(PlanClase.class);
		em.getEntityManagerFactory().getCache().evict(PlanBibliografia.class);
		em.getEntityManagerFactory().getCache().evict(PlanMetodologia.class);
		em.getEntityManagerFactory().getCache().evict(PlanEstrategia.class);	
	}

	public void grabarRecursoBibliografico(RecursoBibliografico recursoBibliografico) {

		Long idRecursoBibliografico = recursoBibliografico.getId();
		if (idRecursoBibliografico == null) {
			RecursoBibliografico _recursoBibliografico = new RecursoBibliografico();
			BeanUtils.copyProperties(recursoBibliografico, _recursoBibliografico,"planBibliografias","areaBibliografias","asignaturaBibliografias");
			idRecursoBibliografico  = recursoBibliograficoRepository.saveAndFlush(_recursoBibliografico).getId();
		}
		//Iteración para areaBibliografias
		for (int i = 0; i < (recursoBibliografico.getAreaBibliografias().size()); i++) {
			recursoBibliografico.getAreaBibliografias().get(i).setIdRecursoBibliografico(idRecursoBibliografico);
		}

		//Iteración para asignaturaBibliografias
		for (int i = 0; i < (recursoBibliografico.getAsignaturaBibliografias().size()); i++) {
			recursoBibliografico.getAsignaturaBibliografias().get(i).setIdRecursoBibliografico(idRecursoBibliografico);
		}

		RecursoBibliografico _recursoBibliograficoV = new RecursoBibliografico();
		_recursoBibliograficoV = recursoBibliograficoRepository.findById(idRecursoBibliografico).get();
		Integer version = _recursoBibliograficoV.getVersion();
		recursoBibliografico.setId(idRecursoBibliografico);
		recursoBibliografico.setVersion(version);
		recursoBibliograficoRepository.save(recursoBibliografico);

		em.getEntityManagerFactory().getCache().evict(RecursoBibliografico.class);
		em.getEntityManagerFactory().getCache().evict(AreaBibliografia.class);
		em.getEntityManagerFactory().getCache().evict(AsignaturaBibliografia.class);
		em.getEntityManagerFactory().getCache().evict(PlanBibliografia.class);	
	}

	//Funcion para borrar por completo una bibligrafia
	public void borrarBibliografia(Long idRecursoBibliografico) {
		RecursoBibliografico recursoBibliografico =  recursoBibliograficoRepository.findById(idRecursoBibliografico).get();
		if (recursoBibliografico !=null) {
			recursoBibliografico.setEstado("I");
			if(recursoBibliografico.getPlanBibliografias()!=null) {
				for (int i = 0; i < recursoBibliografico.getPlanBibliografias().size(); i++) {
					PlanBibliografia  _planBibliografia =  recursoBibliografico.getPlanBibliografias().get(i);
					if (_planBibliografia.getId() != null)
						_planBibliografia.setEstado("I");
				}
			}
			for (int i = 0; i < recursoBibliografico.getAreaBibliografias().size(); i++) {
				AreaBibliografia  _areaBibliografia =  recursoBibliografico.getAreaBibliografias().get(i);
				if (_areaBibliografia.getId() != null)
					_areaBibliografia.setEstado("I");
			}
			for (int i = 0; i < recursoBibliografico.getAsignaturaBibliografias().size(); i++) {
				AsignaturaBibliografia  _asignaturaBibliografia =  recursoBibliografico.getAsignaturaBibliografias().get(i);
				if (_asignaturaBibliografia.getId() != null)
					_asignaturaBibliografia.setEstado("I");
			}
			recursoBibliograficoRepository.save(recursoBibliografico);
		}
	}
	
	//Funcion para borrar por completo un Plan de Clase
	public void borrarPlanClase(Long idPlanClase) {
		PlanClase planClase =  planClaseRepository.findById(idPlanClase).get();
		if (planClase !=null) {
			planClase.setEstado("I");
			if(planClase.getPlanBibliografias()!=null) {
				for (int i = 0; i < planClase.getPlanBibliografias().size(); i++) {
					PlanBibliografia  _planBibliografia =  planClase.getPlanBibliografias().get(i);
					if (_planBibliografia.getId() != null)
						_planBibliografia.setEstado("I");
				}
			}
			for (int i = 0; i < planClase.getPlanEstrategias().size(); i++) {
				PlanEstrategia  _planEstrategia =  planClase.getPlanEstrategias().get(i);
				if (_planEstrategia.getId() != null)
					_planEstrategia.setEstado("I");
			}
			for (int i = 0; i < planClase.getPlanMetodologias().size(); i++) {
				PlanMetodologia  _planMetodologia =  planClase.getPlanMetodologias().get(i);
				if (_planMetodologia.getId() != null)
					_planMetodologia.setEstado("I");
			}
			planClaseRepository.save(planClase);
		}
	}

}
