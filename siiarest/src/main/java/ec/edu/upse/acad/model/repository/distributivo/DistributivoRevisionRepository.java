package ec.edu.upse.acad.model.repository.distributivo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.distributivo.DistributivoRevision;
@Repository
public interface DistributivoRevisionRepository extends JpaRepository<DistributivoRevision, Integer> {

}
