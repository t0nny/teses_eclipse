package ec.edu.upse.acad.model.pojo.matricula;

import javax.persistence.*;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ec.edu.upse.acad.model.pojo.PeriodoAcademico;
import ec.edu.upse.acad.model.pojo.TipoOferta;
import ec.edu.upse.acad.model.pojo.reglamento.Reglamento;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;


@Entity
@Table(schema="aca", name="matricula_general")
@Where(clause = "estado='A'")
@NoArgsConstructor
public class MatriculaGeneral{
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_matricula_general")
	@Getter @Setter private Integer id;
	
	@Column(name="id_periodo_academico")
	@Getter @Setter  private Integer idPeriodoAcademico;
	
	@Column(name="id_reglamento")
	@Getter @Setter  private Integer idReglamento;
	
//	@Column(name="id_tipo_oferta")
//	@Getter @Setter  private Integer idTipoOferta;
	
	/*@Column(name="matricula_nivel")
	@Getter @Setter private Boolean matriculaNivel;
	*/
	@Column(name="fecha_desde")
	@Getter @Setter private Date fechaDesde;

	@Column(name="fecha_hasta")
	@Getter @Setter  private Date fechaHasta;
	
	@Getter @Setter private String estado;

//	@Column(name="fecha_ingreso")
//	@Getter @Setter  private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private int version;

	//RELACIONES
	//bi-directional many-to-one association to PeriodoAcademico
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_periodo_academico" , insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private PeriodoAcademico periodoAcademico;

	//bi-directional many-to-one association to Reglamento
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_reglamento" , insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private Reglamento reglamento;

	//bi-directional many-to-one association to TipoOferta
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="id_tipo_oferta" , insertable=false, updatable = false)
//	@JsonIgnore
//	@Getter @Setter private TipoOferta tipoOferta;

	//bi-directional many-to-one association to TipoMatriculaFecha
	@OneToMany(mappedBy="matriculaGeneral", cascade=CascadeType.ALL)
	@Getter @Setter private List<TipoMatriculaFecha> tipoMatriculaFechas;
	
	//bi-directional many-to-one association to EstudianteMatricula
	@OneToMany(mappedBy="matriculaGeneral", cascade=CascadeType.ALL)
	private List<EstudianteMatricula> estudianteMatriculas;


	 @PrePersist
		void preInsert() {
		   if (this.estado == null)
		       this.estado = "A";
	 }


	
}