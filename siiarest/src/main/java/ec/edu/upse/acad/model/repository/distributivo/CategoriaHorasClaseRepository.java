package ec.edu.upse.acad.model.repository.distributivo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.reglamento.DedicacionHorasClase;
@Repository
public interface CategoriaHorasClaseRepository extends JpaRepository<DedicacionHorasClase, Integer> {

}
