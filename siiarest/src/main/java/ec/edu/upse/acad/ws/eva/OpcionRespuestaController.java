package ec.edu.upse.acad.ws.eva;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.upse.acad.model.repository.evaluacion.OpcionPreguntaRepository;
import ec.edu.upse.acad.model.repository.evaluacion.OpcionRespuestaRepository;
import ec.edu.upse.acad.model.service.SecurityService;

@RestController
@RequestMapping("/api/opciones")
@CrossOrigin
public class OpcionRespuestaController {
	@Autowired private OpcionRespuestaRepository opcionRespuestaRepository;
	@Autowired private OpcionPreguntaRepository opcionPreguntaRepository;
	@Autowired private SecurityService securityService;


	//****************** SERVICIOS PARA OPCIONES ************************//

	@RequestMapping(value="/listarOpcionRespuestaPregunta", method=RequestMethod.GET)
	public ResponseEntity<?> listarOpcionPreguntaRespuesta(@RequestHeader(value="Authorization") String Authorization) {
		if (!securityService.isTokenValido(Authorization)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		return ResponseEntity.ok(opcionRespuestaRepository.listarOpcionRespuestaPregunta());
	}
}
