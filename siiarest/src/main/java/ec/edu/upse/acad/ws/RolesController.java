package ec.edu.upse.acad.ws;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.upse.acad.model.pojo.seguridad.Rol;

import ec.edu.upse.acad.model.repository.RolesRepository;


@RestController
@RequestMapping("/api/roles")
@CrossOrigin

public class RolesController {

	@Autowired private RolesRepository rolesRepository;

	//****************** SERVICIOS PARA ROLES************************//

	/**

	//Buscar todos los modulos, para listar en angular
	 * Buscar todos los modulos, para listar en angular
	 * @param authorization de acuerdo al rol aasignado
	 * @return visualiza los tipos de roles
	 */
	@RequestMapping(value="/buscarRoles", method=RequestMethod.GET)
	public ResponseEntity<?> buscarRoles() {
		return ResponseEntity.ok(rolesRepository.findAll());
	}

	// buscar Rol por Id

	@RequestMapping(value="/buscarRol/{id}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarRol(@PathVariable("id") Integer id) {
		Rol rol;
		if (rolesRepository.findById(id).isPresent()) {
			rol = rolesRepository.findById(id).get();
			return ResponseEntity.ok(rol);
		}else {
			return ResponseEntity.notFound().build();
		}
	}

	@RequestMapping(value="/buscarRolesUsuarioModulos/{usuario}/{modulo_id}", 
			method=RequestMethod.GET)
	public ResponseEntity<?> buscarModulos(@PathVariable("usuario") String usuario,
			@PathVariable("modulo_id") Integer modulo_id) {
		return ResponseEntity.ok(rolesRepository.buscarRolesUsuarioModulos(usuario,modulo_id));
	}


}
