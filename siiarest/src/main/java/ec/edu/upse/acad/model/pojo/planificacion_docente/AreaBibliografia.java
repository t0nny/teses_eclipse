package ec.edu.upse.acad.model.pojo.planificacion_docente;

import javax.persistence.*;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ec.edu.upse.acad.model.pojo.AreaConocimiento;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;



/**
 * The persistent class for the area_bibliografia database table.
 * 
 */
@Entity
@Table(schema="aca", name="area_bibliografia")
@Where(clause = "estado='A'")
@NoArgsConstructor
public class AreaBibliografia  {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_area_bibliografia")
	@Getter @Setter private Integer id;
	
	@Column(name="id_area_conocimiento")
	@Getter @Setter private Integer idAreaConocimiento;
	
	@Column(name="id_recurso_bibliografico")
	@Getter @Setter private Long idRecursoBibliografico;
	
	@Getter @Setter private String estado;

//	@Column(name="fecha_ingreso")
//	private Timestamp fechaIngreso;

	@Column(name="usuario_ingreso_id")
	@Getter @Setter private String usuarioIngresoId;

	@Version
	@Getter @Setter private Integer version;

	//RELACIONES
	//bi-directional many-to-one association to AreaConocimiento
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_area_conocimiento", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private AreaConocimiento areaConocimiento;

	//bi-directional many-to-one association to RecursoBibliografico
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_recurso_bibliografico", insertable=false, updatable = false)
	@JsonIgnore
	@Getter @Setter private RecursoBibliografico recursoBibliografico;
	
	@PrePersist
	void preInsert() {
	   if (this.estado == null)
	       this.estado = "A";
	}

}