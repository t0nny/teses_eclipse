package ec.edu.upse.acad.model.repository.matricula;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ec.edu.upse.acad.model.pojo.matricula.MatriculaGeneral;
import ec.edu.upse.acad.model.pojo.matricula.TipoMatriculaFecha;

@Repository
public interface TipoMatriculaFechaRepository extends JpaRepository<TipoMatriculaFecha, Integer>{

	@Query(value=" select vg.id as idValidacionGeneral,vg.numDiasAnularAsig as numDiasAnularAsig ,vg.numMaxCreditos as numMaxCreditos, " + 
			" vg.numMaxNivelTomados as numMaxNivelTomados,vg.horasMaxSemanaComponentes as horasMaxSemanaComponentes,vg.horasMaxSemanaDocencia as horasMaxSemanaDocencia, " + 
			" vg.calificacionMinAprobar  as calificacionMinAprobar from ValidacionGeneral vg where vg.idReglamento = (?1) ")
	List<CustomObjectValidacion> recuperarValidacionesReglamento(Integer idReglamento);

	interface CustomObjectValidacion{ 
		Integer getIdValidacionGeneral(); 
		Integer getNumDiasAnularAsig(); 
		Integer getNumMaxCreditos(); 
		Integer getNumMaxNivelTomados();
		Integer getHorasMaxSemanaComponentes();
		Integer getHorasMaxSemanaDocencia();
		Double getCalificacionMinAprobar();
	} 
	
	@Query(value=" select r.id as idReglamento,r.nombre as nombreReglamento,tr.codigo as codigoTipoReglamento," + 
			" tof.descripcionCorta as codigoTipoOferta from Reglamento r" + 
			" inner join TipoReglamento tr on r.idTipoReglamento = tr.id " + 
			" inner join ReglamentoTipoOferta rto on r.id = rto.idReglamento " + 
			" inner join TipoOferta tof on tof.id = rto.idTipoOferta " + 
			" where tr.codigo = 'MAT' and tof.descripcionCorta ='PRE' and r.estado= 'A' ")
	List<CustomObjectPeriodoMatricula> listarReglamentosMatricula();

	interface CustomObjectPeriodoMatricula{ 
		Integer getIdReglamento(); 
		String getNombreReglamento(); 
		String getCodigoTipoReglamento(); 
		String getCodigoTipoOferta();
	} 
	
	
	@Query(value=" select tm.id as idTipoMatricula,tm.codigo as codigoTipo,tm.descripcion as nombreTipoMatricula from TipoMatricula tm "
			+ " where tm.codigo in ('ORD','EXT','ESP')")
	List<CustomObjectTipoMat> listarTiposMatricula();

	interface CustomObjectTipoMat{ 
		Integer getIdTipoMatricula(); 
		String getCodigoTipo(); 
		String getNombreTipoMatricula();
	} 
	
	@Query(value=" select pa.id as idPeriodoAcademico,pa.codigo as codigoPeriodo,pa.descripcion as descripcionPeriodo,pa.fechaDesde as fechaDesde, " + 
			" pa.fechaHasta as fechaHasta from PeriodoAcademico pa where cast(CURRENT_TIMESTAMP as date) >= pa.fechaDesde and cast(CURRENT_TIMESTAMP as date)<=pa.fechaHasta")
	List<CustomObjectPeriodo> recuperarUltimoPeriodoAcademico();

	interface CustomObjectPeriodo{ 
		Integer getIdPeriodoAcademico(); 
		String getCodigoPeriodo(); 
		String getDescripcionPeriodo();
		Date getFechaDesde();
		Date getFechaHasta();
	} 
	
}
