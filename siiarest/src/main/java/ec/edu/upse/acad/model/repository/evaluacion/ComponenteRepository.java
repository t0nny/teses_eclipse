package ec.edu.upse.acad.model.repository.evaluacion;

import org.springframework.data.jpa.repository.JpaRepository;

import ec.edu.upse.acad.model.pojo.evaluacion.Componente;

public interface ComponenteRepository extends JpaRepository<Componente, Integer>{

}
