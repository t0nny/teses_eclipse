package ec.edu.upse.acad.ws.eva;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.upse.acad.model.pojo.evaluacion.EvaluacionDocenteGeneral;
import ec.edu.upse.acad.model.repository.evaluacion.EvaluacionDocenteGeneralRepository;
import ec.edu.upse.acad.model.service.eva.EvaluacionDocenteGeneralService;

@RestController
@RequestMapping("/api/evaluacionDocenteGeneral")
@CrossOrigin
public class EvaluacionDocenteGeneralController {
	
	@Autowired private EvaluacionDocenteGeneralRepository evaluacionDocenteGeneralRepository;
	@Autowired private EvaluacionDocenteGeneralService evaluacionDocenteGeneralService;

	/**
	 * Método que me permite listar todas las evaluaciones de docencia general
	 * 
	 * @return Objeto con el resultado esperado.
	 */
	@RequestMapping(value="/getAllEvalDocGral", method=RequestMethod.GET)//filtrar por usuario????
	public ResponseEntity<?> getAllEvalDocGral(){
		return ResponseEntity.ok(evaluacionDocenteGeneralRepository.getAllEvalDocGral());
	}
	
	/**
	 * Método que me permite conocer si un registro de evlauación docente general se encuentra en un período de evaluación
	 * 
	 * @param idEvalDocGral identificador de evaluación docente general
	 * @return Objeto con resultado esperado.
	 */
	@RequestMapping(value="/getPEvalDoc/{idEvalDocGral}", method=RequestMethod.GET)//filtrar por usuario????
	public ResponseEntity<?> getPEvalDoc(@PathVariable("idEvalDocGral") Integer idEvalDocGral){
		return ResponseEntity.ok(evaluacionDocenteGeneralRepository.getPEvalDoc(idEvalDocGral));
	}
	
	/**
	 * Método que me permite obtener todos los reglamentos
	 * @return Objeto con todos los reglamentos activos
	 */
	@RequestMapping(value="/getReglamentos", method=RequestMethod.GET)//filtrar por usuario????
	public ResponseEntity<?> getReglamentos(){ 
		return ResponseEntity.ok(evaluacionDocenteGeneralRepository.getReglamentos());
	}
	
	/**
	 * Método que me permite grabar o editar una Evaluación Docente general
	 * @param evaluacionDocenteGeneral
	 * @return Objeto con los datos que se guardaron.
	 */
	@RequestMapping(value="/saveEvalDocenteGeneral", method=RequestMethod.POST)
	public ResponseEntity<?> saveEvalDocenteGeneral(@RequestBody EvaluacionDocenteGeneral evaluacionDocenteGeneral){
		evaluacionDocenteGeneralService.saveEvalDocenteGeneral(evaluacionDocenteGeneral);
		return ResponseEntity.ok(evaluacionDocenteGeneral);
	}
	

	/**
	 * Método que me permite obtener una evaluación docente general por medio de un identificador.
	 * @param idEvalDocGral Identificador de Evaluación Docente General.
	 * @return Objeto con resultado esperado.
	 */
	@RequestMapping(value="/getEvalDocGralById/{idEvalDocGral}", method=RequestMethod.GET)//filtrar por usuario????
	public ResponseEntity<?> getEvalDocGralById(@PathVariable("idEvalDocGral") Integer idEvalDocGral){
		return ResponseEntity.ok(evaluacionDocenteGeneralRepository.getEvalDocGralById(idEvalDocGral));
	}
	
	/**
	 * Método que me permite eliminar una Evaluación Docente General por medio de un identificador.
	 * @param idEvalDocGral Identificador de Evaluación Docente General.
	 * @return respuesta del método.
	 */
	@RequestMapping(value = "/deleteEvaluacionDocenteGeneral/{idEvalDocGral}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteEvaluacionDocenteGeneral(@PathVariable("idEvalDocGral") Integer idEvalDocGral){
		evaluacionDocenteGeneralService.deleteEvaluacionDocenteGeneral(idEvalDocGral);
		return ResponseEntity.ok().build();
	}
}
