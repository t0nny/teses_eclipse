package ec.edu.upse.acad.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ec.edu.upse.acad.model.pojo.AsignaturaOrganizacion;

@Repository
public interface AsignaturaOrganizacionRepository extends JpaRepository<AsignaturaOrganizacion, Integer> {

	// Lista las materias agrupadas por niveles (a�os y semestre) y las ubica en su
	// correspondiente UOC - AB
	@Query(value = " SELECT CONCAT('NIVEL ',CAST(n.orden as text)) as detalle,co.id as estado,co.descripcion as nombre ,n.id as resourceId,'#ff7878' as hex  "
			+ " from Nivel n inner join MallaAsignatura ma on ma.idNivel = n.id "
			+ " INNER JOIN AsignaturaOrganizacion ao on ao.idMallaAsignatura = ma.id "
			+ " INNER JOIN ComponenteOrganizacion co on ao.idCompOrganizacion = co.id "
			+ " inner join TipoCompOrganizacion tco on tco.id = co.idTipoCompOrganizacion "
			+ " WHERE tco.abreviatura ='UOC' and ma.idMalla=(?1) and ma.estado='A' and ao.estado='A' and n.orden<=(select m.numNiveles from Malla AS m where m.id = (?1))"
			+ " group by n.orden,co.id,co.descripcion,n.id")
	List<CustomObjectNivelMalla> listarNivelesUnidades(Integer idMalla);

	interface CustomObjectNivelMalla {
		// Integer getId();
		String getDetalle();

		Integer getEstado();

		String getNombre();

		String getResourceId();

		String getHex();
	}


	@Modifying
	@Query("update AsignaturaOrganizacion ao set ao.estado = 'I'  where ao.id in(?1) ")
	void actualizaAo(Integer idAo);

	// filtrar por campo de formacion
	@Query(value = "select ao.id,a.descripcion,ma.estado,ao.idCompOrganizacion,ao.idMallaAsignatura,ao.version "
			+ "from Asignatura a " + "inner join MallaAsignatura ma on ma.idAsignatura = a.id "
			+ "inner join AsignaturaOrganizacion ao on ma.id = ao.idMallaAsignatura "
			+ "inner join ComponenteOrganizacion co on ao.idCompOrganizacion = co.id "
			+ "inner join TipoCompOrganizacion tco on co.idTipoCompOrganizacion = tco.id "
			+ "where tco.id = 1 and ma.idMalla = ?1 and ma.estado = 'A' and ao.estado='A'")
	List<CustomObjectCampo> filtrarporcampoformacion(Integer id);

	// ao.idMallaAsignatura
	interface CustomObjectCampo {
		Integer getId();

		String getMateria();

		String getEstado1();

		Integer getEstado();

		Integer getResourceId();

		Integer getTags();
	}

	// Filtrar materias por campos de formacion, Busqueda por Idmalla y idoferta
	@Query(value = "select av.descripcion,ma.id,ma.estado from Malla m "
			+ "left join MallaAsignatura ma on ma.idMalla = m.id "
			+ "left join Asignatura av on ma.idAsignatura = av.id "
			+ "INNER JOIN OfertaAsignatura oa ON oa.asignatura.id = av.id "
			+ "INNER JOIN Oferta o on oa.oferta.id = o.id "
			+ "where ma.id not in (select ao.idMallaAsignatura from AsignaturaOrganizacion ao "
			+ "inner join ComponenteOrganizacion co on ao.idCompOrganizacion = co.id "
			+ "inner join TipoCompOrganizacion tco on co.idTipoCompOrganizacion = tco.id "
			+ "where  ao.estado='A' and tco.id = 1) and ma.estado='A' and m.id = ?1 and oa.oferta.id=?2")
	List<CustomObjectAsignaturaOrganizacion> filtrarmateriasporcamposdeformacion(Integer id, Integer idOferta);

	interface CustomObjectAsignaturaOrganizacion {
		String getMateria();

		Integer getResourceId();

		String getEstado();
	}

	// Servicio para filtrar la malla componente aprendizaje
	@Query(value = "select ma.idNivel,aa.id,a.descripcion,aa.idComponenteAprendizaje,ca.descripcion,aa.valor "
			+ " from AsignaturaAprendizaje aa "
			+ " inner join ComponenteAprendizaje ca on aa.idComponenteAprendizaje = ca.id "
			+ " left join MallaAsignatura ma on aa.idMallaAsignatura = ma.id "
			+ " inner join Asignatura a on ma.idAsignatura = a.id " + " inner join Nivel n on ma.idNivel = n.id "
			+ " inner join Malla m on ma.idMalla = m.id " + " where m.id = (?1) and aa.estado = 'A' "
			+ " order by ma.idNivel")
	List<CustomObjectAsignaturaComponente> filtrarmallacomponenteaprendizaje(Integer id);

	interface CustomObjectAsignaturaComponente {
		Integer getNivel();
		Integer getIdAsignaturaAprendizaje();
		String getDescripcion();
		Integer getidComponenteAprendizaje();
		String getDescripcionCompApren();
		String getValor();
	}


	/**
	 * Lista los componentes de organizacion que tengan el tipo Unidad de
	 * Organización Curricular(UOC)
	 **/
	@Query(value = " SELECT co.id as dataField,co.descripcion as text,co.abreviatura  as abreviatura"
			+ "	  from ComponenteOrganizacion co "
			+ "	  inner join TipoCompOrganizacion tco on tco.id = co.idTipoCompOrganizacion "
			+ "	  WHERE tco.abreviatura ='UOC' order by tco.id")
	List<ListaUOC> listarUOC();

	interface ListaUOC {
		Integer getDataField();
		String getText();
		String getAbreviatura();
	}

}
